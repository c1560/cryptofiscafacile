# Crypto Fisca Facile [![Latest Release](https://gitlab.com/c1560/cryptofiscafacile/-/badges/release.svg)](https://gitlab.com/c1560/cryptofiscafacile/-/releases) [![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/c1560/cryptofiscafacile/wasm)](https://goreportcard.com/report/gitlab.com/c1560/cryptofiscafacile/wasm)
[![Build status](https://gitlab.com/c1560/cryptofiscafacile/badges/develop/pipeline.svg)](https://gitlab.com/c1560/cryptofiscafacile/-/commits/develop)
[![Coverage Report](https://gitlab.com/c1560/cryptofiscafacile/badges/develop/coverage.svg)](https://gitlab.com/c1560/cryptofiscafacile/-/commits/develop)

## Présentation

CryptoFiscaFacile est un outil open source conçu pour aider les utilisateurs à gérer leur fiscalité en ce qui concerne les actifs numériques tels que les crypto-monnaies. Il est construit avec une attention particulière à la vie privée, garantissant que vos données financières restent en sécurité et confidentielles.

Toutes vos données personnelles restent dans votre navigateur sur votre PC, rien n'est envoyé sur des serveurs. Vous pouvez vous en convaincre en essayant d'y ajouter un CSV/XLS/JSON puis en utilisant un autre navigateur qui ne sera au courant de rien...

Il est facile à utiliser et vous permet de suivre vos transactions en temps réel, de calculer vos gains et pertes en capital et de générer des déclarations fiscales précises. Il prend en charge une large gamme de monnaies virtuelles et est constamment mis à jour pour s'assurer qu'il est en conformité avec les lois fiscales les plus récentes.

## Utilisation simple

Si vous souhaitez utiliser CryptoFiscaFacile sans avoir à le recompiler chez vous, allez simplement sur [la version publique en ligne gratuite](https://crypto.fiscafacile.com).

Il est possible de faire tourner CryptoFiscaFacile en local. Pour ce faire suivez la documentation.

## Documentation

Une documentation développeur est disponible à [https://docs.crypto.fiscafacile.com](https://docs.crypto.fiscafacile.com).
