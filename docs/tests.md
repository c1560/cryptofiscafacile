# Tests

## Moteur CFF

Dans /wasm :

- `make test`

## Frontend CFF

Dans /frontend :

### Pour des tests en ligne de commande

- `yarn test` # tous les tests par ordre
- `yarn test --filter "Integration | Component | App::Header::EngineIndicator"` # tous les tests dont le nom du module inclus "Integration | Component | App::Header::EngineIndicator"
- `yarn test:random` # tous les tests en aléatoire

### Pour des tests accessibles sur un navigateur

A l'adresse [http://localhost:7357/](http://localhost:7357/)

- `yarn test:server` # tous les tests
- `yarn test:server --filter "Integration | Component | App::Header::EngineIndicator"` # tous les tests dont le nom du module inclus "Integration | Component | App::Header::EngineIndicator"
- `yarn test:server:random` # tous les tests en aléatoire
