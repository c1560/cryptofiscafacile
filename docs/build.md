# Compilation

Ce guide couvre seulement une compilation et une utilisation sur un environement Linux.

Pré-Requis :

- [Go 1.19](https://tip.golang.org/learn/)
- [Volta](https://docs.volta.sh/guide/getting-started)
- [pip](https://pip.pypa.io/en/stable/getting-started/)
- [Python](https://www.python.org/about/gettingstarted/)
- [mkdocs](https://www.mkdocs.org/getting-started/)

## Moteur CFF

À la racine du projet, afin de compilé de moteur, entrer les commandes suivantes :

- `yarn install`
- `yarn wasm:build`

## Frontend CFF

À la racine du projet, afin de compiler et lancer l'application, entrer les commandes suivantes :

- `cd frontend`
- `yarn start`

L'application est dès lors accessible à l'adresse [http://localhost:4200](http://localhost:4200)

## Documentation CFF

À la racine du projet, afin d'accéder à la documentation, entrer les commandes suivantes :

- `mkdocs serve`

La documentation est dès lors accessible à l'adresse [http://localhost:8000](http://localhost:8000)

## Variables d'environement

Certaines requêtes externes depuis une utilisation locale doivent utiliser un Proxy pour éviter les erreurs CORS :
`PROXY_URL` : URL par défaut du proxy CORS
