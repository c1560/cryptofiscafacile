# Bienvenue!

CryptoFiscaFacile est un outil open source conçu pour aider les utilisateurs à gérer leur fiscalité en ce qui concerne les actifs numériques tels que les crypto-monnaies. Il est construit avec une attention particulière à la vie privée, garantissant que vos données financières restent en sécurité et confidentielles.

Toutes vos données personnelles restent dans votre navigateur sur votre PC, rien n'est envoyé sur des serveurs. Vous pouvez vous en convaincre en essayant d'y ajouter un CSV/XLS/JSON puis en utilisant un autre navigateur qui ne sera au courant de rien...

Il est facile à utiliser et vous permet de suivre vos transactions en temps réel, de calculer vos gains et pertes en capital et de générer des déclarations fiscales précises. Il prend en charge une large gamme de monnaies virtuelles et est constamment mis à jour pour s'assurer qu'il est en conformité avec les lois fiscales les plus récentes.

## Arborescence CFF

```
├── docs/        # Documentation de CFF
├── frontend/    # Interface graphique de CFF en emberjs
├── wasm/        # Moteur de CFF en go puis compilé en wasm
```

### Frontend

```
├── frontend/    # Interface graphique de CFF en emberjs
    ├──  app/
        ├──  components/
            ├──  ac5/           # Figures amChart de la page Synthèse
            ├──  api/           # Page Outils
            ├──  app/
            ├──  calendar/
            ├──  documentation/ # Page Documentation
            ├──  download/
            ├──  help/
            ├──  removal/
            ├──  reporting/     # Page Rapports
            ├──  synthesis/     # Page Synthèse
            ├──  tx-table/      # Page Transactions
            ├──  user-wallets/  # Page Portefeuilles
        ├──  controllers/
        ├──  enums/
        ├──  initializers/
        ├──  modifiers/
        ├──  objects/
        ├──  pods/              # CFF utliise les pods ember
        ├──  routes/
        ├──  services/
        ├──  styles/            # Feuilles de style SCSS/Sass
        ├──  templates/
        ├──  utils/
        ├── app.ts
        ├── formats.js
        ├── index.html          # Base
        └── router.js           # Routes
    ├──  public/                # Images, icônes, JSON de définitions et base des taux
    ├──  recordings/
    ├──  scripts/
    ├──  tests/                 # Tests unitaires et d'intégration
    ├──  translations/          # Fichiers de traduction
    ├──  types/                 # Types globaux
```

### Moteur CFF & Wasm

```
├── wasm/                   # Moteur de CFF en go puis compilé en wasm
    ├──  api/
    ├──  csv/
    ├──  internal/          # Services Go, tests Go
    ├──  json/
    ├──  rate/
    ├──  src/               # Sources WASM pour le frontend
    ├──  token/
    ├──  tx/
    ├──  wallet/
    ├── file.go             # API Go pour la gestion des fichiers en entrée
    ├── indexeddb.go        # Gestion localStorage du navigateur
    ├── main.go             # Base
    ├── rates.go            # Gestion des requêtes vers Coingecko...
    ├── reports.go          # API Go pour la page rapports
    ├── synthesis.go        # API Go pour la page synthèse
    ├── synthesis_test.go
    ├── test.sh*
    ├── tools.go            # API Go pour la page outils
    ├── transactions.go     # API Go pour la page transactions
    └── wallets.go          # API Go pour la page portefeuille
```
