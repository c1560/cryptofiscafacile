# Sparrow wallet

L'export de sparrow doit se faire sans monnaie FIAT configurée.

Allez dans File > Preferences

![Preferences](preferences.png)

Et Selectionnez None dans exchange rate source

![No exchange source](no-exchange-source.png)
