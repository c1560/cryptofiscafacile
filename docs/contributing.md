# Contribuer

## Sémantique des commits

Merci d'utiliser le format suivant pour vos commits :
`#ticket-id <message compréhensible du commit>`

- Si vous êtes génés par un commit qui commence par `#`, qui sera considéré par un commentaire dans l'éditeur de texte, il suffit de préfixer ce commit par un 'espace'.
- Éviter les messages comme `#100 update file` : ce message n'apporte aucun contexte sur le commit, impossible de savoir ce qui a été implémenté hormis le fait qu'il est lié au ticket 100.
- Éviter de nombreux commits dans une même Merge Request. Utiliser [git rebase](https://git-scm.com/book/en/v2/Git-Branching-Rebasing) pour renommer ou fusionner des commits.
- Éviter les mot-clés comme 'wip' dans les commits.

## Changelog

- Chaque Merge Request qui correspond à un _changement visible_ par l'utilisateur doit apparaitre dans le fichier **CHANGELOG.md**
- Suffixer chaque changelog par la référence (!merge-request-id) de la Merge Request.
  Exemple : - voici une modification dans le changelog (!mr)
- Si plusieus Merge Requests correspondent à un même changement dans une même version (exemple : une 'feature' suivi par un 'bugfix'), préférer n'avoir qu'une _seule ligne_ suivie des références des Merge Request, comme ceci (!mr1, !mr2).
