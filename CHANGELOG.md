# Changelog

Tous les changements notifiés aux utilisateurs entre les versions doivent être documentés dans ce fichier.

Le format est inspiré de [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

# [Unreleased]

## Ajouté

- Binance: Type 'Buy Crypto' comme cashin (!1246)
- Binance: Type 'Buy Crypto With Fiat' comme dépôt (!1246)
- Binance: Type 'Cashback Voucher' comme remise commerciale (!1246)
- Binance: Type 'Crypto Box' comme remise commerciale (!1244)
- Binance: Type 'Launchpool Earnings Withdrawal' comme minage (!1245)
- Binance: Type 'Send' comme retrait (!1246)
- Crypto.com: Type 'finance.dpos.compound_interest.crypto_wallet' comme minage (!1243)
- Crypto.com: Type 'finance.dpos.non_compound_interest.crypto_wallet' comme minage (!1242)
- Crypto.com: Type 'finance.dpos.staking.crypto_wallet' comme ignoré (!1242)
- Crypto.com: Type 'finance.dpos.unstaking.crypto_wallet' comme ignoré (!1243)
- Crypto.com: Type 'finance.lockup.dpos_compound_interest.crypto_wallet' comme intérêt (!1241)
- Crypto.com: Type 'finance.lockup.dpos_lock.crypto_wallet' comme ignoré (!1243)
- Crypto.com: Type 'trading.limit_order.fiat_wallet.purchase_lock' non affiché (!1242)
- Crypto.com: Type 'trading.limit_order.fiat_wallet.purchase_unlock' non affiché (!1242)
- Nexo: Nouveau format CSV (!1229)
- Phoenix: Support plateforme (!1228)
- Sparrow: Nouveau format CSV (!1256)

## Modifié

- Sparrow: Ancien format avec une date europe/paris (!1256)

## Corrigé

# [2.4.0]

## Ajouté

- Binance: Type 'Airdrop Assets' comme dépôt (!1148)
- Binance: Type 'Asset Recovery' comme retrait (!1148)
- Binance: Type 'Mission Reward Distribution' comme remise commerciale (!1093)
- Binance: Type 'Token Swap - Distribution' comme dépôt (!1148)
- Binance: Type 'Token Swap - Redenomination/Rebranding' comme dépôt/retrait (!1148)
- Binance: Type 'Transaction Fee' comme échanges (!1093)
- Binance: Type 'Transfer Between Main Account/Futures and Margin Account' comme ignoré (!1093)
- Coinhouse: Support fichier avancé (!1167)
- Finary: Support plateforme (!1169)
- Kraken: Nouveau format CSV (!1144)
- Ledger Live: Nouveau format CSV (!1224)
- Ledger Live: Type 'REDELEGATE' comme frais (!1224)
- Ledger Live: Type 'UNDELEGATE' comme frais (!1147)
- Rayn: Support plateforme (!1223)

## Modifié

- Augmentation du nombre de requêtes vers coingecko (!1141)
- Frais de cession maintenant à zero dans un but de simplification (!1166)
- Kraken: Type 'transfer' avec sous-type 'spotfromfutures' est maintenant catégorisé en dépôt (!1097)
- Kraken: Type 'transfer' avec sous-type 'spottofutures' est maintenant catégorisé en retrait (!1097)
- Kraken: Actif ETH2 est maintenant ETH (!1125)
- Sparrow: Nouveau format de date, réimportation de tous vos fichiers necessaires (!1162)

## Corrigé

- Amélioration de la sélection des jetons (!1151)
- Fixation des actifs BTC -> bitcoin (!1140)
- Fixation des actifs DAI -> dai, ETH -> ethereum, USDT -> tether, XRP -> ripple (!1092)
- Fixation des actifs XLM -> stellar (!1128)
- Kraken: Débloquage des gains ethers suite à Shapella sont maintenant ignorés au lieu d'être catégorisé en minage (!1129)
- Utilisation d'un proxy pour les prix car coingecko limité à un an (!1140)
- Uphold: Retrait dans un autre actif pouvait générer des montants incorrects (!1127)

# [2.3.2]

## Ajouté

- Ajout d'un 2086 prévisionnel (!1029)
- Calcul des statistiques de plus value latente / réalisée (!1019)

### CSV

- Binance: Type 'Binance Card Cashback' comme remise commerciale (!1002)
- Binance: Type 'Binance Convert' comme échanges (!991)
- Binance: Type 'Main and Funding Account Transfer' comme ignoré (!991)
- Binance: Type 'Referral Commission' comme remise commerciale (!991)
- Binance: Type 'Savings Distribution' comme ignoré (!991)
- Binance: Type 'Simple Earn Flexible Airdrop' comme airdrop (!1000)
- Binance: Type 'Small Assets Exchange BNB' comme échanges (!991)
- Binance: Type 'Transfer Between Main and Funding Wallet' comme ignoré (!1002)
- Binance: Type 'Transfer from Main Account/Futures to Margin Account' comme ignoré (!991)
- Binance: Type 'Transfer from Margin Account to Main Account/Futures' comme ignoré (!991)
- Binance: Type 'Transaction Buy' comme échanges (!991)
- Binance: Type 'Transaction Revenue' comme échanges (!991)
- Binance: Type 'Transaction Sold' comme échanges (!991)
- Binance: Type 'Transaction Spend' comme échanges (!991)
- CryptoComApp: Type 'supercharger_reward_to_app_credited' comme minage (!999)
- Sparrow: Support nouvelle entête (!1001)

## Modifié

- Non génération des années inutiles dans le 2086 (!1029)

## Corrigé

- Titre de l'application (!1068)

# [2.3.1]

## Ajouté

- Affichage d'une modale lors d'une nouvelle version (!866)
- Filtre de la page synthèse par portefeuille (!902)
- Export table cff avec la configuration (!979)
- Icône moteur CFF dans la navbar (!874)
- Ouverture du tour pour les nouveaux utilisateurs (!860,!935)
- Persistance de la version de CFF (!855)
- Synthèse: Tuile Balances à la place de historique (!879)
- Transactions: Icone pour les mouvements à identifier (!885)

### API

Crypto.org: Nouvelle catégorie (!674)

### CSV

- Binance: Type 'Simple Earn Locked Redemption' comme Ignoré (!944)
- Binance: Type 'Stablecoins Auto-Conversion' comme Échanges (!909)
- Binance: Type 'Staking Redemption' comme Ignoré (!909)
- Celsius: Type 'Swap in/swap out' comme Échanges (!815)
- Coinbase: Type 'Learning Reward' comme Minage (!812)
- Coinbase: Support nouvelle entête (!813)
- Kraken: Type 'dividend' comme AirDrop (!893)
- Nexo: Support nouvelle entête (!814)
- Nexo: Type 'ReferralBonus' comme Parrainages (!939)
- Xumm: Support du csv (!785)

## Modifié

- Ajout case 3AN/3BN dans le rapport 2086 (!888)
- Texte en gras dans le rapport 2086 (!886)
- Calendrier mis à jour pour la saison 2023 (!828)
- Persistance de la liste des actifs de coingecko (!622)
- Symbole OMG est par défaut le token OMISEGO (!870)
- Symbole USDC est par défaut le stablecoin USDC (!870)

## Corrigé

- Synthèse: Décalage dans certaines tuiles (!873)
- Synthèse: Montant en € dans les tooltip du portfolio (!869)
- Synthèse: Intervalle du graphique Évolution (!871)
- Transactions: Affichage bouton valider (!861)
- Transactions: Troncage du nom des actifs à 6 caractères (!872)
- Documentation: Scrollbar pour des largeurs d'écran de 2560px (!546)
- Limiteur du nombre de requêtes vers coingecko (!850)

### API

- Elrond: Gestion des rewards (!629)

### CSV

- Binance: fusion des échanges sur plusieurs secondes (!945)
- Stackinsat: importation des parrainages sans la contrevaleur euro (!818)

[unreleased]: https://gitlab.com/c1560/cryptofiscafacile/-/compare/develop...v2.4.0
[2.4.0]: https://gitlab.com/c1560/cryptofiscafacile/-/compare/v2.3.2...v2.4.0
[2.3.2]: https://gitlab.com/c1560/cryptofiscafacile/-/compare/v2.3.1...v2.3.2
[2.3.1]: https://gitlab.com/c1560/cryptofiscafacile/-/compare/v2.3.0...v2.3.1
