'use strict';

const { configs } = require('@nullvoxpopuli/eslint-configs');

const config = configs.ember({ prettierIntegration: true });

// add ./scripts/**/*.js to config files
config.overrides.filter((f) => f.files.includes('./config/**/*.js'))[0].files.push('./scripts/**/*.js');

module.exports = {
  ...config,
  overrides: [
    ...config.overrides,
    {
      files: ['./**/*.js', './**/*.ts'],
      rules: {
        'prettier/prettier': ['error', { singleQuote: true, printWidth: 120, trailingComma: 'all' }],
      },
    },
  ],
};
