'use strict';

const fs = require('fs');
const path = require('path');
const { parse } = require('@babel/parser');
const Glimmer = require('@glimmer/syntax');
let globby;
let chalk;

async function importLibs() {
  chalk = (await import('chalk')).default;
  globby = (await import('globby')).globby;
}

// inspired from https://github.com/mainmatter/ember-intl-analyzer
async function analyzeIconifyIcons() {
  await importLibs();

  const rootDir = process.cwd();
  const loaderPath = 'app/initializers/offline-iconify-icons-loader.ts';
  const NUM_STEPS = 4;
  const step = (num) => chalk.dim(`[${num}/${NUM_STEPS}]`);

  // eslint-disable-next-line no-console
  console.log(`${step(1)} 🔍 Searching offline icons...`);

  const loaderFile = fs.readFileSync(loaderPath, 'utf-8');
  const ast = parse(loaderFile, {
    sourceType: 'module',
    plugins: ['typescript'],
  });
  const iconsNode = ast.program.body.filter(
    (n) =>
      n.type === 'ExportNamedDeclaration' &&
      n.declaration.declarations &&
      n.declaration.declarations.find((d) => d.id.name === 'icons'),
  )[0];
  let mapSetter = iconsNode.declaration.declarations[0].init;
  const offlineIcons = new Map();

  do {
    if (mapSetter.arguments[0]) {
      offlineIcons.set(mapSetter.arguments[0].value, mapSetter);
    }

    mapSetter = mapSetter.callee.object;
  } while (mapSetter);

  // eslint-disable-next-line no-console
  console.log(`${step(2)} 🔍 Searching for icons in HBS files...`);

  let files = await findAppFiles(rootDir);
  let usedIcons = await analyzeFiles(rootDir, files);

  // eslint-disable-next-line no-console
  console.log(`${step(3)} ⚙️  Checking for unused offline icons...`);

  const unusedOfflineIcons = Array.from(offlineIcons.keys()).filter(
    (i) =>
      !Array.from(usedIcons.keys())
        .map((i) => 'cff:' + i.name)
        .includes(i),
  );

  if (unusedOfflineIcons.length > 0) {
    unusedOfflineIcons.sort(
      (a, b) => getOfflineIconLine(offlineIcons.get(a)) - getOfflineIconLine(offlineIcons.get(b)),
    );
    console.error(`${rootDir}/${loaderPath}`);
    unusedOfflineIcons.forEach((i) => {
      console.error(
        ` ${offlineIcons.get(i).arguments[0].loc.start.line} ${chalk.red('error')} unused offline icon ${i}`,
      );
    });
    console.error();
    process.exitCode = 1;
  }

  // eslint-disable-next-line no-console
  console.log(`${step(4)} ⚙️  Checking for missing offline icons...`);

  const missingOfflineIcons = Array.from(usedIcons.keys()).filter(
    (i) => !Array.from(offlineIcons.keys()).includes('cff:' + i.name),
  );

  if (missingOfflineIcons.length > 0) {
    missingOfflineIcons.forEach((i) => {
      console.error(`${rootDir}/${Array.from(usedIcons.get(i))[0]}`);
      console.error(
        ` ${i.loc.start.line + ':' + i.loc.start.column} ${chalk.red('error')} missing offline icon ${'cff:' + i.name}`,
      );
    });
    console.error();
    process.exitCode = 1;
  }
}

function getOfflineIconLine(node) {
  return node.arguments[0].loc.start.line;
}

async function findAppFiles(cwd) {
  let extensions = ['.hbs'];

  let pathsWithExtensions = extensions.map((extension) => 'app/**/*' + extension);

  return globby(pathsWithExtensions, { cwd });
}

async function analyzeFiles(cwd, files, options) {
  let icons = new Map();

  for (let file of files) {
    let fileIcons = await analyzeFile(cwd, file, options);

    for (let icon of fileIcons) {
      if (icons.has(icon)) {
        icons.get(icon).add(file);
      } else {
        icons.set(icon, new Set([file]));
      }
    }
  }

  return icons;
}

async function analyzeFile(cwd, file, options) {
  let content = fs.readFileSync(`${cwd}/${file}`, 'utf8');
  let extension = path.extname(file).toLowerCase();

  if (extension === '.hbs') {
    return analyzeHbsFile(content, options);
  } else {
    throw new Error(`Unknown extension: ${extension} (${file})`);
  }
}

async function analyzeHbsFile(content) {
  let icons = [];

  // parse the HBS file
  let ast = Glimmer.preprocess(content);

  // find used icons in the syntax tree
  Glimmer.traverse(ast, {
    // <App::Icon @icon="ion:help-circle" />
    ElementNode(node) {
      processNode(node);
    },
  });

  function processNode(node) {
    if (node.tag !== 'App::Icon') return;

    const iconAttr = node.attributes.find((a) => a.name === '@icon');

    if (iconAttr.value.type === 'TextNode') {
      icons.push({
        name: iconAttr.value.chars,
        loc: iconAttr.value.loc,
      });
    } else if (iconAttr.value.type === 'MustacheStatement' && iconAttr.value.path.type === 'PathExpression') {
      icons.push({
        name: iconAttr.value.params[1].value,
        loc: iconAttr.value.params[1].loc,
      });
      icons.push({
        name: iconAttr.value.params[2].value,
        loc: iconAttr.value.params[2].loc,
      });
    }
  }

  return icons;
}

analyzeIconifyIcons();
