export function forceModulesToBeLoaded(filterFunction?: (type: string, module: string) => boolean): void;
export function sendCoverage(callback?: () => void): Promise<void>;
