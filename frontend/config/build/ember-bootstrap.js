'use strict';

module.exports = {
  bootstrapVersion: 5,
  importBootstrapCSS: false,
  importBootstrapFont: false,
  include: [],
  insertEmberWormholeElementToDom: false,
};
