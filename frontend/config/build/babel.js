'use strict';

const emberCliCodeCoverage = require('ember-cli-code-coverage');

module.exports = {
  // enable "loose" mode
  loose: true,
  plugins: [
    ...(process.env.COVERAGE ? emberCliCodeCoverage.buildBabelPlugin() : []),
    require.resolve('ember-auto-import/babel-plugin'),
  ],
  // https://github.com/babel/ember-cli-babel#enabling-source-maps
  sourceMaps: 'inline',
};
