'use strict';

function addonConfigs() {
  return {
    babel: require('./babel'),
    'ember-bootstrap': require('./ember-bootstrap'),
    'ember-composable-helpers': require('./ember-composable-helpers'),
    'ember-cli-string-helpers': require('./ember-cli-string-helpers'),
    'ember-fetch': {
      preferNative: true,
    },
    postcssOptions: require('./postcss'),
  };
}

module.exports = {
  addonConfigs,
};
