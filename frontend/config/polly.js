'use strict';

module.exports = function (env) {
  return {
    // Addon Configuration Options
    enabled: env === 'test',
  };
};
