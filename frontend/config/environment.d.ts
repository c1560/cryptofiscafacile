export default config;

/**
 * Type declarations for
 *    import config from 'cryptofiscafacile-gui/config/environment';
 *
 * For now these need to be managed by the developer
 * since different ember addons can materialize new entries.
 */
declare const config: {
  environment: 'development' | 'production' | 'staging' | 'test';
  locationType: string;
  modulePrefix: string;
  podModulePrefix: string;
  rootURL: string;
  APP: { version: string; proxyUrl: string; [key: string]: unknown };
};
