'use strict';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');
const Funnel = require('broccoli-funnel');
const UnwatchedDir = require('broccoli-source').UnwatchedDir;
const MergeTrees = require('broccoli-merge-trees');
const { addonConfigs } = require('./config/build/addons');

module.exports = function (defaults) {
  const environment = EmberApp.env();
  const isTest = environment === 'test';

  const appOptions = {
    fingerprint: {
      exclude: ['nofingerprint'],
    },
    flatpickr: {
      locales: ['fr'],
    },
    sourcemaps: {
      enabled: true,
      extensions: ['js'],
    },

    tests: isTest, // Don't even generate test files unless a test build

    ...addonConfigs(),
  };

  const app = new EmberApp(defaults, appOptions);

  const fonts = ['inter', 'lato', 'quicksand'].map(
    (font) =>
      // @ts-ignore
      new Funnel(new UnwatchedDir(`../node_modules/@fontsource/${font}/files`), {
        destDir: `assets/@fontsource/${font}`,
        include: ['*.eot', '*.ttf', '*.woff', '*.woff2'],
      }),
  );

  app.import('node_modules/cryptofiscafacile-wasm/dist/cff2.wasm', {
    destDir: '',
  });

  app.import('node_modules/cryptofiscafacile-wasm/dist/worker.js', {
    destDir: '',
    outputFile: 'worker.js',
    type: 'vendor',
  });

  app.import('node_modules/cryptofiscafacile-wasm/dist/wasm_exec.js', {
    destDir: '',
    outputFile: 'wasm_exec.js',
    type: 'vendor',
  });

  return MergeTrees([app.toTree(), ...fonts]);
};
