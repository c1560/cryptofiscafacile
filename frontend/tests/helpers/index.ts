import {
  setupApplicationTest as upstreamSetupApplicationTest,
  setupRenderingTest as upstreamSetupRenderingTest,
  setupTest as upstreamSetupTest,
} from 'ember-qunit';

import { setupIntl } from 'ember-intl/test-support';

import { setupPolly } from './polly-helper';
import { setupCleanDatabase, setupWasmFrontendTest } from './wasm-helper';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';
import type EmberResolver from 'ember-resolver';

interface SetupTestOptions {
  /**
   * The resolver to use when instantiating container-managed entities in the test.
   */
  resolver?: EmberResolver;
}

// This file exists to provide wrappers around ember-qunit's / ember-mocha's
// test setup functions. This way, you can easily extend the setup that is
// needed per test type.
export function setupApplicationTest(hooks: NestedHooks, options?: SetupTestOptions) {
  upstreamSetupApplicationTest(hooks, options);

  // Additional setup for application tests can be done here.
  setupIntl(hooks, ['fr']);
  setupPolly(hooks);
  setupWasmFrontendTest(hooks);
  setupCleanDatabase(hooks);
}

export function setupRenderingTest(hooks: NestedHooks, options?: SetupTestOptions) {
  upstreamSetupRenderingTest(hooks, options);

  // Additional setup for rendering tests can be done here.
  setupIntl(hooks, ['fr']);
  setupPolly(hooks);
  setupWasmFrontendTest(hooks);
  setupCleanDatabase(hooks);
}

export function setupTest(hooks: NestedHooks, options?: SetupTestOptions) {
  upstreamSetupTest(hooks, options);

  // Additional setup for unit tests can be done here.
  setupIntl(hooks, ['fr']);
  setupPolly(hooks);
  setupWasmFrontendTest(hooks);
  setupCleanDatabase(hooks);
}

export function setupWasmDefinitionTest(hooks: NestedHooks, csvDef: string, walletDef?: string) {
  setupTest(hooks);
  hooks.beforeEach(async function () {
    const wasm = this.owner.lookup('service:wasm') as WasmService;

    await wasm.updateDefinitionCsvDef(csvDef);

    if (walletDef) {
      await wasm.updateDefinitionWalletDef(walletDef);
    }
  });
}
