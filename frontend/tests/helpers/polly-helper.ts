import { setupQunit } from '@pollyjs/core';

import FastPassthroughFetchAdapter from './fast-passthrough-fetch-adapter';

import type { Polly } from '@pollyjs/core';
import type { TestContext } from 'ember-intl/test-support';

export interface PollyContext extends TestContext {
  polly: Polly;
}

export function setupPolly(hooks: NestedHooks) {
  setupQunit(hooks);

  hooks.beforeEach(function (this: PollyContext) {
    this.polly.configure({
      adapters: [FastPassthroughFetchAdapter],
      flushRequestsOnStop: true,
      //logLevel: 'DEBUG',
      matchRequestsBy: {
        url: {
          port: false,
        },
      },
    });

    const { server } = this.polly;

    server.any('/**').passthrough();
  });
}
