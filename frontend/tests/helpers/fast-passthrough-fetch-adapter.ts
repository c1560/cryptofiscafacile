import FetchAdapter from '@pollyjs/adapter-fetch';

import type { Request, RequestArguments } from '@pollyjs/core';

export default class FastPassthroughFetchAdapter extends FetchAdapter {
  declare nativeFetch: typeof fetch;

  async onPassthrough(pollyRequest: Request<RequestArguments>) {
    if (pollyRequest.action === 'passthrough' && this.nativeFetch) {
      // don't convert reponse body to string in case of passthrough request
      const { context } = this.options;
      const { options } = pollyRequest.requestArguments;

      const response = await this.nativeFetch.apply(context, [
        pollyRequest.url,
        {
          ...options,
          method: pollyRequest.method,
          headers: pollyRequest.headers,
          body: pollyRequest.body,
        },
      ]);

      const pollyResponse = {
        statusCode: response.status,
        headers: this.serializeHeaders(response.headers),
        body: response.body,
      };

      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      await pollyRequest.respond(pollyResponse);
    } else {
      await super.onPassthrough(pollyRequest);
    }
  }

  serializeHeaders(headers: Headers) {
    if (headers && typeof headers.forEach === 'function') {
      const serializedHeaders: Record<string, string> = {};

      headers.forEach((value, key) => (serializedHeaders[key] = value));

      return serializedHeaders;
    }

    return headers || {};
  }
}
