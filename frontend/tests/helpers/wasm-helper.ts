import { settled } from '@ember/test-helpers';

import type ApplicationService from 'cryptofiscafacile-gui/services/application';
import type IndexeddbService from 'cryptofiscafacile-gui/services/indexeddb';
import type WasmService from 'cryptofiscafacile-gui/services/wasm';

export function setupHasIndexedDb(hooks: NestedHooks) {
  hooks.beforeEach(async function () {
    const application = this.owner.lookup('service:application') as ApplicationService;

    application.hasIndexedDb = true;
  });
}

export function setupCleanDatabase(hooks: NestedHooks) {
  setupHasIndexedDb(hooks);
  hooks.afterEach(async function () {
    await settled();

    const indexeddb = this.owner.lookup('service:indexeddb') as IndexeddbService;

    await indexeddb.cleanDatabases();
  });
}

export function setupWasmFrontendTest(hooks: NestedHooks) {
  setupHasIndexedDb(hooks);
  hooks.beforeEach(async function () {
    const wasm = this.owner.lookup('service:wasm') as WasmService;

    await wasm.setupFrontendTestTask.perform(/*don't fetch rates by default*/ false);
  });
}
