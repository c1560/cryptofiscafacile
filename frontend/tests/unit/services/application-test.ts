import { module, test } from 'qunit';

import { setupTest } from 'cryptofiscafacile-gui/tests/helpers';

import type ApplicationService from 'cryptofiscafacile-gui/services/application';

module('Unit | Service | application', function (hooks) {
  setupTest(hooks);

  test('which browser is installed', function (assert) {
    let service = this.owner.lookup('service:application') as ApplicationService;

    assert.ok(service);
  });
});
