import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';
import type { Cerfa2086YearResultData } from 'cryptofiscafacile-wasm';

module('Unit | Service | wasm | reports', function (hooks) {
  setupWasmDefinitionTest(hooks, 'CryptoFiscaFacile', 'CryptoFiscaFacile');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it add to pta gifts', async function (assert) {
    // Given
    await wasm.setFetchRates.perform(true);

    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2019-01-02 00:00:00,Don,,,5,BTC,,,Don entrant 5 BTC\n` +
          `2019-04-02 00:00:00,Don,1,BTC,,,,,Don sortant 1 BTC\n` +
          `2019-12-01 00:00:00,Cashout,4,BTC,10000,EUR,,,Cashout 5 BTC -> 10000 €`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);

    const results = await wasm.get2086ResultsTask.perform();

    // Then
    assert.strictEqual(results?.initialPta, 0, '2086 has a initial pta of 0€');
    assert.strictEqual(results?.initialPtaTxs.length, 0, '2086 initial pta has 0 txs');

    assert.strictEqual(results?.years[2019]?.gifts, 16066.35, '2019 year has gifts 16066€');
    assert.strictEqual(
      results?.years[2019]?.cashOuts[0]?.prixTotalAcquisition220,
      16066,
      '2019 year has a cashout pta of 16066€',
    );
  });

  test('it add to pta mining rewards', async function (assert) {
    // Given
    await wasm.setFetchRates.perform(true);

    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2019-01-02 00:00:00,Minage,,,5,BTC,,,Minage 5 BTC\n` +
          `2019-12-01 00:00:00,Cashout,5,BTC,10000,EUR,,,Cashout 5 BTC -> 10000 €`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);

    const results = await wasm.get2086ResultsTask.perform();

    // Then
    assert.strictEqual(results?.initialPta, 0, '2086 has a initial pta of 0€');
    assert.strictEqual(results?.initialPtaTxs.length, 0, '2086 initial pta has 0 txs');

    assert.strictEqual(results?.years[2019]?.minings, 16066.35, '2019 year has minings rewards of 16066€');
    assert.strictEqual(
      results?.years[2019]?.cashOuts[0]?.prixTotalAcquisition220,
      16066,
      '2019 year has a cashout pta of 16066€',
    );
  });

  test('it computes 2086', async function (assert) {
    // Given
    await wasm.setFetchRates.perform(true);

    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2017-07-17 12:00:00,Cashin,9005,EUR,5,BTC,,,Cashin 1 801 € / BTC\n` +
          `2017-12-15 12:00:00,Cashin,16346,EUR,1,BTC,,,Cashin 16 346 € / BTC\n` +
          `2019-01-09 12:00:00,Cashout,1,BTC,3744,EUR,,,Cashout 3 744 € / BTC\n` +
          `2019-03-24 12:00:00,Cashin,9280,EUR,2.5,BTC,,,Cashin 3 712 € / BTC\n` +
          `2019-03-27 12:00:00,Cashin,225,EUR,100,GRIN,,,Cashin 2.25 € / GRIN\n` +
          `2019-06-16 12:00:00,Cashout,50,GRIN,224,EUR,,,Cashout 4.48 € / GRIN\n` +
          `2019-06-25 12:00:00,Cashout,1,BTC,10463,EUR,,,Cashout 10 463 € / BTC`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);

    const results = await wasm.get2086ResultsTask.perform();

    // Then
    const r2019 = results?.years[2019] as Cerfa2086YearResultData;

    assert.strictEqual(r2019.cashOuts.length, 3, '2019 has three cashout');
    assert.deepEqual(r2019.cashOuts[0]?.date211, new Date(Date.UTC(2019, 0, 9, 11, 0, 0)));
    assert.strictEqual(r2019.cashOuts[0]?.valeurPortefeuille212, 20820, 'cession1[212]: 20 820 €');
    assert.strictEqual(r2019.cashOuts[0]?.prix213, 3744, 'cession1[213]: 3 744 €');
    assert.strictEqual(r2019.cashOuts[0]?.frais214, 0, 'cession1[214]: 0 €');
    assert.strictEqual(r2019.cashOuts[0]?.prixNetDeFrais215, 3744, 'cession1[215]: 3 744 €');
    assert.strictEqual(r2019.cashOuts[0]?.soulteRecueOuVersee216, 0, 'cession1[216]: 0 €');
    assert.strictEqual(r2019.cashOuts[0]?.prixNetDeSoulte217, 3744, 'cession1[217]: 3 744 €');
    assert.strictEqual(r2019.cashOuts[0]?.prixNet218, 3744, 'cession1[218]: 3 744 €');
    assert.strictEqual(r2019.cashOuts[0]?.prixTotalAcquisition220, 25351, 'cession1[220]: 20 820 €');
    assert.strictEqual(r2019.cashOuts[0]?.fractionDeCapital221, 0, 'cession1[221]: 0 €');
    assert.strictEqual(r2019.cashOuts[0]?.soulteRecueEnCasDechangeAnterieur222, 0, 'cession1[222]: 0 €');
    assert.strictEqual(r2019.cashOuts[0]?.prixTotalAcquisitionNet223, 25351, 'cession1[223]: 20 820 €');
    assert.strictEqual(r2019.cashOuts[0]?.plusMoinsValue, -815, 'cession1[224]: -815 €');

    assert.deepEqual(r2019.cashOuts[1]?.date211, new Date(Date.UTC(2019, 5, 16, 10, 0, 0)));
    assert.strictEqual(r2019.cashOuts[1]?.valeurPortefeuille212, 59299, 'cession2[212]: 59 299 €');
    assert.strictEqual(r2019.cashOuts[1]?.prix213, 224, 'cession2[213]: 224 €');
    assert.strictEqual(r2019.cashOuts[1]?.frais214, 0, 'cession2[214]: 0 €');
    assert.strictEqual(r2019.cashOuts[1]?.prixNetDeFrais215, 224, 'cession2[215]: 224 €');
    assert.strictEqual(r2019.cashOuts[1]?.soulteRecueOuVersee216, 0, 'cession2[216]: 0 €');
    assert.strictEqual(r2019.cashOuts[1]?.prixNetDeSoulte217, 224, 'cession2[217]: 224 €');
    assert.strictEqual(r2019.cashOuts[1]?.prixNet218, 224, 'cession2[218]: 224 €');
    assert.strictEqual(r2019.cashOuts[1]?.prixTotalAcquisition220, 34856, 'cession2[220]: 34 856 €');
    assert.strictEqual(r2019.cashOuts[1]?.fractionDeCapital221, 4559, 'cession2[221]: 4 559 €');
    assert.strictEqual(r2019.cashOuts[1]?.soulteRecueEnCasDechangeAnterieur222, 0, 'cession2[222]: 0 €');
    assert.strictEqual(r2019.cashOuts[1]?.prixTotalAcquisitionNet223, 30297, 'cession2[223]: 30 297 €');
    assert.strictEqual(r2019.cashOuts[1]?.plusMoinsValue, 110, 'cession2[224]: 110 €');

    assert.deepEqual(r2019.cashOuts[2]?.date211, new Date(Date.UTC(2019, 5, 25, 10, 0, 0)));
    assert.strictEqual(r2019.cashOuts[2]?.valeurPortefeuille212, 78159, 'cession3[212]: 78 159 €');
    assert.strictEqual(r2019.cashOuts[2]?.prix213, 10463, 'cession3[213]: 10 463 €');
    assert.strictEqual(r2019.cashOuts[2]?.frais214, 0, 'cession3[214]: 0 €');
    assert.strictEqual(r2019.cashOuts[2]?.prixNetDeFrais215, 10463, 'cession3[215]: 10 463 €');
    assert.strictEqual(r2019.cashOuts[2]?.soulteRecueOuVersee216, 0, 'cession3[216]: 0 €');
    assert.strictEqual(r2019.cashOuts[2]?.prixNetDeSoulte217, 10463, 'cession3[217]: 10 463 €');
    assert.strictEqual(r2019.cashOuts[2]?.prixNet218, 10463, 'cession3[218]: 10 463 €');
    assert.strictEqual(r2019.cashOuts[2]?.prixTotalAcquisition220, 34856, 'cession3[220]: 34 856 €');
    assert.strictEqual(r2019.cashOuts[2]?.fractionDeCapital221, 4673, 'cession3[221]: 4 673 €');
    assert.strictEqual(r2019.cashOuts[2]?.soulteRecueEnCasDechangeAnterieur222, 0, 'cession3[222]: 0 €');
    assert.strictEqual(r2019.cashOuts[2]?.prixTotalAcquisitionNet223, 30183, 'cession3[223]: 30 183 €');
    assert.strictEqual(r2019.cashOuts[2]?.plusMoinsValue, 6422, 'cession3[224]: 6 422 €');

    assert.strictEqual(r2019.realizedPnl, 5717, 'total plus moins value 2019: 5 717 €');
  });

  test('it takes effective cashin as initial pta', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2017-07-17 00:00:00,Cashin,9005,EUR,5,BTC,,,Cashin 1 801 € / BTC\n` +
          `2017-12-15 00:00:00,Cashin,16346,EUR,1,BTC,,,Cashin 16 346 € / BTC`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    const results = await wasm.get2086ResultsTask.perform();

    // Then
    assert.strictEqual(results?.initialPta, 25351, '2086 has a initial pta of 25351€');
  });

  test('it do not show tx with no in and no out at same time in initial pta ', async function (assert) {
    // Given
    await wasm.setFetchRates.perform(true);

    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2017-07-17 00:00:00,Cashin,9005,EUR,5,BTC,,,Cashin 5 BTC\n` +
          `2017-12-15 00:00:00,Echange,1,BTC,10,ETH,,,Echange 1 BTC -> 10 ETH\n` +
          `2017-12-20 00:00:00,Echange,1,BTC,15,ETH,,,Echange 1 BTC -> 15 ETH\n` +
          `2018-03-15 00:00:00,Cashin,10005,EUR,2,BTC,,,Cashin 2 BTC\n` +
          `2018-04-15 00:00:00,Echange,2,BTC,20,ETH,,,Echange 2 BTC -> 20 ETH\n` +
          `2019-02-01 00:13:37,Cashout,10,ETH,2000,EUR,,,Cashout 10 ETH -> 2000 €`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);

    const results = await wasm.get2086ResultsTask.perform();

    // Then
    assert.strictEqual(results?.initialPta, 36232, '2086 has a initial pta of 36232€');
    assert.strictEqual(results?.initialPtaTxs.length, 8, '2086 initial pta has 8 txs');

    const ethTxs = results?.initialPtaTxs.filter((tx) => tx.asset === 'ETH') ?? [];

    assert.strictEqual(ethTxs.length, 3, '2086 initial pta has 3 eth txs');

    assert.true(
      (ethTxs[0]?.quantityIn ? ethTxs[0]?.quantityIn : 0) + (ethTxs[0]?.quantityOut ? ethTxs[0]?.quantityOut : 0) > 0,
      '2086 initial tx has a in or and out',
    );
    assert.true(
      (ethTxs[1]?.quantityIn ? ethTxs[1]?.quantityIn : 0) + (ethTxs[1]?.quantityOut ? ethTxs[1]?.quantityOut : 0) > 0,
      '2086 initial tx has a in or and out',
    );
    assert.true(
      (ethTxs[2]?.quantityIn ? ethTxs[2]?.quantityIn : 0) + (ethTxs[2]?.quantityOut ? ethTxs[2]?.quantityOut : 0) > 0,
      '2086 initial tx has a in or and out',
    );

    assert.strictEqual(results?.initialPtaTxs[0]?.asset, 'BTC', '2086 initial pta txs begin with BTC');
    assert.strictEqual(results?.initialPtaTxs[7]?.asset, 'ETH', '2086 initial pta txs ends with ETH');
  });

  test('it includes in 2086 computation other transactions at the same time of a cashout', async function (assert) {
    // Given
    await wasm.setFetchRates.perform(true);

    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2019-07-19 12:00:00,Cashin,25000,EUR,5,BTC,,,Cashin 5 000 € / BTC\n` +
          `2019-07-19 12:00:00,Cashin,5000,EUR,1,BTC,,,Cashin 5 000 € / BTC\n` +
          `2019-07-19 12:00:00,Cashout,1,BTC,3744,EUR,,,Cashout 5 000 € / BTC\n` +
          `2019-07-19 12:00:00,Cashin,12500,EUR,2.5,BTC,,,Cashin 5 000 € / BTC\n` +
          `2019-07-19 12:00:00,Cashin,300,EUR,100,GRIN,,,Cashin 3 € / GRIN\n` +
          `2019-07-19 12:00:00,Cashout,50,GRIN,150,EUR,,,Cashout 3 € / GRIN\n` +
          `2019-07-19 12:00:00,Cashout,1.5,BTC,7500,EUR,,,Cashout 5 000 € / BTC`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);

    const results = await wasm.get2086ResultsTask.perform();

    // Then
    // Then
    const r2019 = results?.years[2019] as Cerfa2086YearResultData;

    assert.strictEqual(r2019.cashOuts.length, 3, '2019 has three cashout');
    assert.deepEqual(r2019.cashOuts[0]?.date211, new Date(Date.UTC(2019, 6, 19, 10, 0, 0)));
    // 6 Bitcoins at 9446.34€ / btc
    assert.strictEqual(r2019.cashOuts[0]?.valeurPortefeuille212, 56678, 'cession1[212]: 56 678 €');
    assert.strictEqual(r2019.cashOuts[0]?.prix213, 3744, 'cession1[213]: 3 744 €');
    assert.strictEqual(r2019.cashOuts[0]?.frais214, 0, 'cession1[214]: 0 €');
    assert.strictEqual(r2019.cashOuts[0]?.prixNetDeFrais215, 3744, 'cession1[215]: 3 744 €');
    assert.strictEqual(r2019.cashOuts[0]?.soulteRecueOuVersee216, 0, 'cession1[216]: 0 €');
    assert.strictEqual(r2019.cashOuts[0]?.prixNetDeSoulte217, 3744, 'cession1[217]: 3 744 €');
    assert.strictEqual(r2019.cashOuts[0]?.prixNet218, 3744, 'cession1[218]: 3 744 €');
    assert.strictEqual(r2019.cashOuts[0]?.prixTotalAcquisition220, 30000, 'cession1[220]: 30 000 €');
    assert.strictEqual(r2019.cashOuts[0]?.fractionDeCapital221, 0, 'cession1[221]: 0 €');
    assert.strictEqual(r2019.cashOuts[0]?.soulteRecueEnCasDechangeAnterieur222, 0, 'cession1[222]: 0 €');
    assert.strictEqual(r2019.cashOuts[0]?.prixTotalAcquisitionNet223, 30000, 'cession1[223]: 30 000 €');
    assert.strictEqual(r2019.cashOuts[0]?.plusMoinsValue, 1762, 'cession1[224]: 1 762 €');

    assert.deepEqual(r2019.cashOuts[0]?.date211, new Date(Date.UTC(2019, 6, 19, 10, 0, 0)));
    // 7.5 Bitcoins at 9446.34€ / btc + 100 GRIN at 2.94€ / grin
    assert.strictEqual(r2019.cashOuts[1]?.valeurPortefeuille212, 71142, 'cession2[212]: 71 142 €');
    assert.strictEqual(r2019.cashOuts[1]?.prix213, 150, 'cession2[213]: 150 €');
    assert.strictEqual(r2019.cashOuts[1]?.frais214, 0, 'cession2[214]: 0 €');
    assert.strictEqual(r2019.cashOuts[1]?.prixNetDeFrais215, 150, 'cession2[215]: 150 €');
    assert.strictEqual(r2019.cashOuts[1]?.soulteRecueOuVersee216, 0, 'cession2[216]: 0 €');
    assert.strictEqual(r2019.cashOuts[1]?.prixNetDeSoulte217, 150, 'cession2[217]: 150 €');
    assert.strictEqual(r2019.cashOuts[1]?.prixNet218, 150, 'cession2[218]: 150 €');
    assert.strictEqual(r2019.cashOuts[1]?.prixTotalAcquisition220, 42800, 'cession2[220]: 42 800 €');
    assert.strictEqual(r2019.cashOuts[1]?.fractionDeCapital221, 1982, 'cession2[221]: 1 982 €');
    assert.strictEqual(r2019.cashOuts[1]?.soulteRecueEnCasDechangeAnterieur222, 0, 'cession2[222]: 0 €');
    assert.strictEqual(r2019.cashOuts[1]?.prixTotalAcquisitionNet223, 40818, 'cession2[223]: 40 818 €');
    assert.strictEqual(r2019.cashOuts[1]?.plusMoinsValue, 64, 'cession2[224]: 64 €');

    // 7.5 Bitcoins at 9446.34€ / btc + 50 GRIN at 2.94€ / grin
    assert.deepEqual(r2019.cashOuts[0]?.date211, new Date(Date.UTC(2019, 6, 19, 10, 0, 0)));
    assert.strictEqual(r2019.cashOuts[2]?.valeurPortefeuille212, 70995, 'cession3[212]: 70 995 €');
    assert.strictEqual(r2019.cashOuts[2]?.prix213, 7500, 'cession3[213]: 7 500 €');
    assert.strictEqual(r2019.cashOuts[2]?.frais214, 0, 'cession3[214]: 0 €');
    assert.strictEqual(r2019.cashOuts[2]?.prixNetDeFrais215, 7500, 'cession3[215]: 7 500 €');
    assert.strictEqual(r2019.cashOuts[2]?.soulteRecueOuVersee216, 0, 'cession3[216]: 0 €');
    assert.strictEqual(r2019.cashOuts[2]?.prixNetDeSoulte217, 7500, 'cession3[217]: 7 500 €');
    assert.strictEqual(r2019.cashOuts[2]?.prixNet218, 7500, 'cession3[218]: 7 500 €');
    assert.strictEqual(r2019.cashOuts[2]?.prixTotalAcquisition220, 42800, 'cession3[220]: 42 800 €');
    assert.strictEqual(r2019.cashOuts[2]?.fractionDeCapital221, 2068, 'cession3[221]: 2 068 €');
    assert.strictEqual(r2019.cashOuts[2]?.soulteRecueEnCasDechangeAnterieur222, 0, 'cession3[222]: 0 €');
    assert.strictEqual(r2019.cashOuts[2]?.prixTotalAcquisitionNet223, 40732, 'cession3[223]: 40 732 €');
    assert.strictEqual(r2019.cashOuts[2]?.plusMoinsValue, 3197, 'cession3[224]: 3 197 €');

    assert.strictEqual(r2019.realizedPnl, 5023, 'total plus moins value 2019: 5 023 €');
  });

  test('it show all txs even after all quantity to found has been found in initial pta', async function (assert) {
    // Given
    await wasm.setFetchRates.perform(true);

    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2017-07-17 00:00:00,Cashin,9005,EUR,5,BTC,,,Cashin 5 BTC\n` +
          `2017-12-15 00:00:00,Echange,5,BTC,10,ETH,,,Echange 5 BTC -> 10 ETH\n` +
          `2018-07-17 00:00:00,Echange,10,ETH,5,BTC,,,Echange 10 ETH -> 5 BTC\n` +
          `2018-12-15 00:00:00,Echange,5,BTC,10,ETH,,,Echange 5 BTC -> 10 ETH\n`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);

    const results = await wasm.get2086ResultsTask.perform();

    // Then
    assert.strictEqual(results?.initialPta, 757, '2086 has a initial pta of 757€');
    assert.strictEqual(results?.initialPtaTxs.length, 7, '2086 initial pta has 7 txs');
    assert.strictEqual(results?.initialPtaTxs[0]?.asset, 'BTC', '2086 initial pta txs begin with BTC');
    assert.strictEqual(results?.initialPtaTxs[6]?.asset, 'ETH', '2086 initial pta txs ends with ETH');
  });

  test('it should offer cahsin choice if needed', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2021-07-01 00:00:00,Depot,,,1,BTC,,,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    const tx = wasm.transactions[0];

    if (tx) {
      await wasm.setCategoryTask.perform(tx.id, TxCategory.INTERESTS);
    }

    // When
    await wasm.getReportsTask.perform();

    // Then
    assert.ok(wasm.reports?.cerfa2086.cashin.filter((conf) => conf.year === 2021));
  });

  test('it takes received crypto countervalue of crypto exchanges as initial pta', async function (assert) {
    // Given
    await wasm.setFetchRates.perform(true);

    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2017-07-17 00:00:00,Cashin,9005,EUR,5,BTC,,,Cashin 1 801 € / BTC\n` +
          `2017-12-15 00:00:00,Echange,5,BTC,40,ETH,,,Echange 5 BTC -> 40 ETH`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);

    const results = await wasm.get2086ResultsTask.perform();

    // Then
    assert.strictEqual(results?.initialPta, 23935, '2086 has a initial pta of 23935€');
  });

  test('it generates correct pta on 2086 report', async function (assert) {
    // Given
    await wasm.setFetchRates.perform(true);

    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2021-02-08 08:00:00,Cashin,100,EUR,1,ETH,,,Cashin 100€\n` +
          `2021-03-08 08:00:00,Interet,,,0.1,ETH,,,Intérets\n` +
          `2021-04-08 08:00:00,Interet,,,0.1,ETH,,,Intérets\n` +
          `2021-05-08 08:00:00,Interet,,,0.1,ETH,,,Intérets\n` +
          `2021-06-08 08:00:00,Interet,,,0.1,ETH,,,Intérets\n` +
          `2021-07-18 08:30:10,Cashout,0.5,ETH,1000,EUR,,,Cashout 1000\n` +
          `2021-08-08 08:00:00,Interet,,,0.1,ETH,,,Intérets\n` +
          `2021-09-08 08:00:00,Interet,,,0.1,ETH,,,Intérets\n` +
          `2021-10-08 08:00:00,Interet,,,0.1,ETH,,,Intérets\n` +
          `2021-11-08 08:00:00,Interet,,,0.1,ETH,,,Intérets\n` +
          `2021-12-08 08:30:10,Cashout,0.5,ETH,1000,EUR,,,Cashout 1000€`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.saveChoiceBNCTask.perform(2021, true);

    const results = await wasm.get2086ResultsTask.perform();

    // Then
    assert.strictEqual(results?.years[2021]?.cashOuts[0]?.prixTotalAcquisition220, 911, 'cashout1 have a pta of 911');
    assert.strictEqual(results?.years[2021]?.cashOuts[1]?.prixTotalAcquisition220, 2181, 'cashout2 have a pta of 2181');
  });

  test('it should keep original order of same txs timestamps on 2086 report', async function (assert) {
    // Given
    await wasm.updateDefinitionCsvDef('StackinSat');

    const csv = new File(
      [
        `bitcoinSavingPlanName,bitcoinSavingPlanIcon,purchaseOrderType,purchaseOrderFrequency,amountEur,transactionType,sendingDate,deliveryDate,transactionHash,marginEur,amountBtc,miningFeesBtc,price,withdrawOrderId\n` +
          `"Satoshis billionaire",beach_access,PUNCTUAL,,1337.0,DELIVERY_PERSONAL_VAULT,"2022-01-03T19:21:38.127716Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,4.801041666666666E-7,41549.99996366998,\n` +
          `"Satoshis billionaire",beach_access,PUNCTUAL,,1337.0,DELIVERY_PERSONAL_VAULT,"2022-01-04T19:05:45.363277Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,6.737908496732027E-7,41199.99995517422,\n` +
          `"Satoshis billionaire",beach_access,STACKINSAT_GIFT,,1337.0,DELIVERY_PERSONAL_VAULT,"2022-01-10T18:25:47.865101Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,9.905263157894738E-7,37034.999124981696,\n` +
          `"Satoshis billionaire",beach_access,RECURRING,WEEKLY,1337.0,DELIVERY_PERSONAL_VAULT,"2022-01-11T19:41:38.865439Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,4.818E-7,37025.0,\n` +
          `"Satoshis billionaire",beach_access,PUNCTUAL,,1337.0,DELIVERY_PERSONAL_VAULT,"2022-01-11T19:41:38.865439Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,4.818E-7,36965.0,\n` +
          `"Satoshis billionaire",beach_access,RECURRING,WEEKLY,1337.0,DELIVERY_PERSONAL_VAULT,"2022-01-18T18:01:08.021293Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,4.830534351145039E-7,36639.99989674547,\n` +
          `"Satoshis billionaire",beach_access,PUNCTUAL,,1337.0,DELIVERY_PERSONAL_VAULT,"2022-01-19T18:00:40.265683Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,6.137942122186495E-7,37230.00006735757,\n` +
          `"Satoshis billionaire",beach_access,RECURRING,WEEKLY,1337.0,DELIVERY_PERSONAL_VAULT,"2022-01-25T19:31:42.970663Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,6.451877133105802E-7,32285.0,\n` +
          `"Satoshis billionaire",beach_access,RECURRING,WEEKLY,1337.0,DELIVERY_PERSONAL_VAULT,"2022-02-01T19:37:47.280869Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,1.668682634730539E-6,34004.999944187875,\n` +
          `"Satoshis billionaire",beach_access,RECURRING,WEEKLY,1337.0,DELIVERY_PERSONAL_VAULT,"2022-02-08T18:06:45.403961Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,1.652852233676976E-6,38675.00002071558,\n` +
          `"Satoshis billionaire",beach_access,RECURRING,WEEKLY,1337.0,DELIVERY_PERSONAL_VAULT,"2022-02-14T18:47:42.419319Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,6.899214659685863E-7,37755.0,\n` +
          `"Satoshis billionaire",beach_access,RECURRING,WEEKLY,1337.0,DELIVERY_PERSONAL_VAULT,"2022-02-22T17:56:47.969569Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,1.0026666666666668E-6,32810.00004832754,\n` +
          `"Satoshis billionaire",beach_access,RECURRING,WEEKLY,1337.0,DELIVERY_PERSONAL_VAULT,"2022-03-02T10:51:54.796623Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,7.040583554376657E-7,38785.0,\n` +
          `"Satoshis billionaire",beach_access,PUNCTUAL,,1337.0,DELIVERY_PERSONAL_VAULT,"2022-03-04T17:30:58.538489Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,1.6493548387096776E-6,37939.99996588895,\n` +
          `"Satoshis billionaire",beach_access,RECURRING,WEEKLY,1337.0,DELIVERY_PERSONAL_VAULT,"2022-03-08T18:33:08.026798Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,8.015306122448979E-7,35620.0,\n` +
          `"Satoshis billionaire",beach_access,RECURRING,WEEKLY,1337.0,DELIVERY_PERSONAL_VAULT,"2022-03-15T18:45:08.373027Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,9.977230769230769E-7,35210.0,\n` +
          `"Satoshis billionaire",beach_access,PUNCTUAL,,1337.0,DELIVERY_PERSONAL_VAULT,"2022-03-21T17:29:10.277239Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,1.2006376811594203E-6,37414.99995901384,\n` +
          `"Satoshis billionaire",beach_access,RECURRING,WEEKLY,1337.0,DELIVERY_PERSONAL_VAULT,"2022-03-22T18:25:24.875062Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,1.1860915492957745E-6,38664.999957462445,\n` +
          `"Satoshis billionaire",beach_access,RECURRING,WEEKLY,1337.0,DELIVERY_PERSONAL_VAULT,"2022-03-29T16:20:55.898901Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,6.70056925996205E-7,43304.99992811136,\n` +
          `"Satoshis billionaire",beach_access,PUNCTUAL,,1337.0,DELIVERY_PERSONAL_VAULT,"2022-03-29T16:20:55.898901Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,6.70056925996205E-7,43244.99996415521,\n` +
          `"Satoshis billionaire",beach_access,PUNCTUAL,,1337.0,DELIVERY_PERSONAL_VAULT,"2022-04-04T15:23:16.036086Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,6.721757322175733E-7,41870.0,\n` +
          `"Satoshis billionaire",beach_access,RECURRING,WEEKLY,1337.0,DELIVERY_PERSONAL_VAULT,"2022-04-05T16:17:14.519461Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,6.784285714285714E-7,42554.99993728387,\n` +
          `"Satoshis billionaire",beach_access,RECURRING,WEEKLY,1337.0,DELIVERY_PERSONAL_VAULT,"2022-04-12T17:17:34.317559Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,7.200564971751413E-7,37049.999874660505,\n` +
          `"Satoshis billionaire",beach_access,RECURRING,WEEKLY,1337.0,DELIVERY_PERSONAL_VAULT,"2022-04-20T16:26:10.083748Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,5.166058394160584E-7,38290.000043238935,\n` +
          `"Satoshis billionaire",beach_access,RECURRING,WEEKLY,1337.0,DELIVERY_PERSONAL_VAULT,"2022-05-17T17:12:35.385772Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,7.436065573770492E-7,29225.000033943394,\n` +
          `"Satoshis billionaire",beach_access,RECURRING,WEEKLY,1337.0,DELIVERY_PERSONAL_VAULT,"2022-05-03T17:06:15.125721Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,4.818881118881119E-7,36710.0,\n` +
          `"Satoshis billionaire",beach_access,PUNCTUAL,,1337.0,DELIVERY_PERSONAL_VAULT,"2022-05-06T16:34:04.347230Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,1.6003865979381444E-6,34590.0,\n` +
          `"Satoshis billionaire",beach_access,RECURRING,WEEKLY,1337.0,DELIVERY_PERSONAL_VAULT,"2022-05-10T16:54:23.639230Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,4.806818181818182E-7,30485.0,\n` +
          `"Satoshis billionaire",beach_access,PUNCTUAL,,1337.0,DELIVERY_PERSONAL_VAULT,"2022-05-11T20:00:39.527952Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,5.466785714285714E-6,30100.0,\n` +
          `"Satoshis billionaire",beach_access,RECURRING,WEEKLY,1337.0,DELIVERY_PERSONAL_VAULT,"2022-04-26T16:50:54.347686Z",,"0000000000000000000000000000000000000000000000000000000000000000",1.337,0.0010000,6.389878542510121E-7,37945.0,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    await wasm.useFileTask.perform(csv);
    await wasm.saveChoiceBNCTask.perform(2022, true);

    // When
    await wasm.getTransactionsTask.perform();
    await wasm.get2086ResultsTask.perform();

    // Then
    assert.ok((wasm.transactions ?? []).length > 0, 'there is txs');
    assert.strictEqual(wasm.errorMessage2086, '', '2086 is generated without negative balances');
  });

  test('it should report pta over the years', async function (assert) {
    // Given
    await wasm.setFetchRates.perform(true);

    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2019-02-08 08:00:00,Cashin,100,EUR,1,ETH,,,Cashin 100€\n` +
          `2019-03-08 08:00:00,Interet,,,0.1,ETH,,,Intérets\n` +
          `2019-07-18 08:30:10,Cashout,0.5,ETH,1000,EUR,,,Cashout 1000\n` +
          `2020-08-30 08:30:10,Cashout,0.5,ETH,1000,EUR,,,Cashout 1000€`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.saveChoiceBNCTask.perform(2019, true);

    const results = await wasm.get2086ResultsTask.perform();

    // Then
    assert.strictEqual(results?.years[2019]?.cashOuts.length, 1, '2019 has one cashout');
    assert.strictEqual(results?.years[2019]?.cashOuts[0]?.prixTotalAcquisition220, 112, '2019 has a pta of 112€');
    assert.strictEqual(results?.years[2020]?.cashOuts.length, 1, '2020 has one cashout');
    assert.strictEqual(results?.years[2020]?.cashOuts[0]?.prixTotalAcquisition220, 112, '2020 has a pta of 112€');
  });

  test('it should report capital fraction over the years', async function (assert) {
    // Given
    await wasm.setFetchRates.perform(true);

    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2019-02-08 08:00:00,Cashin,100,EUR,1,ETH,,,Cashin 100€\n` +
          `2019-03-08 08:00:00,Interet,,,0.1,ETH,,,Intérets\n` +
          `2019-07-18 08:30:10,Cashout,0.5,ETH,1000,EUR,,,Cashout 1000\n` +
          `2020-08-30 08:30:10,Cashout,0.5,ETH,1000,EUR,,,Cashout 1000€`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.saveChoiceBNCTask.perform(2019, true);

    const results = await wasm.get2086ResultsTask.perform();

    // Then
    assert.strictEqual(results?.years[2019]?.cashOuts.length, 1, '2019 has one cashout');
    assert.strictEqual(results?.years[2019]?.cashOuts[0]?.fractionDeCapital221, 0, '2019 has a capital fraction of 0€');
    assert.strictEqual(results?.years[2020]?.cashOuts.length, 1, '2020 has one cashout');
    assert.strictEqual(
      results?.years[2020]?.cashOuts[0]?.fractionDeCapital221,
      543,
      '2020 has a capital fraction of 543€',
    );
  });
});
