import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | monero', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Monero_Wallet', 'Monero');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import deposit', async function (assert) {
    // Given
    const csv = new File(
      [
        `blockHeight,epoch,date,direction,amount,atomicAmount,fee,txid,label,subaddrAccount,paymentId,description\n` +
          `7306620,1651853327,2022-05-06 18:08:47,in,4.020043531049,06824547000,8.202171529291,1c51336f978d88b34454cf6f76be7df489bfb63bfd477b4916386d6726152f9d,Primary account,0,,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DEPOSITS, 'tx is categorized as deposits');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 4, 6, 16, 8, 47)));
    assert.strictEqual(wasm.transactions[0]?.note, '7306620 Primary account 0', 'transaction have a note');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.006824547, 'tx have a to of quantity 0.006824547');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'XMR', 'transaction have a to of currency XMR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction has no fee');
  });

  test('it should import withdrawals', async function (assert) {
    // Given
    const csv = new File(
      [
        `blockHeight,epoch,date,direction,amount,atomicAmount,fee,txid,label,subaddrAccount,paymentId,description\n` +
          `7306620,1651853327,2022-05-06 18:08:47,out,4.020043531049,06824547000,0.202171520000,1c51336f978d88b34454cf6f76be7df489bfb63bfd477b4916386d6726152f9d,Primary account,0,,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.WITHDRAWALS, 'tx is categorized as withdrawals');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 4, 6, 16, 8, 47)));
    assert.strictEqual(wasm.transactions[0]?.note, '7306620 Primary account 0', 'transaction have a note');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 0.006824547, 'tx have a from of quantity 0.006824547');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'XMR', 'transaction have a from of currency XMR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx have a to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'transaction has one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.20217152, 'tx have a fee of quantity 0.20217152');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'XMR', 'transaction have a fee of currency XMR');
  });
});
