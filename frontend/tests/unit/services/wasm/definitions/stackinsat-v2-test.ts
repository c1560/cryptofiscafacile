import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | stackinsat-v2', function (hooks) {
  setupWasmDefinitionTest(hooks, 'StackinSat_v2', 'StackinSat');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import cash in to self custody', async function (assert) {
    // Given
    const csv = new File(
      [
        `bitcoinSavingPlanName,cardPaymentAmountEur,cardPaymentFees,cardPaymentFeesAmountEur,purchaseOrderType,"purchaseOrderStackinsatGiftType",purchaseOrderFrequency,tcbType,amountEur,stackinsatBaseFees,"stackinsatBaseFeesAmountEur",discountedFees,discountedFeesAmountEur,transactionType,sendingDate,deliveryDate,transactionHash,marginEur,amountBtc,miningFeesBtc,price,withdrawOrderId,"withdrawalStackinsatFeesBtc"\n` +
          `"Satoshis billionaire",,,,PUNCTUAL,,,BANK_TRANSFER,500.0,1.0,5.0,0.0,0.0,DELIVERY_PERSONAL_VAULT,"2021-01-12T09:08:18.000000Z",2021-01-12T12:00:00Z,"000000000000000000000000000000000000000",5.0,0.01191287,4.8010416E-7,41549.99996366998,,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 12, 12, 0, 0)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_IN, 'tx1 is categorized as cash in');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx1 has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 500, 'tx1 have a to of quantity 500');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'EUR', 'tx1 has a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx1 has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.01191335010416, 'tx1 has a to of qty 0.01191335010416');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx1 has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx1 has no fee');

    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 0, 12, 12, 0, 0)));
    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.WITHDRAWALS, 'tx2 is a withdrawal');
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx2 has one from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 0.01191287, 'tx2 have a from of quantity 0.01191287');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'BTC', 'tx2 has a from of currency BTC');
    assert.strictEqual(wasm.transactions[1]?.to.length, 0, 'tx2 has no to');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 1, 'tx2 has one fee');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.quantity, 4.8010416e-7, 'tx2 has a from of qty 4.8010416e-7');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.currency, 'BTC', 'tx2 has a from of currency BTC');
  });

  test('it should import cash in to custody', async function (assert) {
    // Given
    const csv = new File(
      [
        `bitcoinSavingPlanName,cardPaymentAmountEur,cardPaymentFees,cardPaymentFeesAmountEur,purchaseOrderType,"purchaseOrderStackinsatGiftType",purchaseOrderFrequency,tcbType,amountEur,stackinsatBaseFees,"stackinsatBaseFeesAmountEur",discountedFees,discountedFeesAmountEur,transactionType,sendingDate,deliveryDate,transactionHash,marginEur,amountBtc,miningFeesBtc,price,withdrawOrderId,"withdrawalStackinsatFeesBtc"\n` +
          `"PEB Custody",,,,RECURRING,,MONTHLY,BANK_TRANSFER,40.0,2.0,0.8,0.25,0.1,"DELIVERY_STACKINSAT_VAULT","2021-01-12T09:08:18.000000Z",2021-01-12T12:00:00Z,"6f27e8e5070f1e9e50d51b4ff19a7000ed13b1fcad37b60824277ba70b23a8c9",0.7000000000000001,0.00135167,0.0,29075.0,,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 12, 12, 0, 0)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_IN, 'tx1 is categorized as cash in');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx1 has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 40, 'tx1 have a to of quantity 500');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'EUR', 'tx1 has a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx1 has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.00135167, 'tx1 has a to of qty 0.00135167');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx1 has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx1 has no fee');
  });

  test('it should import referral transactions', async function (assert) {
    // Given
    const csv = new File(
      [
        `bitcoinSavingPlanName,cardPaymentAmountEur,cardPaymentFees,cardPaymentFeesAmountEur,purchaseOrderType,"purchaseOrderStackinsatGiftType",purchaseOrderFrequency,tcbType,amountEur,stackinsatBaseFees,"stackinsatBaseFeesAmountEur",discountedFees,discountedFeesAmountEur,transactionType,sendingDate,deliveryDate,transactionHash,marginEur,amountBtc,miningFeesBtc,price,withdrawOrderId,"withdrawalStackinsatFeesBtc"\n` +
          `"Satoshis billionaire",,,,STACKINSAT_GIFT,SPONSORSHIP,,STACKINSAT_GIFT,15.0,0.0,0.0,0.0,0.0,DELIVERY_PERSONAL_VAULT,"2021-01-12T09:08:18.000000Z",2021-01-12T12:00:00Z,"0000000000000000000000000000000000000000000000000000000000000000",0.0,4.0403E-4,9.905E-7,37034.999124981696,,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 12, 12, 0, 0)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.REFERRALS, 'tx1 is categorized as referrals');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx1 has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx1 has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.0004050205, 'tx1 has a to of qty 0.0004050205');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx1 has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx1 has no fee');

    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 0, 12, 12, 0, 0)));
    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.WITHDRAWALS, 'tx2 is a withdrawal');
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx2 has one from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 0.00040403, 'tx2 have a from of quantity 0.00040403');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'BTC', 'tx2 has a from of currency BTC');
    assert.strictEqual(wasm.transactions[1]?.to.length, 0, 'tx2 has no to');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 1, 'tx2 has one fee');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.quantity, 0.0000009905, 'tx2 has a fee of qty 0.0000009905');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.currency, 'BTC', 'tx2 has a from of currency BTC');
  });

  test('it should import withdrawal', async function (assert) {
    // Given
    const csv = new File(
      [
        `bitcoinSavingPlanName,cardPaymentAmountEur,cardPaymentFees,cardPaymentFeesAmountEur,purchaseOrderType,"purchaseOrderStackinsatGiftType",purchaseOrderFrequency,tcbType,amountEur,stackinsatBaseFees,"stackinsatBaseFeesAmountEur",discountedFees,discountedFeesAmountEur,transactionType,sendingDate,deliveryDate,transactionHash,marginEur,amountBtc,miningFeesBtc,price,withdrawOrderId,"withdrawalStackinsatFeesBtc"\n` +
          `"PEB Custody",,,,,,,,,,,,,WITHDRAW_PERSONAL_VAULT,"2021-01-12T09:08:18.000000Z",2021-01-12T12:00:00Z,"17fe32d274974b993dcb4c419ebcf2fcdde1a737ccc7a258343807997940d6b9",0.0,5.9E-4,0.0,,00000,1.0E-5`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 12, 12, 0, 0)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.WITHDRAWALS, 'tx is categorized as withdrawal');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 0.00059, 'tx1 has a to of quantity 0.00059');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'tx1 has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx1 has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx has one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.00001, 'tx has a fee of quantity 0.00001');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'BTC', 'tx has a to of currency BTC');
  });

  test('it should import two tx the same day', async function (assert) {
    // Given
    const csv = new File(
      [
        `bitcoinSavingPlanName,cardPaymentAmountEur,cardPaymentFees,cardPaymentFeesAmountEur,purchaseOrderType,"purchaseOrderStackinsatGiftType",purchaseOrderFrequency,tcbType,amountEur,stackinsatBaseFees,"stackinsatBaseFeesAmountEur",discountedFees,discountedFeesAmountEur,transactionType,sendingDate,deliveryDate,transactionHash,marginEur,amountBtc,miningFeesBtc,price,withdrawOrderId,"withdrawalStackinsatFeesBtc"\n` +
          `"Satoshis billionaire",,,,PUNCTUAL,,,BANK_TRANSFER,500.0,1.0,5.0,0.0,0.0,DELIVERY_PERSONAL_VAULT,"2021-01-12T09:08:18.000000Z",2021-01-12T12:00:00Z,"000000000000000000000000000000000000000",5.0,0.01191287,4.8010416E-7,41549.99996366998,,\n` +
          `"Satoshis billionaire",,,,PUNCTUAL,,,BANK_TRANSFER,500.10,1.0,5.0,0.0,0.0,DELIVERY_PERSONAL_VAULT,"2021-01-12T09:08:18.000000Z",2021-01-12T12:00:00Z,"000000000000000000000000000000000000000",5.0,0.01191287,4.8010416E-7,41549.99996366998,,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 4, 'has four txs');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_IN, 'tx1 is categorized as exchange');
    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.WITHDRAWALS, 'tx2 is a withdrawal');
    assert.strictEqual(wasm.transactions[2]?.category, TxCategory.CASH_IN, 'tx3 is categorized as exchange');
    assert.strictEqual(wasm.transactions[3]?.category, TxCategory.WITHDRAWALS, 'tx4 is a withdrawal');
  });

  test('it should not put same id for cahsin and withdrawal txs', async function (assert) {
    // Given
    const csv = new File(
      [
        `bitcoinSavingPlanName,cardPaymentAmountEur,cardPaymentFees,cardPaymentFeesAmountEur,purchaseOrderType,"purchaseOrderStackinsatGiftType",purchaseOrderFrequency,tcbType,amountEur,stackinsatBaseFees,"stackinsatBaseFeesAmountEur",discountedFees,discountedFeesAmountEur,transactionType,sendingDate,deliveryDate,transactionHash,marginEur,amountBtc,miningFeesBtc,price,withdrawOrderId,"withdrawalStackinsatFeesBtc"\n` +
          `"Satoshis billionaire",,,,PUNCTUAL,,,BANK_TRANSFER,500.0,1.0,5.0,0.0,0.0,DELIVERY_PERSONAL_VAULT,"2021-01-12T09:08:18.000000Z",2021-01-12T12:00:00Z,"000000000000000000000000000000000000000",5.0,0.01191287,4.8010416E-7,41549.99996366998,,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two txs');
    assert.notStrictEqual(wasm.transactions[0]?.id, wasm.transactions[1]?.id, 'tx1 id != tx2 id');
  });
});
