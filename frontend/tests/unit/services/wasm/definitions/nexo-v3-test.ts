import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | nexo-v3', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Nexo_v3', 'Nexo');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import interest transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Transaction,Type,Input Currency,Input Amount,Output Currency,Output Amount,USD Equivalent,Details,Date / Time\n` +
          `TXID000000000,Interest,NEXO,7.91850604,NEXO,1.29841576,$0.08,approved / NEXO Interest Earned,2022-01-13 01:02:03`,
      ],
      'uphold.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.INTERESTS, 'transaction is categorized as interests');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 0, 13, 1, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.note, 'approved / NEXO Interest Earned', 'transaction have a note');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction do not have a from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction have a to');
    assert.strictEqual(
      wasm.transactions[0]?.to[0]?.quantity,
      1.29841576,
      'transaction have a to of quantity 1.29841576',
    );
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'NEXO', 'transaction have a to of currency NEXO');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction do not have a fee');
  });

  test('it should ignore self locking term deposit', async function (assert) {
    // Given
    const csv = new File(
      [
        `Transaction,Type,Input Currency,Input Amount,Output Currency,Output Amount,USD Equivalent,Details,Date / Time\n` +
          `TXID000000000,LockingTermDeposit,ETH,-2.00000000,ETH,2.1,$5862.086048,"approved / Transfer from Savings Wallet to Term Wallet",2022-01-13 01:02:03`,
      ],
      'uphold.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'transaction is categorized as ignored');
    assert.strictEqual(
      wasm.transactions[0]?.note,
      'approved / Transfer from Savings Wallet to Term Wallet',
      'transaction have a note',
    );
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'transaction have a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 2, 'transaction have a from of quantity 2');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'ETH', 'transaction have a from of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 2.1, 'transaction have a to of quantity 2.1');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'transaction have a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction have no fee');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'transaction is ignored');
  });

  test('it should ignore self unlocking term deposit', async function (assert) {
    // Given
    const csv = new File(
      [
        `Transaction,Type,Input Currency,Input Amount,Output Currency,Output Amount,USD Equivalent,Details,Date / Time\n` +
          `TXID000000000,UnlockingTermDeposit,ETH,2.00000000,ETH,2.1,$5862.086048,"approved / Transfer from Term Wallet to Savings Wallet",2022-01-13 01:02:03`,
      ],
      'uphold.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'transaction is categorized as ignored');
    assert.strictEqual(
      wasm.transactions[0]?.note,
      'approved / Transfer from Term Wallet to Savings Wallet',
      'transaction have a note',
    );
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'transaction have a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 2, 'transaction have a from of quantity 2');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'ETH', 'transaction have a from of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 2.1, 'transaction have a to of quantity 2.1');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'transaction have a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction have no fee');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'transaction is ignored');
  });

  test('it should import exchange', async function (assert) {
    // Given
    const csv = new File(
      [
        `Transaction,Type,Input Currency,Input Amount,Output Currency,Output Amount,USD Equivalent,Details,Date / Time\n` +
          `TXID000000000,Exchange,BTC,-5.19341718,NEXO,34.89780924,$91.54,"approved / Exchange Bitcoin to NEXO Token",2022-01-13 01:02:03`,
      ],
      'uphold.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'transaction is categorized as exchanges');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 0, 13, 1, 2, 3)));
    assert.strictEqual(
      wasm.transactions[0]?.note,
      'approved / Exchange Bitcoin to NEXO Token',
      'transaction have a note',
    );
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'transaction have a from');
    assert.strictEqual(
      wasm.transactions[0]?.from[0]?.quantity,
      5.19341718,
      'transaction have a to of quantity 5.19341718',
    );
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'transaction have a from of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction have a to');
    assert.strictEqual(
      wasm.transactions[0]?.to[0]?.quantity,
      34.89780924,
      'transaction have a to of quantity 34.89780924',
    );
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'NEXO', 'transaction have a to of currency NEXO');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction do not have a fee');
  });

  test('it should import exchange cashback', async function (assert) {
    // Given
    const csv = new File(
      [
        `Transaction,Type,Input Currency,Input Amount,Output Currency,Output Amount,USD Equivalent,Details,Date / Time\n` +
          `TXID000000000,Exchange Cashback,NEXO,8.94079362,NEXO,9.45744196,$3.20190401,"approved / 0.25% on top of your Exchange transaction",2022-01-13 01:02:03`,
      ],
      'uphold.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(
      wasm.transactions[0]?.category,
      TxCategory.COMMERCIAL_REBATES,
      'transaction is categorized as commercial rebates',
    );
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 0, 13, 1, 2, 3)));
    assert.strictEqual(
      wasm.transactions[0]?.note,
      'approved / 0.25% on top of your Exchange transaction',
      'transaction have a note',
    );
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction do not have a from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction have a to');
    assert.strictEqual(
      wasm.transactions[0]?.to[0]?.quantity,
      9.45744196,
      'transaction have a to of quantity 9.45744196',
    );
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'NEXO', 'transaction have a to of currency NEXO');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction do not have a fee');
  });

  test('it should import ExchangeDepositedOn', async function (assert) {
    // Given
    const csv = new File(
      [
        `Transaction,Type,Input Currency,Input Amount,Output Currency,Output Amount,USD Equivalent,Details,Date / Time\n` +
          `TXID000000000,ExchangeDepositedOn,EUR,367.06015580,EURX,863.90420349,$7519.473405,"approved / EUR to EURX",2022-01-13 01:02:03`,
      ],
      'uphold.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'transaction is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 0, 13, 1, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.note, 'approved / EUR to EURX', 'transaction have a note');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'transaction have a from');
    assert.strictEqual(
      wasm.transactions[0]?.from[0]?.quantity,
      367.0601558,
      'transaction have a to of quantity 367.06015580',
    );
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'EUR', 'transaction have a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction have a to');
    assert.strictEqual(
      wasm.transactions[0]?.to[0]?.quantity,
      863.90420349,
      'transaction have a to of quantity 863.90420349',
    );
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'EUR', 'transaction have a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction do not have a fee');
  });

  test('it should import DepositToExchange', async function (assert) {
    // Given
    const csv = new File(
      [
        `Transaction,Type,Input Currency,Input Amount,Output Currency,Output Amount,USD Equivalent,Details,Date / Time\n` +
          `TXID000000000,DepositToExchange,EUR,142.92453766,EURX,000.98813489,$8407.642156,"approved / EUR Top Up",2022-01-13 01:02:03`,
      ],
      'uphold.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'transaction is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 0, 13, 1, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.note, 'approved / EUR Top Up', 'transaction have a note');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction do not have a from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction have a to');
    assert.strictEqual(
      wasm.transactions[0]?.to[0]?.quantity,
      0.98813489,
      'transaction have a to of quantity 0.98813489',
    );
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'EUR', 'transaction have a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction do not have a fee');
  });

  test('it should import Dividend', async function (assert) {
    // Given
    const csv = new File(
      [
        `Transaction,Type,Input Currency,Input Amount,Output Currency,Output Amount,USD Equivalent,Details,Date / Time\n` +
          `TXID000000000,Dividend,NEXO,9.88256498,NEXO,1.63172447,$6.0265843,"approved / Loyalty Dividend",2022-01-13 01:02:03`,
      ],
      'uphold.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.INTERESTS, 'transaction is categorized as interests');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 0, 13, 1, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.note, 'approved / Loyalty Dividend', 'transaction have a note');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction do not have a from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction have a to');
    assert.strictEqual(
      wasm.transactions[0]?.to[0]?.quantity,
      1.63172447,
      'transaction have a to of quantity 1.63172447',
    );
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'NEXO', 'transaction have a to of currency NEXO');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction do not have a fee');
  });

  test('it should import Withdrawal', async function (assert) {
    // Given
    const csv = new File(
      [
        `Transaction,Type,Input Currency,Input Amount,Output Currency,Output Amount,USD Equivalent,Details,Date / Time\n` +
          `TXID000000000,Withdrawal,USDT,-6648.82928223,USDT,1552.52172094,$9366.24896441,"approved / USDT withdrawal",2022-01-13 01:02:03`,
      ],
      'uphold.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(
      wasm.transactions[0]?.category,
      TxCategory.WITHDRAWALS,
      'transaction is categorized as withdrawal',
    );
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 0, 13, 1, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.note, 'approved / USDT withdrawal', 'transaction have a note');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'transaction have a from');
    assert.strictEqual(
      wasm.transactions[0]?.from[0]?.quantity,
      6648.82928223,
      'transaction have a to of quantity 6648.82928223',
    );
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'USDT', 'transaction have a to of currency USDT');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'transaction do not have a to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction do not have a fee');
  });

  test('it should import Bonus', async function (assert) {
    // Given
    const csv = new File(
      [
        `Transaction,Type,Input Currency,Input Amount,Output Currency,Output Amount,USD Equivalent,Details,Date / Time\n` +
          `TXID000000000,Bonus,BTC,2.70448234,BTC,6.41022732,$9.5289417,"approved / Bonus interest from #X3inBTC campaign",2022-01-13 01:02:03`,
      ],
      'uphold.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.INTERESTS, 'transaction is categorized as interests');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 0, 13, 1, 2, 3)));
    assert.strictEqual(
      wasm.transactions[0]?.note,
      'approved / Bonus interest from #X3inBTC campaign',
      'transaction have a note',
    );
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction do not have a from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction have a to');
    assert.strictEqual(
      wasm.transactions[0]?.to[0]?.quantity,
      6.41022732,
      'transaction have a to of quantity 6.41022732',
    );
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'transaction have a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction do not have a fee');
  });

  test('it should import ReferralBonus', async function (assert) {
    // Given
    const csv = new File(
      [
        `Transaction,Type,Input Currency,Input Amount,Output Currency,Output Amount,USD Equivalent,Details,Date / Time\n` +
          `TXID000000000,ReferralBonus,BTC,2.83124777,BTC,1.46219603,$69.12550798,approved / Referral bonus,2022-01-13 01:02:03`,
      ],
      'uphold.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.REFERRALS, 'transaction is categorized as referrals');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 0, 13, 1, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.note, 'approved / Referral bonus', 'transaction has a note');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction do not have a from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 1.46219603, 'tx has a to of quantity 1.46219603');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'transaction has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction do not have a fee');
  });
});
