import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | ledger-live-v3', function (hooks) {
  setupWasmDefinitionTest(hooks, 'LedgerLive_v3', 'LedgerLive');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import crypto deposit', async function (assert) {
    // Given
    const csv = new File(
      [
        `Operation Date,Status,Currency Ticker,Operation Type,Operation Amount,Operation Fees,Operation Hash,Account Name,Account xpub,Countervalue Ticker,Countervalue at Operation Date,Countervalue at CSV Export\n` +
          `2021-12-31T10:00:37.000Z,Confirmed,ETH,IN,0.99223,0.000395,000000000000000000000000000000000000000000000000001,Tezos 1,0000000000000000000000000000000000000000000000000000000000000xpub1,USD,7826.12,1155.73`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DEPOSITS, 'tx is categorized as deposit');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 11, 31, 10, 0, 37)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx have no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx have one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.99223, 'tx have a from of 0.99223');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'tx have a from of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx have no fee');
  });

  test('it should import crypto withdrawal', async function (assert) {
    // Given
    const csv = new File(
      [
        `Operation Date,Status,Currency Ticker,Operation Type,Operation Amount,Operation Fees,Operation Hash,Account Name,Account xpub,Countervalue Ticker,Countervalue at Operation Date,Countervalue at CSV Export\n` +
          `2021-12-31T10:00:37.000Z,Confirmed,ETH,OUT,2,0.000735,000000000000000000000000000000000000000000000000001,Tezos 1,0000000000000000000000000000000000000000000000000000000000000xpub1,EUR,2.29,2.22`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.WITHDRAWALS, 'tx is categorized as withdrawal');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 11, 31, 10, 0, 37)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx have one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 1.999265, 'tx have a from of 1.999265');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'ETH', 'tx have a from of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx have no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx have one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.000735, 'tx have a from of 0.000735');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'ETH', 'tx have a from of currency ETH');
  });

  test('it should import delegate deposit', async function (assert) {
    // Given
    const csv = new File(
      [
        `Operation Date,Status,Currency Ticker,Operation Type,Operation Amount,Operation Fees,Operation Hash,Account Name,Account xpub,Countervalue Ticker,Countervalue at Operation Date,Countervalue at CSV Export\n` +
          `2021-12-31T10:00:37.000Z,Confirmed,XTZ,DELEGATE,1000.902971,0.03,000000000000000000000000000000000000000000000000001,Tezos 1,0000000000000000000000000000000000000000000000000000000000000xpub1,EUR,6589.23,4935.51`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.FEES, 'tx is categorized as fees');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 11, 31, 10, 0, 37)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx have no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx have no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx have one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.03, 'tx have a from of 0.03');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'XTZ', 'tx have a from of currency XTZ');
  });

  test('it should import reveal deposit', async function (assert) {
    // Given
    const csv = new File(
      [
        `Operation Date,Status,Currency Ticker,Operation Type,Operation Amount,Operation Fees,Operation Hash,Account Name,Account xpub,Countervalue Ticker,Countervalue at Operation Date,Countervalue at CSV Export\n` +
          `2021-12-31T10:00:37.000Z,Confirmed,XTZ,REVEAL,0.03,0.03,000000000000000000000000000000000000000000000000001,Tezos 1,0000000000000000000000000000000000000000000000000000000000000xpub1,EUR,6589.23,4935.51`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.FEES, 'tx is categorized as fees');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 11, 31, 10, 0, 37)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx have no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx have no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx have one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.03, 'tx have a from of 0.03');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'XTZ', 'tx have a from of currency XTZ');
  });

  test('it should import staking transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Operation Date,Status,Currency Ticker,Operation Type,Operation Amount,Operation Fees,Operation Hash,Account Name,Account xpub,Countervalue Ticker,Countervalue at Operation Date,Countervalue at CSV Export\n` +
          `2021-02-13T18:40:30.009Z,Confirmed,ETH,REWARD_PAYOUT,0.2081,0,0x0000000000000000000000000000000000000000000000000000000000000001,Ethereum,00000000000000000000000000000000000000000000000xpub1,EUR,1.36,1.36`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.MINING, 'tx is categorized as mining');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 1, 13, 18, 40, 30, 9)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx have no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx have one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.2081, 'tx have a from of 0.2081');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'tx have a from of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx have no fee');
  });

  test('it should import redelegate transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Operation Date,Status,Currency Ticker,Operation Type,Operation Amount,Operation Fees,Operation Hash,Account Name,Account xpub,Countervalue Ticker,Countervalue at Operation Date,Countervalue at CSV Export\n` +
          `2021-12-31T10:00:37.000Z,Confirmed,XTZ,UNDELEGATE,0.03,0.03,000000000000000000000000000000000000000000000000001,Tezos 1,0000000000000000000000000000000000000000000000000000000000000xpub1,USD,0.00,0.00`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.FEES, 'tx is categorized as fees');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 11, 31, 10, 0, 37)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx have no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx have no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx have one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.03, 'tx have a from of 0.03');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'XTZ', 'tx have a from of currency XTZ');
  });

  test('it should import undelegate transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Operation Date,Status,Currency Ticker,Operation Type,Operation Amount,Operation Fees,Operation Hash,Account Name,Account xpub,Countervalue Ticker,Countervalue at Operation Date,Countervalue at CSV Export\n` +
          `2021-12-31T10:00:37.000Z,Confirmed,XTZ,REDELEGATE,0.03,0.03,000000000000000000000000000000000000000000000000001,Tezos 1,0000000000000000000000000000000000000000000000000000000000000xpub1,USD,0.00,0.00`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.FEES, 'tx is categorized as fees');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 11, 31, 10, 0, 37)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx have no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx have no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx have one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.03, 'tx have a from of 0.03');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'XTZ', 'tx have a from of currency XTZ');
  });
});
