import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | coinhouse', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Coinhouse', 'Coinhouse');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import cashin', async function (assert) {
    // Given
    const csv = new File(
      [
        `ID Coinhouse,Date,Type,Quantité,Devise,Prix du marché,Contre-valeur (EUR),Frais (devise),Frais Contre-valeur (EUR),Remise frais,Solde,Compte\n` +
          `ce000000,16/03/2024 23:05:48,Echange,77.84,ada,0.6142,47.809328,,,,77.84,Portefeuille\n` +
          `ce000000,16/03/2024 23:05:48,Echange,-50.0,eur,1,-50.0,2.19,2.19,0.0,,""`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_IN, 'tx is categorized as cashin');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 50, 'tx has a from of quantity 50');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'EUR', 'tx has a from of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 77.84, 'tx has a to of quantity 77.84');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ADA', 'tx has a to of currency ADA');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import exchange', async function (assert) {
    // Given
    const csv = new File(
      [
        `ID Coinhouse,Date,Type,Quantité,Devise,Prix du marché,Contre-valeur (EUR),Frais (devise),Frais Contre-valeur (EUR),Remise frais,Solde,Compte\n` +
          `ce000000,16/03/2024 23:05:48,Echange,77.84,ada,0.6142,47.809328,,,,77.84,Portefeuille\n` +
          `ce000000,16/03/2024 23:05:48,Echange,-50.0,eth,1,-50.0,2.19,2.19,0.0,,""`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'tx is categorized as exchange');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 47.81, 'tx has a from of quantity 47.81');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'ETH', 'tx has a from of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 77.84, 'tx has a to of quantity 77.84');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ADA', 'tx has a to of currency ADA');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx has a fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 2.19, 'tx has a to of quantity 2.19');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'ETH', 'tx has a to of currency ETH');
  });

  test('it should import cashout', async function (assert) {
    // Given
    const csv = new File(
      [
        `ID Coinhouse,Date,Type,Quantité,Devise,Prix du marché,Contre-valeur (EUR),Frais (devise),Frais Contre-valeur (EUR),Remise frais,Solde,Compte\n` +
          `ce000000,16/03/2024 23:05:48,Echange,77.84,eur,0.6142,47.809328,,,,77.84,Portefeuille\n` +
          `ce000000,16/03/2024 23:05:48,Echange,-50.0,ada,1,-50.0,2.19,2.19,0.0,,""`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_OUT, 'tx is categorized as cashout');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 47.81, 'tx has a from of quantity 47.81');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'ADA', 'tx has a from of currency ADA');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 77.84, 'tx has a to of quantity 77.84');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'EUR', 'tx has a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx has a fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 2.19, 'tx has a fee of quantity 2.19');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'ADA', 'tx has a fee of currency ADA');
  });
});
