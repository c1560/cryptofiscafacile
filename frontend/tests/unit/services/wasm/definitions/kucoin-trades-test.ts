import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | kucoin-trades', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Kucoin_Trades', 'Kucoin');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import buy trades', async function (assert) {
    // Given
    const csv = new File(
      [
        `tradeCreatedAt,orderId,symbol,side,price,size,funds,fee,liquidity,feeCurrency,orderType,\n` +
          `2021-01-02 01:02:03,000000000000000000000000,BTC-ETH,buy,0.52725,5.9704,1.5052334,1.0005052334,taker,ETH,market,\n` +
          `2021-01-02 01:02:03,000000000000000000000000,BTC-ETH,buy,0.12724,1.5704,0.199817696,0.000199817696,taker,ETH,market,`,
      ],
      'kucoin-trades.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'transaction is categorized as exchange');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 2, 1, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 2, 'transaction has two from');
    assert.strictEqual(
      wasm.transactions[0]?.from[0]?.quantity,
      1.5052334,
      'transaction have a from of quantity 1.5052334',
    );
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'ETH', 'transaction has a from of currency ETH');
    assert.strictEqual(
      wasm.transactions[0]?.from[1]?.quantity,
      0.199817696,
      'transaction have a from of quantity 0.199817696',
    );
    assert.strictEqual(wasm.transactions[0]?.from[1]?.currency, 'ETH', 'transaction has a from of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.to.length, 2, 'transaction has two to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 5.9704, 'transaction have a to of quantity 5.9704');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'transaction has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to[1]?.quantity, 1.5704, 'transaction have a to of quantity 1.5704');
    assert.strictEqual(wasm.transactions[0]?.to[1]?.currency, 'BTC', 'transaction has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 2, 'transaction has two fee');
    assert.strictEqual(
      wasm.transactions[0]?.fee[0]?.quantity,
      1.0005052334,
      'transaction have a fee of quantity 1.0005052334',
    );
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'ETH', 'transaction has a fee of currency ETH');
    assert.strictEqual(
      wasm.transactions[0]?.fee[1]?.quantity,
      0.000199817696,
      'transaction have a fee of quantity 0.00019981769612345678',
    );
    assert.strictEqual(wasm.transactions[0]?.fee[1]?.currency, 'ETH', 'transaction has a fee of currency ETH');
  });

  test('it should import sell trades', async function (assert) {
    // Given
    const csv = new File(
      [
        `tradeCreatedAt,orderId,symbol,side,price,size,funds,fee,liquidity,feeCurrency,orderType,\n` +
          `2021-01-02 01:02:03,000000000000000000000000,LUNA-USDT,sell,08.7836,0.7589,584.93999552,9.71722122995,taker,USDT,market,\n`,
      ],
      'kucoin-trades.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'transaction is categorized as exchange');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 2, 1, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'transaction has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 0.7589, 'transaction have a from of quantity 0.7589');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'LUNA', 'transaction has a from of currency LUNA');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has one from');
    assert.strictEqual(
      wasm.transactions[0]?.to[0]?.quantity,
      584.93999552,
      'transaction have a to of quantity 584.93999552',
    );
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'USDT', 'transaction has a to of currency USDT');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'transaction has one fee');
    assert.strictEqual(
      wasm.transactions[0]?.fee[0]?.quantity,
      9.71722122995,
      'transaction have a fee of quantity 9.71722122995',
    );
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'USDT', 'transaction has a fee of currency USDT');
  });
});
