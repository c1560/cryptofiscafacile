import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | coinbase-pro-account', function (hooks) {
  setupWasmDefinitionTest(hooks, 'CoinbasePro_Account', 'CoinbasePro');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should ignore fiat deposit', async function (assert) {
    // Given
    const csv = new File(
      [
        `portfolio,type,time,amount,balance,amount/balance unit,transfer id,trade id,order id\n` +
          `default,deposit,2021-02-12T09:08:18.000Z,5.0000000000000000,5.0000000000000000,EUR,00000000-0000-0000-0000-000000000001,,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 1, 12, 9, 8, 18)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 5, 'tx have a to of quantity 5');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'EUR', 'tx has a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should ignore fiat withdrawal', async function (assert) {
    // Given
    const csv = new File(
      [
        `portfolio,type,time,amount,balance,amount/balance unit,transfer id,trade id,order id\n` +
          `default,withdrawal,2021-02-12T09:08:18.000Z,-5.0000000000000000,5.0000000000000000,EUR,00000000-0000-0000-0000-000000000001,,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 1, 12, 9, 8, 18)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 5, 'tx have a from of quantity 5');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'EUR', 'tx has a from of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import cashin', async function (assert) {
    // Given
    const csv = new File(
      [
        `portfolio,type,time,amount,balance,amount/balance unit,transfer id,trade id,order id\n` +
          `default,match,2021-02-12T09:08:18.000Z,-4.4820000000000000,0.5180000000000000,EUR,,0000001,00000000-0000-0000-0000-000000000001\n` +
          `default,match,2021-02-12T09:08:18.000Z,3.0000000000000000,3.0000000000000000,ALGO,,0000001,00000000-0000-0000-0000-000000000001\n` +
          `default,fee,2021-02-12T09:08:18.000Z,-0.0224100000000000,0.4955900000000000,EUR,,0000001,00000000-0000-0000-0000-000000000001\n` +
          `default,match,2021-02-12T09:08:18.000Z,-4.4820000000000000,0.5180000000000000,EUR,,0000002,00000000-0000-0000-0000-000000000002\n` +
          `default,match,2021-02-12T09:08:18.000Z,3.0000000000000000,3.0000000000000000,ALGO,,0000002,00000000-0000-0000-0000-000000000002\n` +
          `default,fee,2021-02-12T09:08:18.000Z,-0.0224100000000000,0.4955900000000000,ALGO,,0000002,00000000-0000-0000-0000-000000000002`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 1, 12, 9, 8, 18)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_IN, 'tx is categorized as cashin');
    assert.strictEqual(wasm.transactions[0]?.from.length, 2, 'tx has two from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 4.482, 'tx have a from of quantity 4.482');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'EUR', 'tx has a from of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.from[1]?.quantity, 0.02241, 'tx have a from of quantity 0.02241');
    assert.strictEqual(wasm.transactions[0]?.from[1]?.currency, 'EUR', 'tx has a from of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 3, 'tx have a to of quantity 3');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ALGO', 'tx has a to of currency ALGO');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');

    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 1, 12, 9, 8, 18)));
    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.CASH_IN, 'tx is categorized as cashin');
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 4.482, 'tx have a from of quantity 5');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'EUR', 'tx has a from of currency EUR');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 3, 'tx have a to of quantity 3');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'ALGO', 'tx has a to of currency ALGO');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 1, 'tx has one fee');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.quantity, 0.02241, 'tx have a fee of quantity 0.02241');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.currency, 'ALGO', 'tx has a fee of currency ALGO');
  });

  test('it should import cashout', async function (assert) {
    // Given
    const csv = new File(
      [
        `portfolio,type,time,amount,balance,amount/balance unit,transfer id,trade id,order id\n` +
          `default,match,2021-01-03T09:46:49.000Z,-50,450,GRT,,1732354,743732a2-e5bf-11ec-8fea-0242ac120002\n` +
          `default,match,2021-01-03T09:46:49.000Z,45,45,EUR,,1732354,743732a2-e5bf-11ec-8fea-0242ac120002\n` +
          // We cannot diff this fee tx from a cashin or a cashout. So we use special MergeProcessor CashinSingletonExchangeWithCashout to move the from item to substract with the to item
          // of the previous cashout tx
          `default,fee,2021-01-03T09:46:49.000Z,-1.55,43.45,EUR,,1732354,743732a2-e5bf-11ec-8fea-0242ac120002\n` +
          `default,match,2021-01-04T09:46:49.000Z,-50,400,GRT,,1732355,743732a2-e5bf-11ec-8fea-0242ac120002\n` +
          `default,match,2021-01-04T09:46:49.000Z,45,88.45,EUR,,1732355,743732a2-e5bf-11ec-8fea-0242ac120002\n` +
          `default,fee,2021-01-04T09:46:49.000Z,-1.55,86.9,EUR,,1732355,743732a2-e5bf-11ec-8fea-0242ac120002\n` +
          `default,match,2021-02-12T09:08:18.000Z,-3.0000000000000000,3.0000000000000000,ALGO,,0000002,00000000-0000-0000-0000-000000000002\n` +
          `default,match,2021-02-12T09:08:18.000Z,4.4820000000000000,0.5180000000000000,EUR,,0000002,00000000-0000-0000-0000-000000000002\n` +
          `default,fee,2021-02-12T09:08:18.000Z,-0.0224100000000000,0.4955900000000000,ALGO,,0000002,00000000-0000-0000-0000-000000000002`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 3, 'has three tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 3, 9, 46, 49)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_OUT, 'tx is categorized as cashout');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 50, 'tx have a from of quantity 50');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'GRT', 'tx has a from of currency GRT');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 43.45, 'tx have a to of quantity 43.45');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'EUR', 'tx has a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');

    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 0, 4, 9, 46, 49)));
    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.CASH_OUT, 'tx is categorized as cashout');
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 50, 'tx have a from of quantity 50');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'GRT', 'tx has a from of currency GRT');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 43.45, 'tx have a to of quantity 43.45');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'EUR', 'tx has a to of currency EUR');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx has no fee');

    assert.deepEqual(wasm.transactions[2]?.date, new Date(Date.UTC(2021, 1, 12, 9, 8, 18)));
    assert.strictEqual(wasm.transactions[2]?.category, TxCategory.CASH_OUT, 'tx is categorized as cashout');
    assert.strictEqual(wasm.transactions[2]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[2]?.from[0]?.quantity, 3, 'tx have a from of quantity 3');
    assert.strictEqual(wasm.transactions[2]?.from[0]?.currency, 'ALGO', 'tx has a from of currency ALGO');
    assert.strictEqual(wasm.transactions[2]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[2]?.to[0]?.quantity, 4.482, 'tx have a to of quantity 4.482');
    assert.strictEqual(wasm.transactions[2]?.to[0]?.currency, 'EUR', 'tx has a to of currency EUR');
    assert.strictEqual(wasm.transactions[2]?.fee.length, 1, 'tx has one fee');
    assert.strictEqual(wasm.transactions[2]?.fee[0]?.quantity, 0.02241, 'tx have a fee of quantity 0.02241');
    assert.strictEqual(wasm.transactions[2]?.fee[0]?.currency, 'ALGO', 'tx has a fee of currency ALGO');
  });

  test('it should import deposit', async function (assert) {
    // Given
    const csv = new File(
      [
        `portfolio,type,time,amount,balance,amount/balance unit,transfer id,trade id,order id\n` +
          `default,deposit,2021-02-12T09:08:18.000Z,3.0000000000000000,2400.5595900000000000,ALGO,00000000-0000-0000-0000-000000000001,,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 1, 12, 9, 8, 18)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DEPOSITS, 'tx is categorized as deposit');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 3, 'tx have a to of quantity 3');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ALGO', 'tx has a to of currency ALGO');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import exchange', async function (assert) {
    // Given
    const csv = new File(
      [
        `portfolio,type,time,amount,balance,amount/balance unit,transfer id,trade id,order id\n` +
          `default,match,2021-02-12T09:08:18.000Z,-2.1689524100000000,0.0000000000000000,USDC,,000001,00000000-0000-0000-0000-000000000001\n` +
          `default,match,2021-12-21T12:12:08.000Z,0.0000424200000000,2.1976000000000000,BTC,,000001,00000000-0000-0000-0000-000000000001\n` +
          `default,fee,2021-12-21T12:12:08.000Z,-0.0108447620000000,2.1866120000000000,USDC,,000001,00000000-0000-0000-0000-000000000001\n` +
          `default,match,2021-02-12T09:08:18.000Z,-2.1689524100000000,0.0000000000000000,USDC,,000002,00000000-0000-0000-0000-000000000002\n` +
          `default,match,2021-12-21T12:12:08.000Z,0.0000424200000000,2.1976000000000000,BTC,,000002,00000000-0000-0000-0000-000000000002\n` +
          `default,fee,2021-12-21T12:12:08.000Z,-0.0108447620000000,2.1866120000000000,BTC,,000002,00000000-0000-0000-0000-000000000002`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 1, 12, 9, 8, 18)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'tx1 is categorized as deposit');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx1 has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 2.16895241, 'tx1 have a from of quantity 2.16895241');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'USDC', 'tx1 has a from of currency USDC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx1 has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.00004242, 'tx1 have a to of quantity 0.00004242');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx1 has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx1 has one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.010844762, 'tx1 have a fee of quantity 0.010844762');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'USDC', 'tx1 has a fee of currency USDC');

    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 1, 12, 9, 8, 18)));
    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.EXCHANGES, 'tx2 is categorized as deposit');
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx2 has one from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 2.16895241, 'tx2 have a from of quantity 2.16895241');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'USDC', 'tx2 has a from of currency USDC');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx2 has one to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 0.00004242, 'tx2 have a to of quantity 0.00004242');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'BTC', 'tx2 has a to of currency BTC');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 1, 'tx2 has one fee');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.quantity, 0.010844762, 'tx2 have a fee of quantity 0.010844762');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.currency, 'BTC', 'tx2 has a fee of currency BTC');
  });

  test('it should import withdrawal', async function (assert) {
    // Given
    const csv = new File(
      [
        `portfolio,type,time,amount,balance,amount/balance unit,transfer id,trade id,order id\n` +
          `default,withdrawal,2021-02-12T09:08:18.000Z,-3.0000000000000000,0.0000000000000000,ALGO,00000000-0000-0000-0000-000000000001,,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 1, 12, 9, 8, 18)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.WITHDRAWALS, 'tx is categorized as withdrawal');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 3, 'tx have a from of quantity 3');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'ALGO', 'tx has a from of currency ALGO');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });
});
