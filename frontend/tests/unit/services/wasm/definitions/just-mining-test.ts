import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | just-mining', function (hooks) {
  setupWasmDefinitionTest(hooks, 'JustMining', 'JustMining');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import cashin', async function (assert) {
    // Given
    const csv = new File(
      [
        `id;type;source amount;source currency;destination amount;destination currency;address;memo;destination type;fee (%);info;date\n` +
          `n/a;exchange;186.78;EUR;100000.00000000;ETH;;;;0.00;;2021-04-01 01:02:03`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_IN, 'tx is categorized as cashin');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 3, 1, 1, 2, 3)));
    assert.deepEqual(wasm.transactions[0]?.note, '');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 186.78, 'tx have a to of quantity 186.78');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'EUR', 'transaction has a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 100000, 'transaction have a to of quantity 100000');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'transaction has a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction has no fee');
  });

  test('it should import crypto withdrawal', async function (assert) {
    // Given
    const csv = new File(
      [
        `id;type;source amount;source currency;destination amount;destination currency;address;memo;destination type;fee (%);info;date\n` +
          `000000;withdraw;1.1;ETH;;;0000000000000000000000000000000000;;NATIVE;0.00;;2021-04-01 01:02:03`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.WITHDRAWALS, 'tx is categorized as withdrawal');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 3, 1, 1, 2, 3)));
    assert.deepEqual(wasm.transactions[0]?.note, '');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'transaction has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 1.1, 'transaction have a to of quantity 1.1');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'ETH', 'transaction has a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'transaction has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction has no fee');
  });

  test('it should import fiat deposit', async function (assert) {
    // Given
    const csv = new File(
      [
        `id;type;source amount;source currency;destination amount;destination currency;address;memo;destination type;fee (%);info;date\n` +
          `n/a;credit;;;908.65;EUR;;;;6.06;order;2021-04-01 01:02:03`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 3, 1, 1, 2, 3)));
    assert.deepEqual(wasm.transactions[0]?.note, 'order');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 908.65, 'transaction have a to of quantity 908.65');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'EUR', 'transaction has a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction has no fee');
  });

  test('it should import masternode reward', async function (assert) {
    // Given
    const csv = new File(
      [
        `id;type;source amount;source currency;destination amount;destination currency;address;memo;destination type;fee (%);info;date\n` +
          `n/a;credit;;;8131.466;ETH;;;;0.00;reward;2021-04-01 01:02:03`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.MINING, 'tx is categorized as mining');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 3, 1, 1, 2, 3)));
    assert.deepEqual(wasm.transactions[0]?.note, 'reward');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 8131.466, 'tx has a to of quantity 8131.466');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'transaction has a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction has no fee');
  });

  test('it should import masternode subscription', async function (assert) {
    // Given
    const csv = new File(
      [
        `id;type;source amount;source currency;destination amount;destination currency;address;memo;destination type;fee (%);info;date\n` +
          `n/a;debit;10.00000000;ETH;;;;;;0.00;masternode;2021-04-01 01:02:03`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 3, 1, 1, 2, 3)));
    assert.deepEqual(wasm.transactions[0]?.note, 'masternode');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'transaction has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 10, 'tx have a to of quantity 10');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'ETH', 'transaction has a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'transaction has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction has no fee');
  });

  test('it should not ignore second transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `id;type;source amount;source currency;destination amount;destination currency;address;memo;destination type;fee (%);info;date\n` +
          `n/a;credit;;;334.00;EUR;;;;0.00;order;2021-04-01 01:02:03\n` +
          `n/a;credit;;;236.65;EUR;;;;0.00;order;2021-04-01 01:02:03`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two txs');
  });
});
