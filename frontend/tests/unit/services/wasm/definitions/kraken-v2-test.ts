import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | kraken-v2', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Kraken_v2', 'Kraken');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should ignore transfer from staking account to spot account', async function (assert) {
    // Given
    const csv = new File(
      [
        `"txid","refid","time","type","subtype","aclass","asset","wallet","amount","fee","balance"\n` +
          `"","000000-00000-000001","2021-09-14 07:09:30","withdrawal","","currency","ETH","spot / main",-215.00000000,0.00000000,""\n` +
          `"","000000-00000-000002","2021-09-14 07:09:31","deposit","","currency","ETH","spot / main",215.00000000,0.00000000,""\n` +
          `"000000-00000-000000","000000-00000-000002","2021-09-14 07:09:32","transfer","stakingfromspot","currency","ETH","spot / main",215.00000000,0.00000000,215.00000000\n` +
          `"000000-00000-000000","000000-00000-000001","2021-09-14 07:09:33","transfer","spottostaking","currency","ETH","spot / main",-215.00000000,0.00000000,0.00000003`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 8, 14, 7, 9, 32)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx have no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx have one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 215, 'tx have a to of quantity 10');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'tx have a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx have no fee');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 8, 14, 7, 9, 33)));
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx have one from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 215, 'tx have a from of 215');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'ETH', 'tx have a from of currency ETH');
    assert.strictEqual(wasm.transactions[1]?.to.length, 0, 'tx have no to');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx have no fee');
  });

  test('it should ignore transfer from spot account to staking account', async function (assert) {
    // Given
    const csv = new File(
      [
        `"txid","refid","time","type","subtype","aclass","asset","wallet","amount","fee","balance"\n` +
          `"000000-00000-000000","000000-00000-000001","2021-09-14 07:09:30","transfer","spottostaking","currency","ETH","spot / main",-215.00000000,0.00000000,0.00000003\n` +
          `"000000-00000-000000","000000-00000-000002","2021-09-14 07:09:35","transfer","stakingfromspot","currency","ETH","spot / main",215.00000000,0.00000000,215.00000000`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two transactions');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx1 is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 8, 14, 7, 9, 30)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx1 have one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 215, 'tx1 have a from of 215');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'ETH', 'tx1 have a from of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx1 have no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx1 have no fee');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.DONT_CARE, 'tx2 is categorized as ignored');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 8, 14, 7, 9, 35)));
    assert.strictEqual(wasm.transactions[1]?.from.length, 0, 'tx2 have no from');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx2 have one to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 215, 'tx2 have a from of 215');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'ETH', 'tx2 have a from of currency ETH');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx2 have no fee');
  });

  test('it should ignore transfer from staking account eth2 rewards to eth', async function (assert) {
    // Given
    const csv = new File(
      [
        `"txid","refid","time","type","subtype","aclass","asset","wallet","amount","fee","balance"\n` +
          `"000000-00000-000000","000000-00000-000001","2021-09-14 07:09:30","staking","","currency","XETH","spot / main",1.1148,0,1.1202667114\n` +
          // We cannot substract this amount. So we use special MergeProcessor NegativeMining to ignore previous mining transaction
          `"000000-00000-000000","000000-00000-000002","2021-09-14 17:09:35","staking","","currency","ETH2","spot / main",-1.1148,0,0.0005933150`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx1 is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 8, 14, 7, 9, 30)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx1 has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 1.1148, 'tx1 has a from of 1.1148');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'ETH', 'tx1 has a from of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx1 has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 1.1148, 'tx1 has a from of 1.1148');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'tx1 has a from of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx1 has no fee');
  });

  test('it should import airdrop - transfer', async function (assert) {
    // Given
    const csv = new File(
      [
        `"txid","refid","time","type","subtype","aclass","asset","wallet","amount","fee","balance"\n` +
          `"000000-00000-000000","000000-00000-000002","2021-09-14 07:09:32","transfer","","currency","BTC","spot / main",302.2000000000,0.0000000000,302.2000000000`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.AIR_DROPS, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 8, 14, 7, 9, 32)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx have no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx have one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 302.2, 'tx have a to of quantity 302.2');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx have a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx have no fee');
  });

  test('it should import airdrop - dividend', async function (assert) {
    // Given
    const csv = new File(
      [
        `"txid","refid","time","type","subtype","aclass","asset","wallet","amount","fee","balance"\n` +
          `"000000-00000-000000","000000-00000-000002","2021-09-14 07:09:32","dividend","","currency","LUNA2","spot / main",2.2000000000,0.0000000000,5.2000000000`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.AIR_DROPS, 'tx is categorized as airdrop');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 8, 14, 7, 9, 32)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx have no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx have one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 2.2, 'tx have a to of quantity 2.2');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'LUNA2', 'tx have a to of currency LUNA2');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx have no fee');
  });

  test('it should import cashin', async function (assert) {
    // Given
    const csv = new File(
      [
        `"txid","refid","time","type","subtype","aclass","asset","wallet","amount","fee","balance"\n` +
          `"000000-00000-00000A","000000-00000-000001","2021-09-14 07:09:39","trade","","currency","ZEUR","spot / main",-9.9741,0.0259,0.0000\n` +
          `"000000-00000-00000B","000000-00000-000001","2021-09-14 07:09:39","trade","","currency","XXBT","spot / main",0.2110474000,0.000006400,0.2110467600\n` +
          `"000000-00000-00000C","000000-00000-000002","2021-10-20 14:20:35","spend","","currency","ZEUR","spot / main",-98.5200,1.4800,900.0000\n` +
          `"000000-00000-00000D","000000-00000-000002","2021-10-20 14:20:35","receive","","currency","XXBT","spot / main",2.6934815300,0.0000000000,2.6934815300`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_IN, 'tx is categorized as cashin');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 8, 14, 7, 9, 39)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx have one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 10, 'tx have a from of 10');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'EUR', 'tx have a from of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx have one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.2110474, 'tx have a to of quantity 0.2110474');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx have a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx have one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.0000064, 'tx have a fee of quantity 0.0000064');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'BTC', 'tx have a to of currency BTC');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.CASH_IN, 'tx is categorized as cashin');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 9, 20, 14, 20, 35)));
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx have one from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 100, 'tx have a from of 100');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'EUR', 'tx have a from of currency EUR');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx have one to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 2.69348153, 'tx have a to of quantity 2.69348153');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'BTC', 'tx have a to of currency BTC');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx have no fee');
  });

  test('it should import cashout', async function (assert) {
    // Given
    const csv = new File(
      [
        `"txid","refid","time","type","subtype","aclass","asset","wallet","amount","fee","balance"\n` +
          `"000000-00000-000000","000000-00000-000001","2021-09-14 07:09:39","trade","","currency","XETH","spot / main",-2.2073800000,0.0000000000,0.0000044758\n` +
          `"000000-00000-000000","000000-00000-000001","2021-09-14 07:09:39","trade","","currency","ZEUR","spot / main",7807.8562,20.3004,7787.5558`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_OUT, 'tx is categorized as cashout');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 8, 14, 7, 9, 39)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx have one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 2.20738, 'tx have a from of 2.20738');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'ETH', 'tx have a from of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx have one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 7787.5558, 'tx have a to of quantity 7787.5558');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'EUR', 'tx have a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx have no fee');
  });

  test('it should import crypto deposit', async function (assert) {
    // Given
    const csv = new File(
      [
        `"txid","refid","time","type","subtype","aclass","asset","wallet","amount","fee","balance"\n` +
          `"","0000000-000000-000001","2021-09-14 06:55:00","deposit","","currency","XXBT","spot / main",1.0000000000,0.0000000000,""\n` +
          `"000000-00000-000000","0000000-000000-000001","2021-09-14 07:03:24","deposit","","currency","XXBT","spot / main",1.0000000000,0.0000000000,1.0000000000`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DEPOSITS, 'tx is categorized as deposit');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 8, 14, 7, 3, 24)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx have no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx have one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 1, 'tx have a from of 1');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx have a from of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx have no fee');
  });

  test('it should import fiat deposit', async function (assert) {
    // Given
    const csv = new File(
      [
        `"txid","refid","time","type","subtype","aclass","asset","wallet","amount","fee","balance"\n` +
          `"","0000000-000000-000001","2021-09-14 06:55:00","deposit","","currency","ZEUR","spot / main",10.0000,0.0000,""\n` +
          `"000000-00000-000000","0000000-000000-000001","2021-09-14 07:03:24","deposit","","currency","ZEUR","spot / main",10.0000,0.0000,10.0000`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 8, 14, 7, 3, 24)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx have no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx have one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 10, 'tx have a from of 10');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'EUR', 'tx have a from of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx have no fee');
  });

  test('it should import staking', async function (assert) {
    // Given
    const csv = new File(
      [
        `"txid","refid","time","type","subtype","aclass","asset","wallet","amount","fee","balance"\n` +
          `"000000-00000-000000","0000000-000000-000001","2021-09-14 07:03:24","staking","","currency","ETH","spot / main",0.09752700,0.00000000,0.99486700`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.MINING, 'tx is categorized as mining');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 8, 14, 7, 3, 24)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx have no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx have one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.097527, 'tx have a from of 0.097527');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'tx have a from of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx have no fee');
  });

  test('it should import trades', async function (assert) {
    // Given
    const csv = new File(
      [
        `"txid","refid","time","type","subtype","aclass","asset","wallet","amount","fee","balance"\n` +
          `"000000-00000-000000","000000-00000-000001","2021-09-14 07:09:39","trade","","currency","XETH","spot / main",-0.0016383000,0.0000000000,0.001\n` +
          `"000000-00000-000000","000000-00000-000001","2021-09-14 07:09:39","trade","","currency","XXBT","spot / main",0.0000067200,0.0010000000,0.001`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'tx is categorized as exchanges');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 8, 14, 7, 9, 39)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx have one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 0.0016383, 'tx have a from of 0.0016383');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'ETH', 'tx have a from of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx have one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.00000672, 'tx have a to of quantity 0.00000672');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx have a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx have one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.001, 'tx have a fee of quantity 0.001');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'BTC', 'tx have a to of currency BTC');
  });

  test('it should import transfer between spot and futures account', async function (assert) {
    // Given
    const csv = new File(
      [
        `"txid","refid","time","type","subtype","aclass","asset","wallet","amount","fee","balance"\n` +
          `"000000-00000-000000","000000-00000-000001","2021-09-14 07:09:32","transfer","spottofutures","currency","ETH","spot / main",-215.00000000,0.00000000,0.00000000\n` +
          `"000000-00000-000000","000000-00000-000002","2021-09-14 07:09:33","transfer","spotfromfutures","currency","ETH","spot / main",215.00000000,0.00000000,215.00000000`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two transactions');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.WITHDRAWALS, 'tx1 is categorized as withdrawal');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 8, 14, 7, 9, 32)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx1 have one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 215, 'tx1 have a from of 215');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'ETH', 'tx1 have a from of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx1 have no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx1 have no fee');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.DEPOSITS, 'tx2 is categorized as deposit');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 8, 14, 7, 9, 33)));
    assert.strictEqual(wasm.transactions[1]?.from.length, 0, 'tx2 have no from');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx2 have one to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 215, 'tx2 have a to of 215');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'ETH', 'tx2 have a to of currency ETH');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx2 have no fee');
  });

  test('it should merge singleton exchange', async function (assert) {
    // Given
    const csv = new File(
      [
        `"txid","refid","time","type","subtype","aclass","asset","wallet","amount","fee","balance"\n` +
          `"000000-00000-000001","000000-00000-00000A","2020-01-01 06:18:01","trade","","currency","XETH","spot / main",-0.8774760000,0.0000000000,0.0014039600\n` +
          `"000000-00000-000002","000000-00000-00000A","2020-01-01 06:18:01","trade","","currency","XXBT","spot / main",0.0177950000,0.0000280000,0.0206291700\n` +
          `"000000-00000-000003","000000-00000-00000B","2020-01-01 06:18:01","trade","","currency","XETH","spot / main",-0.0014010000,0.0000000000,0.0000022400\n` +
          `"000000-00000-000004","000000-00000-00000B","2020-01-01 06:18:01","trade","","currency","XXBT","spot / main",0.0000280000,0.0000000000,0.0206571700\n` +
          `"000000-00000-000005","000000-00000-00000C","2020-01-01 06:18:01","trade","","currency","XETH","spot / main",-0.0001440000,0.0000000000,0.0000008000\n`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'tx1 is categorized as exchange');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2020, 0, 1, 6, 18, 1)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx1 have one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 0.877476, 'tx1 has a from of quantity 0.877476');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'ETH', 'tx1 has a from of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx1 has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.017795, 'tx1 has a to of quantity 0.017795');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx1 has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx1 has one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.000028, 'tx1 has a fee of quantity 0.000028');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'BTC', 'tx1 has a fee of currency BTC');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.EXCHANGES, 'tx2 is categorized as exchange');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2020, 0, 1, 6, 18, 1)));
    assert.strictEqual(wasm.transactions[1]?.from.length, 2, 'tx2 has two from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 0.001401, 'tx2 has a from of quantity 0.001401');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'ETH', 'tx2 has a from of currency ETH');
    assert.strictEqual(wasm.transactions[1]?.from[1]?.quantity, 0.000144, 'tx2 has a from of quantity 0.000144');
    assert.strictEqual(wasm.transactions[1]?.from[1]?.currency, 'ETH', 'tx2 has a from of currency ETH');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx2 has one to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 0.000028, 'tx2 has a to of quantity 0.000028');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'BTC', 'tx2 has a to of currency BTC');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx2 has no fee');
  });

  test('it should not merge singleton exchange of different currency', async function (assert) {
    // Given
    const csv = new File(
      [
        `"txid","refid","time","type","subtype","aclass","asset","wallet","amount","fee","balance"\n` +
          `"000000-00000-000003","000000-00000-00000B","2020-01-01 06:18:01","trade","","currency","XETH","spot / main",-0.0014010000,0.0000000000,0.0000022400\n` +
          `"000000-00000-000004","000000-00000-00000B","2020-01-01 06:18:01","trade","","currency","XXBT","spot / main",0.0000280000,0.0000000000,0.0206571700\n` +
          `"000000-00000-000005","000000-00000-00000C","2020-01-01 06:18:01","trade","","currency","XXMR","spot / main",-0.0001440000,0.0000000000,0.0000008000\n`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'tx1 is categorized as exchange');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2020, 0, 1, 6, 18, 1)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx1 has two from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 0.001401, 'tx1 has a from of quantity 0.001401');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'ETH', 'tx1 has a from of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx1 has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.000028, 'tx1 has a to of quantity 0.000028');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx1 has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx1 has no fee');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.EXCHANGES, 'tx2 is categorized as exchange');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2020, 0, 1, 6, 18, 1)));
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx2 has one from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 0.000144, 'tx2 has a from of quantity 0.000144');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'XMR', 'tx2 has a from of currency XMR');
    assert.strictEqual(wasm.transactions[1]?.to.length, 0, 'tx2 has no to');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx2 has no fee');
  });
});
