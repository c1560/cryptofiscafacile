import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | celsius', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Celsius', 'Celsius');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import reward', async function (assert) {
    // Given
    const csv = new File(
      [
        `Internal id, Date and time, Transaction type, Coin type, Coin amount, USD Value, Original Reward Coin, Reward Amount In Original Coin, Confirmed\n` +
          `"00000000-0000-0000-0000-000000000000","December 31, 2021 5:00 AM","Reward","BTC","0.000003005711448861","0.142542859750784064","BTC","","Yes"`,
      ],
      'celsius.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one reward');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.INTERESTS, 'reward is categorized as Interests');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'reward has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'reward has a to');
    assert.strictEqual(
      wasm.transactions[0]?.to[0]?.quantity,
      0.000003005711448861,
      'reward have a to of quantity 0.000003005711448861',
    );
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'reward has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'reward has no fee');
  });

  test('it should import promo Code promo Code reward', async function (assert) {
    // Given
    const csv = new File(
      [
        `Internal id, Date and time, Transaction type, Coin type, Coin amount, USD Value, Original Reward Coin, Reward Amount In Original Coin, Confirmed\n` +
          `"00000000-0000-0000-0000-000000000000","June 7, 2021 6:37 AM","Promo Code Reward","BTC","0.001436209214507804","50.000000000000000000","","","Yes"`,
      ],
      'celsius.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one reward');
    assert.strictEqual(
      wasm.transactions[0]?.category,
      TxCategory.COMMERCIAL_REBATES,
      'reward is categorized as CommercialRebates',
    );
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 7, 6, 37)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'reward has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'reward has a to');
    assert.strictEqual(
      wasm.transactions[0]?.to[0]?.quantity,
      0.001436209214507804,
      'reward have a to of quantity 0.001436209214507804',
    );
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'reward has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'reward has no fee');
  });

  test('it should import deposit', async function (assert) {
    // Given
    const csv = new File(
      [
        `Internal id, Date and time, Transaction type, Coin type, Coin amount, USD Value, Original Reward Coin, Reward Amount In Original Coin, Confirmed\n` +
          `"00000000-0000-0000-0000-000000000000","April 29, 2021 6:20 AM","Transfer","ETH","2","5464.19897738","","","Yes"`,
      ],
      'celsius.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one deposit');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DEPOSITS, 'deposit is categorized as deposit');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 3, 29, 6, 20)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'deposit has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'deposit has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 2, 'deposit have a to of quantity 2');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'deposit has a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'deposit has no fee');
  });

  test('it should import swap', async function (assert) {
    // Given
    const csv = new File(
      [
        `Internal id, Date and time, Transaction type, Coin type, Coin amount, USD Value, Original Reward Coin, Reward Amount In Original Coin, Confirmed\n` +
          `"00000000-0000-0000-0000-000000000000","June 8, 2021 8:20 AM","Swap in","BTC","0.0801234","2461.49","","","Yes"\n` +
          `"00000000-0000-0000-0000-000000000001","June 8, 2021 8:20 AM","Swap out","ETH","-1.123456","-2461.49","","","Yes"`,
      ],
      'celsius.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'tx is categorized as exchanges');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 8, 8, 20)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 1.123456, 'tx has a from of quantity 1.123456');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'ETH', 'tx has a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.0801234, 'tx have a to of quantity 0.0801234');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import withdrawal', async function (assert) {
    // Given
    const csv = new File(
      [
        `Internal id, Date and time, Transaction type, Coin type, Coin amount, USD Value, Original Reward Coin, Reward Amount In Original Coin, Confirmed\n` +
          `"d261bcc5-2a98-709f-a8d9-a12ea8f513cc","April 01, 2021 6:25 AM","Withdrawal","BTC","-0.47785308","-15.8423232387683146012","","","Yes"`,
      ],
      'celsius.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(
      wasm.transactions[0]?.category,
      TxCategory.WITHDRAWALS,
      'transaction is categorized as withdrawals',
    );
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 3, 1, 6, 25)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'transaction has one from');
    assert.strictEqual(
      wasm.transactions[0]?.from[0]?.quantity,
      0.47785308,
      'transaction have a from of quantity 0.47785308',
    );
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'transaction has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'transaction has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction has no fee');
  });
});
