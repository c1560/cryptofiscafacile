import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | cryptofiscafacile', function (hooks) {
  setupWasmDefinitionTest(hooks, 'CryptoFiscaFacile', 'CryptoFiscaFacile');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import Cashin transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2022-02-08 08:28:00,Cashin,100,EUR,10,ETH,10,EUR,Exemple simple de Cashin\n` +
          `2022-04-08 08:28:01,Cashin,100,EUR,10,ETH,0.1,ETH,Exemple simple de Cashin2`,
      ],
      'custom.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_IN, 'tx is categorized as Cashin');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 1, 8, 7, 28, 0)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 110, 'tx has a from of 100 + 10 EUR fees');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'EUR', 'tx has a from of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 10, 'tx has a to of quantity 10');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'tx has a to of currency ETH');
    // FEE is included in CASHIN FROM
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.CASH_IN, 'tx is categorized as Cashin');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2022, 3, 8, 6, 28, 1)));
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx has a from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 100, 'tx has a from of 100 + 10 EUR fees');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'EUR', 'tx has a from of currency EUR');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 10, 'tx has a to of quantity 10');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'ETH', 'tx has a to of currency ETH');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 1, 'tx has one fee');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.quantity, 0.1, 'tx has a fee of quantity 10');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.currency, 'ETH', 'tx has a fee of currency ETH');
  });

  test('it should import Cashout transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2021-04-18 08:30:10,Cashout,1,BTC,10000,EUR,0.000001,BTC,Exemple simple de Cashout\n` +
          `2021-04-18 08:30:11,Cashout,1,BTC,1000,EUR,1,EUR,Exemple simple de Cashout`,
      ],
      'custom.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two txs');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_OUT, 'tx is categorized as Cashout');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 3, 18, 6, 30, 10)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 1, 'tx has a from of 1');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'tx has a from of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 10000, 'tx has a to of quantity 10000');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'EUR', 'tx has a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx has a fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.000001, 'tx has a fee of quantity 0.000001');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'BTC', 'tx has a fee of currency BTC');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.CASH_OUT, 'tx is categorized as Cashout');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 3, 18, 6, 30, 11)));
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx has a from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 1, 'tx has a from of 1');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'BTC', 'tx has a from of currency BTC');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 999, 'tx has a to of quantity 1000 - 1 EUR fee');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'EUR', 'tx has a to of currency EUR');
    // FIAT FEE is excluded from all CASHOUT
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import Deposit transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2022-04-08 08:28:00,Depot,,,2,BTC,,,Exemple simple de Dépôt\n` +
          `2022-04-08 08:28:01,Depot,,,2,BTC,0.05,BTC,Exemple simple de Dépôt\n`,
      ],
      'custom.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two transactions');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DEPOSITS, 'tx is categorized as Deposit');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 3, 8, 6, 28, 0)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 2, 'transaction has a to of quantity 2');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'transaction has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction has no fee');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.DEPOSITS, 'tx is categorized as Deposit');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2022, 3, 8, 6, 28, 1)));
    assert.strictEqual(wasm.transactions[1]?.from.length, 0, 'transaction has no from');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'transaction has a to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 2, 'transaction has a to of quantity 3');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'BTC', 'transaction has a to of currency BTC');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 1, 'transaction has one fee');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.quantity, 0.05, 'transaction has a fee of quantity 3');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.currency, 'BTC', 'transaction has a fee of currency BTC');
  });

  test('it should import Gifts sent transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2022-04-08 08:28:00,Don,1,BTC,,,0.000001,BTC,Exemple simple de Don émis`,
      ],
      'custom.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.GIFTS, 'transaction is categorized as Cashin');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 3, 8, 6, 28, 0)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'transaction has a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 1, 'transaction has a from of 1');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'transaction has a from of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'transaction has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'transaction has a fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.000001, 'tx has a fee of quantity 0.000001');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'BTC', 'transaction has a fee of currency BTC');
  });

  test('it should import Gifts received transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2022-04-08 08:28:00,Don,,,1,BTC,,,Exemple simple de Don reçu`,
      ],
      'custom.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.GIFTS, 'transaction is categorized as Cashin');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 3, 8, 6, 28, 0)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 1, 'transaction has a from of 1');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'transaction has a from of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction has no fee');
  });

  test('it should import Exchange transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2021-04-18 08:30:10,Echange,1,BTC,10,ETH,0.000001,BTC,Exemple simple de Echange`,
      ],
      'custom.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'transaction is categorized as Exchange');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 3, 18, 6, 30, 10)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'transaction has a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 1, 'transaction has a from of 1');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'transaction has a from of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 10, 'transaction has a to of quantity 10');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'transaction has a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'transaction has a fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.000001, 'transaction has a fee of quantity 0.000001');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'BTC', 'transaction has a fee of currency BTC');
  });

  test('it should import Interests transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2022-04-08 08:28:00,Interet,,,2,BTC,,,Exemple simple d'Intérêt perçu\n` +
          `2022-04-08 08:28:00,Interet,,,3,BTC,1,BTC,Exemple d'Intérêt perçu avec frais`,
      ],
      'custom.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two transactions');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.INTERESTS, 'tx is categorized as Interests');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 3, 8, 6, 28, 0)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 2, 'transaction has a to of quantity 2');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'transaction has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction has no fee');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.INTERESTS, 'tx is categorized as Interests');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2022, 3, 8, 6, 28, 0)));
    assert.strictEqual(wasm.transactions[1]?.from.length, 0, 'transaction has no from');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'transaction has a to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 3, 'transaction has a to of quantity 3');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'BTC', 'transaction has a to of currency BTC');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 1, 'transaction has one fee');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.quantity, 1, 'transaction has a to of quantity 3');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.currency, 'BTC', 'transaction has a to of currency BTC');
  });

  test('it should import Mining transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2022-04-08 08:28:00,Minage,,,1,BTC,,,Exemple simple de récompense de Minage`,
      ],
      'custom.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.MINING, 'transaction is categorized as Mining');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 3, 8, 6, 28, 0)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 1, 'transaction has a to of quantity 1');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'transaction has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction has no fee');
  });

  test('it should import Referral transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2022-04-08 08:28:00,Parrainage,,,1,BTC,,,Exemple simple de récompense de Parrainage`,
      ],
      'custom.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.REFERRALS, 'transaction is categorized as Referral');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 3, 8, 6, 28, 0)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 1, 'transaction has a to of quantity 1');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'transaction has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction has no fee');
  });

  test('it should import Commercial Rebate transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2022-04-08 08:28:00,Remise Commerciale,,,1,BTC,,,Exemple simple de Remise Commerciale reçue`,
      ],
      'custom.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(
      wasm.transactions[0]?.category,
      TxCategory.COMMERCIAL_REBATES,
      'transaction is categorized as Commercial Rebate',
    );
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 3, 8, 6, 28, 0)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 1, 'transaction has a to of quantity 1');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'transaction has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction has no fee');
  });

  test('it should import Withdrawal transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2021-04-18 08:30:10,Retrait,1,BTC,,,0.000001,BTC,Exemple simple de Retrait`,
      ],
      'custom.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(
      wasm.transactions[0]?.category,
      TxCategory.WITHDRAWALS,
      'transaction is categorized as Withdrawal',
    );
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 3, 18, 6, 30, 10)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'transaction has a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 1, 'transaction has a from of 1');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'transaction has a from of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'transaction has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'transaction has a fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.000001, 'transaction has a fee of quantity 0.000001');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'BTC', 'transaction has a fee of currency BTC');
  });

  test('it should import fees', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2022-04-08 08:28:00,Frais,,,,,50,BTC,Note`,
      ],
      'test.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.FEES, 'tx is fees');
    assert.strictEqual(wasm.transactions[0]?.note, 'Note', 'tx note is Note');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx have no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx have no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx have one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 50, 'tx have a fee of quantity 50');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'BTC', 'tx have a fee of currency BTC');
  });
});
