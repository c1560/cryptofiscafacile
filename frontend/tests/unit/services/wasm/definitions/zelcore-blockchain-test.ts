import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | zelcore-blockchain', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Zelcore_Blockchain', 'Zelcore');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import crypto deposit', async function (assert) {
    // Given
    const csv = new File(
      [
        `txid,formattedDate,timestamp,amount,direction\n` +
          `000000000000000000000000000000A,"06/05/2022, 18:08:47",1651853327,1,received`,
      ],
      'FLUX_transactions_00000000000000000000000000000000000.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DEPOSITS, 'tx is categorized as deposit');
    assert.strictEqual(wasm.transactions[0]?.note, '000000000000000000000000000000A', 'tx has a note');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 4, 6, 16, 8, 47)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 1, 'tx has a from of quantity 1');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'FLUX', 'tx has a from of currency FLUX');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import crypto withdrawal', async function (assert) {
    // Given
    const csv = new File(
      [
        `txid,formattedDate,timestamp,amount,direction\n` +
          `000000000000000000000000000000A,"06/05/2022, 18:08:47",1651853327,-1,sent`,
      ],
      'FLUX_transactions_00000000000000000000000000000000000.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.WITHDRAWALS, 'tx is categorized as deposit');
    assert.strictEqual(wasm.transactions[0]?.note, '000000000000000000000000000000A', 'tx has a note');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 4, 6, 16, 8, 47)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 1, 'tx has a from of quantity 1');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'FLUX', 'tx has a from of currency FLUX');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });
});
