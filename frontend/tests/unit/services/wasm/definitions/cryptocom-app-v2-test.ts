import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | cryptocom-app-v2', function (hooks) {
  setupWasmDefinitionTest(hooks, 'CryptoCom_App_v2', 'CryptoCom_App');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import airdrop locked', async function (assert) {
    // Given
    const csv = new File(
      [
        `Timestamp (UTC),Transaction Description,Currency,Amount,To Currency,To Amount,Native Currency,Native Amount,Native Amount (in USD),Transaction Kind,Transaction Hash\n` +
          `2021-11-25 04:42:02,CRO Airdrop,CRO,84157.65,,,EUR,671.56,304.3378493719905,airdrop_locked,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.MINING, 'tx is categorized as mining');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 10, 25, 4, 42, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 84157.65, 'tx has a to of quantity 84157.65');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'CRO', 'tx have a to of currency CRO');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import finance.dpos.compound_interest.crypto_wallet', async function (assert) {
    // Given
    const csv = new File(
      [
        `Timestamp (UTC),Transaction Description,Currency,Amount,To Currency,To Amount,Native Currency,Native Amount,Native Amount (in USD),Transaction Kind,Transaction Hash\n` +
          `2021-11-25 04:42:02,DPoS compound interest received,DOT,4.5501,,,EUR,9.11300528,1.752644318556588,finance.dpos.compound_interest.crypto_wallet,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.MINING, 'tx is categorized as mining');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 10, 25, 4, 42, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 4.5501, 'tx has a to of quantity 4.5501');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'DOT', 'tx have a to of currency DOT');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import finance.lockup.dpos_compound_interest.crypto_wallet', async function (assert) {
    // Given
    const csv = new File(
      [
        `Timestamp (UTC),Transaction Description,Currency,Amount,To Currency,To Amount,Native Currency,Native Amount,Native Amount (in USD),Transaction Kind,Transaction Hash\n` +
          `2021-11-25 04:42:02,Cardholder CRO Stake Reward,CRO,955.46630,,,EUR,31.46133991,02.3659104098183375,finance.lockup.dpos_compound_interest.crypto_wallet,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.INTERESTS, 'tx is categorized as interests');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 10, 25, 4, 42, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 955.4663, 'tx has a to of quantity 955.4663');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'CRO', 'tx have a to of currency CRO');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import finance.dpos.non_compound_interest.crypto_wallet', async function (assert) {
    // Given
    const csv = new File(
      [
        `Timestamp (UTC),Transaction Description,Currency,Amount,To Currency,To Amount,Native Currency,Native Amount,Native Amount (in USD),Transaction Kind,Transaction Hash\n` +
          `2021-11-25 04:42:02,DPoS non-compound interest received,ETH,9.46817455,,,EUR,0.23023209,8.0114453469522728,finance.dpos.non_compound_interest.crypto_wallet,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.MINING, 'tx is categorized as mining');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 10, 25, 4, 42, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 9.46817455, 'tx has a to of quantity 9.46817455');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'tx have a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import supercharger', async function (assert) {
    // Given
    const csv = new File(
      [
        `Timestamp (UTC),Transaction Description,Currency,Amount,To Currency,To Amount,Native Currency,Native Amount,Native Amount (in USD),Transaction Kind,Transaction Hash\n` +
          `2021-11-25 04:42:02,Supercharger Reward,ETH,0.0000088,,,EUR,0.01,0.0106469373,supercharger_reward_to_app_credited,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.MINING, 'tx is categorized as deposit');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 10, 25, 4, 42, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.0000088, 'tx has a to of quantity 0.0000088');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'tx have a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should ignore finance.dpos.staking.crypto_wallet', async function (assert) {
    // Given
    const csv = new File(
      [
        `Timestamp (UTC),Transaction Description,Currency,Amount,To Currency,To Amount,Native Currency,Native Amount,Native Amount (in USD),Transaction Kind,Transaction Hash\n` +
          `2021-11-25 04:42:02,DPoS staking payment,ETH,-8.34025453,,,EUR,0138.70,14044.12358509352,finance.dpos.staking.crypto_wallet,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 10, 25, 4, 42, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 8.34025453, 'tx has a to of quantity 8.34025453');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'ETH', 'tx have a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should ignore finance.dpos.unstaking.crypto_wallet', async function (assert) {
    // Given
    const csv = new File(
      [
        `Timestamp (UTC),Transaction Description,Currency,Amount,To Currency,To Amount,Native Currency,Native Amount,Native Amount (in USD),Transaction Kind,Transaction Hash\n` +
          `2021-11-25 04:42:02,DPoS unstaking payout,INJ,2.0,,,EUR,41.388588284707897836,40.9268538728826075953196560,finance.dpos.unstaking.crypto_wallet,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 10, 25, 4, 42, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 2, 'tx has a to of quantity 2');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'INJ', 'tx have a to of currency INJ');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should ignore finance.lockup.dpos_lock.crypto_wallet', async function (assert) {
    // Given
    const csv = new File(
      [
        `Timestamp (UTC),Transaction Description,Currency,Amount,To Currency,To Amount,Native Currency,Native Amount,Native Amount (in USD),Transaction Kind,Transaction Hash\n` +
          `2021-11-25 04:42:02,Cardholder CRO Stake,CRO,-4735.4,,,EUR,243.55,986.226987822,finance.lockup.dpos_lock.crypto_wallet,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 10, 25, 4, 42, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 4735.4, 'tx has a to of quantity 4735.4');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'CRO', 'tx have a to of currency CRO');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should ignore invest deposit', async function (assert) {
    // Given
    const csv = new File(
      [
        `Timestamp (UTC),Transaction Description,Currency,Amount,To Currency,To Amount,Native Currency,Native Amount,Native Amount (in USD),Transaction Kind,Transaction Hash\n` +
          `2021-11-25 04:42:02,Invest Deposit,BTC,-3.6287473,,,USD,603.3,731.9,invest_deposit,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 10, 25, 4, 42, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 3.6287473, 'tx has a to of quantity 3.6287473');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'tx have a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should ignore invest withdrawal', async function (assert) {
    // Given
    const csv = new File(
      [
        `Timestamp (UTC),Transaction Description,Currency,Amount,To Currency,To Amount,Native Currency,Native Amount,Native Amount (in USD),Transaction Kind,Transaction Hash\n` +
          `2021-11-25 04:42:02,Invest Withdrawal,BTC,0.36650234,,,EUR,956.14,367.83569040601496,invest_withdrawal,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as deposit');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 10, 25, 4, 42, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.36650234, 'tx has a to of quantity 0.36650234');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx have a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should not merge deposits different coins transactions', async function (assert) {
    // Given
    const csv = new File(
      [
        `Timestamp (UTC),Transaction Description,Currency,Amount,To Currency,To Amount,Native Currency,Native Amount,Native Amount (in USD),Transaction Kind,Transaction Hash\n` +
          `2021-11-25 04:42:02,Transfer: Exchange -> App wallet,BOSON,0.0091,,,EUR,0.02,0.0208626006,exchange_to_crypto_transfer,\n` +
          `2021-11-25 04:42:02,Transfer: Exchange -> App wallet,CRO,0.097608,,,EUR,0.07,0.0730191021,exchange_to_crypto_transfer,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DEPOSITS, 'tx1 is categorized as deposit');
    assert.strictEqual(
      wasm.transactions[0]?.id,
      'c451a173e8cdf25e478d80036d8d777b276eb6bd271ca13b9878a13d5a0a7756',
      'tx1 id is c451a173e8cdf25e478d80036d8d777b276eb6bd271ca13b9878a13d5a0a7756',
    );
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx1 has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.0091, 'tx1 have a to of quantity 0.0091');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BOSON', 'tx1 have a to of currency BOSON');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.DEPOSITS, 'tx2 is categorized as deposit');
    assert.strictEqual(
      wasm.transactions[1]?.id,
      'c451a173e8cdf25e478d80036d8d777b276eb6bd271ca13b9878a13d5a0a7756-1',
      'tx2 id is c451a173e8cdf25e478d80036d8d777b276eb6bd271ca13b9878a13d5a0a7756-1',
    );
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx2 has a to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 0.097608, 'tx2 have a to of quantity 0.097608');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'CRO', 'tx2 have a to of currency CRO');
  });

  test('it should not show trading.limit_order.fiat_wallet.purchase_lock', async function (assert) {
    // Given
    const csv = new File(
      [
        `Timestamp (UTC),Transaction Description,Currency,Amount,To Currency,To Amount,Native Currency,Native Amount,Native Amount (in USD),Transaction Kind,Transaction Hash\n` +
          `2021-11-25 04:42:02,EUR -> BNB,EUR,-15.60,BNB,0.19,USD,69.700053809709518294,63.291545442662185650,trading.limit_order.fiat_wallet.purchase_lock,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 0, 'has one tx');
    assert.strictEqual((wasm.history ?? [])[0]?.unknowns.length, 0, 'has no unknows');
  });

  test('it should not show trading.limit_order.fiat_wallet.purchase_unlock', async function (assert) {
    // Given
    const csv = new File(
      [
        `Timestamp (UTC),Transaction Description,Currency,Amount,To Currency,To Amount,Native Currency,Native Amount,Native Amount (in USD),Transaction Kind,Transaction Hash\n` +
          `2021-11-25 04:42:02,EUR -> BNB,EUR,25.00,BNB,0.19,USD,00.338328860341531374,84.870819937935444576,trading.limit_order.fiat_wallet.purchase_unlock,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 0, 'has one tx');
    assert.strictEqual((wasm.history ?? [])[0]?.unknowns.length, 0, 'has no unknows');
  });
});
