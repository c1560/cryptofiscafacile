import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | feel-mining', function (hooks) {
  setupWasmDefinitionTest(hooks, 'FeelMining', 'FeelMining');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import cashin', async function (assert) {
    // Given
    const csv = new File(
      [
        `"Type,Client,Crypto/Pack,Debit,Credit,""Valeur euro"",tx_id,Date"\n` +
          `"Paiement,0000000,EUR,20.00000000,0.00000000,20.00000000,,""13/01/2021 15:07:11"""\n` +
          `"Achat direct de crypto-actif,0000000,DOT,0.00000000,0.7581,19.50000000,,""13/01/2021 15:27:02"""`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 13, 15, 7, 11)));
    assert.deepEqual(wasm.transactions[0]?.note, 'Paiement');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 20, 'tx have a from of quantity 20');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'EUR', 'transaction has a from of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'transaction has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction has no fee');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.CASH_IN, 'tx is categorized as cashin');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 0, 13, 15, 27, 2)));
    assert.deepEqual(wasm.transactions[1]?.note, 'Achat direct de crypto-actif');
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 19.5, 'tx have a from of quantity 19.5');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'EUR', 'transaction has a from of currency EUR');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'transaction has one to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 0.7581, 'transaction has a to of quantity 0.7581');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'DOT', 'transaction has a to of currency DOT');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'transaction has no fee');
  });

  test('it should import contract termination', async function (assert) {
    // Given
    const csv = new File(
      [
        `"Type,Client,Crypto/Pack,Debit,Credit,""Valeur euro"",tx_id,Date"\n` +
          `"Résiliation de contrat,0000000,NRG,0.00000000,200.00000000,388.00000000,,""13/01/2021 15:07:11"""\n`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 13, 15, 7, 11)));
    assert.deepEqual(wasm.transactions[0]?.note, 'Résiliation de contrat');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 200, 'tx have a to of quantity 200');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'NRG', 'transaction has a to of currency NRG');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction has no fee');
  });

  test('it should import exchange', async function (assert) {
    // Given
    const csv = new File(
      [
        `"Type,Client,Crypto/Pack,Debit,Credit,""Valeur euro"",tx_id,Date"\n` +
          `"Conversion,0000000,NRG,47.90804,0.00000000,91.50437147,45225,""13/01/2021 15:07:11"""\n` +
          `"Conversion,0000000,USDT,0.00000000,109.26351,91.50437147,45225,""13/01/2021 15:07:11"""\n` +
          `"Frais de conversion,0000000,USDT,3.2779,0.00000000,2.74513114,,""13/01/2021 15:07:11"""`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'tx is categorized as exchanges');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 13, 15, 7, 11)));
    assert.deepEqual(wasm.transactions[0]?.note, 'Conversion 45225; Conversion 45225; Frais de conversion');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 47.90804, 'tx have a from of quantity 47.90804');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'NRG', 'transaction has a from of currency NRG');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 109.26351, 'tx have a to of quantity 109.26351');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'USDT', 'transaction has a to of currency USDT');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'transaction has one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 3.2779, 'tx have a fee of quantity 3.2779');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'USDT', 'transaction has a fee of currency USDT');
  });

  test('it should import fiat deposit', async function (assert) {
    // Given
    const csv = new File(
      [
        `"Type,Client,Crypto/Pack,Debit,Credit,""Valeur euro"",tx_id,Date"\n` +
          `"Dépôt Euro,0000000,EUR,0.00000000,10.00000000,10.00000000,,""13/01/2021 15:07:11"""`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 13, 15, 7, 11)));
    assert.deepEqual(wasm.transactions[0]?.note, 'Dépôt Euro');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 10, 'tx have a to of quantity 10');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'EUR', 'transaction has a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction has no fee');
  });

  test('it should import masternode rewarks', async function (assert) {
    // Given
    const csv = new File(
      [
        `"Type,Client,Crypto/Pack,Debit,Credit,""Valeur euro"",tx_id,Date"\n` +
          `"Frais de service,0000000,NRG,0.0092116,0.00000000,,,""13/01/2021 15:07:11"""\n` +
          `"Revenus,0000000,NRG,0.00000000,0.17501804,,,""13/01/2021 15:07:11"""`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.MINING, 'tx is categorized as mining');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 13, 15, 7, 11)));
    assert.deepEqual(wasm.transactions[0]?.note, 'Frais de service; Revenus');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.17501804, 'tx have a to of quantity 0.17501804');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'NRG', 'transaction has a to of currency NRG');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'transaction has one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.0092116, 'tx have a fee of quantity 0.0092116');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'NRG', 'transaction has a fee of currency NRG');
  });

  test('it should import withdrawal', async function (assert) {
    // Given
    const csv = new File(
      [
        `"Type,Client,Crypto/Pack,Debit,Credit,""Valeur euro"",tx_id,Date"\n` +
          `"Retraits,0000000,NRG,200.041,0.00000000,328.95763680,,""13/01/2021 15:07:11"""\n` +
          `"Frais de retrait,0000000,NRG,0.02000000,0.00000000,0.03288887,,""13/01/2021 15:07:11"""`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.WITHDRAWALS, 'tx is categorized as withdrawal');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 13, 15, 7, 11)));
    assert.deepEqual(wasm.transactions[0]?.note, 'Retraits; Frais de retrait');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 200.041, 'tx have a from of quantity 200.041');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'NRG', 'transaction has a from of currency NRG');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'transaction has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'transaction has one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.02, 'tx have a fee of quantity 0.02');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'NRG', 'transaction has a fee of currency NRG');
  });
});
