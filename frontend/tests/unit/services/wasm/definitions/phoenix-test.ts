import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | phoenix', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Phoenix', 'Phoenix');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import incoming transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Amount Millisatoshi,Fees Millisatoshi,Amount Fiat,Fees Fiat,Context,Description,Notes\n` +
          `2024-01-01T20:46:07.000Z,1250002000,-4998000,700.5663 EU,-2.8011 EUR,Swap-in with inputs: [0000000000000000000000000000000000000000000000000000000000000000],Dépôt on-chain,\n` +
          `2024-01-02T12:09:42.000Z,335693000,0,199.9998 EUR,0.0000 EUR,Incoming LN payment,,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two txs');

    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2024, 0, 1, 20, 46, 7)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DEPOSITS, 'tx1 is categorized as deposit');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx1 has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx1 has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.01255, 'tx1 has a to of quantity 0.01255');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx1 has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx1 has one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.00004998, 'tx1 has a fee of quantity 0.00004998');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'BTC', 'tx1 has a fee of currency BTC');

    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2024, 0, 2, 12, 9, 42)));
    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.DEPOSITS, 'tx2 is categorized as deposit');
    assert.strictEqual(wasm.transactions[1]?.from.length, 0, 'tx2 has no from');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx2 has one to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 0.00335693, 'tx2 have a to of quantity 0.00335693');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'BTC', 'tx2 has a to of currency BTC');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx2 has no fee');
  });

  test('it should import outgoing transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Amount Millisatoshi,Fees Millisatoshi,Amount Fiat,Fees Fiat,Context,Description,Notes\n` +
          `2024-01-01T20:47:15.000Z,-5000,-4000,-1.0059 EUR,-0.0065 EUR,Outgoing LN payment to 000000000000000000000000000000000000000000000000000000000000000000,,\n` +
          `2024-01-02T17:08:18.000Z,-335386000,-2820000,-87.4600 EUR,-0.7353 EUR,Swap-out to bc1q00000000000000000000000000000000000000,Swap-out vers bc1q00000000000000000000000000000000000000,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two txs');

    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2024, 0, 1, 20, 47, 15)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.WITHDRAWALS, 'tx1 is categorized as withdrawal');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx1 has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 0.00000001, 'tx1 have a from of quantity 0.00000001');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'tx1 has a from of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx1 has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx1 has one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.00000004, 'tx1 have a fee of quantity 0.0000004');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'BTC', 'tx1 has a to of currency BTC');

    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2024, 0, 2, 17, 8, 18)));
    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.WITHDRAWALS, 'tx2 is categorized as withdrawal');
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx2 has one from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 0.00332566, 'tx2 have a from of quantity 0.00332566');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'BTC', 'tx2 has a from of currency BTC');
    assert.strictEqual(wasm.transactions[1]?.to.length, 0, 'tx2 has no to');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 1, 'tx1 has one fee');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.quantity, 0.0000282, 'tx1 have a fee of quantity 0.0000282');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.currency, 'BTC', 'tx1 has a to of currency BTC');
  });
});
