import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | swissborg', function (hooks) {
  setupWasmDefinitionTest(hooks, 'SwissBorg_App', 'SwissBorg');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import cashin', async function (assert) {
    // Given
    const csv = new File(
      [
        `Local time,Time in UTC,Type,Currency,Gross amount,Gross amount (EUR),Fee,Fee (EUR),Net amount,Net amount (EUR),Note\n` +
          `2021-07-17 10:00:00,2021-07-17 08:00:00,Sell,EUR,170.9,170.9,0.,0.,170.9,170.9,Exchanged to 225.917197 XRP. The fees for the exchange was taken in the bought currency\n` +
          `2021-07-17 10:00:00,2021-07-17 08:00:00,Buy,XRP,228.19919,170.9,2.28,1.71,225.917198,169.19,Exchanged from 170.89 EUR`,
      ],
      'swissborg.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_IN, 'transaction is deposit');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 6, 17, 8, 0, 0)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 170.9, 'tx have a to of quantity 170.9');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'EUR', 'exchange have a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'exchange has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 228.197198, 'tx have a to of quantity 228.197198');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'XRP', 'exchange have a to of currency XRP');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'exchange has one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 2.28, 'tx have a to of quantity 2.28');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'XRP', 'exchange have a to of currency XRP');
  });

  test('it should import referral', async function (assert) {
    // Given
    const csv = new File(
      [
        `Local time,Time in UTC,Type,Currency,Gross amount,Gross amount (EUR),Fee,Fee (EUR),Net amount,Net amount (EUR),Note\n` +
          `2021-01-12 02:02:03,2021-01-12 00:02:03,Earnings,BTC,0.0012107,12.,0.,0.,0.0012107,20.,`,
      ],
      'swissborg.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.REFERRALS, 'transaction is referrals');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx have no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'exchange have one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.0012107, 'tx have a to of quantity 0.0012107');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'exchange have a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'exchange have no fee');
  });

  test('it should import yields earning', async function (assert) {
    // Given
    const csv = new File(
      [
        `Local time,Time in UTC,Type,Currency,Gross amount,Gross amount (EUR),Fee,Fee (EUR),Net amount,Net amount (EUR),Note\n` +
          `2021-07-17 10:00:00,2021-07-17 08:00:00,Earnings,USDT,11.4235,9.69,5.7118,4.84,5.7118,4.84,Yield earnings`,
      ],
      'swissborg.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.INTERESTS, 'transaction is interests');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 6, 17, 8, 0, 0)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx have no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'exchange have one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 11.4236, 'tx have a to of quantity 11.4236');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'USDT', 'exchange have a to of currency USDT');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'exchange have one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 5.7118, 'tx have a fee of quantity 5.7118');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'USDT', 'exchange have a fee of currency USDT');
  });

  test('it should import withdrawal', async function (assert) {
    // Given
    const csv = new File(
      [
        `Local time,Time in UTC,Type,Currency,Gross amount,Gross amount (EUR),Fee,Fee (EUR),Net amount,Net amount (EUR),Note\n` +
          `2021-07-17 10:00:00,2021-07-17 08:00:00,Withdrawal,BTC,0.011224,111.33,0.0004,3.97,0.010824,107.36,`,
      ],
      'swissborg.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.WITHDRAWALS, 'tx type is withdrawal');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 6, 17, 8, 0, 0)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx have one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 0.010824, 'tx have a from of quantity 0.010824');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'exchange have a from of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'exchange have no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'exchange have one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.0004, 'tx have a to of quantity 0.0004');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'BTC', 'exchange have a to of currency BTC');
  });

  test('it should import deposit', async function (assert) {
    // Given
    const csv = new File(
      [
        `Local time,Time in UTC,Type,Currency,Gross amount,Gross amount (EUR),Fee,Fee (EUR),Net amount,Net amount (EUR),Note\n` +
          `2021-07-17 10:00:00,2021-07-17 08:00:00,Deposit,BTC,0.01001337,99.41,0,0,0.01001337,99.41,`,
      ],
      'swissborg.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DEPOSITS, 'transaction is deposit');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 6, 17, 8, 0, 0)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx have no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'exchange have one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.01001337, 'tx have a to of quantity 0.01001337');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'exchange have a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'exchange have no fee');
  });
});
