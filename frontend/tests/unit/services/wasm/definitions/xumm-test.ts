import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | xumm', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Xumm_Wallet', 'Xumm');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import deposit transactions', async function (assert) {
    // Given
    const csv = new File(
      [
        `tx_type,direction,amount,date,currency,is_fee,fee,hash\n` +
          `AccountDelete,received,45.104,2022-01-16T00:13:37.000Z,XRP,0,0,TXHASH1\n` +
          `Payment,received,45.704,2022-01-16T00:13:38.000Z,XRP,0,0,TXHASH2`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two tx');

    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 0, 16, 0, 13, 37)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DEPOSITS, 'tx is categorized as deposit');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 45.104, 'tx have a to of quantity 45.704');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'XRP', 'tx has a to of currency XRP');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');

    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2022, 0, 16, 0, 13, 38)));
    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.DEPOSITS, 'tx is categorized as deposit');
    assert.strictEqual(wasm.transactions[1]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 45.704, 'tx have a to of quantity 45.704');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'XRP', 'tx has a to of currency XRP');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import exchanges transactions', async function (assert) {
    // Given
    const csv = new File(
      [
        `tx_type,direction,amount,date,currency,is_fee,fee,hash\n` +
          `OfferCreate,sent,-123.598,2022-01-16T00:13:37.000Z,XRP,0,-0.000012,TXHASH1\n` +
          `OfferCreate,received,40.990243,2022-01-16T00:13:37.000Z,SGB,0,0,TXHASH1\n` +
          `Payment,other,658.797267,2022-01-16T00:13:38.000Z,GRIME,0,0,TXHASH2\n` +
          `Payment,other,-9.881959,2022-01-16T00:13:38.000Z,XRP,0,0,TXHASH2`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two tx');

    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 0, 16, 0, 13, 37)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'tx1 is categorized as exchange');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx1 has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 123.598, 'tx1 have a from of quantity 123.598');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'XRP', 'tx1 has a from of currency XRP');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx1 has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 40.990243, 'tx1 have a to of quantity 40.990243');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'SGB', 'tx1 has a to of currency SGB');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx1 has one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.000012, 'tx1 have a to of quantity 0.000012');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'XRP', 'tx1 has a to of currency XRP');

    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2022, 0, 16, 0, 13, 38)));
    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.EXCHANGES, 'tx2 is categorized as exchange');
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx2 has one from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 9.881959, 'tx2 have a from of quantity 9.881959');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'XRP', 'tx2 has a from of currency XRP');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx2 has one to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 658.797267, 'tx2 have a to of quantity 658.797267');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'GRIME', 'tx2 has a to of currency GRIME');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx2 has no fee');
  });

  test('it should import fees transactions', async function (assert) {
    // Given
    const csv = new File(
      [
        `tx_type,direction,amount,date,currency,is_fee,fee,hash\n` +
          `AccountSet,sent,0,2022-01-16T00:13:37.000Z,XRP,1,-0.000012,TXHASH1\n` +
          `NFTokenMint,sent,0,2022-01-16T00:13:38.000Z,XRP,1,-0.000012,TXHASH2\n` +
          `OfferCancel,sent,0,2022-01-16T00:13:39.000Z,XRP,1,-0.000012,TXHASH3\n` +
          `SetRegularKey,sent,0,2022-01-16T00:13:40.000Z,XRP,1,-0.000012,TXHASH4\n` +
          `TrustSet,sent,0,2022-01-16T00:13:41.000Z,XRP,1,-0.000012,TXHASH5`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 5, 'has five txs');

    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 0, 16, 0, 13, 37)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.FEES, 'tx1 is categorized as fees');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx1 has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx1 has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx1 has one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.000012, 'tx1 have a to of quantity 0.000012');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'XRP', 'tx1 has a to of currency XRP');

    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2022, 0, 16, 0, 13, 38)));
    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.FEES, 'tx2 is categorized as fees');
    assert.strictEqual(wasm.transactions[1]?.from.length, 0, 'tx2 has no from');
    assert.strictEqual(wasm.transactions[1]?.to.length, 0, 'tx2 has no to');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 1, 'tx2 has one fee');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.quantity, 0.000012, 'tx2 have a to of quantity 0.000012');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.currency, 'XRP', 'tx2 has a to of currency XRP');

    assert.deepEqual(wasm.transactions[2]?.date, new Date(Date.UTC(2022, 0, 16, 0, 13, 39)));
    assert.strictEqual(wasm.transactions[2]?.category, TxCategory.FEES, 'tx3 is categorized as fees');
    assert.strictEqual(wasm.transactions[2]?.from.length, 0, 'tx3 has no from');
    assert.strictEqual(wasm.transactions[2]?.to.length, 0, 'tx3 has no to');
    assert.strictEqual(wasm.transactions[2]?.fee.length, 1, 'tx3 has one fee');
    assert.strictEqual(wasm.transactions[2]?.fee[0]?.quantity, 0.000012, 'tx3 have a to of quantity 0.000012');
    assert.strictEqual(wasm.transactions[2]?.fee[0]?.currency, 'XRP', 'tx3 has a to of currency XRP');

    assert.deepEqual(wasm.transactions[3]?.date, new Date(Date.UTC(2022, 0, 16, 0, 13, 40)));
    assert.strictEqual(wasm.transactions[3]?.category, TxCategory.FEES, 'tx4 is categorized as fees');
    assert.strictEqual(wasm.transactions[3]?.from.length, 0, 'tx4 has no from');
    assert.strictEqual(wasm.transactions[3]?.to.length, 0, 'tx4 has no to');
    assert.strictEqual(wasm.transactions[3]?.fee.length, 1, 'tx4 has one fee');
    assert.strictEqual(wasm.transactions[3]?.fee[0]?.quantity, 0.000012, 'tx4 have a to of quantity 0.000012');
    assert.strictEqual(wasm.transactions[3]?.fee[0]?.currency, 'XRP', 'tx4 has a to of currency XRP');

    assert.deepEqual(wasm.transactions[4]?.date, new Date(Date.UTC(2022, 0, 16, 0, 13, 41)));
    assert.strictEqual(wasm.transactions[4]?.category, TxCategory.FEES, 'tx5 is categorized as fees');
    assert.strictEqual(wasm.transactions[4]?.from.length, 0, 'tx5 has no from');
    assert.strictEqual(wasm.transactions[4]?.to.length, 0, 'tx5 has no to');
    assert.strictEqual(wasm.transactions[4]?.fee.length, 1, 'tx5 has one fee');
    assert.strictEqual(wasm.transactions[4]?.fee[0]?.quantity, 0.000012, 'tx5 have a to of quantity 0.000012');
    assert.strictEqual(wasm.transactions[4]?.fee[0]?.currency, 'XRP', 'tx5 has a to of currency XRP');
  });

  test('it should import withdrawal transactions', async function (assert) {
    // Given
    const csv = new File(
      [
        `tx_type,direction,amount,date,currency,is_fee,fee,hash\n` +
          `Payment,sent,-35.703988,2022-01-16T00:13:37.000Z,XRP,0,-0.000012,TXHASH`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');

    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2022, 0, 16, 0, 13, 37)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.WITHDRAWALS, 'tx is categorized as withdrawal');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 35.703988, 'tx have a to of quantity 35.703988');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'XRP', 'tx has a to of currency XRP');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx has one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.000012, 'tx have a to of quantity 0.000012');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'XRP', 'tx has a to of currency XRP');
  });
});
