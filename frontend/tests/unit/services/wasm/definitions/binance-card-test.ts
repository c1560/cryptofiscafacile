import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | binance-card', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Binance_Card', 'Binance');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import binance card cashout', async function (assert) {
    // Given
    const csv = new File(
      [
        `Timestamp,Description,Paid OUT (EUR),Paid IN (EUR),Transaction Fee (EUR),Assets Used,Exchange Rates\n` +
          `Fri Oct 23 11:14:59 UTC 2020,Depense bidon,17.10,,0.10,EUR 0.86; SXP 0.21918620; BNB 0.61800000,1.00 EUR = 1.02693597 SXP; 1.00 EUR = 0.03856098 BNB\n` +
          `Fri Oct 23 11:14:59 UTC 2020,Depense bidon1,0.86,,0.00,BNB 0.03400000,1.00 EUR = 0.03953488 BNB\n`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two txs');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_OUT, 'tx is categorized as cashout');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2020, 9, 23, 11, 14, 59)));
    assert.strictEqual(wasm.transactions[0]?.note, 'Depense bidon', 'tx has a note');
    assert.strictEqual(wasm.transactions[0]?.from.length, 2, 'tx has two from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 0.2191862, 'tx has a from of quantity 0.2191862');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'SXP', 'tx has a from of currency SXP');
    assert.strictEqual(wasm.transactions[0]?.from[1]?.quantity, 0.618, 'tx has a from of quantity 0.61800000');
    assert.strictEqual(wasm.transactions[0]?.from[1]?.currency, 'BNB', 'tx has a from of currency BNB');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 16.14, 'tx has a to of quantity 16.14');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'EUR', 'tx has a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.CASH_OUT, 'tx is categorized as cashout');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2020, 9, 23, 11, 14, 59)));
    assert.strictEqual(wasm.transactions[1]?.note, 'Depense bidon1', 'tx has a note');
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 0.034, 'tx has a from of quantity 0.034');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'BNB', 'tx has a from of currency BNB');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 0.86, 'tx has a to of quantity 0.86');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'EUR', 'tx has a to of currency EUR');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx has no fee');
  });

  test('it should not import eur only transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Timestamp,Description,Paid OUT (EUR),Paid IN (EUR),Transaction Fee (EUR),Assets Used,Exchange Rates\n` +
          `Fri Oct 23 11:14:59 UTC 2020,Depense bidon,17.10,,0.10,EUR 17.10,\n`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 0, 'has no txs');
    assert.strictEqual((wasm.history ?? [])[0]?.unknowns.length, 0, 'has no unknows');
  });
});
