import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | kucoin-withdrawals', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Kucoin_Withdrawals', 'Kucoin');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import deposits', async function (assert) {
    // Given
    const csv = new File(
      [
        `Time,Coin,Amount,Type,Wallet Address,Remark\n` +
          `2021-01-02 01:02:03,BTC,20.12345678,Transfer Via Blockchain,00000000000000000000000000000000,`,
      ],
      'kucoin-withdrawals.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(
      wasm.transactions[0]?.category,
      TxCategory.WITHDRAWALS,
      'transaction is categorized as withdrawals',
    );
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 2, 1, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'transaction has one from');
    assert.strictEqual(
      wasm.transactions[0]?.from[0]?.quantity,
      20.12345678,
      'transaction have a to of quantity 20.12345678',
    );
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'transaction has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'transaction has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction has no fee');
  });
});
