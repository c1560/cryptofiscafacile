import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | ftx', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Ftx', 'Ftx');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import buy transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `ID,Time,Market,Side,Order Type,Size,Price,Total,Fee,Fee Currency\n` +
          `8256648829,2021-05-12T01:02:03.000000+00:00,BTC/USDT,buy,Limit,9.6441,76409,97.2,1.36e-2,BTC`,
      ],
      'ftx.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'transaction is categorized as exchanges');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 4, 12, 1, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'transaction have a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 97.2, 'transaction have a from of 97.2');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'transaction have a from of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 9.6441, 'transaction have a to of quantity 9.6441');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'USDT', 'transaction have a to of currency USDT');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'transaction have a fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.0136, 'transaction have a fee of quantity 0.0136');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'BTC', 'transaction have a fee of currency BTC');
  });

  test('it should import sell transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `ID,Time,Market,Side,Order Type,Size,Price,Total,Fee,Fee Currency\n` +
          `0502831247,2021-05-12T01:02:03.000000+00:00,BTC/USDT,sell,Limit,9.6441,76409,97.2,1.36e-2,BTC`,
      ],
      'ftx.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'transaction is categorized as exchanges');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 4, 12, 1, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'transaction have a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 9.6441, 'transaction have a from of 9.6441');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'USDT', 'transaction have a from of currency USDT');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 97.2, 'transaction have a to of quantity 97.2');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'transaction have a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'transaction have a fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.0136, 'transaction have a fee of quantity 0.0136');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'BTC', 'transaction have a fee of currency BTC');
  });
});
