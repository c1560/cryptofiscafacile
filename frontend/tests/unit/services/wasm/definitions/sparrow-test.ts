import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | sparrow', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Sparrow_Wallet', 'Sparrow');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import deposit transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Label,Value,Balance,Txid\n` +
          `2021-12-13 13:37,,1437414,1437414,0000000000000000000000000000000000000000000000000000000000000000\n` +
          `2021-12-13 13:37,,1.017824,1.017824,0000000000000000000000000000000000000000000000000000000000000000`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two txs');

    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 11, 13, 13, 37, 0)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DEPOSITS, 'tx1 is categorized as deposit');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx1 has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx1 has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.01437414, 'tx1 have a to of quantity 0.01437414');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx1 has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx1 has no fee');

    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 11, 13, 13, 37, 0)));
    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.DEPOSITS, 'tx2 is categorized as deposit');
    assert.strictEqual(wasm.transactions[1]?.from.length, 0, 'tx2 has no from');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx2 has one to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 1.017824, 'tx2 have a to of quantity 1.017824');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'BTC', 'tx2 has a to of currency BTC');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx2 has no fee');
  });

  test('it should import withdrawal transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Label,Value,Balance,Txid\n` +
          `2021-12-13 13:37,,-1437414,0,0000000000000000000000000000000000000000000000000000000000000000\n` +
          `2021-12-13 13:37,,-1.017824,-1.017824,0000000000000000000000000000000000000000000000000000000000000000`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two txs');

    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 11, 13, 13, 37, 0)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.WITHDRAWALS, 'tx1 is categorized as withdrawal');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx1 has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 0.01437414, 'tx1 have a to of quantity 0.01437414');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'tx1 has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx1 has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx1 has no fee');

    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 11, 13, 13, 37, 0)));
    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.WITHDRAWALS, 'tx2 is categorized as withdrawal');
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx2 has one from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 1.017824, 'tx2 have a to of quantity 1.017824');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'BTC', 'tx2 has a to of currency BTC');
    assert.strictEqual(wasm.transactions[1]?.to.length, 0, 'tx2 has no to');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx2 has no fee');
  });
});
