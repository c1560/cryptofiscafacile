import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | stackinsat', function (hooks) {
  setupWasmDefinitionTest(hooks, 'StackinSat', 'StackinSat');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import cashin and withdrawal transactions', async function (assert) {
    // Given
    const csv = new File(
      [
        `bitcoinSavingPlanName,bitcoinSavingPlanIcon,purchaseOrderType,purchaseOrderFrequency,amountEur,transactionType,sendingDate,deliveryDate,transactionHash,marginEur,amountBtc,miningFeesBtc,price,withdrawOrderId\n` +
          `"Satoshis billionaire",beach_access,PUNCTUAL,,500.0,DELIVERY_PERSONAL_VAULT,"2021-01-12T09:08:18.000000Z",2022-01-04T03:46:28Z,"0000000000000000000000000000000000000000000000000000000000000000",5.0,0.01191,0.0000048,41549.99996366998,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 12, 9, 8, 18)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_IN, 'tx1 is categorized as exchange');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx1 has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 500, 'tx1 have a to of quantity 500');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'EUR', 'tx1 has a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx1 has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.0119148, 'tx1 have a to of quantity 0.0119148');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx1 has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx1 has no fee');

    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 0, 12, 9, 8, 18)));
    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.WITHDRAWALS, 'tx2 is a withdrawal');
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx2 has one from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 0.01191, 'tx2 have a from of quantity 0.01191');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'BTC', 'tx2 has a from of currency BTC');
    assert.strictEqual(wasm.transactions[1]?.to.length, 0, 'tx2 has no to');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 1, 'tx2 has one fee');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.quantity, 0.0000048, 'tx2 has a from of quantity 0.0000048');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.currency, 'BTC', 'tx2 has a from of currency BTC');
  });

  test('it should import two tx the same day', async function (assert) {
    // Given
    const csv = new File(
      [
        `bitcoinSavingPlanName,bitcoinSavingPlanIcon,purchaseOrderType,purchaseOrderFrequency,amountEur,transactionType,sendingDate,deliveryDate,transactionHash,marginEur,amountBtc,miningFeesBtc,price,withdrawOrderId\n` +
          `"Satoshis billionaire",beach_access,PUNCTUAL,,500.0,DELIVERY_PERSONAL_VAULT,"2021-01-12T09:08:18.000000Z",2022-01-04T03:46:28Z,"0000000000000000000000000000000000000000000000000000000000000000",5.0,0.01191,0.0000048,41549.99996366998,\n` +
          `"Satoshis billionaire",beach_access,RECURRING,WEEKLY,500.0,DELIVERY_PERSONAL_VAULT,"2021-01-12T09:08:18.000000Z",2022-01-04T03:46:28Z,"0000000000000000000000000000000000000000000000000000000000000000",5.0,0.01191,0.0000048,44000,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 4, 'has four txs');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_IN, 'tx1 is categorized as exchange');
    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.WITHDRAWALS, 'tx2 is a withdrawal');
    assert.strictEqual(wasm.transactions[2]?.category, TxCategory.CASH_IN, 'tx3 is categorized as exchange');
    assert.strictEqual(wasm.transactions[3]?.category, TxCategory.WITHDRAWALS, 'tx4 is a withdrawal');
  });

  test('it should not put same id for cahsin and withdrawal txs', async function (assert) {
    // Given
    const csv = new File(
      [
        `bitcoinSavingPlanName,bitcoinSavingPlanIcon,purchaseOrderType,purchaseOrderFrequency,amountEur,transactionType,sendingDate,deliveryDate,transactionHash,marginEur,amountBtc,miningFeesBtc,price,withdrawOrderId\n` +
          `"Satoshis billionaire",beach_access,STACKINSAT_GIFT,,15.0,DELIVERY_PERSONAL_VAULT,"2021-01-12T09:08:18.000000Z",2022-01-04T03:46:28Z,"0000000000000000000000000000000000000000000000000000000000000000",5.0,0.01191,0.0000048,41549.99996366998,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two txs');
    assert.notStrictEqual(wasm.transactions[0]?.id, wasm.transactions[1]?.id, 'tx1 id != tx2 id');
  });
});
