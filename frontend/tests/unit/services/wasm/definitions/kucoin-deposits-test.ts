import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | kucoin-deposits', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Kucoin_Deposits', 'Kucoin');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import deposits', async function (assert) {
    // Given
    const csv = new File(
      [`Time,Coin,Amount,Type,Remark\n` + `2021-04-01 01:02:03,ETH,2.12345678,Transfer Via Blockchain,Deposit`],
      'kucoin-deposits.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DEPOSITS, 'transaction is categorized as deposits');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 3, 1, 1, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has a to');
    assert.strictEqual(
      wasm.transactions[0]?.to[0]?.quantity,
      2.12345678,
      'transaction have a to of quantity 2.12345678',
    );
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'transaction has a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction has no fee');
  });
});
