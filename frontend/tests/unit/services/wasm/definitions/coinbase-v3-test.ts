import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | coinbase-v3', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Coinbase_v3', 'Coinbase');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import convert transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Timestamp,Transaction Type,Asset,Quantity Transacted,Spot Price Currency,Spot Price at Transaction,Subtotal,Total (inclusive of fees and/or spread),Fees and/or Spread,Notes\n` +
          `2021-02-12T09:08:18Z,Convert,BTC,4.27,EUR,0.580000,2.44,2.47,0.030000,"Converted 4,27 BTC to 0,00167499 ETH"\n` +
          `2021-02-12T09:08:18Z,Convert,BTC,9207024,EUR,3.83,68.04,15.25,1.48,"Converted 9 207 024 BTC to 2,935124 USDT"\n` +
          `2021-02-12T09:08:18Z,Convert,USDT,02.93,EUR,3.83,68.04,15.25,1.48,"Converted 02,93 USDT to 7 024,72 ETH"`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 3, 'has three tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 1, 12, 9, 8, 18)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'tx1 is categorized as exchange');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx1 has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 4.27, 'tx1 have a to of quantity 4.27');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'tx1 has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx1 has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.00167499, 'tx1 have a to of quantity 0.00167499');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'tx1 has a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx1 has no fee');

    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx2 has one from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 9207024, 'tx2 have a from of quantity 9207024');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'BTC', 'tx2 has a from of currency BTC');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx2 has a to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 2.935124, 'tx2 have a to of quantity 2.935124');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'USDT', 'tx2 has a to of currency USDT');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx2 has no fee');

    assert.strictEqual(wasm.transactions[2]?.from.length, 1, 'tx3 has one from');
    assert.strictEqual(wasm.transactions[2]?.from[0]?.quantity, 2.93, 'tx3 have a from of quantity 2.93');
    assert.strictEqual(wasm.transactions[2]?.from[0]?.currency, 'USDT', 'tx3 has a from of currency USDT');
    assert.strictEqual(wasm.transactions[2]?.to.length, 1, 'tx3 has a to');
    assert.strictEqual(wasm.transactions[2]?.to[0]?.quantity, 7024.72, 'tx3 have a to of quantity 7 024.72');
    assert.strictEqual(wasm.transactions[2]?.to[0]?.currency, 'ETH', 'tx3 has a to of currency ETH');
    assert.strictEqual(wasm.transactions[2]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import cashout transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Timestamp,Transaction Type,Asset,Quantity Transacted,Spot Price Currency,Spot Price at Transaction,Subtotal,Total (inclusive of fees and/or spread),Fees and/or Spread,Notes\n` +
          `2021-02-12T09:03:14Z,Sell,BTC,3456789,EUR,26774.48,4925.60,1254852,73.39,"Sold 3 456 789 BTC for 1 254 852 â‚¬ EUR"`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 1, 12, 9, 3, 14)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_OUT, 'tx is categorized as cashout');
    assert.strictEqual(wasm.transactions[0]?.note, 'Sold 3 456 789 BTC for 1 254 852 â‚¬ EUR');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 3456789, 'tx have a to of quantity 3456789');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'tx has a from of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 1254852, 'tx have a to of quantity 1254852');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'EUR', 'tx has a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import learning reward transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Timestamp,Transaction Type,Asset,Quantity Transacted,Spot Price Currency,Spot Price at Transaction,Subtotal,Total (inclusive of fees and/or spread),Fees and/or Spread,Notes\n` +
          `2021-02-12T09:03:14Z,Learning Reward,ETH,2.2783012,EUR,0.580000,0.820000,0.820000,0.00,"Received 2.2783012 ETH from Coinbase as a learning reward"`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 1, 12, 9, 3, 14)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.MINING, 'tx is categorized as mining');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 2.2783012, 'tx have a to of quantity 2.2783012');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'tx has a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });
});
