import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | bitpanda', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Bitpanda', 'Bitpanda');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import buy metal tx', async function (assert) {
    // Given
    const csv = new File(
      [
        `Transaction ID,Timestamp,Transaction Type,In/Out,Amount Fiat,Fiat,Amount Asset,Asset,Asset market price,Asset market price currency,Asset class,Product ID,Fee,Fee asset,Spread,Spread Currency\n` +
          `000000000-0000-0000-0000-000000000000,2021-01-01T01:02:03+01:00,buy,outgoing,50.00,EUR,1.02,Gold,48.87,EUR,Metal,28,-,-,-,-`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 1, 0, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 50, 'tx has a from of 50');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'EUR', 'tx has a from of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 1.02, 'tx has a to of quantity 1.02');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'GOLD', 'tx has a to of currency GOLD');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import buy stock tx', async function (assert) {
    // Given
    const csv = new File(
      [
        `Transaction ID,Timestamp,Transaction Type,In/Out,Amount Fiat,Fiat,Amount Asset,Asset,Asset market price,Asset market price currency,Asset class,Product ID,Fee,Fee asset,Spread,Spread Currency\n` +
          `000000000-0000-0000-0000-000000000000,2021-01-01T01:02:03+01:00,buy,outgoing,25.00,EUR,0.30,AAPL,103.82,EUR,"Stock (derivative)",75,-,-,0.03000000,EUR`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 1, 0, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 25.03, 'tx has a from of 25.03');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'EUR', 'tx has a from of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.3, 'tx has a to of quantity 0.30');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'AAPL', 'tx has a to of currency AAPL');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import cashin', async function (assert) {
    // Given
    const csv = new File(
      [
        `Transaction ID,Timestamp,Transaction Type,In/Out,Amount Fiat,Fiat,Amount Asset,Asset,Asset market price,Asset market price currency,Asset class,Product ID,Fee,Fee asset,Spread,Spread Currency\n` +
          `000000000-0000-0000-0000-000000000000,2021-01-01T01:02:03+01:00,buy,outgoing,50.00,EUR,0.01,ETH,3506.76,EUR,Cryptocurrency,5,-,-,-,-\n` +
          `000000000-0000-0000-0000-000000000001,2021-01-01T01:02:03+01:00,buy,outgoing,50.00,EUR,0.02,ETH,32.79,EUR,Fiat,56,-,-,-,-`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_IN, 'tx is categorized as cashin');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 1, 0, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 50, 'tx has a from of 50');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'EUR', 'tx has a from of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.01, 'tx has a to of quantity 0.01');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'tx has a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.CASH_IN, 'tx is categorized as cashin');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 0, 1, 0, 2, 3)));
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 50, 'tx has a from of 50');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'EUR', 'tx has a from of currency EUR');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 0.02, 'tx has a to of quantity 0.01');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'ETH', 'tx has a to of currency ETH');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import cashout', async function (assert) {
    // Given
    const csv = new File(
      [
        `Transaction ID,Timestamp,Transaction Type,In/Out,Amount Fiat,Fiat,Amount Asset,Asset,Asset market price,Asset market price currency,Asset class,Product ID,Fee,Fee asset,Spread,Spread Currency\n` +
          `000000000-0000-0000-0000-000000000000,2021-01-01T01:02:03+01:00,sell,incoming,0.69,EUR,0.02,BTC,24.00,EUR,Cryptocurrency,25,-,-,-,-\n` +
          `000000000-0000-0000-0000-000000000001,2021-01-01T01:02:03+01:00,sell,incoming,0.73,EUR,0.03,BTC,20.35,EUR,Fiat,56,-,-,-,-`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_OUT, 'tx is categorized as cashout');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 1, 0, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 0.02, 'tx has a from of quantity 0.02');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'tx has a from of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.69, 'tx has a to of 0.69');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'EUR', 'tx has a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.CASH_OUT, 'tx is categorized as cashout');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 0, 1, 0, 2, 3)));
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 0.03, 'tx has a from of quantity 0.03');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'BTC', 'tx has a from of currency ETH');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 0.73, 'tx has a to of 0.73');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'EUR', 'tx has a to of currency EUR');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import commercial rebate stock tx', async function (assert) {
    // Given
    const csv = new File(
      [
        `Transaction ID,Timestamp,Transaction Type,In/Out,Amount Fiat,Fiat,Amount Asset,Asset,Asset market price,Asset market price currency,Asset class,Product ID,Fee,Fee asset,Spread,Spread Currency\n` +
          `000000000-0000-0000-0000-000000000000,2021-01-01T01:02:03+01:00,transfer,incoming,5,EUR,0.04,AAPL,114.23,EUR,"Stock (derivative)",81,-,-,0.00000000,EUR`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 1, 0, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 5, 'tx has a to of quantity 5');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'EUR', 'tx has a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.04, 'tx has a to of quantity 0.04');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'AAPL', 'tx has a to of currency AAPL');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import commercial rebate crypto tx', async function (assert) {
    // Given
    const csv = new File(
      [
        `Transaction ID,Timestamp,Transaction Type,In/Out,Amount Fiat,Fiat,Amount Asset,Asset,Asset market price,Asset market price currency,Asset class,Product ID,Fee,Fee asset,Spread,Spread Currency\n` +
          `000000000-0000-0000-0000-000000000000,2021-01-01T01:02:03+01:00,transfer,incoming,0.00,EUR,0.1,ETH,0.80,EUR,Cryptocurrency,33,-,-,-,-`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.COMMERCIAL_REBATES, 'commercial rebates tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 1, 0, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.1, 'tx has a to of quantity 0.1');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'tx has a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import fiat deposit', async function (assert) {
    // Given
    const csv = new File(
      [
        `Transaction ID,Timestamp,Transaction Type,In/Out,Amount Fiat,Fiat,Amount Asset,Asset,Asset market price,Asset market price currency,Asset class,Product ID,Fee,Fee asset,Spread,Spread Currency\n` +
          `000000000-0000-0000-0000-000000000000,2021-01-01T01:02:03+01:00,deposit,incoming,500.00,EUR,-,EUR,-,-,Fiat,-,7.39000000,EUR,-,-`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 1, 0, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx have one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 492.61, 'tx have a to of quantity 492.61');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'EUR', 'tx have a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import fees', async function (assert) {
    // Given
    const csv = new File(
      [
        `Transaction ID,Timestamp,Transaction Type,In/Out,Amount Fiat,Fiat,Amount Asset,Asset,Asset market price,Asset market price currency,Asset class,Product ID,Fee,Fee asset,Spread,Spread Currency\n` +
          `000000000-0000-0000-0000-000000000000,2021-01-01T01:02:03+01:00,withdrawal,outgoing,0,EUR,0.00000000,BTC,0.00,-,Cryptocurrency,33,0.001,BTC,-,-`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.FEES, 'tx is categorized as fees');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 1, 0, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx has one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.001, 'tx have a to of quantity 0.001');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'BTC', 'tx have a to of currency BTC');
  });

  test('it should import sell stock tx', async function (assert) {
    // Given
    const csv = new File(
      [
        `Transaction ID,Timestamp,Transaction Type,In/Out,Amount Fiat,Fiat,Amount Asset,Asset,Asset market price,Asset market price currency,Asset class,Product ID,Fee,Fee asset,Spread,Spread Currency\n` +
          `000000000-0000-0000-0000-000000000000,2021-01-01T01:02:03+01:00,sell,incoming,52.46,EUR,0.018,AAPL,2793.18,EUR,"Stock (derivative)",73,-,-,0.05000000,EUR\n` +
          `000000000-0000-0000-0000-000000000001,2021-01-01T01:02:03+01:00,withdrawal,outgoing,0,EUR,0.00000000,BTC,0.00,-,Cryptocurrency,33,0.001,BTC,-,-`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 1, 0, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 0.018, 'tx has a from of 0.018');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'AAPL', 'tx has a from of currency AAPL');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 52.41, 'tx has a to of quantity 52.41');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'EUR', 'tx has a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.FEES, 'tx is categorized as fees');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 0, 1, 0, 2, 3)));
    assert.strictEqual(wasm.transactions[1]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[1]?.to.length, 0, 'tx has no to');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 1, 'tx has one fee');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.quantity, 0.001, 'tx have a to of quantity 0.001');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.currency, 'BTC', 'tx have a to of currency BTC');
  });

  test('it should not remove tx with differents id', async function (assert) {
    // Given
    const csv = new File(
      [
        `Transaction ID,Timestamp,Transaction Type,In/Out,Amount Fiat,Fiat,Amount Asset,Asset,Asset market price,Asset market price currency,Asset class,Product ID,Fee,Fee asset,Spread,Spread Currency\n` +
          `000000000-0000-0000-0000-000000000000,2021-01-01T01:02:03+01:00,withdrawal,outgoing,0,EUR,0.00000000,BEST,0.00,-,Cryptocurrency,33,0.00755081,BEST,-,-\n` +
          `000000000-0000-0000-0000-000000000001,2021-01-01T01:02:03+01:00,withdrawal,outgoing,0,EUR,0.00000000,BEST,0.00,-,Cryptocurrency,33,0.00755081,BEST,-,-`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two tx');
  });
});
