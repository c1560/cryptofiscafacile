import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | trastra', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Trastra', 'Trastra');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import buy transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Tx created UTC;Tx confirmed UTC;Tx Type;Reference;In (+);Out (-);Balance\n` +
          `31.03.2021 19:37;31.03.2021 19:38;Buy;Deposit 5.29870989 BTC to BTC Wallet;5.29870989;;31.28513001`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DEPOSITS, 'tx is categorized as deposit');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 2, 31, 19, 38)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 5.29870989, 'tx has a to of quantity 5.29870989');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import sell transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `Tx created UTC;Tx confirmed UTC;Tx Type;Reference;In (+);Out (-);Balance\n` +
          `31.03.2021 19:37;31.03.2021 19:38;Buy;Withdrawal 5.29870989 BTC from BTC Wallet;;5.29870989;31.28513001`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DEPOSITS, 'tx is categorized as deposit');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 2, 31, 19, 38)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 5.29870989, 'tx has a from of quantity 5.29870989');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'tx has a from of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });
});
