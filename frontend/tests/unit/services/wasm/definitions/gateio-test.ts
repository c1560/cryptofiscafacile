import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | gate.io', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Gateio', 'Gateio');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import buy transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `No	Time	Trade type	Pair	Price	Amount	Total	Fee\n` +
          `1	2021-03-31 19:38:22	Buy	SIS/USDT	1.345000000	88.38800000000	118.88186000000	0.176776 SIS`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'tx is categorized as exchanges');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 2, 31, 19, 38, 22)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 118.88186, 'tx has a from of 118.88186');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'USDT', 'tx has a from of currency SIS');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 88.388, 'tx has a to of quantity 88.388');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'SIS', 'tx has a to of currency USDT');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx has a fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.176776, 'tx has a fee of quantity 0.176776');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'SIS', 'tx has a fee of currency SIS');
  });

  test('it should import sell transaction', async function (assert) {
    // Given
    const csv = new File(
      [
        `No	Time	Trade type	Pair	Price	Amount	Total	Fee\n` +
          `10	2021-03-11 21:00:22	Sell	CSPR/USDT	0.060400000	235.80000000000	14.24232000000	0.02848464 USDT`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'tx is categorized as exchanges');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 2, 11, 21, 0, 22)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 235.8, 'tx has a from of 235.8');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'CSPR', 'tx has a from of currency CSPR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 14.24232, 'tx has a to of quantity 14.24232');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'USDT', 'tx has a to of currency USDT');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx has a fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.02848464, 'tx has a fee of quantity 0.02848464');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'USDT', 'tx has a fee of currency USDT');
  });
});
