import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | kucoin-account-details', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Kucoin_Account_Details', 'Kucoin');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import transfer', async function (assert) {
    // Given
    const csv = new File(
      [
        `Coin,Side,Type,Amount(Fee included),Fee,Time,Remark\n` +
          `BTC,Withdrawal,Transfer,0.12345678,0,2021/01/02 01:02:03,Within KuCoin Account\n` +
          `BTC,Deposit,Transfer,0.12345678,0,2021/01/02 01:02:03,Within KuCoin Account`,
      ],
      'kucoin-account-details.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'transaction is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 2, 1, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'transaction has one from');
    assert.strictEqual(
      wasm.transactions[0]?.from[0]?.quantity,
      0.12345678,
      'transaction have a from of quantity 0.12345678',
    );
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'transaction has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has one to');
    assert.strictEqual(
      wasm.transactions[0]?.to[0]?.quantity,
      0.12345678,
      'transaction have a to of quantity 0.12345678',
    );
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'transaction has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction has no fee');
  });

  test('it should import trades', async function (assert) {
    // Given
    const csv = new File(
      [
        `Coin,Side,Type,Amount(Fee included),Fee,Time,Remark\n` +
          `BTC,Withdrawal,TRADE_EXCHANGE,0.12345678,0.00012345678,2021/01/02 01:02:03,\n` +
          `ETH,Deposit,TRADE_EXCHANGE,100,0,2021/01/02 01:02:03,`,
      ],
      'kucoin-trades.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'transaction is categorized as exchange');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 2, 1, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'transaction has one from');
    assert.strictEqual(
      wasm.transactions[0]?.from[0]?.quantity,
      0.12345678,
      'transaction have a from of quantity 0.12345678',
    );
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'transaction has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 100, 'transaction have a to of quantity 100');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'transaction has a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'transaction has one fee');
    assert.strictEqual(
      wasm.transactions[0]?.fee[0]?.quantity,
      0.00012345678,
      'transaction have a fee of quantity 0.00012345678',
    );
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'BTC', 'transaction has a fee of currency BTC');
  });
});
