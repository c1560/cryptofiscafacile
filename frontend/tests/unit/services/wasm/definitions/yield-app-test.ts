import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | yield-app', function (hooks) {
  setupWasmDefinitionTest(hooks, 'YieldApp', 'YieldApp');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import crypto deposit', async function (assert) {
    // Given
    const csv = new File(
      [
        `TIMESTAMP,TYPE,AMT,USD,TICKER,PRICE,YLD,USDT,USDC,ETH,BTC,YLD,USDT,USDC,ETH,BTC\n` +
          `2021-01-01,DEPOSIT,500,500,BTC,1.00,0.00,500,0.00,0.00,0.00,0.00,500,0.00,0.00,0.00`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has two tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DEPOSITS, 'tx is categorized as deposit');
    assert.strictEqual(wasm.transactions[0]?.note, 'DEPOSIT', 'tx has a note');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 1, 0, 0, 0)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 500, 'tx has a from of quantity 500');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx has a from of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import transfer', async function (assert) {
    // Given
    const csv = new File(
      [
        `TIMESTAMP,TYPE,AMT,USD,TICKER,PRICE,YLD,USDT,USDC,ETH,BTC,YLD,USDT,USDC,ETH,BTC\n` +
          `2021-01-01,INVESTMENT,500,500,BTC,1.00,0.00,500,0.00,0.00,0.00,0.00,500,0.00,0.00,0.00`,
      ],
      'custom.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.strictEqual(wasm.transactions[0]?.note, 'INVESTMENT', 'tx has a note');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 0, 1, 0, 0, 0)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 500, 'tx has a from of quantity 500');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx has a from of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });
});
