import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | rayn', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Rayn', 'Rayn');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should ignore EUR buy rayn services', async function (assert) {
    // Given
    const csv = new File(
      [
        `type,date,volumeReceived,currencyReceived,volumeSent,currencySent,volumeFee,currencyFee,platform,description,label,operationRef,transactionHash,walletAddress,blockchain\n` +
          `Withdraw,2023-08-22T13:05:39.968Z,,,9.90,EUR,,,RAYN,,"Buy Rayn services","00000000-0000-0000-0000-000000000000",,,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2023, 7, 22, 13, 5, 39, 968)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 9.9, 'tx has a to of quantity 9.9');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'EUR', 'tx has a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should ignore EUR card payment', async function (assert) {
    // Given
    const csv = new File(
      [
        `type,date,volumeReceived,currencyReceived,volumeSent,currencySent,volumeFee,currencyFee,platform,description,label,operationRef,transactionHash,walletAddress,blockchain\n` +
          `Withdraw,2023-08-22T13:05:39.968Z,,,20.00,EUR,,,RAYN,"Card Payment to SUPER SHOP","Card Payment","00000000-0000-0000-0000-000000000000",,,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2023, 7, 22, 13, 5, 39, 968)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 20, 'tx has a to of quantity 9.9');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'EUR', 'tx has a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should ignore EUR deposit', async function (assert) {
    // Given
    const csv = new File(
      [
        `type,date,volumeReceived,currencyReceived,volumeSent,currencySent,volumeFee,currencyFee,platform,description,label,operationRef,transactionHash,walletAddress,blockchain\n` +
          `Deposit,2023-08-22T13:05:39Z,100.00,EUR,,,,,RAYN,"Transfer from JEAN","SEPA Transfer","00000000-0000-0000-0000-000000000000",,,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2023, 7, 22, 13, 5, 39)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 100, 'tx has a to of quantity 100');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'EUR', 'tx has a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should ignore staking', async function (assert) {
    // Given
    const csv = new File(
      [
        `type,date,volumeReceived,currencyReceived,volumeSent,currencySent,volumeFee,currencyFee,platform,description,label,operationRef,transactionHash,walletAddress,blockchain\n` +
          `Withdraw,2023-08-22T13:05:39.968Z,,,5133,AKTIO,,,RAYN,,Staking,"00000000-0000-0000-0000-000000000000",,,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2023, 7, 22, 13, 5, 39, 968)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is ignored');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 5133, 'tx has a to of quantity 5133');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'AKTIO', 'tx has a to of currency AKTIO');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import cashin', async function (assert) {
    // Given
    const csv = new File(
      [
        `type,date,volumeReceived,currencyReceived,volumeSent,currencySent,volumeFee,currencyFee,platform,description,label,operationRef,transactionHash,walletAddress,blockchain\n` +
          `Trade,2023-08-22T13:05:39.968Z,5079.6312,AKTIO,958.00,EUR,48.7193,AKTIO,RAYN,"From - To",Trade,"00000000-0000-0000-0000-000000000000",,,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2023, 7, 22, 13, 5, 39, 968)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_IN, 'tx is a cash in');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 958, 'tx has a to of quantity 958');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'EUR', 'tx has a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 5079.6312, 'tx has a to of quantity 5079.6312');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'AKTIO', 'tx has a to of currency AKTIO');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx has one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 48.7193, 'tx has a fee of quantity 48.7193');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'AKTIO', 'tx has a fee of currency AKTIO');
  });

  test('it should import interests', async function (assert) {
    // Given
    const csv = new File(
      [
        `type,date,volumeReceived,currencyReceived,volumeSent,currencySent,volumeFee,currencyFee,platform,description,label,operationRef,transactionHash,walletAddress,blockchain\n` +
          `Deposit,2023-08-22T13:05:39.968Z,0.4649,PAR,,,,,RAYN,,Interest,"00000000-0000-0000-0000-000000000000",,,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2023, 7, 22, 13, 5, 39, 968)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.INTERESTS, 'tx is a interets');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.4649, 'tx has a to of quantity 0.4649');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'PAR', 'tx has a to of currency PAR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import Referrer bonus', async function (assert) {
    // Given
    const csv = new File(
      [
        `type,date,volumeReceived,currencyReceived,volumeSent,currencySent,volumeFee,currencyFee,platform,description,label,operationRef,transactionHash,walletAddress,blockchain\n` +
          `Deposit,2023-08-22T13:05:39.968Z,48.5177,AKTIO,,,,,RAYN,,"Referrer bonus","00000000-0000-0000-0000-000000000000",,,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2023, 7, 22, 13, 5, 39, 968)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.REFERRALS, 'tx is a referral');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 48.5177, 'tx has a to of quantity 48.5177');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'AKTIO', 'tx has a to of currency AKTIO');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import Shared revenue from referrals', async function (assert) {
    // Given
    const csv = new File(
      [
        `type,date,volumeReceived,currencyReceived,volumeSent,currencySent,volumeFee,currencyFee,platform,description,label,operationRef,transactionHash,walletAddress,blockchain\n` +
          `Deposit,2023-08-22T13:05:39.968Z,0.2047,PAR,,,,,RAYN,,"Shared revenue from referrals","00000000-0000-0000-0000-000000000000",,,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2023, 7, 22, 13, 5, 39, 968)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.REFERRALS, 'tx is a referral');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.2047, 'tx has a to of quantity 0.2047');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'PAR', 'tx has a to of currency PAR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import welcome bonus', async function (assert) {
    // Given
    const csv = new File(
      [
        `type,date,volumeReceived,currencyReceived,volumeSent,currencySent,volumeFee,currencyFee,platform,description,label,operationRef,transactionHash,walletAddress,blockchain\n` +
          `Deposit,2023-08-22T13:05:39.968Z,53.73,AKTIO,,,,,RAYN,,"Welcome bonus","00000000-0000-0000-0000-000000000000",,,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2023, 7, 22, 13, 5, 39, 968)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.COMMERCIAL_REBATES, 'tx is a commercial rebates');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 53.73, 'tx has a to of quantity 53.73');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'AKTIO', 'tx has a to of currency AKTIO');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });
});
