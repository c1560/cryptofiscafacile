import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | binance-v2', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Binance_v2', 'Binance');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import airdrop assets', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `000000000,2021-06-12 17:01:02,Spot,Airdrop Assets,LUNA,0.00098304,`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    // airdrop assets can be a token rebranding, so we cannot apply airdrop automatically
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DEPOSITS, 'tx is a deposit');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.00098304, 'tx has a to of quantity 0.00098304');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'LUNA', 'tx has a to of currency LUNA');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import binance card cashback', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `000000000,2021-06-12 17:01:02,Funding,Binance Card Cashback,BNB,0.00098304,Binance Card`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.COMMERCIAL_REBATES, 'tx is a commercial rebate');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.00098304, 'tx has a to of quantity 0.00098304');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BNB', 'tx has a to of currency BNB');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import bnb deducts fee', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `000000000,2021-06-12 17:01:02,Spot,BNB deducts fee,BNB,-6.60679711,""\n` +
          `000000000,2021-06-12 17:01:03,IsolatedMargin,BNB deducts fee,AVAX,6.13850820,""`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two txs');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.FEES, 'tx is categorized as fee');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx has one fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 6.60679711, 'tx has a fee of quantity 6.60679711');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'BNB', 'tx has a fee of currency BNB');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.FEES, 'tx is categorized as fee');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 3)));
    assert.strictEqual(wasm.transactions[1]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[1]?.to.length, 0, 'tx has no to');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 1, 'tx has one fee');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.quantity, 6.1385082, 'tx has a fee of quantity 6.13850820');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.currency, 'AVAX', 'tx has a fee of currency AVAX');
  });

  test('it should import bnb vault rewards', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:02,Spot,BNB Vault Rewards,HFT,0.10223372,""\n`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.INTERESTS, 'transaction is categorized as interest');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction do not have a from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.10223372, 'tx has a to of quantity 0.10223372');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'HFT', 'transaction have a to of currency HFT');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction do not have a fee');
  });

  test('it should import buy crypto', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `"00000000","2021-06-12 17:01:01","Spot","Buy Crypto","EUR","14.71000000","N00000000000000000000000000"\n` +
          `"00000000","2021-06-12 17:01:02","Spot","Buy Crypto","EUR","-14.71000000","N00000000000000000000000000"\n` +
          `"00000000","2021-06-12 17:01:02","Spot","Buy Crypto","BNB","0.05530311","N00000000000000000000000000"\n`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two txs');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx1 is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 1)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx1 do not have a from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx1 has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 14.71, 'tx1 has a to of quantity 14.71');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'EUR', 'tx1 have a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx1 do not have a fee');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.CASH_IN, 'tx2 is categorized as cashin');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx2 do not have a from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 14.71, 'tx2 has a to of quantity 14.71');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'EUR', 'tx2 have a to of currency EUR');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx2 has a to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 0.05530311, 'tx2 has a to of quantity 0.05530311');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'BNB', 'tx2 have a to of currency BNB');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx2 do not have a fee');
  });

  test('it should import buy crypto with fiat', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `"00000000","2021-06-12 17:01:02","Spot","Buy Crypto With Fiat","BNB","0.0498","Ref - N00000000000000000000000000"\n`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DEPOSITS, 'tx is a deposit');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction do not have a from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.0498, 'tx has a to of quantity 0.0498');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BNB', 'transaction have a to of currency BNB');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction do not have a fee');
  });

  test('it should import cashback voucher', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:02,Spot,Cashback Voucher,USDT,3.6613,""\n`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.COMMERCIAL_REBATES, 'tx is a commercial rebate');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction do not have a from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 3.6613, 'tx has a to of quantity 3.6613');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'USDT', 'transaction have a to of currency USDT');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction do not have a fee');
  });

  test('it should import crypto box', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:02,Funding,Crypto Box,BUSD,8.8777,"Binance Pay"\n`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.COMMERCIAL_REBATES, 'tx is a commercial rebate');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction do not have a from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 8.8777, 'tx has a to of quantity 8.8777');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BUSD', 'transaction have a to of currency BUSD');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction do not have a fee');
  });

  test('it should import exchange Binance Convert', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `000000000,2021-11-26 08:26:58,SPOT,Binance Convert,BETH,-0.18300000,""\n` +
          `000000000,2021-11-26 08:26:58,SPOT,Binance Convert,BNB,0.99650896,""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'exchange have a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 0.183, 'exchange have a from of 0.183');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BETH', 'exchange have a from of currency BETH');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'exchange have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.99650896, 'exchange have a to of quantity 0.99650896');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BNB', 'exchange have a to of currency BNB');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'exchange have no fee');
  });

  test('it should import exchange Buy | Buy', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `000000000,2021-11-26 08:26:58,Spot,Buy,USDT,-318.09000000,""\n` +
          `000000000,2021-11-26 08:26:58,Spot,Buy,ETH,158.54000000,""\n` +
          `000000000,2021-11-26 08:26:58,Spot,Fee,BTC,-0.00041685,""\n`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'exchange have a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 318.09, 'exchange have a from of 318.09');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'USDT', 'exchange have a from of currency USDT');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'exchange have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 158.54, 'exchange have a to of quantity 158.54');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'exchange have a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'exchange have a fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.00041685, 'tx have a fee of quantity 0.00041685');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'BTC', 'exchange have a fee of currency BTC');
  });

  test('it should import exchange Buy | Transaction Related', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `000000000,2021-05-08 17:22:45,Spot,Transaction Related,BTC,-0.02922786,""\n` +
          `000000000,2021-05-08 17:22:45,Spot,Fee,ETH,-1.10000000,""\n` +
          `000000000,2021-05-08 17:22:45,Spot,Buy,ETH,4574.00000000,""\n`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one exchange');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'exchange have a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 0.02922786, 'exchange have a from of 0.02922786');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'exchange have a from of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'exchange have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 4574, 'exchange have a to of quantity 4574');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'exchange have a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'exchange have a fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 1.1, 'exchange have a fee of quantity 1.1');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'ETH', 'exchange have a fee of currency ETH');
  });

  test('it should import exchange Large OTC trading | Large OTC trading', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `000000000,2021-11-26 08:26:58,Spot,Large OTC trading,USDT,-318.09000000,""\n` +
          `000000000,2021-11-26 08:26:58,Spot,Large OTC trading,BTC,158.54000000,""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'exchange have a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 318.09, 'exchange have a from of 318.09');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'USDT', 'exchange have a from of currency USDT');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'exchange have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 158.54, 'exchange have a to of quantity 158.54');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'exchange have a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'exchange have no fee');
  });

  test('it should import exchange Sell | Sell', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `000000000,2021-12-03 01:51:28,Spot,Fee,BTC,-0.00001905,""\n` +
          `000000000,2021-12-03 01:51:28,Spot,Sell,ETH,-244.00000000,""\n` +
          `000000000,2021-12-03 01:51:28,Spot,Sell,BTC,0.01905152,""\n`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one exchange');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'exchange have a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 244, 'exchange have a from of 244');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'ETH', 'exchange have a from of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'exchange have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.01905152, 'exchange have a to of quantity 0.01905152');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'exchange have a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'exchange have a fee');
    assert.strictEqual(
      wasm.transactions[0]?.fee[0]?.quantity,
      0.00001905,
      'exchange have a fee of quantity 0.00001905',
    );
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'BTC', 'exchange have a fee of currency BTC');
  });

  test('it should import exchange Stablecoins Auto-Conversion', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `000000000,2021-11-26 08:26:58,Spot,Stablecoins Auto-Conversion,BUSD,200.123456,""\n` +
          `000000000,2021-11-26 08:26:58,Spot,Stablecoins Auto-Conversion,USDC,-200.123456,""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx have a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 200.123456, 'tx have a from of 200.123456');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'USDC', 'tx have a from of currency USDC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 200.123456, 'tx have a to of quantity 200.123456');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BUSD', 'tx have a to of currency BUSD');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx have no fee');
  });

  test('it should import exchange Small Assets Exchange BNB', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `000000000,2021-11-26 08:26:58,SPOT,Small Assets Exchange BNB,BNB,0.00027824,""\n` +
          `000000000,2021-11-26 08:26:58,SPOT,Small Assets Exchange BNB,BUSD,-0.10000000,""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'exchange have a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 0.1, 'exchange have a from of 0.1');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BUSD', 'exchange have a from of currency BUSD');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'exchange have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.00027824, 'exchange have a to of quantity 0.00027824');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BNB', 'exchange have a to of currency BNB');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'exchange have no fee');
  });

  test('it should import exchange Transaction Buy | Transaction Spend', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `000000000,2021-05-08 17:22:45,SPOT,Transaction Buy,LUNA,167.43800000,""\n` +
          `000000000,2021-05-08 17:22:45,SPOT,Transaction Spend,ETH,-0.06362644,""\n` +
          `000000000,2021-05-08 17:22:45,SPOT,Fee,LUNA,-0.16743800,""\n` +
          `000000000,2021-05-08 17:22:45,SPOT,Referral Commission,LUNA,0.01674380,""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'tx1 is categorized as exchange');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx1 have a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 0.06362644, 'tx1 have a from of 0.06362644');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'ETH', 'tx1 have a from of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx1 has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 167.438, 'tx1 have a to of quantity 167.438');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'LUNA', 'tx1 have a to of currency LUNA');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx1 have a fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.167438, 'tx1 have a fee of quantity 0.167438');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'LUNA', 'tx1 have a fee of currency BTC');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.COMMERCIAL_REBATES, 'tx2 is a commercial rebates');
    assert.strictEqual(wasm.transactions[1]?.from.length, 0, 'tx2 has no from');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx2 has one to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 0.0167438, 'tx2 have a to of quantity 0.01674380');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'LUNA', 'tx2 have a to of currency LUNA');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx2 has no fee');
  });

  test('it should import exchange Transaction Revenue | Transaction Sold', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `000000000,2021-05-08 17:22:45,SPOT,Fee,BTC,-0.00012518,""\n` +
          `000000000,2021-05-08 17:22:45,SPOT,Transaction Revenue,BTC,0.12517863,""\n` +
          `000000000,2021-05-08 17:22:45,SPOT,Transaction Sold,ETH,-1.68850000,""\n` +
          `000000000,2021-05-09 17:22:45,SPOT,Transaction Fee,BTC,-0.00012518,""\n` +
          `000000000,2021-05-09 17:22:45,SPOT,Transaction Revenue,BTC,0.12517863,""\n` +
          `000000000,2021-05-09 17:22:45,SPOT,Transaction Sold,ETH,-1.68850000,""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'transaction is categorized as exchange');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx have a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 1.6885, 'tx have a from of 1.6885');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'ETH', 'tx have a from of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.12517863, 'tx have a to of quantity 0.12517863');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx have a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx have a fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.00012518, 'tx have a fee of quantity 0.00012518');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'BTC', 'tx have a fee of currency BTC');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.EXCHANGES, 'transaction is categorized as exchange');
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx have a from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 1.6885, 'tx have a from of 1.6885');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'ETH', 'tx have a from of currency ETH');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 0.12517863, 'tx have a to of quantity 0.12517863');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'BTC', 'tx have a to of currency BTC');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 1, 'tx have a fee');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.quantity, 0.00012518, 'tx have a fee of quantity 0.00012518');
    assert.strictEqual(wasm.transactions[1]?.fee[0]?.currency, 'BTC', 'tx have a fee of currency BTC');
  });

  test('it should import fiat deposit', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `000000000,2021-12-03 01:51:28,Spot,Deposit,EUR,300.00000000,""\n` +
          `000000000,2021-12-03 01:51:29,Spot,Fiat Deposit,EUR,400.00000000,""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'transaction is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 11, 3, 1, 51, 28)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction have no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 300, 'transaction have a from of 300');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'EUR', 'transaction have a from of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction have no fee');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.DONT_CARE, 'transaction is categorized as ignored');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 11, 3, 1, 51, 29)));
    assert.strictEqual(wasm.transactions[1]?.from.length, 0, 'transaction have no from');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'transaction have a to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 400, 'transaction have a from of 300');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'EUR', 'transaction have a from of currency EUR');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'transaction have no fee');
  });

  test('it should import crypto deposit', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `000000000,2021-12-03 01:51:28,Spot,Deposit,ETH,1.99926500,""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DEPOSITS, 'transaction is categorized as deposits');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 11, 3, 1, 51, 28)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction have no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 1.999265, 'transaction have a from of 1.99926500');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'transaction have a from of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction have no fee');
  });

  test('it should import cashin', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `000000000,2021-12-03 01:51:28,Spot,Transaction Related,ETH,1.43300000,""\n` +
          `000000000,2021-12-03 01:51:28,Spot,Transaction Related,EUR,-50.00000000,""\n` +
          `000000000,2021-02-02 11:52:06,Spot,Broker Withdraw,BTC,302.00000000,Deposit fee is included\n` +
          `000000000,2021-02-02 11:52:06,Spot,Broker Withdraw,EUR,-100.00000000,Deposit fee is included\n`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_IN, 'transaction is categorized as cashin');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 11, 3, 1, 51, 28)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'transaction have a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 50, 'transaction have a from of 50');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'EUR', 'transaction have a from of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 1.433, 'transaction have a to of quantity 1.433');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'transaction have a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction have no fee');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.CASH_IN, 'transaction is categorized as cashin');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 1, 2, 11, 52, 6)));
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'transaction have a from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 100, 'transaction have a from of 100');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'EUR', 'transaction have a from of currency EUR');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'transaction have a to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 302, 'transaction have a to of quantity 302');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'BTC', 'transaction have a to of currency BTC');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'transaction have no fee');
  });

  test('it should import binance rewards distribution', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `000000000,2021-07-21 01:02:03,Spot,Rewards Distribution,ETH,9.53153307,""\n`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.MINING, 'transaction is categorized as mining');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 6, 21, 1, 2, 3)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction do not have a from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction have a to');
    assert.strictEqual(
      wasm.transactions[0]?.to[0]?.quantity,
      9.53153307,
      'transaction have a to of quantity 9.53153307',
    );
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'transaction have a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction do not have a fee');
  });

  test('it should import future referral rewards', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:02,USDT-Futures,Referee rebates,USDT,8.87778051,""\n` +
          `00000000,2021-06-12 17:01:03,USDT-Futures,Referrer rebates,USDT,0.01245227,""`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.REFERRALS, 'tx is categorized as referral');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx have no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 8.87778051, 'tx have a to of 8.87778051');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'USDT', 'tx have a to of currency USDT');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx have no fee');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.REFERRALS, 'tx is categorized as referral');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 3)));
    assert.strictEqual(wasm.transactions[1]?.from.length, 0, 'tx have no from');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx have a to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 0.01245227, 'tx have a to of 0.01245227');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'USDT', 'tx have a to of currency USDT');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx have no fee');
  });

  test('it should import launchpad subcription', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `000000000,2021-06-12 17:01:02,Spot,Launchpad subscribe,BNB,-8.34454671,""\n` +
          `000000000,2021-06-12 17:10:02,Spot,Launchpad subscribe,BNB,8.34454676,""`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two txs');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx have a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 8.34454671, 'tx have a from of 8.34454671');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BNB', 'tx have a from of currency BNB');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx have no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx have no fee');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 5, 12, 17, 10, 2)));
    assert.strictEqual(wasm.transactions[1]?.from.length, 0, 'tx have no from');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx have no to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 8.34454676, 'tx have a to of 8.34454676');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'BNB', 'tx have a to of currency BNB');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx have no fee');
  });

  test('it should import launchpad token distribution', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `000000000,2021-06-12 17:01:02,Spot,Launchpad token distribution,LINA,2.98709891,""`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.MINING, 'tx is categorized as mining');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx have no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 2.98709891, 'tx have a to of 2.98709891');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'LINA', 'tx have a to of currency LINA');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx have no fee');
  });

  test('it should import launchpool earnings withdrawal', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `000000000,2021-06-12 17:01:02,Spot,Launchpool Earnings Withdrawal,NFP,58.0758,"Binance Launchpool"`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.MINING, 'tx is categorized as mining');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx have no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 58.0758, 'tx have a to of 58.0758');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'NFP', 'tx have a to of currency NFP');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx have no fee');
  });

  test('it should import binance pool distribution', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:02,Pool,Pool Distribution,ETH,7.28138869,""\n`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.MINING, 'transaction is categorized as mining');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction do not have a from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction have a to');
    assert.strictEqual(
      wasm.transactions[0]?.to[0]?.quantity,
      7.28138869,
      'transaction have a to of quantity 7.28138869',
    );
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'transaction have a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction do not have a fee');
  });

  test('it should import binance card spending', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2020-10-22 21:07:58,Card,Binance Card Spending,EUR,0.86000000,""\n` +
          `00000000,2020-10-22 21:07:58,Card,Binance Card Spending,BNB,-0.03400000,""\n` +
          `00000000,2020-10-22 22:07:58,Card,Binance Card Spending,EUR,0.86000000,""\n` +
          `00000000,2020-10-22 23:07:58,Card,Binance Card Spending,EUR,-0.86000000,""\n` +
          `00000000,2020-10-23 11:15:00,Card,Binance Card Spending,BNB,-0.61800000,""\n` +
          `00000000,2020-10-23 11:15:00,Card,Binance Card Spending,EUR,-0.86000000,""\n` +
          `00000000,2020-10-23 11:15:00,Card,Binance Card Spending,SXP,-0.21918620,""\n`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 7, 'has seven transactions');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2020, 9, 22, 21, 7, 58)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'transaction is categorized as ignored');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.86, 'transaction have a to of quantity 0.86');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'EUR', 'transaction have a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction do not have a fee');

    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2020, 9, 22, 21, 7, 58)));
    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.WITHDRAWALS, 'tx is categorized as withdrawal');
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 0.034, 'transaction have a to of quantity 0.034');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'BNB', 'transaction have a to of currency BNB');
    assert.strictEqual(wasm.transactions[1]?.to.length, 0, 'tx has no to');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'transaction do not have a fee');

    assert.deepEqual(wasm.transactions[2]?.date, new Date(Date.UTC(2020, 9, 22, 22, 7, 58)));
    assert.strictEqual(wasm.transactions[2]?.category, TxCategory.DONT_CARE, 'tx3 is categorized as ignored');
    assert.strictEqual(wasm.transactions[2]?.from.length, 0, 'tx3 has no from');
    assert.strictEqual(wasm.transactions[2]?.to.length, 1, 'tx3 has one to');
    assert.strictEqual(wasm.transactions[2]?.to[0]?.quantity, 0.86, 'tx3 have a to of quantity 0.86');
    assert.strictEqual(wasm.transactions[2]?.to[0]?.currency, 'EUR', 'tx3 have a to of currency EUR');
    assert.strictEqual(wasm.transactions[2]?.fee.length, 0, 'tx3 do not have a fee');

    assert.deepEqual(wasm.transactions[3]?.date, new Date(Date.UTC(2020, 9, 22, 23, 7, 58)));
    assert.strictEqual(wasm.transactions[3]?.category, TxCategory.DONT_CARE, 'tx4 is categorized as ignored');
    assert.strictEqual(wasm.transactions[3]?.from.length, 1, 'tx4 has one from');
    assert.strictEqual(wasm.transactions[3]?.from[0]?.quantity, 0.86, 'tx4 have a from of quantity 0.86');
    assert.strictEqual(wasm.transactions[3]?.from[0]?.currency, 'EUR', 'tx4 have a from of currency EUR');
    assert.strictEqual(wasm.transactions[3]?.to.length, 0, 'tx4 has no to');
    assert.strictEqual(wasm.transactions[3]?.fee.length, 0, 'tx4 do not have a fee');

    assert.deepEqual(wasm.transactions[4]?.date, new Date(Date.UTC(2020, 9, 23, 11, 15, 0)));
    assert.strictEqual(wasm.transactions[4]?.category, TxCategory.WITHDRAWALS, 'tx5 is categorized as ignored');
    assert.strictEqual(wasm.transactions[4]?.from.length, 1, 'tx5 has one from');
    assert.strictEqual(wasm.transactions[4]?.from[0]?.quantity, 0.618, 'tx5 have a to of quantity 0.618');
    assert.strictEqual(wasm.transactions[4]?.from[0]?.currency, 'BNB', 'tx5 have a to of currency BNB');
    assert.strictEqual(wasm.transactions[4]?.to.length, 0, 'tx5 has no to');
    assert.strictEqual(wasm.transactions[4]?.fee.length, 0, 'transaction do not have a fee');

    assert.deepEqual(wasm.transactions[5]?.date, new Date(Date.UTC(2020, 9, 23, 11, 15, 0)));
    assert.strictEqual(wasm.transactions[5]?.category, TxCategory.DONT_CARE, 'tx6 is categorized as ignored');
    assert.strictEqual(wasm.transactions[5]?.from.length, 1, 'tx6 has one from');
    assert.strictEqual(wasm.transactions[5]?.from[0]?.quantity, 0.86, 'tx6 have a to of quantity 0.86');
    assert.strictEqual(wasm.transactions[5]?.from[0]?.currency, 'EUR', 'tx6 have a to of currency EUR');
    assert.strictEqual(wasm.transactions[5]?.to.length, 0, 'tx6 has no to');
    assert.strictEqual(wasm.transactions[5]?.fee.length, 0, 'tx6 do not have a fee');

    assert.deepEqual(wasm.transactions[6]?.date, new Date(Date.UTC(2020, 9, 23, 11, 15, 0)));
    assert.strictEqual(wasm.transactions[6]?.category, TxCategory.WITHDRAWALS, 'tx7 is categorized as ignored');
    assert.strictEqual(wasm.transactions[6]?.from.length, 1, 'tx7 has one from');
    assert.strictEqual(wasm.transactions[6]?.from[0]?.quantity, 0.2191862, 'tx7 have a to of quantity 0.21918620');
    assert.strictEqual(wasm.transactions[6]?.from[0]?.currency, 'SXP', 'tx7 have a to of currency SXP');
    assert.strictEqual(wasm.transactions[6]?.to.length, 0, 'tx7 has no to');
    assert.strictEqual(wasm.transactions[6]?.fee.length, 0, 'tx7 do not have a fee');
  });

  test('it should import send', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:02,Funding,Send,USDT,-3.2050,"Binance Pay - P_0000000000000000"\n`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.WITHDRAWALS, 'tx is categorized as withdrawal');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'transaction has a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 3.205, 'transaction have a to of quantity 3.205');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'USDT', 'transaction have a to of currency USDT');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'transaction has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction do not have a fee');
  });

  test('it should import mission reward distribution', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `"00000000","2021-06-12 17:01:02","SPOT","Mission Reward Distribution","BUSD","0.10000000",""\n`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.COMMERCIAL_REBATES, 'tx is a commercial rebate');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction do not have a from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.1, 'tx has a to of quantity 0.1');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BUSD', 'transaction have a to of currency BUSD');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction do not have a fee');
  });

  test('it should import simple earn flexible airdrop', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:02,SPOT,Simple Earn Flexible Airdrop,VTHO,2.91545442,""\n`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.AIR_DROPS, 'transaction is categorized as airdrop');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction do not have a from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 2.91545442, 'tx has a to of quantity 2.91545442');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'VTHO', 'transaction have a to of currency VTHO');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction do not have a fee');
  });

  test('it should import simple earn flexible interest', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:02,Earn,Simple Earn Flexible Interest,BTC,0.00000208,""\n`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.INTERESTS, 'transaction is categorized as interest');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction do not have a from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.00000208, 'tx has a to of quantity 0.00000208');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'transaction have a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction do not have a fee');
  });

  test('it should import simple earn locked rewards', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:02,Spot,Simple Earn Locked Rewards,BNB,0.00008082,""\n`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.INTERESTS, 'transaction is categorized as interest');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction do not have a from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.00008082, 'tx has a to of quantity 0.00008082');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BNB', 'transaction have a to of currency BNB');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction do not have a fee');
  });

  test('it should import stacking rewards rewards', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:02,Spot,Staking Rewards,BNB,0.00012315,""\n`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.MINING, 'transaction is categorized as mining');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'transaction do not have a from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'transaction has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.00012315, 'tx has a to of quantity 0.00012315');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BNB', 'transaction have a to of currency BNB');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'transaction do not have a fee');
  });

  test('it should import token rebranding/swap', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `000000000,2021-12-23 17:12:08,Spot,Distribution,OOKI,32500.72300000,""\n` +
          `000000000,2021-12-27 08:32:33,Spot,Distribution,BZRX,-3250.07230000,""\n` +
          `"000000000","2021-12-24 17:12:08","Spot","Airdrop Assets","OOKI","32500.72300000",""\n` +
          `"000000000","2021-12-28 08:32:33","Spot","Distribution","BZRX","-3250.07230000","BERX deduction"\n` +
          `"000000000","2022-04-30 12:39:38","Spot","Distribution","REI","4861.22500000","GXS to REI"\n` +
          `"000000000","2022-05-10 07:48:08","Spot","Token Swap - Redenomination/Rebranding","GXS","-486.12250000",""\n` +
          `"000000000","2023-10-25 16:01:42","Spot","Asset Recovery","EUR","-0.02933700",""\n` +
          `"000000000","2023-10-25 16:48:50","Spot","Distribution","USDT","0.03105322","Token swap distribution"\n` +
          `"000000000","2023-10-26 16:01:42","Spot","Asset Recovery","MC","-9.751",""\n` +
          `"000000000","2023-10-26 16:48:50","Spot","Token Swap - Distribution","BEAMX","56.14367",""`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 9, 'has nine txs');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DEPOSITS, 'tx1 is a deposit');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 11, 23, 17, 12, 8)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx1 has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx1 has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 32500.723, 'tx1 has a to of quantity 32500.723');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'OOKI', 'tx1 has a to of currency OOKI');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx1 has no fee');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.WITHDRAWALS, 'tx2 is a withdrawal');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 11, 27, 8, 32, 33)));
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx2 has one from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 3250.0723, 'tx2 has a from of quantity 3250.0723');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'BZRX', 'tx2 has a from of currency BZRX');
    assert.strictEqual(wasm.transactions[1]?.to.length, 0, 'tx2 has no to');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx2 has no fee');

    assert.strictEqual(wasm.transactions[2]?.category, TxCategory.DEPOSITS, 'tx3 is a deposit');
    assert.deepEqual(wasm.transactions[2]?.date, new Date(Date.UTC(2021, 11, 24, 17, 12, 8)));
    assert.strictEqual(wasm.transactions[2]?.from.length, 0, 'tx3 has no from');
    assert.strictEqual(wasm.transactions[2]?.to.length, 1, 'tx3 has one to');
    assert.strictEqual(wasm.transactions[2]?.to[0]?.quantity, 32500.723, 'tx3 has a to of quantity 32500.723');
    assert.strictEqual(wasm.transactions[2]?.to[0]?.currency, 'OOKI', 'tx3 has a to of currency OOKI');
    assert.strictEqual(wasm.transactions[2]?.fee.length, 0, 'tx3 has no fee');

    assert.strictEqual(wasm.transactions[3]?.category, TxCategory.WITHDRAWALS, 'tx4 is a withdrawal');
    assert.deepEqual(wasm.transactions[3]?.date, new Date(Date.UTC(2021, 11, 28, 8, 32, 33)));
    assert.strictEqual(wasm.transactions[3]?.from.length, 1, 'tx4 has one from');
    assert.strictEqual(wasm.transactions[3]?.from[0]?.quantity, 3250.0723, 'tx4 has a from of quantity 3250.0723');
    assert.strictEqual(wasm.transactions[3]?.from[0]?.currency, 'BZRX', 'tx4 has a from of currency BZRX');
    assert.strictEqual(wasm.transactions[3]?.to.length, 0, 'tx4 has no to');
    assert.strictEqual(wasm.transactions[3]?.fee.length, 0, 'tx4 has no fee');

    assert.strictEqual(wasm.transactions[4]?.category, TxCategory.DEPOSITS, 'tx5 is a deposit');
    assert.deepEqual(wasm.transactions[4]?.date, new Date(Date.UTC(2022, 3, 30, 12, 39, 38)));
    assert.strictEqual(wasm.transactions[4]?.from.length, 0, 'tx5 has no from');
    assert.strictEqual(wasm.transactions[4]?.to.length, 1, 'tx5 has one to');
    assert.strictEqual(wasm.transactions[4]?.to[0]?.quantity, 4861.225, 'tx5 has a to of quantity 4861.225');
    assert.strictEqual(wasm.transactions[4]?.to[0]?.currency, 'REI', 'tx5 has a to of currency REI');
    assert.strictEqual(wasm.transactions[4]?.fee.length, 0, 'tx5 has no fee');

    assert.strictEqual(wasm.transactions[5]?.category, TxCategory.WITHDRAWALS, 'tx6 is a withdrawal');
    assert.deepEqual(wasm.transactions[5]?.date, new Date(Date.UTC(2022, 4, 10, 7, 48, 8)));
    assert.strictEqual(wasm.transactions[5]?.from.length, 1, 'tx6 has one from');
    assert.strictEqual(wasm.transactions[5]?.from[0]?.quantity, 486.1225, 'tx6 has a from of quantity 486.1225');
    assert.strictEqual(wasm.transactions[5]?.from[0]?.currency, 'GXS', 'tx6 has a from of currency GXS');
    assert.strictEqual(wasm.transactions[5]?.to.length, 0, 'tx6 has no to');
    assert.strictEqual(wasm.transactions[5]?.fee.length, 0, 'tx6 has no fee');

    assert.strictEqual(wasm.transactions[6]?.category, TxCategory.DEPOSITS, 'tx7 is a deposit');
    assert.deepEqual(wasm.transactions[6]?.date, new Date(Date.UTC(2023, 9, 25, 16, 48, 50)));
    assert.strictEqual(wasm.transactions[6]?.from.length, 0, 'tx7 has no from');
    assert.strictEqual(wasm.transactions[6]?.to.length, 1, 'tx7 has one to');
    assert.strictEqual(wasm.transactions[6]?.to[0]?.quantity, 0.03105322, 'tx7 has a from of quantity 0.03105322');
    assert.strictEqual(wasm.transactions[6]?.to[0]?.currency, 'USDT', 'tx7 has a from of currency USDT');
    assert.strictEqual(wasm.transactions[6]?.fee.length, 0, 'tx7 has no fee');

    assert.strictEqual(wasm.transactions[7]?.category, TxCategory.WITHDRAWALS, 'tx8 is a withdrawal');
    assert.deepEqual(wasm.transactions[7]?.date, new Date(Date.UTC(2023, 9, 26, 16, 1, 42)));
    assert.strictEqual(wasm.transactions[7]?.from.length, 1, 'tx8 has one from');
    assert.strictEqual(wasm.transactions[7]?.from[0]?.quantity, 9.751, 'tx8 has a from of quantity 9.751');
    assert.strictEqual(wasm.transactions[7]?.from[0]?.currency, 'MC', 'tx8 has a from of currency MC');
    assert.strictEqual(wasm.transactions[7]?.to.length, 0, 'tx8 has no to');
    assert.strictEqual(wasm.transactions[7]?.fee.length, 0, 'tx8 has no fee');

    assert.strictEqual(wasm.transactions[8]?.category, TxCategory.DEPOSITS, 'tx9 is a deposit');
    assert.deepEqual(wasm.transactions[8]?.date, new Date(Date.UTC(2023, 9, 26, 16, 48, 50)));
    assert.strictEqual(wasm.transactions[8]?.from.length, 0, 'tx9 has no from');
    assert.strictEqual(wasm.transactions[8]?.to.length, 1, 'tx9 has one to');
    assert.strictEqual(wasm.transactions[8]?.to[0]?.quantity, 56.14367, 'tx9 has a to of quantity 56.14367');
    assert.strictEqual(wasm.transactions[8]?.to[0]?.currency, 'BEAMX', 'tx9 has a from of currency BEAMX');
    assert.strictEqual(wasm.transactions[8]?.fee.length, 0, 'tx9 has no fee');
  });

  test('it should ignore binance savings distribution', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:02,Savings,Savings purchase,USDT,-166.58524800,""\n` +
          `00000000,2021-06-12 17:01:02,Spot,Savings distribution,LDUSDT,166.58524800,""\n` +
          `00000000,2021-06-12 17:01:03,EARN,Simple Earn Flexible Subscription,USDT,-166.58524800,""\n` +
          `00000000,2021-06-12 17:01:03,Spot,Savings Distribution,LDUSDT,166.58524800,""\n`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two transactions');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx1 is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx1 has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 166.585248, 'tx1 has a to of quantity 166.585248');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'USDT', 'tx1 has a to of currency USDT');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx1 has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 166.585248, 'tx1 has a to of quantity 166.585248');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'USDT', 'tx1 has a to of currency USDT');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx1 do not have a fee');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.DONT_CARE, 'tx2 is categorized as ignored');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 3)));
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx2 has one from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 166.585248, 'tx2 has a to of quantity 166.585248');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'USDT', 'tx2 has a to of currency USDT');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx2 has a to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 166.585248, 'tx2 has a to of quantity 166.585248');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'USDT', 'tx2 has a to of currency USDT');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx2 do not have a fee');
  });

  test('it should ignore binance Savings Principal redemption', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:02,Savings,Savings Principal redemption,USDT,166.58524800,""\n` +
          `00000000,2021-06-12 17:01:02,Savings,Savings Principal redemption,LDUSDT,-166.58524800,""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 166.585248, 'tx has a to of quantity 166.585248');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'USDT', 'tx has a to of currency USDT');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 166.585248, 'tx has a to of quantity 166.585248');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'USDT', 'tx has a to of currency USDT');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx do not have a fee');
  });

  test('it should ignore binance Savings purchase', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:02,Savings,Savings purchase,LDLDO,0.00194000,""\n` +
          `00000000,2021-06-12 17:01:02,Savings,Savings purchase,LDO,-0.00194000,""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 0.00194, 'tx has a to of quantity 0.00194');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'LDO', 'tx has a to of currency LDO');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.00194, 'tx has a to of quantity 0.00194');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'LDO', 'tx has a to of currency LDO');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx do not have a fee');
  });

  test('it should ignore simple earn flexible redemption', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:04,Earn,Simple Earn Flexible Redemption,LDBTC,-0.01000000,""\n` +
          `00000000,2021-06-12 17:01:04,Earn,Simple Earn Flexible Redemption,BTC,0.01000000,""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 4)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 0.01, 'tx has a to of quantity 0.01000000');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'tx has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.01, 'tx has a to of quantity 0.01000000');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx do not have a fee');
  });

  test('it should ignore simple earn locked redemption', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:04,Earn,Simple Earn Locked Redemption,MATIC,4.5094000,""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 4)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 4.5094, 'tx has a to of quantity 4.5094');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'MATIC', 'tx has a to of currency MATIC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx do not have a fee');
  });

  test('it should ignore simple earn flexible subscription', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:02,Earn,Simple Earn Flexible Subscription,BTC,-0.03619745,""\n` +
          `00000000,2021-06-12 17:01:02,Spot,Savings distribution,LDBTC,0.03619745,""\n` +
          `00000000,2021-06-12 17:01:04,Earn,Simple Earn Flexible Subscription,LDBTC,0.00000194,""\n` +
          `00000000,2021-06-12 17:01:04,Earn,Simple Earn Flexible Subscription,BTC,-0.00000194,""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two transactions');

    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx1 is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 2)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx1 has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 0.03619745, 'tx1 has a to of quantity 0.03619745');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'tx1 has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx1 has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.03619745, 'tx1 has a to of quantity 0.03619745');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx1 has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx1 do not have a fee');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.DONT_CARE, 'tx2 is categorized as ignored');
    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 4)));
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx2 has one from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 0.00000194, 'tx2 has a to of quantity 0.00000194');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'BTC', 'tx2 has a to of currency BTC');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx2 has a to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 0.00000194, 'tx2 has a to of quantity 0.00000194');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'BTC', 'tx2 has a to of currency BTC');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx2 do not have a fee');
  });

  test('it should ignore staking redemption', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:04,Spot,Staking Redemption,DASH,1.01000000,""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one transaction');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'tx is categorized as ignored');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 5, 12, 17, 1, 4)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 1.01, 'tx has a to of quantity 1.01');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'DASH', 'tx has a to of currency DASH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx do not have a fee');
  });

  test('it should not show main and funding account transfer', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:04,Spot,Main and funding account transfer,BTC,-0.00100000,""\n` +
          `00000000,2021-06-12 17:01:05,Spot,Main and Funding Account Transfer,BTC,-0.00100000,""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 0, 'has no transaction');
    assert.strictEqual(wasm.history?.[0]?.unknowns.length, 0, 'has no unknows transactions');
  });

  test('it should not show Transfer between Main Account/Futures to Margin Account', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `"00000000","2021-06-12 17:01:04","Spot","Transfer Between Main Account/Futures and Margin Account","BTC","-0.00100000",""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 0, 'has no transaction');
    assert.strictEqual(wasm.history?.[0]?.unknowns.length, 0, 'has no unknows transactions');
  });

  test('it should not show Transfer from Main Account/Futures to Margin Account', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:04,Spot,Transfer from Main Account/Futures to Margin Account,BTC,-0.00100000,""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 0, 'has no transaction');
    assert.strictEqual(wasm.history?.[0]?.unknowns.length, 0, 'has no unknows transactions');
  });

  test('it should not show Transfer from Margin Account to Main Account/Futures', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:04,Spot,Transfer from Margin Account to Main Account/Futures,BTC,-0.00100000,""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 0, 'has no transaction');
    assert.strictEqual(wasm.history?.[0]?.unknowns.length, 0, 'has no unknows transactions');
  });

  test('it should not show Transfer Between Main and Funding Wallet', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:04,Funding,Transfer Between Main and Funding Wallet,BUSD,-0.25943200,""\n` +
          `00000000,2021-06-12 17:01:04,SPOT,Transfer Between Main and Funding Wallet,BUSD,0.25943200,""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 0, 'has no transaction');
    assert.strictEqual(wasm.history?.[0]?.unknowns.length, 0, 'has no unknows transactions');
  });

  test('it should not show simple earn locked subscription', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:04,Spot,Simple Earn Locked Subscription,BNB,-0.50000000,""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 0, 'has no transaction');
    assert.strictEqual(wasm.history?.[0]?.unknowns.length, 0, 'has no unknows transactions');
  });

  test('it should not show staking purchase', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-06-12 17:01:04,Spot,Staking Purchase,BNB,-0.50000000,""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 0, 'has no transaction');
    assert.strictEqual(wasm.history?.[0]?.unknowns.length, 0, 'has no unknows transactions');
  });

  test('it should merge exchange not on the same second', async function (assert) {
    // Given
    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `00000000,2021-04-28 17:59:49,Spot,Large OTC trading,EUR,-50.00000000,""\n` +
          `00000000,2021-04-28 17:59:50,Spot,Large OTC trading,USDT,60.37150000,""\n` +
          `00000000,2021-04-28 18:59:49,Spot,Large OTC trading,USDT,-50.00000000,""\n` +
          `00000000,2021-04-28 18:59:50,Spot,Large OTC trading,EUR,60.37150000,""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two txs');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_IN, 'tx1 is categorized as cash in');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx1 have a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 50, 'tx1 have a from of 50');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'EUR', 'tx1 have a from of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx1 have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 60.3715, 'tx1 have a to of quantity 60.3715');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'USDT', 'tx1 have a to of currency USDT');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx1 has no fee');

    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.CASH_OUT, 'tx2 is categorized as cash out');
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx2 have a from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 50, 'tx2 have a from of 50');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'USDT', 'tx2 have a from of currency USDT');
    assert.strictEqual(wasm.transactions[1]?.to.length, 1, 'tx2 have a to');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.quantity, 60.3715, 'tx2 have a to of quantity 60.3715');
    assert.strictEqual(wasm.transactions[1]?.to[0]?.currency, 'EUR', 'tx2 have a to of currency EUR');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx2 has no fee');
  });
});
