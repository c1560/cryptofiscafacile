import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | finary', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Finary', 'Finary');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should import cashin', async function (assert) {
    // Given
    const csv = new File(
      [
        `type,date,timezone,received_amount,received_currency,sent_amount,sent_currency,fee_amount,fee_currency,description,address,transaction_hash,external_id\n` +
          `Trade,2023-08-12T11:00:18.654314841Z,GMT,0.00368409,BTC,99.01,EUR,0.99,EUR,Buy,,,00000000-0000-0000-0000-000000000000`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_IN, 'tx is categorized as cashin');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2023, 7, 12, 11, 0, 18, 654)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 100, 'tx has a from of quantity 100');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'EUR', 'tx has a from of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.00368409, 'tx has a to of quantity 0.00368409');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import cashout', async function (assert) {
    // Given
    const csv = new File(
      [
        `type,date,timezone,received_amount,received_currency,sent_amount,sent_currency,fee_amount,fee_currency,description,address,transaction_hash,external_id\n` +
          `Trade,2023-08-12T11:00:18.654314841Z,GMT,100,EUR,0.00368409,BTC,0.000001,BTC,Sell,,,00000000-0000-0000-0000-000000000000`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_OUT, 'tx is categorized as cashout');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 0.00368509, 'tx has a from of quantity 0.00368509');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BTC', 'tx has a from of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 100, 'tx has a to of quantity 100');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'EUR', 'tx has a to of currency EUR');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx has a fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.000001, 'tx has a fee of quantity 0.000001');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'BTC', 'tx has a fee of currency BTC');
  });

  test('it should import exchange', async function (assert) {
    // Given
    const csv = new File(
      [
        `type,date,timezone,received_amount,received_currency,sent_amount,sent_currency,fee_amount,fee_currency,description,address,transaction_hash,external_id\n` +
          `Trade,2023-08-12T11:00:18.654314841Z,GMT,0.00368409,BTC,99.01,LTC,0.99,LTC,Buy,,,00000000-0000-0000-0000-000000000000`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'tx is categorized as exchange');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2023, 7, 12, 11, 0, 18, 654)));
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 100, 'tx has a from of quantity 100');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'LTC', 'tx has a from of currency LTC');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.00368409, 'tx has a to of quantity 0.00368409');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BTC', 'tx has a to of currency BTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx has a fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.99, 'tx has a to of quantity 0.99');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'LTC', 'tx has a to of currency LTC');
  });

  test('it should import reward', async function (assert) {
    // Given
    const csv = new File(
      [
        `type,date,timezone,received_amount,received_currency,sent_amount,sent_currency,fee_amount,fee_currency,description,address,transaction_hash,external_id\n` +
          `Reward,2024-02-15T08:00:16.419884001Z,GMT,0.10,USDC,,,0.001,USDC,,,,00000000-0000-0000-0000-000000000000`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.INTERESTS, 'tx is categorized as interest');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has one to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.101, 'tx has a to of quantity 0.101');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'USDC', 'tx has a to of currency USDC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx has a fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.001, 'tx has a fee of quantity 0.001');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'USDC', 'tx has a fee of currency BTC');
  });
});
