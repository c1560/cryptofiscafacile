import { module, test } from 'qunit';

import { setupWasmDefinitionTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | definition | uphold', function (hooks) {
  setupWasmDefinitionTest(hooks, 'Uphold', 'Uphold');

  let wasm!: WasmService;

  hooks.beforeEach(async function () {
    wasm = this.owner.lookup('service:wasm') as WasmService;
  });

  test('it should ignore self transfer', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Destination,Destination Amount,Destination Currency,Fee Amount,Fee Currency,Id,Origin,Origin Amount,Origin Currency,Status,Type\n` +
          `Wed Jun 02 2021 08:24:24 GMT+0000,uphold,2.1375,ETH,,,00000000-0000-0000-0000-000000000000,uphold,2.1375,ETH,completed,transfer`,
      ],
      'uphold.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one exchange');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'exchange have a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 2.1375, 'exchange have a from of quantity 2.1375');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'ETH', 'exchange have a from of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'exchange have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 2.1375, 'exchange have a to of quantity 2.1375');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'exchange have a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'exchange have no fee');
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DONT_CARE, 'transaction is ignored');
  });

  test('it should import deposit', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Destination,Destination Amount,Destination Currency,Fee Amount,Fee Currency,Id,Origin,Origin Amount,Origin Currency,Status,Type\n` +
          `Fri Feb 05 2021 16:01:09 GMT+0000,uphold,9.5,BAT,,,00000000-0000-0000-0000-000000000000,uphold,9.5,BAT,completed,in`,
      ],
      'uphold.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 1, 5, 16, 1, 9)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.DEPOSITS, 'tx is a deposit');
    assert.strictEqual(wasm.transactions[0]?.from.length, 0, 'tx has no from');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 9.5, 'tx has a to of quantity 9.5');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'BAT', 'tx has a to of currency BAT');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import cashout', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Destination,Destination Amount,Destination Currency,Fee Amount,Fee Currency,Id,Origin,Origin Amount,Origin Currency,Status,Type\n` +
          `Fri Feb 05 2021 16:01:09 GMT+0000,uphold,0.19,USD,0.01,USD,00000000-0000-0000-0000-000000000000,uphold,0.95,BAT,completed,out`,
      ],
      'uphold.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 1, 5, 16, 1, 9)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.CASH_OUT, 'tx is a cashout');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 0.95, 'tx has a from of quantity 0.95');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BAT', 'tx has a from of currency BAT');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx has a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.19, 'tx has a to of quantity 0.19');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'USD', 'tx has a to of currency USD');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import withdrawal', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Destination,Destination Amount,Destination Currency,Fee Amount,Fee Currency,Id,Origin,Origin Amount,Origin Currency,Status,Type\n` +
          `Fri Feb 05 2021 16:01:09 GMT+0000,uphold,5,BAT,,,00000000-0000-0000-0000-000000000000,uphold,5,BAT,completed,out`,
      ],
      'uphold.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 1, 'has one tx');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 1, 5, 16, 1, 9)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.WITHDRAWALS, 'tx is a withdrawal');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx has one from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 5, 'tx has a from of quantity 5');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BAT', 'tx has a from of currency BAT');
    assert.strictEqual(wasm.transactions[0]?.to.length, 0, 'tx has no to');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 0, 'tx has no fee');
  });

  test('it should import withdrawal on another network with fee currency same as destination currency', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Destination,Destination Amount,Destination Currency,Fee Amount,Fee Currency,Id,Origin,Origin Amount,Origin Currency,Status,Type\n` +
          `Fri Feb 05 2021 16:01:09 GMT+0000,litecoin,0.062952,LTC,0.004311,LTC,00000000-0000-0000-0000-000000000000,uphold,30,BAT,completed,out`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two txs');
    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 1, 5, 16, 1, 9)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'tx1 is an exchange');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx1 have a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 30, 'tx1 have a from of quantity 30');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BAT', 'tx1 have a from of currency BAT');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx1 have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.067263, 'tx1 have a to of quantity 0.067263');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'LTC', 'tx1 have a to of currency LTC');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx1 a fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 0.004311, 'tx1 has a fee of quantity 0.004311');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'LTC', 'tx1 has a fee of currency LTC');

    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 1, 5, 16, 1, 9)));
    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.WITHDRAWALS, 'tx2 is a withdrawal');
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx2 have a from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 0.062952, 'tx2 have a from of quantity 0.062952');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'LTC', 'tx2 have a from of currency LTC');
    assert.strictEqual(wasm.transactions[1]?.to.length, 0, 'tx2 have no to');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx2 has no fee');
  });

  test('it should import withdrawal on another network with fee currency same as origin currency', async function (assert) {
    // Given
    const csv = new File(
      [
        `Date,Destination,Destination Amount,Destination Currency,Fee Amount,Fee Currency,Id,Origin,Origin Amount,Origin Currency,Status,Type\n` +
          `Fri Feb 05 2021 18:01:09 GMT+0000,ethereum,0.00254,ETH,10.6375,BAT,00000000-0000-0000-0000-000000000001,uphold,37.6375,BAT,completed,out`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    // When
    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    // Then
    assert.strictEqual(wasm.transactions.length, 2, 'has two txs');

    assert.deepEqual(wasm.transactions[0]?.date, new Date(Date.UTC(2021, 1, 5, 18, 1, 9)));
    assert.strictEqual(wasm.transactions[0]?.category, TxCategory.EXCHANGES, 'tx1 is an exchange');
    assert.strictEqual(wasm.transactions[0]?.from.length, 1, 'tx1 has a from');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.quantity, 27, 'tx1 has a from of quantity 27');
    assert.strictEqual(wasm.transactions[0]?.from[0]?.currency, 'BAT', 'tx1 have a from of currency BAT');
    assert.strictEqual(wasm.transactions[0]?.to.length, 1, 'tx1 have a to');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.quantity, 0.00254, 'tx1 have a to of quantity 0.00254');
    assert.strictEqual(wasm.transactions[0]?.to[0]?.currency, 'ETH', 'tx1 have a to of currency ETH');
    assert.strictEqual(wasm.transactions[0]?.fee.length, 1, 'tx1 has a fee');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.quantity, 10.6375, 'tx1 have a fee of quantity 10.6375');
    assert.strictEqual(wasm.transactions[0]?.fee[0]?.currency, 'BAT', 'tx1 have a fee of currency BAT');

    assert.deepEqual(wasm.transactions[1]?.date, new Date(Date.UTC(2021, 1, 5, 18, 1, 9)));
    assert.strictEqual(wasm.transactions[1]?.category, TxCategory.WITHDRAWALS, 'tx2 is a withdrawal');
    assert.strictEqual(wasm.transactions[1]?.from.length, 1, 'tx2 has a from');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.quantity, 0.00254, 'tx2 has a from of quantity 0.00254');
    assert.strictEqual(wasm.transactions[1]?.from[0]?.currency, 'ETH', 'tx2 has a from of currency ETH');
    assert.strictEqual(wasm.transactions[1]?.to.length, 0, 'tx2 has no to');
    assert.strictEqual(wasm.transactions[1]?.fee.length, 0, 'tx2 has no fee');
  });
});
