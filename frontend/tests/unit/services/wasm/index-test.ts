import { module, test } from 'qunit';

import { setupTest } from 'cryptofiscafacile-gui/tests/helpers';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Unit | Service | wasm | index', function (hooks) {
  setupTest(hooks);

  test('it exists', async function (assert) {
    const service = this.owner.lookup('service:wasm');

    assert.ok(service);
  });

  test('definitions loaded', async function (assert) {
    // Given
    const service = this.owner.lookup('service:wasm') as WasmService;

    // When
    await service.updateDefinitonsTask.perform();

    const definitions = await service.getDefinitonsTask.perform();

    // Then
    assert.true(definitions.length > 0);
  });

  test('it import empty file', async function (assert) {
    // Given
    const csv = new File([`User_ID,UTC_Time,Account,Operation,Coin,Change,Remark`], 'file.csv', { type: 'text/csv' });

    const wasm = this.owner.lookup('service:wasm') as WasmService;

    await wasm.updateDefinitionCsvDef('Binance_v2');
    await wasm.useFileTask.perform(csv);

    // When
    await wasm.getHistoryTask.perform();

    // Then
    assert.strictEqual(wasm.history?.length, 1);
    assert.strictEqual((wasm.history ?? [])[0]?.kind, 'Binance');
    assert.strictEqual((wasm.history ?? [])[0]?.result, '0 transactions trouvées (0 ajoutés)');
  });
});
