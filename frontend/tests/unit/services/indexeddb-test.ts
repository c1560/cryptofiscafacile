import { module, test } from 'qunit';

import { setupTest } from 'cryptofiscafacile-gui/tests/helpers';

module('Unit | Service | indexeddb', function (hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function (assert) {
    let service = this.owner.lookup('service:indexeddb');

    assert.ok(service);
  });
});
