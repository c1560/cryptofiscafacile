import Application from '@ember/application';
import { run } from '@ember/runloop';
import { module, test } from 'qunit';

import config from 'cryptofiscafacile-gui/config/environment';
// import { setupTest } from 'ember-qunit';
import { initialize } from 'cryptofiscafacile-gui/initializers/offline-iconify-icons-loader';
import Resolver from 'ember-resolver';
import { listIcons } from 'iconify-icon';

import type ApplicationInstance from '@ember/application/instance';
import type { TestContext } from '@ember/test-helpers';

class DummyApplication extends Application {
  modulePrefix = config.modulePrefix;
  podModulePrefix = config.podModulePrefix;
  Resolver = Resolver;
}

interface Context extends TestContext {
  application: DummyApplication;
  instance: ApplicationInstance;
}

module('Unit | Initializer | offline-iconify-icons-loader', function (this: Context, hooks) {
  hooks.beforeEach(function (this: Context) {
    DummyApplication.instanceInitializer({
      name: 'offline-iconify-icons-loader',
      initialize,
    });

    this.application = DummyApplication.create({ autoboot: false });
    this.instance = this.application.buildInstance();
  });
  hooks.afterEach(function (this: Context) {
    run(this.application, 'destroy');
    run(this.instance, 'destroy');
  });

  test('it works', async function (this: Context, assert) {
    await this.instance.boot();
    assert.ok(listIcons().length > 0);
  });
});
