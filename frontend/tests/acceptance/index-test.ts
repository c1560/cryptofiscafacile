import { currentURL, visit } from '@ember/test-helpers';
import { module, test } from 'qunit';

import { setupApplicationTest } from 'cryptofiscafacile-gui/tests/helpers';
import Sinon from 'sinon';

import { getPageTitle } from 'ember-page-title/test-support';

import type ApplicationService from 'cryptofiscafacile-gui/services/application';
import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Acceptance | index', function (hooks) {
  setupApplicationTest(hooks);

  test('visiting /', async function (assert) {
    await visit('/');

    assert.strictEqual(currentURL(), '/');
    assert.strictEqual(getPageTitle(), 'CryptoFiscaFacile');
  });

  test('should show tour to new user', async function (assert) {
    await visit('/');

    assert.dom('.introjs-tooltip').exists();
  });

  test('should not show tour to new user if indexeddb is desactivated', async function (assert) {
    const application = this.owner.lookup('service:application') as ApplicationService;

    Sinon.stub(application, 'checkHasIndexedDb').returns(new Promise((resolve) => resolve(false)));

    await visit('/');

    assert.dom('.introjs-tooltip').doesNotExist();
  });

  test('should show new version when updated', async function (assert) {
    const wasm = this.owner.lookup('service:wasm') as WasmService;

    await wasm.createApplicationEntryTask.perform({
      key: 'application.version',
      value: '2.0.0',
    });

    await visit('/');

    assert.dom('.modal').exists();
    assert.dom('.modal').containsText('Nouvelle version CFF');
    assert.dom('.modal').containsText('Une nouvelle version de CFF est sortie.');
  });
});
