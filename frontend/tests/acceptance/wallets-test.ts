import { currentURL, visit } from '@ember/test-helpers';
import { module, test } from 'qunit';

import { setupApplicationTest } from 'cryptofiscafacile-gui/tests/helpers';

module('Acceptance | wallets', function (hooks) {
  setupApplicationTest(hooks);

  test('visiting /wallets', async function (assert) {
    await visit('/wallets');

    assert.strictEqual(currentURL(), '/wallets');
  });
});
