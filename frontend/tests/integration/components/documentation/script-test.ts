import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';

import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';
import Sinon from 'sinon';

import { triggerCopySuccess } from 'ember-cli-clipboard/test-support';

module('Integration | Component | Documentation::Script', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders button', async function (assert) {
    await render(hbs`<Documentation::Script />`);

    assert.dom('.btn').exists();
    assert.dom('.btn').hasText('SCRIPT');
  });

  test('it renders modal', async function (assert) {
    await render(hbs`<Documentation::Script />`);
    await click('.btn');

    assert.dom('.modal').exists();
    assert.dom('.modal').containsText('Voici le script à coller dans la console de votre navigateur :');
  });

  test('it copies script', async function (assert) {
    // Given
    await render(hbs`<Documentation::Script />`);
    await click('.btn');

    const debug = Sinon.spy(console, 'debug');

    // When
    triggerCopySuccess();

    // Then
    assert.true(debug.calledOnce);
    assert.deepEqual(debug.lastCall.args[0], 'DEBUG: Copy to clipboard success');
  });
});
