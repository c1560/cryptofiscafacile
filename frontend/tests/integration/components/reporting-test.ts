import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';

import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';

module('Integration | Component | reporting', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    this.set('wallet', {
      name: 'string',
      logo: 'string',
      crypto: true,
      firsttx: new Date(),
      lasttx: new Date(),
      legal: 'string',
      address: 'string',
      url: 'string',
      accounts: { id: '1234', opened: new Date(), closed: new Date(), isClosed: false },
    });

    await render(hbs`<Reporting @wallet={{this.wallet}} />`);

    assert.dom('.form-control').exists();
  });
});
