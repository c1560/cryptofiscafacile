import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';

module('Integration | Component | bs/offcanvas', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    await render(hbs`<Bs::Offcanvas />`);

    assert.dom().hasText('');

    // Template block usage:
    await render(hbs`<Bs::Offcanvas>
  template block text
</Bs::Offcanvas>`);

    assert.dom().hasText('template block text');
  });
});
