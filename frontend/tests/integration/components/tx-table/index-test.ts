import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';

import Option from 'cryptofiscafacile-gui/components/option';
import { SortOrder } from 'cryptofiscafacile-gui/enums/sort-order';
import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Integration | Component | TxTable', function (hooks) {
  setupRenderingTest(hooks);

  test('it shows offcanvas on filter button click', async function (assert) {
    // Given
    const wasm = this.owner.lookup('service:wasm') as WasmService;

    wasm.transactions = [
      {
        id: 'test',
        date: new Date(),
        category: TxCategory.CASH_IN,
        original_category: TxCategory.CASH_IN,
        fee: [],
        from: [{ currency: 'EUR', quantity: 10 }],
        to: [{ currency: 'BTC', quantity: 0.01 }],
        merged: false,
        note: '',
        wallet: '',
      },
    ];

    this.set('catSortOrder', SortOrder.NONE);
    this.set('dateSortOrder', SortOrder.DESC);
    this.set('pfSortOrder', SortOrder.NONE);
    this.set('selectedCategory', [Option.ALL]);
    this.set('selectedCoins', [Option.ALL]);
    this.set('selectedDates', []);
    this.set('selectedWallet', [Option.ALL]);
    this.set('searchedItem', '');

    await render(hbs`<TxTable
  @pageSize={{1}}
  @userPage={{1}}
  @catSortOrder={{this.catSortOrder}}
  @dateSortOrder={{this.dateSortOrder}}
  @pfSortOrder={{this.pfSortOrder}}
  @selectedCategory={{this.selectedCategory}}
  @selectedCoins={{this.selectedCoins}}
  @selectedDates={{this.selectedDates}}
  @selectedWallet={{this.selectedWallet}}
  @searchedItem={{this.searchedItem}}
/>`);

    // When
    await click('[data-test-tx-table-filter-button]');

    // Then
    assert.dom('.offcanvas').hasClass('show');
  });

  test('it hides offcanvas on filter button click', async function (assert) {
    // Given
    const wasm = this.owner.lookup('service:wasm') as WasmService;

    wasm.transactions = [
      {
        id: 'test',
        date: new Date(),
        category: TxCategory.CASH_IN,
        original_category: TxCategory.CASH_IN,
        fee: [],
        from: [{ currency: 'EUR', quantity: 10 }],
        to: [{ currency: 'BTC', quantity: 0.01 }],
        merged: false,
        note: '',
        wallet: '',
      },
    ];

    this.set('catSortOrder', SortOrder.NONE);
    this.set('dateSortOrder', SortOrder.DESC);
    this.set('pfSortOrder', SortOrder.NONE);
    this.set('selectedCategory', [Option.ALL]);
    this.set('selectedCoins', [Option.ALL]);
    this.set('selectedDates', []);
    this.set('selectedWallet', [Option.ALL]);
    this.set('searchedItem', '');

    await render(hbs`<TxTable
  @pageSize={{1}}
  @userPage={{1}}
  @catSortOrder={{this.catSortOrder}}
  @dateSortOrder={{this.dateSortOrder}}
  @pfSortOrder={{this.pfSortOrder}}
  @selectedCategory={{this.selectedCategory}}
  @selectedCoins={{this.selectedCoins}}
  @selectedDates={{this.selectedDates}}
  @selectedWallet={{this.selectedWallet}}
  @searchedItem={{this.searchedItem}}
/>`);

    await click('[data-test-tx-table-filter-button]');

    // When
    await click('[data-test-tx-table-validate-button]');

    // Then
    assert.dom('.offcanvas').lacksClass('show');
  });
});
