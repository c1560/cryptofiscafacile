import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';

import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';

module('Integration | Component | tx-table/tx-item/tx-values', function (hooks) {
  setupRenderingTest(hooks);

  test('it has css ellipsis to truncate token name', async function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });
    this.set('values', [{ quantity: 123, currency: 'EGLD' }]);
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    this.set('action', () => {});
    await render(
      hbs`<TxTable::TxItem::TxValues @values={{this.values}} @onShowCoinDetail={{this.action}} @secretMode={{false}} />`,
    );
    assert.dom('.tx-table-currency').hasStyle({
      'min-width': '48px',
      'white-space': 'nowrap',
      overflow: 'hidden',
      'text-overflow': 'ellipsis',
    });
  });
});
