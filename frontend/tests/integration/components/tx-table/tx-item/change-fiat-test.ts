import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';

import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';

module('Integration | Component | tx-table/tx-item/change-fiat', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`<TxTable::TxItem::ChangeFiat />`);

    assert.dom('[data-test-change-fiat-button]').exists();
  });
});
