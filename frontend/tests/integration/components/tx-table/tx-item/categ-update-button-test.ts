import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';

import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';

module('Integration | Component | TxTable::TxItem::CategUpdateButton', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    this.set('emptyFunction', () => {
      /*Dummy*/
    });
    await render(hbs`<TxTable::TxItem::CategUpdateButton @onClick={{this.emptyFunction}} />`);

    assert.dom('[data-test-categ-update-button]').exists();
  });
});
