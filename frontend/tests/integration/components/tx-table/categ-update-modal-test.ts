import { click, render, select } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';

import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type { TestContext } from '@ember/test-helpers';
import type WasmService from 'cryptofiscafacile-gui/services/wasm';
import type { Transaction } from 'cryptofiscafacile-wasm';

interface Context extends TestContext {
  txs: Transaction[];
  visible: boolean;
}

module('Integration | Component | tx-table/categ-update/deposit-modal', function (hooks) {
  setupRenderingTest(hooks);

  test('it does not renders on @open={{false}}', async function (this: Context, assert) {
    this.txs = [];

    await render(hbs`<TxTable::CategUpdateModal @open={{false}} @txs={{this.txs}} />`);

    assert.dom('.modal').doesNotExist();
  });

  test('it renders', async function (this: Context, assert) {
    this.txs = [];

    await render(hbs`<TxTable::CategUpdateModal @open={{true}} @txs={{this.txs}} />`);

    assert.dom('.modal').exists();
  });

  test('it renders deposit modal', async function (this: Context, assert) {
    this.txs = [
      {
        id: 'tx-id',
        date: new Date(),
        wallet: '',
        original_category: TxCategory.NONE,
        category: TxCategory.DEPOSITS,
        to: [],
        from: [],
        fee: [],
        note: '',
        merged: false,
      },
    ];

    await render(hbs`<TxTable::CategUpdateModal @open={{true}} @txs={{this.txs}} />`);

    assert.dom('.modal-title').hasText("Identifier la Catégorie pour 'Dépôt'");
  });

  test('it renders withdrawal modal', async function (this: Context, assert) {
    this.txs = [
      {
        id: 'tx-id',
        date: new Date(),
        wallet: '',
        original_category: TxCategory.NONE,
        category: TxCategory.WITHDRAWALS,
        to: [],
        from: [],
        fee: [],
        note: '',
        merged: false,
      },
    ];

    await render(hbs`<TxTable::CategUpdateModal @open={{true}} @txs={{this.txs}} />`);

    assert.dom('.modal-title').hasText("Identifier la Catégorie pour 'Retrait'");
  });

  test('it can update category of a deposit transaction', async function (this: Context, assert) {
    // Given
    const wasm = this.owner.lookup('service:wasm') as WasmService;

    await wasm.updateDefinitionCsvDef('Binance_v2');
    await wasm.updateDefinitionWalletDef('Binance');

    const csv = new File(
      [
        `User_ID,UTC_Time,Account,Operation,Coin,Change,Remark\n` +
          `000000000,2021-01-01 01:02:03,Spot,Deposit,ETH,1.99926500,""`,
      ],
      'binance.csv',
      { type: 'text/csv' },
    );

    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    let firstTx = wasm.transactions[0];

    if (firstTx) {
      this.txs = [firstTx];
    }

    this.visible = true;
    this.set('onHidden', () => {
      this.visible = false;
    });

    // When
    await render(
      hbs`<TxTable::CategUpdateModal @onHidden={{this.onHidden}} @open={{this.visible}} @txs={{this.txs}} />`,
    );

    await select('#categ-select', 'Fork');
    await click('[data-test-categ-update-modal-confirm-button]');

    firstTx = wasm.transactions[0];

    if (firstTx) {
      this.txs = [firstTx];
    }

    // Then
    assert.notStrictEqual(this.txs, undefined);
    assert.strictEqual(this.txs[0]?.category, TxCategory.FORKS);
    assert.false(this.visible);
  });
});
