import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, skip } from 'qunit';

import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';
import type { PollyContext } from 'cryptofiscafacile-gui/tests/helpers/polly-helper';

module('Integration | Component | TxTable::CoinDetailModal', function (hooks) {
  setupRenderingTest(hooks);

  // to be implemented on digital assets proxy
  skip('it renders coin when found', async function (assert) {
    // Given
    const wasm = this.owner.lookup('service:wasm') as WasmService;

    await wasm.setFetchRates.perform(true);

    // When
    await render(hbs`<TxTable::CoinDetailModal @open={{true}} @symbol='BTC' />`);

    // Then
    await wasm.getCoinDetailTask.last;

    assert.dom('[data-test-coin-detail-modal-coin-name]').hasText('Bitcoin');
    assert.dom('[data-test-coin-detail-modal-coin-price-eur]').containsText('15 847,64');
  });

  // to be implemented on digital assets proxy
  skip('it renders unknown coin', async function (this: PollyContext, assert) {
    this.polly.configure({ recordFailedRequests: true });

    await render(hbs`<TxTable::CoinDetailModal @open={{true}} @symbol='INCONNU' />`);

    const wasm = this.owner.lookup('service:wasm') as WasmService;

    await wasm.getCoinDetailTask.last;
    // await settled();

    assert.dom('[data-test-coin-detail-modal-coin-name]').hasText('Jeton inconnu');
  });
});
