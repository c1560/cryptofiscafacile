import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, skip, test } from 'qunit';

import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';
import Sinon from 'sinon';

import { triggerCopySuccess } from 'ember-cli-clipboard/test-support';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Integration | Component | TxTable::Validate', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders button', async function (assert) {
    await render(hbs`<TxTable::Validate />`);

    assert.dom('.btn').exists();
    assert.dom('.btn').hasText('VALIDER');
  });

  test('it renders modal', async function (assert) {
    await render(hbs`<TxTable::Validate />`);
    await click('.btn');

    assert.dom('.modal').exists();
    assert.dom('.modal').containsText('Validation des TX');
  });

  skip('it renders modal flow', async function (assert) {
    await render(hbs`<TxTable::Validate />`);
    await click('.btn');
    assert.dom('.modal').containsText('Validation des TX 1');

    await click('[data-test-validate-next-button-1]');
    assert.dom('.modal').containsText('Validation des TX 2');

    await click('[data-test-validate-next-button-2]');
    assert.dom('.modal').containsText('Validation des TX 3');

    await click('[data-test-validate-next-button-3]');
    assert.dom('.modal').containsText('Validation des TX 4');
  });

  test('it copies script', async function (assert) {
    // Given
    const wasm = this.owner.lookup('service:wasm') as WasmService;

    Sinon.stub(wasm, 'checkConsistencyCategVsToFrom').callsFake(async () => {
      wasm.consistencyCategVsToFrom = {
        errors: [
          {
            error: 'test',
            tx: {
              id: '',
              amounts: '',
              category: '',
              coin: '',
              date: new Date(),
              wallet: '',
            },
            related: [],
          },
        ],
      };
    });

    await render(hbs`<TxTable::Validate />`);
    await click('.btn');

    const debug = Sinon.spy(console, 'debug');

    // When
    triggerCopySuccess();

    // Then
    assert.true(debug.calledOnce);
    assert.deepEqual(debug.lastCall.args[0], 'DEBUG: Copy to clipboard success');
  });
});
