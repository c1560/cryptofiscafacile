import { click, fillIn, find, render, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';
import { setupRenderingTest as upstreamSetupRenderingTest } from 'ember-qunit';

import WasmService from 'cryptofiscafacile-gui/services/wasm';
import { task, timeout } from 'ember-concurrency';

import type { ApiData } from 'cryptofiscafacile-wasm';

module('Integration | Component | Api::Blockchains', function (hooks) {
  class MockWasmService extends WasmService {
    getApi(api: ApiData) {
      this.toolsProgress.set(api, 50);

      const progressTask = this.getApiProgressTask.perform(api);

      this.toolsTasks.set(api, [progressTask]);
    }

    getApiProgressTask = task(async (api: ApiData) => {
      await timeout(500);
      this.toolsProgress.set(api, 100);
    });
  }

  upstreamSetupRenderingTest(hooks);
  hooks.beforeEach(function () {
    this.owner.register('service:wasm', MockWasmService);
  });

  test('it renders', async function (assert) {
    // Given
    const api = { name: 'Blockchain', url: 'https://shitcoin-api.com' } as ApiData;

    this.set('api', api);

    // When
    await render(hbs`<Api::Blockchains @blockchain={{this.api}} />`);

    // Then
    assert.dom('[data-test-api-blockchains]').exists();
    assert.dom('[data-test-api-blockchains-url]').containsText('https://shitcoin-api.com');
  });

  test('it renders progress bar', async function (assert) {
    // Given
    const api = { name: 'Blockchain', url: 'https://shitcoin-api.com' } as ApiData;

    this.set('api', api);

    // When
    await render(hbs`<Api::Blockchains @blockchain={{this.api}} />`);
    await fillIn('[data-test-api-blockchains-input-address]', 'super-compte');
    click('[data-test-api-blockchains-sync-button]');
    await waitUntil(
      () => {
        return find('.progress') !== undefined;
      },
      { timeout: 1000 },
    );

    // Then
    assert.dom('.progress').exists();
    assert.dom('.progress').containsText('50%');
    assert.dom('.btn').hasAttribute('disabled');
  });
});
