import { click, fillIn, find, render, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';

import WasmService from 'cryptofiscafacile-gui/services/wasm';
import { task, timeout } from 'ember-concurrency';

import type { ApiData } from 'cryptofiscafacile-wasm';

module('Integration | Component | Api::Exchanges', function (hooks) {
  setupRenderingTest(hooks);
  class MockWasmService extends WasmService {
    getApi(api: ApiData) {
      this.toolsProgress.set(api, 50);

      const progressTask = this.getApiProgressTask.perform(api);

      this.toolsTasks.set(api, [progressTask]);
    }

    getApiProgressTask = task(async (api: ApiData) => {
      await timeout(500);
      this.toolsProgress.set(api, 100);
    });
  }

  class MockErrorWasmService extends WasmService {
    getApi(api: ApiData) {
      this.toolsErrors.set(api, 'exchange api error');
    }
  }

  test('it renders', async function (assert) {
    // Given
    const wallet = { name: 'Exchange', url: 'https://shitcoin-exchange.com' } as ApiData;

    this.set('wallet', wallet);

    // When
    await render(hbs`<Api::Exchanges @wallet={{this.wallet}} />`);

    // Then
    assert.dom('[data-test-api-exchanges]').exists();
    assert.dom('[data-test-api-exchanges-url]').containsText('https://shitcoin-exchange.com');
  });

  test('it renders progress bar', async function (assert) {
    // Given
    this.owner.register('service:wasm', MockWasmService);

    const wallet = { name: 'Exchange', url: 'https://shitcoin-exchange.com' } as ApiData;

    this.set('wallet', wallet);

    // When
    await render(hbs`<Api::Exchanges @wallet={{this.wallet}} />`);
    await fillIn('[data-test-api-exchanges-input-pub]', 'super-pub');
    await fillIn('[data-test-api-exchanges-input-priv]', 'super-priv');
    click('[data-test-api-exchanges-sync-button]');
    await waitUntil(
      () => {
        return find('.progress') !== undefined;
      },
      { timeout: 1000 },
    );

    // Then
    assert.dom('.progress').exists();
    assert.dom('.progress').containsText('50%');
    assert.dom('.btn').hasAttribute('disabled');
  });

  test('it renders error', async function (assert) {
    // Given
    this.owner.register('service:wasm', MockErrorWasmService);

    const wallet = { name: 'Exchange', url: 'https://shitcoin-exchange.com' } as ApiData;

    this.set('wallet', wallet);

    // When
    await render(hbs`<Api::Exchanges @wallet={{this.wallet}} />`);
    await fillIn('[data-test-api-exchanges-input-pub]', 'super-pub');
    await fillIn('[data-test-api-exchanges-input-priv]', 'super-priv');
    await click('[data-test-api-exchanges-sync-button]');

    // Then
    assert.dom('[data-test-api-exchanges-progress-error]').exists();
    assert.dom('[data-test-api-exchanges-progress-error]').containsText('exchange api error');
    assert.dom('.btn').doesNotHaveAttribute('disabled');
  });
});
