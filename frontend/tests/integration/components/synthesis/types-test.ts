import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';

import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';

module('Integration | Component | synthesis/types', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    this.set('dates', [new Date(2021, 1, 1).getTime(), new Date(2021, 12, 31).getTime()]);

    await render(hbs`<Synthesis::Types @dates={{this.dates}} />`);

    assert.dom('.card-body').exists();
  });
});
