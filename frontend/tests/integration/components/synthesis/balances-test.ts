import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';

import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Integration | Component | Synthesis::Balances', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    this.set('date', new Date(2022, 0, 1));
    await render(hbs`<Synthesis::Balances @date={{this.date}} />`);

    assert.dom('.card-body').exists();
    assert.dom('.card-title').containsText('Balances au 01/01/2022');
    assert.dom('.table-balances').containsText('Aucun actif');
  });

  test('it renders assets', async function (assert) {
    const wasm = this.owner.lookup('service:wasm') as WasmService;

    wasm.synthBalances = [
      { coin: 'BTC', amount: 0.0005 },
      { coin: 'ETH', amount: 1.1234 },
    ];

    this.set('date', new Date(2022, 0, 1));
    await render(hbs`<Synthesis::Balances @date={{this.date}} />`);

    assert.dom('.card-body').exists();
    assert.dom('.card-title').containsText('Balances au 01/01/2022');
    assert.dom('.table-balances').containsText('BTC 0,0005 ETH 1,1234');
  });

  test('it renders negative assets', async function (assert) {
    const wasm = this.owner.lookup('service:wasm') as WasmService;

    wasm.synthBalances = [
      { coin: 'BTC', amount: 0.0005 },
      { coin: 'ETH', amount: -1.1234 },
    ];

    this.set('date', new Date(2022, 0, 1));
    await render(hbs`<Synthesis::Balances @date={{this.date}} />`);

    assert.dom('.card-body').exists();
    assert.dom('.card-title').containsText('Balances au 01/01/2022');
    assert.dom('.line-balance:nth-child(2)').containsText('ETH -1,1234');
    assert.dom('.line-balance:nth-child(2)').hasStyle({ 'background-color': 'rgba(255, 0, 0, 0.2)' });

    await triggerEvent('.line-balance:nth-child(2)', 'mouseenter');
    assert
      .dom('.tooltip')
      .hasText(
        'Attention, vous avez une balance négative de ETH le 01/01/2022. Veuillez vérifier vos transactions pour corriger cette balance',
      );
  });
});
