import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';

import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';

module('Integration | Component | Synthesis::Evolution', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    // set the dates context
    //  setFlatpickrDate(selector: string, date: object, triggerChange: boolean)
    this.set('dates', [new Date(2021, 1, 1), new Date(2021, 12, 31)]);

    await render(hbs`<Synthesis::Evolution @dates={{this.dates}} @error={{true}} />`);

    assert.dom('.card-body').exists();
  });
});
