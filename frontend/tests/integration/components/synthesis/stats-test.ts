import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';

import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Integration | Component | Synthesis::Stats', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    this.set('dates', [new Date(2021, 1, 1), new Date(2021, 12, 31)]);
    await render(hbs`<Synthesis::Stats @dates={{this.dates}} />`);

    assert.dom('.card-body').exists();
    assert.dom('.card-title').containsText('au 31/01/2022');
  });

  test('it renders no assets', async function (assert) {
    const wasm = this.owner.lookup('service:wasm') as WasmService;

    wasm.wallets = [{ name: 'test', icon: 'none' }];

    this.set('dates', [new Date(2021, 1, 1), new Date(2021, 12, 31)]);
    await render(hbs`<Synthesis::Stats @dates={{this.dates}} />`);

    assert.dom('.card-body').exists();
    assert.dom('#chartdiv').hasText('Aucun actif');
  });
});
