import { render } from '@ember/test-helpers';
import click from '@ember/test-helpers/dom/click';
import find from '@ember/test-helpers/dom/find';
import triggerEvent from '@ember/test-helpers/dom/trigger-event';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';

import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';

import type { Target } from '@ember/test-helpers';
import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Integration | Component | UserWallets', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders empty history table', async function (assert) {
    await render(hbs`<UserWallets />`);

    assert.dom('[data-test-table-wallets]').containsText('AUCUN IMPORT');
  });

  test('it renders history table with one file', async function (assert) {
    // Given
    const wasm = this.owner.lookup('service:wasm') as WasmService;

    await wasm.updateDefinitionCsvDef('Uphold');
    await wasm.updateDefinitionWalletDef('Uphold');

    const filename = 'uphold.csv';

    await render(hbs`<UserWallets />`);

    // When
    const input = find('#file-input') as Target;
    const csv = new File(
      [
        `Date,Destination,Destination Amount,Destination Currency,Fee Amount,Fee Currency,Id,Origin,Origin Amount,Origin Currency,Status,Type\n` +
          `Fri Dec 25 2020 05:00:57 GMT+0000,uphold,5,ETH,,,cb3aa538-5f3a-4599-880f-aaaaaaaaaaaa,uphold,5,ETH,completed,out\n` +
          `Wed Nov 25 2020 05:00:25 GMT+0000,uphold,5,ETH,,,4100aa11-e130-4d5f-bf71-aaaaaaaaaaaa,uphold,5,ETH,completed,out`,
      ],
      filename,
      { type: 'text/csv' },
    );

    await triggerEvent(input, 'change', { files: [csv] });
    await click('#addFilesButton');

    await wasm.useFileTask.last;

    // Then
    assert.dom('[data-test-table-wallets]').containsText(filename);
    assert.dom('[data-test-table-wallets]').containsText('2 transactions trouvées (2 ajoutés)');
  });
});
