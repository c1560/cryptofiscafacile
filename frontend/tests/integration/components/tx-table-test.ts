import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';

import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Integration | Component | TxTable', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    this.set('selectedCategory', []);
    this.set('selectedCoins', []);
    this.set('selectedDates', []);
    this.set('selectedWallet', []);
    this.set('searchedItem', '');

    await render(
      hbs`<TxTable
  @selectedCategory={{this.selectedCategory}}
  @selectedCoins={{this.selectedCoins}}
  @selectedDates={{this.selectedDates}}
  @selectedWallet={{this.selectedWallet}}
  @searchedItem={{this.searchedItem}}
/>`,
    );

    assert.dom('.table').exists();
    assert.dom('[data-test-tx-table-validate-button]').hasClass('disabled');
  });

  test('it renders validate button not in disabled state if there are transactions', async function (assert) {
    // Given
    const wasm = this.owner.lookup('service:wasm') as WasmService;

    await wasm.updateDefinitionCsvDef('CryptoFiscaFacile');

    const csv = new File(
      [
        `Date,Type de Transaction,De Montant,De Symbol,Vers Montant,Vers Symbol,Frais Montant,Frais Symbol,Memo\n` +
          `2019-01-02 00:00:00,Don,,,5,BTC,,,Don entrant 5 BTC`,
      ],
      'file.csv',
      { type: 'text/csv' },
    );

    await wasm.useFileTask.perform(csv);
    await wasm.getTransactionsTask.perform();

    this.set('selectedCategory', []);
    this.set('selectedCoins', []);
    this.set('selectedDates', []);
    this.set('selectedWallet', []);
    this.set('searchedItem', '');

    // When
    await render(
      hbs`<TxTable @selectedCategory={{this.selectedCategory}} @selectedCoins={{this.selectedCoins}} @selectedDates={{this.selectedDates}} @selectedWallet={{this.selectedWallet}} @searchedItem={{this.searchedItem}} />`,
    );

    // Then
    assert.dom('[data-test-tx-table-validate-button]').doesNotHaveClass('disabled');
  });
});
