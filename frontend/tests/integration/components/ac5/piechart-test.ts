import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';

import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';

module('Integration | Component | Ac5::Piechart', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders chart', async function (assert) {
    this.set('assets', [{ coin: 'BTC', balance: 20000, amount: 1 }]);
    await render(hbs`<Ac5::Piechart @data={{this.assets}} @categoryField='coin' @valueField='balance' />`);

    assert.dom('[data-test-ac5-pie-chart]').exists();
  });
});
