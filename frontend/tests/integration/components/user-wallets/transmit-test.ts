import { click, render, select } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';

import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';
import Sinon from 'sinon';

import { triggerCopySuccess } from 'ember-cli-clipboard/test-support';

import type { FileUsageStatus } from 'cryptofiscafacile-wasm';

module('Integration | Component | UserWallets::Transmit', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders button', async function (assert) {
    await render(hbs`<UserWallets::Transmit />`);

    assert.dom('.btn').exists();
    assert.dom('.btn').hasText('SUPPORT');
  });

  test('it renders modal', async function (assert) {
    this.set<FileUsageStatus>('data', { error: '', name: 'test', ext: '', kind: '', result: '', unknowns: [''] });
    await render(hbs`<UserWallets::Transmit @data={{this.data}} />`);
    await click('.btn');

    assert.dom('.modal').exists();
    assert.dom('.modal').containsText('Veuillez donc nous transmettre les informations ci-dessous par e-mail');
  });

  test('it copies script', async function (assert) {
    // Given
    this.set<FileUsageStatus>('data', { error: '', name: 'test', ext: '', kind: '', result: '', unknowns: [''] });
    await render(hbs`<UserWallets::Transmit @data={{this.data}} />`);
    await click('.btn');

    await select('[name=categ]', 'Autre');

    const debug = Sinon.spy(console, 'debug');

    // When
    triggerCopySuccess();

    // Then
    assert.true(debug.calledOnce);
    assert.deepEqual(debug.lastCall.args[0], 'DEBUG: Copy to clipboard success');
  });
});
