import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';

import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';

module('Integration | Component | sidebar', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    await render(hbs`<App::MainSidebar />`);

    assert
      .dom()
      .hasText(
        [
          'CryptoFiscaFacile',
          'Synthèse',
          'Portefeuilles',
          'Outils',
          '(API)',
          'Transactions',
          'Rapports',
          'Documentation',
          'N’hésitez',
          'pas',
          'à',
          'laisser',
          'un',
          'commentaire',
          'sur',
          'le',
          'Gitlab',
          'du',
          'projet:',
          'https://gitlab.com/c1560/cryptofiscafacile',
          'Exporter',
          'Configuration',
        ].join(' '),
      );
  });
});
