import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';

import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';
import Sinon from 'sinon';

module('Integration | Component | App::Icon', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    await render(hbs('<App::Icon @icon="ion:information-circle-outline" />'));

    assert.dom('iconify-icon').exists();
  });

  test('it does not render unknow icon', async function (assert) {
    const warn = Sinon.stub(console, 'warn');

    await render(hbs('<App::Icon @icon="unknown" />'));

    assert.true(warn.calledOnce);
    assert.deepEqual(
      warn.lastCall.args[0],
      'WARNING: icon unknown not found. please add cff:unknown to offline-iconify-icons-loader.ts',
    );
  });
});
