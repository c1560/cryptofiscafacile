import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';

import { EngineStatus } from 'cryptofiscafacile-gui/enums/engine-status';
import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Integration | Component | App::Header::EngineIndicator', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders loading status', async function (assert) {
    // Given
    const wasm = this.owner.lookup('service:wasm') as WasmService;

    wasm.status = EngineStatus.LOADING;

    // When
    await render(hbs`<App::Header::EngineIndicator />`);

    // Then
    assert.dom('span').hasClass('engine-loading');
    assert.dom('span').hasStyle({
      animation: '1s ease 0s infinite normal none running engine-loading',
    });
    assert.dom('iconify-icon').hasAttribute('icon', 'cff:mdi:engine-outline');

    await triggerEvent('span', 'mouseenter');
    assert.dom('.tooltip').hasText('Le moteur CFF démarre');
  });

  test('it renders running status', async function (assert) {
    // When
    await render(hbs`<App::Header::EngineIndicator />`);

    // Then
    assert.dom('span').hasClass('engine-running');
    assert.dom('span').hasStyle({
      color: 'rgb(67, 154, 134)',
    });
    assert.dom('iconify-icon').hasAttribute('icon', 'cff:mdi:engine-outline');

    await triggerEvent('span', 'mouseenter');
    assert.dom('.tooltip').hasText('Le moteur CFF tourne parfaitement');
  });

  test('it renders halted status', async function (assert) {
    // Given
    const wasm = this.owner.lookup('service:wasm') as WasmService;

    wasm.status = EngineStatus.HALTED;

    // When
    await render(hbs`<App::Header::EngineIndicator />`);

    // Then
    assert.dom('span').hasClass('engine-halted');
    assert.dom('span').hasStyle({
      animation: '1s ease 0s infinite normal none running engine-halted',
    });
    assert.dom('iconify-icon').hasAttribute('icon', 'cff:mdi:engine-off-outline');

    await triggerEvent('span', 'mouseenter');
    assert.dom('.tooltip').hasText('Le moteur CFF est arrêté, merci de recharger la page');
  });
});
