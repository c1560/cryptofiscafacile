import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';

import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';

module('Integration | Component | App::Header', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`<App::Header />`);

    // TODO écrire un test d'int. qui pourrait vérifier qu'on a bien un min. 2*string &&& 6*2 chiffres
    // , voire 'dont les 2 premiers chiffres sont 01 to 07
    assert.dom('.slide-in-top').exists();
  });
});
