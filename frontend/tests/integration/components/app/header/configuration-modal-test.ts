import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';

import config from 'cryptofiscafacile-gui/config/environment';
import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';

module('Integration | Component | App::Header::ConfigurationModal', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    await render(hbs`<App::Header::ConfigurationModal @open={{true}} />`);

    assert.dom('[data-test-configuration-modal]').exists();
  });

  test('it renders version', async function (assert) {
    await render(hbs`<App::Header::ConfigurationModal @open={{true}} />`);

    assert.dom('[data-test-configuration-modal-version]').exists();
    assert.dom('[data-test-configuration-modal-version]').hasText(`Version: ${config.APP.version}`);
  });
});
