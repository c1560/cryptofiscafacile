import { module } from 'qunit';

import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';

module('Integration | Component | user-data', function (hooks) {
  setupRenderingTest(hooks);
});
