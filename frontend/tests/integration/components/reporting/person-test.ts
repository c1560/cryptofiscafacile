import { click, fillIn, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';

import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

module('Integration | Component | Reporting::Person', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    await render(hbs`<Reporting::Person />`);
    assert.dom('[data-test-reports-person]').exists();
  });

  test('button save disabled with no changes', async function (assert) {
    await render(hbs`<Reporting::Person />`);
    assert.dom('[data-test-reports-person-button]').hasAttribute('disabled');
  });

  test('button save enabled on change', async function (assert) {
    await render(hbs`<Reporting::Person />`);
    await fillIn('#name', 'Martin');
    assert.dom('[data-test-reports-person-button]').doesNotHaveAttribute('disabled');
  });

  test('wasm called on save clicked', async function (assert) {
    // Given

    const wasm = this.owner.lookup('service:wasm') as WasmService;

    await wasm.getUserInfosTask.perform();

    await render(hbs`<Reporting::Person />`);
    await fillIn('#name', 'Martin');

    // When
    await click('[data-test-reports-person-button]');

    // Then
    assert.strictEqual(wasm.saveUserTask.performCount, 1);
    assert.strictEqual(wasm.userInfos?.name, 'Martin');
  });
});
