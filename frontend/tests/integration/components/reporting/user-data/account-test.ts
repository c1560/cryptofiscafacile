import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';

import { setupRenderingTest } from 'cryptofiscafacile-gui/tests/helpers';

module('Integration | Component | reporting/user-data/account', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });
    // isFlatpickrOpen(1);
    // setFlatpickrDate(1, new Date(), false);

    this.set('account', { id: '1234', opened: new Date(), closed: new Date(), isClosed: false });
    this.set('first', new Date());
    this.set('last', new Date());
    await render(
      hbs`<Reporting::UserData::Account @account={{this.account}} @firsttx={{this.first}} @lasttx={{this.last}} />`,
    );

    assert.dom('.row').exists();
  });
});
