import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';

module('Integration | Modifier | intro', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    await render(
      hbs`<button id='button' type='button' {{intro '#intro-content'}}>Click Me</button>
<div id='intro-content'>
  <p data-intro='test content'>blabla</p>
</div>`,
    );
    await click('#button');

    assert.dom('.introjs-tooltip').exists();
    assert.dom('.introjs-tooltip').containsText('content');
  });
});
