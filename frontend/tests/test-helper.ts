import { setApplication } from '@ember/test-helpers';
import QUnit from 'qunit';
import { setup } from 'qunit-dom';
import { start } from 'ember-qunit';

import Application from 'cryptofiscafacile-gui/app';
import config from 'cryptofiscafacile-gui/config/environment';
import setupSinon from 'ember-sinon-qunit';

import { forceModulesToBeLoaded, sendCoverage } from 'ember-cli-code-coverage/test-support';

QUnit.done(async () => {
  forceModulesToBeLoaded();
  await sendCoverage();
});

setup(QUnit.assert);
setApplication(Application.create(config.APP));
setupSinon();

start();
