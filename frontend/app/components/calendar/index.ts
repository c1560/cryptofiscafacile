import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

interface CalendarArgs {
  dates: Date[];
  onChange: (dates: Date[]) => void;
}

export default class Calendar extends Component<CalendarArgs> {
  @tracked min = new Date(2013, 4, 30);
  @tracked max: Date = ((d) => new Date(d.setDate(d.getDate() - 1)))(new Date());
  @tracked step = new Date();

  @action
  onClose(selectedDates: Date[]) {
    this.args.onChange(selectedDates);
  }

  /**
   * Unfillter is only callable from FILTER
   * feature
   * @memberof Calendar
   */
  @action
  unfilter() {
    this.args.onChange([]);
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@glimmer/component' {
  interface Registry {
    'calendar-component': Calendar;
  }
}
