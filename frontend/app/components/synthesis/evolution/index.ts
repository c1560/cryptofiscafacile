import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';
interface SynthesisEvolutionArgs {
  error: boolean;
  dates: Date[];
}

export default class SynthesisEvolution extends Component<SynthesisEvolutionArgs> {
  @service declare wasm: WasmService;

  get isRunning() {
    return (
      this.wasm.getSynthEvolutionTask.isRunning ||
      (this.aggregatedAssets.length == 0 && this.wasm.getWalletsTask.isRunning)
    );
  }

  get aggregatedAssets() {
    return this.wasm.synthEvolution;
  }

  get valueFields() {
    if (this.aggregatedAssets[0]) {
      return Object.keys(this.aggregatedAssets[0]).filter((element) => element !== 'date');
    } else {
      return [];
    }
  }

  get startDate() {
    return this.args.dates[0];
  }
  get endDate() {
    return this.args.dates[1];
  }
  /**
   * This method is mandatory to state if we should display the
   * evolution graph.
   * Return empty if there is no key in the array of getSynth
   *
   * @readonly
   */
  get tokenImported() {
    return this.aggregatedAssets.length > 0 && this.aggregatedAssets[0]
      ? Object.keys(this.aggregatedAssets[0]).some((element) => element !== 'date')
      : false;
  }

  get hasWallets() {
    return this.wasm.wallets.length > 0;
  }
}
