import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { debug } from '@ember/debug';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';
import type { Wallet } from 'cryptofiscafacile-wasm';

export default class Synthesis extends Component {
  @service declare wasm: WasmService;

  @tracked selectedDates: Date[] = [];
  @tracked selectedWallet?: Wallet;
  @tracked errorRange = false;

  constructor(owner: Component, args: never) {
    super(owner, args);
    this.selectedDates = [this.wasm.defaultStartDate, this.wasm.defaultEndDate];
  }

  get endDate() {
    return this.selectedDates[1];
  }

  @action
  selectDates(dates: Date[]) {
    this.selectedDates = dates;
    this.refreshSynthData();
  }

  @action
  onWalletSelected(wallet: Wallet) {
    this.selectedWallet = wallet;
    this.refreshSynthData();
  }

  refreshSynthData() {
    if (this.selectedDates[0] && this.selectedDates[1]) {
      try {
        this.wasm.getSynthBalancesTask.perform(this.selectedDates[1], this.selectedWallet);
        this.wasm.getSynthEvolutionTask.perform(this.selectedDates[0], this.selectedDates[1], this.selectedWallet);
        this.wasm.getSynthPortfolioTask.perform(this.selectedDates[1], this.selectedWallet);
        this.wasm.getSynthStatTotalValueTask.perform(this.selectedDates[1], this.selectedWallet);

        if (!this.selectedWallet) {
          this.wasm.getSynthStatsGainsLossesTask.perform(this.selectedDates[1]);
        } else {
          this.wasm.synthStatsGainsLosses = undefined;
          this.wasm.synthStatsGainsError = undefined;
        }

        this.wasm.getSynthTxTypesTask.perform(this.selectedDates[0], this.selectedDates[1], this.selectedWallet);
      } catch (error) {
        this.errorRange = true;
        debug(
          'error in get custom synth with custom dates. Range too short: avoid using 7 days, choose instead more than 1 week !',
        );
      }
    }
  }
}
