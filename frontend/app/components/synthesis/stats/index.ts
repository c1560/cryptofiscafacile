import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

interface SynthesisStatsArgs {
  dates: Date[];
}

export default class SynthesisStats extends Component<SynthesisStatsArgs> {
  @service declare wasm: WasmService;

  get isRunning() {
    return (
      this.wasm.getSynthPortfolioTask.isRunning || (this.portfolio.length == 0 && this.wasm.getWalletsTask.isRunning)
    );
  }

  get portfolio() {
    return this.wasm.synthPorfolio;
  }

  get totalValue() {
    return this.wasm.synthStatTotalValue?.total_value ?? 0;
  }

  get unrealizedGainsLosses() {
    return this.wasm.synthStatsGainsLosses?.unrealized;
  }

  get realizedGainsLosses() {
    return this.wasm.synthStatsGainsLosses?.realized;
  }

  get bnc() {
    return this.wasm.synthStatsGainsLosses?.bnc;
  }

  get statsError() {
    return this.wasm.synthStatsGainsError;
  }

  get endDate() {
    return this.args.dates[1];
  }

  get year() {
    return this.args.dates[1]?.getFullYear();
  }

  get hasWallets() {
    return this.wasm.wallets.length > 0;
  }
}
