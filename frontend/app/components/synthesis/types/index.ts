import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';
export default class SynthesisTypes extends Component {
  @service declare wasm: WasmService;

  get unprocessedSynthTx() {
    return this.wasm.synthTxTypes;
  }
}
