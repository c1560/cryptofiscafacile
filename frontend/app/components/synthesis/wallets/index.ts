import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import { tracked } from 'tracked-built-ins';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';
import type { Wallet } from 'cryptofiscafacile-wasm';

interface SynthesisWalletsArgs {
  onSelected: (wallet?: Wallet) => void;
  dates: Date[];
}

export default class SynthesisWallets extends Component<SynthesisWalletsArgs> {
  @service declare wasm: WasmService;

  @tracked selected?: Wallet;

  get wallets() {
    return this.wasm.wallets;
  }

  @action
  onSelect(wallet: Wallet) {
    this.selected = this.selected === wallet ? undefined : wallet;

    if (this.args.onSelected) {
      this.args.onSelected(this.selected);
    }
  }
}
