import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import { tracked } from 'tracked-built-ins';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

export default class SynthesisTxBalances extends Component {
  @service declare wasm: WasmService;

  @tracked userPage = 1;
  defaultPageSize = 7;

  get pageSortedBalances() {
    return this.wasm.synthBalances
      ? this.wasm.synthBalances.slice((this.page - 1) * this.pageSize, this.page * this.pageSize)
      : [];
  }

  get page() {
    return Math.min(this.userPage, this.maxPage);
  }

  get maxPage() {
    return Math.max(1, Math.ceil(this.wasm.synthBalances.length / this.pageSize));
  }

  get pageSize() {
    return this.defaultPageSize;
  }

  @action
  onUserPageChanged(page: number): void {
    this.userPage = page;
  }

  @action
  setPageSize(pageSize: number) {
    this.defaultPageSize = pageSize;
  }
}
