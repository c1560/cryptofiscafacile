import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { debug } from '@ember/debug';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import type Wasm from 'cryptofiscafacile-gui/services/wasm';
import type { TaskInstance } from 'ember-concurrency';

interface RemovalArgs {
  filename: string;
}

export default class Removal extends Component<RemovalArgs> {
  @service declare wasm: Wasm;

  @tracked deletingTask?: TaskInstance<void>;

  get deletingFile() {
    return this.wasm.deleteFileTask.isRunning;
  }

  get deletingThisFile() {
    return this.deletingTask?.isRunning;
  }

  @action
  async delete() {
    debug('Removal of file: ' + this.args.filename);
    this.deletingTask = this.wasm.deleteFileTask.perform(this.args.filename);
    await this.deletingTask;
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@glimmer/component' {
  interface Registry {
    removal: Removal;
  }
}
