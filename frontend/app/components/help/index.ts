import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import type ApplicationService from 'cryptofiscafacile-gui/services/application';
import type WasmService from 'cryptofiscafacile-gui/services/wasm';

export default class Help extends Component {
  @service declare application: ApplicationService;
  @service declare wasm: WasmService;

  get autoStart() {
    return this.application.hasIndexedDb && this.application.newUser && !this.application.firstTourDone;
  }

  @action
  onIntroExit() {
    if (this.autoStart) {
      this.wasm.updateApplicationEntryTask.perform({
        key: 'application.version',
        value: this.application.version,
      });

      this.application.firstTourDone = true;
    }
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@glimmer/component' {
  interface Registry {
    help: Help;
  }
}
