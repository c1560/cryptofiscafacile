import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

interface TxTableCoinDetailModalArgs {
  symbol: string;
}

export default class TxTableCoinDetailModal extends Component<TxTableCoinDetailModalArgs> {
  @service declare wasm: WasmService;

  @action
  async fetchCoinDetail() {
    if (this.args.symbol) {
      this.wasm.getCoinDetailTask.perform(this.args.symbol);
    }
  }

  get coin() {
    return this.wasm.coinsDetail.get(this.args.symbol);
  }

  get hasCurrentPriceBtc() {
    return this.coin?.marketData.currentPriceEur !== undefined;
  }

  get hasCurrentPriceEur() {
    return this.coin?.marketData.currentPriceEur !== undefined;
  }

  get hasCurrentPriceUsd() {
    return this.coin?.marketData.currentPriceUsd !== undefined;
  }

  get priceChange24hEur() {
    return this.coin?.marketData.currentPriceChange24hEur
      ? this.coin.marketData.currentPriceChange24hEur / 100
      : undefined;
  }
}
