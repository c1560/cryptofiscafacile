import Component from '@glimmer/component';
import { debug } from '@ember/debug';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

interface TxTableTxItemUnmergedArgs {
  id: string;
}

export default class TxTableTxItemUnmerged extends Component<TxTableTxItemUnmergedArgs> {
  @service declare wasm: WasmService;

  @action
  async unMerge() {
    debug('Unmerge transactions with id : ' + this.args.id);
    await this.wasm.unMergeTransactionsTask.perform(this.args.id);
    await this.wasm.getTransactionsTask.perform();
  }
}
