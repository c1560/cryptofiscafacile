import Component from '@glimmer/component';
import { debug } from '@ember/debug';
import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';

import Transactions from 'cryptofiscafacile-gui/utils/transactions';

import type { Transaction, TxCategory } from 'cryptofiscafacile-wasm';
interface TxTableTxItemArgs {
  onUpdateCategory: (tx: Transaction) => void;
  onSelected: (tx: Transaction, value: boolean) => void;
  allowedCategoryForSelection?: TxCategory;
  secretMode?: boolean;
  selected: boolean;
  tx: Transaction;
}

export default class TxTableTxItem extends Component<TxTableTxItemArgs> {
  @action
  onCopy() {
    let copyText = this.args.tx.id;

    navigator.clipboard.writeText(copyText);

    debug('Copied the text: ' + copyText);
  }

  @action
  onUpdateCategory() {
    this.args.onUpdateCategory(this.args.tx);
  }

  @action
  onSelect(event: Event) {
    this.args.onSelected(this.args.tx, (event.target as HTMLInputElement).checked);
  }

  get checkableId() {
    return guidFor(this);
  }

  get checkable() {
    return Transactions.isUserCategorizable(this.args.tx);
  }

  get checkableDisabled() {
    return (
      this.args.allowedCategoryForSelection !== undefined &&
      this.args.tx.category !== this.args.allowedCategoryForSelection
    );
  }
}
