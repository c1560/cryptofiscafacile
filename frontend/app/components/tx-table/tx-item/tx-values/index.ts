import Component from '@glimmer/component';

import type { TxValue } from 'cryptofiscafacile-wasm';

interface TxValuesArgs {
  values: TxValue[];
  onShowCoinDetail: (symbol: string) => void;
  secretMode?: boolean;
}

export default class TxTableTxItemTxValues extends Component<TxValuesArgs> {
  get txEmpty(): boolean {
    return this.args.values.length === 0;
  }
}
