import Component from '@glimmer/component';
import { action } from '@ember/object';

import type { TxValue } from 'cryptofiscafacile-wasm';

interface TxTableTxItemChangeFiatInputFiatArgs {
  value: TxValue;
  onChange: (values: number, old: number) => void;
}

export default class TxTableTxItemChangeFiatInputFiat extends Component<TxTableTxItemChangeFiatInputFiatArgs> {
  @action
  updateValue(e: { target: HTMLInputElement }) {
    if (e.target.value) {
      this.args.onChange(Number(e.target.value), this.args.value.quantity);
    } else {
      this.args.onChange(0, 0);
    }
  }
}
