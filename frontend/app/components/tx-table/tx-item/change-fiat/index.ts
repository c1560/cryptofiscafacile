import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { debug } from '@ember/debug';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';
import type { TxValue } from 'cryptofiscafacile-wasm';

interface TxTableTxItemChangeFiatArgs {
  id: string;
  values: TxValue[];
  type: string;
}

export default class TxTableTxItemChangeFiat extends Component<TxTableTxItemChangeFiatArgs> {
  @service declare wasm: WasmService;

  @tracked fiatValue: number[] = [];
  @tracked changeModal = false;
  @tracked toConfirm = false;

  @tracked isInvalid = '';

  @action
  selectedValue(val: number, oldValue: number) {
    if (val !== 0 && oldValue !== 0) {
      this.fiatValue = this.fiatValue.map((v) => {
        if (oldValue === v) {
          return val;
        } else {
          return v;
        }
      });
      this.toConfirm = true;
      this.isInvalid = '';
    } else {
      this.toConfirm = false;
    }
  }

  @action
  modalVal() {
    this.args.values.forEach((element) => {
      this.fiatValue.push(element.quantity);
    });

    this.changeModal = true;
  }

  @action
  async modifyFiat() {
    this.changeModal = false;
    debug('Modify fiat for transaction with id : ' + this.args.id + ' with value : ' + this.fiatValue);

    try {
      await this.wasm.setValueTask.perform(this.args.id, this.fiatValue);
      await this.wasm.getTransactionsTask.perform();
    } catch (error) {
      debug('Modification of fiat : ' + (error as Error).message);
    }
  }
  @action
  modifyFiatValid() {
    this.isInvalid = 'is-invalid';
  }

  @action
  close() {
    this.changeModal = false;
    this.fiatValue = [];
    this.toConfirm = false;
    this.isInvalid = '';
  }
}
