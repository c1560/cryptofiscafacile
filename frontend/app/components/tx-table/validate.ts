import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { debug } from '@ember/debug';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import TxIco from 'cryptofiscafacile-gui/objects/tx-ico';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type Application from 'cryptofiscafacile-gui/services/application';
import type Wasm from 'cryptofiscafacile-gui/services/wasm';
import type { ConsistencyError, ConsistencyTx } from 'cryptofiscafacile-wasm';

interface ValidateArgs {
  modalSt: boolean;
}

export default class Validate extends Component<ValidateArgs> {
  @service declare wasm: Wasm;
  @service declare application: Application;

  @tracked validState = false;
  @tracked icoState = false;
  @tracked consistencyState = false;
  @tracked consistencyStateNegativeBalance = false;
  @tracked resultWithdrawal: TxIco[] = [];

  @tracked resultDeposit: TxIco[] = [];
  @tracked checkedWithdraw: TxIco[] = [];

  @tracked consistencyRelated: ConsistencyTx[][] = [];
  @tracked consistencyAlmostTx: ConsistencyError[] = [];
  @tracked firstTx = '';

  @tracked consistencyAlmost = false;
  @tracked mergeAlmostBtn = false;

  @tracked tabMerge: { id1: string; id2: string } = { id1: '', id2: '' };

  @tracked mergeBtn = false;
  @tracked isTabId = false;

  @tracked mergeNegBtn = false;
  @tracked toShow = false;
  @tracked consistencyNegativeBalanceMergeable: ConsistencyTx[] = [];

  multiId: string[] = [];
  multiId2: string[] = [];
  coin = '';
  time: Date = new Date();
  dateMax = new Date();
  dateMin = new Date();
  index = 0;
  index2 = 0;
  tabId: string[] = [];
  txMerged = false;
  firstid = '';
  secondid = '';

  /**
   * STEP 1 - TO/FROM consistency
   */
  @action
  setCheckConsistency() {
    this.wasm.checkConsistencyCategVsToFromTask.perform();
    this.consistencyState = true;

    this.consistencyAlmost = false;
    this.consistencyStateNegativeBalance = false;
    this.validState = false;
    this.icoState = false;
  }

  get consistencyCategVsToFrom() {
    return this.wasm.consistencyCategVsToFrom?.errors;
  }

  /**
   * STEP 2 - TX to merge
   */
  get unprocessedTx() {
    return this.wasm.consistencyAlmost?.errors ?? [];
  }

  @action
  setCheckConsistencyAlmost() {
    this.consistencyState = false;
    this.wasm.checkConsistencyAlmostTransferTask.perform();
    this.consistencyAlmost = true;

    this.consistencyStateNegativeBalance = false;
    this.validState = false;
    this.icoState = false;
  }

  get errorAlmostRelated() {
    let related: ConsistencyTx[][] = [];

    this.unprocessedTx.forEach((element) => {
      for (let i = 0; i < this.multiId.length; i++) {
        if (element.tx.id === this.multiId[i]) {
          related.push(element.related);
        }
      }
    });

    return related;
  }

  get coinAlmostRelated() {
    let coin = '';

    this.unprocessedTx.forEach((element) => {
      if (element.tx.id === this.firstTx) {
        coin = element.tx.coin;
      }
    });

    return coin;
  }

  get timeAlmostRelated() {
    let time: Date = new Date();

    this.unprocessedTx.forEach((element) => {
      if (element.tx.id === this.firstTx) {
        time = element.tx.date;
      }
    });

    return time;
  }

  addDays(days: number, date: Date) {
    let result = new Date(date);

    result.setDate(result.getDate() + days);

    return result;
  }

  @action
  selectAlmost(e: { target: HTMLInputElement }) {
    if (e.target.checked) {
      this.index += 1;
      this.mergeAlmostBtn = false;

      // Condition to store the first datas for tx checked, for which we should retain the coin && to filter by Coin/Datemax/Datemin
      if (this.index === 1) {
        this.firstTx = e.target.id;
        this.coin = this.coinAlmostRelated;
        this.time = this.timeAlmostRelated;
        this.dateMax = this.addDays(1, this.time);
        this.dateMin = this.addDays(-1, this.time);
        // Load a new filter on TX in error
        this.consistencyAlmostTx = this.unprocessedTx.filter(
          (tx) => tx.tx.coin === this.coin && (tx.tx.date <= this.dateMax || tx.tx.date >= this.dateMin),
        );
      }

      // Work on TX in error related TX(s)
      this.multiId.push(e.target.id);
      this.consistencyRelated = this.errorAlmostRelated;
      // Remove potential duplicabtes in consistencyRelated
      this.consistencyRelated = [...new Map(this.consistencyRelated.map((v) => [v[0]?.id, v])).values()];
      // Allow displaying the second part of the modal
      this.toShow = true;
    } else if (e.target.checked === false) {
      this.index -= 1;
      this.multiId = this.multiId.filter((el) => el !== e.target.id);
      this.consistencyRelated = this.errorAlmostRelated;

      if (this.index === 0) {
        this.coin = '';
        this.firstTx = '';
        this.time = new Date();
        this.dateMax = new Date();
        this.dateMin = new Date();
        this.toShow = false;
        this.consistencyAlmostTx = [];
      }
    }
  }
  @action
  selectAlmost2(e: { target: HTMLInputElement }) {
    if (e.target.checked) {
      this.index2 += 1;
      this.multiId2.push(e.target.id);
      this.mergeAlmostBtn = true;
    } else if (e.target.checked === false) {
      this.index2 -= 1;
      this.multiId2 = this.multiId2.filter((el) => el !== e.target.id);

      if (this.index2 === 0) {
        this.mergeAlmostBtn = false;
      }
    }
  }

  @action
  async mergeAlmost() {
    this.toShow = false;

    if (this.multiId && this.multiId2) {
      if (this.multiId.length > 1 && this.multiId2.length === 1) {
        const id = this.multiId2[0];

        if (id) {
          this.multiId.forEach(async (el) => {
            await this.wasm.mergeTransactionsTask.perform(el, id);
            await this.wasm.checkConsistencyAlmostTransferTask.perform();
            await this.wasm.getTransactionsTask.perform();
            debug('1-Merge transactions with id1 : ' + el + ', and id2 : ' + id);
          });
        } else {
          debug('MultiId2 is empty');
        }
      } else if (this.multiId2.length > 1 && this.multiId.length === 1) {
        const id = this.multiId[0];

        if (id) {
          this.multiId2.forEach(async (el) => {
            await this.wasm.mergeTransactionsTask.perform(id, el);
            await this.wasm.checkConsistencyAlmostTransferTask.perform();
            await this.wasm.getTransactionsTask.perform();
            debug('2Merge transactions with id1 : ' + id + ', and id2 : ' + el);
          });
        } else {
          debug('MultiId is empty');
        }
      } else if (this.multiId2.length === 1 && this.multiId.length === 1) {
        const id1 = this.multiId[0];
        const id2 = this.multiId2[0];

        if (id1 && id2) {
          await this.wasm.mergeTransactionsTask.perform(id1, id2);
          await this.wasm.checkConsistencyAlmostTransferTask.perform();
          await this.wasm.getTransactionsTask.perform();
          debug('Merge transactions with id1 : ' + id1 + ', and id2 : ' + id2);
        } else {
          debug('MultiId and MultiId2 are empty');
        }
      } else {
        debug('Error in multiId or multiId2 array!');
      }
    } else {
      debug('Try to merge without TX selected !');
    }

    this.index = 0;
    this.index2 = 0;
    this.mergeAlmostBtn = false;
    this.firstTx = '';
    this.coin = '';
    this.time = new Date();
    this.dateMax = new Date();
    this.dateMin = new Date();
    this.multiId = [];
    this.multiId2 = [];
    this.consistencyRelated = [];
    this.consistencyAlmostTx = [];
  }

  /**
   * STEP 3 - Negative Balances
   */
  @action
  setCheckConsistencyNegativeBalance() {
    this.consistencyState = false;
    this.consistencyAlmost = false;
    this.wasm.checkConsistencyNegativeBalanceTask.perform();
    this.consistencyStateNegativeBalance = true;

    this.validState = false;
    this.icoState = false;
  }

  get consistencyNegativeBalance() {
    return this.wasm.consistencyNegativeBalance?.errors ?? [];
  }

  get showMergeable() {
    let related: ConsistencyTx[] = [];

    this.consistencyNegativeBalance.forEach((element) => {
      if (element.tx.id === this.firstid) {
        related = element.related;
      }
    });

    return related;
  }
  @action
  selectNeg(e: { target: HTMLInputElement }) {
    let id = e.target.id;

    if (e.target.checked) {
      this.mergeNegBtn = false;
      this.firstid = id;
      this.consistencyNegativeBalanceMergeable = this.showMergeable;
      this.toShow = true;
    } else {
      this.toShow = false;
    }
  }
  @action
  selectNeg2(e: { target: HTMLInputElement }) {
    let id2 = e.target.id;

    if (e.target.checked) {
      this.toShow = true;
      this.secondid = id2;

      this.mergeNegBtn = true;
    } else {
      this.toShow = false;
      this.mergeNegBtn = false;
    }
  }

  @action
  async mergeNeg() {
    this.toShow = false;

    if (this.firstid && this.secondid) {
      await this.wasm.mergeTransactionsTask.perform(this.firstid, this.secondid);
      await this.wasm.checkConsistencyNegativeBalanceTask.perform();
      await this.wasm.getTransactionsTask.perform();
      debug('Merge transactions with id1 : ' + this.firstid + ', and id2 : ' + this.secondid);
    } else {
      debug('Try to merge without TX selected !');
    }

    this.mergeNegBtn = false;
  }

  /**
   * STEP 4 - ICO
   */
  @action
  onValidation() {
    let to: TxIco = new TxIco();

    this.consistencyState = false;
    this.consistencyAlmost = false;
    this.consistencyStateNegativeBalance = false;
    this.validState = true;
    this.icoState = false;

    //1st - List all WITHDRAWALS TXs
    let res1 = this.wasm.transactions.filter((tx) => 'Retrait' === tx.category || 'ICO' === tx.category);
    let res2 = this.wasm.transactions.filter((tx) => 'ICO' === tx.category);
    let res = res1.concat(res2);

    for (const from of res) {
      to = new TxIco();

      to.id = from.id;
      to.wallet = from.wallet;
      to.date = from.date;

      from.from.forEach((element) => {
        to.quantity = element.quantity;
        to.currency = element.currency;
      });

      this.resultWithdrawal.push(to);
    }

    // Remove potential duplicabtes in resultWithdrawal
    this.resultWithdrawal = this.resultWithdrawal.filter((v, i, a) => a.findIndex((v2) => v2.id === v.id) === i);
  }

  @action
  toggleCheck(e: { target: HTMLInputElement }) {
    if (e.target.checked) {
      this.tabId.push(e.target.id);
      this.isTabId = true;
    } else if (e.target.checked === false) {
      this.tabId = this.tabId.filter((el) => el !== e.target.id);
      this.isTabId = false;
    }
  }
  @action
  setIco() {
    this.consistencyState = false;
    this.consistencyAlmost = false;
    this.consistencyStateNegativeBalance = false;
    this.validState = false;
    this.tabId.forEach((id) => {
      this.resultWithdrawal.forEach((element) => {
        if (element.id === id) {
          this.checkedWithdraw.push(element);
        }
      });
    });

    //2nd - List all DEPOSITS TXs
    let tx: TxIco = new TxIco();
    let res = this.wasm.transactions.filter((tx) => 'Dépôt' === tx.category);

    for (const from of res) {
      tx = new TxIco();

      tx.id = from.id;
      tx.wallet = from.wallet;
      tx.date = from.date;

      from.to.forEach((element) => {
        tx.quantity = element.quantity;
        tx.currency = element.currency;
      });

      this.resultDeposit.push(tx);
    }

    // Remove potential duplicabtes in resultDeposit
    this.resultDeposit = this.resultDeposit.filter((v, i, a) => a.findIndex((v2) => v2.id === v.id) === i);

    this.icoState = true;

    this.tabId = [];
    this.resultWithdrawal = [];
  }

  @action
  selectIds(idWith: string, idDep: string) {
    if (idWith) {
      this.tabMerge.id1 = idWith;
    }

    if (idDep) {
      this.tabMerge.id2 = idDep;
    }

    if (this.tabMerge.id1 && this.tabMerge.id2) {
      this.mergeBtn = true;
    } else {
      this.mergeBtn = false;
    }
  }
  @action
  async merge() {
    if (this.tabMerge.id1 && this.tabMerge.id2) {
      await this.wasm.setCategoryTask.perform(this.tabMerge.id1, TxCategory.ICO);
      await this.wasm.mergeTransactionsTask.perform(this.tabMerge.id1, this.tabMerge.id2);
      await this.wasm.getTransactionsTask.perform();

      this.checkedWithdraw = this.checkedWithdraw.filter((e) => e.id !== this.tabMerge.id1);

      this.resultDeposit = this.resultDeposit.filter((e) => e.id !== this.tabMerge.id2);
    } else {
      debug('Try to merge without TX selected !');
    }

    this.mergeBtn = false;
    this.txMerged = true;
  }

  @action
  hidden1() {
    this.toShow = false;
  }
  @action
  hidden2() {
    this.mergeAlmostBtn = false;
    this.toShow = false;

    this.coin = '';
    this.index = 0;
    this.index2 = 0;
    this.firstTx = '';
    this.time = new Date();
    this.dateMax = new Date();
    this.dateMin = new Date();
    this.multiId = [];
    this.multiId2 = [];
    this.consistencyRelated = [];
    this.toShow = false;
    this.consistencyAlmostTx = [];
  }
  @action
  hidden3() {
    this.mergeAlmostBtn = false;
    this.mergeNegBtn = false;
    this.toShow = false;
  }
  @action
  hidden4() {
    this.mergeAlmostBtn = false;
    this.mergeNegBtn = false;
    this.toShow = false;

    if (this.icoState) {
      this.resultWithdrawal = [];
    } else {
      this.resultWithdrawal = [];
      this.checkedWithdraw = [];
    }

    this.isTabId = false;
  }
  @action
  hidden5() {
    this.mergeAlmostBtn = false;
    this.mergeNegBtn = false;
    this.toShow = false;
    this.resultWithdrawal = [];
    this.resultDeposit = [];
    this.checkedWithdraw = [];
    this.tabId = [];
    this.tabMerge = { id1: '', id2: '' };
    this.isTabId = false;
    this.mergeBtn = false;
  }

  @action
  close() {
    this.consistencyState = false;
    this.consistencyAlmost = false;
    this.consistencyStateNegativeBalance = false;
    this.validState = false;
    this.icoState = false;

    this.mergeBtn = false;
    this.mergeNegBtn = false;
    this.mergeAlmostBtn = false;
    this.toShow = false;

    this.coin = '';
    this.index = 0;
    this.index2 = 0;
    this.firstTx = '';
    this.time = new Date();
    this.dateMax = new Date();
    this.dateMin = new Date();
    this.multiId = [];
    this.multiId2 = [];
    this.consistencyRelated = [];
    this.toShow = false;
    this.consistencyAlmostTx = [];
  }

  /**
   * COPY TO CLIPBOARD PART
   */
  @action
  onSuccess() {
    debug('Copy to clipboard success');
  }
  @action
  onError() {
    debug('Copy to clipboard error... Please try again');
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@glimmer/component' {
  interface Registry {
    validate: Validate;
  }
}
