import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import { SortOrder } from 'cryptofiscafacile-gui/enums/sort-order';
import Sorts from 'cryptofiscafacile-gui/utils/sorts';
import Transactions from 'cryptofiscafacile-gui/utils/transactions';
import { tracked } from 'tracked-built-ins';

import Option from '../option';
import PageSizeModifier from './page-size-modifier';

import type WasmService from 'cryptofiscafacile-gui/app/services/wasm';
import type { Transaction } from 'cryptofiscafacile-wasm';

interface TxTableArgs {
  catSortOrder: SortOrder;
  dateSortOrder: SortOrder;
  pfSortOrder: SortOrder;
  userPage: number;
  pageSize?: number;
  searchedItem: string;
  selectedCategory: string[];
  selectedCoins: string[];
  selectedDates: Date[];
  selectedWallet: string[];
  onCatSortOrderChanged: (value: SortOrder) => void;
  onDateSortOrderChanged: (value: SortOrder) => void;
  onPfSortOrderChanged: (value: SortOrder) => void;
  onUserPageChanged: (value: number) => void;
  onPageSizeChanged: (value: number) => void;
  onSelectedCategoryChanged: (values: string[]) => void;
  onSelectedCoinsChanged: (values: string[]) => void;
  onSelectedDatesChanged: (values: Date[]) => void;
  onSelectedWalletChanged: (values: string[]) => void;
  onSearchedItemChanged: (value: string) => void;
}

export default class TxTable extends Component<TxTableArgs> {
  @service declare wasm: WasmService;

  @tracked defaultPageSize = 10;

  @tracked amountsHidden = false;

  @tracked coinDetailModalSymbol = '';
  @tracked coinDetailModalVisible = false;

  selectedTxs: Set<Transaction> = tracked(new Set<Transaction>());

  pageSizeModifier = PageSizeModifier;

  get categories() {
    const allCategories = this.wasm.transactions.map((tx) => tx.category);

    return [...new Set(allCategories)].sort((c1, c2) => c1.localeCompare(c2));
  }

  get categoryOptions() {
    return this.categories.map((c) => new Option(c, this.args.selectedCategory.includes(c)));
  }

  get hasSelectedCategory() {
    return this.categoryOptions.some((w) => w.selected);
  }

  get coins() {
    const allCoins = [
      ...this.wasm.transactions.flatMap((tx) => tx.to),
      ...this.wasm.transactions.flatMap((tx) => tx.from),
      ...this.wasm.transactions.flatMap((tx) => tx.fee),
    ].map((value) => value.currency);

    return [...new Set(allCoins)].sort((c1, c2) => c1.localeCompare(c2));
  }

  get coinOptions() {
    return this.coins.map((c) => new Option(c, this.args.selectedCoins.includes(c)));
  }

  get hasSelectedCoin() {
    return this.coinOptions.some((w) => w.selected);
  }

  get wallets() {
    const allWallets = this.wasm.transactions.map((tx) => tx.wallet);

    return [...new Set(allWallets)].sort((w1, w2) => w1.localeCompare(w2));
  }

  get walletOptions() {
    return this.wallets.map((w) => new Option(w, this.args.selectedWallet.includes(w)));
  }

  get hasSelectedWallet() {
    return this.walletOptions.some((w) => w.selected);
  }

  get totalTx() {
    return this.sortedTransactions.length;
  }

  get filteredTransactions() {
    let filteredTransactions = this.#filterTransactionsOnSelectedCategory(this.wasm.transactions);

    filteredTransactions = this.#filterTransactionsOnSelectedCoins(filteredTransactions);

    filteredTransactions = this.#filterTransactionsOnSelectedDates(filteredTransactions);

    filteredTransactions = this.#filterTransactionsOnSelectedWallet(filteredTransactions);

    //SEARCHING PARTS
    filteredTransactions = this.#filterTransactionsOnSearchItem(filteredTransactions);

    return filteredTransactions;
  }

  get isFecthingTransactions() {
    return this.wasm.getTransactionsTask.isRunning;
  }

  #filterTransactionsOnSelectedCoins(transactions: Transaction[]) {
    if (this.args.selectedCoins.includes(Option.ALL)) {
      return transactions;
    } else {
      return transactions.filter((tx) => this.#getCoins(tx).find((c) => this.args.selectedCoins.includes(c)));
    }
  }

  #getCoins(transaction: Transaction) {
    return [
      ...new Set([
        ...transaction.fee.flatMap((value) => value.currency),
        ...transaction.from.flatMap((value) => value.currency),
        ...transaction.to.flatMap((value) => value.currency),
      ]),
    ];
  }

  #filterTransactionsOnSelectedCategory(transactions: Transaction[]) {
    if (this.args.selectedCategory.includes(Option.ALL)) {
      return transactions;
    } else {
      return transactions.filter((tx) => this.args.selectedCategory.includes(tx.category));
    }
  }

  #filterTransactionsOnSelectedDates(transactions: Transaction[]) {
    const fromDate = this.args.selectedDates[0];
    const toDate = this.args.selectedDates[1];

    if (this.args.selectedDates.length === 0 || !fromDate || !toDate) {
      return transactions;
    } else {
      return transactions.filter((tx) => tx.date >= fromDate && tx.date <= toDate);
    }
  }

  #filterTransactionsOnSelectedWallet(transactions: Transaction[]) {
    if (this.args.selectedWallet.includes(Option.ALL)) {
      return transactions;
    } else {
      return transactions.filter((tx) => this.args.selectedWallet.includes(tx.wallet));
    }
  }

  get sortedTransactions() {
    if (this.args.catSortOrder !== SortOrder.NONE) {
      return this.filteredTransactions
        .slice()
        .sort(Sorts.text((tx: Transaction) => tx.category, this.args.catSortOrder));
    }

    if (this.args.dateSortOrder !== SortOrder.NONE) {
      return this.filteredTransactions.slice().sort(Sorts.date((tx: Transaction) => tx.date, this.args.dateSortOrder));
    }

    if (this.args.pfSortOrder !== SortOrder.NONE) {
      return this.filteredTransactions.slice().sort(Sorts.text((tx: Transaction) => tx.wallet, this.args.pfSortOrder));
    }

    return this.filteredTransactions;
  }

  get pageSortedTransactions() {
    return this.sortedTransactions
      ? this.sortedTransactions.slice((this.page - 1) * this.pageSize, this.page * this.pageSize)
      : [];
  }

  get maxPage() {
    return Math.max(1, Math.ceil(this.sortedTransactions.length / this.pageSize));
  }

  get page() {
    return Math.min(this.maxPage, this.args.userPage);
  }

  get pageSize() {
    return this.args.pageSize ?? this.defaultPageSize;
  }

  @action
  onUserPageChanged(page: number): void {
    this.args.onUserPageChanged(page);
    this.selectedTxs.clear();
  }

  @action
  setPageSize(e: { target: HTMLSelectElement }) {
    this.args.onPageSizeChanged(Number(e.target.value));
  }

  @action
  setDefaultPageSizeNumber(pageSize: number) {
    this.defaultPageSize = pageSize;
  }

  @action
  toggleAmountsHidden() {
    this.amountsHidden = !this.amountsHidden;
  }

  @action
  sortCat(currentOrder: SortOrder) {
    this.args.onDateSortOrderChanged(SortOrder.NONE);
    this.args.onPfSortOrderChanged(SortOrder.NONE);
    this.args.onCatSortOrderChanged(currentOrder === SortOrder.DESC ? SortOrder.ASC : SortOrder.DESC);
  }

  @action
  sortDate(currentOrder: SortOrder) {
    this.args.onPfSortOrderChanged(SortOrder.NONE);
    this.args.onCatSortOrderChanged(SortOrder.NONE);
    this.args.onDateSortOrderChanged(currentOrder === SortOrder.DESC ? SortOrder.ASC : SortOrder.DESC);
  }

  @action
  sortPf(currentOrder: SortOrder) {
    this.args.onDateSortOrderChanged(SortOrder.NONE);
    this.args.onCatSortOrderChanged(SortOrder.NONE);
    this.args.onPfSortOrderChanged(currentOrder === SortOrder.DESC ? SortOrder.ASC : SortOrder.DESC);
  }

  @action
  selectCategory(e: { target: HTMLSelectElement }) {
    this.args.onSelectedCategoryChanged(Array.from(e.target.selectedOptions).map((elt) => elt.value));
    this.onUserPageChanged(1);
  }

  @action
  selectCoin(e: { target: HTMLSelectElement }) {
    this.args.onSelectedCoinsChanged(Array.from(e.target.selectedOptions).map((elt) => elt.value));
    this.onUserPageChanged(1);
  }

  @action
  selectDates(dates: Date[]) {
    this.args.onSelectedDatesChanged(dates);
    this.onUserPageChanged(1);
  }

  @action
  selectWallet(e: { target: HTMLSelectElement }) {
    this.args.onSelectedWalletChanged(Array.from(e.target.selectedOptions).map((elt) => elt.value));
    this.onUserPageChanged(1);
  }

  /**
   * SEARCHING PART
   */
  #filterTransactionsOnSearchItem(transactions: Transaction[]) {
    if (this.args.searchedItem.length === 0) {
      return transactions;
    } else {
      const searchString = this.args.searchedItem
        .toLowerCase()
        .normalize('NFD')
        .replace(/\p{Diacritic}/gu, '');

      return transactions.filter(
        (tx) =>
          this.#getCoins(tx).find((c) =>
            c
              .toLowerCase()
              .normalize('NFD')
              .replace(/\p{Diacritic}/gu, '')
              .includes(searchString),
          ) ||
          tx['category']
            .toLowerCase()
            .normalize('NFD')
            .replace(/\p{Diacritic}/gu, '')
            .includes(searchString) ||
          tx['wallet']
            .toLowerCase()
            .normalize('NFD')
            .replace(/\p{Diacritic}/gu, '')
            .includes(searchString),
      );
    }
  }

  @action
  searchItem(e: { target: HTMLSelectElement }) {
    this.args.onSearchedItemChanged(e.target.value);
    this.onUserPageChanged(1);
  }

  /**
   * Coin detail modal
   */
  @action
  onShowCoinDetail(symbol: string) {
    this.coinDetailModalSymbol = symbol;
    this.coinDetailModalVisible = true;
  }

  @action
  onCoinDetailModalHidden() {
    this.coinDetailModalSymbol = '';
    this.coinDetailModalVisible = false;
  }

  /**
   * UPDATE CATEG PART
   */
  @tracked updateCategoryModalVisible = false;
  @tracked updateCategoryTxs: Transaction[] = [];

  @action
  onUpdateCategory(tx: Transaction) {
    this.updateCategoryTxs = [tx];
    this.updateCategoryModalVisible = true;
  }

  @action
  onMultiUpdateCategory() {
    this.updateCategoryTxs = this.visibleSelectedTxs;
    this.updateCategoryModalVisible = true;
  }

  @action
  onUpdateCategoryModalHidden() {
    this.updateCategoryModalVisible = false;
    this.updateCategoryTxs = [];
  }

  // Multi selection
  get visibleSelectedTxs() {
    return [...this.selectedTxs.keys()].filter((tx) => this.pageSortedTransactions.includes(tx));
  }

  get selectedCategory() {
    return this.visibleSelectedTxs[0]?.category;
  }

  @action
  isTxSelected(tx: Transaction) {
    return this.selectedTxs.has(tx);
  }

  @action
  onTxSelected(tx: Transaction, checked: boolean) {
    if (checked) {
      this.selectedTxs.add(tx);
    } else {
      this.selectedTxs.delete(tx);
    }
  }

  get allSelectionIndeterminate() {
    const selectedCategory = this.selectedCategory;

    if (selectedCategory) {
      const sameCategoryCategorizableTxs = this.pageSortedTransactions.filter(
        (tx) => Transactions.isUserCategorizable(tx) && tx.category === selectedCategory,
      );

      return sameCategoryCategorizableTxs.length !== this.visibleSelectedTxs.length;
    }

    return false;
  }

  get allTxsSelected() {
    const selectedCategory = this.selectedCategory;

    if (selectedCategory) {
      const sameCategoryCategorizableTxs = this.pageSortedTransactions.filter(
        (tx) => Transactions.isUserCategorizable(tx) && tx.category === selectedCategory,
      );

      return sameCategoryCategorizableTxs.length === this.visibleSelectedTxs.length;
    }

    return false;
  }

  set allTxsSelected(checked: boolean) {
    if (checked) {
      const categorizableTxs = this.pageSortedTransactions.filter((tx) => Transactions.isUserCategorizable(tx));

      if (categorizableTxs.length > 0) {
        if (!this.selectedCategory && categorizableTxs[0]) {
          this.selectedTxs.add(categorizableTxs[0]);
        }

        const selectedCategory = this.selectedCategory;

        if (selectedCategory) {
          categorizableTxs.filter((tx) => tx.category === selectedCategory).forEach((tx) => this.selectedTxs.add(tx));
        }
      }
    } else {
      this.selectedTxs.clear();
    }
  }
}
