import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { debug } from '@ember/debug';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { waitFor } from '@ember/test-waiters';

import { TransfertType } from 'cryptofiscafacile-gui/enums/transfert-type';
import { TxCategory } from 'cryptofiscafacile-wasm';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';
import type { Transaction } from 'cryptofiscafacile-wasm';

interface TxTableCategUpdateModalArgs {
  onHidden: () => void;
  open: boolean;
  txs: Transaction[];
}

export default class TxTableCategUpdateModal extends Component<TxTableCategUpdateModalArgs> {
  @service declare wasm: WasmService;

  @tracked toConfirm = false;
  @tracked categToUpdate = TxCategory.NONE;

  get categories() {
    if (this.transfertType === TransfertType.DEPOSIT) {
      return [
        TxCategory.AIR_DROPS,
        TxCategory.CASH_IN,
        TxCategory.GIFTS,
        TxCategory.FORKS,
        TxCategory.INTERESTS,
        TxCategory.MINING,
        TxCategory.REFERRALS,
        TxCategory.COMMERCIAL_REBATES,
        TxCategory.DONT_CARE,
      ];
    }

    if (this.transfertType === TransfertType.WITHDRAWAL) {
      return [TxCategory.CASH_OUT, TxCategory.GIFTS, TxCategory.ICO, TxCategory.DONT_CARE];
    }

    return [];
  }

  get categoryModified() {
    return this.args.txs[0]?.category !== this.args.txs[0]?.original_category;
  }

  get transfertType() {
    if (this.args.txs.length === 0) {
      return undefined;
    } else {
      if (this.args.txs[0]?.category === TxCategory.DEPOSITS) {
        return TransfertType.DEPOSIT;
      } else if (this.args.txs[0]?.category === TxCategory.WITHDRAWALS) {
        return TransfertType.WITHDRAWAL;
      } else {
        if (this.args.txs[0]?.original_category === TxCategory.DEPOSITS) {
          return TransfertType.DEPOSIT;
        } else if (this.args.txs[0]?.original_category === TxCategory.WITHDRAWALS) {
          return TransfertType.WITHDRAWAL;
        }
      }
    }

    return undefined;
  }

  @action
  updateCateg(e: { target: HTMLSelectElement }) {
    this.toConfirm = true;
    this.categToUpdate = e.target.value as TxCategory;
  }

  @action
  @waitFor
  async modifyCateg() {
    if (this.args.txs.length > 0 && this.categToUpdate !== TxCategory.NONE) {
      for (let tx of this.args.txs) {
        await this.wasm.setCategoryTask.perform(tx.id, this.categToUpdate);
      }

      await this.wasm.getTransactionsTask.perform();
    } else {
      debug('Error in modifyCateg - category not updated...');
    }

    this.#closeModal();
  }

  @action
  async revertCateg() {
    if (this.args.txs.length > 0 && this.categoryModified) {
      for (let tx of this.args.txs) {
        await this.wasm.revertCategoryTask.perform(tx.id);
      }

      await this.wasm.getTransactionsTask.perform();
    } else {
      debug('Error in revertCateg - category not reverted...');
    }

    this.#closeModal();
  }

  #closeModal() {
    this.toConfirm = false;
    this.args.onHidden();
  }
}
