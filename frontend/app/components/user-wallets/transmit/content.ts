import Component from '@glimmer/component';
import { debug } from '@ember/debug';
import { action } from '@ember/object';

interface UserWalletsTransmitContentArgs {
  id: string;
  label: string;
}

export default class UserWalletsTransmitContent extends Component<UserWalletsTransmitContentArgs> {
  @action
  onSuccess() {
    debug('Copy to clipboard success');
  }
  @action
  onError() {
    debug('Copy to clipboard error... Please try again');
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@glimmer/component' {
  interface Registry {
    'user-wallets-transmit-content': UserWalletsTransmitContent;
  }
}
