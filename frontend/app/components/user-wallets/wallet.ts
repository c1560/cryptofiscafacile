import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

import type { FileUsageStatus } from 'cryptofiscafacile-wasm';

interface UserWalletsWalletArgs {
  wallet: FileUsageStatus;
}

export default class UserWalletsWallet extends Component<UserWalletsWalletArgs> {
  @tracked addressesCollapsed = true;

  @action
  toggle() {
    this.addressesCollapsed = !this.addressesCollapsed;
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@glimmer/component' {
  interface Registry {
    'user-wallets-wallet': UserWalletsWallet;
  }
}
