import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import { SortOrder } from 'cryptofiscafacile-gui/enums/sort-order';
import Sorts from 'cryptofiscafacile-gui/utils/sorts';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';
import type { Transaction } from 'cryptofiscafacile-wasm';

interface ReportingAddWalletModalArgs {
  onHide: () => void;
  onWalletAdded: () => void;
  open: boolean;
  txs: Transaction[];
}

export default class UserWalletsAddModal extends Component<ReportingAddWalletModalArgs> {
  @service declare wasm: WasmService;

  @tracked userPage = 1;

  pageSize = 12;

  constructor(owner: unknown, args: ReportingAddWalletModalArgs) {
    super(owner, args);

    if (!this.wasm.walletDefs) {
      this.wasm.getWalletDefinitionsTask.perform();
    }
  }

  @action
  reset() {
    this.invalidSelection = false;
    this.selected = '';
    this.walletNameFilter = '';
  }

  get custodialWallets() {
    const searchString = this.walletNameFilter
      .toLowerCase()
      .normalize('NFD')
      .replace(/\p{Diacritic}/gu, '');

    return (
      (this.wasm.walletDefs ?? [])
        .filter((w) => w.custodial)
        .filter((w) =>
          w.name
            .toLowerCase()
            .normalize('NFD')
            .replace(/\p{Diacritic}/gu, '')
            .includes(searchString),
        )
        .sort(Sorts.text((w) => w.name, SortOrder.ASC)) ?? []
    );
  }

  get pageCustodialWallets() {
    return this.custodialWallets.slice((this.page - 1) * this.pageSize, this.page * this.pageSize);
  }

  get page() {
    return Math.min(this.maxPage, this.userPage);
  }

  get maxPage() {
    return Math.max(1, Math.ceil(this.custodialWallets.length / this.pageSize));
  }

  @action
  onUserPageChanged(page: number): void {
    this.userPage = page;
  }

  @tracked walletNameFilter = '';

  @action
  filterWallets(e: { target: HTMLSelectElement }) {
    this.walletNameFilter = e.target.value;
    this.userPage = 1;
  }

  @tracked selected = '';

  @action
  selectWallet(wallet: string) {
    this.invalidSelection = false;
    this.selected = wallet;
  }

  @tracked invalidSelection = false;

  @action
  async addWallet() {
    if (!this.selected) {
      this.invalidSelection = true;

      return;
    }

    await this.wasm.addEmptyWalletTask.perform(this.selected);
    this.args.onWalletAdded();
    this.args.onHide();
  }
}
