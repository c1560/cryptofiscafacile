import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { debug } from '@ember/debug';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import { SortOrder } from 'cryptofiscafacile-gui/enums/sort-order';
import Sorts from 'cryptofiscafacile-gui/utils/sorts';

import Option from '../option';

import type Wasm from 'cryptofiscafacile-gui/services/wasm';
import type { FileUsageStatus } from 'cryptofiscafacile-wasm';

export default class Wallets extends Component {
  @service declare wasm: Wasm;

  files!: FileList;
  len = 0;

  @tracked calculs = '';
  @tracked selectedWallet = [Option.ALL];
  @tracked pfSortOrder = SortOrder.NONE;
  @tracked nameSortOrder = SortOrder.NONE;

  inputFileElement: HTMLInputElement | undefined;
  @tracked didAddedFiles = false;

  get unprocessedHistory() {
    return this.wasm.history;
  }

  get sortedHistory() {
    if (this.nameSortOrder !== SortOrder.NONE) {
      return this.unprocessedHistory.slice().sort(Sorts.text((file: FileUsageStatus) => file.name, this.nameSortOrder));
    }

    if (this.pfSortOrder !== SortOrder.NONE) {
      return this.unprocessedHistory.slice().sort(Sorts.text((file: FileUsageStatus) => file.kind, this.pfSortOrder));
    }

    return this.unprocessedHistory;
  }

  get isAddingFiles() {
    return this.wasm.digestFilesTask.isRunning;
  }

  get progressValue() {
    return (
      (this.wasm.digestFilesTaskUseFileTasks.filter((t) => t.isFinished).length * 100) /
      this.wasm.digestFilesTaskFilesAmount
    );
  }

  @action
  sortPf(currentOrder: SortOrder) {
    this.nameSortOrder = SortOrder.NONE;
    this.pfSortOrder = currentOrder === SortOrder.DESC ? SortOrder.ASC : SortOrder.DESC;
  }
  @action
  sortName(currentOrder: SortOrder) {
    this.pfSortOrder = SortOrder.NONE;
    this.nameSortOrder = currentOrder === SortOrder.DESC ? SortOrder.ASC : SortOrder.DESC;
  }

  @action
  async upload(e: { target: HTMLInputElement }) {
    this.inputFileElement = e.target;

    this.files = this.inputFileElement.files as FileList;
    this.len = this.files.length;
  }

  @action
  async add() {
    if (this.files) {
      const filesAsArray = Array.from(this.files);

      this.didAddedFiles = true;
      await this.wasm.digestFilesTask.perform(filesAsArray);

      if (this.inputFileElement) {
        this.inputFileElement.value = '';
      }
    } else {
      debug('File(s) not recognized or no file loaded.');
    }
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@glimmer/component' {
  interface Registry {
    wallets: Wallets;
  }
}
