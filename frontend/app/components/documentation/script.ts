import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { debug } from '@ember/debug';
import { action } from '@ember/object';

export default class DocumentationScript extends Component {
  @tracked transmitModal = false;
  @tracked code = '';

  @action
  async onTransmit() {
    //Launch the modal
    this.transmitModal = true;

    let response = await (await fetch('/json_cdc_ex_exporter')).text();

    this.code = response;
  }

  @action
  hidden() {
    this.transmitModal = false;
  }
  @action
  close() {
    this.transmitModal = false;
  }

  @action
  onSuccess() {
    debug('Copy to clipboard success');
  }
  @action
  onError() {
    debug('Copy to clipboard error... Please try again');
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@glimmer/component' {
  interface Registry {
    'documentation-script': DocumentationScript;
  }
}
