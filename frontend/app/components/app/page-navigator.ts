import Component from '@glimmer/component';
import { action } from '@ember/object';

interface AppPageNavigatorArgs {
  userPage: number;
  maxPage: number;
  onUserPageChanged: (value: number) => void;
}

export default class AppPageNavigator extends Component<AppPageNavigatorArgs> {
  get page() {
    return Math.min(this.args.maxPage, this.args.userPage);
  }

  @action
  handlePageInputOnInput(inputEvent: Event): void {
    const userPage = Number((inputEvent.target as HTMLInputElement).value);

    this.args.onUserPageChanged(Math.min(userPage, this.args.maxPage));
  }

  @action
  firstPage() {
    this.args.onUserPageChanged(1);
  }

  @action
  lastPage() {
    this.args.onUserPageChanged(this.args.maxPage);
  }

  @action
  nextPage() {
    this.args.onUserPageChanged(Math.min(this.page + 1, this.args.maxPage));
  }

  @action
  previousPage() {
    this.args.onUserPageChanged(Math.max(1, this.page - 1));
  }
}
