import Component from '@glimmer/component';
import { warn } from '@ember/debug';

import { iconExists } from 'iconify-icon';

interface AppIconArgs {
  icon: string;
}

export default class AppIcon extends Component<AppIconArgs> {
  get icon() {
    let cffIcon = `cff:${this.args.icon}`;

    if (!iconExists(cffIcon)) {
      warn(`icon ${this.args.icon} not found. please add ${cffIcon} to offline-iconify-icons-loader.ts`, {
        id: 'icon-not-found',
      });

      return this.args.icon;
    }

    return cffIcon;
  }
}
