import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import type ApplicationService from 'cryptofiscafacile-gui/services/application';

interface AppNewVersionModalArgs {
  open: boolean;
  onHidden?: () => void;
}

export default class AppNewVersionModal extends Component<AppNewVersionModalArgs> {
  @service declare application: ApplicationService;

  @action
  close() {
    if (this.args.onHidden) {
      this.args.onHidden();
    }
  }
}
