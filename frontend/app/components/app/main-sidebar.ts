import Component from '@glimmer/component';
import { debug } from '@ember/debug';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import FileSaver from 'file-saver';
import JSZip from 'jszip';

import type Indexeddb from 'cryptofiscafacile-gui/services/indexeddb';

export default class MainSidebar extends Component {
  @service declare indexeddb: Indexeddb;

  @action
  async onExportConfig() {
    let zip = new JSZip();

    try {
      await this.indexeddb.exportDatabaseTask.perform('cff');
      await this.indexeddb.exportDatabaseTask.perform('wasm-db-confs');
      await this.indexeddb.exportDatabaseTask.perform('wasm-db-files');
      await this.indexeddb.exportDatabaseTask.perform('wasm-db-rates');
      await this.indexeddb.exportDatabaseTask.perform('wasm-db-txs');
    } catch (err) {
      debug('export : ' + (err as Error).message);
    } finally {
      let files = this.indexeddb.files;

      if (files) {
        files.forEach((file) => {
          zip.file(file.name + '.json', file);
        });
      }

      zip.generateAsync({ type: 'blob' }).then(function (blob) {
        FileSaver.saveAs(blob, 'cff-dbs.cff');
        debug('export-confs : ' + '__cff-dbs.cff__');
      });
    }
  }
}
