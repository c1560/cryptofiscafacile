import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

import { EngineStatus } from 'cryptofiscafacile-gui/enums/engine-status';

import type Wasm from 'cryptofiscafacile-gui/services/wasm';

export default class AppHeaderEngineIndicator extends Component {
  @service declare wasm: Wasm;

  get status() {
    return `engine-${this.wasm.status.toLowerCase()}`;
  }

  get tooltipText() {
    switch (this.wasm.status) {
      case EngineStatus.HALTED:
        return 'Le moteur CFF est arrêté, merci de recharger la page';
      case EngineStatus.LOADING:
        return 'Le moteur CFF démarre';
      case EngineStatus.RUNNING:
        return 'Le moteur CFF tourne parfaitement';
      default:
        throw new Error('Unknown status');
    }
  }
}
