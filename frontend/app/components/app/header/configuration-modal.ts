import Ember from 'ember';
import Component from '@glimmer/component';
import { debug } from '@ember/debug';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import config from 'cryptofiscafacile-gui/config/environment';
import { dropTask, restartableTask, timeout } from 'ember-concurrency';
import JSZip from 'jszip';
import { tracked } from 'tracked-built-ins';

import type ApplicationService from 'cryptofiscafacile-gui/services/application';
import type IndexeddbService from 'cryptofiscafacile-gui/services/indexeddb';
import type WasmService from 'cryptofiscafacile-gui/services/wasm';

interface HeaderConfigurationModalArgs {
  onHidden: () => void;
}

export default class HeaderConfigurationModal extends Component<HeaderConfigurationModalArgs> {
  @service declare application: ApplicationService;
  @service declare indexeddb: IndexeddbService;
  @service declare wasm: WasmService;

  @tracked _proxy?: string;
  @tracked activated = '';
  @tracked dropZoneDisabled = false;
  @tracked files: Blob[] = [];
  @tracked saveConfigTaskError = false;
  @tracked saveConfigTaskSuccess = false;

  constructor(owner: undefined, args: never) {
    super(owner, args);
    this.activated = this.wasm.verboseActivated === false ? 'ACTIVER' : 'DESACTIVER';
  }

  get production() {
    return config.environment === 'production';
  }

  get proxy() {
    if (this.wasm.userInfos) {
      if (this._proxy !== undefined) {
        return this._proxy;
      } else {
        return this.wasm.userInfos.proxy;
      }
    }

    return this._proxy;
  }

  set proxy(value) {
    this._proxy = value;
  }

  get proxyUpdated() {
    return this._proxy !== undefined;
  }

  get defaultProxy() {
    return config.APP.proxyUrl;
  }

  @action
  async onImport() {
    await this.indexeddb.cleanDatabases();

    if (this.files.length > 0) {
      for (const element of this.files) {
        try {
          await this.indexeddb.importDatabase(element);
        } catch (err) {
          debug('onImport() error : ' + (err as Error).message);
        }
      }
    } else {
      debug('File(s) not recognized or no file loaded.');
    }

    await this.wasm.reloadIDBTask.perform();
    await this.wasm.getHistoryTask.perform();
    await this.wasm.getWalletsTask.perform();
    await this.wasm.getTransactionsTask.perform();
    await this.wasm.getToolsTask.perform();
    await this.wasm.getReportsTask.perform();
    this.close();
  }

  @action
  close() {
    if (this.application.hasIndexedDb) {
      this.files = [];
      this.args.onHidden();
    }
  }

  @action
  onShow() {
    this._proxy = undefined;
  }

  @action
  async onSave() {
    await this.saveConfigTask.perform();
  }

  @action
  async onSetDebug() {
    if (this.activated === 'ACTIVER') {
      // default case or in "TO ACTIVATE" mode
      debug('__Activation DEBUG WASM___');
      this.wasm.setDebugTask.perform(true);
      this.activated = 'DESACTIVER';
    } else {
      debug('__Désactivation DEBUG WASM___');
      this.wasm.setDebugTask.perform(false);
      this.activated = 'ACTIVER';
    }
  }

  private saveConfigTask = dropTask(async () => {
    try {
      await this.wasm.saveConfigTask.perform(this._proxy ?? '');
      this.saveConfigTaskSuccess = true;
    } catch (error) {
      debug((error as Error).message);
      this.saveConfigTaskError = true;
    }

    this.removeSaveConfigTaskIndicator.perform();
  });

  private removeSaveConfigTaskIndicator = restartableTask(async () => {
    if (!Ember.testing) {
      await timeout(3000);
    }

    this.saveConfigTaskError = false;
    this.saveConfigTaskSuccess = false;
  });

  @action
  dropZoneEnabled() {
    this.dropZoneDisabled = !this.dropZoneDisabled;
  }

  @action
  async upload(e: { target: HTMLInputElement }) {
    const zipper = new JSZip();
    let files = e.target.files as FileList;

    if (files[0]) {
      const zip = await zipper.loadAsync(files[0]);

      this.indexeddb.dbs.forEach(async (dbName) => {
        const file = await zip?.file(dbName + '.json')?.async('blob');

        if (file) {
          this.files.push(file);
        }
      });
    }
  }
}
