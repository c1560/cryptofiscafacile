import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { debug } from '@ember/debug';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import type ApplicationService from 'cryptofiscafacile-gui/services/application';
import type Indexeddb from 'cryptofiscafacile-gui/services/indexeddb';
import type Wasm from 'cryptofiscafacile-gui/services/wasm';

export default class Header extends Component {
  @service declare application: ApplicationService;
  @service declare wasm: Wasm;
  @service declare indexeddb: Indexeddb;

  @tracked page = '';

  constructor(owner: undefined, args: never) {
    super(owner, args);
    this.page = window.location.href;
    this.wasm.getUserInfosTask.perform();

    if (!this.application.hasIndexedDb) {
      this.confModal = true;
    }
  }

  get onPage() {
    return this.page.includes('transactions') ?? false;
  }

  get infos() {
    return this.wasm.userInfos;
  }

  @tracked confModal = false;
  @tracked calendarModal = false;
  @tracked donationModal = false;
  @action
  setConfModal() {
    this.confModal = true;
  }
  @action
  setCalendarModal() {
    this.calendarModal = true;
  }
  @action
  setDonationModal() {
    this.donationModal = true;
  }

  @action
  hiddenCal() {
    //Placeholder - Do nothing
  }
  @action
  hiddenDon() {
    //Placeholder - Do nothing
  }

  @action
  close() {
    if (this.application.hasIndexedDb) {
      this.confModal = false;
      this.calendarModal = false;
      this.donationModal = false;
    }
  }
  @action
  onSuccess() {
    debug('Copy to clipboard success');
  }
  @action
  onError() {
    debug('Copy to clipboard error... Please try again');
  }
}
