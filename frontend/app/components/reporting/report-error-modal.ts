import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';
import type IntlService from 'ember-intl/services/intl';

interface ReportingReportErrorModalArgs {
  onHidden: () => void;
  type: 2086 | 3916;
}

export default class ReportingReportErrorModal extends Component<ReportingReportErrorModalArgs> {
  @service declare intl: IntlService;
  @service declare wasm: WasmService;

  get errorMessage2086() {
    return this.wasm.translateError(this.wasm.errorMessage2086);
  }
}
