import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

import type { Cerfa3916Account } from 'cryptofiscafacile-wasm';
import type { Cerfa3916Wallet } from 'cryptofiscafacile-wasm';

interface UserDataArgs {
  wallet: Cerfa3916Wallet;
}

export default class UserData extends Component<UserDataArgs> {
  @tracked index = 1;
  @tracked buffer: Cerfa3916Account[] = this.args.wallet.accounts;

  @action
  addAccount() {
    let arr: Cerfa3916Account[] = this.args.wallet.accounts;
    const obj: Cerfa3916Account = {
      id: '-',
      opened: new Date('January 01, 2007 03:24:00'),
      closed: new Date(),
      isClosed: false,
    };

    arr.push(obj);

    this.args.wallet.accounts = arr;
    this.buffer = arr;
  }
}
