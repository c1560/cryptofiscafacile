import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { debug } from '@ember/debug';
import { action } from '@ember/object';

import type { Cerfa3916Account } from 'cryptofiscafacile-wasm';

interface ReportingUserDataAccountArgs {
  account: Cerfa3916Account;
  firsttx: Date;
  lasttx: Date;
}

export default class ReportingUserDataAccount extends Component<ReportingUserDataAccountArgs> {
  @tracked errorOpenDate = false;
  @tracked errorCloseDate = false;

  @tracked openDate: Date = this.args.account.opened;
  @tracked closeDate: Date = this.args.account.closed;

  @tracked closed = this.args.account.isClosed;

  @action
  toggleCheck(e: { target: HTMLInputElement }) {
    this.closed = e.target.checked;

    this.args.account.isClosed = e.target.checked;
  }

  @action
  onChangeOpen(date: Date[]) {
    if (date[0]) {
      this.openDate = date[0];
    }

    if (this.openDate > this.args.firsttx) {
      debug('Chosen date is upper than first TX!');
      this.errorOpenDate = true;
    } else {
      this.errorOpenDate = false;
      this.args.account.opened = this.openDate;
    }
  }
  @action
  onChangeClose(date: Date[]) {
    if (date[0]) {
      this.closeDate = date[0];
    }

    if (this.closed === true) {
      if (this.closeDate < this.args.lasttx) {
        debug('Chosen date is lower than last TX!');
        this.errorCloseDate = true;
      } else {
        this.errorCloseDate = false;
        this.args.account.closed = this.closeDate;
      }
    } else {
      debug('Checkbox account closed not checked!');
    }
  }
  @action
  setAccount(e: { target: HTMLInputElement }) {
    this.args.account.id = e.target.value;
  }
}
