import Ember from 'ember';
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { debug } from '@ember/debug';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import { dropTask, restartableTask, timeout } from 'ember-concurrency';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

export default class Person extends Component {
  @service declare wasm: WasmService;

  @tracked newName?: string;
  @tracked newSurname?: string;
  @tracked newEmail?: string;

  @tracked success = false;
  @tracked error = false;

  get name() {
    return this.newName || (this.wasm.userInfos?.name ?? '');
  }
  get surname() {
    return this.newSurname || (this.wasm.userInfos?.surname ?? '');
  }
  get email() {
    return this.newEmail || (this.wasm.userInfos?.email ?? '');
  }

  get nameUpdated() {
    return this.newName && this.wasm.userInfos?.name !== this.newName;
  }

  get surnameUpdated() {
    return this.newSurname && this.wasm.userInfos?.surname !== this.newSurname;
  }

  get emailUpdated() {
    return this.newEmail && this.wasm.userInfos?.email !== this.newEmail;
  }

  get updated() {
    return this.nameUpdated || this.surnameUpdated || this.emailUpdated;
  }

  @action
  setName(e: { target: HTMLInputElement }) {
    this.newName = e.target.value;
  }

  @action
  setSurname(e: { target: HTMLInputElement }) {
    this.newSurname = e.target.value;
  }

  @action
  setEmail(e: { target: HTMLInputElement }) {
    this.newEmail = e.target.value;
  }

  @action
  saveUserData() {
    if (this.nameUpdated || this.surnameUpdated || this.emailUpdated) {
      this.saveUserDataTask.perform();
    } else {
      debug('No update of user data. No task loaded!');
    }
  }

  private saveUserDataTask = dropTask(async () => {
    try {
      await this.wasm.saveUserTask.perform(this.name, this.surname, this.email);
      this.success = true;
      await this.wasm.getUserInfosTask.perform();
    } catch (error) {
      debug((error as Error).message);
      this.error = true;
    }

    this.removeSaveUserStatusIndicatorTask.perform();
  });

  private removeSaveUserStatusIndicatorTask = restartableTask(async () => {
    if (!Ember.testing) {
      await timeout(5000);
    }

    this.success = false;
    this.error = false;
  });
}
