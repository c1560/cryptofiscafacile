import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import Bnc from 'cryptofiscafacile-gui/objects/bnc';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';

interface CerfaBncChoiceArgs {
  year: number;
  choice: boolean;
  made: boolean;
  locked: boolean;
  bnc: Bnc[];
}

export default class CerfaBncChoice extends Component<CerfaBncChoiceArgs> {
  @service declare wasm: WasmService;

  get isMade() {
    return !this.args.made;
  }

  @action
  radioCheck(e: { target: HTMLSelectElement }) {
    const id = e.target.value;
    const res = id.indexOf('zero');

    if (res > -1) {
      const year = parseInt(id.substring(0, res));

      this.args.bnc.push(new Bnc(year, false));
    } else {
      const res2 = id.indexOf('bnc');
      const year = parseInt(id.substring(0, res2));

      this.args.bnc.push(new Bnc(year, true));
    }
  }
}
