import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { debug } from '@ember/debug';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import { dropTask, restartableTask, timeout } from 'ember-concurrency';

import type Bnc from 'cryptofiscafacile-gui/objects/bnc';
import type WasmService from 'cryptofiscafacile-gui/services/wasm';
import type { Cerfa2086BNC } from 'cryptofiscafacile-wasm';

interface CerfaBncArgs {
  cerfas: Cerfa2086BNC[];
}

export default class CerfaBnc extends Component<CerfaBncArgs> {
  @service declare wasm: WasmService;

  @tracked selectedBnc: Bnc[] = [];

  get sortedCerfas() {
    let sortedCerfas: Cerfa2086BNC[] = this.args.cerfas;

    sortedCerfas = this.#sortCerfas(sortedCerfas);

    return sortedCerfas;
  }
  #sortCerfas(cerfas: Cerfa2086BNC[]): Cerfa2086BNC[] {
    return cerfas.sort((y1: Cerfa2086BNC, y2: Cerfa2086BNC) => y1.year - y2.year);
  }

  @tracked success = false;
  @tracked error = false;
  @action
  saveBnc() {
    this.saveChoiceBNCTask.perform();
  }

  private saveChoiceBNCTask = dropTask(async () => {
    this.selectedBnc.forEach((element) =>
      debug(`Call to saveChoiceCashIn: For ${element.year} / Choice : ${element.cashin}`),
    );

    const saveChoiceBNCPromises = Promise.all(
      this.selectedBnc.map((element) => this.wasm.saveChoiceBNCTask.perform(element.year, element.cashin)),
    );

    try {
      await saveChoiceBNCPromises;
      this.success = true;
    } catch (error) {
      debug((error as Error).message);
      this.error = true;
    }

    this.saveChoiceBNCStatusIndicatorTask.perform();
  });

  private saveChoiceBNCStatusIndicatorTask = restartableTask(async () => {
    await timeout(5000);
    this.success = false;
    this.error = false;
  });
}
