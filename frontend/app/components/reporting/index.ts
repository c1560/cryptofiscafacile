import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { debug } from '@ember/debug';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import { SortOrder } from 'cryptofiscafacile-gui/enums/sort-order';
import Sorts from 'cryptofiscafacile-gui/utils/sorts';
import { dropTask, restartableTask, timeout } from 'ember-concurrency';

import type WasmService from 'cryptofiscafacile-gui/services/wasm';
import type { Cerfa3916Wallet } from 'cryptofiscafacile-wasm';

export default class Reporting extends Component {
  private static readonly ACC_ITEM_REPORTS_NAME = 'reports';

  @service declare wasm: WasmService;

  @tracked filename = '';
  @tracked fileURL = '';

  @tracked isDownloadable = false;

  @tracked reportsSortOrder = SortOrder.ASC;

  @tracked reportErrorModalVisible = false;
  @tracked reportErrorModalType?: number;

  @tracked addWalletModalVisible = false;

  get accItemReportsName() {
    return Reporting.ACC_ITEM_REPORTS_NAME;
  }

  /**
   * List of getter methods for CERFA 3916
   *
   * @readonly
   * @memberof Reporting
   */
  get unprocessedReports() {
    return this.wasm.reports?.cerfa3916.wallets ?? [];
  }
  get notEmptyReports() {
    return this.wasm.reports?.hasTX || this.reports;
  }

  get reports() {
    this.unprocessedReports.forEach((element) => {
      if (element.accounts.length === 0) {
        element.accounts.push({ id: '-', opened: element.firsttx, closed: element.lasttx, isClosed: false });
      }
    });

    return this.unprocessedReports;
  }
  get sortedReports() {
    // This is for value to use in HBS = sorted wallets
    return this.reports.slice().sort(Sorts.text((cerfa: Cerfa3916Wallet) => cerfa.name, this.reportsSortOrder));
  }

  get cerfas2086CashinChoice() {
    return this.wasm.reports?.cerfa2086.cashin ?? [];
  }

  @tracked success = false;
  @tracked error = false;
  @action
  saveAccData() {
    this.saveAccountsTask.perform();
  }

  @action
  reloadReports() {
    this.wasm.getReportsTask.perform();
  }

  private saveAccountsTask = dropTask(async () => {
    this.sortedReports.forEach((element) => debug(`Call to saveAccounts for ${element.name}`));

    const saveAccountsPromises = Promise.all(
      this.sortedReports.map((element) => this.wasm.saveAccountsTask.perform(element.name, element.accounts)),
    );

    try {
      await saveAccountsPromises;
      this.success = true;
    } catch (error) {
      debug((error as Error).message);
      this.error = true;
    }

    this.removeSaveAccountsStatusIndicatorTask.perform();
  });

  private removeSaveAccountsStatusIndicatorTask = restartableTask(async () => {
    await timeout(3000);
    this.success = false;
    this.error = false;
  });

  @action
  onAccordionItemChange(newValue: string) {
    if (newValue === Reporting.ACC_ITEM_REPORTS_NAME) {
      this.generateReports();
    }
  }

  generateReports() {
    debug('reports generating');
    this.wasm.get2086XlsxTask.perform();
    this.wasm.get2086XlsxUpToTodayTask.perform();
    this.wasm.get3916XlsxTask.perform();
    this.wasm.getStocksXlsxTask.perform();
  }

  get is2086Downloadable() {
    return !this.wasm.errorMessage2086 && this.wasm.cerfa2086;
  }

  get is2086UpToTodayDownloadable() {
    return !this.wasm.errorMessage2086UpToToday && this.wasm.cerfa2086UpToToday;
  }

  get is3916Downloadable() {
    return !this.wasm.errorMessage3916 && this.wasm.cerfa3916;
  }

  get isStocksDownloadable() {
    return this.wasm.stocksXLSX;
  }

  @action
  buildCerfaLink(id: string, content: Uint8Array | undefined) {
    debug(`Call to CERFA ${id}...`);

    if (content) {
      debug('XLSX generated!');

      // Convert Uint8Array to ArrayBuffer
      let buf = this.typedArrayToBuffer(content);
      //Create blob from the ArrayBuffer
      let blob = new Blob([buf], {
        type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;',
      });

      let today = new Date();
      let dd = String(today.getDate()).padStart(2, '0');
      let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      let yyyy = String(today.getFullYear());

      this.filename = `CERFA_${id}_${yyyy}${mm}${dd}.xlsx`;
      // Then create the URI, for file download
      this.fileURL = window.URL.createObjectURL(blob);
    } else {
      debug(`ERROR - CERFA ${id} not generated!`);
    }
  }

  @action
  ficStock() {
    debug('Call to ficStock...');

    if (this.wasm.stocksXLSX) {
      debug('XLSX generated!');

      // Convert Uint8Array to ArrayBuffer
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      let buf = this.typedArrayToBuffer(this.wasm.stocksXLSX);
      //Create blob from the ArrayBuffer
      let blob = new Blob([buf], {
        type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;',
      });

      let today = new Date();
      let dd = String(today.getDate()).padStart(2, '0');
      let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      let yyyy = String(today.getFullYear());

      this.filename = 'FicheStocks_' + yyyy + mm + dd + '.xlsx';
      // Then create the URI, for file download
      this.fileURL = window.URL.createObjectURL(blob);
    } else {
      debug('ERROR - XLSX not generated!');
    }
  }

  typedArrayToBuffer(array: Uint8Array): ArrayBuffer {
    return array.buffer.slice(array.byteOffset, array.byteLength + array.byteOffset);
  }

  @action
  displayReportErrorModal(type: number) {
    this.reportErrorModalType = type;
    this.reportErrorModalVisible = true;
  }

  @action
  closeReportErrorModal() {
    this.reportErrorModalType = undefined;
    this.reportErrorModalVisible = false;
  }
}
