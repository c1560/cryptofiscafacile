export default class Option {
  public static readonly ALL = 'all';

  constructor(
    readonly name: string,
    readonly selected: boolean,
  ) {}
}
