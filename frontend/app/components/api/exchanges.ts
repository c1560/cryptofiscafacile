import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { debug } from '@ember/debug';
import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';
import { inject as service } from '@ember/service';

import type Wasm from 'cryptofiscafacile-gui/services/wasm';
import type { ApiData } from 'cryptofiscafacile-wasm';

interface ApiExchangesArgs {
  wallet: ApiData;
}

export default class ApiExchanges extends Component<ApiExchangesArgs> {
  @service declare wasm: Wasm;

  @tracked pub = this.args.wallet.arg1;
  @tracked priv = this.args.wallet.arg2;
  @tracked other = this.args.wallet.arg3;
  @tracked toStore = false;

  @tracked idPriv = 'priv-' + guidFor(this);
  @tracked idPub = 'pub-' + guidFor(this);
  @tracked idstoreYes = 'store-' + guidFor(this);
  @tracked idstoreNo = 'nostore-' + guidFor(this);

  get progressWallet() {
    return this.wasm.toolsProgress.get(this.args.wallet) ?? 0;
  }

  get progressError() {
    return this.wasm.toolsErrors.get(this.args.wallet) ?? '';
  }

  get isRunning() {
    const tasks = this.wasm.toolsTasks.get(this.args.wallet);

    return tasks ? tasks[0]?.isRunning : false;
  }

  @action
  submitExch() {
    this.wasm.getApi(this.args.wallet, this.pub, this.priv, this.other, this.toStore);

    debug('Call to GetAPI: ' + this.args.wallet.name + ' / ' + this.pub + ' / ' + this.toStore);
  }

  @action
  setPub(e: { target: HTMLInputElement }) {
    if (e.target.value !== '') {
      this.pub = e.target.value;
    } else {
      this.pub = this.args.wallet.arg1;
    }
  }
  @action
  setPriv(e: { target: HTMLInputElement }) {
    if (e.target.value !== '') {
      this.priv = e.target.value;
    } else {
      this.priv = this.args.wallet.arg2;
    }
  }

  @action
  setCheckStore(e: { target: HTMLInputElement }) {
    this.toStore = e.target.checked;
  }
}
