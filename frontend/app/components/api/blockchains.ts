import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { debug } from '@ember/debug';
import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';
import { inject as service } from '@ember/service';

import type Wasm from 'cryptofiscafacile-gui/services/wasm';
import type { ApiData } from 'cryptofiscafacile-wasm';
interface ApiBlockchainsArgs {
  blockchain: ApiData;
}

export default class ApiBlockchains extends Component<ApiBlockchainsArgs> {
  @service declare wasm: Wasm;

  @tracked address = this.args.blockchain.arg1;
  @tracked pub = this.args.blockchain.arg2;
  @tracked other = this.args.blockchain.arg3;
  @tracked toStore = false;

  @tracked idPriv = 'priv-' + guidFor(this);
  @tracked idPub = 'pub-' + guidFor(this);
  @tracked idstoreYes = 'store-' + guidFor(this);
  @tracked idstoreBlockNo = 'nostore-' + guidFor(this);

  get progressBlock() {
    return this.wasm.toolsProgress.get(this.args.blockchain) ?? 0;
  }

  get progressError() {
    return this.wasm.translateError(this.wasm.toolsErrors.get(this.args.blockchain) ?? '');
  }

  @action
  submitBc() {
    this.wasm.getApi(this.args.blockchain, this.address, this.pub, this.other, this.toStore);

    debug(
      'Call to GetAPI for blockchains: ' +
        this.args.blockchain.name +
        ' / ' +
        this.address +
        ' / ' +
        this.pub +
        ' / ' +
        this.toStore,
    );
  }

  get isRunning() {
    const tasks = this.wasm.toolsTasks.get(this.args.blockchain);

    return tasks ? tasks[0]?.isRunning : false;
  }

  @action
  setAddress(e: { target: HTMLInputElement }) {
    if (e.target.value !== '') {
      this.address = e.target.value;
    } else {
      this.address = this.args.blockchain.arg1;
    }
  }
  @action
  setPub(e: { target: HTMLInputElement }) {
    if (e.target.value !== '') {
      this.pub = e.target.value;
    } else {
      this.pub = this.args.blockchain.arg2;
    }
  }

  @action
  setCheckStore(e: { target: HTMLInputElement }) {
    this.toStore = e.target.checked;
  }
}
