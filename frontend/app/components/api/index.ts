import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

import type Application from 'cryptofiscafacile-gui/services/application';
import type Wasm from 'cryptofiscafacile-gui/services/wasm';
import type { ApiData } from 'cryptofiscafacile-wasm';

export default class Api extends Component {
  @service declare application: Application;
  @service declare wasm: Wasm;

  get unprocessedTools() {
    return this.wasm.tools?.apis ?? [];
  }

  get wallets() {
    return this.#filterTools(this.unprocessedTools, 'wallet');
  }

  get blockchains() {
    return this.#filterTools(this.unprocessedTools, 'block');
  }

  #filterTools(tools: ApiData[], target: string) {
    if (target === 'wallet') {
      return tools.filter((item) => item.plateform);
    } else if (target === 'block') {
      return tools.filter((item) => !item.plateform);
    }

    return undefined;
  }

  get isFecthingTools() {
    return this.wasm.getToolsTask.isRunning;
  }
}
