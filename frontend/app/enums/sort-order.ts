export enum SortOrder {
  ASC = 'ASC',
  DESC = 'DESC',
  NONE = 'NONE',
}
