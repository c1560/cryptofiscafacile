export enum EngineStatus {
  LOADING = 'LOADING',
  RUNNING = 'RUNNING',
  HALTED = 'HALTED',
}
