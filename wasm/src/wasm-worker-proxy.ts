import { methods } from './cff2-wasm.js';

export function buildWorkerProxy(wasmFile: string, onExit: () => void) {
  // Create an object to later interact with
  const proxy: Record<string, () => void> = {};

  // Keep track of the messages being sent
  // so we can resolve them correctly
  let id = 0;
  let idPromises: Record<number, { resolve: (value: unknown) => void; reject: (value: unknown) => void }> = {};

  return new Promise((resolve, reject) => {
    const worker = new Worker('/worker.js');

    worker.postMessage({ eventType: 'INITIALISE', eventData: wasmFile });
    worker.addEventListener('message', function (event) {
      const { eventType, eventData, eventId } = event.data;

      if (eventType === 'INITIALISED') {
        methods.forEach((method: string) => {
          proxy[method] = function (...args) {
            return new Promise((resolve, reject) => {
              worker.postMessage({
                eventType: 'CALL',
                eventData: {
                  method: method,
                  arguments: args,
                },
                eventId: id,
              });

              idPromises[id] = { resolve, reject };
              id++;
            });
          };
        });
        resolve(proxy);

        return;
      } else if (eventType === 'RESULT') {
        if (eventId !== undefined && idPromises[eventId]) {
          idPromises[eventId]?.resolve(eventData);
          delete idPromises[eventId];
        }
      } else if (eventType === 'ERROR') {
        if (eventId !== undefined && idPromises[eventId]) {
          idPromises[eventId]?.reject(event.data.eventData);
          delete idPromises[eventId];
        }
      } else if (eventType === 'HALTED') {
        onExit();
      }
    });

    worker.addEventListener('error', function (error) {
      reject(error);
    });
  });
}
