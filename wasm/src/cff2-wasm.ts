export enum DbName {
  CFF = 'cff',
  CONFS = 'wasm-db-confs',
  FILES = 'wasm-db-files',
  RATES = 'wasm-db-rates',
  TRANSACTIONS = 'wasm-db-txs',
}

export interface ApplicationEntry {
  key: 'application.version';
  value: string;
}

/**
 * Files Data Model
 */
export interface FileUsageStatus {
  bcAddresses?: string[];
  name: string;
  error: string;
  ext: string;
  kind: string;
  result: string;
  unknowns: string[];
}

export interface Definition {
  name: string;
  version: number;
}

/**
 * Wallet Data Model
 */
export interface Wallet {
  name: string;
  icon: string;
}

/**
 * Tools Data Model
 */
export interface ApiData {
  name: string;
  url: string; // ex: URL pour créer son API Key/Secret
  plateform: boolean; // true si Exchange, false si blockchain
  key: boolean; // true si besoin de API Key
  arg1: string;
  arg2: string;
  arg3: string;
}
export interface ToolsData {
  apis: ApiData[];
}

/**
 * TXs Data Model
 */
export interface TxValue {
  quantity: number;
  currency: string;
}

export enum TxCategory {
  AIR_DROPS = 'AirDrop',
  CASH_OUT = 'CashOut',
  CASH_IN = 'CashIn',
  COMMERCIAL_REBATES = 'Remise Commerciale',
  DEPOSITS = 'Dépôt',
  DONT_CARE = 'Ignoré',
  EXCHANGES = 'Echange',
  FEES = 'Frais',
  FORKS = 'Fork',
  GIFTS = 'Don',
  ICO = 'ICO',
  INTERESTS = 'Intérêt',
  MINING = 'Minage',
  NONE = '',
  REFERRALS = 'Parrainage',
  TRANSFERS = 'Transfert',
  WITHDRAWALS = 'Retrait',
}

export interface Transaction {
  id: string;
  date: Date;
  wallet: string;
  original_category: TxCategory;
  category: TxCategory;
  to: TxValue[];
  from: TxValue[];
  fee: TxValue[];
  note: string;
  merged: boolean;
}

export interface ConsistencyTx {
  id: string;
  date: Date;
  wallet: string;
  category: string;
  amounts: string;
  coin: string;
}
export interface ConsistencyError {
  error: string;
  tx: ConsistencyTx;
  related: ConsistencyTx[];
}
export interface ConsistencyErrors {
  errors: ConsistencyError[];
}

/**
 * Synthesis Data Model
 */
export interface SynthTxTypes {
  txCntCashIn: number;
  txCntCashOut: number;
  txCntExchanges: number;
  txCntMinings: number;
  txCntInterests: number;
  txCntCommercialRebates: number;
  txCntAirDrops: number;
}
export interface SynthBalance {
  coin: string;
  amount: number;
}
export interface SynthCoin {
  coin: string;
  amount: number;
  value: number;
}
export interface SynthEvolution {
  [key: string]: number;
}

export interface SynthStatTotalValue {
  total_value: number;
}

export interface SynthStatsGainsLosses {
  unrealized: number;
  realized: number;
  bnc: number;
}

/**
 * Reports Data Model
 */
export interface UserData {
  name: string;
  surname: string;
  email: string;
  proxy: string;
}
export interface Cerfa3916Account {
  id: string;
  opened: Date;
  closed: Date;
  isClosed: boolean;
}
export interface Cerfa3916Wallet {
  name: string;
  logo: string; // tu peux l'afficher non ?
  crypto: boolean; // Dans le 3916 faut différencier les compte d'actifs numériques (ex 3916-bis) des comptes Fiat a l'étrangers
  firsttx: Date; // date de premiere TX vue
  lasttx: Date; // date de derniere TX vue
  legal: string; // legal name
  address: string; // à afficher sans possibilité de modification
  url: string; // à afficher sans possibilité de modification
  accounts: Cerfa3916Account[];
}
export interface Cerfa3916Data {
  wallets: Cerfa3916Wallet[];
}
export interface Cerfa2086BNC {
  year: number;
  choice: boolean;
  isMade: boolean;
  isLocked: boolean;
}
export interface Cerfa2086Data {
  cashin: Cerfa2086BNC[];
}
export interface ReportsData {
  user: UserData;
  cerfa3916: Cerfa3916Data;
  cerfa2086: Cerfa2086Data;
  hasTX: boolean;
}

export interface Cerfa2086ResultData {
  initialPta: number;
  initialPtaTxs: C2086InitTxResult[];
  years: Cerfa2086YearResultData[];
}

export interface C2086InitTxResult {
  date: Date;
  asset: string;
  quantityFound: number;
  quantityPending: number;
  quantityOut: number;
  quantityIn: number;
  fiatValue: number;
  ptaValue: number;
  taxable: boolean;
  note: string;
}

export interface Cerfa2086YearResultData {
  incomesAsCashIn: boolean;
  airDrops: number;
  commRebates: number;
  deposits: number;
  gifts: number;
  interests: number;
  minings: number;
  referrals: number;
  withdrawals: number;
  cashOuts: Cerfa2086YearCashoutResultData[];
  totalPurchasePrice: number;
  capitalFraction: number;
  realizedPnl: number;
  bncAmount: number;
  bncCategories: TxCategory[];
  bncReal: boolean;
  foreignInterests: number;
}

export interface Cerfa2086YearCashoutResultData {
  source: string;
  note: string;
  date211: Date;
  valeurPortefeuille212: number;
  prix213: number;
  frais214: number;
  prixNetDeFrais215: number;
  soulteRecueOuVersee216: number;
  prixNetDeSoulte217: number;
  prixNet218: number;
  prixTotalAcquisition220: number;
  fractionDeCapital221: number;
  soulteRecueEnCasDechangeAnterieur222: number;
  prixTotalAcquisitionNet223: number;
  plusMoinsValue: number;
}

export interface CoinDetailData {
  id: string;
  image: { thumb: string };
  name: string;
  symbol: string;
  marketData: CoinDetailMarketData;
}

export interface CoinDetailMarketData {
  currentPriceBtc: number;
  currentPriceEur: number;
  currentPriceUsd: number;
  currentPriceChange24hEur: number;
  currentSupply: number;
  marketCapEur: number;
  maxSupply?: number;
  totalSupply?: number;
}

export interface WalletDefinition {
  name: string;
  crypto: boolean;
  custodial: boolean;
  logo: string;
  legal: string;
  address: string;
  url: string;
}

export interface Cff2Wasm {
  addEmptyWallet(wallet: string): Promise<void>;
  checkConsistencyAlmostTransfer(): Promise<ConsistencyErrors>;
  checkConsistencyCategVsToFrom(): Promise<ConsistencyErrors>;
  checkConsistencyNegativeBalance(): Promise<ConsistencyErrors>;
  cleanDbs(names: DbName[]): Promise<void>;
  deleteFile(fileName: string): Promise<FileUsageStatus[]>;
  downloadFile(fileName: string): Promise<Uint8Array>;
  get2086Results(): Promise<Cerfa2086ResultData>;
  get2086Xlsx(): Promise<Uint8Array>;
  get2086XlsxUpToToday(): Promise<Uint8Array>;
  get3916XLSX(): Promise<Uint8Array>;
  getApi(provider: string, arg1: string, arg2: string, arg3: string, remember: boolean): Promise<void>;
  getApiProgress(provider: string): Promise<number>;
  getCoinDetail(symbol: string): Promise<CoinDetailData>;
  getDefinitions(): Promise<Definition[]>;
  getHistory(confs: boolean): Promise<FileUsageStatus[]>;
  getReports(): Promise<ReportsData>;
  getStocksXLSX(): Promise<Uint8Array>;
  getSynthBalances(date: Date, wallet?: string): Promise<SynthBalance[]>;
  getSynthEvolution(start: Date, end: Date, wallet?: string): Promise<SynthEvolution[]>;
  getSynthPortfolio(date: Date, wallet?: string): Promise<SynthCoin[]>;
  getSynthStatTotalValue(date: Date, wallet?: string): Promise<SynthStatTotalValue>;
  getSynthStatsGainsLosses(date: Date): Promise<SynthStatsGainsLosses>;
  getSynthTxTypes(start: Date, end: Date, wallet?: string): Promise<SynthTxTypes>;
  getTools(): Promise<ToolsData>;
  getTransactions(): Promise<Transaction[]>;
  getUserInfos(): Promise<UserData>;
  getWalletDefinitions(): Promise<WalletDefinition[]>;
  getWallets(): Promise<Wallet[]>;
  mergeTransactions(srcID: string, dstID: string): Promise<void>;
  reloadIDB(): Promise<void>;
  saveAccounts(wallet: string, accounts: Cerfa3916Account[]): Promise<void>;
  saveChoiceCashIn(year: number, cashin: boolean): Promise<void>;
  saveConfig(proxy: string | undefined): Promise<void>;
  saveUser(name: string | undefined, surname: string | undefined, email: string | undefined): Promise<void>;
  setCategory(txID: string, newCateg: TxCategory): Promise<void>;
  setDebug(enable: boolean): Promise<boolean>;
  setDefinition(fileName: string, fileVersion: number): Promise<void>;
  setFetchRates(fetchRates: boolean): Promise<void>;
  setupFrontendTest(fetchRates: boolean): Promise<void>;
  setValue(txID: string, values: number[]): Promise<void>;
  revertCateg(txID: string): Promise<void>;
  unMergeTransactions(srcID: string): Promise<void>;
  useFile(fileName: string, fileData: Uint8Array): Promise<FileUsageStatus[]>;
  createApplicationEntry(entry: ApplicationEntry): Promise<void>;
  getApplicationEntry(key: string): Promise<ApplicationEntry | undefined>;
  updateApplicationEntry(entry: ApplicationEntry): Promise<void>;
}

const methods: Array<keyof Cff2Wasm> = [
  'addEmptyWallet',
  'checkConsistencyAlmostTransfer',
  'checkConsistencyCategVsToFrom',
  'checkConsistencyNegativeBalance',
  'cleanDbs',
  'deleteFile',
  'downloadFile',
  'get2086Results',
  'get2086Xlsx',
  'get2086XlsxUpToToday',
  'get3916XLSX',
  'getApi',
  'getApiProgress',
  'getCoinDetail',
  'getDefinitions',
  'getHistory',
  'getReports',
  'getStocksXLSX',
  'getSynthBalances',
  'getSynthEvolution',
  'getSynthPortfolio',
  'getSynthStatTotalValue',
  'getSynthStatsGainsLosses',
  'getSynthTxTypes',
  'getTools',
  'getTransactions',
  'getUserInfos',
  'getWalletDefinitions',
  'getWallets',
  'mergeTransactions',
  'reloadIDB',
  'revertCateg',
  'saveAccounts',
  'saveChoiceCashIn',
  'saveConfig',
  'saveUser',
  'setCategory',
  'setDebug',
  'setDefinition',
  'setFetchRates',
  'setupFrontendTest',
  'setValue',
  'unMergeTransactions',
  'useFile',
  'createApplicationEntry',
  'getApplicationEntry',
  'updateApplicationEntry',
];

export { methods };
