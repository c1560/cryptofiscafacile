// eslint-disable-next-line import/no-unassigned-import
import './wasm_exec.js';

import { methods } from './cff2-wasm.js';

declare global {
  interface Window {
    onWasmInitialized: (value: unknown) => void;
  }
}

let proxies = new Map<string, Record<string, () => void>>();

export async function getProxy(wasmFile: string, onExit: () => void) {
  if (!proxies.has(wasmFile)) {
    proxies.set(wasmFile, await buildProxy(wasmFile, onExit));
  }

  return proxies.get(wasmFile);
}

async function buildProxy(wasmFile: string, onExit: () => void) {
  const go = new Go();
  const proxy: Record<string, () => void> = {};

  const instantiatedModule = await WebAssembly.instantiateStreaming(fetch(wasmFile), go.importObject);

  const isReady = new Promise((resolve) => {
    self.onWasmInitialized = resolve;
  });

  go.run(instantiatedModule.instance)
    .catch((r) => {
      console.warn(r);
    })
    .finally(onExit);

  methods.forEach((method: string) => {
    proxy[method] = function (...args) {
      return new Promise((resolve, reject) => {
        try {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          const result = self[method].apply(null, args);

          resolve(result);
        } catch (e) {
          reject(e);
        }
      });
    };
  });
  await isReady;

  return proxy;
}
