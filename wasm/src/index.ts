import { buildWorkerProxy } from './wasm-worker-proxy.js';

import type { Cff2Wasm } from './cff2-wasm.js';

export * from './cff2-wasm.js';

export async function loadCff2WasmProxy(wasmFilePath: string, onExit: () => void, worker: boolean): Promise<Cff2Wasm> {
  if (worker) {
    return (await buildWorkerProxy(wasmFilePath, onExit)) as Cff2Wasm;
  } else {
    const module = await import('./wasm-proxy.js');

    return (await module.getProxy(wasmFilePath, onExit)) as unknown as Cff2Wasm;
  }
}
