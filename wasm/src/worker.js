importScripts('./wasm_exec.js');

// Handle incoming messages
self.addEventListener(
  'message',
  async function (event) {
    const { eventType, eventData, eventId } = event.data;

    if (eventType === 'INITIALISE') {
      const go = new self.Go();
      const instantiatedModule = await WebAssembly.instantiateStreaming(fetch(eventData), go.importObject);

      const isReady = new Promise((resolve) => {
        self.onWasmInitialized = resolve;
      });

      go.run(instantiatedModule.instance).finally(() =>
        this.self.postMessage({
          eventType: 'HALTED',
        }),
      );
      await isReady;

      // Send back initialised message to main thread
      self.postMessage({
        eventType: 'INITIALISED',
      });
    } else if (eventType === 'CALL') {
      try {
        const result = await self[eventData.method].apply(null, eventData.arguments);

        self.postMessage({
          eventType: 'RESULT',
          eventData: result,
          eventId: eventId,
        });
      } catch (error) {
        self.postMessage({
          eventType: 'ERROR',
          eventData: error,
          eventId: eventId,
        });
      }
    }
  },
  false,
);
