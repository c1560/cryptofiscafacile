module gitlab.com/c1560/CryptoFiscaFacile/wasm

go 1.19

replace (
	gitlab.com/c1560/CryptoFiscaFacile/wasm/api => ./api
	gitlab.com/c1560/CryptoFiscaFacile/wasm/csv => ./csv
	gitlab.com/c1560/CryptoFiscaFacile/wasm/json => ./json
	gitlab.com/c1560/CryptoFiscaFacile/wasm/rate => ./rate
	gitlab.com/c1560/CryptoFiscaFacile/wasm/tx => ./tx
	gitlab.com/c1560/CryptoFiscaFacile/wasm/wallet => ./wallet
)

require (
	github.com/360EntSecGroup-Skylar/excelize v1.4.1
	github.com/davecgh/go-spew v1.1.1
	github.com/hack-pad/go-indexeddb v0.3.2
	github.com/shopspring/decimal v1.4.0
	github.com/stretchr/testify v1.9.0
	gitlab.com/c1560/CryptoFiscaFacile/wasm/api v0.0.0-00010101000000-000000000000
	gitlab.com/c1560/CryptoFiscaFacile/wasm/csv v0.0.0-00010101000000-000000000000
	gitlab.com/c1560/CryptoFiscaFacile/wasm/json v0.0.0-00010101000000-000000000000
	gitlab.com/c1560/CryptoFiscaFacile/wasm/rate v0.0.0-00010101000000-000000000000
	gitlab.com/c1560/CryptoFiscaFacile/wasm/tx v0.0.0-00010101000000-000000000000
	gitlab.com/c1560/CryptoFiscaFacile/wasm/wallet v0.0.0-00010101000000-000000000000
	golang.org/x/time v0.5.0
)

require (
	github.com/go-resty/resty/v2 v2.13.1 // indirect
	github.com/gobwas/ws v1.1.0 // indirect
	github.com/gogs/chardet v0.0.0-20211120154057-b7413eaefb8f // indirect
	github.com/hack-pad/safejs v0.1.0 // indirect
	github.com/klauspost/compress v1.10.3 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/net v0.25.0 // indirect
	golang.org/x/text v0.15.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	nhooyr.io/websocket v1.8.7 // indirect
)
