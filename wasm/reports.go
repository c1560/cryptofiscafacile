package main

import (
	"errors"
	"log"
	"sort"
	"strconv"
	"strings"
	"syscall/js"
	"time"

	"github.com/360EntSecGroup-Skylar/excelize"
	"github.com/davecgh/go-spew/spew"
	"github.com/hack-pad/go-indexeddb/idb"
	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/wallet"
)

type configUser struct {
	name    string
	surname string
	email   string
	proxy   string
}

type configAccount struct {
	id       string
	opened   time.Time
	closed   time.Time
	isClosed bool
}

type configCashIn struct {
	// add incomes to cashins
	choice bool
	// choice made
	isMade   bool
	isLocked bool
}

type configAPI struct {
	arg1 string
	arg2 string
	arg3 string
}

type c2086 struct {
	FirstTxDate   time.Time
	InitialPta    decimal.Decimal
	InitialPtaTxs []c2086InitTx
	years         map[int]c2086Year
}

type c2086InitTx struct {
	Date            time.Time
	Asset           string
	QuantityPending decimal.Decimal
	QuantityFound   decimal.Decimal
	QuantityOut     decimal.Decimal
	QuantityIn      decimal.Decimal
	FiatValue       decimal.Decimal
	PtaValue        decimal.Decimal
	Taxable         bool
	Note            string
}

type c2086Year struct {
	IncomesAsCashIn bool
	// Incomes
	AirDrops    decimal.Decimal
	CommRebates decimal.Decimal
	Deposits    decimal.Decimal
	Gifts       decimal.Decimal
	Interests   decimal.Decimal
	Minings     decimal.Decimal
	Referrals   decimal.Decimal
	Withdrawals decimal.Decimal
	// Results
	CashOuts           []tx.Cession
	TotalPurchasePrice decimal.Decimal
	CapitalFraction    decimal.Decimal
	RealizedPnl        decimal.Decimal
	UnrealizedPnl      decimal.Decimal
	BncAmount          decimal.Decimal
	BncCategories      []tx.FiscalCateg
	BncReal            bool
	ForeignInterests   decimal.Decimal
}

const databaseNameConfs = "wasm-db-confs"

var dbConfs *idb.Database
var confUser = configUser{name: "INCONNUE", surname: "Identité", email: "Aller sur Rapports", proxy: ""}
var confAccounts = map[string][]configAccount{}
var confCashIn = map[int]configCashIn{}
var confAPI = map[string]configAPI{}

func updateConfCashIn() {
	for year := 2019; year <= time.Now().Year(); year++ {
		airdrops := allTXs.Total(tx.AirDrops, year, tx.Void)
		if airdrops.IsPositive() {
			if _, ok := confCashIn[year]; !ok {
				confCashIn[year] = configCashIn{}
			}
			continue
		}
		interests := allTXs.Total(tx.Interests, year, tx.Void)
		if interests.IsPositive() {
			if _, ok := confCashIn[year]; !ok {
				confCashIn[year] = configCashIn{}
			}
			continue
		}
		commRebates := allTXs.Total(tx.CommercialRebates, year, tx.Void)
		if commRebates.IsPositive() {
			if _, ok := confCashIn[year]; !ok {
				confCashIn[year] = configCashIn{}
			}
			continue
		}
		referrals := allTXs.Total(tx.Referrals, year, tx.Void)
		if referrals.IsPositive() {
			if _, ok := confCashIn[year]; !ok {
				confCashIn[year] = configCashIn{}
			}
			continue
		}
	}
}

func dbConfsUpgrader(ldb *idb.Database, oldVersion, newVersion uint) error {
	names, _ := ldb.ObjectStoreNames()
	firstUseConfsDBUser := true
	firstUseConfsDBAccounts := true
	firstUseConfsDBCashIn := true
	firstUseConfsDBAPI := true
	for _, name := range names {
		if name == "user" {
			firstUseConfsDBUser = false
		}
		if name == "accounts" {
			firstUseConfsDBAccounts = false
		}
		if name == "cashin" {
			firstUseConfsDBCashIn = false
		}
		if name == "api" {
			firstUseConfsDBAPI = false
		}
	}
	if firstUseConfsDBUser {
		_, err := ldb.CreateObjectStore("user", idb.ObjectStoreOptions{})
		if err != nil {
			return err
		}
	}
	if firstUseConfsDBAccounts {
		_, err := ldb.CreateObjectStore("accounts", idb.ObjectStoreOptions{})
		if err != nil {
			return err
		}
	}
	if firstUseConfsDBCashIn {
		_, err := ldb.CreateObjectStore("cashin", idb.ObjectStoreOptions{})
		if err != nil {
			return err
		}
	}
	if firstUseConfsDBAPI {
		_, err := ldb.CreateObjectStore("api", idb.ObjectStoreOptions{})
		if err != nil {
			return err
		}
	}
	return nil
}

func initConfsDB() {
	// increment ver when you want to change the dbConfs structure
	ver := uint(3)
	openRequest, _ := idb.Global().Open(ctx, databaseNameConfs, ver, dbConfsUpgrader)
	dbConfs, _ = openRequest.Await(ctx)
	txnUser, err := dbConfs.Transaction(idb.TransactionReadOnly, "user")
	if err == nil {
		storeUser, err := txnUser.ObjectStore("user")
		if err == nil {
			reqName, err := storeUser.Get(js.ValueOf("name"))
			if err == nil {
				name, err := reqName.Await(ctx)
				if err == nil {
					if !name.IsUndefined() {
						confUser.name = name.String()
					}
				}
			}
			reqSurname, err := storeUser.Get(js.ValueOf("surname"))
			if err == nil {
				surname, err := reqSurname.Await(ctx)
				if err == nil {
					if !surname.IsUndefined() {
						confUser.surname = surname.String()
					}
				}
			}
			reqEmail, err := storeUser.Get(js.ValueOf("email"))
			if err == nil {
				email, err := reqEmail.Await(ctx)
				if err == nil {
					if !email.IsUndefined() {
						confUser.email = email.String()
					}
				}
			}
			reqProxy, err := storeUser.Get(js.ValueOf("proxy"))
			if err == nil {
				proxy, err := reqProxy.Await(ctx)
				if err == nil {
					if !proxy.IsUndefined() {
						confUser.proxy = proxy.String()
					}
				}
			}
		}
	}
	txnAccounts, err := dbConfs.Transaction(idb.TransactionReadOnly, "accounts")
	if err == nil {
		storeAccounts, err := txnAccounts.ObjectStore("accounts")
		if err == nil {
			countRequestAccounts, err := storeAccounts.Count()
			if err == nil {
				confsCountAccounts, err := countRequestAccounts.Await(ctx)
				if err == nil {
					if confsCountAccounts > 0 {
						cursorRequestAccounts, err := storeAccounts.OpenCursor(idb.CursorNext)
						if err == nil {
							cursorRequestAccounts.Iter(ctx, func(cursor *idb.CursorWithValue) error {
								accName, err := cursor.Key()
								if err != nil {
									return err
								}
								accs, err := cursor.Value()
								if err != nil {
									return err
								}
								if accs.Type() == js.TypeObject {
									for i := 0; i < accs.Length(); i++ {
										a := accs.Index(i)
										confAccounts[accName.String()] = append(confAccounts[accName.String()], configAccount{
											id:       a.Get("id").String(),
											opened:   time.UnixMilli(int64(a.Get("opened").Call("getTime").Int())),
											closed:   time.UnixMilli(int64(a.Get("closed").Call("getTime").Int())),
											isClosed: a.Get("isClosed").Bool(),
										})
									}
								}
								return nil
							})
						}
					}
				}
			}
		}
	}
	txnCashIn, err := dbConfs.Transaction(idb.TransactionReadOnly, "cashin")
	if err == nil {
		storeCashIn, err := txnCashIn.ObjectStore("cashin")
		if err == nil {
			countRequestCashIn, err := storeCashIn.Count()
			if err == nil {
				confsCountCashIn, err := countRequestCashIn.Await(ctx)
				if err == nil {
					if confsCountCashIn > 0 {
						cursorRequestCashIn, err := storeCashIn.OpenCursor(idb.CursorNext)
						if err == nil {
							cursorRequestCashIn.Iter(ctx, func(cursor *idb.CursorWithValue) error {
								year, err := cursor.Key()
								if err != nil {
									return err
								}
								cashin, err := cursor.Value()
								if err != nil {
									return err
								}
								if cashin.Type() == js.TypeObject {
									confCashIn[year.Int()] = configCashIn{choice: cashin.Get("choice").Bool(), isLocked: cashin.Get("isLocked").Bool(), isMade: true}
								}
								return nil
							})
						}
					}
				}
			}
		}
	}
	txnAPI, err := dbConfs.Transaction(idb.TransactionReadOnly, "api")
	if err == nil {
		storeAPI, err := txnAPI.ObjectStore("api")
		if err == nil {
			countRequestAPI, err := storeAPI.Count()
			if err == nil {
				confsCountAPI, err := countRequestAPI.Await(ctx)
				if err == nil {
					if confsCountAPI > 0 {
						cursorRequestAPI, err := storeAPI.OpenCursor(idb.CursorNext)
						if err == nil {
							cursorRequestAPI.Iter(ctx, func(cursor *idb.CursorWithValue) error {
								provider, err := cursor.Key()
								if err != nil {
									return err
								}
								conf, err := cursor.Value()
								if err != nil {
									return err
								}
								if conf.Type() == js.TypeObject {
									confAPI[provider.String()] = configAPI{arg1: conf.Get("arg1").String(), arg2: conf.Get("arg2").String(), arg3: conf.Get("arg3").String()}
								}
								return nil
							})
						}
					}
				}
			}
		}
	}
}

func saveConfig(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("saveConfig()")
		spew.Dump(args)
	}
	proxy := args[0]
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			confUser.proxy = proxy.String()
			if dbConfs == nil {
				reject.Invoke(js.Global().Get("Error").New(databaseNameConfs + " not Initialized"))
				return
			}
			txn, err := dbConfs.Transaction(idb.TransactionReadWrite, "user")
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			store, err := txn.ObjectStore("user")
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			store.PutKey(js.ValueOf("proxy"), proxy)
			resolve.Invoke()
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func saveUser(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("saveUser()")
		spew.Dump(args)
	}
	name := args[0]
	surname := args[1]
	email := args[2]
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			confUser.name = name.String()
			confUser.surname = surname.String()
			confUser.email = email.String()
			if dbConfs == nil {
				reject.Invoke(js.Global().Get("Error").New(databaseNameConfs + " not Initialized"))
				return
			}
			txn, err := dbConfs.Transaction(idb.TransactionReadWrite, "user")
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			store, err := txn.ObjectStore("user")
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			store.PutKey(js.ValueOf("name"), name)
			store.PutKey(js.ValueOf("surname"), surname)
			store.PutKey(js.ValueOf("email"), email)
			resolve.Invoke()
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func saveWalletAccountsDB(wallet string) error {
	if dbConfs == nil {
		return errors.New(databaseNameConfs + " not Initialized")
	}
	txn, err := dbConfs.Transaction(idb.TransactionReadWrite, "accounts")
	if err != nil {
		return err
	}
	store, err := txn.ObjectStore("accounts")
	if err != nil {
		return err
	}
	dateConstructor := js.Global().Get("Date")
	accounts := make([]interface{}, len(confAccounts[wallet]))
	for j, ac := range confAccounts[wallet] {
		account := make(map[string]interface{})
		account["id"] = ac.id
		account["opened"] = dateConstructor.New(ac.opened.UnixMilli())
		account["closed"] = dateConstructor.New(ac.closed.UnixMilli())
		account["isClosed"] = ac.isClosed
		accounts[j] = account
	}
	store.PutKey(js.ValueOf(wallet), js.ValueOf(accounts))
	return nil
}

func saveAccounts(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("saveAccounts()")
		spew.Dump(args)
	}
	wallet := args[0]
	accounts := args[1]
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			if accounts.Type() == js.TypeObject {
				confAccounts[wallet.String()] = []configAccount{}
				for i := 0; i < accounts.Length(); i++ {
					a := accounts.Index(i)
					confAccounts[wallet.String()] = append(confAccounts[wallet.String()], configAccount{
						id:       a.Get("id").String(),
						opened:   time.UnixMilli(int64(a.Get("opened").Call("getTime").Int())),
						closed:   time.UnixMilli(int64(a.Get("closed").Call("getTime").Int())),
						isClosed: a.Get("isClosed").Bool(),
					})
				}
			}
			err := saveWalletAccountsDB(wallet.String())
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			resolve.Invoke()
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func saveChoiceCashIn(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("saveChoiceCashIn()")
		spew.Dump(args)
	}
	year := args[0]
	isCashIn := args[1]
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]

		go func() {
			confCashIn[year.Int()] = configCashIn{choice: isCashIn.Bool(), isLocked: false, isMade: true}
			if dbConfs == nil {
				reject.Invoke(js.Global().Get("Error").New(databaseNameConfs + " not Initialized"))
				return
			}
			txn, err := dbConfs.Transaction(idb.TransactionReadWrite, "cashin")
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			store, err := txn.ObjectStore("cashin")
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			cashinYear := make(map[string]interface{})
			cashinYear["choice"] = isCashIn.Bool()
			cashinYear["isLocked"] = false
			store.PutKey(year, js.ValueOf(cashinYear))
			resolve.Invoke()
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func saveProviderAPIConfDB(provider string) error {
	if dbConfs == nil {
		return errors.New(databaseNameConfs + " not Initialized")
	}
	txn, err := dbConfs.Transaction(idb.TransactionReadWrite, "api")
	if err != nil {
		return err
	}
	store, err := txn.ObjectStore("api")
	if err != nil {
		return err
	}
	conf := make(map[string]interface{})
	conf["arg1"] = confAPI[provider].arg1
	conf["arg2"] = confAPI[provider].arg2
	conf["arg3"] = confAPI[provider].arg3
	store.PutKey(js.ValueOf(provider), js.ValueOf(conf))
	return nil
}

func getUserInfos(this js.Value, args []js.Value) interface{} {
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]

		go func() {
			if dbConfs == nil {
				reject.Invoke(js.Global().Get("Error").New(databaseNameConfs + " not Initialized"))
				return
			}
			user := make(map[string]interface{})
			user["name"] = confUser.name
			user["surname"] = confUser.surname
			user["email"] = confUser.email
			user["proxy"] = confUser.proxy
			if debug {
				log.Println("getUserInfos():")
				spew.Dump(user)
			}
			resolve.Invoke(user)
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func getReports(this js.Value, args []js.Value) interface{} {
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			if dbConfs == nil {
				reject.Invoke(js.Global().Get("Error").New(databaseNameConfs + " not Initialized"))
				return
			}
			// User section
			user := make(map[string]interface{})
			user["name"] = confUser.name
			user["surname"] = confUser.surname
			user["email"] = confUser.email
			user["proxy"] = confUser.proxy
			// Cerfa3916 section
			stats := allTXs.GetStatsByWallet()
			// Add empty wallets
			for _, statusTX := range statusTXs {
				walletName := statusTX.(map[string]interface{})["wallet"]
				if walletName != nil {
					if _, exist := stats[walletName.(string)]; !exist {
						stats[walletName.(string)] = tx.Stats{Oldest: time.Now()}
					}
				}
			}
			for name := range stats {
				w, exist := wallet.Get(name)
				if !exist || !w.Custodial { // only Custodial
					delete(stats, name)
				}
			}
			wallets := make([]interface{}, len(stats))
			i := 0
			for name, stat := range stats {
				wall := make(map[string]interface{})
				w, _ := wallet.Get(name)
				wall["name"] = name
				wall["logo"] = w.Logo
				wall["crypto"] = w.Crypto
				dateConstructor := js.Global().Get("Date")
				wall["firsttx"] = dateConstructor.New(stat.Oldest.UnixMilli())
				wall["lasttx"] = dateConstructor.New(stat.Newest.UnixMilli())
				wall["legal"] = w.LegalName
				wall["address"] = w.Address
				wall["url"] = w.URL
				accounts := make([]interface{}, len(confAccounts[name]))
				for j, ac := range confAccounts[name] {
					account := make(map[string]interface{})
					account["id"] = ac.id
					if ac.opened.IsZero() {
						account["opened"] = dateConstructor.New(stat.Oldest.UnixMilli())
					} else {
						account["opened"] = dateConstructor.New(ac.opened.UnixMilli())
					}
					if ac.closed.IsZero() {
						account["closed"] = dateConstructor.New(stat.Newest.UnixMilli())
					} else {
						account["closed"] = dateConstructor.New(ac.closed.UnixMilli())
					}
					account["isClosed"] = ac.isClosed
					accounts[j] = account
				}
				wall["accounts"] = accounts
				wallets[i] = wall
				i++
			}
			cerfa3916 := make(map[string]interface{})
			cerfa3916["wallets"] = wallets

			updateConfCashIn()
			cashin := make([]interface{}, len(confCashIn))
			i = 0
			for k, v := range confCashIn {
				cashinYear := make(map[string]interface{})
				cashinYear["year"] = k
				cashinYear["choice"] = v.choice
				cashinYear["isMade"] = v.isMade
				cashinYear["isLocked"] = v.isLocked
				cashin[i] = cashinYear
				i++
			}
			cerfa2086 := make(map[string]interface{})
			cerfa2086["cashin"] = cashin
			reports := make(map[string]interface{})
			reports["user"] = user
			reports["cerfa3916"] = cerfa3916
			reports["cerfa2086"] = cerfa2086
			if len(allTXs) > 0 {
				reports["hasTX"] = true
			} else {
				reports["hasTX"] = false
			}
			if debug {
				log.Println("getReports():")
				spew.Dump(reports)
			}
			resolve.Invoke(reports)
		}()

		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func get2086Xlsx(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("get2086XLSX():")
	}
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			europeParis, _ := time.LoadLocation("Europe/Paris")
			until := time.Date(time.Now().Year()-1, 12, 31, 23, 59, 59, 999999999, europeParis)
			dataJs, err := generate2086Xlsx(until)

			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
			} else {
				resolve.Invoke(dataJs)
			}
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func get2086XlsxUpToToday(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("get2086XlsxUpToToday():")
	}
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			dataJs, err := generate2086Xlsx(time.Now())

			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
			} else {
				resolve.Invoke(dataJs)
			}
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func generate2086Xlsx(until time.Time) (dataJs js.Value, err error) {
	c2086Result, err := compute2086(until)
	if err != nil {
		return
	}
	f := excelize.NewFile()
	// PTA Initial
	europeParis, _ := time.LoadLocation("Europe/Paris")
	if c2086Result.FirstTxDate.Before(time.Date(2020, 12, 31, 23, 59, 59, 999999999, europeParis)) {
		sheet := "Prix Total Acquisition PEPS"
		f.NewSheet(sheet)
		f.SetCellValue(sheet, "A1", "Votre stock d'actifs numériques a été valorisé à "+c2086Result.InitialPta.String()+"€ au 1 janvier 2019.")
		f.SetCellValue(sheet, "A2", "Ce prix total d'acquisition à été calculé par une itération antichronologique de vos encaissements d'actifs selon la méthode du premier entier premier sorti.")
		f.SetCellValue(sheet, "A3", "Une réception d'actif suite à un échange est valorisée à son cours du jour. Une réception d'actif suite à un cashin est valorisée à votre prix d'achat effectif.")

		f.SetCellValue(sheet, "A5", "Date")
		f.SetCellValue(sheet, "B5", "Crypto")
		f.SetCellValue(sheet, "C5", "Quantité à Valoriser")
		f.SetCellValue(sheet, "D5", "Quantité trouvée")
		f.SetCellValue(sheet, "E5", "Quantité Entrée")
		f.SetCellValue(sheet, "F5", "Quantité Sortie")
		f.SetCellValue(sheet, "G5", "Valeur EUR")
		f.SetCellValue(sheet, "H5", "Valeur PTA")
		f.SetCellValue(sheet, "I5", "Imposable")
		f.SetCellValue(sheet, "J5", "Note")
		row := 6
		centeredColumn, _ := f.NewStyle(`{"alignment":{"horizontal":"center"}}`)
		for _, initialPtaTx := range c2086Result.InitialPtaTxs {
			rowAsString := strconv.Itoa(row)
			f.SetCellValue(sheet, "A"+rowAsString, initialPtaTx.Date.Format("02/01/2006 15:04:05"))
			f.SetCellValue(sheet, "B"+rowAsString, initialPtaTx.Asset)
			toFind, _ := initialPtaTx.QuantityPending.Float64()
			f.SetCellValue(sheet, "C"+rowAsString, toFind)
			if !initialPtaTx.QuantityFound.IsZero() {
				found, _ := initialPtaTx.QuantityFound.Float64()
				f.SetCellValue(sheet, "D"+rowAsString, found)
			}
			if !initialPtaTx.QuantityIn.IsZero() {
				in, _ := initialPtaTx.QuantityIn.Float64()
				f.SetCellValue(sheet, "E"+rowAsString, in)
			}
			if !initialPtaTx.QuantityOut.IsZero() {
				out, _ := initialPtaTx.QuantityOut.Float64()
				f.SetCellValue(sheet, "F"+rowAsString, out)
			}
			valEur, _ := initialPtaTx.FiatValue.RoundBank(2).Float64()
			f.SetCellValue(sheet, "G"+rowAsString, valEur)
			if !initialPtaTx.PtaValue.IsZero() {
				valPta, _ := initialPtaTx.PtaValue.RoundBank(2).Float64()
				f.SetCellValue(sheet, "H"+rowAsString, valPta)
			}
			if initialPtaTx.Taxable {
				f.SetCellStyle(sheet, "I"+rowAsString, "I"+rowAsString, centeredColumn)
				f.SetCellValue(sheet, "I"+rowAsString, "X")
			}
			f.SetCellValue(sheet, "J"+strconv.Itoa(row), initialPtaTx.Note)
			row++
		}
		f.SetColWidth(sheet, "A", "A", 19)
		f.SetColWidth(sheet, "C", "C", 17)
		f.SetColWidth(sheet, "D", "D", 17)
		f.SetColWidth(sheet, "E", "E", 17)
		f.SetColWidth(sheet, "F", "F", 17)
		f.SetColWidth(sheet, "J", "J", 50)
	}
	// 2086 par année
	for year := c2086Result.FirstTxDate.Year(); year <= until.Year(); year++ {
		sheet := strconv.Itoa(year)
		f.NewSheet(sheet)
		style, _ := f.NewStyle(`{"font":{"bold":true, "size":12, "family":"DejaVu Sans"}}`)
		f.SetCellStyle(sheet, "B1", "B1", style)
		f.SetCellValue(sheet, "B1", "Déclaration "+strconv.Itoa(year+1)+" sur les revenus de "+strconv.Itoa(year))
		f.SetCellValue(sheet, "A4", 211)
		f.SetCellValue(sheet, "A5", 212)
		f.SetCellValue(sheet, "A6", 213)
		f.SetCellValue(sheet, "A7", 214)
		f.SetCellValue(sheet, "A8", 215)
		f.SetCellValue(sheet, "A9", 216)
		f.SetCellValue(sheet, "A10", 217)
		f.SetCellValue(sheet, "A11", 218)
		f.SetCellValue(sheet, "A12", 220)
		f.SetCellValue(sheet, "A13", 221)
		f.SetCellValue(sheet, "A14", 222)
		f.SetCellValue(sheet, "A15", 223)
		f.SetCellValue(sheet, "A18", 224)
		f.SetCellValue(sheet, "B3", "Cession")
		f.SetCellValue(sheet, "B4", "Date de la cession")
		f.SetCellValue(sheet, "B5", "Valeur globale du portefeuille au moment de la cession")
		f.SetCellValue(sheet, "B6", "Prix de cession")
		f.SetCellValue(sheet, "B7", "Frais de cession")
		f.SetCellValue(sheet, "B8", "Prix de cession net des frais")
		f.SetCellValue(sheet, "B9", "Soulte reçue ou versée lors de la cession")
		f.SetCellValue(sheet, "B10", "Prix de cession net des soultes")
		f.SetCellValue(sheet, "B11", "Prix de cession net des frais et soultes")
		f.SetCellValue(sheet, "B12", "Prix total d’acquisition")
		f.SetCellValue(sheet, "B13", "Fractions de capital initial contenues dans le prix total d’acquisition")
		f.SetCellValue(sheet, "B14", "Soultes reçues en cas d’échanges antérieurs à la cession")
		f.SetCellValue(sheet, "B15", "Prix total d’acquisition net")
		f.SetCellValue(sheet, "B16", "Plus-values et moins-values")
		f.SetCellValue(sheet, "B18", "Plus-value ou moins-value globale")
		f.SetColWidth(sheet, "B", "B", 60)

		col := "C"
		count := 1
		c2086YearResult := c2086Result.years[year]
		for _, c := range c2086YearResult.CashOuts {
			if c.Date211.After(time.Date(year, time.January, 1, 0, 0, 0, 0, time.UTC)) {
				if c.Date211.Before(time.Date(year, time.December, 31, 23, 59, 59, 999, time.UTC)) {
					f.SetCellValue(sheet, col+"3", "#"+strconv.Itoa(count))
					f.AddComment(sheet, col+"3", `{"author":"`+c.Source+`: ","text":"`+c.Note+`"}`)
					f.SetCellValue(sheet, col+"4", c.Date211.Format("02/01/2006"))
					f.SetCellValue(sheet, col+"5", c.ValeurPortefeuille212.RoundBank(0).IntPart())
					f.SetCellValue(sheet, col+"6", c.Prix213.RoundBank(0).IntPart())
					f.SetCellValue(sheet, col+"7", c.Frais214.RoundBank(0).IntPart())
					f.SetCellValue(sheet, col+"8", c.PrixNetDeFrais215.RoundBank(0).IntPart())
					f.SetCellValue(sheet, col+"9", c.SoulteRecueOuVersee216.RoundBank(0).IntPart())
					f.SetCellValue(sheet, col+"10", c.PrixNetDeSoulte217.RoundBank(0).IntPart())
					f.SetCellValue(sheet, col+"11", c.PrixNet218.RoundBank(0).IntPart())
					f.SetCellValue(sheet, col+"12", c.PrixTotalAcquisition220.RoundBank(0).IntPart())
					f.SetCellValue(sheet, col+"13", c.FractionDeCapital221.RoundBank(0).IntPart())
					f.SetCellValue(sheet, col+"14", c.SoulteRecueEnCasDechangeAnterieur222.RoundBank(0).IntPart())
					f.SetCellValue(sheet, col+"15", c.PrixTotalAcquisitionNet223.RoundBank(0).IntPart())
					f.SetCellValue(sheet, col+"16", c.PlusMoinsValue.RoundBank(0).IntPart())
					count++
					num := count + 2
					col = ""
					for num > 0 {
						col = string(rune((num-1)%26+65)) + col
						num = (num - 1) / 26
					}
				} else {
					break
				}
			}
		}

		f.SetCellValue(sheet, "C18", c2086YearResult.RealizedPnl.RoundBank(0).IntPart())
		f.SetCellValue(sheet, "A20", "Voici votre récapitulatif par catégorie de l'année fiscale "+sheet+" :")
		f.SetCellValue(sheet, "A21", "- Airdrops fortuits : "+c2086YearResult.AirDrops.RoundBank(2).String()+"€")
		f.SetCellValue(sheet, "A22", "- Remises commerciales (cashback, etc) : "+c2086YearResult.CommRebates.RoundBank(2).String()+"€")
		f.SetCellValue(sheet, "A23", "- Intérêts (lending, etc) : "+c2086YearResult.Interests.RoundBank(2).String()+"€")
		f.SetCellValue(sheet, "A24", "- Revenus de récompenses (staking, mining, aidrops avec contrepartie, etc) : "+c2086YearResult.Minings.RoundBank(2).String()+"€")
		f.SetCellValue(sheet, "A25", "- Revenus de parrainage : "+c2086YearResult.Referrals.RoundBank(2).String()+"€")
		f.SetCellValue(sheet, "A26", "Pour information : ")

		if !c2086YearResult.Deposits.IsZero() {
			// Put line in Bold
			style, _ := f.NewStyle(`{"font":{"bold":true}}`)
			f.SetCellStyle(sheet, "A27", "A27", style)
		}
		f.SetCellValue(sheet, "A27", " - "+c2086YearResult.Deposits.RoundBank(2).String()+"€ de Dépôts non identifiés ont été considérés comme des Dons reçus imposables !")

		if !c2086YearResult.Withdrawals.IsZero() {
			// Put line in Bold
			style, _ := f.NewStyle(`{"font":{"bold":true}}`)
			f.SetCellStyle(sheet, "A28", "A28", style)
		}
		f.SetCellValue(sheet, "A28", " - "+c2086YearResult.Withdrawals.RoundBank(2).String()+"€ de Retraits non identifiés ont été considérés comme des CashOut imposables !")

		f.SetCellValue(sheet, "A29", " - vous avez un total de "+c2086YearResult.AirDrops.Add(c2086YearResult.CommRebates).RoundBank(0).String()+"€ non imposable (airdrops fortuits + remises commerciales).")
		if confCashIn[year].choice {
			f.SetCellValue(sheet, "A30", "Voici vos autres obligations déclaratives dues aux cryptos-actifs (vous avez choisi de rentrer au Portefeuille les Parrainages/Intérêts valorisés au taux du jour) :")
		} else {
			f.SetCellValue(sheet, "A30", "Voici vos autres obligations déclaratives dues aux cryptos-actifs :")
		}

		categories := []string{}
		for _, categ := range c2086YearResult.BncCategories {
			if categ == tx.Minings {
				categories = append(categories, "récompenses de minage")
			}
			if categ == tx.Referrals {
				categories = append(categories, "parrainage")
			}
		}
		amountBNC := c2086YearResult.BncAmount
		detailsBNC := " (" + strings.Join(categories, " + ") + ")"
		case2TR := c2086YearResult.ForeignInterests
		lieuBNC := "- Régime Micro BNC (case 5KU du formulaire 2042-C-PRO) : "
		if c2086YearResult.BncReal {
			lieuBNC = "- Régime BNC réel (formulaire 2035) : "
		}
		f.SetCellValue(sheet, "A29", lieuBNC+amountBNC.RoundBank(0).String()+"€"+detailsBNC)
		f.SetCellValue(sheet, "A30", "- case 2TR du formulaire 2047 et à reporter sur la déclaration principale : "+case2TR.RoundBank(0).String()+"€ (intérêts)")
		f.SetCellValue(sheet, "A31", "- dons manuels : "+c2086YearResult.Gifts.RoundBank(0).String()+"€ (dons reçus)")

		if c2086YearResult.RealizedPnl.RoundBank(0).IntPart() > 0 {
			style, _ = f.NewStyle(`{"font":{"bold":true, "size":12}}`)
			f.SetCellStyle(sheet, "A35", "A35", style)
			f.SetCellValue(sheet, "A35", "NOTA: Vérifiez le bon report automatique de votre plus value globale "+c2086YearResult.RealizedPnl.RoundBank(0).String()+"dans la case 3AN.")
		} else {
			style, _ = f.NewStyle(`{"font":{"bold":true, "size":12}}`)
			f.SetCellStyle(sheet, "A35", "A35", style)
			f.SetCellValue(sheet, "A35", "NOTA: Vérifiez le bon report automatique de votre moins value globale "+c2086YearResult.RealizedPnl.RoundBank(0).String()+"dans la case 3BN.")
		}
	}
	f.DeleteSheet("Sheet1")
	buf, err := f.WriteToBuffer()
	if err != nil {
		return
	}
	data := buf.Bytes()
	arrayConstructor := js.Global().Get("Uint8Array")
	dataJs = arrayConstructor.New(len(data))
	js.CopyBytesToJS(dataJs, data)

	return dataJs, err
}

func get2086Results(this js.Value, args []js.Value) interface{} {
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			europeParis, _ := time.LoadLocation("Europe/Paris")
			until := time.Date(time.Now().Year()-1, 12, 31, 23, 59, 59, 999999999, europeParis)
			c2086, err := compute2086(until)
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}

			dateConstructor := js.Global().Get("Date")
			exportedC2086 := make(map[string]interface{}, 1)
			exportedC2086["initialPta"], _ = c2086.InitialPta.Float64()

			initialPtaTxs := make([]interface{}, len(c2086.InitialPtaTxs))
			for i := 0; i < len(c2086.InitialPtaTxs); i++ {
				initialPtaTx := make(map[string]interface{}, 1)
				initialPtaTx["asset"] = c2086.InitialPtaTxs[i].Asset
				initialPtaTx["date"] = dateConstructor.New(c2086.InitialPtaTxs[i].Date.UnixMilli())
				initialPtaTx["fiatValue"], _ = c2086.InitialPtaTxs[i].FiatValue.RoundBank(2).Float64()
				initialPtaTx["ptaValue"], _ = c2086.InitialPtaTxs[i].PtaValue.RoundBank(2).Float64()
				initialPtaTx["note"] = c2086.InitialPtaTxs[i].Note
				initialPtaTx["quantityFound"], _ = c2086.InitialPtaTxs[i].QuantityFound.Float64()
				initialPtaTx["quantityIn"], _ = c2086.InitialPtaTxs[i].QuantityIn.Float64()
				initialPtaTx["quantityOut"], _ = c2086.InitialPtaTxs[i].QuantityOut.Float64()
				initialPtaTx["quantityPending"], _ = c2086.InitialPtaTxs[i].QuantityPending.Float64()
				initialPtaTx["taxable"] = c2086.InitialPtaTxs[i].Taxable
				initialPtaTxs[i] = initialPtaTx
			}
			exportedC2086["initialPtaTxs"] = initialPtaTxs

			years := make(map[string]interface{}, len(c2086.years))
			for year, c2086YearResult := range c2086.years {
				exportedC2086Year := make(map[string]interface{}, 1)
				exportedC2086Year["airDrops"] = c2086YearResult.AirDrops.RoundBank(2).InexactFloat64()
				exportedC2086Year["bncAmount"] = c2086YearResult.BncAmount.RoundBank(2).InexactFloat64()
				exportedC2086Year["bncReal"] = c2086YearResult.BncReal
				exportedC2086Year["capitalFraction"] = c2086YearResult.CapitalFraction.RoundBank(2).InexactFloat64()
				exportedC2086Year["commRebates"] = c2086YearResult.CommRebates.RoundBank(2).InexactFloat64()
				exportedC2086Year["deposits"] = c2086YearResult.Deposits.RoundBank(2).InexactFloat64()
				exportedC2086Year["foreignInterests"] = c2086YearResult.ForeignInterests.RoundBank(2).InexactFloat64()
				exportedC2086Year["gifts"] = c2086YearResult.Gifts.RoundBank(2).InexactFloat64()
				exportedC2086Year["incomesAsCashIn"] = c2086YearResult.IncomesAsCashIn
				exportedC2086Year["interests"] = c2086YearResult.Interests.RoundBank(2).InexactFloat64()
				exportedC2086Year["minings"] = c2086YearResult.Minings.RoundBank(2).InexactFloat64()
				exportedC2086Year["realizedPnl"] = c2086YearResult.RealizedPnl.RoundBank(0).InexactFloat64()
				exportedC2086Year["referrals"] = c2086YearResult.Referrals.RoundBank(2).InexactFloat64()
				exportedC2086Year["totalPurchasePrice"] = c2086YearResult.TotalPurchasePrice.RoundBank(2).InexactFloat64()
				exportedC2086Year["withdrawals"] = c2086YearResult.Withdrawals.RoundBank(2).InexactFloat64()

				bncCategories := make([]interface{}, len(c2086YearResult.BncCategories))
				for i := 0; i < len(bncCategories); i++ {
					bncCategories[i] = fiscalCateg2French(c2086YearResult.BncCategories[i])
				}
				exportedC2086Year["bncCategories"] = bncCategories

				cashOuts := make([]interface{}, len(c2086YearResult.CashOuts))
				for i := 0; i < len(c2086YearResult.CashOuts); i++ {
					cashOut := make(map[string]interface{}, 1)
					cashOut["source"] = c2086YearResult.CashOuts[i].Source
					cashOut["note"] = c2086YearResult.CashOuts[i].Note
					cashOut["date211"] = dateConstructor.New(c2086YearResult.CashOuts[i].Date211.UnixMilli())
					cashOut["valeurPortefeuille212"] = c2086YearResult.CashOuts[i].ValeurPortefeuille212.RoundBank(0).IntPart()
					cashOut["prix213"] = c2086YearResult.CashOuts[i].Prix213.RoundBank(0).IntPart()
					cashOut["frais214"] = c2086YearResult.CashOuts[i].Frais214.RoundBank(0).IntPart()
					cashOut["prixNetDeFrais215"] = c2086YearResult.CashOuts[i].PrixNetDeFrais215.RoundBank(0).IntPart()
					cashOut["soulteRecueOuVersee216"] = c2086YearResult.CashOuts[i].SoulteRecueOuVersee216.RoundBank(0).IntPart()
					cashOut["prixNetDeSoulte217"] = c2086YearResult.CashOuts[i].PrixNetDeSoulte217.RoundBank(0).IntPart()
					cashOut["prixNet218"] = c2086YearResult.CashOuts[i].PrixNet218.RoundBank(0).IntPart()
					cashOut["prixTotalAcquisition220"] = c2086YearResult.CashOuts[i].PrixTotalAcquisition220.RoundBank(0).IntPart()
					cashOut["fractionDeCapital221"] = c2086YearResult.CashOuts[i].FractionDeCapital221.RoundBank(0).IntPart()
					cashOut["soulteRecueEnCasDechangeAnterieur222"] = c2086YearResult.CashOuts[i].SoulteRecueEnCasDechangeAnterieur222.RoundBank(0).IntPart()
					cashOut["prixTotalAcquisitionNet223"] = c2086YearResult.CashOuts[i].PrixTotalAcquisitionNet223.RoundBank(0).IntPart()
					cashOut["plusMoinsValue"] = c2086YearResult.CashOuts[i].PlusMoinsValue.RoundBank(0).IntPart()
					cashOuts[i] = cashOut
				}
				exportedC2086Year["cashOuts"] = cashOuts

				years[strconv.Itoa(year)] = exportedC2086Year
			}
			exportedC2086["years"] = years

			if debug {
				log.Println("get2086Results():")
				spew.Dump(exportedC2086)
			}
			resolve.Invoke(exportedC2086)
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func compute2086(until time.Time) (c2086Results c2086, err error) {
	// PTA Initial
	c2086Results = c2086{
		InitialPtaTxs: []c2086InitTx{},
	}
	failed := allTXs.CheckConsistancyNegativeBalance(until)
	if len(failed) > 0 {
		return c2086Results, errors.New("il y a toujours des balances négatives")
	}

	c2086Txs := make(tx.TXs, 0)
	for _, tx := range allTXs {
		if tx.Timestamp.Before(until) {
			c2086Txs = append(c2086Txs, tx)
		}
	}
	c2086Txs.SortByDate(true)
	if len(c2086Txs) > 0 {
		c2086Results.FirstTxDate = c2086Txs[0].Timestamp
	}

	for i := range c2086Txs {
		c2086Txs[i].Items = make(map[string]tx.Values)
		for k, v := range allTXs[i].Items {
			c2086Txs[i].Items[k] = v
		}
		c2086Txs[i].NFTs = make(map[string]tx.NFTs)
		for k, v := range allTXs[i].NFTs {
			c2086Txs[i].NFTs[k] = v
		}
	}
	initialPta, err := c2086Txs.InitialPTAFIFO()
	if err != nil {
		return c2086Results, err
	}
	exactInitialPtaAmount := initialPta.PrixTotalAcquisition
	c2086Results.InitialPta = exactInitialPtaAmount.RoundBank(0)

	// Sort initial pta txs by asset
	assets := make([]string, 0, len(initialPta.Acquisitions))
	for coin := range initialPta.Acquisitions {
		assets = append(assets, coin)
	}
	sort.Strings(assets)

	for _, asset := range assets {
		buyPrice := initialPta.Acquisitions[asset]
		for _, vtx := range buyPrice.Transactions {
			tx := c2086InitTx{
				Date:            vtx.TX.Timestamp,
				Asset:           asset,
				QuantityPending: vtx.QtyToFind,
				QuantityFound:   vtx.QtyFound,
				QuantityIn:      vtx.QtyIn,
				QuantityOut:     vtx.QtyOut,
				FiatValue:       vtx.NativeValue,
				PtaValue:        vtx.PtaValue,
				Taxable:         vtx.Taxable,
				Note:            vtx.TX.Note,
			}

			c2086Results.InitialPtaTxs = append(c2086Results.InitialPtaTxs, tx)
		}
	}

	// 2086 par année
	ptaAmount := exactInitialPtaAmount
	fractionCapital := decimal.New(0, 0)
	c2086Results.years = make(map[int]c2086Year)
	for year := 2019; year <= until.Year(); year++ {
		convert := tx.Void
		if confCashIn[year].choice {
			convert = tx.CashIn
		}

		var commRebates decimal.Decimal
		if year < 2021 { // Force CashIn starting on 2021
			commRebates = c2086Txs.Total(tx.CommercialRebates, year, convert)
		} else {
			commRebates = c2086Txs.Total(tx.CommercialRebates, year, tx.CashIn)
		}

		c2086YearResult := c2086Year{
			IncomesAsCashIn: confCashIn[year].choice,
			AirDrops:        c2086Txs.Total(tx.AirDrops, year, convert).RoundBank(2),
			CommRebates:     commRebates.RoundBank(2),
			Deposits:        c2086Txs.Total(tx.Deposits, year, tx.Gifts).RoundBank(2),
			Gifts:           c2086Txs.Total(tx.Gifts, year, tx.CashIn).RoundBank(2),
			Interests:       c2086Txs.Total(tx.Interests, year, convert).RoundBank(2),
			Minings:         c2086Txs.Total(tx.Minings, year, tx.CashIn).RoundBank(2),
			Referrals:       c2086Txs.Total(tx.Referrals, year, convert).RoundBank(2),
			Withdrawals:     c2086Txs.Total(tx.Withdrawals, year, tx.CashOut).RoundBank(2),
			BncCategories:   []tx.FiscalCateg{},
		}

		choice2020required := c2086YearResult.AirDrops.Add(c2086YearResult.Interests).Add(c2086YearResult.CommRebates).Add(c2086YearResult.Referrals).IsPositive() && year < 2021
		choice2021required := c2086YearResult.AirDrops.Add(c2086YearResult.Interests).Add(c2086YearResult.Referrals).IsPositive() && year > 2020
		if (choice2020required || choice2021required) && !confCashIn[year].isMade {
			return c2086Results, errors.New("Un choix d'intégration des catégories spécifiques dans la page Rapports doit être fait pour l'année " + strconv.Itoa(year))
		}

		c2086YearResult.CashOuts, ptaAmount, fractionCapital, err = c2086Txs.GetCessions(year, ptaAmount, fractionCapital)
		if err != nil {
			return c2086Results, err
		}
		c2086YearResult.TotalPurchasePrice = ptaAmount
		c2086YearResult.CapitalFraction = fractionCapital

		for _, c := range c2086YearResult.CashOuts {
			if c.Date211.After(time.Date(year, time.January, 1, 0, 0, 0, 0, time.UTC)) {
				if c.Date211.Before(time.Date(year, time.December, 31, 23, 59, 59, 999, time.UTC)) {
					c2086YearResult.RealizedPnl = c2086YearResult.RealizedPnl.Add(c.PlusMoinsValue)
				} else {
					break
				}
			}
		}
		c2086YearResult.UnrealizedPnl, err = c2086Txs.GetUnrealizedPnl(until, ptaAmount, fractionCapital)
		if err != nil {
			return c2086Results, err
		}

		c2086YearResult.BncAmount = c2086YearResult.Minings
		c2086YearResult.BncCategories = append(c2086YearResult.BncCategories, tx.Minings)
		c2086YearResult.ForeignInterests = decimal.New(0, 0)
		if confCashIn[year].choice {
			c2086YearResult.BncCategories = append(c2086YearResult.BncCategories, tx.Referrals)
			c2086YearResult.BncAmount = c2086YearResult.BncAmount.Add(c2086YearResult.Referrals)
			c2086YearResult.ForeignInterests = c2086YearResult.Interests
		}
		c2086YearResult.BncReal = c2086YearResult.BncAmount.GreaterThanOrEqual(decimal.New(72600, 0))
		c2086Results.years[year] = c2086YearResult
	}

	return c2086Results, nil
}

func add3916Sheet(f *excelize.File, sheet string, w wallet.Wallet, ac configAccount) {
	sanitize := strings.NewReplacer(
		"@", "AROBASE",
		".", "POINT",
		"-", "TIRET",
	)
	f.NewSheet(sheet)
	if w.Crypto {
		f.SetCellValue(sheet, "A1", "4.1 Désignation du compte d'actifs numériques ouvert, détenu, utilisé ou clos à l'étranger")
		f.SetCellValue(sheet, "A2", "Numéro de compte")
		if ac.id != "-" {
			f.SetCellValue(sheet, "B2", sanitize.Replace(ac.id))
		}
		f.SetCellValue(sheet, "A3", "Date d'ouverture*")
		if !ac.opened.IsZero() {
			f.SetCellValue(sheet, "B3", ac.opened.Add(12*time.Hour).Format("02-01-2006"))
		}
		f.SetCellValue(sheet, "A4", "Date de clôture*")
		if ac.isClosed && !ac.closed.IsZero() {
			f.SetCellValue(sheet, "B4", ac.closed.Add(12*time.Hour).Format("02-01-2006"))
		}
		f.SetCellValue(sheet, "A5", "Designation de l'organisme gestionnaire du compte")
		f.SetCellValue(sheet, "B5", w.LegalName)
		f.SetCellValue(sheet, "A6", "Adresse de l'organisme gestionnaire du compte")
		f.SetCellValue(sheet, "B6", w.Address)
		f.SetCellValue(sheet, "A7", "URL du site internet de l'organisme gestionnaire du compte")
		f.SetCellValue(sheet, "B7", w.URL)
	} else {
		f.SetCellValue(sheet, "A1", "3.1 Désignation du compte bancaire ouvert, détenu, utilisé ou clos à l'étranger")
		f.SetCellValue(sheet, "A2", "Numéro de compte")
		if ac.id != "-" {
			f.SetCellValue(sheet, "B2", sanitize.Replace(ac.id))
		}
		f.SetCellValue(sheet, "A3", "Caractéristiques du compte")
		f.SetCellValue(sheet, "B3", "[x] Compte courant")
		f.SetCellValue(sheet, "A4", "Date d'ouverture*")
		if !ac.opened.IsZero() {
			f.SetCellValue(sheet, "B4", ac.opened.Add(12*time.Hour).Format("02-01-2006"))
		}
		f.SetCellValue(sheet, "A5", "Date de clôture*")
		if ac.isClosed && !ac.closed.IsZero() {
			f.SetCellValue(sheet, "B5", ac.closed.Add(12*time.Hour).Format("02-01-2006"))
		}
		f.SetCellValue(sheet, "A6", "Designation de l'organisme gestionnaire du compte")
		f.SetCellValue(sheet, "B6", w.LegalName)
		f.SetCellValue(sheet, "A7", "Adresse de l'organisme gestionnaire du compte")
		f.SetCellValue(sheet, "B7", w.Address)
	}
	f.SetColWidth(sheet, "A", "B", 83)
}

func get3916XLSX(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("get3916XLSX():")
	}
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]

		go func() {
			stats := allTXs.GetStatsByWallet()
			// Add empty wallets
			for _, statusTX := range statusTXs {
				walletName := statusTX.(map[string]interface{})["kind"]
				if _, exist := stats[walletName.(string)]; !exist {
					stats[walletName.(string)] = tx.Stats{Oldest: time.Now()}
				}
			}
			for name := range stats {
				w, exist := wallet.Get(name)
				if !exist || !w.Custodial { // only Custodial
					delete(stats, name)
				}
			}
			if len(stats) == 0 {
				reject.Invoke(js.Global().Get("Error").New("Aucun Portefeuille Custodial n'a été utilisé"))
				return
			}
			f := excelize.NewFile()
			for name := range stats {
				w, exist := wallet.Get(name)
				if exist {
					if _, ok := confAccounts[name]; ok {
						for i, ac := range confAccounts[name] {
							sheet := name
							if len(confAccounts[name]) > 1 {
								sheet += " " + strconv.Itoa(i+1)
							}
							add3916Sheet(f, sheet, *w, ac)
						}
					} else {
						add3916Sheet(f, name, *w, configAccount{})
					}
				}
			}
			f.DeleteSheet("Sheet1")
			buf, err := f.WriteToBuffer()
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			data := buf.Bytes()
			arrayConstructor := js.Global().Get("Uint8Array")
			dataJS := arrayConstructor.New(len(data))
			js.CopyBytesToJS(dataJS, data)
			resolve.Invoke(dataJS)
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func getStocksXLSX(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("getStocksXLSX():")
	}
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			cs := allTXs.Stocks()
			coins := make([]string, 0, len(cs))
			for coin := range cs {
				coins = append(coins, coin)
			}
			sort.Strings(coins)

			f := excelize.NewFile()
			for _, coin := range coins {
				f.NewSheet(coin)
				f.SetCellValue(coin, "A1", "Date (UTC)")
				f.SetCellValue(coin, "B1", "Type d'opération")
				f.SetCellValue(coin, "C1", "Entrée")
				f.SetCellValue(coin, "D1", "Sortie")
				f.SetCellValue(coin, "E1", "Balance")
				f.SetCellValue(coin, "F1", "Note")
				row := 2

				stock := cs[coin]
				for _, op := range stock {
					f.SetCellValue(coin, "A"+strconv.Itoa(row), op.Date.UTC().Format("02/01/2006 15:04:05"))
					f.SetCellValue(coin, "B"+strconv.Itoa(row), op.Name)
					val, _ := op.Amount.Float64()
					if op.Amount.IsPositive() {
						f.SetCellValue(coin, "C"+strconv.Itoa(row), val)
					} else {
						f.SetCellValue(coin, "D"+strconv.Itoa(row), val)
					}
					bal, _ := op.Balance.Float64()
					f.SetCellValue(coin, "E"+strconv.Itoa(row), bal)
					f.SetCellValue(coin, "F"+strconv.Itoa(row), op.Note)
					row++
				}
				f.SetColWidth(coin, "A", "A", 18)
				f.SetColWidth(coin, "B", "B", 16)
				f.SetColWidth(coin, "F", "F", 50)
			}
			f.DeleteSheet("Sheet1")
			buf, err := f.WriteToBuffer()
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			data := buf.Bytes()
			arrayConstructor := js.Global().Get("Uint8Array")
			dataJS := arrayConstructor.New(len(data))
			js.CopyBytesToJS(dataJS, data)
			resolve.Invoke(dataJS)
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}
