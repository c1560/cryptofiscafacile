package main

import (
	"log"
	"syscall/js"

	"github.com/davecgh/go-spew/spew"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/csv"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/utils/indexeddb"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/wallet"
)

func cleanDbs(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("cleanDbs()")
		spew.Dump(args)
	}
	dbnames := js.ValueOf(args[0])
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		go func() {
			for i := 0; i < dbnames.Length(); i++ {
				if dbnames.Index(i).String() == databaseNameConfs {
					indexeddb.CleanDb(ctx, dbConfs)
				}
				if dbnames.Index(i).String() == databaseNameFiles {
					indexeddb.CleanDb(ctx, dbFiles)
				}
				if dbnames.Index(i).String() == databaseNameRates {
					indexeddb.CleanDb(ctx, dbRates)
				}
				if dbnames.Index(i).String() == databaseNameTxs {
					indexeddb.CleanDb(ctx, dbTXs)
				}
				if dbnames.Index(i).String() == domain.DatabaseName {
					cff.CleanDatabase()
				}
			}
			if dbnames.Length() > 0 {
				resetDbCache()
			}
			resolve.Invoke()
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func reloadIDB(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("reloadIDB()")
		spew.Dump(args)
	}
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		go func() {
			performReloadIdb()
			resolve.Invoke()
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func performReloadIdb() {
	initRatesDB()
	initConfsDB()
	resetDbCache()
	initFilesDB()
	initTXsDB()
}

func resetDbCache() {
	allTXs = tx.TXs{}
	statusConfs = make([]interface{}, 0)
	statusTXs = make([]interface{}, 0)
	csv.ResetDefinition()
	wallet.ResetDefinition()
}
