package main

import (
	"context"
	"testing"
	"time"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/app"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/rate"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

// https://github.com/manusa/com.marcnuri.uuid/blob/v0.0.1/go/uuid_resource_test.go#L10-L30
type testContext struct {
}

func (c *testContext) beforeEach() {
	cff = app.NewCff(context.Background())
	rate.SetCff(cff)
}

func (c *testContext) afterEach() {
	resetDbCache()
}

func testCase(test func(t *testing.T, c *testContext)) func(*testing.T) {
	return func(t *testing.T) {
		context := &testContext{}
		context.beforeEach()
		defer context.afterEach()
		test(t, context)
	}
}

func TestGet2086Results(t *testing.T) {
	t.Run("Should not include miners tx fees in case frais 214", testCase(func(t *testing.T, c *testContext) {
		// Given
		allTXs = append(allTXs, tx.TX{
			Timestamp:   time.Date(2022, 1, 1, 0, 0, 1, 0, time.UTC),
			Wallet:      "wallet",
			FiscalCateg: tx.CashIn,
			Items: map[string]tx.Values{
				"From": []tx.Value{{Code: "EUR", Amount: decimal.NewFromFloat(5000)}},
				"To":   []tx.Value{{Code: "BTC", Amount: decimal.NewFromFloat(1)}}},
		})
		allTXs = append(allTXs, tx.TX{
			Timestamp:   time.Date(2022, 11, 1, 0, 0, 1, 0, time.UTC),
			Wallet:      "wallet",
			FiscalCateg: tx.CashOut,
			Items: map[string]tx.Values{
				"From": []tx.Value{{Code: "BTC", Amount: decimal.NewFromFloat(0.5)}},
				"To":   []tx.Value{{Code: "EUR", Amount: decimal.NewFromFloat(4000)}},
				"Fee":  []tx.Value{{Code: "BTC", Amount: decimal.NewFromFloat(0.004)}}},
		})

		// When
		europeParis, _ := time.LoadLocation("Europe/Paris")
		until := time.Date(2023, 2, 1, 0, 0, 0, 0, europeParis)
		c2086Result, err := compute2086(until)
		if err != nil {
			t.Fail()
		}

		// Then
		assert.Equal(t, 1, len(c2086Result.years[2022].CashOuts))
		assert.Equal(t, int64(4000), c2086Result.years[2022].CashOuts[0].Prix213.IntPart())
		assert.Equal(t, int64(0), c2086Result.years[2022].CashOuts[0].Frais214.IntPart())
		assert.Equal(t, int64(4000), c2086Result.years[2022].CashOuts[0].PrixNetDeFrais215.IntPart())
	}))
}
