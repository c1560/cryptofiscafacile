'use strict';

const { configs } = require('@nullvoxpopuli/eslint-configs');

const config = configs.nodeCJS({ prettierIntegration: true });

module.exports = {
  ...config,
  ignorePatterns: 'dist',
  overrides: [
    ...config.overrides,
    {
      files: ['./src/worker.js'],
      env: {
        worker: true,
      },
    },
    {
      files: ['./**/*.js', './**/*.ts'],
      rules: {
        'prettier/prettier': ['error', { singleQuote: true, printWidth: 120, trailingComma: 'all' }],
      },
    },
  ],
};
