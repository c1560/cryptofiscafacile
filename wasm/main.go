package main

import (
	"context"
	"syscall/js"

	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/app"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/rate"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

var allTXs tx.TXs
var commit string
var debug bool

var c chan struct{}
var ctx context.Context
var cff app.Cff

func init() {
	c = make(chan struct{})
	ctx = context.Background()
}

func main() {
	go func() {
		cff = app.NewCff(ctx)
		rate.SetCff(cff)
		performReloadIdb()
		js.Global().Set("setDebug", js.FuncOf(setDebug))
		js.Global().Set("setFetchRates", js.FuncOf(setFetchRates))
		js.Global().Set("setupFrontendTest", js.FuncOf(setupFrontendTest))
		js.Global().Set("addEmptyWallet", js.FuncOf(addEmptyWallet))
		js.Global().Set("downloadFile", js.FuncOf(downloadFile))
		js.Global().Set("reloadIDB", js.FuncOf(reloadIDB))
		js.Global().Set("getSynthBalances", js.FuncOf(getSynthBalances))
		js.Global().Set("getSynthEvolution", js.FuncOf(getSynthEvolution))
		js.Global().Set("getSynthPortfolio", js.FuncOf(getSynthPortfolio))
		js.Global().Set("getSynthStatTotalValue", js.FuncOf(getSynthStatTotalValue))
		js.Global().Set("getSynthStatsGainsLosses", js.FuncOf(getSynthStatsGainsLosses))
		js.Global().Set("getSynthTxTypes", js.FuncOf(getSynthTxTypes))
		js.Global().Set("getUserInfos", js.FuncOf(getUserInfos))
		js.Global().Set("saveConfig", js.FuncOf(saveConfig))
		js.Global().Set("useFile", js.FuncOf(useFile))
		js.Global().Set("deleteFile", js.FuncOf(deleteFile))
		js.Global().Set("setDefinition", js.FuncOf(setDefinition))
		js.Global().Set("getDefinitions", js.FuncOf(getDefinitions))
		js.Global().Set("getHistory", js.FuncOf(getHistory))
		js.Global().Set("getWallets", js.FuncOf(getWallets))
		js.Global().Set("getTools", js.FuncOf(getTools))
		js.Global().Set("getApi", js.FuncOf(getApi))
		js.Global().Set("getApiProgress", js.FuncOf(getApiProgress))
		js.Global().Set("getCoinDetail", js.FuncOf(getCoinDetail))
		js.Global().Set("getTransactions", js.FuncOf(getTransactions))
		js.Global().Set("checkConsistencyCategVsToFrom", js.FuncOf(checkConsistencyCategVsToFrom))
		js.Global().Set("checkConsistencyNegativeBalance", js.FuncOf(checkConsistencyNegativeBalance))
		js.Global().Set("checkConsistencyAlmostTransfer", js.FuncOf(checkConsistencyAlmostTransfer))
		js.Global().Set("setValue", js.FuncOf(setValue))
		js.Global().Set("setCategory", js.FuncOf(setCategory))
		js.Global().Set("revertCateg", js.FuncOf(revertCateg))
		js.Global().Set("mergeTransactions", js.FuncOf(mergeTransactions))
		js.Global().Set("unMergeTransactions", js.FuncOf(unMergeTransactions))
		js.Global().Set("getReports", js.FuncOf(getReports))
		js.Global().Set("saveUser", js.FuncOf(saveUser))
		js.Global().Set("saveAccounts", js.FuncOf(saveAccounts))
		js.Global().Set("saveChoiceCashIn", js.FuncOf(saveChoiceCashIn))
		js.Global().Set("get2086Results", js.FuncOf(get2086Results))
		js.Global().Set("get2086Xlsx", js.FuncOf(get2086Xlsx))
		js.Global().Set("get2086XlsxUpToToday", js.FuncOf(get2086XlsxUpToToday))
		js.Global().Set("get3916XLSX", js.FuncOf(get3916XLSX))
		js.Global().Set("getStocksXLSX", js.FuncOf(getStocksXLSX))
		js.Global().Set("getWalletDefinitions", js.FuncOf(getWalletDefinitions))
		js.Global().Set("cleanDbs", js.FuncOf(cleanDbs))
		js.Global().Get("onWasmInitialized").Invoke()
	}()
	<-c
}

func setupFrontendTest(this js.Value, args []js.Value) any {
	rate.Flush()
	rate.FetchRates = args[0].Bool()
	return nil
}

func setDebug(this js.Value, args []js.Value) interface{} {
	debug = args[0].Bool()
	if debug {
		println("Go WebAssembly version [" + commit + "]")
	}
	return js.ValueOf(debug)
}

func setFetchRates(this js.Value, args []js.Value) interface{} {
	rate.FetchRates = args[0].Bool()
	return nil
}
