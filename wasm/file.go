package main

import (
	"bytes"
	enccsv "encoding/csv"
	"errors"
	"log"
	"sort"
	"strconv"
	"strings"
	"syscall/js"
	"time"

	"github.com/360EntSecGroup-Skylar/excelize"
	"github.com/davecgh/go-spew/spew"
	"github.com/hack-pad/go-indexeddb/idb"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/csv"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/json"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/rate"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/wallet"
)

const databaseNameFiles = "wasm-db-files"

var dbFiles *idb.Database
var statusConfs []interface{}
var statusTXs []interface{}

func dbFilesUpgrader(ldb *idb.Database, oldVersion, newVersion uint) error {
	names, _ := ldb.ObjectStoreNames()
	alreadyHaveConf := false
	alreadyHaveTXs := false
	alreadyHaveDefs := false
	for _, name := range names {
		if name == "confs" {
			alreadyHaveConf = true
		}
		if name == "txs" {
			alreadyHaveTXs = true
		}
		if name == "defs" {
			alreadyHaveDefs = true
		}
	}
	if !alreadyHaveConf {
		_, err := ldb.CreateObjectStore("confs", idb.ObjectStoreOptions{})
		if err != nil {
			return err
		}
	}
	if !alreadyHaveTXs {
		_, err := ldb.CreateObjectStore("txs", idb.ObjectStoreOptions{})
		if err != nil {
			return err
		}
	}
	if !alreadyHaveDefs {
		_, err := ldb.CreateObjectStore("defs", idb.ObjectStoreOptions{})
		if err != nil {
			return err
		}
	}
	return nil
}

func initFilesDB() {
	// increment ver when you want to change the dbFiles structure
	ver := uint(3)
	openRequest, _ := idb.Global().Open(ctx, databaseNameFiles, ver, dbFilesUpgrader)
	var err error
	dbFiles, err = openRequest.Await(ctx)
	if err != nil {
		statusConfs = make([]interface{}, 0)
		statusTXs = make([]interface{}, 0)
		return
	}
	// Conf Files
	statusConfs, err = loadFilesConf()
	if err != nil {
		statusTXs = make([]interface{}, 0)
		return
	}
	// TX Files
	statusTXs, _ = loadFilesTX()
}

func loadFilesConf() ([]interface{}, error) {
	files, err := fetchFilesConf()
	status := make([]interface{}, len(files))
	if err != nil {
		return status, err
	}

	i := 0
	for filename, fileData := range files {
		_, status[i] = digestFile(filename, fileData)
		i++
	}
	return status, nil
}

func fetchFilesConf() (map[string][]byte, error) {
	files := make(map[string][]byte)
	if dbFiles == nil {
		return files, errors.New(databaseNameFiles + " not Initialized")
	}
	txnConfs, err := dbFiles.Transaction(idb.TransactionReadWrite, "confs")
	if err != nil {
		return files, err
	}
	storeConfs, err := txnConfs.ObjectStore("confs")
	if err != nil {
		return files, err
	}
	countRequestConfs, err := storeConfs.Count()
	if err != nil {
		return files, err
	}
	filesCountConfs, err := countRequestConfs.Await(ctx)
	if err != nil || filesCountConfs == 0 {
		return files, err
	}
	cursorRequestConfs, err := storeConfs.OpenCursor(idb.CursorNext)
	if err != nil {
		return files, err
	}
	cursorRequestConfs.Iter(ctx, func(cursor *idb.CursorWithValue) error {
		fileName, err := cursor.Key()
		if err != nil {
			return err
		}
		array, err := cursor.Value()
		if err != nil {
			return err
		}
		fileData := make([]byte, array.Get("byteLength").Int())
		js.CopyBytesToGo(fileData, array)
		files[fileName.String()] = fileData
		return nil
	})
	txnConfs.Commit()
	return files, nil
}

func loadFilesTX() ([]interface{}, error) {
	status := make([]interface{}, 0)
	if dbFiles == nil {
		return status, errors.New(databaseNameFiles + " not Initialized")
	}
	txnTXs, err := dbFiles.Transaction(idb.TransactionReadOnly, "txs")
	if err != nil {
		return status, err
	}
	storeTXs, err := txnTXs.ObjectStore("txs")
	if err != nil {
		return status, err
	}
	countRequestTXs, err := storeTXs.Count()
	if err != nil {
		return status, err
	}
	// Clear allTXs to restart fresh
	allTXs = allTXs[:0]
	filesCountTXs, err := countRequestTXs.Await(ctx)
	if err != nil || filesCountTXs == 0 {
		return status, err
	}
	cursorRequestTXs, err := storeTXs.OpenCursor(idb.CursorNext)
	if err != nil {
		return status, err
	}
	status = make([]interface{}, int(filesCountTXs))
	j := 0
	cursorRequestTXs.Iter(ctx, func(cursor *idb.CursorWithValue) error {
		fileName, err := cursor.Key()
		if err != nil {
			return err
		}
		array, err := cursor.Value()
		if err != nil {
			return err
		}
		fileData := make([]byte, array.Get("byteLength").Int())
		js.CopyBytesToGo(fileData, array)
		_, status[j] = digestFile(fileName.String(), fileData)
		j++
		return nil
	})
	return status, nil
}

func downloadFile(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("downloadFile():")
		spew.Dump(args)
	}
	filename := args[0]
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			data, err := getTXFileData(filename.String())
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			arrayConstructor := js.Global().Get("Uint8Array")
			dataJS := arrayConstructor.New(len(data))
			js.CopyBytesToJS(dataJS, data)
			resolve.Invoke(dataJS)
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func getTXFileData(key string) ([]byte, error) {
	if dbFiles == nil {
		return nil, errors.New(databaseNameFiles + " not Initialized")
	}
	txnTXs, err := dbFiles.Transaction(idb.TransactionReadOnly, "txs")
	if err == nil {
		storeTXs, err := txnTXs.ObjectStore("txs")
		if err == nil {
			req, err := storeTXs.Get(js.ValueOf(key))
			if err == nil {
				array, err := req.Await(ctx)
				if err == nil {
					if !array.IsUndefined() {
						fileData := make([]byte, array.Get("byteLength").Int())
						js.CopyBytesToGo(fileData, array)
						return fileData, nil
					}
				}
			}
		}
	}
	return nil, errors.New("error getting txs file")
}

func getWalletDefinitions(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("getWalletDefinitions():")
		spew.Dump(args)
	}
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		// reject := args[1]
		go func() {
			wallets := wallet.GetAll()
			jsWallets := make([]interface{}, len(wallets))
			for i := 0; i < len(wallets); i++ {
				wallet := wallets[i]
				jsWallet := make(map[string]interface{})
				jsWallet["name"] = wallet.Name
				jsWallet["crypto"] = wallet.Crypto
				jsWallet["custodial"] = wallet.Custodial
				jsWallet["logo"] = wallet.Logo
				jsWallet["legal"] = wallet.LegalName
				jsWallet["address"] = wallet.Address
				jsWallet["url"] = wallet.URL
				jsWallets[i] = jsWallet
			}
			resolve.Invoke(jsWallets)
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func useFile(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("useFile():")
		spew.Dump(args)
	}
	name := args[0]
	array := args[1]
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			fileData := make([]byte, array.Get("byteLength").Int())
			js.CopyBytesToGo(fileData, array)
			if err := digestAndSaveFile(name.String(), fileData); err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			resolve.Invoke(statusTXs)
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func addEmptyWallet(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("useFile():")
		spew.Dump(args)
	}
	name := args[0]
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		// reject := args[1]
		go func() {
			// Find wallet in json
			jsonFile := generateApiEmptyFile(name.String())
			if jsonFile != nil {
				digestAndSaveFile((*jsonFile).Name, (*jsonFile).Data)
			} else {
				csvData := csv.GenerateEmptyCsv(name.String())
				if csvData != nil {
					filename := "empty-" + strings.ReplaceAll(strings.ToLower(name.String()), " ", "-") + ".csv"
					digestAndSaveFile(filename, csvData)
				}
			}
			// Find wallet in csv defs
			resolve.Invoke()
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func deleteFile(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("deleteFile():")
		spew.Dump(args)
	}
	name := args[0]
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			if dbFiles == nil {
				reject.Invoke(js.Global().Get("Error").New(databaseNameFiles + " not Initialized"))
				return
			}
			txn, err := dbFiles.Transaction(idb.TransactionReadWrite, "txs")
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			store, err := txn.ObjectStore("txs")
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			req, err := store.Delete(name)
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			err = req.Await(ctx)
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			statusTXs, _ = loadFilesTX()
			resolve.Invoke(statusTXs)
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func setDefinition(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("setDefinition():")
		spew.Dump(args)
	}
	name := args[0]
	version := args[1]
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			if dbFiles == nil {
				reject.Invoke(js.Global().Get("Error").New(databaseNameFiles + " not Initialized"))
				return
			}
			txn, err := dbFiles.Transaction(idb.TransactionReadWrite, "defs")
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			store, err := txn.ObjectStore("defs")
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			req, err := store.PutKey(name, version)
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			_, err = req.Await(ctx)
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			resolve.Invoke()
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func getDefinitions(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("getDefinitions():")
		spew.Dump(args)
	}
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			if dbFiles == nil {
				reject.Invoke(js.Global().Get("Error").New(databaseNameFiles + " not Initialized"))
				return
			}
			txn, err := dbFiles.Transaction(idb.TransactionReadOnly, "defs")
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			store, err := txn.ObjectStore("defs")
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			countRequest, err := store.Count()
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			defsCount, err := countRequest.Await(ctx)
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			if defsCount == 0 {
				resolve.Invoke(make([]interface{}, 0))
				return
			}
			cursorRequest, err := store.OpenCursor(idb.CursorNext)
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			definitions := make([]interface{}, int(defsCount))
			j := 0
			cursorRequest.Iter(ctx, func(cursor *idb.CursorWithValue) error {
				fileName, err := cursor.Key()
				if err != nil {
					return err
				}
				fileVersion, err := cursor.Value()
				if err != nil {
					return err
				}
				definition := make(map[string]interface{})
				definition["name"] = fileName.String()
				definition["version"] = fileVersion.Int()
				definitions[j] = definition
				j++
				return nil
			})
			resolve.Invoke(definitions)
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func getHistory(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("getHistory():")
		spew.Dump(args)
		if args[0].Bool() {
			spew.Dump(statusConfs)
		} else {
			spew.Dump(statusTXs)
		}
	}
	if args[0].Bool() {
		return js.ValueOf(statusConfs)
	}
	return js.ValueOf(statusTXs)
}

func digestFile(filename string, fileData []byte) (string, map[string]interface{}) {
	if debug {
		log.Println("digest file", filename)
	}

	stat := make(map[string]interface{})
	stat["name"] = filename
	filenameExt := parseFilenameExtension(filename)
	if len(fileData) == 0 {
		stat["error"] = "Fichier vide (aucune donnée)"
		return "error", stat
	}
	// file is a JSON
	if fileData[0] == '{' ||
		fileData[0] == '[' {
		return parseJson(stat, fileData)
	}
	// file is CFF rate
	if fileData[0] == '$' {
		coin, err := rate.ParseCFFR(fileData)
		if err == nil {
			stat["ext"] = "CFFR"
			stat["kind"] = "Taux de conversion Crypto en EUR"
			stat["result"] = "Taux ajoutés : " + coin + "/EUR"
			return "confs", stat
		}
	}

	var customWalletName string
	if filenameExt == "XLS" || filenameExt == "XLSX" {
		return parseExcel(stat, filename, fileData, customWalletName)
	}

	if filenameExt == "CSV" {
		return parseCsv(stat, filename, fileData, customWalletName)
	}

	stat["error"] = "Format de fichier complètement inconnu"
	return "error", stat
}

func parseFilenameExtension(filename string) string {
	parts := strings.Split(filename, ".")
	return strings.ToUpper(parts[len(parts)-1])
}

func parseExcel(stat map[string]interface{}, filename string, fileData []byte, customWalletName string) (string, map[string]interface{}) {
	buf := bytes.NewBuffer(fileData)
	xlsx, err := excelize.OpenReader(buf) //, opt ...Options)
	if err == nil {
		stat["ext"] = "XLSX"
		if xlsx.GetCellValue("Transactions", "A1") == "User ID" &&
			xlsx.GetCellValue("Transactions", "A2") == "Start Date" &&
			xlsx.GetCellValue("Transactions", "A3") == "End Date" {
			// SwissBorg XLSX v1
			// Save User ID for 3916
			cellUserID := xlsx.GetCellValue("Transactions", "B1")
			if cellUserID != "" {
				if _, ok := confAccounts["SwissBorg"]; !ok {
					confAccounts["SwissBorg"] = append(confAccounts["SwissBorg"], configAccount{
						id: cellUserID,
						// opened:   opened,
						// closed:   closed,
						// isClosed: false,
					})
					saveWalletAccountsDB("SwissBorg")
				}
			}
			records := xlsx.GetRows("Transactions")
			var csvData []byte
			csvBuf := bytes.NewBuffer(csvData)
			csvWriter := enccsv.NewWriter(csvBuf)
			csvWriter.WriteAll(records[8:])
			fileData = csvBuf.Bytes()
		} else if xlsx.GetCellValue("Transactions", "C6") == "User ID" &&
			xlsx.GetCellValue("Transactions", "C7") == "Start Date" &&
			xlsx.GetCellValue("Transactions", "C8") == "End Date" {
			// SwissBorg XLSX v2
			// Save User ID for 3916
			cellUserID := xlsx.GetCellValue("Transactions", "D6")
			if cellUserID != "" {
				if _, ok := confAccounts["SwissBorg"]; !ok {
					confAccounts["SwissBorg"] = append(confAccounts["SwissBorg"], configAccount{
						id: cellUserID,
						// opened:   opened,
						// closed:   closed,
						// isClosed: false,
					})
					saveWalletAccountsDB("SwissBorg")
				}
			}
			records := xlsx.GetRows("Transactions")
			var csvData []byte
			csvBuf := bytes.NewBuffer(csvData)
			csvWriter := enccsv.NewWriter(csvBuf)
			csvWriter.WriteAll(records[13:])
			fileData = csvBuf.Bytes()
		} else if xlsx.GetCellValue("CFF", "A1") == "Format Custom CFF, ne pas supprimer cette ligne" {
			// CFF Custom XLSX
			customWalletName = xlsx.GetCellValue("CFF", "B2")
			if customWalletName != "" {
				customWallet := wallet.Wallet{
					Name:      customWalletName,
					Crypto:    xlsx.GetCellValue("CFF", "B6") == "oui",
					Custodial: xlsx.GetCellValue("CFF", "B7") == "oui" && xlsx.GetCellValue("CFF", "B5") == "oui",
					Logo:      "nofingerprint/wallets/cff-64x64.png",
					LegalName: customWalletName,
					Address:   xlsx.GetCellValue("CFF", "B3"),
					URL:       xlsx.GetCellValue("CFF", "B4"),
				}
				wallet.Add(customWallet)
			}
			records := xlsx.GetRows("CFF")
			var csvData []byte
			csvBuf := bytes.NewBuffer(csvData)
			csvWriter := enccsv.NewWriter(csvBuf)
			csvWriter.WriteAll(records[8:])
			fileData = csvBuf.Bytes()
		} else if len(xlsx.GetSheetMap()) == 1 {
			// Try to parse xlsx with one sheet
			sheets := xlsx.GetSheetMap()
			for _, sheet := range sheets {
				records := xlsx.GetRows(sheet)
				var csvData []byte
				csvBuf := bytes.NewBuffer(csvData)
				csvWriter := enccsv.NewWriter(csvBuf)
				csvWriter.WriteAll(records)
				fileData = csvBuf.Bytes()
			}
		} else {
			stat["error"] = "Format de fichier XLSX inconnu"
			return "error", stat
		}

		return parseCsv(stat, filename, fileData, customWalletName)
	}

	stat["error"] = "Format de fichier XLSX inconnu"
	return "error", stat
}

func parseJson(stat map[string]interface{}, fileData []byte) (string, map[string]interface{}) {
	stat["ext"] = "JSON"
	txs, source, walletname, unknowns, addresses, err := json.Digest(fileData)
	if err == nil {
		stat["kind"] = source
		stat["wallet"] = walletname
		result := strconv.Itoa(len(txs)) + " transactions trouvées ("
		before := len(allTXs)
		allTXs.AddUniq(txs)
		stat["result"] = result + strconv.Itoa(len(allTXs)-before) + " ajoutés)"
		unk := make([]interface{}, len(unknowns))
		for i, u := range unknowns {
			unk[i] = u
		}
		stat["unknowns"] = unk
		bcAddresses := make([]interface{}, len(addresses))
		sort.Strings(addresses)
		for i, addr := range addresses {
			bcAddresses[i] = addr
		}
		stat["bcAddresses"] = bcAddresses
		return "txs", stat
	}
	name, err := wallet.AddDefinition(fileData)
	if err == nil {
		stat["kind"] = "Définition de wallet"
		stat["result"] = "Définition ajoutée : " + name
		return "confs", stat
	}
	name, err = csv.AddDefinition(fileData)
	if err == nil {
		stat["kind"] = "Définition de fichier CSV"
		stat["result"] = "Définition ajoutée : " + name
		return "confs", stat
	}
	stat["error"] = "Format de fichier JSON inconnu"
	return "error", stat
}

func parseCsv(stat map[string]interface{}, filename string, fileData []byte, customWalletName string) (string, map[string]interface{}) {
	stat["ext"] = parseFilenameExtension(stat["name"].(string))
	// specify a csv header if necessary
	var header []string
	// Remove unwanted Header if any
	start := 0
	// detect CSV Custom
	if bytes.Contains(fileData, []byte("Format Custom CFF, ne pas supprimer cette ligne.")) {
		start = bytes.Index(fileData, []byte("Date,Type de Transaction"))
		// Save User data for 3916
		customCrypto := ""
		customStranger := ""
		customKey := ""
		customAddress := ""
		customUrl := ""
		// wallet:
		walletStart := bytes.Index(fileData, []byte("Portefeuille"))
		if walletStart >= 0 {
			walletEnd := bytes.Index(fileData[walletStart:], []byte{0x0A})
			if walletEnd >= 0 {
				rec := strings.Split(string(fileData[walletStart:walletStart+walletEnd]), "-")
				customWalletName = rec[1]
			}
		}
		// Crypto:
		cryptoStart := bytes.Index(fileData, []byte("Crypto"))
		if cryptoStart >= 0 {
			cryptoEnd := bytes.Index(fileData[cryptoStart:], []byte{0x0A})
			if cryptoEnd >= 0 {
				rec := strings.Split(string(fileData[cryptoStart:cryptoStart+cryptoEnd]), "-")
				customCrypto = rec[1]
			}
		}
		// Custodial:
		strangerStart := bytes.Index(fileData, []byte("Compte à l'étranger"))
		if strangerStart >= 0 {
			strangerEnd := bytes.Index(fileData[strangerStart:], []byte{0x0A})
			if strangerEnd >= 0 {
				rec := strings.Split(string(fileData[strangerStart:strangerStart+strangerEnd]), "-")
				customStranger = rec[1]
			}
		}
		keyStart := bytes.Index(fileData, []byte("Détient voc clés privées"))
		if keyStart >= 0 {
			keyEnd := bytes.Index(fileData[keyStart:], []byte{0x0A})
			if keyEnd >= 0 {
				rec := strings.Split(string(fileData[keyStart:keyStart+keyEnd]), "-")
				customKey = rec[1]
			}
		}
		// Address:
		addrStart := bytes.Index(fileData, []byte("Adresse"))
		if addrStart >= 0 {
			addrEnd := bytes.Index(fileData[addrStart:], []byte{0x0A})
			if addrEnd >= 0 {
				rec := strings.Split(string(fileData[addrStart:addrStart+addrEnd]), "-")
				customAddress = rec[1]
			}
		}
		// URL:
		urlStart := bytes.Index(fileData, []byte("Website(URL)"))
		if urlStart >= 0 {
			urlEnd := bytes.Index(fileData[urlStart:], []byte{0x0A})
			if urlEnd >= 0 {
				rec := strings.Split(string(fileData[urlStart:urlStart+urlEnd]), "-")
				customUrl = rec[1]
			}
		}
		if customWalletName != "" && customCrypto != "" && customStranger != "" && customKey != "" {
			customWalletCsv := wallet.Wallet{
				Name:      customWalletName,
				Crypto:    customCrypto == "oui",
				Custodial: customStranger == "oui" && customKey == "oui",
				Logo:      "nofingerprint/wallets/cff-64x64.png",
				LegalName: customWalletName,
				Address:   customAddress,
				URL:       customUrl,
			}
			wallet.Add(customWalletCsv)
		}
	}
	// detect Coinbase CSV
	if bytes.Contains(fileData, []byte("You can use this transaction report to inform your likely tax obligations.")) {
		start = bytes.Index(fileData, []byte("Timestamp,Transaction"))
		// Save User ID for 3916
		user := bytes.Index(fileData, []byte("User,"))
		if user >= 0 {
			userEnd := bytes.Index(fileData[user:], []byte{0x0A})
			if userEnd >= 0 {
				users := strings.Split(string(fileData[user:user+userEnd]), ",")
				if _, ok := confAccounts["Coinbase"]; !ok {
					confAccounts["Coinbase"] = append(confAccounts["Coinbase"], configAccount{
						id: users[2],
						// opened:   opened,
						// closed:   closed,
						// isClosed: false,
					})
					saveWalletAccountsDB("Coinbase")
				}
			}
		}
	}
	// detect Bitpanda and BitpandaPro CSV
	if bytes.Contains(fileData, []byte("Disclaimer: All data is without guarantee, errors and changes are reserved.")) {
		// here is Bitpanda Pro
		if bytes.Contains(fileData, []byte("Bitpanda Pro")) {
			if bytes.Contains(fileData, []byte("trades history")) {
				start = bytes.Index(fileData, []byte("\"Order ID\""))
			} else if bytes.Contains(fileData, []byte("transactions history")) {
				start = bytes.Index(fileData, []byte("\"Transaction ID\""))
				// add the missing headers
				if !bytes.Contains(fileData, []byte(`,"Account ID","Account Name"`)) {
					fileData = bytes.ReplaceAll(fileData, []byte(`"Time Created"`), []byte(`"Time Created","Account ID","Account Name"`))
				}
			}
			if start > 0 {
				// Save Account ID and Opened Date for 3916
				accountId := bytes.Index(fileData, []byte("Main Account ID:"))
				if accountId >= 0 {
					accountIdEnd := bytes.Index(fileData[accountId:], []byte{0x0A}) - 1
					if accountIdEnd >= 0 {
						accounts := strings.Split(string(fileData[accountId:accountId+accountIdEnd]), ":")
						if _, ok := confAccounts["Bitpanda Pro"]; !ok {
							confAccounts["Bitpanda Pro"] = append(confAccounts["Bitpanda Pro"], configAccount{
								id: strings.TrimSpace(accounts[1]),
								// opened:   opened,
								// closed:   closed,
								// isClosed: false,
							})
							saveWalletAccountsDB("Bitpanda Pro")
						}
					}
				}
			}
		} else {
			// otherwise it is Bitpanda
			start = bytes.Index(fileData, []byte("\"Transaction ID\",Timestamp"))
			// Save Account ID and Opened Date for 3916
			emailStart := bytes.Index(fileData[50:], []byte(", ")) + 15 // line end is 0x0d0a
			if emailStart >= 0 {
				opened := bytes.Index(fileData, []byte("\"Account opened at:"))
				if opened >= 0 {
					acc := configAccount{
						id: string(fileData[50+emailStart : opened-2]),
					}
					openedEnd := bytes.Index(fileData[opened:], []byte{0x0D, 0x0A}) - 1
					if openedEnd >= 0 {
						open, err := time.Parse("1/2/06, 3:04 PM", string(fileData[opened+20:opened+openedEnd]))
						if err == nil {
							acc.opened = open
						}
					}
					if _, ok := confAccounts["Bitpanda"]; !ok {
						confAccounts["Bitpanda"] = append(confAccounts["Bitpanda"], acc)
						saveWalletAccountsDB("Bitpanda")
					}
				}
			}
		}
	}
	// detect CDC Exchange CSVs DEPOSIT, SUPERCHARGER, WITHDRAWAL and REFERRAL_BONUS
	if bytes.Contains(fileData, []byte("You understand and agree that the content of this document reflects order")) {
		// if CSV WITHDRAWAL: (la particularité est qu'ils répètent l'entête 2 fois après les libellés de colonnes !)
		start = bytes.Index(fileData, []byte("Time (UTC),Coin,Withdrawal Amount,Fee,Withdrawal Address"))
		if start <= 0 {
			start = bytes.Index(fileData, []byte("Time (UTC)"))
		} else {
			start = 985
			header = []string{"Time (UTC)", "Coin", "Withdrawal Amount", "Fee", "Withdrawal Address", "Status", "Txid"}
		}
	}

	// extraction
	txs, source, wallet, unknowns, err := csv.ParseTXs(filename, fileData[start:], customWalletName, header)
	if err == nil {
		stat["kind"] = source
		stat["wallet"] = wallet
		result := strconv.Itoa(len(txs)) + " transactions trouvées ("
		before := len(allTXs)
		allTXs.AddUniq(txs)
		stat["result"] = result + strconv.Itoa(len(allTXs)-before) + " ajoutés)"
		unk := make([]interface{}, len(unknowns))
		for i, u := range unknowns {
			unk[i] = u
		}
		stat["unknowns"] = unk
		return "txs", stat
	} else {
		if debug {
			log.Println(err)
		}
	}

	stat["error"] = "Format de fichier " + stat["ext"].(string) + " inconnu"
	return "error", stat
}

func saveFile(fileName, fileData js.Value, fileType string) error {
	if dbFiles == nil {
		return errors.New(databaseNameFiles + " not Initialized")
	}
	txn, err := dbFiles.Transaction(idb.TransactionReadWrite, fileType)
	if err != nil {
		return err
	}
	store, err := txn.ObjectStore(fileType)
	if err != nil {
		return err
	}
	store.PutKey(fileName, fileData)
	return nil
}

func digestAndSaveFile(fileName string, fileData []byte) error {
	typ, sta := digestFile(fileName, fileData)
	if debug {
		spew.Dump(sta)
	}
	if typ == "txs" {
		found := false
		for i, s := range statusTXs {
			if s.(map[string]interface{})["name"].(string) == fileName {
				found = true
				statusTXs[i] = sta
				break
			}
		}
		if !found {
			statusTXs = append(statusTXs, sta)
		}
	} else if typ == "confs" {
		statusConfs = append(statusConfs, sta)
		statusTXs, _ = loadFilesTX()
	} else { // if typ == "error" {
		statusTXs = append(statusTXs, sta)
		return nil
	}
	loadTXsDB()
	// Save file in DB
	arrayConstructor := js.Global().Get("Uint8Array")
	dataJS := arrayConstructor.New(len(fileData))
	js.CopyBytesToJS(dataJS, fileData)
	return saveFile(js.ValueOf(fileName), dataJS, typ)
}
