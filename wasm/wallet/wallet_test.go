package wallet

import (
	"testing"
)

func TestAddDefinition(t *testing.T) {
	type args struct {
		jsonData []byte
	}
	tests := []struct {
		name     string
		args     args
		wantName string
		wantErr  bool
	}{
		{
			name: "Simple Def",
			args: args{
				jsonData: []byte("{\"name\":\"Wal1\",\"logo\":\"logo1\",\"crypto\":true}"),
			},
			wantName: "Wal1",
			wantErr:  false,
		},
		{
			name: "Void Def",
			args: args{
				jsonData: []byte("{}"),
			},
			wantErr: true,
		},
		{
			name: "Error Parsing",
			args: args{
				jsonData: []byte("{"),
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotName, err := AddDefinition(tt.args.jsonData)
			if (err != nil) != tt.wantErr {
				t.Errorf("AddDefinition() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotName != tt.wantName {
				t.Errorf("AddDefinition() = %v, want %v", gotName, tt.wantName)
			}
		})
	}
}
