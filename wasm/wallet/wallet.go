package wallet

import (
	"encoding/json"
	"errors"
	"sort"
)

// Wallet : a crypto Wallet or an Exchange Plateform
type Wallet struct {
	Name      string `json:"name"`
	Crypto    bool   `json:"crypto"`
	Custodial bool   `json:"custodial"`
	Logo      string `json:"logo"`
	LegalName string `json:"legal"`
	Address   string `json:"address"`
	URL       string `json:"url"`
}

var wallets = make(map[string]*Wallet)

// ResetDefinition : Forget all Wallet definitions
func ResetDefinition() {
	wallets = make(map[string]*Wallet)
}

func Add(w Wallet) {
	if _, exist := wallets[w.Name]; !exist {
		wallets[w.Name] = &w
	}
}

// AddDefinition : Add a Wallet definition
func AddDefinition(jsonData []byte) (name string, err error) {
	var wallet Wallet
	err = json.Unmarshal(jsonData, &wallet)
	if wallet.Name == "" || wallet.Logo == "" { // Mandatory fields
		err = errors.New("JSON Format does not match")
	}
	if err == nil {
		wallets[wallet.Name] = &wallet
	}
	return wallet.Name, err
}

// Get the wallet
func Get(name string) (*Wallet, bool) {
	w, exist := wallets[name]
	return w, exist
}

func GetAll() []Wallet {
	walletsSlice := make([]Wallet, len(wallets))
	i := 0
	for name := range wallets {
		walletsSlice[i] = *wallets[name]
		i++
	}
	sort.Slice(walletsSlice, func(i, j int) bool {
		return walletsSlice[i].Name < walletsSlice[j].Name
	})
	return walletsSlice
}
