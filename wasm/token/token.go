package token

import (
	"strconv"
	"log"

	// resty "github.com/go-resty/resty/v2"
)

type Param struct {
	Name  		string	`json:"name"`
	UnitName 	string	`json:"unit-name"`
}
type Assets struct {
	Id  		int		`json:"index"`
	Params  Param `json:"params"`
}
type AlgoResult struct {
	CurrentRound  	int		`json:"current-round"`
	AssetList				Assets `json:"asset"`
}

// Get : Get a symbol
func Get(coinId int) string {
	if coinId == 0 {
		log.Println("Error: coinId is " + strconv.Itoa(coinId))
		return "Unk."
	}
	name := GetToken(coinId)
	if name == "" {
		log.Println("Error Requesting coin name for: " + strconv.Itoa(coinId))
		return "Unk."
		// TODO try to add a request to repatriate the name from algoexplorer (not working)
		// n, err := getCoinNameById(coinId)
		// if err != nil {
		// 	log.Println(err)
		// 	return "Unk."
		// }
		// name = n
	} 
	return name
}

// type AlgoAPI struct {
// 	debug       bool
// 	indent      string
// 	client      *resty.Client
// 	address     string
// 	baseURL     string
// 	json        AlgoResult
// }
// var Algo = AlgoAPI{
// 	baseURL: "https://algoindexer.algoexplorerapi.io/v2/assets/",
// }
// func (alg AlgoAPI) getCoinNameById(assetId int) (name string, err error) {
// 	headers := map[string]string{
// 		"Accept": "application/json",
// 	}
// 	var res AlgoResult
// 	resp, err := alg.client.R().
// 		SetHeaders(headers).
// 		SetResult(&res).
// 		Get(alg.baseURL + strconv.Itoa(assetId))
// 	if err != nil {
// 		return
// 	}
// 	if !resp.IsSuccess() {
// 		err = errors.New("Error Requesting coin name for " + strconv.Itoa(assetId) + " " + strconv.Itoa(resp.StatusCode()))
// 		return
// 	}
// 	name = res.AssetList.Params.UnitName
// 	return
// }