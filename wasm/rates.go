package main

import (
	"errors"
	"log"
	"syscall/js"

	"github.com/davecgh/go-spew/spew"
	"github.com/hack-pad/go-indexeddb/idb"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/external"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/rate"
)

const databaseNameRates = "wasm-db-rates"

var dbRatesFirstUse bool
var dbRates *idb.Database

func dbRatesUpgrader(ldb *idb.Database, oldVersion, newVersion uint) error {
	names, _ := ldb.ObjectStoreNames()
	dbRatesFirstUse = true
	for _, name := range names {
		if name == "cffr" {
			dbRatesFirstUse = false
		}
	}
	if dbRatesFirstUse {
		_, err := ldb.CreateObjectStore("cffr", idb.ObjectStoreOptions{})
		if err != nil {
			return err
		}
	}
	return nil
}

func initRatesDB() {
	// increment ver when you want to change the dbRates structure
	ver := uint(1)
	openRequest, _ := idb.Global().Open(ctx, "wasm-db-rates", ver, dbRatesUpgrader)
	var err error
	dbRates, err = openRequest.Await(ctx)
	if err != nil {
		return
	}
	// CFFR Format
	go func() {
		cffrToSave := make(chan string, 50)
		rate.RegisterSaveChannel(cffrToSave)
		for {
			coin := <-cffrToSave
			cffr := rate.ExportCFFR(coin)
			arrayConstructor := js.Global().Get("Uint8Array")
			dataJS := arrayConstructor.New(len(cffr))
			js.CopyBytesToJS(dataJS, cffr)
			txnCFFR, err := dbRates.Transaction(idb.TransactionReadWrite, "cffr")
			if err != nil {
				continue
			}
			storeCFFR, err := txnCFFR.ObjectStore("cffr")
			if err != nil {
				continue
			}
			storeCFFR.PutKey(js.ValueOf(coin), dataJS)
		}
	}()
	txnCFFR, err := dbRates.Transaction(idb.TransactionReadOnly, "cffr")
	if err != nil {
		return
	}
	storeCFFR, err := txnCFFR.ObjectStore("cffr")
	if err != nil {
		return
	}
	countRequestCFFR, err := storeCFFR.Count()
	if err != nil {
		return
	}
	ratesCountCFFR, err := countRequestCFFR.Await(ctx)
	if err != nil || ratesCountCFFR == 0 {
		return
	}
	cursorRequestCFFR, err := storeCFFR.OpenCursor(idb.CursorNext)
	if err != nil {
		return
	}
	cursorRequestCFFR.Iter(ctx, func(cursor *idb.CursorWithValue) error {
		cffr, err := cursor.Value()
		if err != nil {
			return err
		}
		cffrData := make([]byte, cffr.Get("byteLength").Int())
		js.CopyBytesToGo(cffrData, cffr)
		rate.ParseCFFR(cffrData)
		return nil
	})
}

func getCoinDetail(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("getCoinDetail()")
		spew.Dump(args)
	}
	symbol := args[0]
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			coinDetail, err := rate.GetCoinDetail(symbol.String())

			var restErr *external.RestClientError
			if err != nil &&
				!(errors.As(err, &restErr) && restErr.StatusCode == 404) {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}

			// Json response
			json := make(map[string]interface{}, 1)
			if coinDetail != nil {
				json["id"] = coinDetail.ID
				json["name"] = coinDetail.Name
				json["symbol"] = coinDetail.Symbol
				json["image"] = coinDetail.Image.Thumb

				marketDataJson := make(map[string]interface{}, 1)
				marketDataJson["currentPriceBtc"] = coinDetail.MarketData.CurrentPrice["btc"]
				marketDataJson["currentPriceEur"] = coinDetail.MarketData.CurrentPrice["eur"]
				marketDataJson["currentPriceUsd"] = coinDetail.MarketData.CurrentPrice["usd"]
				marketDataJson["currentPriceChange24hEur"] = coinDetail.MarketData.PriceChangePercentage24hInCurrency["eur"]
				marketDataJson["circulatingSupply"] = coinDetail.MarketData.CirculatingSupply
				marketDataJson["marketCapEur"] = coinDetail.MarketData.MarketCap["eur"]
				marketDataJson["maxSupply"] = coinDetail.MarketData.MaxSupply
				marketDataJson["totalSupply"] = *coinDetail.MarketData.TotalSupply
				json["marketData"] = marketDataJson
			}

			resolve.Invoke(json)
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}
