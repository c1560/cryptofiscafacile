module gitlab.com/c1560/CryptoFiscaFacile/wasm/tx

go 1.19

replace (
	gitlab.com/c1560/CryptoFiscaFacile/wasm/rate => ../rate
	gitlab.com/c1560/CryptoFiscaFacile/wasm/wallet => ../wallet
)

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/google/go-cmp v0.6.0
	github.com/shopspring/decimal v1.4.0
	gitlab.com/c1560/CryptoFiscaFacile/wasm/rate v0.0.0-00010101000000-000000000000
)

require (
	github.com/go-resty/resty/v2 v2.7.0 // indirect
	golang.org/x/net v0.0.0-20211029224645-99673261e6eb // indirect
)
