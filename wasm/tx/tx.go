package tx

import (
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"log"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/utils/slices"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/rate"
)

// FiscalCateg : Fiscal Category
type FiscalCateg string

const (
	// Deposits : TX is a Deposit
	Deposits FiscalCateg = "Deposits"
	// Withdrawals : TX is a Withdrawal
	Withdrawals FiscalCateg = "Withdrawals"
	// Fees : TX is a Fee
	Fees FiscalCateg = "Fees"
	// Minings : TX is a Mining
	Minings FiscalCateg = "Minings"
	// CashOut : TX is a CashOut
	CashOut FiscalCateg = "CashOut"
	// CashIn : TX is a CashIn
	CashIn FiscalCateg = "CashIn"
	// Exchanges : TX is an Exchange
	Exchanges FiscalCateg = "Exchanges"
	// Transfers : TX is an Transfer
	Transfers FiscalCateg = "Transfers"
	// AirDrops : TX is an AirDrop
	AirDrops FiscalCateg = "AirDrops"
	// Interests : TX is an Interest
	Interests FiscalCateg = "Interests"
	// CommercialRebates : TX is a Commercial Rebate
	CommercialRebates FiscalCateg = "CommercialRebates"
	// Referrals : TX is a Referral
	Referrals FiscalCateg = "Referrals"
	// Forks : TX is a Fork credit
	Forks FiscalCateg = "Forks"
	// Gifts : TX is a Gift
	Gifts FiscalCateg = "Gifts"
	// ICO : TX is a ICO
	ICO FiscalCateg = "ICO"
	// DontCare : TX is useless
	DontCare FiscalCateg = "DontCare"
	// Void : Not categorized TX
	Void FiscalCateg = ""
)

// NFT : Non Fungible Token
type NFT struct {
	ID     string
	Name   string
	Symbol string
}

// NFTs : collection of NFT
type NFTs []NFT

// Value : an Amount of Currency
type Value struct {
	Code   string
	Amount decimal.Decimal
}

// IsFiat : Check if this Value Currency is Fiat
func (v Value) IsFiat() bool {
	return rate.IsFiat(v.Code)
}

// Values : collection of Value
type Values []Value

// TX : one transaction
type TX struct {
	Timestamp     time.Time
	ID            string
	Wallet        string
	LinkedTxsIds  []string // Merge tx id of other transaction
	Method        string
	Source        string
	FiscalCateg   FiscalCateg
	ModifiedCateg FiscalCateg
	Items         map[string]Values
	NFTs          map[string]NFTs
	Note          string
}

// GenerateID : Generate a new ID based on current TX info
func (tx *TX) GenerateID() {
	hash := sha256.Sum256([]byte(tx.Source + tx.Timestamp.String() + string(tx.FiscalCateg)))
	tx.ID = hex.EncodeToString(hash[:])
}

// SimilarDate : Check if 2 TXs have similar date
func (tx TX) SimilarDate(delta time.Duration, t time.Time) bool {
	if tx.Timestamp.Sub(t) < delta &&
		t.Sub(tx.Timestamp) < delta {
		return true
	}
	return false
}

// deltaDate : Return absolute difference between a TX timestamp and a date
func (tx TX) deltaDate(t time.Time) time.Duration {
	if tx.Timestamp.Before(t) {
		return t.Sub(tx.Timestamp)
	}
	return tx.Timestamp.Sub(t)
}

// EffectiveCateg : get effective Fiscal Categ
func (tx TX) EffectiveCateg() FiscalCateg {
	if tx.ModifiedCateg == Void {
		return tx.FiscalCateg
	}
	return tx.ModifiedCateg
}

// Balances : list of Amount per Currency
type Balances map[string]decimal.Decimal

// Values of Balances
func (b Balances) Values() (vals Values) {
	for k, v := range b {
		if !v.IsZero() {
			vals = append(vals, Value{
				Code:   k,
				Amount: v,
			})
		}
	}
	return
}

// GetBalances : Get Balances of a TX
func (tx TX) GetBalances(includeFiat, includeFee bool) (b Balances) {
	b = make(Balances)
	for k, i := range tx.Items {
		for _, c := range i {
			if c.Code != "" &&
				(includeFiat || !c.IsFiat()) {
				if (k == "Fee" && includeFee) || k == "From" {
					b[c.Code] = b[c.Code].Sub(c.Amount)
				} else if k == "To" {
					b[c.Code] = b[c.Code].Add(c.Amount)
				}
			}
		}
	}
	return
}

// SameBalances : Check if 2 TXs have same balance
func (tx TX) SameBalances(t TX) bool {
	bs := tx.GetBalances(false, false) // no Fiat, no Fees
	b := t.GetBalances(false, false)   // no Fiat, no Fees
	if len(bs) != len(b) {
		return false
	}
	for k, v := range bs {
		if !b[k].Equal(v) {
			return false
		}
	}
	return true
}

// OppositeBalances : Check if 2 TXs have opposite balance
func (tx TX) OppositeBalances(t TX) bool {
	bs := tx.GetBalances(false, false) // no Fiat, no Fees
	b := t.GetBalances(false, false)   // no Fiat, no Fees
	if len(bs) != len(b) {
		return false
	}
	for k, v := range bs {
		if !b[k].Equal(v.Neg()) {
			return false
		}
	}
	return true
}

// NoDifferentItemsCode : Check if 2 TXs have same Items Codes
func (tx TX) DifferentItemsCode(t TX) bool {
	code := ""
	for _, v := range tx.Items {
		for _, i := range v {
			if code == "" {
				code = i.Code
			} else {
				if code != i.Code {
					// tx has more than 1 Code per Item
					return true
				}
			}
		}
	}
	if code != "" { // No need to check if tx as no code
		for _, v2 := range t.Items {
			for _, j := range v2 {
				if j.Code != code {
					// t has at least 1 Code that does not match tx Item Code
					return true
				}
			}
		}
	}
	return false
}

func (tx TX) checkConsistancyCategVsToFrom() (err []error) {
	categ := tx.EffectiveCateg()
	// TODO : not sure yet for Transfers Forks Gifts
	if len(tx.Items["From"]) > 0 &&
		(categ == AirDrops ||
			(categ == CommercialRebates && !strings.Contains(tx.Note, "reverted")) ||
			categ == Deposits ||
			categ == Fees ||
			categ == Interests ||
			categ == Minings ||
			categ == Referrals) {
		err = append(err, errors.New("ne devrait pas avoir de DE/SORTIE"))
	}
	if len(tx.Items["From"]) == 0 &&
		(categ == CashIn ||
			categ == CashOut ||
			(categ == CommercialRebates && strings.Contains(tx.Note, "reverted")) ||
			categ == Exchanges ||
			categ == Withdrawals) {
		err = append(err, errors.New("devrait avoir au moins un DE/SORTIE"))
	}
	if len(tx.Items["To"]) > 0 &&
		(categ == Fees ||
			(categ == CommercialRebates && strings.Contains(tx.Note, "reverted")) ||
			categ == Withdrawals) {
		err = append(err, errors.New("ne devrait pas avoir de VERS/ENTREE"))
	}
	if len(tx.Items["To"]) == 0 &&
		(categ == AirDrops ||
			categ == CashIn ||
			categ == CashOut ||
			(categ == CommercialRebates && !strings.Contains(tx.Note, "reverted")) ||
			categ == Deposits ||
			categ == Exchanges ||
			categ == Interests ||
			categ == Minings ||
			categ == Referrals) {
		err = append(err, errors.New("devrait avoir au moins un VERS/ENTREE"))
	}
	return
}

func (tx TX) checkConsistancyExchangesLenToFrom() (err []error) {
	if tx.EffectiveCateg() == Exchanges {
		if len(tx.Items["To"]) != len(tx.Items["From"]) ||
			(len(tx.Items["Fee"]) != len(tx.Items["From"]) &&
				len(tx.Items["Fee"]) > 0) {
			err = append(err, errors.New("devrait avoir le même nombre de DE/SORTIE et de VERS/ENTREE"))
		}
	}
	return
}

// TXs : collection of TX
type TXs []TX

// TXsByCategory : TXs grouped by category
type TXsByCategory map[FiscalCateg]TXs

// SortByCategory : Sort TXs by Category
func (txs TXs) SortByCategory() (txsByCategory TXsByCategory) {
	txsByCategory = make(map[FiscalCateg]TXs)
	for _, t := range txs {
		txsByCategory[t.EffectiveCateg()] = append(txsByCategory[t.EffectiveCateg()], t)
	}
	return
}

// SortByDate : Sort TXs by Timestamp
func (txs TXs) SortByDate(ascending bool) {
	// keep equal elements in their original order
	sort.SliceStable(txs, func(i, j int) bool {
		if ascending {
			return txs[i].Timestamp.Before(txs[j].Timestamp)
		} else {
			return txs[i].Timestamp.After(txs[j].Timestamp)
		}
	})
}

// Stats : TXs Statistics
type Stats struct {
	Oldest time.Time
	Newest time.Time
}

// GetStatsByWallet : Get TXs Statistics by Wallet
func (txs TXs) GetStatsByWallet() map[string]Stats {
	stats := make(map[string]Stats)
	for _, t := range txs {
		stat, exist := stats[t.Wallet]
		if !exist {
			stat.Oldest = time.Date(2109, time.January, 1, 0, 0, 0, 0, time.UTC)
			stat.Newest = time.Date(2009, time.January, 1, 0, 0, 0, 0, time.UTC)
		}
		if stat.Oldest.After(t.Timestamp) {
			stat.Oldest = t.Timestamp
		}
		if stat.Newest.Before(t.Timestamp) {
			stat.Newest = t.Timestamp
		}
		stats[t.Wallet] = stat
	}
	return stats
}

// Merge TXs Exchange to try to equalize len(From) and len(To)
func (txs TXs) mergeExchangesLenToFrom() (mergedTXs TXs) {
	waitForMerge := make(map[string]TX)
	for _, tx := range txs {
		if errs := tx.checkConsistancyExchangesLenToFrom(); len(errs) == 0 {
			mergedTXs = append(mergedTXs, tx)
		} else {
			merged := false
			for _, wfm := range waitForMerge {
				if len(tx.Items["From"])+len(wfm.Items["From"]) == len(tx.Items["To"])+len(wfm.Items["To"]) &&
					(len(tx.Items["From"])+len(wfm.Items["From"]) == len(tx.Items["Fee"])+len(wfm.Items["Fee"]) ||
						len(tx.Items["Fee"])+len(wfm.Items["Fee"]) == 0) &&
					!tx.DifferentItemsCode(wfm) {
					for k, v := range wfm.Items {
						tx.Items[k] = append(tx.Items[k], v...)
					}
					delete(waitForMerge, wfm.ID)
					mergedTXs = append(mergedTXs, tx)
					merged = true
				}
			}
			if !merged {
				waitForMerge[tx.ID] = tx
			}
		}
	}
	// could not merge them, so add them
	for _, tx := range waitForMerge {
		mergedTXs = append(mergedTXs, tx)
	}
	return
}

// AddUniq : Merge TXS without duplicates
func (txs *TXs) AddUniq(newTxs TXs) {
	for _, newTx := range newTxs.mergeExchangesLenToFrom() {
		found := false
		type buddy struct {
			i     int
			delta time.Duration
		}
		candidate := buddy{delta: 1000000 * time.Hour}
		for i, t := range *txs {
			if t.ID == newTx.ID &&
				t.Wallet == newTx.Wallet &&
				t.SameBalances(newTx) {
				found = true
				break
			}
			// Detect Transfers
			if ((newTx.FiscalCateg == Deposits && t.FiscalCateg == Withdrawals) ||
				(newTx.FiscalCateg == Withdrawals && t.FiscalCateg == Deposits)) &&
				newTx.OppositeBalances(t) {
				delta := newTx.deltaDate(t.Timestamp)
				if newTx.Wallet != t.Wallet || t.Timestamp == newTx.Timestamp {
					if delta < candidate.delta {
						candidate.delta = delta
						candidate.i = i
					}
				}
			}
		}
		if !found {
			if candidate.delta < 3*time.Hour {
				// Merge Items
				for k, v := range (*txs)[candidate.i].Items {
					newTx.Items[k] = append(newTx.Items[k], v...)
				}
				// Merge Notes
				if newTx.FiscalCateg == Deposits {
					newTx.Note = "[" + (*txs)[candidate.i].Wallet + "] " + (*txs)[candidate.i].Note + " => " + newTx.Note
				} else {
					newTx.Note = newTx.Note + " => [" + (*txs)[candidate.i].Wallet + "] " + (*txs)[candidate.i].Note
				}
				// implicit setCateg(src, "DontCare")
				(*txs)[candidate.i].FiscalCateg = DontCare
				// implicit setCateg(dst, "Transfers")
				newTx.FiscalCateg = Transfers
				// links
				(*txs)[candidate.i].LinkedTxsIds = append((*txs)[candidate.i].LinkedTxsIds, newTx.ID)
				newTx.LinkedTxsIds = append(newTx.LinkedTxsIds, (*txs)[candidate.i].ID)
			}
			*txs = append(*txs, newTx)
		}
	}
}

// Total TXs with specific categ/year to CashIn
func (txs TXs) Total(categ FiscalCateg, year int, convert FiscalCateg) (cumulatedAnnualValue decimal.Decimal) {
	for i, t := range txs {
		if t.Timestamp.Year() == year && t.EffectiveCateg() == categ {
			txValue := decimal.New(0, 0)
			for k, v := range t.GetBalances(false, false) { // no Fiat, no Fees
				if v.IsPositive() ||
					(v.IsNegative() && categ != Gifts) { // For Gifts we do not count givens, only received
					txValue = txValue.Add(v.Abs().Mul(rate.Get(k, t.Timestamp, true)))
				}
			}
			cumulatedAnnualValue = cumulatedAnnualValue.Add(txValue)
			if convert == CashIn {
				txs[i].Items["From"] = append(txs[i].Items["From"], Value{
					Code:   "EUR",
					Amount: txValue.RoundBank(2),
				})
				txs[i].ModifiedCateg = CashIn
			} else if convert == CashOut {
				txs[i].Items["To"] = append(txs[i].Items["To"], Value{
					Code:   "EUR",
					Amount: txValue,
				})
				txs[i].ModifiedCateg = CashOut
			} else if convert == Gifts {
				txs[i].ModifiedCateg = Gifts
			}
		}
	}
	return
}

// SetCateg : Set a Category on a specific TX
func (txs TXs) SetCateg(id string, categ FiscalCateg, remoteFetchRate bool) error {
	for i, t := range txs {
		if t.ID == id {
			if t.FiscalCateg == Deposits {
				if t.ModifiedCateg == CashIn {
					delete(txs[i].Items, "From")
				}
				switch categ {
				case AirDrops:
					txs[i].ModifiedCateg = AirDrops
				case CashIn:
					var value decimal.Decimal
					for k, v := range t.GetBalances(false, false) { // no Fiat, no Fees
						if v.IsPositive() {
							value = value.Add(v.Mul(rate.Get(k, t.Timestamp, remoteFetchRate)))
						}
					}
					if value.IsPositive() {
						txs[i].Items["From"] = append(txs[i].Items["From"], Value{
							Code:   "EUR",
							Amount: value.RoundBank(2),
						})
						txs[i].ModifiedCateg = CashIn
					}
				case Gifts:
					txs[i].ModifiedCateg = Gifts
				case Interests:
					txs[i].ModifiedCateg = Interests
				case CommercialRebates:
					txs[i].ModifiedCateg = CommercialRebates
				case Referrals:
					txs[i].ModifiedCateg = Referrals
				case Forks:
					txs[i].ModifiedCateg = Forks
				case Minings:
					txs[i].ModifiedCateg = Minings
				case DontCare:
					txs[i].ModifiedCateg = DontCare
				case Void:
					txs[i].ModifiedCateg = Void
				default:
					return errors.New("new Category not accepted")
				}
				return nil
			}
			if t.FiscalCateg == Withdrawals {
				if t.ModifiedCateg == CashOut {
					delete(txs[i].Items, "To")
				}
				switch categ {
				case CashOut:
					var value decimal.Decimal
					for k, v := range t.GetBalances(false, false) { // no Fiat, no Fees
						if v.IsNegative() {
							value = value.Add(v.Mul(rate.Get(k, t.Timestamp, remoteFetchRate)))
						}
					}
					if value.IsNegative() {
						txs[i].Items["To"] = append(txs[i].Items["To"], Value{
							Code:   "EUR",
							Amount: value.Neg(),
						})
						txs[i].ModifiedCateg = CashOut
					}
				case Gifts:
					txs[i].ModifiedCateg = Gifts
				case ICO:
					txs[i].ModifiedCateg = ICO
				case DontCare:
					txs[i].ModifiedCateg = DontCare
				case Void:
					txs[i].ModifiedCateg = Void
				default:
					return errors.New("new Category not accepted")
				}
				return nil
			}
			return errors.New("category change not permitted")
		}
	}
	return errors.New("not found")
}

// SetValue : Set a Fiat Value on a specific TX
func (txs TXs) SetValue(id string, values []float64) error {
	for _, t := range txs {
		if t.ID == id {
			categ := t.EffectiveCateg()
			if categ == CashIn {
				if len(values) != len(t.Items["From"]) {
					return errors.New("new values count does not match From count")
				}
				for i, v := range t.Items["From"] {
					new := decimal.NewFromFloat(values[i])
					if v.IsFiat() {
						t.Items["From"][i].Amount = new
					}
				}
				return nil
			}
			if categ == CashOut {
				if len(values) != len(t.Items["To"]) {
					return errors.New("new values count does not match To count")
				}
				for i, v := range t.Items["To"] {
					new := decimal.NewFromFloat(values[i])
					if v.IsFiat() {
						t.Items["To"][i].Amount = new
					}
				}
				return nil
			}
			return errors.New("values change not permitted")
		}
	}
	return errors.New("tx not found")
}

// GetTXbyID : GetTXbyID a TX by id
func (txs TXs) GetTXbyID(id string) TX {
	for _, t := range txs {
		if t.ID == id {
			return t
		}
	}
	return TX{}
}

// Merge two txs by ids
func (txs TXs) Merge(idSrc, idDst string) error {
	for i, src := range txs {
		if src.ID == idSrc {
			srcCateg := src.EffectiveCateg()
			for j, dst := range txs {
				if dst.ID == idDst {
					dstCateg := dst.EffectiveCateg()
					if (srcCateg == ICO && dstCateg == Deposits) || // ICO case
						srcCateg == dstCateg || // Negative Balance case
						((((srcCateg == Withdrawals || srcCateg == DontCare) && dstCateg == Deposits) ||
							((srcCateg == Deposits || srcCateg == DontCare) && dstCateg == Withdrawals) ||
							(srcCateg == Withdrawals && (dstCateg == Deposits || dstCateg == Transfers)) ||
							(srcCateg == Deposits && (dstCateg == Withdrawals || dstCateg == Transfers))) &&
							src.Wallet != dst.Wallet) { // Almost Transfer case with multiple select
						// Merge Items
						for k, v := range src.Items {
							txs[j].Items[k] = append(txs[j].Items[k], v...)
						}
						// Merge Notes
						if srcCateg == Withdrawals || dstCateg == Deposits {
							txs[j].Note = "[" + src.Wallet + "] " + src.Note + " => " + dst.Note
						} else {
							txs[j].Note = dst.Note + " => [" + src.Wallet + "] " + src.Note
						}
						// implicit setCateg(src, "DontCare")
						txs[i].ModifiedCateg = DontCare
						if srcCateg == ICO && dstCateg == Deposits { // ICO case
							// implicit setCateg(dst, "Exchanges")
							txs[j].ModifiedCateg = Exchanges
						}
						if (((srcCateg == Withdrawals || srcCateg == DontCare) && dstCateg == Deposits) ||
							((srcCateg == Deposits || srcCateg == DontCare) && dstCateg == Withdrawals) ||
							(srcCateg == Withdrawals && (dstCateg == Deposits || dstCateg == Transfers)) ||
							(srcCateg == Deposits && (dstCateg == Withdrawals || dstCateg == Transfers))) &&
							src.Wallet != dst.Wallet { // Almost Transfer case with multiple select
							// implicit setCateg(dst, "Transfers")
							txs[j].ModifiedCateg = Transfers
						}
						// links
						txs[i].LinkedTxsIds = append(txs[i].LinkedTxsIds, txs[j].ID)
						txs[j].LinkedTxsIds = append(txs[j].LinkedTxsIds, txs[i].ID)
						return nil
					}
					log.Println("Source/Destination Categories not accepted on merge ["+idSrc+"] with ["+idDst+"]", src, dst)
					return errors.New("Source/Destination Categories not accepted")
				}
			}
		}
	}
	log.Println("Txid [" + idSrc + "] not found")
	return errors.New("not found")
}

// Operation : a single Operatioon
type Operation struct {
	Date    time.Time
	Name    string
	Amount  decimal.Decimal
	Balance decimal.Decimal
	Note    string
}

// CoinsStocks : Stocks per Coin
type CoinsStocks map[string][]Operation

// Stocks : get Stocks for all Coins
func (txs TXs) Stocks() CoinsStocks {
	cs := make(CoinsStocks)
	txs.SortByDate(true)
	balances := make(map[string]decimal.Decimal)
	for _, tx := range txs {
		if tx.EffectiveCateg() != DontCare &&
			tx.EffectiveCateg() != ICO {
			bal := tx.GetBalances(false, true) // no Fiat, with Fees
			for k, v := range bal {
				balances[k] = balances[k].Add(v)
				cs[k] = append(cs[k], Operation{
					Date:    tx.Timestamp,
					Name:    string(tx.EffectiveCateg()),
					Amount:  v,
					Balance: balances[k],
					Note:    tx.Wallet + " : " + tx.Note,
				})
			}
		}
	}
	return cs
}

// Before : Filter only TXs BEFORE a specific date
func (txs TXs) Before(date time.Time) TXs {
	var filteredTXs TXs
	for _, t := range txs {
		if t.Timestamp.Before(date) {
			filteredTXs = append(filteredTXs, t)
		}
	}
	return filteredTXs
}

// GetBalances : Get Balances after considering all TXs
func (txs TXs) GetBalances(includeFiat bool) Balances {
	b := make(Balances)
	txs.SortByDate(true)
	for _, tx := range txs {
		if tx.EffectiveCateg() != DontCare {
			for k, v := range tx.GetBalances(includeFiat, true) { // with Fee
				b[k] = b[k].Add(v)
			}
		}
	}
	return b
}

// GetBalancesAt : Get Balances after considering all TXs until a date
func (txs TXs) GetBalancesAt(date time.Time, includeFiat bool) Balances {
	b := make(Balances)
	txs.SortByDate(true)
	for _, tx := range txs {
		if tx.Timestamp.After(date) {
			return b
		}
		if tx.EffectiveCateg() != DontCare {
			for k, v := range tx.GetBalances(includeFiat, true) { // with Fee
				new := b[k].Add(v)
				if new.IsZero() {
					delete(b, k)
				} else {
					b[k] = new
				}
			}
		}
	}
	return b
}

func (txs TXs) GetWalletBalancesAt(date time.Time, wallet string) Balances {
	b := make(Balances)
	txs.SortByDate(true)
	for _, tx := range txs {
		if tx.Timestamp.After(date) {
			return b
		}
		if tx.Wallet == wallet &&
			(tx.EffectiveCateg() != DontCare ||
				tx.EffectiveCateg() == DontCare && len(tx.LinkedTxsIds) > 0) { // Outbound tranfert, we have to count the dontcare balances
			txBal := tx.GetBalances(false, true) // with Fee
			linkedTxsBal := make(Balances)
			if len(tx.LinkedTxsIds) > 0 &&
				tx.EffectiveCateg() != DontCare { // Outbound tranfert, do not include linked balances if tx is DontCare
				for _, linkedTxId := range tx.LinkedTxsIds {
					linkedTx := slices.Find(txs, func(candidate TX) bool {
						return candidate.ID == linkedTxId
					})
					linkedTxBal := linkedTx.GetBalances(false, true) // with Fee
					for coin, value := range linkedTxBal {
						linkedTxsBal[coin] = linkedTxsBal[coin].Add(value)
					}
				}
			}
			for txCoin, txValue := range txBal {
				bal := b[txCoin].Add(txValue)
				for linkedTxCoin, linkedTxValue := range linkedTxsBal {
					if txCoin == linkedTxCoin {
						bal = bal.Sub(linkedTxValue)
					}
				}
				if bal.IsZero() {
					delete(b, txCoin)
				} else {
					b[txCoin] = bal
				}
			}
		}
	}
	return b
}

// ConsistancyError : Consistancy Error
type ConsistancyError struct {
	TX      TX
	Related TXs
	Error   string
}

// CheckConsistancyCategVsToFrom : Check Consistancy of all TXs
func (txs TXs) CheckConsistancyCategVsToFrom() (failed []ConsistancyError) {
	for _, tx := range txs {
		var errs []string
		// Unitary Consistency
		for _, e := range tx.checkConsistancyCategVsToFrom() {
			errs = append(errs, e.Error())
		}
		if len(errs) > 0 {
			failed = append(failed, ConsistancyError{TX: tx, Error: strings.Join(errs, " ")})
		}
	}
	return
}

// CheckConsistancyNegativeBalance : Check Consistancy of all TXs
func (txs TXs) CheckConsistancyNegativeBalance(until time.Time) (failed []ConsistancyError) {
	txs.SortByDate(true)
	balances := make(map[string]decimal.Decimal)
	logged := make(map[string]bool)
	for i, tx := range txs {
		var errs []string
		var related TXs
		// Negative Balance
		if tx.EffectiveCateg() != DontCare &&
			tx.EffectiveCateg() != ICO &&
			tx.Timestamp.Before(until) {
			bal := tx.GetBalances(false, true) // no Fiat, with Fees
			for k, v := range bal {
				balances[k] = balances[k].Add(v)
			}
			for k, v := range balances {
				if v.IsNegative() && !logged[k] {
					if v.Mul(rate.Get(k, tx.Timestamp, true)).Add(decimal.New(1, 0)).IsNegative() {
						errs = append(errs, "Rend la balance de "+k+" négative : "+v.String())
						start := i - 50
						if start < 0 {
							start = 0
						}
						end := i + 50
						if end > len(txs) {
							end = len(txs)
						}
						for _, t := range txs[start:end] {
							if t.EffectiveCateg() == tx.EffectiveCateg() &&
								t.SimilarDate(2*time.Hour, tx.Timestamp) &&
								t.ID != tx.ID {
								b := t.GetBalances(false, true) // no Fiat, with Fee
								// we are look for a TX thant can increase Balance of k
								if val, ok := b[k]; ok && val.IsPositive() {
									related = append(related, t)
								}
							}
						}
						logged[k] = true
					} else {
						log.Println("Micro Negative Balance detected and ignored : " + v.String() + " " + k + " = " + v.Mul(rate.Get(k, tx.Timestamp, true)).String() + " €")
					}
				}
			}
		}
		if len(errs) > 0 {
			failed = append(failed, ConsistancyError{TX: tx, Related: related, Error: strings.Join(errs, " ")})
		}
	}
	return
}

// CheckConsistancyAlmostTransfer : Check Consistancy of all TXs
func (txs TXs) CheckConsistancyAlmostTransfer() (almosts []ConsistancyError) {
	txs.SortByDate(true)
	for i, tx := range txs {
		var related TXs
		// Deposit or Withdrawal
		txCateg := tx.EffectiveCateg()
		if txCateg == Deposits ||
			txCateg == Withdrawals {
			end := i + 150
			if end > len(txs) {
				end = len(txs)
			}
			for _, t := range txs[i+1 : end] {
				if !t.SimilarDate(2*time.Hour+20*time.Minute, tx.Timestamp) {
					break
				}
				tCateg := t.EffectiveCateg()
				if (tCateg == Deposits || tCateg == Withdrawals) &&
					tCateg != txCateg &&
					t.Wallet != tx.Wallet &&
					!t.DifferentItemsCode(tx) {
					// TODO : fine tune with balances
					// bal := tx.GetBalances(false, true) // no Fiat, with Fees
					// for k, v := range bal {
					// 	balances[k] = balances[k].Add(v)
					// }
					// b := t.GetBalances(false, true) // no Fiat, with Fees
					// if val, ok := b[k]; ok && val.IsPositive() {
					related = append(related, t)
					// }
				}
			}
			if len(related) > 0 {
				almosts = append(almosts, ConsistancyError{TX: tx, Related: related, Error: "Cette transaction peut possiblement être rapprochée pour former un Transfert entre vos Portefeuilles"})
			}
		}
	}
	return
}

// ValuedTX : Valued Transaction
type ValuedTX struct {
	QtyToFind   decimal.Decimal
	QtyFound    decimal.Decimal
	QtyIn       decimal.Decimal
	QtyOut      decimal.Decimal
	NativeValue decimal.Decimal
	PtaValue    decimal.Decimal
	Taxable     bool
	TX          TX
}

// BuyingPrice : Buying Price (Prix d'Acquisition)
type BuyingPrice struct {
	Transactions []ValuedTX
	Montant      decimal.Decimal
}

// TotalBuyingPrice : Total Buying Price (Prix Total d'Acquisition)
type TotalBuyingPrice struct {
	Acquisitions         map[string]BuyingPrice
	PrixTotalAcquisition decimal.Decimal
}

// InitialPTAFIFO : calculate Initial (at Jan 1st 2019) Total Buying Price (PTA) using FIFO method
func (txs TXs) InitialPTAFIFO() (pta TotalBuyingPrice, err error) {
	// source Bofip
	// RPPM - Plus-values sur biens meubles et taxe forfaitaire sur les objets précieux
	// - Cession d'actifs numériques à titre occasionnel - Base d'imposition
	// Cas des cessions antérieures au 1er Janvier 2019
	// 130
	// L'article 41 de la loi n° 2018-1317 du 28 décembre 2018 de finances pour 2019,
	// codifié à l'article 150 VH bis du CGI, s'appliquant aux cessions réalisées à
	// compter du 1er janvier 2019, il convient, pour la détermination du prix total
	// d'acquisition, de n'inclure dans ce dernier que les prix effectifs d'acquisition
	// des actifs détenus à cette date.
	// Ainsi, en cas de cessions réalisées antérieurement au 1er janvier 2019, il
	// convient notamment de ne pas inclure dans le prix total d'acquisition déclaré à
	// l'occasion de la première cession réalisée postérieurement à cette date, les
	// prix d'acquisition :
	// - mentionnés dans les déclarations de plus-values de cessions déclarées en
	//   application du droit en vigueur avant le 1er janvier 2019 ;
	// - n'ayant pas été déclarés, conformément au droit en vigueur avant le 1er
	//   janvier 2019 (cessions dont le prix de cession était inférieur à 5 000 € et
	//   ayant bénéficié de l'exonération prévue au 2° du II de l'article 150 UA du CGI
	//   par exemple) ;
	// - n'ayant pas été déclarés en contravention avec le droit en vigueur avant le
	//   1er janvier 2019.
	// Il est rappelé que les éventuelles plus-values réalisées antérieurement au
	// 1er janvier 2019 relèvent du droit de reprise de l'administration.
	// Remarque : En cas d'échange entre actifs numériques réalisé, même sans soulte,
	// antérieurement au 1er janvier 2019, le prix total d'acquisition à retenir à
	// compter du 1er janvier 2019 est constitué de la valeur de l'actif numérique
	// remis lors de cet échange (valeur à la date de cet échange). Corrélativement,
	// le prix d'acquisition retenu à l'occasion de cette cession n'est pas inclus dans
	// le prix total d'acquisition déclaré à compter du 1er janvier 2019.
	// Par ailleurs, les moins-values constatées lors des cessions réalisées
	// antérieurement au 1er janvier 2019 ne peuvent être imputées sur d'éventuelles
	// plus-values réalisées, quelle que soit leur date de réalisation.
	pta.Acquisitions = make(map[string]BuyingPrice)
	date2019Jan1 := time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC)
	txsBefore2019Jan1 := txs.Before(date2019Jan1)
	balances2019Jan1 := txsBefore2019Jan1.GetBalances(false) // no Fiat
	txsBefore2019Jan1.SortByDate(false)
	for coin, balance := range balances2019Jan1 {
		if balance.IsNegative() {
			return pta, errors.New("Erreur : votre stock initial de " + coin + " au 1 Janvier 2019 est négatif, il doit manquer des transactions !")
		}
		amountToFind := balance
		computePta := amountToFind.GreaterThan(decimal.Zero)
		var initialTxs []ValuedTX

		for _, tx := range txsBefore2019Jan1 {
			if _, txHasCoin := tx.GetBalances(true, true)[coin]; tx.EffectiveCateg() == DontCare ||
				tx.EffectiveCateg() == ICO ||
				!txHasCoin { // Check tx has the wanted coin
				continue
			}

			// Find all Tx that have the wanted coin into Items["To"]
			vtx := ValuedTX{
				QtyToFind: amountToFind,
				TX:        tx,
			}
			for _, c := range tx.Items["To"] {
				if c.Code == coin {
					vtx.QtyIn = vtx.QtyIn.Add(c.Amount)
				}
			}
			// ... and the ones consumpting the wanted coin
			for _, c := range tx.Items["From"] {
				if c.Code == coin {
					vtx.QtyOut = vtx.QtyOut.Add(c.Amount)
				}
			}
			for _, c := range tx.Items["Fee"] {
				if c.Code == coin {
					vtx.QtyOut = vtx.QtyOut.Add(c.Amount)
				}
			}

			if computePta {
				vtx.QtyFound = vtx.QtyIn.Sub(vtx.QtyOut)
				amountToFind = amountToFind.Sub(vtx.QtyFound)
			}

			if vtx.TX.EffectiveCateg() == CashIn {
				fromValues := vtx.TX.Items["From"]
				for _, v := range fromValues {
					if v.IsFiat() {
						if v.Code == "EUR" {
							vtx.NativeValue = vtx.NativeValue.Add(v.Amount)
						} else {
							return pta,
								errors.New("Erreur : votre stock initial de [" + coin + "] au 1 Janvier 2019 contient des cashin en [" + v.Code + "], seul l'euro est actuellement supporté !")
						}
					}
				}
			} else if vtx.TX.EffectiveCateg() == CashOut {
				toValues := vtx.TX.Items["To"]
				for _, v := range toValues {
					if v.IsFiat() {
						if v.Code == "EUR" {
							vtx.NativeValue = vtx.NativeValue.Sub(v.Amount)
						} else {
							return pta,
								errors.New("Erreur : votre stock initial de [" + coin + "] au 1 Janvier 2019 contient des cashout en [" + v.Code + "], seul l'euro est actuellement supporté !")
						}
					}
				}
			} else {
				r := rate.Get(coin, tx.Timestamp, true)
				vtx.NativeValue = vtx.QtyIn.Sub(vtx.QtyOut).Mul(r)
			}

			if vtx.QtyFound.IsPositive() {
				vtx.PtaValue = vtx.QtyFound.Mul(vtx.NativeValue).Div(vtx.QtyIn)
			}
			if vtx.NativeValue.RoundBank(0).IntPart() <= -5000 {
				vtx.Taxable = true
			}
			initialTxs = append(initialTxs, vtx)
		}
		if amountToFind.IsPositive() {
			return pta, errors.New("Impossible de trouver assez de transactions pour calculer le prix total d'acquisition de " + coin + " par PEPS (Premier Entrant Premier Sortant), il en manque " + amountToFind.String() + " !")
		} else {
			coinPtaValue := decimal.Zero
			if computePta {
				err := updateCashoutsPtaValue(coin, &initialTxs)
				if err != nil {
					return pta, err
				}
				coinPtaValue = getPtaValue(initialTxs)
				pta.PrixTotalAcquisition = pta.PrixTotalAcquisition.Add(coinPtaValue)
			}

			pta.Acquisitions[coin] = BuyingPrice{
				Transactions: initialTxs,
				Montant:      coinPtaValue,
			}
		}
	}
	return pta, nil
}

func getPtaValue(txs []ValuedTX) (ptaValue decimal.Decimal) {
	for _, tx := range txs {
		ptaValue = ptaValue.Add(tx.PtaValue)
	}

	return ptaValue
}

type fifoCoin struct {
	Amount decimal.Decimal
	Price  decimal.Decimal
}

// Compute pta value of cashouts
func updateCashoutsPtaValue(coin string, txs *[]ValuedTX) (err error) {
	fifo := make([]fifoCoin, 0)
	for i := len(*txs) - 1; i >= 0; i-- {
		tx := &(*txs)[i]
		if tx.QtyFound.GreaterThan(decimal.Zero) {
			// Add buyed coins to fifo
			fifo = append(fifo, fifoCoin{
				Amount: tx.QtyFound,
				Price:  tx.NativeValue.Div(tx.QtyFound),
			})
		} else {
			// Remove buyed coins to fifo
			qty := tx.QtyFound.Abs()
			for _, fifoCoin := range fifo {
				spend := decimal.Min(fifoCoin.Amount, qty)
				fifoCoin.Amount = fifoCoin.Amount.Sub(spend)
				qty = qty.Sub(spend)
				(*tx).PtaValue = tx.PtaValue.Sub(spend.Mul(fifoCoin.Price))
			}
			if qty.GreaterThan(decimal.Zero) {
				return errors.New("Impossible de trouver assez d'actifs achetés pour calculer le capital de la cession de [" + coin + "] au [" + tx.TX.Timestamp.String() + "]")
			}
		}
	}
	return nil
}

// Cession imposable : fait générateur d'impôt
type Cession struct {
	Source                               string
	Note                                 string
	Date211                              time.Time
	ValeurPortefeuille212                decimal.Decimal
	Prix213                              decimal.Decimal
	Frais214                             decimal.Decimal
	PrixNetDeFrais215                    decimal.Decimal
	SoulteRecueOuVersee216               decimal.Decimal
	PrixNetDeSoulte217                   decimal.Decimal
	PrixNet218                           decimal.Decimal
	PrixTotalAcquisition220              decimal.Decimal
	FractionDeCapital221                 decimal.Decimal
	SoulteRecueEnCasDechangeAnterieur222 decimal.Decimal
	PrixTotalAcquisitionNet223           decimal.Decimal
	PlusMoinsValue                       decimal.Decimal
}

// Calculate cession
func (c *Cession) Calculate() {
	c.Prix213 = c.PrixNetDeFrais215.Add(c.Frais214)
	c.PrixNetDeSoulte217 = c.Prix213.Sub(c.SoulteRecueOuVersee216)
	// c.PrixNetDeSoulte217 = c.Prix213.Add(c.SoulteRecueOuVersee216)
	c.PrixNet218 = c.Prix213.Sub(c.Frais214).Sub(c.SoulteRecueOuVersee216)
	// c.PrixNet218 = c.Prix213.Sub(c.Frais214).Add(c.SoulteRecueOuVersee216)
	c.PrixTotalAcquisitionNet223 = c.PrixTotalAcquisition220.Sub(c.FractionDeCapital221).Sub(c.SoulteRecueEnCasDechangeAnterieur222)
	if !c.ValeurPortefeuille212.IsZero() {
		c.PlusMoinsValue = c.PrixNet218.Sub(c.PrixTotalAcquisitionNet223.Mul(c.PrixNetDeSoulte217).Div(c.ValeurPortefeuille212))
	}
}

// GetCessions : calculate Cessions for a specific year
func (txs TXs) GetCessions(year int, startPTA decimal.Decimal, startfractionCapital decimal.Decimal) (cessions []Cession, pta decimal.Decimal, fractionCapital decimal.Decimal, err error) {
	pta = startPTA
	fractionCapital = startfractionCapital
	for i, tx := range txs {
		if tx.Timestamp.Year() == year {
			if tx.EffectiveCateg() == CashOut {
				if len(tx.Items["To"]) == 0 {
					err = errors.New("CashOut on " + tx.Timestamp.String() + " does not have any To items")
					return
				}
				balances := tx.GetBalances(true, false).Values() // with Fiat, no Fee
				for _, b := range balances {
					if b.IsFiat() && b.Amount.GreaterThanOrEqual(decimal.NewFromInt(1)) {
						c := Cession{
							Date211: tx.Timestamp,
							Source:  tx.Source,
							Note:    tx.Note,
						}

						// Valeur globale du portefeuille à la date de la cession
						c.ValeurPortefeuille212, err = txs[:i].computeValeurPortefeuille212(tx.Timestamp)
						if err != nil {
							return
						}

						// Prix de cession
						// Il correspond au prix réel perçu ou à la valeur de la contrepartie
						// obtenue par le cédant lors de la cession.
						if b.Code == "EUR" {
							c.PrixNetDeFrais215 = b.Amount
						} else {
							fiatRate := rate.Get(b.Code, tx.Timestamp, true)
							if fiatRate.IsZero() {
								err = errors.New("Missing rate for " + b.Code + " on " + tx.Timestamp.String())
								return
							}
							c.PrixNetDeFrais215 = b.Amount.Mul(fiatRate)
						}

						// Prix de cession - Frais
						// Il est réduit, sur justificatifs, des frais supportés par le cédant à
						// l’occasion de cette cession. Ces frais s'entendent, notamment, de
						// ceux perçus à l’occasion de l’opération imposable par les plateformes
						// où s'effectuent les cessions à titre onéreux d'actifs numériques ou
						// de droits s'y rapportant ainsi que de ceux perçus par les membres du
						// réseau (appelés "mineurs") chargés de vérifier et valider les
						// transactions qui s'y opèrent. Le paiement de ces frais de transaction
						// perçus par les plateformes ou les "mineurs" peut s'effectuer au moyen
						// d'actifs numériques. Or, dans ce cas, ce paiement est la contrepartie
						// d'un service fourni au cédant et constitue une opération imposable au
						// sens du I de l'article 150 VH bis du CGI. A titre de mesure de
						// simplification, il est toutefois admis que la cession en tant que
						// telle et les différentes prestations de services rendues en
						// contrepartie des frais perçus par les plateformes et les "mineurs"
						// soient assimilées à une seule et même opération de cession pour
						// l'application de l'article 150 VH bis du CGI, pour laquelle le
						// contribuable détermine une seule plus ou moins-value, en déduisant
						// ces frais du prix de cession.
						// Les transactions ne contiennent pas de frais en EUR. On simplifie
						// les cessions en ne définissant pas de frais. Pas de frais, pas de justificatifs.

						// Prix de cession - Soultes
						// Le prix de cession doit être majoré de la soulte que le cédant a
						// reçue lors de la cession ou minoré de la soulte qu’il a versée lors
						// de cette même cession.
						// on ne gère pas les soultes
						// c.SoulteRecueOuVersee216 = ???
						c.PrixTotalAcquisition220 = pta

						// Fractions de capital initial
						// Il s’agit de la fraction de capital contenue dans la valeur ou le
						// prix de chacune des différentes cessions d'actifs numériques à titre
						// gratuit ou onéreux réalisées antérieurement, hors opérations d’échange
						// ayant bénéficié du sursis d’imposition sans soulte.
						c.FractionDeCapital221 = fractionCapital

						// Soulte reçue en cas d’échanges antérieurs à la cession
						// Lorsqu’un ou plusieurs échanges avec soulte reçue par le cédant ont été
						// réalisés antérieurement à la cession imposable, le prix total d’acquisition
						// est minoré du montant des soultes. Indiquez donc les montants reçus.
						// c.SoulteRecueEnCasDechangeAnterieur222 = ???
						c.Calculate() // to have 217 and 223
						cessions = append(cessions, c)
						// Les frais déductibles, quels qu'ils soient, ne viennent pas en
						// diminution du prix de cession pour la détermination du quotient du
						// prix de cession sur la valeur globale du portefeuille (ils doivent
						// seulement être déduits du prix de cession qui constitue le premier
						// terme de la différence prévue dans la formule de calcul mentionnée
						// ci-dessus).
						if c.ValeurPortefeuille212.IsZero() {
							err = errors.New("reports.cashouts.global.wallet.zero|date=" + strconv.FormatInt(tx.Timestamp.UnixMilli(), 10))
							return
						}
						coefCession := c.PrixNetDeSoulte217.Div(c.ValeurPortefeuille212)
						fractionAcquisition := coefCession.Mul(c.PrixTotalAcquisitionNet223)
						fractionCapital = fractionCapital.Add(fractionAcquisition)
					}
				}
			} else if tx.EffectiveCateg() == CashIn {
				if len(tx.Items["From"]) == 0 {
					err = errors.New("CashIn on " + tx.Timestamp.String() + " does not have any From items")
					return
				}
				balances := tx.GetBalances(true, false).Values() // with Fiat, no Fees
				for _, b := range balances {
					if b.IsFiat() {
						// Prix total d’acquisition du portefeuille
						// Le prix total d'acquisition du portefeuille d'actifs numériques est
						// égal à la somme de tous les prix acquittés en monnaie ayant cours
						// légal à l'occasion de l'ensemble des acquisitions d’actifs numériques
						// (sauf opérations d'échange ayant bénéficié du sursis d'imposition)
						// réalisées avant la cession, et de la valeur des biens ou services,
						// comprenant le cas échéant les soultes versées, fournis en
						// contrepartie de ces acquisitions.
						if b.Code == "EUR" {
							pta = pta.Add(b.Amount.Neg())
						} else {
							fiatRate := rate.Get(b.Code, tx.Timestamp, true)
							if fiatRate.IsZero() {
								err = errors.New("Missing rate for " + b.Code + " on " + tx.Timestamp.String())
								return
							}
							pta = pta.Add(b.Amount.Neg().Mul(fiatRate))
						}
					}
				}
			}
		}
	}
	return
}

// Valeur globale du portefeuille
// Il s’agit de la somme des valeurs, évaluées au moment de la cession
// imposable, des différents actifs numériques et droits s'y rapportant,
// détenus par le cédant avant de procéder à la cession, quel que soit
// leur support de conservation (plateformes d’échanges, y compris
// étrangères, serveurs personnels, dispositif de stockage hors-ligne,
// etc.). Cette valorisation doit s’effectuer au moment de chaque cession
// imposable en application de l’article 150 VH bis du CGI.
func (txs TXs) computeValeurPortefeuille212(date time.Time) (valeurPortefeuille212 decimal.Decimal, err error) {
	globalWallet := txs.GetBalances(false).Values() // no Fiat
	for _, v := range globalWallet {
		r := rate.Get(v.Code, date, true)
		amountEUR := v.Amount.Mul(r)
		if amountEUR.IsZero() {
			// err = errors.New("Missing rate for " + v.Code + " on " + tx.Timestamp.String())
			// return
			log.Println("Missing rate for " + v.Code + " on " + date.String())
		}
		if amountEUR.IsNegative() {
			err = errors.New("reports.cashouts.negative.amount|quantity=" + v.Amount.String() + "|code=" + v.Code + "|eurPrice=" + r.String() + "|date=" + strconv.FormatInt(date.UnixMilli(), 10))
			return
		}
		valeurPortefeuille212 = valeurPortefeuille212.Add(amountEUR)
	}

	return valeurPortefeuille212, err
}

// GetUnrealizedPnl : calculate unrealized pnl for a specific year
func (txs TXs) GetUnrealizedPnl(date time.Time, pta decimal.Decimal, fractionCapital decimal.Decimal) (unrealizedPnl decimal.Decimal, err error) {
	valeurPortefeuille212, err := txs.computeValeurPortefeuille212(date)
	if err != nil {
		return
	}
	// Valeur portefeuille - prix total acquisition + fraction capital initial
	return valeurPortefeuille212.Sub(pta.Sub(fractionCapital)), err
}
