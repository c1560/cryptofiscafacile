package tx

import (
	"context"
	"reflect"
	"testing"
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/google/go-cmp/cmp"
	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/app"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/rate"
)

func TestValue_IsFiat(t *testing.T) {
	tests := []struct {
		name string
		v    Value
		want bool
	}{
		{
			name: "EUR",
			v: Value{
				Code: "EUR",
			},
			want: true,
		},
		{
			name: "BTC",
			v: Value{
				Code: "BTC",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.v.IsFiat(); got != tt.want {
				t.Errorf("Value.IsFiat() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTX_GenerateID(t *testing.T) {
	tests := []struct {
		name   string
		tx     *TX
		wantID string
	}{
		{
			name: "Simple",
			tx: &TX{
				Source:    "Source Example",
				Timestamp: time.Date(2021, time.September, 23, 16, 01, 01, 01, time.UTC),
			},
			wantID: "015660c6822aa38471f33e15fe7c3500d124dcf2a5f192dacc5e679bd58dc2c7",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.tx.GenerateID()
			if tt.tx.ID != tt.wantID {
				t.Errorf("tx.ID = %v, want %v", tt.tx.ID, tt.wantID)
			}
		})
	}
}

func TestTX_SimilarDate(t *testing.T) {
	type args struct {
		delta time.Duration
		t     time.Time
	}
	tests := []struct {
		name string
		tx   TX
		args args
		want bool
	}{
		{
			name: "true",
			tx: TX{
				Timestamp: time.Date(2020, time.January, 1, 0, 0, 0, 0, time.UTC),
			},
			args: args{
				delta: time.Second,
				t:     time.Date(2020, time.January, 1, 0, 0, 0, 0, time.UTC),
			},
			want: true,
		},
		{
			name: "false",
			tx: TX{
				Timestamp: time.Date(2020, time.January, 1, 0, 0, 0, 0, time.UTC),
			},
			args: args{
				delta: time.Second,
				t:     time.Date(2021, time.January, 1, 0, 0, 0, 0, time.UTC),
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tx.SimilarDate(tt.args.delta, tt.args.t); got != tt.want {
				t.Errorf("TX.SimilarDate() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBalances_Values(t *testing.T) {
	tests := []struct {
		name     string
		b        Balances
		wantVals Values
	}{
		{
			name: "Simple",
			b: Balances{
				"BTC": decimal.New(2, 0),
			},
			wantVals: Values{
				{
					Code:   "BTC",
					Amount: decimal.New(2, 0),
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotVals := tt.b.Values(); !reflect.DeepEqual(gotVals, tt.wantVals) {
				t.Errorf("Balances.Values() = %v, want %v", spew.Sdump(gotVals), spew.Sdump(tt.wantVals))
			}
		})
	}
}

func TestTX_GetBalances(t *testing.T) {
	type args struct {
		includeFiat bool
		includeFee  bool
	}
	tests := []struct {
		name  string
		tx    TX
		args  args
		wantB Balances
	}{
		{
			name: "Simple",
			tx: TX{
				Items: map[string]Values{
					"Fee": {
						Value{
							Code:   "BTC",
							Amount: decimal.New(1, 0),
						},
						Value{
							Code:   "BTC",
							Amount: decimal.New(2, 0),
						},
					},
					"From": {
						Value{
							Code:   "BTC",
							Amount: decimal.New(5, 0),
						},
					},
					"To": {
						Value{
							Code:   "BTC",
							Amount: decimal.New(10, 0),
						},
					},
				},
			},
			args: args{
				includeFiat: true,
				includeFee:  true,
			},
			wantB: Balances{
				"BTC": decimal.New(2, 0),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotB := tt.tx.GetBalances(tt.args.includeFiat, tt.args.includeFee); !cmp.Equal(gotB, tt.wantB) {
				t.Errorf("TX.GetBalances() = %v, want %v", gotB, tt.wantB)
			}
		})
	}
}

func TestTX_SameBalances(t *testing.T) {
	type args struct {
		t TX
	}
	tests := []struct {
		name string
		tx   TX
		args args
		want bool
	}{
		{
			name: "Same exclude Fee",
			tx: TX{
				Items: map[string]Values{
					"Fee": {
						Value{
							Code:   "BTC",
							Amount: decimal.New(1, 0),
						},
					},
					"To": {
						Value{
							Code:   "BTC",
							Amount: decimal.New(10, 0),
						},
					},
				},
			},
			args: args{
				t: TX{
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
				},
			},
			want: true,
		},
		{
			name: "Different Length",
			tx: TX{
				Items: map[string]Values{
					"To": {
						Value{
							Code:   "ETH",
							Amount: decimal.New(1, 0),
						},
						Value{
							Code:   "BTC",
							Amount: decimal.New(10, 0),
						},
					},
				},
			},
			args: args{
				t: TX{
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
				},
			},
			want: false,
		},
		{
			name: "Different Amount",
			tx: TX{
				Items: map[string]Values{
					"To": {
						Value{
							Code:   "BTC",
							Amount: decimal.New(1, 0),
						},
						Value{
							Code:   "BTC",
							Amount: decimal.New(10, 0),
						},
					},
				},
			},
			args: args{
				t: TX{
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
				},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tx.SameBalances(tt.args.t); got != tt.want {
				t.Errorf("TX.SameBalances() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTX_OppositeBalances(t *testing.T) {
	type args struct {
		t TX
	}
	tests := []struct {
		name string
		tx   TX
		args args
		want bool
	}{
		{
			name: "Opposite exclude Fee",
			tx: TX{
				Items: map[string]Values{
					"Fee": {
						Value{
							Code:   "BTC",
							Amount: decimal.New(1, 0),
						},
					},
					"To": {
						Value{
							Code:   "BTC",
							Amount: decimal.New(-10, 0),
						},
					},
				},
			},
			args: args{
				t: TX{
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
				},
			},
			want: true,
		},
		{
			name: "Different Length",
			tx: TX{
				Items: map[string]Values{
					"To": {
						Value{
							Code:   "ETH",
							Amount: decimal.New(1, 0),
						},
						Value{
							Code:   "BTC",
							Amount: decimal.New(-10, 0),
						},
					},
				},
			},
			args: args{
				t: TX{
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
				},
			},
			want: false,
		},
		{
			name: "Different Amount",
			tx: TX{
				Items: map[string]Values{
					"To": {
						Value{
							Code:   "BTC",
							Amount: decimal.New(1, 0),
						},
						Value{
							Code:   "BTC",
							Amount: decimal.New(-10, 0),
						},
					},
				},
			},
			args: args{
				t: TX{
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
				},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tx.OppositeBalances(tt.args.t); got != tt.want {
				t.Errorf("TX.OppositeBalances() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTXs_SortByCategory(t *testing.T) {
	tests := []struct {
		name              string
		txs               TXs
		wantTxsByCategory TXsByCategory
	}{
		{
			name: "Test 1",
			txs: TXs{
				{
					Source:      "Dep 1",
					FiscalCateg: Deposits,
				},
				{
					Source:      "Wit 1",
					FiscalCateg: Withdrawals,
				},
				{
					Source:      "Dep 2",
					FiscalCateg: Deposits,
				},
			},
			wantTxsByCategory: TXsByCategory{
				Deposits: {
					{
						Source:      "Dep 1",
						FiscalCateg: Deposits,
					},
					{
						Source:      "Dep 2",
						FiscalCateg: Deposits,
					},
				},
				Withdrawals: {
					{
						Source:      "Wit 1",
						FiscalCateg: Withdrawals,
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotTxsByCategory := tt.txs.SortByCategory(); !cmp.Equal(gotTxsByCategory, tt.wantTxsByCategory) {
				t.Errorf("TXs.SortByCategory() = %v, want %v", gotTxsByCategory, tt.wantTxsByCategory)
			}
		})
	}
}

func TestTXs_SortByDate(t *testing.T) {
	type args struct {
		chrono bool
	}
	tests := []struct {
		name    string
		args    args
		txs     TXs
		wantTXs TXs
	}{
		{
			name: "TXs Sort By Date order Chrono",
			args: args{
				chrono: true,
			},
			txs: TXs{
				TX{Timestamp: time.Date(2020, time.January, 1, 0, 0, 0, 0, time.UTC)},
				TX{Timestamp: time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC)},
			},
			wantTXs: TXs{
				TX{Timestamp: time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC)},
				TX{Timestamp: time.Date(2020, time.January, 1, 0, 0, 0, 0, time.UTC)},
			},
		},
		{
			name: "TXs Sort By Date order anti Chrono",
			args: args{
				chrono: false,
			},
			txs: TXs{
				TX{Timestamp: time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC)},
				TX{Timestamp: time.Date(2020, time.January, 1, 0, 0, 0, 0, time.UTC)},
			},
			wantTXs: TXs{
				TX{Timestamp: time.Date(2020, time.January, 1, 0, 0, 0, 0, time.UTC)},
				TX{Timestamp: time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC)},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.txs.SortByDate(tt.args.chrono)
			for i := range tt.txs {
				if !tt.txs[i].Timestamp.Equal(tt.wantTXs[i].Timestamp) {
					t.Errorf("TXs.SortByDate() txs = %v, wantTXs %v", tt.txs, tt.wantTXs)
				}
			}
		})
	}
}

func TestTXs_Total(t *testing.T) {
	rate.SetCff(app.NewCff(context.Background()))
	type args struct {
		categ   FiscalCateg
		year    int
		convert FiscalCateg
	}
	tests := []struct {
		name      string
		txs       TXs
		args      args
		wantValue decimal.Decimal
		wantTXs   TXs
	}{
		{
			name: "Simple",
			txs: TXs{
				TX{
					Timestamp:   time.Date(2020, time.January, 10, 0, 0, 0, 0, time.UTC),
					FiscalCateg: Interests,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
				},
				TX{
					Timestamp:   time.Date(2020, time.January, 10, 0, 0, 0, 0, time.UTC),
					FiscalCateg: AirDrops,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
				},
				TX{
					Timestamp:   time.Date(2020, time.January, 20, 0, 0, 0, 0, time.UTC),
					FiscalCateg: AirDrops,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
				},
				TX{
					Timestamp:   time.Date(2019, time.January, 10, 0, 0, 0, 0, time.UTC),
					FiscalCateg: AirDrops,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
				},
			},
			args: args{
				categ:   AirDrops,
				year:    2020,
				convert: CashIn,
			},
			wantValue: decimal.New(1488450146484375, -10),
			wantTXs: TXs{
				TX{
					Timestamp:   time.Date(2020, time.January, 10, 0, 0, 0, 0, time.UTC),
					FiscalCateg: Interests,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
				},
				TX{
					Timestamp:     time.Date(2020, time.January, 10, 0, 0, 0, 0, time.UTC),
					FiscalCateg:   AirDrops,
					ModifiedCateg: CashIn,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
						"From": {
							Value{
								Code:   "EUR",
								Amount: decimal.New(7051271, -2),
							},
						},
					},
				},
				TX{
					Timestamp:     time.Date(2020, time.January, 20, 0, 0, 0, 0, time.UTC),
					FiscalCateg:   AirDrops,
					ModifiedCateg: CashIn,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
						"From": {
							Value{
								Code:   "EUR",
								Amount: decimal.New(7833231, -2),
							},
						},
					},
				},
				TX{
					Timestamp:   time.Date(2019, time.January, 10, 0, 0, 0, 0, time.UTC),
					FiscalCateg: AirDrops,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotValue := tt.txs.Total(tt.args.categ, tt.args.year, tt.args.convert); !cmp.Equal(gotValue, tt.wantValue) {
				t.Errorf("TXs.Total() = %v, want %v", gotValue, tt.wantValue)
			}
			if !cmp.Equal(tt.txs, tt.wantTXs) {
				t.Errorf("TXs.Total() txs = %s, want %s", spew.Sdump(tt.txs), spew.Sdump(tt.wantTXs))
			}
		})
	}
}

func TestTXs_AddUniq(t *testing.T) {
	type args struct {
		a TXs
	}
	tests := []struct {
		name    string
		txs     TXs
		args    args
		wantTXs TXs
	}{
		{
			name: "Add New ID",
			txs: TXs{
				TX{
					Timestamp: time.Date(2020, time.January, 1, 0, 0, 0, 0, time.UTC),
					ID:        "existingID",
					Wallet:    "Wallet1",
					Source:    "existingSource",
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
				},
			},
			args: args{
				a: TXs{
					TX{
						Timestamp: time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC),
						ID:        "newID",
						Wallet:    "Wallet1",
						Source:    "existingSource",
						Items: map[string]Values{
							"To": {
								Value{
									Code:   "BTC",
									Amount: decimal.New(10, 0),
								},
							},
						},
					},
				},
			},
			wantTXs: TXs{
				TX{
					Timestamp: time.Date(2020, time.January, 1, 0, 0, 0, 0, time.UTC),
					ID:        "existingID",
					Wallet:    "Wallet1",
					Source:    "existingSource",
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
				},
				TX{
					Timestamp: time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC),
					ID:        "newID",
					Wallet:    "Wallet1",
					Source:    "existingSource",
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
				},
			},
		},
		{
			name: "Add Existing Source",
			txs: TXs{
				TX{
					Timestamp: time.Date(2020, time.January, 1, 0, 0, 0, 0, time.UTC),
					ID:        "existingID",
					Wallet:    "Wallet1",
					Source:    "existingSource",
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
				},
			},
			args: args{
				a: TXs{
					TX{
						Timestamp: time.Date(2020, time.January, 1, 0, 0, 0, 0, time.UTC),
						ID:        "existingID",
						Wallet:    "Wallet1",
						Source:    "newSource",
						Items: map[string]Values{
							"To": {
								Value{
									Code:   "BTC",
									Amount: decimal.New(10, 0),
								},
							},
						},
					},
				},
			},
			wantTXs: TXs{
				TX{
					Timestamp: time.Date(2020, time.January, 1, 0, 0, 0, 0, time.UTC),
					ID:        "existingID",
					Wallet:    "Wallet1",
					Source:    "existingSource",
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
				},
			},
		},
		{
			name: "Detect Transfers",
			txs: TXs{
				TX{
					Timestamp:   time.Date(2020, time.January, 1, 0, 0, 0, 0, time.UTC),
					ID:          "depID",
					Wallet:      "Wallet1",
					FiscalCateg: Deposits,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
				},
			},
			args: args{
				a: TXs{
					TX{
						Timestamp:   time.Date(2020, time.January, 1, 1, 0, 0, 0, time.UTC),
						ID:          "witID",
						Wallet:      "Wallet2",
						FiscalCateg: Withdrawals,
						Items: map[string]Values{
							"From": {
								Value{
									Code:   "BTC",
									Amount: decimal.New(1, 1),
								},
							},
							"Fee": {
								Value{
									Code:   "BTC",
									Amount: decimal.New(1, -2),
								},
							},
						},
					},
				},
			},
			wantTXs: TXs{
				TX{
					Timestamp:    time.Date(2020, time.January, 1, 0, 0, 0, 0, time.UTC),
					ID:           "depID",
					Wallet:       "Wallet1",
					LinkedTxsIds: []string{"witID"},
					FiscalCateg:  DontCare,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
				},
				TX{
					Timestamp:    time.Date(2020, time.January, 1, 1, 0, 0, 0, time.UTC),
					ID:           "witID",
					Wallet:       "Wallet2",
					LinkedTxsIds: []string{"depID"},
					FiscalCateg:  Transfers,
					Items: map[string]Values{
						"From": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(1, 1),
							},
						},
						"Fee": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(1, -2),
							},
						},
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
					Note: " => [Wallet1] ",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.txs.AddUniq(tt.args.a)
			if !cmp.Equal(tt.txs, tt.wantTXs) {
				t.Errorf("TXs.AddUniq() txs = %s, want %s", spew.Sdump(tt.txs), spew.Sdump(tt.wantTXs))
			}
		})
	}
}

func TestTXs_SetCateg(t *testing.T) {
	type args struct {
		id    string
		categ FiscalCateg
	}
	tests := []struct {
		name    string
		txs     TXs
		args    args
		wantErr bool
		wantTXs TXs
	}{
		{
			name: "Change Deposit to AirDrops",
			txs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Deposits,
				},
				TX{
					ID:          "id2",
					FiscalCateg: Deposits,
				},
			},
			args: args{
				id:    "id2",
				categ: AirDrops,
			},
			wantErr: false,
			wantTXs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Deposits,
				},
				TX{
					ID:            "id2",
					FiscalCateg:   Deposits,
					ModifiedCateg: AirDrops,
				},
			},
		},
		{
			name: "Change Deposit to CashIn",
			txs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Deposits,
				},
				TX{
					Timestamp:   time.Date(2021, time.September, 24, 16, 01, 01, 01, time.UTC),
					ID:          "id2",
					FiscalCateg: Deposits,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(1, 0),
							},
						},
					},
				},
			},
			args: args{
				id:    "id2",
				categ: CashIn,
			},
			wantErr: false,
			wantTXs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Deposits,
				},
				TX{
					Timestamp:     time.Date(2021, time.September, 24, 16, 01, 01, 01, time.UTC),
					ID:            "id2",
					FiscalCateg:   Deposits,
					ModifiedCateg: CashIn,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(1, 0),
							},
						},
						"From": {
							Value{
								Code:   "EUR",
								Amount: decimal.New(38302, 0),
							},
						},
					},
				},
			},
		},
		{
			name: "Change Deposit to Gift",
			txs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Deposits,
				},
				TX{
					ID:          "id2",
					FiscalCateg: Deposits,
				},
			},
			args: args{
				id:    "id2",
				categ: Gifts,
			},
			wantErr: false,
			wantTXs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Deposits,
				},
				TX{
					ID:            "id2",
					FiscalCateg:   Deposits,
					ModifiedCateg: Gifts,
				},
			},
		},
		{
			name: "Change Deposit to Interests",
			txs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Deposits,
				},
				TX{
					ID:          "id2",
					FiscalCateg: Deposits,
				},
			},
			args: args{
				id:    "id2",
				categ: Interests,
			},
			wantErr: false,
			wantTXs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Deposits,
				},
				TX{
					ID:            "id2",
					FiscalCateg:   Deposits,
					ModifiedCateg: Interests,
				},
			},
		},
		{
			name: "Change Deposit to CommercialRebates",
			txs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Deposits,
				},
				TX{
					ID:          "id2",
					FiscalCateg: Deposits,
				},
			},
			args: args{
				id:    "id2",
				categ: CommercialRebates,
			},
			wantErr: false,
			wantTXs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Deposits,
				},
				TX{
					ID:            "id2",
					FiscalCateg:   Deposits,
					ModifiedCateg: CommercialRebates,
				},
			},
		},
		{
			name: "Change Deposit to Referrals",
			txs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Deposits,
				},
				TX{
					ID:          "id2",
					FiscalCateg: Deposits,
				},
			},
			args: args{
				id:    "id2",
				categ: Referrals,
			},
			wantErr: false,
			wantTXs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Deposits,
				},
				TX{
					ID:            "id2",
					FiscalCateg:   Deposits,
					ModifiedCateg: Referrals,
				},
			},
		},
		{
			name: "Change Deposit to Forks",
			txs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Deposits,
				},
				TX{
					ID:          "id2",
					FiscalCateg: Deposits,
				},
			},
			args: args{
				id:    "id2",
				categ: Forks,
			},
			wantErr: false,
			wantTXs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Deposits,
				},
				TX{
					ID:            "id2",
					FiscalCateg:   Deposits,
					ModifiedCateg: Forks,
				},
			},
		},
		{
			name: "Change Deposit to Minings",
			txs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Deposits,
				},
				TX{
					ID:          "id2",
					FiscalCateg: Deposits,
				},
			},
			args: args{
				id:    "id2",
				categ: Minings,
			},
			wantErr: false,
			wantTXs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Deposits,
				},
				TX{
					ID:            "id2",
					FiscalCateg:   Deposits,
					ModifiedCateg: Minings,
				},
			},
		},
		{
			name: "Change Deposit to Transfers",
			txs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Deposits,
				},
				TX{
					ID:          "id2",
					FiscalCateg: Deposits,
				},
			},
			args: args{
				id:    "id2",
				categ: Transfers,
			},
			wantErr: true,
			wantTXs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Deposits,
				},
				TX{
					ID:          "id2",
					FiscalCateg: Deposits,
				},
			},
		},
		{
			name: "Change Deposit to DontCare",
			txs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Deposits,
				},
				TX{
					ID:          "id2",
					FiscalCateg: Deposits,
				},
			},
			args: args{
				id:    "id2",
				categ: DontCare,
			},
			wantErr: false,
			wantTXs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Deposits,
				},
				TX{
					ID:            "id2",
					FiscalCateg:   Deposits,
					ModifiedCateg: DontCare,
				},
			},
		},
		{
			name: "Change Deposit to Withdrawals",
			txs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Deposits,
				},
			},
			args: args{
				id:    "id1",
				categ: Withdrawals,
			},
			wantErr: true,
			wantTXs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Deposits,
				},
			},
		},
		{
			name: "Change Withdrawals to Gift",
			txs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Withdrawals,
				},
				TX{
					ID:          "id2",
					FiscalCateg: Withdrawals,
				},
			},
			args: args{
				id:    "id2",
				categ: Gifts,
			},
			wantErr: false,
			wantTXs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Withdrawals,
				},
				TX{
					ID:            "id2",
					FiscalCateg:   Withdrawals,
					ModifiedCateg: Gifts,
				},
			},
		},
		{
			name: "Change Withdrawals to CashOut",
			txs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Withdrawals,
				},
				TX{
					Timestamp:   time.Date(2021, time.September, 24, 16, 01, 01, 01, time.UTC),
					ID:          "id2",
					FiscalCateg: Withdrawals,
					Items: map[string]Values{
						"From": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(1, 0),
							},
						},
					},
				},
			},
			args: args{
				id:    "id2",
				categ: CashOut,
			},
			wantErr: false,
			wantTXs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Withdrawals,
				},
				TX{
					Timestamp:     time.Date(2021, time.September, 24, 16, 01, 01, 01, time.UTC),
					ID:            "id2",
					FiscalCateg:   Withdrawals,
					ModifiedCateg: CashOut,
					Items: map[string]Values{
						"From": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(1, 0),
							},
						},
						"To": {
							Value{
								Code:   "EUR",
								Amount: decimal.New(38302, 0),
							},
						},
					},
				},
			},
		},
		{
			name: "Change Withdrawals to Transfers",
			txs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Withdrawals,
				},
				TX{
					ID:          "id2",
					FiscalCateg: Withdrawals,
				},
			},
			args: args{
				id:    "id2",
				categ: Transfers,
			},
			wantErr: true,
			wantTXs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Withdrawals,
				},
				TX{
					ID:          "id2",
					FiscalCateg: Withdrawals,
				},
			},
		},
		{
			name: "Change Withdrawals to ICO",
			txs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Withdrawals,
				},
				TX{
					ID:          "id2",
					FiscalCateg: Withdrawals,
				},
			},
			args: args{
				id:    "id2",
				categ: ICO,
			},
			wantErr: false,
			wantTXs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Withdrawals,
				},
				TX{
					ID:            "id2",
					FiscalCateg:   Withdrawals,
					ModifiedCateg: ICO,
				},
			},
		},
		{
			name: "Change Withdrawals to DontCare",
			txs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Withdrawals,
				},
				TX{
					ID:          "id2",
					FiscalCateg: Withdrawals,
				},
			},
			args: args{
				id:    "id2",
				categ: DontCare,
			},
			wantErr: false,
			wantTXs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Withdrawals,
				},
				TX{
					ID:            "id2",
					FiscalCateg:   Withdrawals,
					ModifiedCateg: DontCare,
				},
			},
		},
		{
			name: "Change Withdrawals to Deposits",
			txs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Withdrawals,
				},
			},
			args: args{
				id:    "id1",
				categ: Deposits,
			},
			wantErr: true,
			wantTXs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Withdrawals,
				},
			},
		},
		{
			name: "Change CashOut",
			txs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: CashOut,
				},
			},
			args: args{
				id: "id1",
			},
			wantErr: true,
			wantTXs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: CashOut,
				},
			},
		},
		{
			name: "Not Found",
			txs:  TXs{},
			args: args{
				id: "id1",
			},
			wantErr: true,
			wantTXs: TXs{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.txs.SetCateg(tt.args.id, tt.args.categ, true); (err != nil) != tt.wantErr {
				t.Errorf("TXs.SetCateg() error = %v, wantErr %v", err, tt.wantErr)
			}
			if !cmp.Equal(tt.txs, tt.wantTXs) {
				t.Errorf("TXs.SetCateg() txs = %s, want %s", spew.Sdump(tt.txs), spew.Sdump(tt.wantTXs))
			}
		})
	}
}

// func TestTXs_Delete(t *testing.T) {
//  type args struct {
//    id string
//  }
//  tests := []struct {
//    name    string
//    txs     TXs
//    args    args
//    wantTXs TXs
//  }{
//    {
//      name: "Not Found",
//      txs: TXs{
//        TX{
//          ID: "id1",
//        },
//        TX{
//          ID: "id2",
//        },
//        TX{
//          ID: "id3",
//        },
//      },
//      args: args{
//        id: "unknown",
//      },
//      wantTXs: TXs{
//        TX{
//          ID: "id1",
//        },
//        TX{
//          ID: "id2",
//        },
//        TX{
//          ID: "id3",
//        },
//      },
//    },
//    {
//      name: "Delete First",
//      txs: TXs{
//        TX{
//          ID: "id1",
//        },
//        TX{
//          ID: "id2",
//        },
//        TX{
//          ID: "id3",
//        },
//      },
//      args: args{
//        id: "id1",
//      },
//      wantTXs: TXs{
//        TX{
//          ID: "id3",
//        },
//        TX{
//          ID: "id2",
//        },
//      },
//    },
//    {
//      name: "Delete Middle",
//      txs: TXs{
//        TX{
//          ID: "id1",
//        },
//        TX{
//          ID: "id2",
//        },
//        TX{
//          ID: "id3",
//        },
//      },
//      args: args{
//        id: "id2",
//      },
//      wantTXs: TXs{
//        TX{
//          ID: "id1",
//        },
//        TX{
//          ID: "id3",
//        },
//      },
//    },
//    {
//      name: "Delete Last",
//      txs: TXs{
//        TX{
//          ID: "id1",
//        },
//        TX{
//          ID: "id2",
//        },
//        TX{
//          ID: "id3",
//        },
//      },
//      args: args{
//        id: "id3",
//      },
//      wantTXs: TXs{
//        TX{
//          ID: "id1",
//        },
//        TX{
//          ID: "id2",
//        },
//      },
//    },
//  }
//  for _, tt := range tests {
//    t.Run(tt.name, func(t *testing.T) {
//      tt.txs.Delete(tt.args.id)
//      if !cmp.Equal(tt.txs, tt.wantTXs) {
//        t.Errorf("TXs.Delete() txs = %s, want %s", spew.Sdump(tt.txs), spew.Sdump(tt.wantTXs))
//      }
//    })
//  }
// }

func TestTXs_Merge(t *testing.T) {
	type args struct {
		idSrc string
		idDst string
	}
	tests := []struct {
		name    string
		txs     TXs
		args    args
		wantTXs TXs
		wantErr bool
	}{
		{
			name: "Not Found",
			txs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: ICO,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
					Note: "Note1",
				},
				TX{
					ID:          "id2",
					FiscalCateg: Deposits,
					Items: map[string]Values{
						"From": {
							Value{
								Code:   "EUR",
								Amount: decimal.New(1, 0),
							},
						},
					},
					Note: "Note2",
				},
			},
			args: args{
				idSrc: "id1",
				idDst: "id3",
			},
			wantTXs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: ICO,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
					Note: "Note1",
				},
				TX{
					ID:          "id2",
					FiscalCateg: Deposits,
					Items: map[string]Values{
						"From": {
							Value{
								Code:   "EUR",
								Amount: decimal.New(1, 0),
							},
						},
					},
					Note: "Note2",
				},
			},
			wantErr: true,
		},
		{
			name: "Merge id1 not ICO",
			txs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Withdrawals,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
					Note: "Note1",
				},
			},
			args: args{
				idSrc: "id1",
				idDst: "id2",
			},
			wantTXs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: Withdrawals,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
					Note: "Note1",
				},
			},
			wantErr: true,
		},
		{
			name: "Merge id2 not Deposits",
			txs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: ICO,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
					Note: "Note1",
				},
				TX{
					ID:          "id2",
					FiscalCateg: Withdrawals,
					Items: map[string]Values{
						"From": {
							Value{
								Code:   "EUR",
								Amount: decimal.New(1, 0),
							},
						},
					},
					Note: "Note2",
				},
			},
			args: args{
				idSrc: "id1",
				idDst: "id2",
			},
			wantTXs: TXs{
				TX{
					ID:          "id1",
					FiscalCateg: ICO,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
					Note: "Note1",
				},
				TX{
					ID:          "id2",
					FiscalCateg: Withdrawals,
					Items: map[string]Values{
						"From": {
							Value{
								Code:   "EUR",
								Amount: decimal.New(1, 0),
							},
						},
					},
					Note: "Note2",
				},
			},
			wantErr: true,
		},
		{
			name: "Merge 1 to 2",
			txs: TXs{
				TX{
					ID:            "id1",
					ModifiedCateg: ICO,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
					Note: "Note1",
				},
				TX{
					ID:          "id2",
					FiscalCateg: Deposits,
					Items: map[string]Values{
						"From": {
							Value{
								Code:   "EUR",
								Amount: decimal.New(1, 0),
							},
						},
					},
					Note: "Note2",
				},
			},
			args: args{
				idSrc: "id1",
				idDst: "id2",
			},
			wantTXs: TXs{
				TX{
					ID:            "id1",
					LinkedTxsIds:  []string{"id2"},
					ModifiedCateg: DontCare,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
					Note: "Note1",
				},
				TX{
					ID:            "id2",
					LinkedTxsIds:  []string{"id1"},
					FiscalCateg:   Deposits,
					ModifiedCateg: Exchanges,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
						"From": {
							Value{
								Code:   "EUR",
								Amount: decimal.New(1, 0),
							},
						},
					},
					Note: "[] Note1 => Note2",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.txs.Merge(tt.args.idSrc, tt.args.idDst); (err != nil) != tt.wantErr {
				t.Errorf("TXs.Merge() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !cmp.Equal(tt.txs, tt.wantTXs) {
				t.Errorf("TXs.Merge() txs = %s, want %s", spew.Sdump(tt.txs), spew.Sdump(tt.wantTXs))
			}
		})
	}
}

func TestTXs_Stocks(t *testing.T) {
	tests := []struct {
		name string
		txs  TXs
		want CoinsStocks
	}{
		{
			name: "Simple example",
			txs: TXs{
				TX{
					Wallet:      "Kraken",
					Timestamp:   time.Date(2021, time.September, 24, 16, 01, 01, 01, time.UTC),
					FiscalCateg: Exchanges,
					Items: map[string]Values{
						"From": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(5, 0),
							},
						},
						"To": {
							Value{
								Code:   "ETH",
								Amount: decimal.New(1, 2),
							},
						},
						"Fee": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(1, -2),
							},
						},
					},
					Note: "Note2",
				},
				TX{
					Wallet:      "Kraken",
					Timestamp:   time.Date(2021, time.September, 23, 16, 01, 01, 01, time.UTC),
					FiscalCateg: Deposits,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
					Note: "Note1",
				},
			},
			want: CoinsStocks{
				"BTC": []Operation{
					{
						Date:    time.Date(2021, time.September, 23, 16, 01, 01, 01, time.UTC),
						Name:    string(Deposits),
						Amount:  decimal.New(10, 0),
						Balance: decimal.New(10, 0),
						Note:    "Kraken : Note1",
					},
					{
						Date:    time.Date(2021, time.September, 24, 16, 01, 01, 01, time.UTC),
						Name:    string(Exchanges),
						Amount:  decimal.New(-501, -2),
						Balance: decimal.New(499, -2),
						Note:    "Kraken : Note2",
					},
				},
				"ETH": []Operation{
					{
						Date:    time.Date(2021, time.September, 24, 16, 01, 01, 01, time.UTC),
						Name:    string(Exchanges),
						Amount:  decimal.New(1, 2),
						Balance: decimal.New(1, 2),
						Note:    "Kraken : Note2",
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.txs.Stocks(); !cmp.Equal(got, tt.want) {
				t.Errorf("TXs.Stocks() = %v, want %v", spew.Sdump(got), spew.Sdump(tt.want))
			}
		})
	}
}

func TestTXs_Before(t *testing.T) {
	type args struct {
		date time.Time
	}
	tests := []struct {
		name string
		txs  TXs
		args args
		want TXs
	}{
		{
			name: "Simple",
			txs: TXs{
				TX{Timestamp: time.Date(2020, time.January, 1, 0, 0, 0, 0, time.UTC)},
				TX{Timestamp: time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC)},
				TX{Timestamp: time.Date(2020, time.January, 1, 0, 0, 0, 0, time.UTC)},
			},
			args: args{
				date: time.Date(2019, time.January, 12, 0, 0, 0, 0, time.UTC),
			},
			want: TXs{
				TX{Timestamp: time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC)},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.txs.Before(tt.args.date); !cmp.Equal(got, tt.want) {
				t.Errorf("TXs.Before() = %v, want %v", spew.Sdump(got), spew.Sdump(tt.want))
			}
		})
	}
}

func TestTXs_GetBalances(t *testing.T) {
	type args struct {
		includeFiat bool
	}
	tests := []struct {
		name string
		txs  TXs
		args args
		want Balances
	}{
		{
			name: "Simple",
			txs: TXs{
				{
					Items: map[string]Values{
						"Fee": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(1, 0),
							},
							Value{
								Code:   "BTC",
								Amount: decimal.New(2, 0),
							},
						},
						"From": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(5, 0),
							},
						},
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
				},
				{
					Items: map[string]Values{
						"Fee": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(1, 0),
							},
							Value{
								Code:   "BTC",
								Amount: decimal.New(2, 0),
							},
						},
						"From": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(5, 0),
							},
						},
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
				},
			},
			args: args{
				includeFiat: false,
			},
			want: Balances{
				"BTC": decimal.New(4, 0),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.txs.GetBalances(tt.args.includeFiat); !cmp.Equal(got, tt.want) {
				t.Errorf("TXs.GetBalances() = %v, want %v", spew.Sdump(got), spew.Sdump(tt.want))
			}
		})
	}
}

func TestTXs_InitialPTAFIFO(t *testing.T) {
	tests := []struct {
		name    string
		txs     TXs
		wantPta TotalBuyingPrice
		wantErr bool
	}{
		{
			name: "Negative Stock",
			txs: TXs{
				{
					Wallet:      "Kraken",
					Timestamp:   time.Date(2018, time.September, 24, 16, 01, 01, 01, time.UTC),
					FiscalCateg: Exchanges,
					Items: map[string]Values{
						"From": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(5, 0),
							},
						},
						"Fee": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(1, -2),
							},
						},
					},
					Note: "Note2",
				},
			},
			wantPta: TotalBuyingPrice{
				Acquisitions: map[string]BuyingPrice{},
			},
			wantErr: true,
		},
		{
			name: "Cashin only",
			txs: TXs{
				{
					Wallet:      "Kraken",
					Timestamp:   time.Date(2017, time.July, 17, 0, 0, 0, 0, time.UTC),
					FiscalCateg: CashIn,
					Items: map[string]Values{
						"From": {
							Value{
								Code:   "EUR",
								Amount: decimal.New(9005, 0),
							},
						},
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(5, 0),
							},
						},
					},
					Note: "Cashin 9005€",
				},
				{
					Wallet:      "Kraken",
					Timestamp:   time.Date(2017, time.December, 15, 0, 0, 0, 0, time.UTC),
					FiscalCateg: CashIn,
					Items: map[string]Values{
						"From": {
							Value{
								Code:   "EUR",
								Amount: decimal.New(16346, 0),
							},
						},
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(1, 0),
							},
						},
					},
					Note: "Cashin 16346€",
				},
			},
			wantPta: TotalBuyingPrice{
				Acquisitions: map[string]BuyingPrice{
					"BTC": {
						Transactions: []ValuedTX{
							{
								QtyToFind:   decimal.New(6, 0),
								QtyFound:    decimal.New(1, 0),
								QtyIn:       decimal.New(1, 0),
								QtyOut:      decimal.New(0, 0),
								NativeValue: decimal.New(16346, 0),
								PtaValue:    decimal.New(16346, 0),
								Taxable:     false,
								TX: TX{
									Wallet:      "Kraken",
									Timestamp:   time.Date(2017, time.December, 15, 0, 0, 0, 0, time.UTC),
									FiscalCateg: CashIn,
									Items: map[string]Values{
										"From": {
											Value{
												Code:   "EUR",
												Amount: decimal.New(16346, 0),
											},
										},
										"To": {
											Value{
												Code:   "BTC",
												Amount: decimal.New(1, 0),
											},
										},
									},
									Note: "Cashin 16346€",
								},
							},
							{
								QtyToFind:   decimal.New(5, 0),
								QtyFound:    decimal.New(5, 0),
								QtyIn:       decimal.New(5, 0),
								QtyOut:      decimal.New(0, 0),
								NativeValue: decimal.New(9005, 0),
								PtaValue:    decimal.New(9005, 0),
								Taxable:     false,
								TX: TX{
									Wallet:      "Kraken",
									Timestamp:   time.Date(2017, time.July, 17, 0, 0, 0, 0, time.UTC),
									FiscalCateg: CashIn,
									Items: map[string]Values{
										"From": {
											Value{
												Code:   "EUR",
												Amount: decimal.New(9005, 0),
											},
										},
										"To": {
											Value{
												Code:   "BTC",
												Amount: decimal.New(5, 0),
											},
										},
									},
									Note: "Cashin 9005€",
								},
							},
						},
						Montant: decimal.New(25351, 0),
					},
				},
				PrixTotalAcquisition: decimal.New(25351, 0),
			},
			wantErr: false,
		},
		{
			name: "All exchanged",
			txs: TXs{
				{
					Wallet:      "Kraken",
					Timestamp:   time.Date(2017, time.July, 17, 0, 0, 0, 0, time.UTC),
					FiscalCateg: CashIn,
					Items: map[string]Values{
						"From": {
							Value{
								Code:   "EUR",
								Amount: decimal.New(9005, 0),
							},
						},
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(5, 0),
							},
						},
					},
					Note: "Cashin 9005€",
				},
				{
					Wallet:      "Kraken",
					Timestamp:   time.Date(2017, time.December, 15, 0, 0, 0, 0, time.UTC),
					FiscalCateg: Exchanges,
					Items: map[string]Values{
						"From": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(5, 0),
							},
						},
						"To": {
							Value{
								Code:   "ETH",
								Amount: decimal.New(40, 0),
							},
						},
					},
					Note: "5 BTC -> 40 ETH",
				},
			},
			wantPta: TotalBuyingPrice{
				Acquisitions: map[string]BuyingPrice{
					"BTC": {
						Transactions: []ValuedTX{
							{
								QtyToFind:   decimal.New(0, 0),
								QtyFound:    decimal.New(0, 0),
								QtyIn:       decimal.New(0, 0),
								QtyOut:      decimal.New(5, 0),
								NativeValue: decimal.New(-76464462890625, -9),
								PtaValue:    decimal.New(0, 0),
								Taxable:     true,
								TX: TX{
									Wallet:      "Kraken",
									Timestamp:   time.Date(2017, time.December, 15, 0, 0, 0, 0, time.UTC),
									FiscalCateg: Exchanges,
									Items: map[string]Values{
										"From": {
											Value{
												Code:   "BTC",
												Amount: decimal.New(5, 0),
											},
										},
										"To": {
											Value{
												Code:   "ETH",
												Amount: decimal.New(40, 0),
											},
										},
									},
									Note: "5 BTC -> 40 ETH",
								},
							},
							{
								QtyToFind:   decimal.New(0, 0),
								QtyFound:    decimal.New(0, 0),
								QtyIn:       decimal.New(5, 0),
								QtyOut:      decimal.New(0, 0),
								NativeValue: decimal.New(9005, 0),
								PtaValue:    decimal.New(0, 0),
								Taxable:     false,
								TX: TX{
									Wallet:      "Kraken",
									Timestamp:   time.Date(2017, time.July, 17, 0, 0, 0, 0, time.UTC),
									FiscalCateg: CashIn,
									Items: map[string]Values{
										"From": {
											Value{
												Code:   "EUR",
												Amount: decimal.New(9005, 0),
											},
										},
										"To": {
											Value{
												Code:   "BTC",
												Amount: decimal.New(5, 0),
											},
										},
									},
									Note: "Cashin 9005€",
								},
							},
						},
						Montant: decimal.Zero,
					},
					"ETH": {
						Transactions: []ValuedTX{
							{
								QtyToFind:   decimal.New(40, 0),
								QtyFound:    decimal.New(40, 0),
								QtyIn:       decimal.New(40, 0),
								QtyOut:      decimal.New(0, 0),
								NativeValue: decimal.New(23582351074218752, -12),
								PtaValue:    decimal.New(23582351074218752, -12),
								Taxable:     false,
								TX: TX{
									Wallet:      "Kraken",
									Timestamp:   time.Date(2017, time.December, 15, 0, 0, 0, 0, time.UTC),
									FiscalCateg: Exchanges,
									Items: map[string]Values{
										"From": {
											Value{
												Code:   "BTC",
												Amount: decimal.New(5, 0),
											},
										},
										"To": {
											Value{
												Code:   "ETH",
												Amount: decimal.New(40, 0),
											},
										},
									},
									Note: "5 BTC -> 40 ETH",
								},
							},
						},
						Montant: decimal.New(23582351074218752, -12),
					},
				},
				PrixTotalAcquisition: decimal.New(23582351074218752, -12),
			},
			wantErr: false,
		},
		{
			name: "Simple",
			txs: TXs{
				{
					Wallet:      "Kraken",
					Timestamp:   time.Date(2018, time.September, 24, 16, 01, 01, 01, time.UTC),
					FiscalCateg: Exchanges,
					Items: map[string]Values{
						"From": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(5, 0),
							},
						},
						"To": {
							Value{
								Code:   "ETH",
								Amount: decimal.New(100, 0),
							},
						},
						"Fee": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(1, -2),
							},
						},
					},
					Note: "Note2",
				},
				{
					Wallet:      "Kraken",
					Timestamp:   time.Date(2017, time.September, 23, 16, 01, 01, 01, time.UTC),
					FiscalCateg: Deposits,
					Items: map[string]Values{
						"To": {
							Value{
								Code:   "BTC",
								Amount: decimal.New(10, 0),
							},
						},
					},
					Note: "Note1",
				},
			},
			wantPta: TotalBuyingPrice{
				Acquisitions: map[string]BuyingPrice{
					"BTC": {
						Transactions: []ValuedTX{
							{
								QtyToFind:   decimal.New(499, -2),
								QtyFound:    decimal.New(-501, -2),
								QtyIn:       decimal.New(0, 0),
								QtyOut:      decimal.New(501, -2),
								NativeValue: decimal.New(-2861724720703125, -11),
								PtaValue:    decimal.New(-154280723876953125, -13),
								Taxable:     true,
								TX: TX{
									Wallet:      "Kraken",
									Timestamp:   time.Date(2018, time.September, 24, 16, 01, 01, 01, time.UTC),
									FiscalCateg: Exchanges,
									Items: map[string]Values{
										"From": {
											Value{
												Code:   "BTC",
												Amount: decimal.New(5, 0),
											},
										},
										"To": {
											Value{
												Code:   "ETH",
												Amount: decimal.New(1, 2),
											},
										},
										"Fee": {
											Value{
												Code:   "BTC",
												Amount: decimal.New(1, -2),
											},
										},
									},
									Note: "Note2",
								},
							},
							{
								QtyToFind:   decimal.New(10, 0),
								QtyFound:    decimal.New(10, 0),
								QtyIn:       decimal.New(10, 0),
								QtyOut:      decimal.New(0, 0),
								NativeValue: decimal.New(307945556640625, -10),
								PtaValue:    decimal.New(307945556640625, -10),
								Taxable:     false,
								TX: TX{
									Wallet:      "Kraken",
									Timestamp:   time.Date(2017, time.September, 23, 16, 01, 01, 01, time.UTC),
									FiscalCateg: Deposits,
									Items: map[string]Values{
										"To": {
											Value{
												Code:   "BTC",
												Amount: decimal.New(10, 0),
											},
										},
									},
									Note: "Note1",
								},
							},
						},
						Montant: decimal.New(153664832763671875, -13),
					},
					"ETH": {
						Transactions: []ValuedTX{
							{
								QtyToFind:   decimal.New(100, 0),
								QtyFound:    decimal.New(100, 0),
								QtyIn:       decimal.New(100, 0),
								QtyOut:      decimal.New(0, 0),
								NativeValue: decimal.New(20834613037109375, -12),
								PtaValue:    decimal.New(20834613037109375, -12),
								Taxable:     false,
								TX: TX{
									Wallet:      "Kraken",
									Timestamp:   time.Date(2018, time.September, 24, 16, 01, 01, 01, time.UTC),
									FiscalCateg: Exchanges,
									Items: map[string]Values{
										"From": {
											Value{
												Code:   "BTC",
												Amount: decimal.New(5, 0),
											},
										},
										"To": {
											Value{
												Code:   "ETH",
												Amount: decimal.New(1, 2),
											},
										},
										"Fee": {
											Value{
												Code:   "BTC",
												Amount: decimal.New(1, -2),
											},
										},
									},
									Note: "Note2",
								},
							},
						},
						Montant: decimal.New(20834613037109375, -12),
					},
				},
				PrixTotalAcquisition: decimal.New(362010963134765625, -13),
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotPta, err := tt.txs.InitialPTAFIFO()
			if (err != nil) != tt.wantErr {
				t.Errorf("TXs.InitialPTAFIFO() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !cmp.Equal(gotPta, tt.wantPta) {
				t.Errorf("TXs.InitialPTAFIFO() = %v, want %v", spew.Sdump(gotPta), spew.Sdump(tt.wantPta))
			}
		})
	}
}

func TestCession_Calculate(t *testing.T) {
	tests := []struct {
		name string
		c    *Cession
	}{
		{
			name: "Simple",
			c:    &Cession{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.c.Calculate()
		})
	}
}

func TestTX_DifferentItemsCode(t *testing.T) {
	type fields struct {
		Items map[string]Values
	}
	type args struct {
		t TX
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Simple Same",
			fields: fields{
				Items: map[string]Values{
					"From": Values{
						Value{
							Code: "BTC",
						},
					},
				},
			},
			args: args{
				t: TX{
					Items: map[string]Values{
						"To": Values{
							Value{
								Code: "BTC",
							},
						},
					},
				},
			},
			want: false,
		},
		{
			name: "Simple Different",
			fields: fields{
				Items: map[string]Values{
					"From": Values{
						Value{
							Code: "BTC",
						},
					},
				},
			},
			args: args{
				t: TX{
					Items: map[string]Values{
						"To": Values{
							Value{
								Code: "ETH",
							},
						},
					},
				},
			},
			want: true,
		},
		{
			name: "Same Different",
			fields: fields{
				Items: map[string]Values{
					"From": Values{
						Value{
							Code: "BTC",
						},
					},
					"To": Values{
						Value{
							Code: "ETH",
						},
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tx := TX{
				Items: tt.fields.Items,
			}
			if got := tx.DifferentItemsCode(tt.args.t); got != tt.want {
				t.Errorf("TX.DifferentItemsCode() = %v, want %v", got, tt.want)
			}
		})
	}
}
