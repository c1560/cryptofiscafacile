package main

import (
	"errors"
	"log"
	"syscall/js"
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/hack-pad/go-indexeddb/idb"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

const databaseNameTxs = "wasm-db-txs"

var dbTXs *idb.Database
var mergedTX = map[string]string{}

func dbTXsUpgrader(ldb *idb.Database, oldVersion, newVersion uint) error {
	names, _ := ldb.ObjectStoreNames()
	alreadyHaveCateg := false
	alreadyHaveMerge := false
	alreadyHaveValue := false
	for _, name := range names {
		if name == "mod_categ" {
			alreadyHaveCateg = true
		}
		if name == "merge_txs" {
			alreadyHaveMerge = true
		}
		if name == "mod_value" {
			alreadyHaveValue = true
		}
	}
	if !alreadyHaveCateg {
		_, err := ldb.CreateObjectStore("mod_categ", idb.ObjectStoreOptions{})
		if err != nil {
			return err
		}
	}
	if !alreadyHaveMerge {
		_, err := ldb.CreateObjectStore("merge_txs", idb.ObjectStoreOptions{})
		if err != nil {
			return err
		}
	}
	if !alreadyHaveValue {
		_, err := ldb.CreateObjectStore("mod_value", idb.ObjectStoreOptions{})
		if err != nil {
			return err
		}
	}
	return nil
}

func initTXsDB() {
	// increment ver when you want to change the dbTXs structure
	ver := uint(2)
	openRequest, _ := idb.Global().Open(ctx, "wasm-db-txs", ver, dbTXsUpgrader)
	var err error
	dbTXs, err = openRequest.Await(ctx)
	if err != nil {
		return
	}
	loadTXsDB()
}

func loadTXsDB() {
	if dbTXs == nil {
		return
	}
	txnModCateg, err := dbTXs.Transaction(idb.TransactionReadOnly, "mod_categ")
	if err != nil {
		return
	}
	storeModCateg, err := txnModCateg.ObjectStore("mod_categ")
	if err != nil {
		return
	}
	countRequestModCateg, err := storeModCateg.Count()
	if err != nil {
		return
	}
	txsCountModCateg, err := countRequestModCateg.Await(ctx)
	if err != nil {
		return
	}
	if txsCountModCateg > 0 {
		cursorRequestModCateg, err := storeModCateg.OpenCursor(idb.CursorNext)
		if err != nil {
			return
		}
		cursorRequestModCateg.Iter(ctx, func(cursor *idb.CursorWithValue) error {
			txID, err := cursor.Key()
			if err != nil {
				return err
			}
			newCateg, err := cursor.Value()
			if err != nil {
				return err
			}
			allTXs.SetCateg(txID.String(), tx.FiscalCateg(newCateg.String()), false)
			return nil
		})
	}
	txnMerge, err := dbTXs.Transaction(idb.TransactionReadOnly, "merge_txs")
	if err != nil {
		return
	}
	storeMerge, err := txnMerge.ObjectStore("merge_txs")
	if err != nil {
		return
	}
	countRequestMerge, err := storeMerge.Count()
	if err != nil {
		return
	}
	txsCountMerge, err := countRequestMerge.Await(ctx)
	if err != nil {
		return
	}
	if txsCountMerge > 0 {
		cursorRequestMerge, err := storeMerge.OpenCursor(idb.CursorNext)
		if err != nil {
			return
		}
		cursorRequestMerge.Iter(ctx, func(cursor *idb.CursorWithValue) error {
			srcID, err := cursor.Key()
			if err != nil {
				log.Println("Cannot read tx ["+srcID.String()+"]", err)
				return err
			}
			dstID, err := cursor.Value()
			if err != nil {
				log.Println("Cannot read tx ["+dstID.String()+"]", err)
				return err
			}
			mergedTX[srcID.String()] = dstID.String()
			allTXs.Merge(srcID.String(), dstID.String())
			return nil
		})
	}
	txnModValue, err := dbTXs.Transaction(idb.TransactionReadOnly, "mod_value")
	if err != nil {
		return
	}
	storeModValue, err := txnModValue.ObjectStore("mod_value")
	if err != nil {
		return
	}
	countRequestModValue, err := storeModValue.Count()
	if err != nil {
		return
	}
	txsCountModValue, err := countRequestModValue.Await(ctx)
	if err != nil {
		return
	}
	if txsCountModValue > 0 {
		cursorRequestModValue, err := storeModValue.OpenCursor(idb.CursorNext)
		if err != nil {
			return
		}
		cursorRequestModValue.Iter(ctx, func(cursor *idb.CursorWithValue) error {
			txID, err := cursor.Key()
			if err != nil {
				return err
			}
			values, err := cursor.Value()
			if err != nil {
				return err
			}
			var newValues []float64
			if values.Type() == js.TypeObject {
				for i := 0; i < values.Length(); i++ {
					v := values.Index(i)
					newValues = append(newValues, v.Float())
				}
			}
			allTXs.SetValue(txID.String(), newValues)
			return nil
		})
	}
}

func getTransactions(this js.Value, args []js.Value) interface{} {
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		//reject := args[1]
		go func() {
			transactions := make([]interface{}, len(allTXs))
			ids := make(map[string]bool)
			for i, t := range allTXs {
				transac := make(map[string]interface{})
				transac["id"] = t.ID
				dateConstructor := js.Global().Get("Date")
				transac["date"] = dateConstructor.New(t.Timestamp.UnixMilli())
				// transac["date"] = t.Timestamp.Format("02/01/2006 15:04:05")
				transac["wallet"] = t.Wallet
				transac["original_category"] = fiscalCateg2French(t.FiscalCateg)
				transac["category"] = fiscalCateg2French(t.EffectiveCateg())
				tos := make([]interface{}, len(t.Items["To"]))
				for j, item := range t.Items["To"] {
					to := make(map[string]interface{})
					to["quantity"], _ = item.Amount.Float64()
					to["currency"] = item.Code
					tos[j] = to
				}
				transac["to"] = tos
				froms := make([]interface{}, len(t.Items["From"]))
				for j, item := range t.Items["From"] {
					from := make(map[string]interface{})
					from["quantity"], _ = item.Amount.Float64()
					from["currency"] = item.Code
					froms[j] = from
				}
				transac["from"] = froms
				fees := make([]interface{}, len(t.Items["Fee"]))
				for j, item := range t.Items["Fee"] {
					fee := make(map[string]interface{})
					fee["quantity"], _ = item.Amount.Float64()
					fee["currency"] = item.Code
					fees[j] = fee
				}
				transac["fee"] = fees
				transac["note"] = t.Note
				_, merged := mergedTX[t.ID]
				transac["merged"] = merged
				transactions[i] = transac
				if ids[t.ID] == true {
					(log.Println("Duplicate tx id", t))
				} else {
					ids[t.ID] = true
				}
			}
			if debug {
				log.Println("getTransactions():")
				spew.Dump(transactions)
			}
			resolve.Invoke(transactions)
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func checkConsistencyCategVsToFrom(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("checkConsistencyCategVsToFrom()")
		spew.Dump(args)
	}
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		// reject := args[1]
		go func() {
			failed := allTXs.CheckConsistancyCategVsToFrom()
			errors := make([]interface{}, len(failed))
			for i, f := range failed {
				err := make(map[string]interface{})
				err["id"] = f.TX.ID
				err["error"] = f.Error
				dateConstructor := js.Global().Get("Date")
				err["date"] = dateConstructor.New(f.TX.Timestamp.UnixMilli())
				err["wallet"] = f.TX.Wallet
				err["category"] = fiscalCateg2French(f.TX.EffectiveCateg())
				errors[i] = err
			}
			ret := make(map[string]interface{})
			ret["errors"] = errors
			resolve.Invoke(ret)
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func checkConsistencyNegativeBalance(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("checkConsistencyNegativeBalance()")
		spew.Dump(args)
	}
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		go func() {
			failed := allTXs.CheckConsistancyNegativeBalance(time.Now())
			errors := make([]interface{}, len(failed))
			for i, f := range failed {
				tx := make(map[string]interface{})
				dateConstructor := js.Global().Get("Date")
				tx["id"] = f.TX.ID
				tx["date"] = dateConstructor.New(f.TX.Timestamp.UnixMilli())
				tx["wallet"] = f.TX.Wallet
				tx["category"] = fiscalCateg2French(f.TX.EffectiveCateg())
				amounts := "->"
				for k, v := range f.TX.GetBalances(true, false) { // with Fiat, no Fee
					if v.IsNegative() {
						amounts = v.Neg().String() + " " + k + " " + amounts
					} else {
						amounts += " " + v.String() + " " + k
					}
				}
				tx["amounts"] = amounts
				tx["coin"] = "" // not used here
				related := make([]interface{}, len(f.Related))
				for j, r := range f.Related {
					rel := make(map[string]interface{})
					rel["id"] = r.ID
					rel["date"] = dateConstructor.New(r.Timestamp.UnixMilli())
					rel["wallet"] = r.Wallet
					rel["category"] = fiscalCateg2French(r.EffectiveCateg())
					amounts := "->"
					for k, v := range r.GetBalances(true, false) { // with Fiat, no Fee
						if v.IsNegative() {
							amounts = v.Neg().String() + " " + k + " " + amounts
						} else {
							amounts += " " + v.String() + " " + k
						}
					}
					rel["amounts"] = amounts
					rel["coin"] = "" // not used here
					related[j] = rel
				}
				err := make(map[string]interface{})
				err["error"] = f.Error
				err["tx"] = tx
				err["related"] = related
				errors[i] = err
			}
			ret := make(map[string]interface{})
			ret["errors"] = errors
			resolve.Invoke(ret)
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func checkConsistencyAlmostTransfer(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("checkConsistencyAlmostTransfer()")
		spew.Dump(args)
	}
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		go func() {
			almosts := allTXs.CheckConsistancyAlmostTransfer()
			errors := make([]interface{}, len(almosts))
			for i, a := range almosts {
				tx := make(map[string]interface{})
				dateConstructor := js.Global().Get("Date")
				tx["id"] = a.TX.ID
				tx["date"] = dateConstructor.New(a.TX.Timestamp.UnixMilli())
				tx["wallet"] = a.TX.Wallet
				tx["category"] = fiscalCateg2French(a.TX.EffectiveCateg())
				amounts := "->"
				coin := ""
				for k, v := range a.TX.GetBalances(true, false) { // with Fiat, no Fee
					if coin == "" {
						coin = k
					}
					if coin == k {
						if v.IsNegative() {
							amounts = v.Neg().String() + " " + k + " " + amounts
						} else {
							amounts += " " + v.String() + " " + k
						}
					}
				}
				tx["amounts"] = amounts
				tx["coin"] = coin
				related := make([]interface{}, len(a.Related))
				for j, r := range a.Related {
					rel := make(map[string]interface{})
					rel["id"] = r.ID
					rel["date"] = dateConstructor.New(r.Timestamp.UnixMilli())
					rel["wallet"] = r.Wallet
					rel["category"] = fiscalCateg2French(r.EffectiveCateg())
					amounts := "->"
					for k, v := range r.GetBalances(true, false) { // with Fiat, no Fee
						if v.IsNegative() {
							amounts = v.Neg().String() + " " + k + " " + amounts
						} else {
							amounts += " " + v.String() + " " + k
						}
					}
					rel["amounts"] = amounts
					rel["coin"] = coin
					related[j] = rel
				}
				err := make(map[string]interface{})
				err["error"] = a.Error
				err["tx"] = tx
				err["related"] = related
				errors[i] = err
			}
			ret := make(map[string]interface{})
			ret["errors"] = errors
			resolve.Invoke(ret)
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func setValue(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("setValue()")
		spew.Dump(args)
	}
	txID := args[0]
	values := args[1]
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			var newValues []float64
			if values.Type() == js.TypeObject {
				for i := 0; i < values.Length(); i++ {
					v := values.Index(i)
					newValues = append(newValues, v.Float())
				}
			}
			err := allTXs.SetValue(txID.String(), newValues)
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			// Save modifier in DB
			if dbTXs == nil {
				reject.Invoke(js.Global().Get("Error").New("wasm-db-txs not Initialized"))
				return
			}
			txn, err := dbTXs.Transaction(idb.TransactionReadWrite, "mod_value")
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			store, err := txn.ObjectStore("mod_value")
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			req, err := store.PutKey(txID, values)
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			_, err = req.Await(ctx)
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			resolve.Invoke()
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func setCategory(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("setCategory()")
		spew.Dump(args)
	}
	txID := args[0]
	newCateg := args[1]
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			newFiscalCateg, err := french2FiscalCateg(newCateg.String())
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			err = allTXs.SetCateg(txID.String(), newFiscalCateg, true)
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			// Save modifier in DB
			if dbTXs == nil {
				reject.Invoke(js.Global().Get("Error").New("wasm-db-txs not Initialized"))
				return
			}
			txn, err := dbTXs.Transaction(idb.TransactionReadWrite, "mod_categ")
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			store, err := txn.ObjectStore("mod_categ")
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			req, err := store.PutKey(txID, js.ValueOf(string(newFiscalCateg)))
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			_, err = req.Await(ctx)
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			resolve.Invoke()
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func revertCateg(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("revertCateg()")
		spew.Dump(args)
	}
	txID := args[0]
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			err := allTXs.SetCateg(txID.String(), tx.Void, true)
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			// Delete modifier in DB
			if dbTXs == nil {
				reject.Invoke(js.Global().Get("Error").New("wasm-db-txs not Initialized"))
				return
			}
			txn, err := dbTXs.Transaction(idb.TransactionReadWrite, "mod_categ")
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			store, err := txn.ObjectStore("mod_categ")
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			req, err := store.Delete(txID)
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			err = req.Await(ctx)
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			resolve.Invoke()
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func mergeTransactions(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("mergeTransactions()")
		spew.Dump(args)
	}
	srcID := args[0]
	dstID := args[1]
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			err := allTXs.Merge(srcID.String(), dstID.String())
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			// Save modifier in DB
			if dbTXs == nil {
				reject.Invoke(js.Global().Get("Error").New("wasm-db-txs not Initialized"))
				return
			}
			txn, err := dbTXs.Transaction(idb.TransactionReadWrite, "merge_txs")
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			store, err := txn.ObjectStore("merge_txs")
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			req, err := store.PutKey(srcID, dstID)
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			_, err = req.Await(ctx)
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
				return
			}
			mergedTX[srcID.String()] = dstID.String()
			resolve.Invoke()
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func unMergeTransactions(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("unMergeTransactions()")
		spew.Dump(args)
	}
	srcID := args[0]
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			if _, ok := mergedTX[srcID.String()]; ok {
				// Delete modifier in DB
				if dbTXs == nil {
					reject.Invoke(js.Global().Get("Error").New("wasm-db-txs not Initialized"))
					return
				}
				txn, err := dbTXs.Transaction(idb.TransactionReadWrite, "merge_txs")
				if err != nil {
					reject.Invoke(js.Global().Get("Error").New(err.Error()))
					return
				}
				store, err := txn.ObjectStore("merge_txs")
				if err != nil {
					reject.Invoke(js.Global().Get("Error").New(err.Error()))
					return
				}
				req, err := store.Delete(srcID)
				if err != nil {
					reject.Invoke(js.Global().Get("Error").New(err.Error()))
					return
				}
				err = req.Await(ctx)
				if err != nil {
					reject.Invoke(js.Global().Get("Error").New(err.Error()))
					return
				}
				delete(mergedTX, srcID.String())
				statusTXs, _ = loadFilesTX()
				loadTXsDB()
				if allTXs.GetTXbyID(srcID.String()).EffectiveCateg() == tx.ICO {
					err := allTXs.SetCateg(srcID.String(), tx.Void, true)
					if err != nil {
						reject.Invoke(js.Global().Get("Error").New(err.Error()))
						return
					}
					// Delete modifier in DB
					if dbTXs == nil {
						reject.Invoke(js.Global().Get("Error").New("wasm-db-txs not Initialized"))
						return
					}
					txn, err := dbTXs.Transaction(idb.TransactionReadWrite, "mod_categ")
					if err != nil {
						reject.Invoke(js.Global().Get("Error").New(err.Error()))
						return
					}
					store, err := txn.ObjectStore("mod_categ")
					if err != nil {
						reject.Invoke(js.Global().Get("Error").New(err.Error()))
						return
					}
					req, err := store.Delete(srcID)
					if err != nil {
						reject.Invoke(js.Global().Get("Error").New(err.Error()))
						return
					}
					err = req.Await(ctx)
					if err != nil {
						reject.Invoke(js.Global().Get("Error").New(err.Error()))
						return
					}
				}
			}
			resolve.Invoke()
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func french2FiscalCateg(french string) (categ tx.FiscalCateg, err error) {
	switch french {
	case "Dépôt":
		categ = tx.Deposits
	case "Retrait":
		categ = tx.Withdrawals
	case "Frais":
		categ = tx.Fees
	case "Minage":
		categ = tx.Minings
	case "CashOut":
		categ = tx.CashOut
	case "CashIn":
		categ = tx.CashIn
	case "Echange":
		categ = tx.Exchanges
	case "Transfert":
		categ = tx.Transfers
	case "AirDrop":
		categ = tx.AirDrops
	case "Intérêt":
		categ = tx.Interests
	case "Remise Commerciale":
		categ = tx.CommercialRebates
	case "Parrainage":
		categ = tx.Referrals
	case "Fork":
		categ = tx.Forks
	case "Don":
		categ = tx.Gifts
	case "ICO":
		categ = tx.ICO
	case "Ignoré":
		categ = tx.DontCare
	default:
		err = errors.New("unknown French FiscalCategory")
	}
	return
}

func fiscalCateg2French(categ tx.FiscalCateg) (french string) {
	switch categ {
	case tx.Deposits:
		french = "Dépôt"
	case tx.Withdrawals:
		french = "Retrait"
	case tx.Fees:
		french = "Frais"
	case tx.Minings:
		french = "Minage"
	case tx.CashOut:
		french = "CashOut"
	case tx.CashIn:
		french = "CashIn"
	case tx.Exchanges:
		french = "Echange"
	case tx.Transfers:
		french = "Transfert"
	case tx.AirDrops:
		french = "AirDrop"
	case tx.Interests:
		french = "Intérêt"
	case tx.CommercialRebates:
		french = "Remise Commerciale"
	case tx.Referrals:
		french = "Parrainage"
	case tx.Forks:
		french = "Fork"
	case tx.Gifts:
		french = "Don"
	case tx.ICO:
		french = "ICO"
	case tx.DontCare:
		french = "Ignoré"
	default:
		french = "Inconnu"
	}
	return
}
