package main

import (
	"syscall/js"
	"testing"
	"time"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/test"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

func TestComputeSynthEvolutionWrongDates(t *testing.T) {
	// Given
	dateConstructor := js.Global().Get("Date")
	start := dateConstructor.New()
	end := dateConstructor.New()
	var wallet js.Value

	// When
	_, err := computeSynthEvolution(start, end, wallet)

	// Then
	assert.EqualError(t, err, "start doit être au minimum 7 jours avant end")
}

func TestComputeSynthEvolutionEmptyTxShouldReturnEmptyArray(t *testing.T) {
	// Given
	dateConstructor := js.Global().Get("Date")
	start := dateConstructor.New(2023, 0, 1)
	end := dateConstructor.New(2023, 0, 10)
	var wallet js.Value

	// When
	evol, err := computeSynthEvolution(start, end, wallet)

	// Then
	assert.Nil(t, err)
	assert.Empty(t, evol)
}

func TestGetEmptyBalances(t *testing.T) {
	// Given
	var this js.Value
	var args []js.Value
	dateConstructor := js.Global().Get("Date")
	date := dateConstructor.New(2023, 0, 1)
	args = append(args, date)

	// When
	promise := getSynthBalances(this, args).(js.Value)

	// Then
	result, errResult := test.Await(promise)

	assert.Nil(t, errResult)
	assert.True(t, result[0].Index(0).IsUndefined())
}

func TestGetBalances(t *testing.T) {
	// Given
	var this js.Value
	var args []js.Value
	dateConstructor := js.Global().Get("Date")
	date := dateConstructor.New(2023, 0, 1)
	args = append(args, date)

	allTXs = append(allTXs, tx.TX{
		Timestamp: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
		Items: map[string]tx.Values{
			"To": []tx.Value{{Code: "BTC", Amount: decimal.New(1, 0)}}},
	})

	// When
	promise := getSynthBalances(this, args).(js.Value)

	// Then
	result, errResult := test.Await(promise)

	assert.Nil(t, errResult)
	assert.Equal(t, "BTC", result[0].Index(0).Get("coin").String())
	assert.Equal(t, 1, result[0].Index(0).Get("amount").Int())
}

func TestGetWalletBalances(t *testing.T) {
	// Given
	var this js.Value
	var args []js.Value
	dateConstructor := js.Global().Get("Date")
	date := dateConstructor.New(2023, 0, 1)
	args = append(args, date)
	args = append(args, js.ValueOf("wallet"))

	allTXs = append(allTXs, tx.TX{
		Timestamp: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
		Wallet:    "wallet",
		Items: map[string]tx.Values{
			"To": []tx.Value{{Code: "BTC", Amount: decimal.New(1, 0)}}},
	})

	// When
	promise := getSynthBalances(this, args).(js.Value)

	// Then
	result, errResult := test.Await(promise)

	assert.Nil(t, errResult)
	assert.Equal(t, "BTC", result[0].Index(0).Get("coin").String())
	assert.Equal(t, 1, result[0].Index(0).Get("amount").Int())
}

func TestGetWalletBalancesEmptyWallet(t *testing.T) {
	// Given
	var this js.Value
	var args []js.Value
	dateConstructor := js.Global().Get("Date")
	date := dateConstructor.New(2023, 0, 1)
	args = append(args, date)
	args = append(args, js.ValueOf("unknown"))

	allTXs = append(allTXs, tx.TX{
		Timestamp: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
		Wallet:    "wallet",
		Items: map[string]tx.Values{
			"To": []tx.Value{{Code: "BTC", Amount: decimal.New(1, 0)}}},
	})

	// When
	promise := getSynthBalances(this, args).(js.Value)

	// Then
	result, errResult := test.Await(promise)

	assert.Nil(t, errResult)
	assert.Equal(t, 0, result[0].Length())
}
