package main

import (
	"errors"
	"log"
	"math"
	"sort"
	"syscall/js"
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/utils/slices"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/rate"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

func getSynthBalances(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("getSynthBalances():")
		spew.Dump(args)
	}
	date := args[0]
	var wallet js.Value
	if len(args) == 2 {
		wallet = args[1]
	}
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			goDate := time.UnixMilli(int64(date.Call("getTime").Int()))
			var goWallet *string
			if !wallet.IsUndefined() {
				w := wallet.String()
				goWallet = &w
			}
			// set the date at the end of the day
			goDate = time.Date(goDate.Year(), goDate.Month(), goDate.Day(), 23, 59, 59, 999999999, goDate.Location())
			now := time.Now()
			startOfToday := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
			if goDate.After(startOfToday) {
				reject.Invoke(js.Global().Get("Error").New("date doit être au maximum à hier"))
			}

			var b tx.Balances
			if goWallet == nil {
				b = allTXs.GetBalancesAt(goDate, false) // without Fiat
			} else {
				b = allTXs.GetWalletBalancesAt(goDate, *goWallet)
			}
			// get sorted coins of balances
			coins := make([]string, 0, len(b))
			for coin := range b {
				coins = append(coins, coin)
			}
			sort.Strings(coins)
			// map to js array
			balances := make([]interface{}, len(b))
			for i, coin := range coins {
				balance := make(map[string]interface{})
				balance["coin"] = coin
				balance["amount"] = b[coin].RoundBank(16).InexactFloat64()
				balances[i] = balance
			}
			resolve.Invoke(balances)
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func getSynthEvolution(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("getSynthEvolution():")
		spew.Dump(args)
	}
	start := args[0]
	end := args[1]
	var wallet js.Value
	if len(args) == 3 {
		wallet = args[2]
	}
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			evolution, err := computeSynthEvolution(start, end, wallet)
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
			} else {
				if debug {
					spew.Dump(evolution)
				}
				resolve.Invoke(evolution)
			}
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func computeSynthEvolution(start js.Value, end js.Value, wallet js.Value) ([]interface{}, error) {
	// Date range from start date 00:00:00 User location to end date 23:59:59 User location
	startDate := time.UnixMilli(int64(start.Call("getTime").Int()))
	startDate = time.Date(startDate.Year(), startDate.Month(), startDate.Day(), 0, 0, 0, 0, startDate.Location())
	endDate := time.UnixMilli(int64(end.Call("getTime").Int()))
	endDate = time.Date(endDate.Year(), endDate.Month(), endDate.Day(), 23, 59, 59, 999999999, endDate.Location())
	if endDate.Sub(startDate).Hours() < 24*7 {
		return nil, errors.New("start doit être au minimum 7 jours avant end")
	}
	var goWallet *string
	if !wallet.IsUndefined() {
		w := wallet.String()
		goWallet = &w
	}

	daysBetweenDates := (endDate.Sub(startDate).Hours() / 24) + 1
	samples := int(math.Min(50, daysBetweenDates))
	interval := time.Duration((endDate.Unix()-startDate.Unix())/int64(samples-1)) * time.Second
	balancesAt := make(map[time.Time]tx.Balances)
	currentDate := endDate

	// Compute top coins + others
	if goWallet == nil {
		balancesAt[currentDate] = allTXs.GetBalancesAt(currentDate, false) // without Fiat
	} else {
		balancesAt[currentDate] = allTXs.GetWalletBalancesAt(currentDate, *goWallet)
	}
	coinEurValues := make(map[string]decimal.Decimal)
	for k, v := range balancesAt[currentDate] {
		coinEurValues[k] = coinEurValues[k].Add(v.Mul(rate.Get(k, currentDate, true)))
	}
	coins := make([]string, 0, len(coinEurValues))
	for coin := range coinEurValues {
		coins = append(coins, coin)
	}
	sort.SliceStable(coins, func(i, j int) bool {
		return coinEurValues[coins[i]].GreaterThan(coinEurValues[coins[j]])
	})
	topCoinsAmount := int(math.Min(7, float64(len(coins))))
	topCoins := make([]string, topCoinsAmount)
	for i := 0; i < topCoinsAmount; i++ {
		topCoins[i] = coins[i]
	}

	// compute sample dates
	dates := make([]time.Time, samples)
	dates[samples-1] = currentDate
	currentDate = currentDate.Add(-interval)
	for i := samples - 2; i >= 0; i-- {
		dates[i] = currentDate
		currentDate = currentDate.Add(-interval)
	}
	// fetch balances at the end of the day
	for i := samples - 2; i >= 0; i-- {
		dates[i] = time.Date(dates[i].Year(), dates[i].Month(), dates[i].Day(), 23, 59, 59, 999999999, dates[i].Location())
		if goWallet == nil {
			balancesAt[dates[i]] = allTXs.GetBalancesAt(dates[i], false) // without Fiat
		} else {
			balancesAt[dates[i]] = allTXs.GetWalletBalancesAt(dates[i], *goWallet)
		}
	}

	// compute coin values
	stocks := make([]interface{}, samples)
	for i, date := range dates {
		stock := make(map[string]interface{})
		stock["date"] = date.UnixMilli()
		for _, coin := range topCoins {
			if !balancesAt[date][coin].IsZero() {
				stock[coin] = balancesAt[date][coin].Mul(rate.Get(coin, date, true)).RoundBank(0).IntPart()
			} else {
				stock[coin] = 0
			}
		}
		othersValue := decimal.Zero
		for k, v := range balancesAt[date] {
			if !slices.ContainsString(topCoins, k) && !balancesAt[date][k].IsZero() {
				otherValue := v.Mul(rate.Get(k, date, true))
				othersValue = othersValue.Add(otherValue)
			}
		}
		stock["Autres"] = othersValue.RoundBank(0).IntPart()
		stocks[i] = stock
	}

	// clean Autres stock if all are zeros
	cleanAutres := true
	for _, stock := range stocks {
		if stock.(map[string]interface{})["Autres"] != int64(0) {
			cleanAutres = false
			break
		}
	}
	if cleanAutres {
		tmpStocks := make([]interface{}, 0, samples)
		for _, stock := range stocks {
			if len(stock.(map[string]interface{})) > 2 {
				delete(stock.(map[string]interface{}), "Autres")
				tmpStocks = append(tmpStocks, stock)
			}
		}
		stocks = tmpStocks
	}

	return stocks, nil
}

func getSynthPortfolio(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("getPortfolio():")
		spew.Dump(args)
	}
	date := args[0]
	var wallet js.Value
	if len(args) == 2 {
		wallet = args[1]
	}
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			goDate := time.UnixMilli(int64(date.Call("getTime").Int()))
			// set the date at the end of the day
			goDate = time.Date(goDate.Year(), goDate.Month(), goDate.Day(), 23, 59, 59, 999999999, goDate.Location())
			now := time.Now()
			startOfToday := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
			if goDate.After(startOfToday) {
				reject.Invoke(js.Global().Get("Error").New("date doit être au maximum à hier"))
			}
			var goWallet *string
			if !wallet.IsUndefined() {
				w := wallet.String()
				goWallet = &w
			}

			var balances tx.Balances
			if goWallet == nil {
				balances = allTXs.GetBalancesAt(goDate, false) // without Fiat
			} else {
				balances = allTXs.GetWalletBalancesAt(goDate, *goWallet)
			}
			coins := make([]interface{}, len(balances))
			j := 0
			for k, v := range balances {
				coin := make(map[string]interface{})
				coin["coin"] = k
				coin["amount"], _ = v.Float64()
				value := v.Mul(rate.Get(k, goDate, true))
				coin["value"] = value.RoundBank(0).IntPart()
				coins[j] = coin
				j++
			}
			resolve.Invoke(js.ValueOf(coins))
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func getSynthStatTotalValue(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("getSynthStatTotalValue():")
		spew.Dump(args)
	}
	date := args[0]
	var wallet js.Value
	if len(args) == 2 {
		wallet = args[1]
	}
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			goDate := time.UnixMilli(int64(date.Call("getTime").Int()))
			// set the date at the end of the day
			goDate = time.Date(goDate.Year(), goDate.Month(), goDate.Day(), 23, 59, 59, 999999999, goDate.Location())
			now := time.Now()
			startOfToday := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
			if goDate.After(startOfToday) {
				reject.Invoke(js.Global().Get("Error").New("date doit être au maximum à hier"))
			}
			var goWallet *string
			if !wallet.IsUndefined() {
				w := wallet.String()
				goWallet = &w
			}

			var b tx.Balances
			if goWallet == nil {
				b = allTXs.GetBalancesAt(goDate, false) // without Fiat
			} else {
				b = allTXs.GetWalletBalancesAt(goDate, *goWallet)
			}
			var totalValue decimal.Decimal
			for k, v := range b {
				if !v.IsZero() {
					value := v.Mul(rate.Get(k, goDate, true))
					totalValue = totalValue.Add(value)
				}
			}

			stats := make(map[string]interface{})
			stats["total_value"] = totalValue.RoundBank(0).IntPart()
			resolve.Invoke(js.ValueOf(stats))
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func getSynthStatsGainsLosses(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("getSynthStatsGainsLosses():")
		spew.Dump(args)
	}
	date := args[0]
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]
		go func() {
			goDate := time.UnixMilli(int64(date.Call("getTime").Int()))
			// set the date at the end of the day
			goDate = time.Date(goDate.Year(), goDate.Month(), goDate.Day(), 23, 59, 59, 999999999, goDate.Location())
			now := time.Now()
			startOfToday := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
			if goDate.After(startOfToday) {
				reject.Invoke(js.Global().Get("Error").New("date doit être au maximum à hier"))
			}

			c2086, err := compute2086(goDate)
			if err != nil {
				reject.Invoke(js.Global().Get("Error").New(err.Error()))
			}

			c2086Year := c2086.years[goDate.Year()]
			stats := make(map[string]interface{})
			stats["realized"] = c2086Year.RealizedPnl.RoundBank(0).IntPart()
			stats["unrealized"] = c2086Year.UnrealizedPnl.RoundBank(0).IntPart()
			stats["bnc"] = c2086Year.BncAmount.RoundBank(0).IntPart()
			resolve.Invoke(js.ValueOf(stats))
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func getSynthTxTypes(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("getSynthTxTypes():")
		spew.Dump(args)
	}
	start := args[0]
	end := args[1]
	var wallet js.Value
	if len(args) == 3 {
		wallet = args[2]
	}
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		go func() {
			// Date range from start date 00:00:00 User location to end date 23:59:59 User location
			startDate := time.UnixMilli(int64(start.Call("getTime").Int()))
			startDate = time.Date(startDate.Year(), startDate.Month(), startDate.Day(), 0, 0, 0, 0, startDate.Location())
			endDate := time.UnixMilli(int64(end.Call("getTime").Int()))
			endDate = time.Date(endDate.Year(), endDate.Month(), endDate.Day(), 23, 59, 59, 999999999, endDate.Location())
			var goWallet *string
			if !wallet.IsUndefined() {
				w := wallet.String()
				goWallet = &w
			}

			// TX Type section
			txCntCashIn := 0
			txCntCashOut := 0
			txCntExchanges := 0
			txCntMinings := 0
			txCntInterests := 0
			txCntCommercialRebates := 0
			txCntAirDrops := 0
			for _, t := range allTXs {
				if t.Timestamp.After(startDate) &&
					t.Timestamp.Before(endDate) &&
					(goWallet == nil || t.Wallet == *goWallet) {
					switch t.EffectiveCateg() {
					case tx.CashIn:
						txCntCashIn++
					case tx.CashOut:
						txCntCashOut++
					case tx.Exchanges:
						txCntExchanges++
					case tx.Minings:
						txCntMinings++
					case tx.Interests:
						txCntInterests++
					case tx.CommercialRebates:
						txCntCommercialRebates++
					case tx.AirDrops:
						txCntAirDrops++
					}
				}
			}
			txsCnt := make(map[string]interface{})
			txsCnt["txCntCashIn"] = txCntCashIn
			txsCnt["txCntCashOut"] = txCntCashOut
			txsCnt["txCntExchanges"] = txCntExchanges
			txsCnt["txCntMinings"] = txCntMinings
			txsCnt["txCntInterests"] = txCntInterests
			txsCnt["txCntCommercialRebates"] = txCntCommercialRebates
			txsCnt["txCntAirDrops"] = txCntAirDrops

			resolve.Invoke(js.ValueOf(txsCnt))
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}
