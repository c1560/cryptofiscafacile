package repositories

import (
	"context"
	"strconv"
	"syscall/js"
	"time"

	"github.com/hack-pad/go-indexeddb/idb"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain/entities"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain/enums"
)

type CoinListRepository interface {
	Find(ctx context.Context, txn *idb.Transaction, provider enums.RateProvider) (*entities.CoinList, error)
	Update(ctx context.Context, txn *idb.Transaction, coinList *entities.CoinList) error
}

func NewCoinListRepository() CoinListRepository {
	return &coinListRepository{}
}

type coinListRepository struct {
}

func (r *coinListRepository) Find(ctx context.Context, txn *idb.Transaction, provider enums.RateProvider) (*entities.CoinList, error) {
	applicationStore, err := txn.ObjectStore(domain.StoreCoinList)
	if err != nil {
		return nil, err
	}

	req, err := applicationStore.Get(js.ValueOf(provider.String()))
	if err != nil {
		return nil, err
	}

	value, err := req.Await(ctx)
	if err != nil || value.IsUndefined() {
		return nil, err
	}

	coins := make([]entities.Coin, value.Get("coins").Length())
	for i := 0; i < value.Get("coins").Length(); i++ {
		coins[i] = entities.Coin{
			ID:     value.Get("coins").Get(strconv.Itoa(i)).Get("id").String(),
			Symbol: value.Get("coins").Get(strconv.Itoa(i)).Get("symbol").String(),
			Name:   value.Get("coins").Get(strconv.Itoa(i)).Get("symbol").String(),
		}
	}

	lastUpdated := time.UnixMicro(int64(value.Get("lastUpdated").Int()))

	return &entities.CoinList{Provider: provider, LastUpdated: lastUpdated, Coins: coins}, err
}

func (r *coinListRepository) Update(ctx context.Context, txn *idb.Transaction, coinList *entities.CoinList) error {
	store, err := txn.ObjectStore(domain.StoreCoinList)
	if err != nil {
		return err
	}

	list := make(map[string]interface{})
	list["provider"] = coinList.Provider.String()
	list["lastUpdated"] = coinList.LastUpdated.UnixMicro()
	coins := make([]interface{}, len(coinList.Coins))
	list["coins"] = coins

	for i, c := range coinList.Coins {
		coin := make(map[string]interface{})
		coin["id"] = c.ID
		coin["symbol"] = c.Symbol
		coin["name"] = c.Name
		coins[i] = coin
	}

	req, err := store.PutKey(js.ValueOf(coinList.Provider.String()), js.ValueOf(list))
	if err != nil {
		return err
	}
	_, err = req.Await(ctx)
	return err
}
