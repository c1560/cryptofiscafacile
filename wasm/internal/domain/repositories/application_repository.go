package repositories

import (
	"context"
	"syscall/js"

	"github.com/hack-pad/go-indexeddb/idb"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain/entities"
)

type ApplicationRepository interface {
	Find(ctx context.Context, txn *idb.Transaction, key string) (*entities.ApplicationEntry, error)
	Create(ctx context.Context, txn *idb.Transaction, entry *entities.ApplicationEntry) error
	Update(ctx context.Context, txn *idb.Transaction, entry *entities.ApplicationEntry) error
}

func NewApplicationRepository() ApplicationRepository {
	return &applicationRepository{}
}

type applicationRepository struct {
}

func (r *applicationRepository) Find(ctx context.Context, txn *idb.Transaction, key string) (*entities.ApplicationEntry, error) {
	applicationStore, err := txn.ObjectStore("application")
	if err != nil {
		return nil, err
	}

	req, err := applicationStore.Get(js.ValueOf(key))
	if err != nil {
		return nil, err
	}

	value, err := req.Await(ctx)
	if err != nil || value.IsUndefined() {
		return nil, err
	}
	return &entities.ApplicationEntry{Key: key, Value: value.String()}, err
}

func (r *applicationRepository) Create(ctx context.Context, txn *idb.Transaction, entry *entities.ApplicationEntry) error {
	applicationStore, err := txn.ObjectStore("application")
	if err != nil {
		return err
	}

	req, err := applicationStore.AddKey(js.ValueOf(entry.Key), js.ValueOf(entry.Value))
	if err != nil {
		return err
	}
	return req.Await(ctx)
}

func (r *applicationRepository) Update(ctx context.Context, txn *idb.Transaction, entry *entities.ApplicationEntry) error {
	applicationStore, err := txn.ObjectStore("application")
	if err != nil {
		return err
	}

	req, err := applicationStore.PutKey(js.ValueOf(entry.Key), js.ValueOf(entry.Value))
	if err != nil {
		return err
	}
	_, err = req.Await(ctx)
	return err
}
