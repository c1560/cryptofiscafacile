package domain

import (
	"context"

	"github.com/hack-pad/go-indexeddb/idb"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/utils/slices"
)

const (
	DatabaseName     = "cff"
	StoreApplication = "application"
	StoreCoinList    = "coin-list"
)

func databaseUpgrader(ldb *idb.Database, oldVersion, newVersion uint) error {
	names, _ := ldb.ObjectStoreNames()

	// create object store application
	if !slices.ContainsString(names, StoreApplication) {
		_, err := ldb.CreateObjectStore(StoreApplication, idb.ObjectStoreOptions{})
		if err != nil {
			return err
		}
	}

	// create object store coin list
	if !slices.ContainsString(names, StoreCoinList) {
		_, err := ldb.CreateObjectStore(StoreCoinList, idb.ObjectStoreOptions{})
		if err != nil {
			return err
		}
	}

	return nil
}

func InitDatabase(ctx context.Context) (*idb.Database, error) {
	// increment ver when you want to change the cff database structure
	ver := uint(2)
	var err error
	openRequest, err := idb.Global().Open(ctx, DatabaseName, ver, databaseUpgrader)
	if err != nil {
		return nil, err
	}

	return openRequest.Await(ctx)
}
