package entities

import (
	"time"

	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain/enums"
)

type CoinList struct {
	Provider    enums.RateProvider
	LastUpdated time.Time
	Coins       []Coin
}
