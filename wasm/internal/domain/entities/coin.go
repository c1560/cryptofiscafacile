package entities

type Coin struct {
	ID     string
	Symbol string
	Name   string
}
