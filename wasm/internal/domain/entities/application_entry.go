package entities

type ApplicationEntry struct {
	Key   string
	Value string
}
