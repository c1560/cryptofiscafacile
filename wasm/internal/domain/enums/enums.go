package enums

type RateProvider string

const (
	RateProviderCoingecko RateProvider = "coingecko"
)

func (provider RateProvider) String() string {
	switch provider {
	case RateProviderCoingecko:
		return "coingecko"
	}
	return "unkown"
}
