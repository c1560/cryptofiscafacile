package coingecko

import (
	"encoding/json"
	"fmt"
	"net/url"
	"strconv"
	"time"

	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/external"
)

const baseURL = "https://digital-assets.fritzy.finance"

type CoinGeckoClient struct {
	restClient *external.RestClient
}

// NewClient create new client object
func NewCoinGeckoClient() *CoinGeckoClient {
	return &CoinGeckoClient{
		restClient: external.NewRestClient(time.Minute, 120),
	}
}

// API
// CoinsList /coins/list
func (c *CoinGeckoClient) CoinsList() (*CoinList, error) {
	url := fmt.Sprintf("%s/coins/list", baseURL)
	resp, err := c.restClient.MakeReqWeighted(url, 5)
	if err != nil {
		return nil, err
	}

	var data *CoinList
	err = json.Unmarshal(resp, &data)
	if err != nil {
		return nil, err
	}
	return data, nil
}

// CoinsID /coins/{id}
func (c *CoinGeckoClient) CoinsID(id string, localization bool, tickers bool, marketData bool, communityData bool, developerData bool, sparkline bool) (*CoinsID, error) {
	if len(id) == 0 {
		return nil, fmt.Errorf("id is required")
	}
	params := url.Values{}
	params.Add("localization", strconv.FormatBool(sparkline))
	params.Add("tickers", strconv.FormatBool(tickers))
	params.Add("market_data", strconv.FormatBool(marketData))
	params.Add("community_data", strconv.FormatBool(communityData))
	params.Add("developer_data", strconv.FormatBool(developerData))
	params.Add("sparkline", strconv.FormatBool(sparkline))
	url := fmt.Sprintf("%s/coins/%s?%s", baseURL, id, params.Encode())
	resp, err := c.restClient.MakeReq(url)
	if err != nil {
		return nil, err
	}

	var data *CoinsID
	err = json.Unmarshal(resp, &data)
	if err != nil {
		return nil, err
	}
	return data, nil
}

// CoinsIDMarketChart /coins/{id}/market_chart?vs_currency={usd, eur, jpy, etc.}&days={1,14,30,max}
func (c *CoinGeckoClient) CoinsIDMarketChart(id string) (*CoinsIDMarketChart, error) {
	if len(id) == 0 {
		return nil, fmt.Errorf("id is required")
	}

	params := url.Values{}
	params.Add("vs_currency", "EUR")
	params.Add("days", "max")

	url := fmt.Sprintf("%s/coins/%s/market_chart?%s", baseURL, id, params.Encode())
	resp, err := c.restClient.MakeReq(url)
	if err != nil {
		return nil, err
	}

	m := CoinsIDMarketChart{}
	err = json.Unmarshal(resp, &m)
	if err != nil {
		return &m, err
	}

	return &m, nil
}

// Ping /ping endpoint
func (c *CoinGeckoClient) Ping() (*Ping, error) {
	url := fmt.Sprintf("%s/ping", baseURL)
	resp, err := c.restClient.MakeReq(url)
	if err != nil {
		return nil, err
	}
	var data *Ping
	err = json.Unmarshal(resp, &data)
	if err != nil {
		return nil, err
	}
	return data, nil
}
