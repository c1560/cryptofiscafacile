package coingecko

// AllCurrencies map all currencies (USD, BTC) to float64
type AllCurrencies map[string]float64

// coinBaseStruct [private]
type coinBaseStruct struct {
	ID     string `json:"id"`
	Symbol string `json:"symbol"`
	Name   string `json:"name"`
}

// ChartItem
type ChartItem [2]float32

// CoinsListItem item in CoinList
type CoinsListItem struct {
	coinBaseStruct
}

// CoinList https://api.coingecko.com/api/v3/coins/list
type CoinList []CoinsListItem

// CoinsID https://api.coingecko.com/api/v3/coins/bitcoin
type CoinsID struct {
	coinBaseStruct
	BlockTimeInMin      int32               `json:"block_time_in_minutes"`
	Categories          []string            `json:"categories"`
	Localization        LocalizationItem    `json:"localization"`
	Description         DescriptionItem     `json:"description"`
	Links               *LinksItem          `json:"links"`
	Image               ImageItem           `json:"image"`
	CountryOrigin       string              `json:"country_origin"`
	GenesisDate         string              `json:"genesis_date"`
	MarketCapRank       uint16              `json:"market_cap_rank"`
	CoinGeckoRank       uint16              `json:"coingecko_rank"`
	CoinGeckoScore      float32             `json:"coingecko_score"`
	DeveloperScore      float32             `json:"developer_score"`
	CommunityScore      float32             `json:"community_score"`
	LiquidityScore      float32             `json:"liquidity_score"`
	PublicInterestScore float32             `json:"public_interest_score"`
	MarketData          *MarketDataItem     `json:"market_data"`
	CommunityData       *CommunityDataItem  `json:"community_data"`
	DeveloperData       *DeveloperDataItem  `json:"developer_data"`
	PublicInterestStats *PublicInterestItem `json:"public_interest_stats"`
	StatusUpdates       *[]StatusUpdateItem `json:"status_updates"`
	LastUpdated         string              `json:"last_updated"`
	Tickers             *[]TickerItem       `json:"tickers"`
}

// CoinsIDMarketChart https://api.coingecko.com/api/v3/coins/bitcoin/market_chart?vs_currency=usd&days=1
type CoinsIDMarketChart struct {
	coinBaseStruct
	Prices       *[]ChartItem `json:"prices"`
	MarketCaps   *[]ChartItem `json:"market_caps"`
	TotalVolumes *[]ChartItem `json:"total_volumes"`
}

// CommunityDataItem map all community data item
type CommunityDataItem struct {
	FacebookLikes            *uint        `json:"facebook_likes"`
	TwitterFollowers         *uint        `json:"twitter_followers"`
	RedditAveragePosts48h    *float64     `json:"reddit_average_posts_48h"`
	RedditAverageComments48h *float64     `json:"reddit_average_comments_48h"`
	RedditSubscribers        *uint        `json:"reddit_subscribers"`
	RedditAccountsActive48h  *interface{} `json:"reddit_accounts_active_48h"`
	TelegramChannelUserCount *uint        `json:"telegram_channel_user_count"`
}

// DescriptionItem map all description (in locale) into respective string
type DescriptionItem map[string]string

// DeveloperDataItem map all developer data item
type DeveloperDataItem struct {
	Forks              *uint `json:"forks"`
	Stars              *uint `json:"stars"`
	Subscribers        *uint `json:"subscribers"`
	TotalIssues        *uint `json:"total_issues"`
	ClosedIssues       *uint `json:"closed_issues"`
	PRMerged           *uint `json:"pull_requests_merged"`
	PRContributors     *uint `json:"pull_request_contributors"`
	CommitsCount4Weeks *uint `json:"commit_count_4_weeks"`
}

// ImageItem struct for all sizes of image
type ImageItem struct {
	Thumb string `json:"thumb"`
	Small string `json:"small"`
	Large string `json:"large"`
}

// LinksItem map all links
type LinksItem map[string]interface{}

// LocalizationItem map all locale (en, zh) into respective string
type LocalizationItem map[string]string

// MarketDataItem map all market data item
type MarketDataItem struct {
	CurrentPrice                           AllCurrencies     `json:"current_price"`
	ROI                                    *ROIItem          `json:"roi"`
	ATH                                    AllCurrencies     `json:"ath"`
	ATHChangePercentage                    AllCurrencies     `json:"ath_change_percentage"`
	ATHDate                                map[string]string `json:"ath_date"`
	ATL                                    AllCurrencies     `json:"atl"`
	ATLChangePercentage                    AllCurrencies     `json:"atl_change_percentage"`
	ATLDate                                map[string]string `json:"atl_date"`
	MarketCap                              AllCurrencies     `json:"market_cap"`
	MarketCapRank                          uint16            `json:"market_cap_rank"`
	TotalVolume                            AllCurrencies     `json:"total_volume"`
	High24                                 AllCurrencies     `json:"high_24h"`
	Low24                                  AllCurrencies     `json:"low_24h"`
	PriceChange24h                         float64           `json:"price_change_24h"`
	PriceChangePercentage24h               float64           `json:"price_change_percentage_24h"`
	PriceChangePercentage7d                float64           `json:"price_change_percentage_7d"`
	PriceChangePercentage14d               float64           `json:"price_change_percentage_14d"`
	PriceChangePercentage30d               float64           `json:"price_change_percentage_30d"`
	PriceChangePercentage60d               float64           `json:"price_change_percentage_60d"`
	PriceChangePercentage200d              float64           `json:"price_change_percentage_200d"`
	PriceChangePercentage1y                float64           `json:"price_change_percentage_1y"`
	MarketCapChange24h                     float64           `json:"market_cap_change_24h"`
	MarketCapChangePercentage24h           float64           `json:"market_cap_change_percentage_24h"`
	PriceChange24hInCurrency               AllCurrencies     `json:"price_change_24h_in_currency"`
	PriceChangePercentage1hInCurrency      AllCurrencies     `json:"price_change_percentage_1h_in_currency"`
	PriceChangePercentage24hInCurrency     AllCurrencies     `json:"price_change_percentage_24h_in_currency"`
	PriceChangePercentage7dInCurrency      AllCurrencies     `json:"price_change_percentage_7d_in_currency"`
	PriceChangePercentage14dInCurrency     AllCurrencies     `json:"price_change_percentage_14d_in_currency"`
	PriceChangePercentage30dInCurrency     AllCurrencies     `json:"price_change_percentage_30d_in_currency"`
	PriceChangePercentage60dInCurrency     AllCurrencies     `json:"price_change_percentage_60d_in_currency"`
	PriceChangePercentage200dInCurrency    AllCurrencies     `json:"price_change_percentage_200d_in_currency"`
	PriceChangePercentage1yInCurrency      AllCurrencies     `json:"price_change_percentage_1y_in_currency"`
	MarketCapChange24hInCurrency           AllCurrencies     `json:"market_cap_change_24h_in_currency"`
	MarketCapChangePercentage24hInCurrency AllCurrencies     `json:"market_cap_change_percentage_24h_in_currency"`
	TotalSupply                            *float64          `json:"total_supply"`
	CirculatingSupply                      float64           `json:"circulating_supply"`
	MaxSupply                              float64           `json:"max_supply"`
	Sparkline                              *SparklineItem    `json:"sparkline_7d"`
	LastUpdated                            string            `json:"last_updated"`
}

// Ping https://api.coingecko.com/api/v3/ping
type Ping struct {
	GeckoSays string `json:"gecko_says"`
}

// PublicInterestItem map all public interest item
type PublicInterestItem struct {
	AlexaRank   uint `json:"alexa_rank"`
	BingMatches uint `json:"bing_matches"`
}

// ROIItem ROI Item
type ROIItem struct {
	Times      float64 `json:"times"`
	Currency   string  `json:"currency"`
	Percentage float64 `json:"percentage"`
}

// SparklineItem for sparkline
type SparklineItem struct {
	Price []float64 `json:"price"`
}

// StatusUpdateItem for BEAM
type StatusUpdateItem struct {
	Description string `json:"description"`
	Category    string `json:"category"`
	CreatedAt   string `json:"created_at"`
	User        string `json:"user"`
	UserTitle   string `json:"user_title"`
	Pin         bool   `json:"pin"`
	Project     struct {
		coinBaseStruct
		Type  string    `json:"type"`
		Image ImageItem `json:"image"`
	} `json:"project"`
}

// TickerItem for ticker
type TickerItem struct {
	Base   string `json:"base"`
	Target string `json:"target"`
	Market struct {
		Name             string `json:"name"`
		Identifier       string `json:"identifier"`
		TradingIncentive bool   `json:"has_trading_incentive"`
	} `json:"market"`
	Last            float64            `json:"last"`
	ConvertedLast   map[string]float64 `json:"converted_last"`
	Volume          float64            `json:"volume"`
	ConvertedVolume map[string]float64 `json:"converted_volume"`
	Timestamp       string             `json:"timestamp"`
	IsAnomaly       bool               `json:"is_anomaly"`
	IsStale         bool               `json:"is_stale"`
	CoinID          string             `json:"coin_id"`
}
