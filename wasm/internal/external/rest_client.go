package external

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"golang.org/x/time/rate"
)

const cffWeightHeader = "CFF-WEIGHT"

type RestClient struct {
	httpClient *http.Client
}

// NewClient create new client object
func NewRestClient(limitPeriod time.Duration, requestCount int) *RestClient {
	httpClient := http.DefaultClient
	// Rate limiter : n requestCount by limitPeriod
	httpClient.Transport = NewThrottledTransport(limitPeriod, requestCount, http.DefaultTransport)
	return &RestClient{httpClient: httpClient}
}

// helper
// ThrottledTransport Rate Limited HTTP Client
type ThrottledTransport struct {
	roundTripperWrap http.RoundTripper
	ratelimiter      *rate.Limiter
}

func NewThrottledTransport(limitPeriod time.Duration, requestCount int, transportWrap http.RoundTripper) http.RoundTripper {
	return &ThrottledTransport{
		roundTripperWrap: transportWrap,
		ratelimiter:      rate.NewLimiter(rate.Every(limitPeriod/time.Duration(requestCount)), requestCount),
	}
}

func (c *ThrottledTransport) RoundTrip(r *http.Request) (*http.Response, error) {
	weight, _ := strconv.Atoi(r.Header.Get(cffWeightHeader))
	r.Header.Del(cffWeightHeader)
	err := c.ratelimiter.WaitN(r.Context(), weight) // This is a blocking call. Honors the rate limit
	if err != nil {
		return nil, err
	}
	return c.roundTripperWrap.RoundTrip(r)
}

// doReq HTTP client
func doReq(req *http.Request, client *http.Client) ([]byte, error) {
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != 200 {
		return nil, &RestClientError{
			StatusCode: resp.StatusCode,
			Err:        fmt.Errorf("%s", body),
		}
	}
	return body, nil
}

// Shorthand for MakeReqWeighted(url,1 )
func (c *RestClient) MakeReq(url string) ([]byte, error) {
	return c.MakeReqWeighted(url, 1)
}

func (c *RestClient) MakeReqWeighted(url string, weight int) ([]byte, error) {
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Add(cffWeightHeader, strconv.Itoa(weight))

	if err != nil {
		return nil, &RestClientError{
			StatusCode: 0,
			Err:        err,
		}
	}
	resp, err := doReq(req, c.httpClient)

	if err != nil {
		return nil, err
	}
	return resp, err
}

type RestClientError struct {
	StatusCode int

	Err error
}

func (r *RestClientError) Error() string {
	return fmt.Sprintf("status %d: err %v", r.StatusCode, r.Err)
}
