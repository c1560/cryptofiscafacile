package services

import (
	"context"
	"errors"
	"strings"

	"github.com/hack-pad/go-indexeddb/idb"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain/entities"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain/repositories"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/utils/slices"
)

const (
	keyAppVersion = "application.version"
)

var allowedAppEntryKeys = []string{keyAppVersion}

type ApplicationService interface {
	Get(ctx context.Context, key string) (*entities.ApplicationEntry, error)
	Create(ctx context.Context, entry *entities.ApplicationEntry) error
	Update(ctx context.Context, entry *entities.ApplicationEntry) error
}

type applicationService struct {
	allowedAppEntryKeys []string
	database            *idb.Database
	repository          repositories.ApplicationRepository
}

func NewApplicationService(database *idb.Database, repository repositories.ApplicationRepository) ApplicationService {
	return &applicationService{
		allowedAppEntryKeys: []string{keyAppVersion},
		database:            database,
		repository:          repository,
	}
}

func (s *applicationService) Get(ctx context.Context, key string) (*entities.ApplicationEntry, error) {
	txn, err := s.database.Transaction(idb.TransactionReadOnly, "application")
	if err != nil {
		return nil, err
	}

	return s.repository.Find(ctx, txn, key)
}

func (s *applicationService) Create(ctx context.Context, entry *entities.ApplicationEntry) error {
	if !slices.ContainsString(allowedAppEntryKeys, entry.Key) {
		return errors.New("ApplicationService.Create: Clé inconnu " + entry.Key)
	}

	filterApplicationVersion(entry)

	txn, err := s.database.Transaction(idb.TransactionReadWrite, "application")
	if err != nil {
		return err
	}

	return s.repository.Create(ctx, txn, entry)
}

func (s *applicationService) Update(ctx context.Context, entry *entities.ApplicationEntry) error {
	if !slices.ContainsString(allowedAppEntryKeys, entry.Key) {
		return errors.New("ApplicationService.Create: Clé inconnu " + entry.Key)
	}

	filterApplicationVersion(entry)

	txn, err := s.database.Transaction(idb.TransactionReadWrite, "application")
	if err != nil {
		return err
	}

	return s.repository.Update(ctx, txn, entry)
}

func filterApplicationVersion(entry *entities.ApplicationEntry) {
	if entry.Key == keyAppVersion {
		entry.Value = strings.Split(entry.Value, "+")[0]
	}
}
