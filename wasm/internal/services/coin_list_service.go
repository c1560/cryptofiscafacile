package services

import (
	"context"
	"time"

	"github.com/hack-pad/go-indexeddb/idb"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain/entities"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain/enums"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain/repositories"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/external/rates/coingecko"
)

type CoinListService interface {
	Get(ctx context.Context, provider enums.RateProvider) (*entities.CoinList, error)
}

type coinListService struct {
	database   *idb.Database
	repository repositories.CoinListRepository
	coingecko  *coingecko.CoinGeckoClient
}

func NewCoinListService(database *idb.Database, repository repositories.CoinListRepository, coingecko *coingecko.CoinGeckoClient) CoinListService {
	return &coinListService{
		database:   database,
		repository: repository,
		coingecko:  coingecko,
	}
}

func (s *coinListService) Get(ctx context.Context, provider enums.RateProvider) (*entities.CoinList, error) {
	txn, err := s.database.Transaction(idb.TransactionReadOnly, domain.StoreCoinList)
	if err != nil {
		return nil, err
	}

	coinList, err := s.repository.Find(ctx, txn, provider)
	if err != nil {
		return nil, err
	}

	// Update coin list if nil or too old
	currentTime := time.Now()
	if coinList == nil || coinList.LastUpdated.Before(currentTime.Add(time.Hour*-24)) {
		return s.updateListFromCoingecko(ctx)
	}

	return coinList, err
}

func (s *coinListService) updateListFromCoingecko(ctx context.Context) (*entities.CoinList, error) {
	coingeckoList, err := s.coingecko.CoinsList()
	if err != nil {
		return nil, err
	}
	var coins = make([]entities.Coin, len(*coingeckoList))
	for i, c := range *coingeckoList {
		coins[i] = entities.Coin{ID: c.ID, Symbol: c.Symbol, Name: c.Name}
	}
	coinList := &entities.CoinList{Provider: enums.RateProviderCoingecko, LastUpdated: time.Now(), Coins: coins}

	txn, err := s.database.Transaction(idb.TransactionReadWrite, domain.StoreCoinList)
	if err != nil {
		return nil, err
	}
	err = s.repository.Update(ctx, txn, coinList)
	if err != nil {
		return nil, err
	}

	return coinList, err
}
