package handlers

import (
	"context"
	"syscall/js"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain/entities"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain/repositories"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/services"
)

func TestCreateApplicationEntry(t *testing.T) {
	// Given
	ctx := context.Background()
	database, _ := domain.InitDatabase(ctx)
	repository := repositories.NewApplicationRepository()
	service := services.NewApplicationService(database, repository)
	handler := NewApplicationHandler(ctx, service)

	entry := make(map[string]interface{})
	entry["key"] = "application.version"
	entry["value"] = "1.0.0"

	// When
	err := handler.Create([]js.Value{js.ValueOf(entry)})

	// Then
	if err != nil {
		t.Error(err)
	}
	actualEntry, _ := service.Get(ctx, "application.version")
	expectedEntry := &entities.ApplicationEntry{Key: entry["key"].(string), Value: entry["value"].(string)}
	assert.Equal(t, actualEntry, expectedEntry)
}

func TestCreateApplicationEntryOnlyAllowed(t *testing.T) {
	// Given
	ctx := context.Background()
	database, _ := domain.InitDatabase(ctx)
	repository := repositories.NewApplicationRepository()
	service := services.NewApplicationService(database, repository)
	handler := NewApplicationHandler(ctx, service)

	entry := make(map[string]interface{})
	entry["key"] = "unknown"
	entry["value"] = "1.0.0"

	// When
	err := handler.Create([]js.Value{js.ValueOf(entry)})

	// Then
	assert.EqualError(t, err, "ApplicationService.Create: Clé inconnu unknown")
}

func TestGetApplicationEntry(t *testing.T) {
	// Given
	ctx := context.Background()
	database, _ := domain.InitDatabase(ctx)
	repository := repositories.NewApplicationRepository()
	service := services.NewApplicationService(database, repository)
	handler := NewApplicationHandler(ctx, service)

	entry := make(map[string]interface{})
	entry["key"] = "application.version"
	entry["value"] = "1.0.0"
	handler.Update([]js.Value{js.ValueOf(entry)})

	// When
	actualEntry, err := handler.Get([]js.Value{js.ValueOf("application.version")})

	// Then
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, actualEntry, entry)
}

func TestGetApplicationEntryUnknownEntry(t *testing.T) {
	// Given
	ctx := context.Background()
	database, _ := domain.InitDatabase(ctx)
	repository := repositories.NewApplicationRepository()
	service := services.NewApplicationService(database, repository)
	handler := NewApplicationHandler(ctx, service)

	// When
	actualEntry, err := handler.Get([]js.Value{js.ValueOf("unknown")})

	// Then
	if err != nil {
		t.Error(err)
	}
	assert.Nil(t, actualEntry)
}

func TestUpdateApplicationEntry(t *testing.T) {
	// Given
	ctx := context.Background()
	database, _ := domain.InitDatabase(ctx)
	repository := repositories.NewApplicationRepository()
	service := services.NewApplicationService(database, repository)
	handler := NewApplicationHandler(ctx, service)

	entry := make(map[string]interface{})
	entry["key"] = "application.version"
	entry["value"] = "1.0.0"

	// When
	err := handler.Update([]js.Value{js.ValueOf(entry)})

	// Then
	if err != nil {
		t.Error(err)
	}
	actualEntry, _ := service.Get(ctx, "application.version")
	expectedEntry := &entities.ApplicationEntry{Key: entry["key"].(string), Value: entry["value"].(string)}
	assert.Equal(t, actualEntry, expectedEntry)
}

func TestUpdateApplicationEntryOnlyAllowed(t *testing.T) {
	// Given
	ctx := context.Background()
	database, _ := domain.InitDatabase(ctx)
	repository := repositories.NewApplicationRepository()
	service := services.NewApplicationService(database, repository)
	handler := NewApplicationHandler(ctx, service)

	entry := make(map[string]interface{})
	entry["key"] = "unknown"
	entry["value"] = "1.0.0"

	// When
	err := handler.Update([]js.Value{js.ValueOf(entry)})

	// Then
	assert.EqualError(t, err, "ApplicationService.Create: Clé inconnu unknown")
}
