package handlers

import (
	"context"
	"syscall/js"

	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain/entities"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/services"
)

const (
	// exposed js methods
	createApplicationEntry = "createApplicationEntry"
	getApplicationEntry    = "getApplicationEntry"
	updateApplicationEntry = "updateApplicationEntry"
)

type ApplicationHandler interface {
	Create(args []js.Value) error
	Get(args []js.Value) (interface{}, error)
	Update(args []js.Value) error
}

type applicationHandler struct {
	ctx     context.Context
	service services.ApplicationService
}

func NewApplicationHandler(ctx context.Context, service services.ApplicationService) ApplicationHandler {
	applicationHandler := &applicationHandler{ctx: ctx, service: service}

	// register exposed methods
	js.Global().Set(createApplicationEntry, js.FuncOf(performCreateApplicationEntry(applicationHandler)))
	js.Global().Set(getApplicationEntry, js.FuncOf(performGetApplicationEntry(applicationHandler)))
	js.Global().Set(updateApplicationEntry, js.FuncOf(performUpdateApplicationEntry(applicationHandler)))

	return applicationHandler
}

func performCreateApplicationEntry(h ApplicationHandler) func(this js.Value, args []js.Value) any {
	return func(_ js.Value, args []js.Value) interface{} {
		handler := js.FuncOf(func(this js.Value, handlerArgs []js.Value) interface{} {
			resolve := handlerArgs[0]
			reject := handlerArgs[1]
			go func() {
				err := h.Create(args)

				if err != nil {
					reject.Invoke(js.Global().Get("Error").New(err.Error()))
				} else {
					resolve.Invoke()
				}
			}()
			return nil
		})
		promiseConstructor := js.Global().Get("Promise")
		return promiseConstructor.New(handler)
	}
}

func (h *applicationHandler) Create(args []js.Value) error {
	entry := js.ValueOf(args[0])
	return h.service.Create(h.ctx, &entities.ApplicationEntry{
		Key:   entry.Get("key").String(),
		Value: entry.Get("value").String()})

}

func performGetApplicationEntry(h ApplicationHandler) func(this js.Value, args []js.Value) any {
	return func(_ js.Value, args []js.Value) interface{} {
		handler := js.FuncOf(func(this js.Value, handlerArgs []js.Value) interface{} {
			resolve := handlerArgs[0]
			reject := handlerArgs[1]
			go func() {
				returnValue, err := h.Get(args)

				if err != nil {
					reject.Invoke(js.Global().Get("Error").New(err.Error()))
				} else {
					resolve.Invoke(returnValue)
				}
			}()
			return nil
		})
		promiseConstructor := js.Global().Get("Promise")
		return promiseConstructor.New(handler)
	}
}

func (h *applicationHandler) Get(args []js.Value) (interface{}, error) {
	persistedEntry, err := h.service.Get(h.ctx, js.ValueOf(args[0]).String())
	if err != nil || persistedEntry == nil {
		return nil, err
	}

	entry := make(map[string]interface{})
	entry["key"] = persistedEntry.Key
	entry["value"] = persistedEntry.Value
	return entry, err
}

func performUpdateApplicationEntry(h ApplicationHandler) func(this js.Value, args []js.Value) any {
	return func(_ js.Value, args []js.Value) interface{} {
		handler := js.FuncOf(func(this js.Value, handlerArgs []js.Value) interface{} {
			resolve := handlerArgs[0]
			reject := handlerArgs[1]
			go func() {
				err := h.Update(args)

				if err != nil {
					reject.Invoke(js.Global().Get("Error").New(err.Error()))
				} else {
					resolve.Invoke()
				}
			}()
			return nil
		})
		promiseConstructor := js.Global().Get("Promise")
		return promiseConstructor.New(handler)
	}
}

func (h *applicationHandler) Update(args []js.Value) error {
	entry := js.ValueOf(args[0])
	return h.service.Update(h.ctx, &entities.ApplicationEntry{
		Key:   entry.Get("key").String(),
		Value: entry.Get("value").String()})
}
