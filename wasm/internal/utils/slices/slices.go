package slices

import (
	"reflect"
)

type CompareFunc func(i int, first interface{}, second interface{}) bool // {}
type MapFunc func(i int, value interface{}) interface{}
type MapStringFunc func(i int, value string) string

var StrictEqualityCompare = func(_ int, first interface{}, second interface{}) bool {
	return first == second
}

// Return whether the given slice contains the given value.  If a comparator is provided, it will
// be used to compare the elements.
func Contains(in interface{}, value interface{}, comparators ...CompareFunc) bool {
	if len(comparators) == 0 {
		comparators = []CompareFunc{StrictEqualityCompare}
	}

	comparator := comparators[0]

	if inV := reflect.ValueOf(in); inV.IsValid() {
		for i := 0; i < inV.Len(); i++ {
			if current := inV.Index(i); current.IsValid() {
				if comparator(i, value, current.Interface()) {
					return true
				}
			}
		}
	}

	return false
}

// Returns whether the given string slice contains a given string.
func ContainsString(list []string, elem string) bool {
	for _, t := range list {
		if t == elem {
			return true
		}
	}

	return false
}

// Returns whether the given string slice contains all of the following strings.
func ContainsAllStrings(list []string, elems ...string) bool {
	for _, e := range elems {
		if !ContainsString(list, e) {
			return false
		}
	}

	return true
}

func Find[T any](list []T, predicate func(T) bool) *T {
	for _, el := range list {
		if predicate(el) {
			return &el
		}
	}

	return nil
}
