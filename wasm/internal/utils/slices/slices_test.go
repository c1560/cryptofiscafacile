package slices

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFind(t *testing.T) {
	// Given
	elts := []string{"1", "2", "3"}

	// When
	elt := Find(elts, func(s string) bool { return s == "2" })

	// Then
	assert.Equal(t, *elt, "2")
}
