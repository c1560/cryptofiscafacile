package indexeddb

import (
	"context"

	"github.com/hack-pad/go-indexeddb/idb"
)

func CleanDb(ctx context.Context, db *idb.Database) {
	names, _ := db.ObjectStoreNames()
	for _, name := range names {
		txn, _ := db.Transaction(idb.TransactionReadWrite, name)
		store, _ := txn.ObjectStore(name)
		clearRequest, _ := store.Clear()
		clearRequest.Await(ctx)
	}
}
