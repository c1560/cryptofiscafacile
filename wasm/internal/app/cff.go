package app

import (
	"context"
	"log"

	"github.com/hack-pad/go-indexeddb/idb"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain/entities"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain/enums"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain/repositories"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/external/rates/coingecko"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/handlers"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/services"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/utils/indexeddb"
)

type Cff interface {
	CleanDatabase()
	GetCoingeckoClient() *coingecko.CoinGeckoClient
	GetCoinList(provider enums.RateProvider) (*entities.CoinList, error)
}

type cff struct {
	ctx             context.Context
	database        *idb.Database
	coinListService services.CoinListService
	coingeckoClient *coingecko.CoinGeckoClient
}

func NewCff(ctx context.Context) Cff {
	database, err := domain.InitDatabase(ctx)
	if err != nil {
		log.Fatalln(err)
	}

	applicationRepository := repositories.NewApplicationRepository()
	applicationService := services.NewApplicationService(database, applicationRepository)
	handlers.NewApplicationHandler(ctx, applicationService)

	coingeckoClient := coingecko.NewCoinGeckoClient()
	coinListRepository := repositories.NewCoinListRepository()
	coinListService := services.NewCoinListService(database, coinListRepository, coingeckoClient)

	return &cff{ctx: ctx, database: database, coinListService: coinListService, coingeckoClient: coingeckoClient}
}

func (app *cff) CleanDatabase() {
	indexeddb.CleanDb(app.ctx, app.database)
}

func (app *cff) GetCoinList(provider enums.RateProvider) (*entities.CoinList, error) {
	return app.coinListService.Get(app.ctx, provider)
}

func (app *cff) GetCoingeckoClient() *coingecko.CoinGeckoClient {
	return app.coingeckoClient
}
