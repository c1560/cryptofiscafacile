module gitlab.com/c1560/CryptoFiscaFacile/wasm/csv/compat_tests

go 1.19

replace (
	github.com/fiscafacile/CryptoFiscaFacile => ../../../../../../github.com/fiscafacile/CryptoFiscaFacile
	gitlab.com/c1560/CryptoFiscaFacile/wasm/csv => ../
	gitlab.com/c1560/CryptoFiscaFacile/wasm/rate => ../../rate
	gitlab.com/c1560/CryptoFiscaFacile/wasm/tx => ../../tx
	gitlab.com/c1560/CryptoFiscaFacile/wasm/wallet => ../../wallet
)

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/fiscafacile/CryptoFiscaFacile v1.5.0-compat2
	gitlab.com/c1560/CryptoFiscaFacile/wasm/csv v0.0.0-00010101000000-000000000000
	gitlab.com/c1560/CryptoFiscaFacile/wasm/tx v0.0.0-00010101000000-000000000000
)

require (
	github.com/360EntSecGroup-Skylar/excelize v1.4.1 // indirect
	github.com/go-resty/resty/v2 v2.7.0 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/jcelliott/lumber v0.0.0-20160324203708-dd349441af25 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
	github.com/nanobox-io/golang-scribble v0.0.0-20190309225732-aa3e7c118975 // indirect
	github.com/shopspring/decimal v1.3.1 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/superoo7/go-gecko v1.0.0 // indirect
	gitlab.com/c1560/CryptoFiscaFacile/wasm/rate v0.0.0-00010101000000-000000000000 // indirect
	gitlab.com/c1560/CryptoFiscaFacile/wasm/wallet v0.0.0-00010101000000-000000000000 // indirect
	golang.org/x/net v0.0.0-20211029224645-99673261e6eb // indirect
	gopkg.in/resty.v1 v1.12.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
