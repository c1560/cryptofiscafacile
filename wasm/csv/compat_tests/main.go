package main

import (
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strconv"

	"github.com/davecgh/go-spew/spew"
	// CFFv1
	"github.com/fiscafacile/CryptoFiscaFacile/binance"
	"github.com/fiscafacile/CryptoFiscaFacile/bitfinex"
	"github.com/fiscafacile/CryptoFiscaFacile/bitstamp"
	"github.com/fiscafacile/CryptoFiscaFacile/bittrex"
	"github.com/fiscafacile/CryptoFiscaFacile/category"
	"github.com/fiscafacile/CryptoFiscaFacile/cfg"
	"github.com/fiscafacile/CryptoFiscaFacile/coinbase"
	"github.com/fiscafacile/CryptoFiscaFacile/coinbasepro"
	"github.com/fiscafacile/CryptoFiscaFacile/cryptocom"
	"github.com/fiscafacile/CryptoFiscaFacile/hitbtc"
	"github.com/fiscafacile/CryptoFiscaFacile/kraken"
	"github.com/fiscafacile/CryptoFiscaFacile/ledgerlive"
	"github.com/fiscafacile/CryptoFiscaFacile/localbitcoin"
	"github.com/fiscafacile/CryptoFiscaFacile/monero"
	"github.com/fiscafacile/CryptoFiscaFacile/poloniex"
	"github.com/fiscafacile/CryptoFiscaFacile/revolut"
	"github.com/fiscafacile/CryptoFiscaFacile/uphold"
	"github.com/fiscafacile/CryptoFiscaFacile/wallet"
	// CFFv2
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/csv"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

func displayCompare(str1, str2 string) {
	file1, err := ioutil.TempFile("temp", "CFFv1_")
	if err != nil {
		log.Fatal(err)
	}
	err = os.WriteFile(file1.Name(), []byte(str1), 0666)
	if err != nil {
		log.Fatal(err)
	}
	file2, err := ioutil.TempFile("temp", "CFFv2_")
	if err != nil {
		log.Fatal(err)
	}
	err = os.WriteFile(file2.Name(), []byte(str2), 0666)
	if err != nil {
		log.Fatal(err)
	}
	cmd := exec.Command("BComp.exe", file1.Name(), file2.Name())
	err = cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
	os.Remove(file1.Name())
	os.Remove(file2.Name())
}

func compare(txs1 wallet.TXsByCategory, txs2 tx.TXsByCategory) string {
	delete(txs2, tx.DontCare)
	if len(txs1) != len(txs2) {
		str1 := "txs1 :"
		for k1 := range txs1 {
			str1 += " " + k1
		}
		log.Println(str1)
		str2 := "txs2 :"
		for k2 := range txs2 {
			str2 += " " + string(k2)
		}
		log.Println(str2)
		displayCompare(spew.Sdump(txs1), spew.Sdump(txs2))
		return "Map length are differents"
	}
	for k1, v1 := range txs1 {
		if v2, ok := txs2[tx.FiscalCateg(k1)]; ok {
			if len(v1) != len(v2) {
				displayCompare(spew.Sdump(v1), spew.Sdump(v2))
				return k1 + " length are differents"
			}
			v1.SortByDate(true)
			v2.SortByDate(true)
			for i := range v1 {
				if !v1[i].Timestamp.Equal(v2[i].Timestamp) {
					displayCompare(spew.Sdump(v1[i]), spew.Sdump(v2[i]))
					return "Timestamp are differents"
				}
				// if v1[i].ID != v2[i].ID {
				// 	displayCompare(spew.Sdump(v1[i]), spew.Sdump(v2[i]))
				// 	return "ID are differents"
				// }
				if len(v1[i].Items) != len(v2[i].Items) {
					displayCompare(spew.Sdump(v1[i]), spew.Sdump(v2[i]))
					return "Items length are differents"
				}
				for ik, iv1 := range v1[i].Items {
					if iv2, ok := v2[i].Items[ik]; ok {
						if len(iv1) != len(iv2) {
							displayCompare(spew.Sdump(v1[i]), spew.Sdump(v2[i]))
							return ik + " length are differents"
						}
						for j := range iv1 {
							if v1[i].Items[ik][j].Code != v2[i].Items[ik][j].Code {
								displayCompare(spew.Sdump(v1[i]), spew.Sdump(v2[i]))
								return ik + "[" + strconv.Itoa(j) + "] Code are differents"
							}
							if !v1[i].Items[ik][j].Amount.Equal(v2[i].Items[ik][j].Amount) {
								displayCompare(spew.Sdump(v1[i]), spew.Sdump(v2[i]))
								return ik + "[" + strconv.Itoa(j) + "] Amount are differents"
							}
						}
					}
				}
			}
		} else {
			displayCompare(spew.Sdump(txs1), spew.Sdump(txs2))
			return k1 + " missing in CFFv2"
		}
	}
	return "OK"
}

func main() {
	// Configure CFFv2 with all defaults definitions
	defs := csv.GetDefaultDefinition()
	for _, def := range defs {
		csv.AddDefinition(def.Data)
	}
	// Configuration
	config, err := cfg.LoadConfig()
	if err != nil {
		log.Fatal(err)
	}
	categ := category.New()
	// Pas encore d'equivalent en CFFv2, on le desactivera quand dispo
	// if config.Options.TxsCategory != "" {
	// 	recordFile, err := os.Open(config.Options.TxsCategory)
	// 	if err != nil {
	// 		log.Fatal("Error opening Transactions CSV Category file:", err)
	// 	}
	// 	categ.ParseCSVCategory(recordFile)
	// }
	for _, file := range config.Exchanges.Binance.CSV.All {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		b := binance.New()
		err = b.ParseCSV(recordFile, config.Options.BinanceExtended, config.Exchanges.Binance.Account)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		// Set delisted coins balances to zero
		if len(config.Exchanges.Binance.DelistedCoins) > 0 {
			for _, dc := range config.Exchanges.Binance.DelistedCoins {
				b.TXsByCategory.RemoveDelistedCoins(dc)
			}
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(b.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Exchanges.Bitfinex.CSV.All {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		bf := bitfinex.New()
		err = bf.ParseCSV(recordFile, config.Exchanges.Bitfinex.Account)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		if len(config.Exchanges.Bitfinex.DelistedCoins) > 0 {
			for _, dc := range config.Exchanges.Bitfinex.DelistedCoins {
				bf.TXsByCategory.RemoveDelistedCoins(dc)
			}
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(bf.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Exchanges.Bitstamp.CSV.All {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		bs := bitstamp.New()
		err = bs.ParseCSV(recordFile, *categ, config.Options.Native, config.Exchanges.Bitstamp.Account)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		if len(config.Exchanges.Bitstamp.DelistedCoins) > 0 {
			for _, dc := range config.Exchanges.Bitstamp.DelistedCoins {
				bs.TXsByCategory.RemoveDelistedCoins(dc)
			}
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(bs.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Exchanges.Bittrex.CSV.All {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		btrx := bittrex.New()
		err = btrx.ParseCSV(recordFile, *categ, config.Exchanges.Bittrex.Account)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		if len(config.Exchanges.Bittrex.DelistedCoins) > 0 {
			for _, dc := range config.Exchanges.Bittrex.DelistedCoins {
				btrx.TXsByCategory.RemoveDelistedCoins(dc)
			}
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(btrx.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Exchanges.Coinbase.CSV.All {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		cb := coinbase.New()
		err = cb.ParseCSV(recordFile, *categ, config.Exchanges.Coinbase.Account)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		if len(config.Exchanges.Coinbase.DelistedCoins) > 0 {
			for _, dc := range config.Exchanges.Coinbase.DelistedCoins {
				cb.TXsByCategory.RemoveDelistedCoins(dc)
			}
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(cb.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Exchanges.CoinbasePro.CSV.Trades {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		cbp := coinbasepro.New()
		err = cbp.ParseFillsCSV(recordFile, config.Exchanges.CoinbasePro.Account)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		if len(config.Exchanges.CoinbasePro.DelistedCoins) > 0 {
			for _, dc := range config.Exchanges.CoinbasePro.DelistedCoins {
				cbp.TXsByCategory.RemoveDelistedCoins(dc)
			}
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(cbp.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Exchanges.CoinbasePro.CSV.Transfers {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		cbp := coinbasepro.New()
		err = cbp.ParseAccountCSV(recordFile, config.Exchanges.CoinbasePro.Account)
		if err != nil {
			log.Fatal("Error parsing Coinbase Pro Account CSV file:", err)
		}
		if len(config.Exchanges.CoinbasePro.DelistedCoins) > 0 {
			for _, dc := range config.Exchanges.CoinbasePro.DelistedCoins {
				cbp.TXsByCategory.RemoveDelistedCoins(dc)
			}
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(cbp.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Exchanges.CdcApp.CSV.All {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		cdc := cryptocom.New()
		err = cdc.ParseCSVAppCrypto(recordFile, *categ, config.Exchanges.CdcApp.Account)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		cdc.MergeTXs()
		if len(config.Exchanges.CdcApp.DelistedCoins) > 0 {
			for _, dc := range config.Exchanges.CdcApp.DelistedCoins {
				cdc.TXsByCategory.RemoveDelistedCoins(dc)
			}
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(cdc.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Exchanges.CdcEx.CSV.Transfers {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		cdc := cryptocom.New()
		err = cdc.ParseCSVExchangeTransfer(recordFile)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		cdc.MergeTXs()
		if len(config.Exchanges.CdcEx.DelistedCoins) > 0 {
			for _, dc := range config.Exchanges.CdcEx.DelistedCoins {
				cdc.TXsByCategory.RemoveDelistedCoins(dc)
			}
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(cdc.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Exchanges.CdcEx.CSV.Staking {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		cdc := cryptocom.New()
		err = cdc.ParseCSVExchangeStake(recordFile)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		cdc.MergeTXs()
		if len(config.Exchanges.CdcEx.DelistedCoins) > 0 {
			for _, dc := range config.Exchanges.CdcEx.DelistedCoins {
				cdc.TXsByCategory.RemoveDelistedCoins(dc)
			}
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(cdc.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Exchanges.CdcEx.CSV.Trades {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		cdc := cryptocom.New()
		err = cdc.ParseCSVExchangeSpotTrade(recordFile)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		cdc.MergeTXs()
		if len(config.Exchanges.CdcEx.DelistedCoins) > 0 {
			for _, dc := range config.Exchanges.CdcEx.DelistedCoins {
				cdc.TXsByCategory.RemoveDelistedCoins(dc)
			}
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(cdc.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Exchanges.CdcEx.CSV.Supercharger {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		cdc := cryptocom.New()
		err = cdc.ParseCSVExchangeSupercharger(recordFile)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		cdc.MergeTXs()
		if len(config.Exchanges.CdcEx.DelistedCoins) > 0 {
			for _, dc := range config.Exchanges.CdcEx.DelistedCoins {
				cdc.TXsByCategory.RemoveDelistedCoins(dc)
			}
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(cdc.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Exchanges.HitBTC.CSV.Trades {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		hb := hitbtc.New()
		err = hb.ParseCSVTrades(recordFile)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		if len(config.Exchanges.HitBTC.DelistedCoins) > 0 {
			for _, dc := range config.Exchanges.HitBTC.DelistedCoins {
				hb.TXsByCategory.RemoveDelistedCoins(dc)
			}
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(hb.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Exchanges.HitBTC.CSV.Transfers {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		hb := hitbtc.New()
		err = hb.ParseCSVTransactions(recordFile)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		if len(config.Exchanges.HitBTC.DelistedCoins) > 0 {
			for _, dc := range config.Exchanges.HitBTC.DelistedCoins {
				hb.TXsByCategory.RemoveDelistedCoins(dc)
			}
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(hb.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Exchanges.Kraken.CSV.All {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		kr := kraken.New()
		err = kr.ParseCSV(recordFile, *categ, config.Exchanges.Kraken.Account)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		if len(config.Exchanges.Kraken.DelistedCoins) > 0 {
			for _, dc := range config.Exchanges.Kraken.DelistedCoins {
				kr.TXsByCategory.RemoveDelistedCoins(dc)
			}
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(kr.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Wallets.LedgerLive.CSV.All {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		// Parse it with CFFv1
		ll := ledgerlive.New()
		err = ll.ParseCSV(recordFile, *categ)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		if err = recordFile.Close(); err != nil {
			log.Fatal(err)
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(ll.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Exchanges.LocalBitcoins.CSV.Trades {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		lb := localbitcoin.New()
		err = lb.ParseTradeCSV(recordFile, config.Exchanges.LocalBitcoins.Account)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(lb.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Exchanges.LocalBitcoins.CSV.Transfers {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		lb := localbitcoin.New()
		err = lb.ParseTransferCSV(recordFile, config.Exchanges.LocalBitcoins.Account)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(lb.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Wallets.Monero.CSV.All {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		xmr := monero.New()
		err = xmr.ParseCSV(recordFile, *categ)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(xmr.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Exchanges.Poloniex.CSV.Deposits {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		pl := poloniex.New()
		err = pl.ParseDepositsCSV(recordFile, config.Exchanges.Poloniex.Account)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		if len(config.Exchanges.Poloniex.DelistedCoins) > 0 {
			for _, dc := range config.Exchanges.Poloniex.DelistedCoins {
				pl.TXsByCategory.RemoveDelistedCoins(dc)
			}
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(pl.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Exchanges.Poloniex.CSV.Distributions {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		pl := poloniex.New()
		err = pl.ParseDistributionsCSV(recordFile, config.Exchanges.Poloniex.Account)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		if len(config.Exchanges.Poloniex.DelistedCoins) > 0 {
			for _, dc := range config.Exchanges.Poloniex.DelistedCoins {
				pl.TXsByCategory.RemoveDelistedCoins(dc)
			}
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(pl.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Exchanges.Poloniex.CSV.Trades {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		pl := poloniex.New()
		err = pl.ParseTradesCSV(recordFile, *categ, config.Exchanges.Poloniex.Account)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		if len(config.Exchanges.Poloniex.DelistedCoins) > 0 {
			for _, dc := range config.Exchanges.Poloniex.DelistedCoins {
				pl.TXsByCategory.RemoveDelistedCoins(dc)
			}
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(pl.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Exchanges.Poloniex.CSV.Withdrawals {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		pl := poloniex.New()
		err = pl.ParseWithdrawalsCSV(recordFile, *categ, config.Exchanges.Poloniex.Account)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		if len(config.Exchanges.Poloniex.DelistedCoins) > 0 {
			for _, dc := range config.Exchanges.Poloniex.DelistedCoins {
				pl.TXsByCategory.RemoveDelistedCoins(dc)
			}
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(pl.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Exchanges.Revolut.CSV.All {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		revo := revolut.New()
		err = revo.ParseCSV(recordFile, config.Exchanges.Revolut.Account)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(revo.TXsByCategory, txs.SortByCategory()))
	}
	for _, file := range config.Exchanges.Uphold.CSV.All {
		recordFile, err := os.Open(file)
		if err != nil {
			log.Fatal("Error opening", file, err)
		}
		defer func() {
			if err = recordFile.Close(); err != nil {
				log.Fatal(err)
			}
		}()
		// Parse it with CFFv1
		uh := uphold.New()
		err = uh.ParseCSV(recordFile, *categ, config.Exchanges.Uphold.Account)
		if err != nil {
			log.Println("Error parsing", file, err)
		}
		// Parse it with CFFv2
		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal("Error reading", file, err)
		}
		txs, _, _, err := csv.ParseTXs(data)
		if err != nil {
			log.Println("Error parsing CFFv2", file, err)
		}
		// Compare TXs
		log.Println("Compare", file, compare(uh.TXsByCategory, txs.SortByCategory()))
	}
}
