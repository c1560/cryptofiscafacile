package csv

import (
	"context"
	"os"
	"reflect"
	"testing"
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/google/go-cmp/cmp"
	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/app"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/rate"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

func TestMain(m *testing.M) {
	rate.FetchRates = false
	exitVal := m.Run()
	os.Exit(exitVal)
}

func TestValue_get(t *testing.T) {
	type args struct {
		strs []string
	}
	tests := []struct {
		name string
		v    Value
		args args
		want string
	}{
		{
			name: "Fix Name",
			v: Value{
				Name: "Fixed",
			},
			args: args{},
			want: "Fixed",
		},
		{
			name: "Index <= None",
			v:    Value{},
			args: args{},
			want: "",
		},
		{
			name: "Index > len(strs)",
			v: Value{
				Index: &[]int{0}[0],
			},
			args: args{
				strs: []string{},
			},
			want: "",
		},
		{
			name: "Simple Index",
			v: Value{
				Index: &[]int{1}[0],
			},
			args: args{
				strs: []string{"foo", "bar"},
			},
			want: "bar",
		},
		{
			name: "Simple Index with Replace",
			v: Value{
				Index:   &[]int{1}[0],
				Replace: []string{"a", "u"},
			},
			args: args{
				strs: []string{"foo", "bar"},
			},
			want: "bur",
		},
		{
			name: "Sub Index",
			v: Value{
				Index:     &[]int{1}[0],
				Separator: "a",
				SubIndex:  1,
			},
			args: args{
				strs: []string{"foo", "bar"},
			},
			want: "r",
		},
		{
			name: "Sub Index overflow",
			v: Value{
				Index:     &[]int{1}[0],
				Separator: "a",
				SubIndex:  2,
			},
			args: args{
				strs: []string{"foo", "bar"},
			},
			want: "bar",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.v.get(tt.args.strs, nil); got != tt.want {
				t.Errorf("Value.get() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestConditions_Check(t *testing.T) {
	type args struct {
		r []string
	}
	tests := []struct {
		name     string
		cs       Conditions
		args     args
		wantSkip bool
	}{
		{
			name:     "Default",
			wantSkip: false,
		},
		{
			name: "Single Found strict Found",
			cs: Conditions{
				{
					Name: Found,
					List: []string{"bur", "bor", "bar"},
					Value: Value{
						Index: &[]int{1}[0],
					},
					Strict: true,
				},
			},
			args: args{
				r: []string{"foo", "bar"},
			},
			wantSkip: false,
		},
		{
			name: "Single Found not Strict Found inverted Logic",
			cs: Conditions{
				{
					Name: Found,
					List: []string{"bar"},
					Value: Value{
						Index: &[]int{1}[0],
					},
					Logic: true,
				},
			},
			args: args{
				r: []string{"foo", "barracuda"},
			},
			wantSkip: true,
		},
		{
			name: "Single Same",
			cs: Conditions{
				{
					Name: Same,
					Value: Value{
						Index: &[]int{1}[0],
					},
					Ref: Value{
						Index: &[]int{0}[0],
					},
					Logic: true,
				},
			},
			args: args{
				r: []string{"bar", "bar"},
			},
			wantSkip: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotSkip := tt.cs.Check(valueGetter(tt.args.r, nil)); gotSkip != tt.wantSkip {
				t.Errorf("Conditions.Check() = %v, want %v", gotSkip, tt.wantSkip)
			}
		})
	}
}

func TestConditions_CheckDecimal(t *testing.T) {
	type args struct {
		r []string
		a decimal.Decimal
	}
	tests := []struct {
		name     string
		cs       Conditions
		args     args
		wantSkip bool
	}{
		{
			name:     "Default",
			wantSkip: false,
		},
		{
			name: "Single Positivity",
			cs: Conditions{
				{
					Name:  Positivity,
					Logic: true,
				},
			},
			args: args{
				a: decimal.New(-1, 1),
			},
			wantSkip: true,
		},
		{
			name: "Single Equality",
			cs: Conditions{
				{
					Name: Equality,
					Value: Value{
						Index: &[]int{0}[0],
					},
					Logic: true,
				},
			},
			args: args{
				r: []string{"-101"},
				a: decimal.New(-1, 1),
			},
			wantSkip: true,
		},
		{
			name: "Single Equality Error parsing",
			cs: Conditions{
				{
					Name: Equality,
					Value: Value{
						Index: &[]int{0}[0],
					},
				},
			},
			args: args{
				r: []string{"-a10"},
				a: decimal.New(-1, 1),
			},
			wantSkip: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotSkip := tt.cs.CheckDecimal(valueGetter(tt.args.r, nil), tt.args.a); gotSkip != tt.wantSkip {
				t.Errorf("Conditions.CheckDecimal() = %v, want %v", gotSkip, tt.wantSkip)
			}
		})
	}
}

func TestAddDefinition(t *testing.T) {
	type args struct {
		jsonData []byte
	}
	tests := []struct {
		name     string
		args     args
		wantName string
		wantErr  bool
	}{
		{
			name: "Void Def",
			args: args{
				jsonData: []byte("{}"),
			},
			wantErr: true,
		},
		{
			name: "Error Parsing",
			args: args{
				jsonData: []byte("{"),
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotName, err := AddDefinition(tt.args.jsonData)
			if (err != nil) != tt.wantErr {
				t.Errorf("AddDefinition() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotName != tt.wantName {
				t.Errorf("AddDefinition() = %v, want %v", gotName, tt.wantName)
			}
		})
	}
}

func TestParseTXs(t *testing.T) {
	rate.SetCff(app.NewCff(context.Background()))
	type args struct {
		jsonDefData []byte
		csvData     []byte
	}
	tests := []struct {
		name         string
		args         args
		wantTxs      tx.TXs
		wantSource   string
		wantUnknowns []string
		wantAddErr   bool
		wantErr      bool
	}{
		{
			name:       "Void",
			wantAddErr: true,
			wantErr:    true,
		},
		{
			name: "No Def so Not Found",
			args: args{
				csvData: []byte("a,b,c\nd,e,f"),
			},
			wantUnknowns: []string{"a,b,c\nd,e,f\n"},
			wantAddErr:   true,
			wantErr:      false,
		},
		{
			name: "Simple Found with Error Parsing CSV",
			args: args{
				jsonDefData: []byte("{\"Name\":\"Test\",\"Wallet\":\"Test\",\"Header\":[\"a\",\"b\",\"c\"]}"),
				csvData:     []byte("a,b,c\n1,2,3,4"),
			},
			wantSource: "Test",
			wantErr:    true,
		},
		{
			name: "Simple Kind Found",
			args: args{
				jsonDefData: []byte("{\"Name\":\"Test\",\"Wallet\":\"Test\",\"Header\":[\"a\",\"b\",\"c\"],\"TX\":[{\"Kinds\":[{\"Key\":{\"Index\":1},\"Categ\":{\"dont\":\"DontCare\"}}],\"Date\":{\"Index\":-1}}]}"),
				csvData:     []byte("a,b,c\n1,dont,3"),
			},
			wantSource: "Test",
			wantErr:    false,
		},
		{
			name: "Simple Kind Not Found",
			args: args{
				jsonDefData: []byte("{\"Name\":\"Test\",\"Wallet\":\"Test\",\"Header\":[\"a\",\"b\",\"c\"],\"TX\":[{\"Kinds\":[{\"Key\":{\"Index\":1},\"Categ\":{\"dont\":\"DontCare\"}}],\"Date\":{\"Index\":-1}}]}"),
				csvData:     []byte("a,b,c\nd,e,f"),
			},
			wantSource:   "Test",
			wantUnknowns: []string{"a,b,c\nd,e,f\n"},
			wantErr:      false,
		},
		{
			name: "Simple Date with Error Parsing",
			args: args{
				jsonDefData: []byte("{\"Name\":\"Test\",\"Wallet\":\"Test\",\"Header\":[\"a\",\"b\",\"c\"],\"TX\":[{\"Date\":{\"Index\":0,\"Format\":\"2006\"}}]}"),
				csvData:     []byte("a,b,c\nd,e,f"),
			},
			wantSource:   "Test",
			wantUnknowns: []string{"a,b,c\nd,e,f\n"},
			wantErr:      false,
		},
		{
			name: "Simple Date without Error",
			args: args{
				jsonDefData: []byte("{\"Name\":\"Test\",\"Wallet\":\"Test\",\"Header\":[\"a\",\"b\",\"c\"],\"TX\":[{\"Date\":{\"Index\":0,\"Format\":\"2006\"},\"Kinds\":[{\"Default\":\"Deposits\"}]}]}"),
				csvData:     []byte("a,b,c\n2021,2,3"),
			},
			wantSource: "Test",
			wantErr:    false,
		},
		{
			name: "Simple Epoch Date with Error Parsing",
			args: args{
				jsonDefData: []byte("{\"Name\":\"Test\",\"Wallet\":\"Test\",\"Header\":[\"a\",\"b\",\"c\"],\"TX\":[{\"Date\":{\"Index\":1,\"Format\":\"epoch\"},\"Kinds\":[{\"Default\":\"Deposits\"}]}]}"),
				csvData:     []byte("a,b,c\nd,e,f"),
			},
			wantSource:   "Test",
			wantUnknowns: []string{"a,b,c\nd,e,f\n"},
			wantErr:      false,
		},
		{
			name: "Simple Epoch Date without Error",
			args: args{
				jsonDefData: []byte("{\"Name\":\"Test\",\"Wallet\":\"Test\",\"Header\":[\"a\",\"b\",\"c\"],\"TX\":[{\"Date\":{\"Index\":1,\"Format\":\"epoch\"},\"Kinds\":[{\"Default\":\"Deposits\"}]}]}"),
				csvData:     []byte("a,b,c\n1,2,3"),
			},
			wantSource: "Test",
			wantErr:    false,
		},
		{
			name: "Simple ID",
			args: args{
				jsonDefData: []byte("{\"Name\":\"Test\",\"Wallet\":\"Test\",\"Header\":[\"a\",\"b\",\"c\"],\"TX\":[{\"ID\":{\"Indexes\":[1]},\"Date\":{\"Index\":-1},\"Kinds\":[{\"Default\":\"Deposits\"}]}]}"),
				csvData:     []byte("a,b,c\n1,some_id,3"),
			},
			wantSource: "Test",
			wantErr:    false,
		},
		{
			name: "Simple Item with Amount Parsing Error",
			args: args{
				jsonDefData: []byte("{\"Name\":\"Test\",\"Wallet\":\"Test\",\"Header\":[\"a\",\"b\",\"c\"],\"TX\":[{\"Kinds\":[{\"Default\":\"Deposits\"}],\"Items\":{\"Fee\":[{\"Amount\":[{\"Operation\":\"Add\",\"Value\":{\"Index\":0}}],\"Currency\":{\"Index\":1}}]},\"Date\":{\"Index\":-1}}]}"),
				csvData:     []byte("a,b,c\nd,BTC,f"),
			},
			wantSource:   "Test",
			wantUnknowns: []string{"a,b,c\nd,BTC,f\n"},
			wantErr:      false,
		},
		{
			name: "Simple Item Fee 1 BTC",
			args: args{
				jsonDefData: []byte("{\"Name\":\"Test\",\"Wallet\":\"Test\",\"Header\":[\"a\",\"b\",\"c\"],\"TX\":[{\"Kinds\":[{\"Default\":\"Deposits\"}],\"Items\":{\"Fee\":[{\"Amount\":[{\"Operation\":\"Add\",\"Value\":{\"Index\":0}}],\"Currency\":{\"Index\":1}}]},\"Date\":{\"Index\":-1}}]}"),
				csvData:     []byte("a,b,c\n1,BTC,3"),
			},
			wantTxs: tx.TXs{
				tx.TX{
					Timestamp:   time.Date(1, 1, 1, 0, 0, 0, 0, time.FixedZone("UTC", 0)),
					ID:          "55d603ac773d1b1621262f8bdb15b33cc3e3ef22f5475d43cefe404dfbad58be",
					Method:      "CSV",
					Wallet:      "Test",
					Source:      "Test CSV",
					FiscalCateg: tx.Deposits,
					Items: map[string]tx.Values{
						"Fee": {
							tx.Value{
								Amount: decimal.New(1, 0),
								Code:   "BTC",
							},
						},
					},
					NFTs: map[string]tx.NFTs{},
				},
			},
			wantSource: "Test",
			wantErr:    false,
		},
		{
			name: "Complex Item Fee 1 BTC",
			args: args{
				jsonDefData: []byte("{\"Name\":\"Test\",\"Wallet\":\"Test\",\"Header\":[\"a\",\"b\",\"c\"],\"TX\":[{\"Kinds\":[{\"Default\":\"Deposits\"}],\"Items\":{\"Fee\":[{\"Amount\":[{\"Operation\":\"Add\",\"Value\":{\"Index\":0}},{\"Operation\":\"Sub\",\"Value\":{\"Index\":2}},{\"Operation\":\"Mul\",\"Value\":{\"Index\":2}},{\"Operation\":\"Div\",\"Value\":{\"Index\":2}}],\"Currency\":{\"Index\":1}}]},\"Date\":{\"Index\":-1}}]}"),
				csvData:     []byte("a,b,c\n2,BTC,1"),
			},
			wantTxs: tx.TXs{
				tx.TX{
					Timestamp:   time.Date(1, 1, 1, 0, 0, 0, 0, time.FixedZone("UTC", 0)),
					ID:          "55d603ac773d1b1621262f8bdb15b33cc3e3ef22f5475d43cefe404dfbad58be",
					Method:      "CSV",
					Wallet:      "Test",
					Source:      "Test CSV",
					FiscalCateg: tx.Deposits,
					Items: map[string]tx.Values{
						"Fee": {
							tx.Value{
								Amount: decimal.New(1, 0),
								Code:   "BTC",
							},
						},
					},
					NFTs: map[string]tx.NFTs{},
				},
			},
			wantSource: "Test",
			wantErr:    false,
		},
		{
			name: "Simple Item Fee -1 BTC",
			args: args{
				jsonDefData: []byte("{\"Name\":\"Test\",\"Wallet\":\"Test\",\"Header\":[\"a\",\"b\",\"c\"],\"TX\":[{\"Kinds\":[{\"Default\":\"Deposits\"}],\"Items\":{\"Fee\":[{\"Amount\":[{\"Operation\":\"Add\",\"Value\":{\"Index\":0}}],\"Currency\":{\"Name\":\"BTC\"}}]},\"Date\":{\"Index\":-1}}]}"),
				csvData:     []byte("a,b,c\n-1,2,3"),
			},
			wantTxs: tx.TXs{
				tx.TX{
					Timestamp:   time.Date(1, 1, 1, 0, 0, 0, 0, time.FixedZone("UTC", 0)),
					ID:          "55d603ac773d1b1621262f8bdb15b33cc3e3ef22f5475d43cefe404dfbad58be",
					Method:      "CSV",
					Wallet:      "Test",
					Source:      "Test CSV",
					FiscalCateg: tx.Deposits,
					Items: map[string]tx.Values{
						"Fee": {
							tx.Value{
								Amount: decimal.New(1, 0),
								Code:   "BTC",
							},
						},
					},
					NFTs: map[string]tx.NFTs{},
				},
			},
			wantSource: "Test",
			wantErr:    false,
		},
		{
			name: "Simple Notes without Error",
			args: args{
				jsonDefData: []byte("{\"Name\":\"Test\",\"Wallet\":\"Test\",\"Header\":[\"a\",\"b\",\"c\"],\"TX\":[{\"Kinds\":[{\"Default\":\"Deposits\"}],\"Notes\":[{\"Index\":0},{\"Index\":1}],\"Date\":{\"Index\":-1}}]}"),
				csvData:     []byte("a,b,c\n1,2,3"),
			},
			wantSource: "Test",
			wantErr:    false,
		},
		{
			name: "Merged Item Fee 1 BTC x2",
			args: args{
				jsonDefData: []byte("{\"Name\":\"Test\",\"Wallet\":\"Test\",\"Header\":[\"a\",\"b\",\"c\"],\"TX\":[{\"Kinds\":[{\"Default\":\"Deposits\"}],\"ID\":{\"MergeBy\":true},\"Items\":{\"Fee\":[{\"Amount\":[{\"Operation\":\"Add\",\"Value\":{\"Index\":0}}],\"Currency\":{\"Index\":1}}]},\"Date\":{\"Index\":-1}}]}"),
				csvData:     []byte("a,b,c\n1,BTC,3\n1,BTC,3"),
			},
			wantTxs: tx.TXs{
				tx.TX{
					Timestamp:   time.Date(1, 1, 1, 0, 0, 0, 0, time.FixedZone("UTC", 0)),
					ID:          "55d603ac773d1b1621262f8bdb15b33cc3e3ef22f5475d43cefe404dfbad58be",
					Method:      "CSV",
					Wallet:      "Test",
					Source:      "Test CSV",
					FiscalCateg: tx.Deposits,
					Items: map[string]tx.Values{
						"Fee": {
							tx.Value{
								Amount: decimal.New(1, 0),
								Code:   "BTC",
							},
							tx.Value{
								Amount: decimal.New(1, 0),
								Code:   "BTC",
							},
						},
					},
					NFTs: map[string]tx.NFTs{},
					Note: "; ",
				},
			},
			wantSource: "Test",
			wantErr:    false,
		},
		{
			name: "Merged Item Exchanges into CashIn",
			args: args{
				jsonDefData: []byte("{\"Name\":\"Test\",\"Wallet\":\"Test\",\"Header\":[\"a\",\"b\",\"c\"],\"TX\":[{\"ID\":{\"MergeBy\":true},\"Kinds\":[{\"Default\": \"Exchanges\"}],\"Items\":{\"From\":[{\"Amount\":[{\"Operation\":\"Add\",\"Value\":{\"Index\":0}}],\"Currency\":{\"Index\":1},\"Skip\":[{\"Name\": \"Positivity\"}]}],\"To\":[{\"Amount\":[{\"Operation\":\"Add\",\"Value\":{\"Index\":0}}],\"Currency\":{\"Index\":1},\"Skip\":[{\"Name\": \"Positivity\",\"Logic\": true}]}]},\"Date\":{\"Index\":-1}}]}"),
				csvData:     []byte("a,b,c\n1,BTC,3\n-10,EUR,3"),
			},
			wantTxs: tx.TXs{
				tx.TX{
					Timestamp:   time.Date(1, 1, 1, 0, 0, 0, 0, time.FixedZone("UTC", 0)),
					ID:          "f74e799afca273dbf25bb9050808041cd309bd769d86c1cdf11a00702add5f7e",
					Method:      "CSV",
					Wallet:      "Test",
					Source:      "Test CSV",
					FiscalCateg: tx.CashIn,
					Items: map[string]tx.Values{
						"From": {
							tx.Value{
								Amount: decimal.New(10, 0),
								Code:   "EUR",
							},
						},
						"To": {
							tx.Value{
								Amount: decimal.New(1, 0),
								Code:   "BTC",
							},
						},
					},
					NFTs: map[string]tx.NFTs{},
					Note: "; ",
				},
			},
			wantSource: "Test",
			wantErr:    false,
		},
		{
			name: "Merged Item Exchanges into CashOut",
			args: args{
				jsonDefData: []byte("{\"Name\":\"Test\",\"Wallet\":\"Test\",\"Header\":[\"a\",\"b\",\"c\"],\"TX\":[{\"ID\":{\"MergeBy\":true},\"Kinds\":[{\"Default\": \"Exchanges\"}],\"Items\":{\"From\":[{\"Amount\":[{\"Operation\":\"Add\",\"Value\":{\"Index\":0}}],\"Currency\":{\"Index\":1},\"Skip\":[{\"Name\": \"Positivity\"}]}],\"To\":[{\"Amount\":[{\"Operation\":\"Add\",\"Value\":{\"Index\":0}}],\"Currency\":{\"Index\":1},\"Skip\":[{\"Name\": \"Positivity\",\"Logic\": true}]}]},\"Date\":{\"Index\":-1}}]}"),
				csvData:     []byte("a,b,c\n-1,BTC,3\n10,EUR,3"),
			},
			wantTxs: tx.TXs{
				tx.TX{
					Timestamp:   time.Date(1, 1, 1, 0, 0, 0, 0, time.FixedZone("UTC", 0)),
					ID:          "f74e799afca273dbf25bb9050808041cd309bd769d86c1cdf11a00702add5f7e",
					Method:      "CSV",
					Wallet:      "Test",
					Source:      "Test CSV",
					FiscalCateg: tx.CashOut,
					Items: map[string]tx.Values{
						"To": {
							tx.Value{
								Amount: decimal.New(10, 0),
								Code:   "EUR",
							},
						},
						"From": {
							tx.Value{
								Amount: decimal.New(1, 0),
								Code:   "BTC",
							},
						},
					},
					NFTs: map[string]tx.NFTs{},
					Note: "; ",
				},
			},
			wantSource: "Test",
			wantErr:    false,
		},
		{
			name: "Ignore Fiat Fee",
			args: args{
				jsonDefData: []byte("{\"Name\":\"Test\",\"Wallet\":\"Test\",\"Header\":[\"a\",\"b\",\"c\",\"d\"],\"TX\":[{\"Kinds\":[{\"Default\":\"Deposits\"}],\"Items\":{\"From\":[{\"Amount\":[{\"Operation\":\"Add\",\"Value\":{\"Index\":0}}],\"Currency\":{\"Index\":1}}],\"Fee\":[{\"Amount\":[{\"Operation\":\"Add\",\"Value\":{\"Index\":2}}],\"Currency\":{\"Index\":3}}]},\"Date\":{\"Index\":-1}}]}"),
				csvData:     []byte("a,b,c,d\n1,BTC,3,EUR"),
			},
			wantTxs: tx.TXs{
				tx.TX{
					Timestamp:   time.Date(1, 1, 1, 0, 0, 0, 0, time.FixedZone("UTC", 0)),
					ID:          "55d603ac773d1b1621262f8bdb15b33cc3e3ef22f5475d43cefe404dfbad58be",
					Method:      "CSV",
					Wallet:      "Test",
					Source:      "Test CSV",
					FiscalCateg: tx.Deposits,
					Items: map[string]tx.Values{
						"From": {
							tx.Value{
								Amount: decimal.New(1, 0),
								Code:   "BTC",
							},
						},
					},
					NFTs: map[string]tx.NFTs{},
				},
			},
			wantSource: "Test",
			wantErr:    false,
		},
		{
			name: "Ignore Fiat Deposit",
			args: args{
				jsonDefData: []byte("{\"Name\":\"Test\",\"Wallet\":\"Test\",\"Header\":[\"a\",\"b\",\"c\",\"d\"],\"TX\":[{\"Kinds\":[{\"Default\": \"Deposits\"}],\"Items\":{\"To\":[{\"Amount\":[{\"Operation\":\"Add\",\"Value\":{\"Index\":0}}],\"Currency\":{\"Index\":1}}],\"Fee\":[{\"Amount\":[{\"Operation\":\"Add\",\"Value\":{\"Index\":2}}],\"Currency\":{\"Index\":3}}]},\"Date\":{\"Index\":-1}}]}"),
				csvData:     []byte("a,b,c,d\n1,EUR,3,EUR"),
			},
			wantSource: "Test",
			wantErr:    false,
		},
		{
			name: "Ignore Fiat Withdrawal",
			args: args{
				jsonDefData: []byte("{\"Name\":\"Test\",\"Wallet\":\"Test\",\"Header\":[\"a\",\"b\",\"c\",\"d\"],\"TX\":[{\"Kinds\":[{\"Default\": \"Withdrawals\"}],\"Items\":{\"From\":[{\"Amount\":[{\"Operation\":\"Add\",\"Value\":{\"Index\":0}}],\"Currency\":{\"Index\":1}}],\"Fee\":[{\"Amount\":[{\"Operation\":\"Add\",\"Value\":{\"Index\":2}}],\"Currency\":{\"Index\":3}}]},\"Date\":{\"Index\":-1}}]}"),
				csvData:     []byte("a,b,c,d\n1,EUR,3,EUR"),
			},
			wantSource: "Test",
			wantErr:    false,
		},
		{
			name: "Detect CashIn",
			args: args{
				jsonDefData: []byte("{\"Name\":\"Test\",\"Wallet\":\"Test\",\"Header\":[\"a\",\"b\",\"c\",\"d\"],\"TX\":[{\"Kinds\":[{\"Default\": \"Exchanges\"}],\"Items\":{\"From\":[{\"Amount\":[{\"Operation\":\"Add\",\"Value\":{\"Index\":0}}],\"Currency\":{\"Index\":1}}],\"To\":[{\"Amount\":[{\"Operation\":\"Add\",\"Value\":{\"Index\":2}}],\"Currency\":{\"Index\":3}}]},\"Date\":{\"Index\":-1}}]}"),
				csvData:     []byte("a,b,c,d\n1,EUR,0.03,BTC"),
			},
			wantTxs: tx.TXs{
				tx.TX{
					Timestamp:   time.Date(1, 1, 1, 0, 0, 0, 0, time.FixedZone("UTC", 0)),
					ID:          "f74e799afca273dbf25bb9050808041cd309bd769d86c1cdf11a00702add5f7e",
					Method:      "CSV",
					Wallet:      "Test",
					Source:      "Test CSV",
					FiscalCateg: tx.CashIn,
					Items: map[string]tx.Values{
						"From": {
							tx.Value{
								Amount: decimal.New(1, 0),
								Code:   "EUR",
							},
						},
						"To": {
							tx.Value{
								Amount: decimal.New(3, -2),
								Code:   "BTC",
							},
						},
					},
					NFTs: map[string]tx.NFTs{},
				},
			},
			wantSource: "Test",
			wantErr:    false,
		},
		{
			name: "Detect CashOut",
			args: args{
				jsonDefData: []byte("{\"Name\":\"Test\",\"Wallet\":\"Test\",\"Header\":[\"a\",\"b\",\"c\",\"d\"],\"TX\":[{\"Kinds\":[{\"Default\": \"Exchanges\"}],\"Items\":{\"From\":[{\"Amount\":[{\"Operation\":\"Add\",\"Value\":{\"Index\":0}}],\"Currency\":{\"Index\":1}}],\"To\":[{\"Amount\":[{\"Operation\":\"Add\",\"Value\":{\"Index\":2}}],\"Currency\":{\"Index\":3}}]},\"Date\":{\"Index\":-1}}]}"),
				csvData:     []byte("a,b,c,d\n0.03,BTC,1,EUR"),
			},
			wantTxs: tx.TXs{
				tx.TX{
					Timestamp:   time.Date(1, 1, 1, 0, 0, 0, 0, time.FixedZone("UTC", 0)),
					ID:          "f74e799afca273dbf25bb9050808041cd309bd769d86c1cdf11a00702add5f7e",
					Method:      "CSV",
					Wallet:      "Test",
					Source:      "Test CSV",
					FiscalCateg: tx.CashOut,
					Items: map[string]tx.Values{
						"To": {
							tx.Value{
								Amount: decimal.New(1, 0),
								Code:   "EUR",
							},
						},
						"From": {
							tx.Value{
								Amount: decimal.New(3, -2),
								Code:   "BTC",
							},
						},
					},
					NFTs: map[string]tx.NFTs{},
				},
			},
			wantSource: "Test",
			wantErr:    false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ResetDefinition()
			if _, err := AddDefinition(tt.args.jsonDefData); (err != nil) != tt.wantAddErr {
				t.Errorf("AddDefinition() error = %v, wantErr %v", err, tt.wantAddErr)
			}
			gotTxs, gotSource, _, gotUnknowns, err := ParseTXs("file.csv", tt.args.csvData, "", nil)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseTXs() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotSource != tt.wantSource {
				t.Errorf("ParseTXs() gotSource = %v, want %v", gotSource, tt.wantSource)
			}
			if !cmp.Equal(gotUnknowns, tt.wantUnknowns) {
				t.Errorf("ParseTXs() gotUnknowns = %s, want %s", spew.Sdump(gotUnknowns), spew.Sdump(tt.wantUnknowns))
			}
			if !cmp.Equal(gotTxs, tt.wantTxs) {
				t.Errorf("ParseTXs() gotTxs = %s, want %s", spew.Sdump(gotTxs), spew.Sdump(tt.wantTxs))
			}
		})
	}
}

func Test_anonymize(t *testing.T) {
	type args struct {
		line []string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "Simple Interger Replace",
			args: args{
				line: []string{
					"132.5",
					"26456",
					"3654 564 654",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotAnonymized := anonymize(tt.args.line); reflect.DeepEqual(gotAnonymized, tt.args.line) {
				t.Errorf("anonymize() = %v, not anonymized", gotAnonymized)
			}
		})
	}
}
