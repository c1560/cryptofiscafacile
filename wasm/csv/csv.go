package csv

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"reflect"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"
	_ "time/tzdata"

	"github.com/gogs/chardet"
	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/utils/slices"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/rate"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
	"golang.org/x/text/encoding/unicode"
	"golang.org/x/text/transform"
)

const (
	// None : Field does not have any index
	None int = -1
)

// Value : Value Field
type Value struct {
	Name         string
	Index        *int
	Separator    string
	SubIndex     int
	Replace      []string
	ReplaceRegex []string
	Shift        int32
	// Captured group of the filename
	FilenameGroupName string
}

func (v Value) get(strs []string, filenameGroupValues map[string]string) string {
	if v.Name != "" {
		return v.Name
	}
	if v.Index != nil && *v.Index < len(strs) {
		val := strings.NewReplacer(v.Replace...).Replace(strs[*v.Index])
		if len(v.ReplaceRegex) > 0 && len(v.ReplaceRegex)%2 == 0 {
			for regexIndex := 0; regexIndex < len(v.ReplaceRegex); regexIndex = regexIndex + 2 {
				compiledRegexp := regexp.MustCompile(v.ReplaceRegex[regexIndex])
				val = compiledRegexp.ReplaceAllString(val, v.ReplaceRegex[regexIndex+1])
			}
		}
		if v.Separator == "" {
			return val
		}
		vals := strings.Split(val, v.Separator)
		if v.SubIndex > None && v.SubIndex < len(vals) {
			return vals[v.SubIndex]
		}
		return val
	}
	if v.FilenameGroupName != "nil" && filenameGroupValues != nil {
		return filenameGroupValues[v.FilenameGroupName]
	}
	return ""
}

var valueGetter = func(recordValues []string, filenameGroupValues map[string]string) func(v Value) string {
	return func(v Value) string {
		return v.get(recordValues, filenameGroupValues)
	}
}

// Date : Date Field
type Date struct {
	Index    int
	Format   string   `json:",omitempty"`
	Location string   `json:",omitempty"`
	Replace  []string `json:",omitempty"`
}

// ID : ID Field
type ID struct {
	Indexes []int
	MergeBy bool
	Text    string
	// Additional merge processors on line when parsing tx
	MergeProcessors []string
}

// ConditionName : Condition Name
type ConditionName string

const (
	// Equality : compare computed Amount to a Value according to Logic
	Equality ConditionName = "Equality"
	// Found : look for a Value in a List and compare to Logic
	Found ConditionName = "Found"
	// Positivity : check Amount positivity and compare to Logic
	Positivity ConditionName = "Positivity"
	// Same : check Ref/Value sameness and compare to Logic
	Same ConditionName = "Same"
)

// Condition : Generic condition
type Condition struct {
	Name   ConditionName
	Value  Value
	Ref    Value
	Logic  bool     `json:",omitempty"`
	Strict bool     `json:",omitempty"`
	List   []string `json:",omitempty"`
}

// Conditions : Condition List
type Conditions []Condition

// Check : Check all Standalone Conditions
func (cs Conditions) Check(valueGetter func(v Value) string) (skip bool) {
	for _, c := range cs {
		if c.Name == Found { // Check Found Condition
			if len(c.List) > 0 {
				found := false
				for _, tc := range c.List {
					if c.Strict {
						if valueGetter(c.Value) == tc {
							found = true
							break
						}
					} else {
						if strings.Contains(valueGetter(c.Value), tc) {
							found = true
							break
						}
					}
				}
				if found == c.Logic {
					skip = true
				}
			}
		} else if c.Name == Same { // Check Same Condition
			if (valueGetter(c.Ref) == valueGetter(c.Value)) == c.Logic {
				skip = true
			}
		} else if c.Name == Positivity { // Check Positivity Condition
			v, err := decimal.NewFromString(strings.ReplaceAll(valueGetter(c.Value), ",", "."))
			if err == nil {
				if c.Logic == v.IsNegative() {
					skip = true
				}
			}
		}
	}
	return skip
}

// CheckDecimal : Check all Conditions against a decimal
func (cs Conditions) CheckDecimal(valueGetter func(v Value) string, a decimal.Decimal) (skip bool) {
	for _, c := range cs {
		if c.Name == Positivity { // Check Positivity Condition
			if c.Logic == a.IsNegative() {
				skip = true
			}
		} else if c.Name == Equality { // Check Equality Condition
			ref, err := decimal.NewFromString(strings.ReplaceAll(valueGetter(c.Value), ",", "."))
			if err != nil {
				return true
			}
			ref = ref.Shift(c.Value.Shift)
			if c.Logic != ref.Equal(a) {
				skip = true
			}
		}
	}
	return skip
}

// Kind : Kind Field
type Kind struct {
	Key      Value
	Categ    map[string]tx.FiscalCateg `json:",omitempty"`
	Contains bool                      `json:",omitempty"`
	Skip     Conditions
	Default  tx.FiscalCateg
}

// Operation : math operation
type Operation string

const (
	// Add : Add
	Add Operation = "Add"
	// Sub : Subtract
	Sub Operation = "Sub"
	// Mul : Multiply
	Mul Operation = "Mul"
	// Div : Divide
	Div Operation = "Div"
)

// Formula : Math Formula using Operations
type Formula struct {
	Operation Operation
	Value     Value
}

// Item : Item
type Item struct {
	Amount   []Formula
	Currency Value
	Skip     Conditions
}

// Items : collection of Items
type Items []Item

// TX : a TX Definition
type TX struct {
	Date            Date
	ID              ID
	Kinds           []Kind
	Items           map[string]Items
	CurrencyReplace []string `json:",omitempty"`
	Notes           []Value  `json:",omitempty"`
}

// Source : CSV containing TXs
type Source struct {
	Name   string
	Wallet string
	// Extract information from filename. A regex is expected here to capture group in the filename.
	Filename string
	Header   []string
	TX       []TX
	Line     struct {
		// Is whole line between quotes
		Quoted       bool
		ReplaceRegex []string
		Separator    string
	}
	// Additional merge processors to perform after all tx are parsed
	MergeProcessors []string
}

var srcs []Source

// ResetDefinition : Forget all CSV Source definitions
func ResetDefinition() {
	srcs = []Source{}
}

// AddDefinition : Add a CSV Source definition
func AddDefinition(jsonData []byte) (name string, err error) {
	var src Source
	err = json.Unmarshal(jsonData, &src)
	if src.Name == "" || src.Wallet == "" || len(src.Header) == 0 { // Mandatory fields
		err = errors.New("JSON Format does not match")
	}
	if err == nil {
		srcs = append(srcs, src)
	}
	return src.Name, err
}

func GenerateEmptyCsv(wallet string) []byte {
	for _, source := range srcs {
		if source.Wallet == wallet {
			var buf bytes.Buffer
			writer := csv.NewWriter(&buf)
			writer.Write(source.Header)
			writer.Flush()
			return buf.Bytes()
		}
	}

	return nil
}

// ParseTXs : Look into Sources how to parse a CSV into TXs
func ParseTXs(filename string, csvData []byte, forceWallet string, forceHeader []string) (txs tx.TXs, source string, wallet string, unknowns []string, err error) {
	if len(csvData) == 0 {
		err = errors.New("csv.ParseTXs: Error Reading File as CSV")
		return
	}

	// Convert UTF-16LE encoded file to UTF-8
	csvDataEncoding, err := chardet.NewTextDetector().DetectBest(csvData)
	if csvDataEncoding.Charset == "UTF-16LE" {
		csvData, _, _ = transform.Bytes(unicode.UTF16(unicode.LittleEndian, unicode.ExpectBOM).NewDecoder(), csvData)
	}
	if err != nil {
		return
	}

	var records [][]string
	var src Source
	if len(srcs) > 0 {
		src, err = detectSource(csvData, forceHeader)
		if err != nil {
			return
		}
	}

	if forceWallet != "" {
		source = forceWallet
	} else {
		source = src.Name
	}
	wallet = src.Wallet

	records, err = src.readRecords(csvData, forceHeader)
	if err != nil {
		return
	}
	if len(records) <= 1 || err != nil {
		return
	}

	var filenameGroupValues map[string]string
	if src.Filename != "" {
		filenameRegexp := regexp.MustCompile(src.Filename)
		groupNames := filenameRegexp.SubexpNames()
		groupValues := filenameRegexp.FindStringSubmatch(filename)

		if len(groupNames) != len(groupValues) {
			err = errors.New("csv.parseSource: invalid file name, expected" + src.Filename)
			return
		} else {
			filenameGroupValues = make(map[string]string)
			for _, groupName := range groupNames {
				if groupName != "" {
					filenameGroupValues[groupName] = groupValues[filenameRegexp.SubexpIndex(groupName)]
				}
			}
		}
	}

	for lineIndex, recordValues := range records[1:] {
		for _, txDef := range src.TX {
			t := tx.TX{
				Source: src.Name + " CSV",
				Wallet: src.Wallet,
				Method: "CSV",
			}
			if forceWallet != "" {
				t.Wallet = forceWallet
			}
			// FiscalCateg
			for _, ki := range txDef.Kinds {
				if t.FiscalCateg != "" {
					break
				}
				if ki.Default != "" {
					t.FiscalCateg = ki.Default
				} else {
					key := ki.Key.get(recordValues, filenameGroupValues)
					for k, v := range ki.Categ {
						if (ki.Contains && strings.Contains(key, k)) ||
							(!ki.Contains && key == k) {
							if !ki.Skip.Check(valueGetter(recordValues, filenameGroupValues)) {
								t.FiscalCateg = v
							}
						}
					}
				}
			}
			if t.FiscalCateg == "" {
				b := bytes.NewBufferString("")
				w := csv.NewWriter(b)
				w.Write(records[0])
				w.Write(anonymize(recordValues))
				w.Flush()
				unknowns = append(unknowns, b.String())
				break
			}
			// Timestamp
			if txDef.Date.Index > None && txDef.Date.Index < len(recordValues) {
				date := strings.NewReplacer(txDef.Date.Replace...).Replace(recordValues[txDef.Date.Index])
				if txDef.Date.Format == "epoch" {
					epoch, err := strconv.ParseInt(date, 10, 64)
					if err != nil {
						b := bytes.NewBufferString("")
						w := csv.NewWriter(b)
						w.Write(records[0])
						w.Write(anonymize(recordValues))
						w.Flush()
						unknowns = append(unknowns, b.String())
						break
					}
					t.Timestamp = time.Unix(epoch, 0)
				} else {
					loc, _ := time.LoadLocation(txDef.Date.Location)
					ts, perr := time.ParseInLocation(txDef.Date.Format, date, loc)
					if perr != nil {
						b := bytes.NewBufferString("")
						w := csv.NewWriter(b)
						w.Write(records[0])
						w.Write(anonymize(recordValues))
						w.Flush()
						unknowns = append(unknowns, b.String())
						break
					}
					t.Timestamp = ts
				}
			}
			// ID
			if len(txDef.ID.Indexes) > 0 {
				for _, i := range txDef.ID.Indexes {
					if i > None && i < len(recordValues) {
						t.ID += recordValues[i]
					}
				}
			}
			if len(txDef.ID.Text) > 0 {
				t.ID += txDef.ID.Text
			}
			if t.ID == "" {
				t.GenerateID()
			}
			// Items
			t.Items = make(map[string]tx.Values)
			for k, vv := range txDef.Items {
				for _, v := range vv {
					if !v.Skip.Check(valueGetter(recordValues, filenameGroupValues)) {
						// Construct Amount
						var amount decimal.Decimal
						for _, f := range v.Amount {
							val := f.Value.get(recordValues, filenameGroupValues)
							if val != "" {
								value, perr := decimal.NewFromString(strings.ReplaceAll(val, ",", "."))
								if perr != nil {
									b := bytes.NewBufferString("")
									w := csv.NewWriter(b)
									w.Write(records[0])
									w.Write(anonymize(recordValues))
									w.Flush()
									unknowns = append(unknowns, b.String())
									break
								}
								value = value.Shift(f.Value.Shift)
								switch f.Operation {
								case Add:
									amount = amount.Add(value)
								case Sub:
									amount = amount.Sub(value)
								case Mul:
									amount = amount.Mul(value)
								case Div:
									amount = amount.Div(value)
								default:
								}
							}
						}
						if !v.Skip.CheckDecimal(valueGetter(recordValues, filenameGroupValues), amount) && !amount.IsZero() {
							// Make sure Amount is positive
							if amount.IsNegative() {
								amount = amount.Neg()
							}
							// Get the Currency Code
							curr := strings.NewReplacer(txDef.CurrencyReplace...).Replace(strings.ToUpper(v.Currency.get(recordValues, filenameGroupValues)))
							val := tx.Value{Code: curr, Amount: amount}
							if !val.IsFiat() ||
								(k != "Fee" && // Ignore Fiat Fee : unrelevant
									(k != "To" || t.FiscalCateg != tx.Deposits) && // Ignore Fiat Deposits : unrelevant
									(k != "From" || t.FiscalCateg != tx.Withdrawals)) { // Ignore Fiat Withdrawals : unrelevant
								t.Items[k] = append(t.Items[k], val)
							}
							// Detect CashIn
							if val.IsFiat() && k == "From" && t.FiscalCateg == tx.Exchanges {
								t.FiscalCateg = tx.CashIn
							}
							// Detect CashOut
							if val.IsFiat() && k == "To" && t.FiscalCateg == tx.Exchanges {
								t.FiscalCateg = tx.CashOut
							}
						}
					}
				}
			}
			// NFTs
			// No NFT support in CSV for now
			t.NFTs = make(map[string]tx.NFTs)
			// Notes
			for _, n := range txDef.Notes {
				t.Note = strings.Trim(t.Note+" "+n.get(recordValues, filenameGroupValues), " ")
			}

			// Merge or Add
			if len(t.Items) > 0 {
				merged := false
				if txDef.ID.MergeBy && len(txs) > 0 {
					// Merge in reverse order because txs to be merged are often just above
					for i := len(txs) - 1; i >= 0; i-- {
						other := txs[i]
						if other.ID == t.ID &&
							(other.FiscalCateg == t.FiscalCateg ||
								(other.FiscalCateg == tx.Exchanges && t.FiscalCateg == tx.CashIn) ||
								(other.FiscalCateg == tx.Exchanges && t.FiscalCateg == tx.CashOut) ||
								(other.FiscalCateg == tx.CashIn && t.FiscalCateg == tx.Exchanges) ||
								(other.FiscalCateg == tx.CashOut && t.FiscalCateg == tx.Exchanges)) {
							if other.FiscalCateg == tx.Exchanges && t.FiscalCateg == tx.CashIn {
								txs[i].FiscalCateg = tx.CashIn
							}
							if other.FiscalCateg == tx.Exchanges && t.FiscalCateg == tx.CashOut {
								txs[i].FiscalCateg = tx.CashOut
							}
							// Do not merge deposits or withdrawals if both have a from or a to
							if (t.FiscalCateg == tx.Deposits && other.FiscalCateg == tx.Deposits && len(t.Items["To"]) > 0 && len(other.Items["To"]) > 0) ||
								(t.FiscalCateg == tx.Withdrawals && other.FiscalCateg == tx.Withdrawals && len(t.Items["From"]) > 0 && len(other.Items["From"]) > 0) {
								t.ID += "-" + strconv.Itoa(lineIndex)
								break
							}
							for k, v := range t.Items {
								other.Items[k] = append(other.Items[k], v...)
							}
							txs[i].Note += "; " + t.Note
							merged = true
							break
						}

						if len(txDef.ID.MergeProcessors) > 0 {
							for _, processor := range txDef.ID.MergeProcessors {
								if processor == "CashinSingletonExchangeWithCashout" {
									if t.FiscalCateg == tx.CashIn && other.FiscalCateg == tx.CashOut &&
										len(t.Items["From"]) == 1 && len(t.Items["To"]) == 0 && len(t.Items["Fee"]) == 0 &&
										len(other.Items["From"]) == 1 && len(other.Items["To"]) == 1 &&
										t.Items["From"][0].Code == other.Items["To"][0].Code {
										other.Items["To"][0].Amount = other.Items["To"][0].Amount.Sub(t.Items["From"][0].Amount)
										txs[i].Note += "; " + t.Note
										merged = true
										break
									}
								}

								// A transfer as been categorized as two minings transactions => ignore the transfert
								if processor == "NegativeMining" {
									if t.FiscalCateg == tx.Minings && len(t.Items["From"]) == 1 && len(t.Items["To"]) == 0 &&
										other.FiscalCateg == tx.Minings && len(other.Items["From"]) == 0 && len(other.Items["To"]) == 1 &&
										t.Items["From"][0].Code == other.Items["To"][0].Code && t.Items["From"][0].Amount.Equal(other.Items["To"][0].Amount) {
										other.Items["From"] = t.Items["From"]
										txs[i].FiscalCateg = tx.DontCare
										txs[i].Note += "; " + t.Note
										merged = true
										break
									}
								}
							}
							if merged {
								break
							}
						}
					}
				}
				if !merged {
					txs = append(txs, t)
				}
			}

			// Prepare rates
			if t.FiscalCateg != tx.DontCare && t.FiscalCateg != tx.Void {
				for _, values := range t.Items {
					for _, value := range values {
						rate.Prepare(value.Code)
					}
				}
			}
		}
	}

	// Additional merge processors
	if len(src.MergeProcessors) > 0 && len(txs) >= 2 {
		for _, processor := range src.MergeProcessors {
			if processor == "SingletonExchange" {
				singletons := make([]tx.TX, 0)
				singletonsIndexes := make([]int, 0)
				singletonsMergedIndexes := make([]int, 0)
				for i, t := range txs {
					if t.FiscalCateg == tx.Exchanges &&
						((len(t.Items["From"]) > 0 && len(t.Items["To"]) == 0) || (len(t.Items["From"]) == 0 && len(t.Items["To"]) > 0)) {
						singletons = append(singletons, t)
						singletonsIndexes = append(singletonsIndexes, i)
					}
				}

				if len(singletons) > 0 {
					for i, singleton := range singletons {
						for j := len(txs) - 1; j >= 0; j-- {
							other := &txs[j]
							if !slices.Contains(singletonsIndexes, j) && singleton.Timestamp == other.Timestamp {
								// Merge only tx if previous one items are found in previous two items
								singletonFromItemsCodes := make([]string, len(singleton.Items["From"]))
								for _, v := range singleton.Items["From"] {
									singletonFromItemsCodes = append(singletonFromItemsCodes, v.Code)
								}
								otherFromItemsCodes := make([]string, len(other.Items["From"]))
								for _, v := range other.Items["From"] {
									otherFromItemsCodes = append(otherFromItemsCodes, v.Code)
								}
								if !slices.ContainsAllStrings(otherFromItemsCodes, singletonFromItemsCodes...) {
									continue
								}

								singletonToItemsCodes := make([]string, len(singleton.Items["To"]))
								for _, v := range singleton.Items["To"] {
									singletonToItemsCodes = append(singletonToItemsCodes, v.Code)
								}
								otherToItemsCodes := make([]string, len(other.Items["To"]))
								for _, v := range other.Items["To"] {
									otherToItemsCodes = append(otherToItemsCodes, v.Code)
								}
								if !slices.ContainsAllStrings(otherToItemsCodes, singletonToItemsCodes...) {
									continue
								}

								// Merge singleton into other
								for k, v := range singleton.Items {
									other.Items[k] = append(other.Items[k], v...)
								}
								other.Note += "; " + singleton.Note
								singletonsMergedIndexes = append(singletonsMergedIndexes, singletonsIndexes[i])
								break
							}
						}
					}

					// Remove merged singletons
					removeTxs(&txs, singletonsMergedIndexes)
				}
			}

			// Merge exchange split on multiple seconds
			if processor == "ExchangeOnMultipleSeconds" {
				singletons := make([]tx.TX, 0)
				singletonsIndexes := make([]int, 0)
				singletonsMergedIndexes := make([]int, 0)
				for i, t := range txs {
					if (t.FiscalCateg == tx.Exchanges || t.FiscalCateg == tx.CashIn || t.FiscalCateg == tx.CashOut) &&
						((len(t.Items["From"]) > 0 && len(t.Items["To"]) == 0) || (len(t.Items["From"]) == 0 && len(t.Items["To"]) > 0)) {
						singletons = append(singletons, t)
						singletonsIndexes = append(singletonsIndexes, i)
					}
				}
				if len(singletons) > 0 {
					for i := len(singletons) - 1; i >= 0; i-- {
						singleton := singletons[i]
						// Do not process singleton if it is not anymore one
						if len(singleton.Items["From"]) > 0 && len(singleton.Items["To"]) > 0 {
							continue
						}
						for j := len(txs) - 1; j >= 0; j-- {
							other := &txs[j]
							// not self
							if singletonsIndexes[i] != j &&
								// exchange or cash out or cash in
								(other.FiscalCateg == tx.Exchanges || other.FiscalCateg == tx.CashIn || other.FiscalCateg == tx.CashOut) &&
								// other is one second before
								singleton.Timestamp == other.Timestamp.Add(time.Second) {

								// Merge singleton into other
								for k, v := range singleton.Items {
									other.Items[k] = append(other.Items[k], v...)
								}
								other.Note += "; " + singleton.Note
								if singleton.FiscalCateg == tx.CashOut && other.FiscalCateg == tx.Exchanges {
									other.FiscalCateg = tx.CashOut
								}
								singletonsMergedIndexes = append(singletonsMergedIndexes, singletonsIndexes[i])
								break
							}
						}
					}

					// Remove merged singletons
					sort.Ints(singletonsMergedIndexes)
					removeTxs(&txs, singletonsMergedIndexes)
				}
			}
		}
	}

	if src.Name == "" && len(records) > 1 {
		b := bytes.NewBufferString("")
		w := csv.NewWriter(b)
		w.Write(records[0])
		w.Write(anonymize(records[1]))
		w.Flush()
		unknowns = append(unknowns, b.String())
	}
	return
}

/* Remove from txs tx located at indexes */
func removeTxs(txs *tx.TXs, indexes []int) {
	if len(indexes) > 0 {
		txsWithoutSingletons := make([]tx.TX, 0)
		txsWithoutSingletons = (*txs)[:indexes[0]]
		for i := 1; i < len(indexes); i++ {
			txsWithoutSingletons = append(txsWithoutSingletons, (*txs)[indexes[i-1]+1:indexes[i]]...)
		}
		// Append all remainings
		txsWithoutSingletons = append(txsWithoutSingletons, (*txs)[indexes[len(indexes)-1]+1:]...)
		(*txs) = txsWithoutSingletons
	}
}

func detectSource(csvData []byte, forceHeader []string) (source Source, err error) {
	firstLineData := csvData
	firstLnIndex := bytes.Index(csvData, []byte{0x0A})
	if firstLnIndex >= 0 {
		firstLineData = csvData[:bytes.Index(csvData, []byte{0x0A})]
	}
	var error error
	for _, src := range srcs {
		csvReader := csv.NewReader(bytes.NewReader(firstLineData))
		err = configureCsvReader(csvReader, src.Name, src.Line.Separator)
		if err != nil {
			return source, err
		}
		firstLine, err := csvReader.ReadAll()
		if err != nil {
			error = err
			continue
		}
		if src.Line.Quoted {
			firstLine = splitLinesQuotedFile(&firstLine)
		}
		if forceHeader != nil {
			firstLine = [][]string{forceHeader}
		}

		if len(firstLine) > 0 &&
			reflect.DeepEqual(src.Header, firstLine[0]) {
			source = src
			break
		}
	}

	if source.Name == "" && error != nil {
		err = errors.New(fmt.Sprintln("csv.detectSource: Error Reading File as CSV", error))
	}

	return source, err
}

func (src Source) readRecords(csvData []byte, forceHeader []string) (records [][]string, err error) {
	if len(src.Line.ReplaceRegex) > 0 && len(src.Line.ReplaceRegex)%2 == 0 {
		for regexIndex := 0; regexIndex < len(src.Line.ReplaceRegex); regexIndex = regexIndex + 2 {
			compiledRegexp := regexp.MustCompile(src.Line.ReplaceRegex[regexIndex])
			csvData = compiledRegexp.ReplaceAll(csvData, []byte(src.Line.ReplaceRegex[regexIndex+1]))
		}
	}
	csvReader := csv.NewReader(bytes.NewReader(csvData))
	err = configureCsvReader(csvReader, src.Name, src.Line.Separator)
	if err != nil {
		return records, err
	}
	records, err = csvReader.ReadAll()
	if err != nil {
		return records, errors.New(fmt.Sprintln("csv.readRecords: Error Reading File as CSV", err))
	}
	if src.Line.Quoted {
		records = splitLinesQuotedFile(&records)
	}
	if forceHeader != nil {
		records = append([][]string{forceHeader}, records...)
	}

	return records, err
}

func configureCsvReader(csvReader *csv.Reader, name string, separator string) (err error) {
	if separator != "" {
		unquoteChar, err := strconv.Unquote(separator)
		if err != nil {
			return errors.New("csv.parseSource: def[" + name + "], separator [" + separator + "] Separator char must be unicode like '\u0009'")
		}
		csvReader.Comma = []rune(unquoteChar)[0]
	}
	return
}

// Split one field into several fields by comma. Do not support a comma inside a field which will be splited
func splitLinesQuotedFile(records *[][]string) (lines [][]string) {
	(lines) = make([][]string, 0)
	for _, record := range *records {
		if len(record) == 1 {
			fields := strings.Split(record[0], ",")
			lines = append(lines, fields)
		}
	}

	return lines
}

func anonymize(line []string) (anonymized []string) {
	for _, s := range line {
		a := ""
		count := 0
		for _, c := range s {
			if c == '0' || c == '1' || c == '2' || c == '3' || c == '4' ||
				c == '5' || c == '6' || c == '7' || c == '8' || c == '9' {
				count++
				a += strconv.Itoa(rand.Intn(10))
			} else {
				a += string(c)
			}
		}
		if count > 3 {
			anonymized = append(anonymized, a)
		} else {
			anonymized = append(anonymized, s)
		}
	}
	return
}
