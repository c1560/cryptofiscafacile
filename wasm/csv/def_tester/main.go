package main

import (
	"flag"
	"log"
	"os"

	"github.com/davecgh/go-spew/spew"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/csv"
)

func main() {
	pCSVDef := flag.String("def", "", "CSV Def file")
	pCSVTx := flag.String("csv", "", "CSV Tx file")
	flag.Parse()
	if *pCSVDef == "" || *pCSVTx == "" {
		log.Fatal("CSV Def file and CSV Tx file MUST be specified")
	}
	def, err := os.ReadFile(*pCSVDef)
	if err != nil {
		log.Fatal(err)
	}
	name, err := csv.AddDefinition(def)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Définition ajoutée : " + name)
	tx, err := os.ReadFile(*pCSVTx)
	if err != nil {
		log.Fatal(err)
	}
	txs, source, unknowns, err := csv.ParseTXs(tx)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Source :", source)
	log.Println("TXs :", spew.Sdump(txs))
	log.Println("Unknowns :", unknowns)
}
