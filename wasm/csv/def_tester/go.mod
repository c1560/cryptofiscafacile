module gitlab.com/c1560/CryptoFiscaFacile/wasm/csv/def_tester

go 1.19

replace (
	gitlab.com/c1560/CryptoFiscaFacile/wasm/csv => ../
	gitlab.com/c1560/CryptoFiscaFacile/wasm/rate => ../../rate
	gitlab.com/c1560/CryptoFiscaFacile/wasm/tx => ../../tx
	gitlab.com/c1560/CryptoFiscaFacile/wasm/wallet => ../../wallet
)

require (
	github.com/davecgh/go-spew v1.1.1
	gitlab.com/c1560/CryptoFiscaFacile/wasm/csv v0.0.0-00010101000000-000000000000
)

require (
	github.com/go-resty/resty/v2 v2.7.0 // indirect
	github.com/shopspring/decimal v1.3.1 // indirect
	gitlab.com/c1560/CryptoFiscaFacile/wasm/rate v0.0.0-00010101000000-000000000000 // indirect
	gitlab.com/c1560/CryptoFiscaFacile/wasm/tx v0.0.0-00010101000000-000000000000 // indirect
	golang.org/x/net v0.0.0-20211029224645-99673261e6eb // indirect
)
