package main

import (
	"log"
	"syscall/js"

	"github.com/davecgh/go-spew/spew"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/api"
)

var apisProgress = make(map[string]*int)

func generateApiEmptyFile(name string) *api.JSONFile {
	all := api.GetAlls()
	for _, one := range all {
		if one.GetName() == name {
			jsonFile := one.GetEmptyJson()
			return &jsonFile
		}
	}

	return nil
}

func getTools(this js.Value, args []js.Value) interface{} {
	if debug {
		log.Println("getTools()")
		spew.Dump(args)
	}
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		// reject := args[1]
		go func() {
			all := api.GetAlls()
			apis := make([]interface{}, len(all))
			for i, a := range all {
				newAPI := make(map[string]interface{})
				name := a.GetName()
				newAPI["name"] = name
				newAPI["url"] = a.GetURL()
				newAPI["plateform"] = a.GetPlateform()
				newAPI["key"] = a.NeedKey()
				c := confAPI[name]
				newAPI["arg1"] = c.arg1
				newAPI["arg2"] = c.arg2
				newAPI["arg3"] = c.arg3
				apis[i] = newAPI
			}
			tools := make(map[string]interface{})
			tools["apis"] = apis
			if debug {
				spew.Dump(tools)
			}
			resolve.Invoke(tools)
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func getApi(this js.Value, args []js.Value) interface{} {
	provider := args[0].String()
	arg1 := args[1].String()
	arg2 := args[2].String()
	arg3 := args[3].String()
	remember := args[4].Bool()
	if debug {
		log.Println("getApi()")
		args[2] = js.ValueOf("**********")
		spew.Dump(args)
	}

	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		reject := args[1]

		go func() {
			conf := make(map[string]interface{})
			conf["arg1"] = arg1
			conf["arg2"] = arg2
			conf["arg3"] = arg3
			conf["debug"] = debug
			if confUser.proxy != "" {
				conf["proxy"] = confUser.proxy
			}
			var wantAPI api.API
			for _, a := range api.GetAlls() {
				if a.GetName() == provider {
					wantAPI = a
				}
			}
			if wantAPI == nil {
				reject.Invoke(js.Global().Get("Error").New("Unmanaged API : " + provider))
				return
			}
			if jsonName := wantAPI.GetJSONName(); jsonName != "" {
				if data, err := getTXFileData(jsonName); err == nil {
					conf["json"] = data
				}
			}
			if wantAPI.WithChannels() {
				jsons, progress, errorChan, err := wantAPI.GetJSONs(conf)
				if err != nil {
					reject.Invoke(js.Global().Get("Error").New("Erreur " + provider + " API : " + err.Error()))
					return
				}
				if remember {
					confAPI[provider] = configAPI{arg1: arg1, arg2: arg2, arg3: arg3}
					saveProviderAPIConfDB(provider)
				}
				for {
					select {
					case j := <-jsons:
						err = digestAndSaveFile(j.Name, j.Data)
						if err != nil {
							reject.Invoke(js.Global().Get("Error").New(provider + " API Error : " + err.Error()))
						}
					case p := <-progress:
						apisProgress[provider] = &p
						if p >= 100 {
							resolve.Invoke()
							return
						}
					case err := <-errorChan:
						reject.Invoke(js.Global().Get("Error").New(err.Error()))
					}
				}
			} else {
				progress := 0
				apisProgress[provider] = &progress
				conf["progress"] = &progress

				conf["onJson"] = func(json api.JSONFile) (err error) {
					return digestAndSaveFile(json.Name, json.Data)
				}

				err := wantAPI.GetJsons(conf)
				if err != nil {
					reject.Invoke(js.Global().Get("Error").New("Erreur " + provider + " API : " + err.Error()))
					return
				}
				if remember {
					confAPI[provider] = configAPI{arg1: arg1, arg2: arg2, arg3: arg3}
					saveProviderAPIConfDB(provider)
				}
				resolve.Invoke()
			}
		}()
		return nil
	})
	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}

func getApiProgress(this js.Value, args []js.Value) interface{} {
	provider := args[0].String()
	if debug {
		log.Println("getApiProgress()")
		spew.Dump(args)
	}

	if apisProgress[provider] == nil {
		return nil
	} else {
		return js.ValueOf(*apisProgress[provider])
	}
}
