#!/usr/bin/env bash
#
# Code coverage generation

COVERAGE_DIR="${COVERAGE_DIR:-coverage}"
PKG_LIST=$(go list -m all | grep c1560 | grep "=>" | cut -d" " -f1)
REPORTS_DIR="reports"

# prepare file system
mkdir -p $COVERAGE_DIR
mkdir -p $REPORTS_DIR
if [ -f "$REPORTS_DIR/tests.log" ]; then
    rm $REPORTS_DIR/tests.log
fi

# junit report dependency
go get github.com/jstemmer/go-junit-report/v2@latest
go install github.com/jstemmer/go-junit-report/v2@latest
# go wasm test in a browser
go get github.com/agnivade/wasmbrowsertest
go install github.com/agnivade/wasmbrowsertest

# unset env variables for wasmbrowsertest
unset GITLAB_FEATURES
unset $(env | grep CI_ | awk -F= '{print $1}')
# run tests with coverage for wasm package
GOOS=js GOARCH=wasm go test -exec="$(go env GOPATH)/bin/wasmbrowsertest" -covermode=count -coverprofile "${COVERAGE_DIR}/cff.out" -v "./..." 2>&1 | tee -a $REPORTS_DIR/tests.log;
# run tests with coverage for each package
for package in ${PKG_LIST}; do
    GOOS=js GOARCH=wasm go test -exec="$(go env GOPATH)/bin/wasmbrowsertest" -covermode=count -coverprofile "${COVERAGE_DIR}/${package##*/}.out" -v "$package" 2>&1 | tee -a $REPORTS_DIR/tests.log;
done ;

# generate tests report in junit format
$(go env GOPATH)/bin/go-junit-report -in $REPORTS_DIR/tests.log -set-exit-code -out reports/tests.xml
HAS_FAILED_TESTS=$?
rm $REPORTS_DIR/tests.log;

# Merge the coverage profile files
echo 'mode: count' > "${COVERAGE_DIR}"/coverage.cov ;
tail -q -n +2 "${COVERAGE_DIR}"/*.out >> "${COVERAGE_DIR}"/coverage.cov ;

# Display the global code coverage
go tool cover -func="${COVERAGE_DIR}"/coverage.cov ;

# If needed, generate cobertura report
if [ "$1" == "cobertura" ]; then
    # install gocover-cobertura tool
    go get -d github.com/boumenot/gocover-cobertura
    go install github.com/boumenot/gocover-cobertura

    # convert gocover to cobertura
    $(go env GOPATH)/bin/gocover-cobertura < "${COVERAGE_DIR}"/coverage.cov > reports/coverage.xml
else
    # If needed, generate HTML report
    if [ "$1" == "html" ]; then
        go tool cover -html="${COVERAGE_DIR}"/coverage.cov -o reports/coverage.html ;
    fi
fi

# Remove the coverage files directory
rm -rf "$COVERAGE_DIR";

if [ "$HAS_FAILED_TESTS" -ne "0" ];then
    exit 1
fi
