package main

import (
	"log"
	"sort"
	"syscall/js"

	"github.com/davecgh/go-spew/spew"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/utils/slices"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/wallet"
)

func getWallets(this js.Value, args []js.Value) interface{} {
	handler := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		resolve := args[0]
		//reject := args[1]

		go func() {
			wallets := make([]wallet.Wallet, 0)
			for _, statusTX := range statusTXs {
				walletName := statusTX.(map[string]interface{})["wallet"]
				if walletName != nil {
					var aWallet *wallet.Wallet
					if walletName != "CryptoFiscaFacile" {
						aWallet, _ = wallet.Get(walletName.(string))
					} else {
						aWallet, _ = wallet.Get(statusTX.(map[string]interface{})["kind"].(string))
					}
					if aWallet != nil && slices.Find(wallets, func(w wallet.Wallet) bool { return w.Name == aWallet.Name }) == nil {
						wallets = append(wallets, *aWallet)
					}
				}
			}
			sort.Slice(wallets, func(i, j int) bool {
				return wallets[i].Name < wallets[j].Name
			})

			jsWallets := make([]interface{}, len(wallets))
			for i, aWallet := range wallets {
				jsWallet := make(map[string]interface{})
				jsWallet["name"] = aWallet.Name
				jsWallet["icon"] = aWallet.Logo
				jsWallets[i] = jsWallet
			}
			if debug {
				log.Println("getWallet():")
				spew.Dump(wallets)
			}
			resolve.Invoke(jsWallets)
		}()
		return nil
	})

	promiseConstructor := js.Global().Get("Promise")
	return promiseConstructor.New(handler)
}
