package json

import (
	"testing"
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/google/go-cmp/cmp"
	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

func TestBinanceJSON_Digest(t *testing.T) {
	type args struct {
		jsonData []byte
	}
	tests := []struct {
		name       string
		json       BinanceJSON
		args       args
		wantTxs    tx.TXs
		wantSource string
		wantErr    bool
	}{
		{
			name:    "Void",
			wantErr: true,
		},
		{
			name: "Broken",
			args: args{
				jsonData: []byte("{"),
			},
			wantErr: true,
		},
		{
			name: "Empty",
			args: args{
				jsonData: []byte("{}"),
			},
			wantErr: true,
		},
		{
			name: "Error Amount",
			args: args{
				jsonData: []byte("{\"assetDividends\":{\"objects\": {\"1234\":{\"amount\": \"a\"}},\"total\": 1}}"),
			},
			wantSource: "Binance API",
			wantErr:    false,
		},
		{
			name: "Minings",
			args: args{
				jsonData: []byte("{\"assetDividends\":{\"objects\": {\"55326129331\":{\"amount\": \"31.11922907\",\"asset\": \"TLM\",\"divTime\": 1619491995000,\"enInfo\": \"BNB Vault\",\"tranId\": 55326129331}},\"total\": 1}}"),
			},
			wantTxs: tx.TXs{
				tx.TX{
					Timestamp:   time.Date(2021, 04, 27, 4, 53, 15, 0, time.FixedZone("CEST", 2*60*60)),
					ID:          "55326129331",
					Source:      "Binance API",
					Method:      "API",
					Wallet:      "Binance",
					FiscalCateg: tx.Minings,
					Items: map[string]tx.Values{
						"To": {
							tx.Value{
								Code:   "TLM",
								Amount: decimal.New(3111922907, -8),
							},
						},
					},
					Note: "BNB Vault",
				},
			},
			wantSource: "Binance API",
			wantErr:    false,
		},
		{
			name: "Interests",
			args: args{
				jsonData: []byte("{\"assetDividends\":{\"objects\": {\"55326129331\":{\"amount\": \"31.11922907\",\"asset\": \"TLM\",\"divTime\": 1619491995000,\"enInfo\": \"Flexible Savings\",\"tranId\": 55326129331}},\"total\": 1}}"),
			},
			wantTxs: tx.TXs{
				tx.TX{
					Timestamp:   time.Date(2021, 04, 27, 4, 53, 15, 0, time.FixedZone("CEST", 2*60*60)),
					ID:          "55326129331",
					Source:      "Binance API",
					Method:      "API",
					Wallet:      "Binance",
					FiscalCateg: tx.Interests,
					Items: map[string]tx.Values{
						"To": {
							tx.Value{
								Code:   "TLM",
								Amount: decimal.New(3111922907, -8),
							},
						},
					},
					Note: "Flexible Savings",
				},
			},
			wantSource: "Binance API",
			wantErr:    false,
		},
		{
			name: "AirDrops",
			args: args{
				jsonData: []byte("{\"assetDividends\":{\"objects\": {\"55326129331\":{\"amount\": \"31.11922907\",\"asset\": \"TLM\",\"divTime\": 1619491995000,\"enInfo\": \"Distribution\",\"tranId\": 55326129331}},\"total\": 1}}"),
			},
			wantTxs: tx.TXs{
				tx.TX{
					Timestamp:   time.Date(2021, 04, 27, 4, 53, 15, 0, time.FixedZone("CEST", 2*60*60)),
					ID:          "55326129331",
					Source:      "Binance API",
					Method:      "API",
					Wallet:      "Binance",
					FiscalCateg: tx.AirDrops,
					Items: map[string]tx.Values{
						"To": {
							tx.Value{
								Code:   "TLM",
								Amount: decimal.New(3111922907, -8),
							},
						},
					},
					Note: "Distribution",
				},
			},
			wantSource: "Binance API",
			wantErr:    false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotTxs, gotSource, _, _, _, err := tt.json.Digest(tt.args.jsonData)
			if (err != nil) != tt.wantErr {
				t.Errorf("BinanceJSON.Digest() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !cmp.Equal(gotTxs, tt.wantTxs) {
				t.Errorf("BinanceJSON.Digest() gotTxs = %v, want %v", spew.Sdump(gotTxs), spew.Sdump(tt.wantTxs))
			}
			if gotSource != tt.wantSource {
				t.Errorf("BinanceJSON.Digest() gotSource = %v, want %v", gotSource, tt.wantSource)
			}
		})
	}
}
