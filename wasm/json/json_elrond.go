package json

import (
	encjson "encoding/json"
	"errors"
	"log"
	"reflect"
	"strings"
	"time"

	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

type ElrondTransaction struct {
	TxHash         string            `json:"txHash"`
	GasLimit       int64             `json:"gasLimit"`
	GasPrice       int64             `json:"gasPrice"`
	GasUsed        int64             `json:"gasUsed"`
	MiniBlockHash  string            `json:"miniBlockHash"`
	Nonce          int               `json:"nonce"`
	Receiver       string            `json:"receiver"`
	ReceiverAssets ElrondAssets      `json:"receiverAssets,omitempty"`
	ReceiverShard  int               `json:"receiverShard"`
	Round          int               `json:"round"`
	Sender         string            `json:"sender"`
	SenderAssets   ElrondAssets      `json:"senderAssets,omitempty"`
	SenderShard    int               `json:"senderShard"`
	Signature      string            `json:"signature"`
	Status         string            `json:"status"`
	Value          string            `json:"value"`
	Fee            string            `json:"fee"`
	Timestamp      int64             `json:"timestamp"`
	Data           string            `json:"data,omitempty"`
	Function       string            `json:"function,omitempty"`
	Action         ElrondAction      `json:"action,omitempty"`
	Operations     []ElrondOperation `json:"operations,omitempty"`
	Logs           ElrondLogs        `json:"logs,omitempty"`
}
type ElrondAssets struct {
	Name        string   `json:"name"`
	Description string   `json:"description,omitempty"`
	Tags        []string `json:"tags"`
	IconPng     string   `json:"iconPng,omitempty"`
	IconSvg     string   `json:"iconSvg,omitempty"`
}
type ElrondTransfers struct {
	Type       string `json:"type"`
	Name       string `json:"name"`
	Ticker     string `json:"ticker"`
	SvgURL     string `json:"svgUrl"`
	Collection string `json:"collection"`
	Decimals   int32  `json:"decimals"`
	Identifier string `json:"identifier"`
	Value      string `json:"value"`
}
type ElrondToken struct {
	Type     string `json:"type"`
	Name     string `json:"name"`
	Ticker   string `json:"ticker"`
	Token    string `json:"token"`
	Decimals int32  `json:"decimals"`
	Value    string `json:"value"`
}
type ElrondArguments struct {
	ElrondTransfers []ElrondTransfers `json:"transfers"`
	Receiver        string            `json:"receiver,omitempty"`
	FunctionName    string            `json:"functionName,omitempty"`
	FunctionArgs    []string          `json:"functionArgs,omitempty"`
	ReceiverAssets  ElrondAssets      `json:"receiverAssets,omitempty"`
	ProviderName    string            `json:"providerName,omitempty"`
	ProviderAvatar  string            `json:"providerAvatar,omitempty"`
	Value           string            `json:"value,omitempty"`
	Token           ElrondToken       `json:"token,omitempty"`
}
type ElrondAction struct {
	Category        string          `json:"category"`
	Name            string          `json:"name"`
	Description     string          `json:"description,omitempty"`
	ElrondArguments ElrondArguments `json:"arguments,omitempty"`
}

type ElrondOperation struct {
	Id             string       `json:"id"`
	Action         string       `json:"action"`
	Type           string       `json:"type"`
	EsdtType       string       `json:"esdtType,omitempty"`
	Collection     string       `json:"collection,omitempty"`
	Identifier     string       `json:"identifier,omitempty"`
	Ticker         string       `json:ticker,omitempty`
	Name           string       `json:"name,omitempty"`
	Sender         string       `json:"sender"`
	Receiver       string       `json:"receiver"`
	Value          string       `json:"value,omitempty"`
	Decimals       int32        `json:"decimals,omitempty"`
	SvgURL         string       `json:"svgUrl,omitempty"`
	SenderAssets   ElrondAssets `json:"senderAssets,omitempty"`
	ReceiverAssets ElrondAssets `json:"receiverAssets,omitempty"`
}

type ElrondEvent struct {
	Address    string   `json:"address"`
	Identifier string   `json:"identifier"`
	Topics     []string `json:"topics"`
	Order      int      `json:"order"`
	Data       string   `json:"data,omitempty"`
}
type ElrondLogs struct {
	Address string        `json:"address"`
	Events  []ElrondEvent `json:"events"`
}

type ElrondJSON struct {
	Addresses    map[string]bool              `json:"egld_addrs"`
	Transactions map[string]ElrondTransaction `json:"transactions"`
}

var elrond = &ElrondJSON{}

// Digest : Digest a Elrond AddressTransactions JSON file
func (j ElrondJSON) Digest(jsonData []byte) (txs tx.TXs, source string, wallet string, unknowns []string, addresses []string, err error) {
	err = encjson.Unmarshal(jsonData, &j)
	if len(j.Addresses) == 0 { // Mandatory fields
		err = errors.New("JSON Format does not match")
	}
	if err != nil {
		return
	}
	source = "Elrond API"
	wallet = "Elrond"
	newType := make(map[string]bool)
	usedNor := make(map[string]bool)
	isUnknown := false

	for _, ntx := range j.Transactions {
		if !usedNor[ntx.TxHash] {
			// If user is the receiver, put it in toIsOwn
			toIsOwn := j.Addresses[ntx.Receiver]
			// If user is the sender, put it in fromIsOwn
			fromIsOwn := j.Addresses[ntx.Sender]
			ntxValue, err := decimal.NewFromString(ntx.Value)
			if err != nil {
				continue
			}
			ntxValue = ntxValue.Shift(-18)
			ntxFee, err := decimal.NewFromString(ntx.Fee)
			if err != nil {
				continue
			}
			ntxFee = ntxFee.Shift(-18)

			// Looking for the type of TX...
			// "function" can be either:
			// - delegate (stake)
			// - unDelegate (stake)
			// - withdraw
			// - wrapEgld

			// - swapTokensFixedInput
			// - claimRewards -> Sender=Receiver
			// - claimRewardsProxy -> Sender=Receiver
			// - compoundRewardsProxy -> Sender=Receiver
			// - enterFarmAndLockRewards -> Sender=Receiver
			// - enterFarmAndLockRewardsProxy -> Sender=Receiver
			// - addLiquidity -> Sender=Receiver
			// - migrateV1_2Position -> Sender=Receiver
			// - migrateToNewFarm -> Sender=Receiver

			// - exitFarm -> Sender=Receiver

			// - saveAttestation
			// - register

			// Other than function:
			// - SaveKeyValue (in action-name) -> Sender=Receiver
			// - maiar: registration funding (senderAssets-name)
			// Otherwise: unknown

			// Each time "function" is present, "action{}" is also present
			function := ntx.Function
			if ntx.Function == "" {
				if ntx.Action.Name != "" {
					// There is 1 case when "action{}" is there without "function"
					// Name = "SaveKeyValue"
					function = ntx.Action.Name
				} else if ntx.SenderAssets.Name != "" {
					// To get for example :
					// - "exchange: crypto.com" from "Exchange: Crypto.com"
					// - or "maiar: registration funding" from "Maiar: Registration Funding"
					function = strings.ToLower(ntx.SenderAssets.Name)
				} else {
					function = "unknown"
					log.Println("Function unknown : ", ntx)
				}
			}

			if toIsOwn && fromIsOwn {
				desc := ntx.SenderAssets.Name + " " + ntx.Action.Name + " : " + ntx.Action.Description
				t := tx.TX{Timestamp: time.Unix(ntx.Timestamp, 0), ID: ntx.TxHash, Source: source, Wallet: "Elrond", Method: "API", Note: desc}
				t.Items = make(map[string]tx.Values)
				t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "EGLD", Amount: ntxFee})
				if ntx.Status == "success" {
					if function == "claimRewards" || function == "claimRewardsProxy" || function == "compoundRewards" || function == "compoundRewardsProxy" {
						for _, op := range ntx.Operations {
							if op.Action == "transfer" {
								val, err := decimal.NewFromString(op.Value)
								if err == nil {
									curr := op.Ticker
									val = val.Shift(-op.Decimals)
									// Here ntx.Sender == ntx.Receiver, so let's compare with Sender
									// If op.Receiver is the user, this is a TO TX, otherwise a FROM
									if op.Receiver == ntx.Sender {
										t.Items["To"] = append(t.Items["To"], tx.Value{Code: curr, Amount: val})
									} else if op.Sender == ntx.Sender {
										t.Items["From"] = append(t.Items["From"], tx.Value{Code: curr, Amount: val})
									}
									t.FiscalCateg = tx.Exchanges
								} else {
									// If val is empty, this is FEES, but may be a malformed TX
									t.FiscalCateg = tx.Fees
								}
							}
						}
					} else if function == "enterFarmAndLockRewards" || function == "enterFarmAndLockRewardsProxy" {
						for _, op := range ntx.Operations {
							if op.Action == "transfer" {
								val, err := decimal.NewFromString(op.Value)
								if err == nil {
									curr := op.Ticker
									val = val.Shift(-op.Decimals)
									// Here ntx.Sender == ntx.Receiver, so let's compare with Sender
									// If op.Receiver is the user, this is a TO TX, otherwise a FROM
									if op.Receiver == ntx.Sender {
										t.Items["To"] = append(t.Items["To"], tx.Value{Code: curr, Amount: val})
									} else if op.Sender == ntx.Sender {
										t.Items["From"] = append(t.Items["From"], tx.Value{Code: curr, Amount: val})
									}
									t.FiscalCateg = tx.Exchanges
								} else {
									// If val is empty, this is FEES, but may be a malformed TX
									t.FiscalCateg = tx.Fees
								}
							}
						}
					} else if function == "exitFarm" {
						for _, op := range ntx.Operations {
							if op.Action == "transfer" {
								val, err := decimal.NewFromString(op.Value)
								if err == nil {
									curr := op.Ticker
									val = val.Shift(-op.Decimals)
									// Here ntx.Sender == ntx.Receiver, so let's compare with Sender
									// If op.Receiver is the user, this is a TO TX, otherwise a FROM
									if op.Receiver == ntx.Sender {
										t.Items["To"] = append(t.Items["To"], tx.Value{Code: curr, Amount: val})
									} else if op.Sender == ntx.Sender {
										t.Items["From"] = append(t.Items["From"], tx.Value{Code: curr, Amount: val})
									}
									t.FiscalCateg = tx.Exchanges
								} else {
									// If val is empty, this is FEES, but may be a malformed TX
									t.FiscalCateg = tx.Fees
								}
							}
						}
					} else if function == "addLiquidity" {
						for _, op := range ntx.Operations {
							if op.Action == "transfer" {
								val, err := decimal.NewFromString(op.Value)
								if err == nil {
									curr := op.Ticker
									val = val.Shift(-op.Decimals)
									// Here ntx.Sender == ntx.Receiver, so let's compare with Sender
									// If op.Receiver is the user, this is a TO TX, otherwise a FROM
									if op.Receiver == ntx.Sender {
										t.Items["To"] = append(t.Items["To"], tx.Value{Code: curr, Amount: val})
									} else if op.Sender == ntx.Sender {
										t.Items["From"] = append(t.Items["From"], tx.Value{Code: curr, Amount: val})
									}
									t.FiscalCateg = tx.Exchanges
								} else {
									// If val is empty, this is FEES, but may be a malformed TX
									t.FiscalCateg = tx.Fees
								}
							}
						}
					} else if function == "migrateV1_2Position" || function == "migrateToNewFarm" {
						for _, op := range ntx.Operations {
							if op.Action == "transfer" {
								val, err := decimal.NewFromString(op.Value)
								if err == nil {
									curr := op.Ticker
									val = val.Shift(-op.Decimals)
									// Here ntx.Sender == ntx.Receiver, so let's compare with Sender
									// If op.Receiver is the user, this is a TO TX, otherwise a FROM
									if op.Receiver == ntx.Sender {
										t.Items["To"] = append(t.Items["To"], tx.Value{Code: curr, Amount: val})
									} else if op.Sender == ntx.Sender {
										t.Items["From"] = append(t.Items["From"], tx.Value{Code: curr, Amount: val})
									}
									t.FiscalCateg = tx.Exchanges
								} else {
									// If val is empty, this is FEES, but may be a malformed TX
									t.FiscalCateg = tx.Fees
								}
							}
						}
					} else if function == "SaveKeyValue" {
						// This is a specific TX, during initialisation of the Elrond wallet
						// It needs to be treated as only Fees
						t.FiscalCateg = tx.Fees
					} else {
						// t.FiscalCateg = tx.Fees
						isUnknown = true
					}
				} else {
					// If status=failed
					t.FiscalCateg = tx.Fees
				}
				txs = append(txs, t)
				usedNor[ntx.TxHash] = true
			} else if toIsOwn {
				// If user is receiver
				desc := ntx.SenderAssets.Name + " " + ntx.Action.Name + " : " + ntx.Action.Description
				t := tx.TX{Timestamp: time.Unix(ntx.Timestamp, 0), ID: ntx.TxHash, Source: source, Wallet: wallet, Method: "API", Note: desc}
				t.Items = make(map[string]tx.Values)
				t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "EGLD", Amount: ntxFee})
				if ntx.Status == "success" {
					if strings.Contains(function, "cashback") {
						if !ntxValue.IsZero() {
							t.Items["To"] = append(t.Items["To"], tx.Value{Code: "EGLD", Amount: ntxValue})
							t.FiscalCateg = tx.CommercialRebates
						} else {
							t.FiscalCateg = tx.Fees
						}
					} else if strings.Contains(function, "registration") {
						if !ntxValue.IsZero() {
							// Here we are in the specific case with "Maiar: Registration Funding" when opening an account
							// This is a very little amount, but it can deorganise the global amount
							t.Items["To"] = append(t.Items["To"], tx.Value{Code: "EGLD", Amount: ntxValue})
							t.FiscalCateg = tx.CommercialRebates
						} else {
							t.FiscalCateg = tx.Fees
						}
					} else {
						if !ntxValue.IsZero() {
							t.Items["To"] = append(t.Items["To"], tx.Value{Code: "EGLD", Amount: ntxValue})
							t.FiscalCateg = tx.Deposits
						} else {
							t.FiscalCateg = tx.Fees
						}
					}
				} else {
					// If status=failed
					t.FiscalCateg = tx.Fees
				}
				txs = append(txs, t)
				usedNor[ntx.TxHash] = true
			} else if fromIsOwn {
				// If user is the sender
				desc := ntx.SenderAssets.Name + " " + ntx.Action.Name + " : " + ntx.Action.Description
				t := tx.TX{Timestamp: time.Unix(ntx.Timestamp, 0), ID: ntx.TxHash, Source: source, Wallet: "Elrond", Method: "API", Note: desc}
				t.Items = make(map[string]tx.Values)
				t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "EGLD", Amount: ntxFee})
				if ntx.Status == "success" {
					if strings.Contains(function, "delegate") {
						if !ntxValue.IsZero() {
							t.Items["From"] = append(t.Items["From"], tx.Value{Code: "EGLD", Amount: ntxValue})
							t.FiscalCateg = tx.Withdrawals
						} else {
							t.FiscalCateg = tx.Fees
						}
					} else if strings.Contains(function, "unDelegate") {
						// Both are amounts unengage from a staker (category=stake)
						if ntxValue.IsZero() {
							if len(ntx.Action.ElrondArguments.Value) != 0 {
								val, err := decimal.NewFromString(ntx.Action.ElrondArguments.Value)
								if err == nil {
									val = val.Shift(-18)
								}
								t.Items["To"] = append(t.Items["To"], tx.Value{Code: "EGLD", Amount: val})
								t.FiscalCateg = tx.Deposits
							} else {
								t.FiscalCateg = tx.Fees
							}
						} else {
							t.FiscalCateg = tx.Fees
						}
					} else if strings.Contains(function, "withdraw") {
						// Both are amounts unengage from a staker (category=stake)
						if ntxValue.IsZero() {
							if len(ntx.Action.ElrondArguments.Value) != 0 {
								val, err := decimal.NewFromString(ntx.Action.ElrondArguments.Value)
								if err == nil {
									val = val.Shift(-18)
								}
								t.Items["From"] = append(t.Items["From"], tx.Value{Code: "EGLD", Amount: val})
								t.FiscalCateg = tx.Withdrawals
							} else {
								t.FiscalCateg = tx.Fees
							}
						} else {
							t.FiscalCateg = tx.Fees
						}
					} else if strings.Contains(function, "wrapEgld") {
						if ntx.Action.ElrondArguments.Token.Token != "" {
							if len(ntx.Action.ElrondArguments.Token.Value) != 0 {
								val, err := decimal.NewFromString(ntx.Action.ElrondArguments.Token.Value)
								if err == nil {
									val = val.Shift(-18)
								}
								if !ntxValue.IsZero() {
									t.Items["From"] = append(t.Items["From"], tx.Value{Code: "EGLD", Amount: ntxValue})
								}
								t.Items["To"] = append(t.Items["To"], tx.Value{Code: ntx.Action.ElrondArguments.Token.Ticker, Amount: val})
								t.FiscalCateg = tx.Exchanges
							}
						} else {
							t.FiscalCateg = tx.Fees
						}
					} else if strings.Contains(function, "swapTokensFixedInput") {
						if reflect.DeepEqual(ntx.Action.ElrondArguments.Token, ElrondToken{}) {
							// "transfer{}" does not exist each time "arguments" is here, we need to filter this...
							if len(ntx.Action.ElrondArguments.ElrondTransfers) > 0 {
								// Get both in and out values
								in, err := decimal.NewFromString(ntx.Action.ElrondArguments.ElrondTransfers[0].Value)
								if err == nil {
									currIn := ntx.Action.ElrondArguments.ElrondTransfers[0].Ticker
									in = in.Shift(-18)
									t.Items["From"] = append(t.Items["From"], tx.Value{Code: currIn, Amount: in})
								}
								out, err := decimal.NewFromString(ntx.Action.ElrondArguments.ElrondTransfers[1].Value)
								if err == nil {
									currOut := ntx.Action.ElrondArguments.ElrondTransfers[1].Ticker
									out = out.Shift(-18)
									t.Items["To"] = append(t.Items["To"], tx.Value{Code: currOut, Amount: out})
								}
								t.FiscalCateg = tx.Exchanges
							} else {
								t.FiscalCateg = tx.Fees
							}
						} else {
							t.FiscalCateg = tx.Fees
						}
					} else if strings.Contains(function, "claimRewards") {
						for _, op := range ntx.Operations {
							if op.Action == "transfer" {
								val, err := decimal.NewFromString(op.Value)
								if err == nil {
									curr := "EGLD"
									if op.Ticker != "" {
										curr = op.Ticker
									}
									val = val.Shift(-18)
									if op.Receiver == ntx.Sender {
										// I think that this case shoul not occur, as isSender
										t.Items["To"] = append(t.Items["To"], tx.Value{Code: curr, Amount: val})
										t.FiscalCateg = tx.Deposits
									} else if op.Sender == ntx.Sender {
										t.Items["From"] = append(t.Items["From"], tx.Value{Code: curr, Amount: val})
										t.FiscalCateg = tx.Withdrawals
									} else {
										t.FiscalCateg = tx.Fees
									}
								} else {
									// If val is empty, this is FEES, but may be a malformed TX
									t.FiscalCateg = tx.Fees
								}
							}
						}
					} else {
						// function == "register" or "saveAttestation"
						t.FiscalCateg = tx.Fees
					}
				} else {
					t.FiscalCateg = tx.Fees
				}
				txs = append(txs, t)
				usedNor[ntx.TxHash] = true
			} else {
				if exist := newType["nor"]; !exist {
					b, err := encjson.Marshal(ntx)
					if err == nil {
						newType["nor"] = true
						unknowns = append(unknowns, "nor:"+string(b))
					}
				}
			}

			if isUnknown {
				if exist := newType["nor"]; !exist {
					b, err := encjson.Marshal(ntx)
					if err == nil {
						isUnknown = false
						newType["nor"] = true
						unknowns = append(unknowns, "nor:"+string(b))
					}
				}
			}
		}
	}

	addresses = make([]string, 0)
	for address := range j.Addresses {
		addresses = append(addresses, address)
	}
	return
}
