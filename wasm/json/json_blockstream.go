package json

import (
	encjson "encoding/json"
	"errors"
	"strconv"
	"time"

	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

type BlockStreamVout struct {
	ScriptPubKey        string `json:"scriptpubkey"`
	ScriptPubKeyAsm     string `json:"scriptpubkey_asm"`
	ScriptPubKeyType    string `json:"scriptpubkey_type"`
	ScriptPubKeyAddress string `json:"scriptpubkey_address"`
	Value               int64  `json:"value"`
	ValueCommitment     string `json:"valuecommitment,omitempty"`
	Asset               string `json:"asset,omitempty"`
	AssetCommitment     string `json:"assetcommitment,omitempty"`
	PegOut              struct {
		GenesisHash         string `json:"genesis_hash"`
		ScriptPubKey        string `json:"scriptpubkey"`
		ScriptPubKeyAsm     string `json:"scriptpubkey_asm"`
		ScriptPubKeyAddress string `json:"scriptpubkey_address"`
	} `json:"pegout,omitempty"`
}

type BlockStreamTransaction struct {
	TxID     string `json:"txid"`
	Version  int    `json:"version"`
	LockTime int    `json:"locktime"`
	Size     int    `json:"size"`
	Weight   int    `json:"weight"`
	Fee      int64  `json:"fee"`
	Vin      []struct {
		TxID                  string          `json:"txid"`
		Vout                  int             `json:"vout"`
		IsCoinbase            bool            `json:"is_coinbase"`
		ScriptSig             string          `json:"scriptsig"`
		ScriptSigAsm          string          `json:"scriptsig_asm"`
		InnerRedeemScriptAsm  string          `json:"inner_redeemscript_asm"`
		InnerWitnessScriptAsm string          `json:"inner_witnessscript_asm"`
		Sequence              int64           `json:"sequence"`
		Witness               []string        `json:"witness"`
		PreVout               BlockStreamVout `json:"prevout"`
		IsPegIn               bool            `json:"is_pegin,omitempty"`
		Issuance              struct {
			AssetID               string `json:"asset_id"`
			IsReissuance          bool   `json:"is_reissuance"`
			AssetBlindingNonce    string `json:"asset_blinding_nonce"`
			AssetEntropy          string `json:"asset_entropy"`
			ContractHash          string `json:"contract_hash"`
			AssetAmount           string `json:"assetamount,omitempty"`
			AssetAmountCommitment string `json:"assetamountcommitment,omitempty"`
			TokenAmount           string `json:"tokenamount,omitempty"`
			TokenAmountCommitment string `json:"tokenamountcommitment,omitempty"`
		} `json:"issuance,omitempty"`
	} `json:"vin"`
	Vout   []BlockStreamVout `json:"vout"`
	Status struct {
		Confirmed   bool   `json:"confirmed"`
		BlockHeight int    `json:"block_height,omitempty"`
		BlockHash   string `json:"block_hash,omitempty"`
		BlockTime   int    `json:"block_time,omitempty"`
	} `json:"status"`
}

type BlockstreamJSON struct {
	Addresses    map[string]bool                   `json:"btc_addrs"`
	Transactions map[string]BlockStreamTransaction `json:"txs"`
}

var blockstream = &BlockstreamJSON{}

// Digest : Digest a BlockStream AddressTransactions JSON file
func (j BlockstreamJSON) Digest(jsonData []byte) (txs tx.TXs, source string, wallet string, unknowns []string, addresses []string, err error) {
	err = encjson.Unmarshal(jsonData, &j)
	if len(j.Addresses) == 0 || len(j.Transactions) == 0 { // Mandatory fields
		err = errors.New("JSON Format does not match")
	}
	if err != nil {
		return
	}
	source = "BlockStream API"
	wallet = "Bitcoin"
	newType := make(map[string]bool)
	for _, transaction := range j.Transactions {
		var valueIn int64
		isInVinPrevVout := false
		for _, vin := range transaction.Vin {
			if exist := j.Addresses[vin.PreVout.ScriptPubKeyAddress]; exist {
				valueIn -= vin.PreVout.Value
				isInVinPrevVout = true
			}
		}
		var totalValueOut int64
		var valueOut []int64
		isInVout := false
		dest := ""
		for _, vout := range transaction.Vout {
			if _, exist := j.Addresses[vout.ScriptPubKeyAddress]; exist {
				totalValueOut += vout.Value
				valueOut = append(valueOut, vout.Value)
				isInVout = true
			} else {
				if dest == "" {
					dest = " destination"
				}
				dest += " " + vout.ScriptPubKeyAddress
			}
		}
		if valueIn+totalValueOut == 0 {
			if exist := newType["zerovalue"]; !exist {
				b, err := encjson.Marshal(transaction)
				if err == nil {
					newType["zerovalue"] = true
					unknowns = append(unknowns, "zerovalue:"+string(b))
				}
			}
		}
		if isInVinPrevVout {
			t := tx.TX{Timestamp: time.Unix(int64(transaction.Status.BlockTime), 0), ID: transaction.TxID, Source: source, Wallet: "Bitcoin", Method: "API", Note: strconv.Itoa(transaction.Status.BlockHeight) + dest}
			t.Items = make(map[string]tx.Values)
			t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "BTC", Amount: decimal.New(transaction.Fee, -8)})
			if isInVout && dest == "" {
				t.Items["From"] = append(t.Items["From"], tx.Value{Code: "BTC", Amount: decimal.New(-valueIn-transaction.Fee, -8)})
				for _, vo := range valueOut {
					t.Items["To"] = append(t.Items["To"], tx.Value{Code: "BTC", Amount: decimal.New(int64(vo), -8)})
				}
				t.FiscalCateg = tx.Transfers
				txs = append(txs, t)
			} else {
				t.Items["From"] = append(t.Items["From"], tx.Value{Code: "BTC", Amount: decimal.New(-totalValueOut-valueIn-transaction.Fee, -8)})
				t.FiscalCateg = tx.Withdrawals
				txs = append(txs, t)
			}
		} else if isInVout {
			t := tx.TX{Timestamp: time.Unix(int64(transaction.Status.BlockTime), 0), ID: transaction.TxID, Source: source, Wallet: "Bitcoin", Method: "API", Note: strconv.Itoa(transaction.Status.BlockHeight)}
			t.Items = make(map[string]tx.Values)
			for _, vo := range valueOut {
				t.Items["To"] = append(t.Items["To"], tx.Value{Code: "BTC", Amount: decimal.New(int64(vo), -8)})
			}
			t.FiscalCateg = tx.Deposits
			txs = append(txs, t)
		} else {
			if exist := newType["lastelse"]; !exist {
				b, err := encjson.Marshal(transaction)
				if err == nil {
					newType["lastelse"] = true
					unknowns = append(unknowns, "zerovalue:"+string(b))
				}
			}
		}
	}

	addresses = make([]string, 0)
	for address := range j.Addresses {
		addresses = append(addresses, address)
	}
	return
}
