package json

import (
	encjson "encoding/json"
	"errors"
	"reflect"
	"strconv"
	"time"

	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

// CdCExchangeJSON : CdC Exchange JSON Format
type CdCExchangeJSON struct {
	Withs struct {
		FinanceList []struct {
			Symbol        string      `json:"symbol"`
			Reason        string      `json:"reason"`
			Amount        string      `json:"amount"`
			Fee           float64     `json:"fee"`
			UpdateAt      string      `json:"updateAt"`
			TxID          string      `json:"txid"`
			Label         string      `json:"label"`
			AddressTo     string      `json:"addressTo"`
			Network       interface{} `json:"network"`
			CreatedAt     string      `json:"createdAt"`
			TxIDAddr      string      `json:"txidAddr"`
			UpdateAtTime  int64       `json:"updateAtTime"`
			CreatedAtTime int64       `json:"createdAtTime"`
			ID            int         `json:"id"`
			StatusText    string      `json:"status_text"`
			Status        int         `json:"status"`
		} `json:"financeList"`
		Count    int `json:"count"`
		Pagesize int `json:"pageSize"`
	} `json:"withs"`
	Deps struct {
		FinanceList []struct {
			Symbol        string      `json:"symbol"`
			Amount        string      `json:"amount"`
			UpdateAt      string      `json:"updateAt"`
			TxID          string      `json:"txid"`
			NoteStatus    interface{} `json:"note_status"`
			Confirmdesc   string      `json:"confirmDesc"`
			AddressTo     string      `json:"addressTo"`
			Network       interface{} `json:"network"`
			CreatedAt     string      `json:"createdAt"`
			TxIDAddr      string      `json:"txidAddr"`
			UpdateAtTime  int64       `json:"updateAtTime"`
			CreatedAtTime int64       `json:"createdAtTime"`
			StatusText    string      `json:"status_text"`
			Status        int         `json:"status"`
		} `json:"financeList"`
		Count    int `json:"count"`
		Pagesize int `json:"pageSize"`
	} `json:"deps"`
	Cros struct {
		HistoryList []struct {
			StakeAmount    string      `json:"stakeAmount"`
			Apr            float64     `json:"apr,string"`
			CoinSymbol     string      `json:"coinSymbol"`
			CreateTime     string      `json:"createTime"`
			Extra          interface{} `json:"extra"`
			Destination    string      `json:"destination"`
			InterestAmount string      `json:"interestAmount"`
			CreatedAtTime  int64       `json:"createdAtTime"`
			StatusText     string      `json:"status_text"`
			Status         int         `json:"status"`
		} `json:"historyList"`
		Count    int `json:"count"`
		Pagesize int `json:"pageSize"`
	} `json:"cros"`
	Sstake struct {
		Count                   int `json:"count"`
		Pagesize                int `json:"pageSize"`
		SoftStakingInterestList []struct {
			Principal       string      `json:"principal"`
			Reason          interface{} `json:"reason"`
			Amount          string      `json:"amount"`
			Apr             float64     `json:"apr,string"`
			CoinSymbol      string      `json:"coinSymbol"`
			CalculateDate   int64       `json:"calculateDate"`
			Ctime           int64       `json:"ctime"`
			ID              int         `json:"id"`
			StakedCROAmount string      `json:"stakedCroAmount"`
			Mtime           int64       `json:"mtime"`
			UserID          int         `json:"userId"`
			Status          int         `json:"status"`
		} `json:"softStakingInterestList"`
	} `json:"sstake"`
	Rebs struct {
		HistoryList []struct {
			CreateTime       string  `json:"createTime"`
			RebateAmount     string  `json:"rebateAmount"`
			RebatePercentage float64 `json:"rebatePercentage,string"`
			FeePaid          string  `json:"feePaid"`
			CreatedAtTime    int64   `json:"createdAtTime"`
			CoinSymbol       string  `json:"coinSymbol"`
			Destination      string  `json:"destination"`
			StatusText       string  `json:"status_text"`
			Status           int     `json:"status"`
			Extra            string  `json:"extra"`
		} `json:"historyList"`
		Count    int `json:"count"`
		Pagesize int `json:"pageSize"`
	} `json:"rebs"`
	Syn struct {
		Activities []struct {
			DeliveredSize          string `json:"deliveredSize"`
			ActivityCROn           string `json:"activityCron"`
			UserStatus             string `json:"userStatus"`
			ID                     string `json:"id"`
			AllocationTime         string `json:"allocationTime"`
			MinCommittedCRO        string `json:"minCommittedCRO"`
			RefundedCRO            string `json:"refundedCRO"`
			PoolSize               string `json:"poolSize"`
			DiscountedPrice        string `json:"discountedPrice"`
			MinPurchased           string `json:"minPurchased"`
			DiscountRate           string `json:"discountRate"`
			AnnouncementTime       string `json:"announcementTime"`
			ActivityStatus         string `json:"activityStatus"`
			PriceDeterminationTime string `json:"priceDeterminationTime"`
			AllocatedPriceCRO      string `json:"allocatedPriceCRO"`
			UserID                 string `json:"userId"`
			ActivityModifyTime     string `json:"activityModifyTime"`
			EndTime                string `json:"endTime"`
			DeliveryTime           int64  `json:"deliveryTime,string"`
			SyndicateCoin          string `json:"syndicateCoin"`
			TotalCommittedCRO      string `json:"totalCommittedCro"`
			ActivityCreateTime     string `json:"activityCreateTime"`
			UserEmailStatus        string `json:"userEmailStatus"`
			UserCreateTime         int64  `json:"userCreateTime,string"`
			PoolSizeCapUSD         string `json:"poolSizeCapUSD"`
			StartTime              string `json:"startTime"`
			AllocatedVolume        string `json:"allocatedVolume"`
			CommittedCRO           string `json:"committedCRO"`
			AllocatedSize          string `json:"allocatedSize"`
			AllocatedPriceUSD      string `json:"allocatedPriceUSD"`
			UserModifyTime         string `json:"userModifyTime"`
		} `json:"activities"`
	} `json:"syn"`
	Spe struct {
		HistoryList []struct {
			Amount        string `json:"amount"`
			CoinSymbol    string `json:"coinSymbol"`
			CreateTime    string `json:"createTime"`
			Description   string `json:"description"`
			CreatedAtTime int64  `json:"createdAtTime"`
		} `json:"historyList"`
		Count    int `json:"count"`
		Pagesize int `json:"pageSize"`
	} `json:"spe"`
	Tcom struct {
		Total int `json:"total"`
		Data  []struct {
			Commission             string `json:"commission"`
			ID                     string `json:"id"`
			MTime                  int64  `json:"mtime,string"`
			Status                 int    `json:"status,string"`
			NetTradingFee          string `json:"netTradingFee"`
			ReferralRelationshipID string `json:"referralRelationshipId"`
			CTime                  int64  `json:"ctime,string"`
			TradingFeeRebate       string `json:"tradingFeeRebate"`
		} `json:"data"`
	} `json:"tcom"`
	Bon struct {
		Total int `json:"total"`
		Data  []struct {
			ReferralRelationshipID string `json:"referralRelationshipId"`
			ReferralBonusInCRO     string `json:"referralBonusInCro"`
			CTime                  int64  `json:"ctime,string"`
			ID                     string `json:"id"`
			MTime                  int64  `json:"mtime,string"`
			ReferralBonusTierID    string `json:"referralBonusTierId"`
			Status                 int    `json:"status,string"`
			ReferralBonusInUSD     string `json:"referralBonusInUsd"`
		} `json:"data"`
	} `json:"bon"`
	Rew struct {
		SignupBonusCreatedAt         string `json:"signUpBonusCreatedAt"`
		TotalEarnFromReferral        string `json:"totalEarnFromReferral"`
		TotalNumberOfUsersBeReferred string `json:"totalNumberOfUsersBeReferred"`
		TotalTradeCommission         string `json:"totalTradeCommission"`
		SignupBonus                  string `json:"signUpBonus"`
		TotalReferralBonus           string `json:"totalReferralBonus"`
		TotalNumberOfUsersSignedUp   string `json:"totalNumberOfUsersSignedUp"`
	} `json:"rew"`
}

var cdcExchangeExportJS CdCExchangeJSON

// Digest : Digest a CdC Exchange Export JS JSON file
func (cdcEx CdCExchangeJSON) Digest(jsonData []byte) (txs tx.TXs, source string, wallet string, unknowns []string, addresses []string, err error) {
	err = encjson.Unmarshal(jsonData, &cdcEx)
	if (len(cdcEx.Withs.FinanceList) == 0 || cdcEx.Withs.Count == 0) &&
		(len(cdcEx.Deps.FinanceList) == 0 || cdcEx.Deps.Count == 0) &&
		(len(cdcEx.Cros.HistoryList) == 0 || cdcEx.Cros.Count == 0) &&
		(len(cdcEx.Sstake.SoftStakingInterestList) == 0 || cdcEx.Sstake.Count == 0) &&
		(len(cdcEx.Rebs.HistoryList) == 0 || cdcEx.Rebs.Count == 0) &&
		(len(cdcEx.Syn.Activities) == 0) &&
		(len(cdcEx.Spe.HistoryList) == 0 || cdcEx.Spe.Count == 0) &&
		(len(cdcEx.Tcom.Data) == 0 || cdcEx.Tcom.Total == 0) &&
		(len(cdcEx.Bon.Data) == 0 || cdcEx.Bon.Total == 0) { // Mandatory fields
		err = errors.New("JSON Format does not match")
	}
	if err != nil {
		return
	}
	void := CdCExchangeJSON{}
	if reflect.DeepEqual(cdcEx, void) {
		err = errors.New("Empty")
		return
	}
	source = "CdC Exchange ExportJS"
	wallet = "Crypto.com Exchange"
	for _, w := range cdcEx.Withs.FinanceList {
		if w.StatusText == "Completed" {
			t := tx.TX{Source: source, Wallet: "Crypto.com Exchange", Method: "ExportJS", Timestamp: time.UnixMilli(w.UpdateAtTime).UTC(), ID: w.TxID, FiscalCateg: tx.Withdrawals, Note: w.AddressTo}
			t.Items = make(map[string]tx.Values)
			amount, err := decimal.NewFromString(w.Amount)
			if err == nil {
				t.Items["From"] = append(t.Items["From"], tx.Value{Code: w.Symbol, Amount: amount})
			}
			if w.Fee != 0 {
				t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: w.Symbol, Amount: decimal.NewFromFloat(w.Fee)})
			}
			txs = append(txs, t)
		}
	}
	for _, d := range cdcEx.Deps.FinanceList {
		if d.StatusText == "Payment received" {
			t := tx.TX{Source: source, Wallet: "Crypto.com Exchange", Method: "ExportJS", Timestamp: time.UnixMilli(d.UpdateAtTime).UTC(), ID: d.TxID, FiscalCateg: tx.Deposits, Note: d.AddressTo}
			t.Items = make(map[string]tx.Values)
			amount, err := decimal.NewFromString(d.Amount)
			if err == nil {
				t.Items["To"] = append(t.Items["To"], tx.Value{Code: d.Symbol, Amount: amount})
				txs = append(txs, t)
			}
		}
	}
	for _, cs := range cdcEx.Cros.HistoryList {
		if cs.StatusText == "Completed" {
			t := tx.TX{Source: source, Wallet: "Crypto.com Exchange", Method: "ExportJS", Timestamp: time.UnixMilli(cs.CreatedAtTime).UTC(), FiscalCateg: tx.Interests, Note: "CRO Stake Interest " + cs.StakeAmount + " " + cs.CoinSymbol + " at " + strconv.FormatFloat(cs.Apr*100, 'f', 1, 64) + "%"}
			t.GenerateID()
			t.Items = make(map[string]tx.Values)
			amount, err := decimal.NewFromString(cs.InterestAmount)
			if err == nil {
				t.Items["To"] = append(t.Items["To"], tx.Value{Code: cs.CoinSymbol, Amount: amount})
				txs = append(txs, t)
			}
		}
	}
	for _, ss := range cdcEx.Sstake.SoftStakingInterestList {
		if ss.Status == 2 {
			t := tx.TX{Source: source, Wallet: "Crypto.com Exchange", Method: "ExportJS", Timestamp: time.UnixMilli(ss.CalculateDate).UTC(), ID: strconv.Itoa(ss.ID), FiscalCateg: tx.Interests, Note: "Soft Stake Interest " + ss.StakedCROAmount + " " + ss.CoinSymbol + " at " + strconv.FormatFloat(ss.Apr*100, 'f', 1, 64) + "%"}
			t.Items = make(map[string]tx.Values)
			amount, err := decimal.NewFromString(ss.Amount)
			if err == nil {
				t.Items["To"] = append(t.Items["To"], tx.Value{Code: ss.CoinSymbol, Amount: amount})
				txs = append(txs, t)
			}
		}
	}
	for _, r := range cdcEx.Rebs.HistoryList {
		if r.StatusText == "Completed" {
			t := tx.TX{Source: source, Wallet: "Crypto.com Exchange", Method: "ExportJS", Timestamp: time.UnixMilli(r.CreatedAtTime).UTC(), FiscalCateg: tx.CommercialRebates, Note: "Rebate on Fee paid " + r.FeePaid + " " + r.CoinSymbol + " at " + strconv.FormatFloat(r.RebatePercentage*100, 'f', 1, 64) + "%"}
			t.GenerateID()
			t.Items = make(map[string]tx.Values)
			amount, err := decimal.NewFromString(r.RebateAmount)
			if err == nil {
				t.Items["To"] = append(t.Items["To"], tx.Value{Code: r.CoinSymbol, Amount: amount})
				txs = append(txs, t)
			}
		}
	}
	for _, s := range cdcEx.Syn.Activities {
		t := tx.TX{Source: source, Wallet: "Crypto.com Exchange", Method: "ExportJS", Timestamp: time.UnixMilli(s.DeliveryTime).UTC(), ID: s.ID, FiscalCateg: tx.Exchanges, Note: "Syndicate"}
		t.Items = make(map[string]tx.Values)
		allocatedVolume, err1 := decimal.NewFromString(s.AllocatedVolume)
		if err1 == nil {
			t.Items["To"] = append(t.Items["To"], tx.Value{Code: s.SyndicateCoin, Amount: allocatedVolume})
		}
		committedCRO, err2 := decimal.NewFromString(s.CommittedCRO)
		var err3 error
		if err2 == nil {
			refundedCRO, err3 := decimal.NewFromString(s.RefundedCRO)
			if err3 == nil {
				t.Items["From"] = append(t.Items["From"], tx.Value{Code: "CRO", Amount: committedCRO.Sub(refundedCRO)})
			}
		}
		if err1 == nil && err2 == nil && err3 == nil {
			txs = append(txs, t)
		}
	}
	for _, s := range cdcEx.Spe.HistoryList {
		t := tx.TX{Source: source, Wallet: "Crypto.com Exchange", Method: "ExportJS", Timestamp: time.UnixMilli(s.CreatedAtTime).UTC(), FiscalCateg: tx.Minings, Note: s.Description}
		t.GenerateID()
		t.Items = make(map[string]tx.Values)
		amount, err := decimal.NewFromString(s.Amount)
		if err == nil {
			t.Items["To"] = append(t.Items["To"], tx.Value{Code: s.CoinSymbol, Amount: amount})
			txs = append(txs, t)
		}
	}
	for _, tc := range cdcEx.Tcom.Data {
		if tc.Status == 1 {
			t := tx.TX{Source: source, Wallet: "Crypto.com Exchange", Method: "ExportJS", Timestamp: time.UnixMilli(tc.MTime).UTC(), ID: tc.ID, FiscalCateg: tx.Referrals, Note: "Trade Commission"}
			t.Items = make(map[string]tx.Values)
			amount, err := decimal.NewFromString(tc.Commission)
			if err == nil {
				t.Items["To"] = append(t.Items["To"], tx.Value{Code: "CRO", Amount: amount})
				txs = append(txs, t)
			}
		}
	}
	for _, b := range cdcEx.Bon.Data {
		if b.Status == 2 {
			t := tx.TX{Source: source, Wallet: "Crypto.com Exchange", Method: "ExportJS", Timestamp: time.UnixMilli(b.MTime).UTC(), ID: b.ID, FiscalCateg: tx.Referrals, Note: "Referral Bonus"}
			t.Items = make(map[string]tx.Values)
			amount, err := decimal.NewFromString(b.ReferralBonusInCRO)
			if err == nil {
				t.Items["To"] = append(t.Items["To"], tx.Value{Code: "CRO", Amount: amount})
				txs = append(txs, t)
			}
		}
	}
	if cdcEx.Rew.SignupBonus != "0" {
		signupBonusCreatedAt, err := strconv.ParseInt(cdcEx.Rew.SignupBonusCreatedAt, 10, 64)
		if err == nil {
			t := tx.TX{Source: source, Wallet: "Crypto.com Exchange", Method: "ExportJS", Timestamp: time.UnixMilli(signupBonusCreatedAt).UTC(), FiscalCateg: tx.CommercialRebates, Note: "Signup Bonus"}
			t.GenerateID()
			t.Items = make(map[string]tx.Values)
			amount, err := decimal.NewFromString(cdcEx.Rew.SignupBonus)
			if err == nil {
				t.Items["To"] = append(t.Items["To"], tx.Value{Code: "CRO", Amount: amount})
				txs = append(txs, t)
			}
		}
	}
	return
}
