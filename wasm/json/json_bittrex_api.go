package json

import (
	encjson "encoding/json"
	"errors"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

type BittrexDeposit struct {
	ID               string    `json:"id"`
	CurrencySymbol   string    `json:"currencySymbol"`
	Quantity         string    `json:"quantity"`
	CryptoAddress    string    `json:"cryptoAddress"`
	Confirmations    int       `json:"confirmations"`
	UpdatedAt        time.Time `json:"updatedAt"`
	CompletedAt      time.Time `json:"completedAt"`
	Status           string    `json:"status"`
	Source           string    `json:"source"`
	TxCost           string    `json:"txCost"`
	TxID             string    `json:"txId"`
	CreatedAt        time.Time `json:"createdAt"`
	CryptoAddressTag string    `json:"cryptoAddressTag,omitempty"`
}

type BittrexTrade struct {
	ID            string    `json:"id"`
	MarketSymbol  string    `json:"marketSymbol"`
	Direction     string    `json:"direction"`
	Type          string    `json:"type"`
	Quantity      string    `json:"quantity"`
	Limit         string    `json:"limit"`
	Ceiling       string    `json:"ceiling"`
	TimeInForce   string    `json:"timeInForce"`
	ClientOrderID string    `json:"clientOrderId"`
	FillQuantity  string    `json:"fillQuantity"`
	Commission    string    `json:"commission"`
	Proceeds      string    `json:"proceeds"`
	Status        string    `json:"status"`
	CreatedAt     time.Time `json:"createdAt"`
	UpdatedAt     time.Time `json:"updatedAt"`
	ClosedAt      time.Time `json:"closedAt"`
	OrderToCancel struct {
		Type string `json:"type"`
		ID   string `json:"id"`
	} `json:"orderToCancel"`
}

type BittrexWithdrawal struct {
	ID               string    `json:"id"`
	CurrencySymbol   string    `json:"currencySymbol"`
	Quantity         string    `json:"quantity"`
	CryptoAddress    string    `json:"cryptoAddress"`
	UpdatedAt        time.Time `json:"updatedAt"`
	CompletedAt      time.Time `json:"completedAt"`
	Status           string    `json:"status"`
	TxCost           string    `json:"txCost"`
	TxID             string    `json:"txId"`
	CreatedAt        time.Time `json:"createdAt"`
	CryptoAddressTag string    `json:"cryptoAddressTag,omitempty"`
}

type BittrexDeposits struct {
	Objects   map[string]BittrexDeposit `json:"objects"`
	StartDate string                    `json:"startDate"`
}

type BittrexTrades struct {
	Objects   map[string]BittrexTrade `json:"objects"`
	StartDate string                  `json:"startDate"`
}

type BittrexWithdrawals struct {
	Objects   map[string]BittrexWithdrawal `json:"objects"`
	StartDate string                       `json:"startDate"`
}

type BittrexJSON struct {
	Deposits    BittrexDeposits    `json:"deposits"`
	Trades      BittrexTrades      `json:"trades"`
	Withdrawals BittrexWithdrawals `json:"withdrawals"`
}

var bittrexAPI = &BittrexJSON{}

// Digest : Digest a Bittrex API JSON file
func (j BittrexJSON) Digest(jsonData []byte) (txs tx.TXs, source string, wallet string, unknowns []string, addresses []string, err error) {
	newType := make(map[string]bool)
	err = encjson.Unmarshal(jsonData, &j)
	if j.Deposits.StartDate == "" { // Mandatory fields
		err = errors.New("JSON Format does not match")
	}
	if err != nil {
		return
	}
	source = "Bittrex API"
	wallet = "Bittrex"
	symRplcr := strings.NewReplacer(
		"REPV2", "REP",
	)
	for tID, tData := range j.Trades.Objects {
		t := tx.TX{Timestamp: tData.ClosedAt, ID: tData.ID, Source: source, Wallet: wallet, Method: "API", Note: tData.Direction}
		symbolSlice := strings.Split(tData.MarketSymbol, "-")
		skip := false
		fillQuantity, err := decimal.NewFromString(tData.FillQuantity)
		if err != nil {
			fmt.Println(tID, "Error while parsing FillQuantity", tData.FillQuantity)
			continue
		}
		commission, err := decimal.NewFromString(tData.Commission)
		if err != nil {
			log.Println(tID, "Error while parsing Commission", tData.Commission)
			continue
		}
		proceeds, err := decimal.NewFromString(tData.Proceeds)
		if err != nil {
			log.Println(tID, "Error while parsing Proceeds", tData.Proceeds)
			continue
		}
		var to, from tx.Value
		if tData.Direction == "BUY" {
			to = tx.Value{Code: symRplcr.Replace(symbolSlice[0]), Amount: fillQuantity}
			from = tx.Value{Code: symRplcr.Replace(symbolSlice[1]), Amount: proceeds}
		} else if tData.Direction == "SELL" {
			to = tx.Value{Code: symRplcr.Replace(symbolSlice[1]), Amount: proceeds}
			from = tx.Value{Code: symRplcr.Replace(symbolSlice[0]), Amount: fillQuantity}
		} else {
			skip = true
			if exist := newType[tData.Direction]; !exist {
				tData.ClosedAt = time.Now()
				tData.Commission = "0.1"
				tData.CreatedAt = time.Now()
				tData.FillQuantity = "1.0"
				tData.ID = "5678"
				tData.Limit = "1.0"
				tData.Proceeds = "1.0"
				tData.Quantity = "1.0"
				tData.UpdatedAt = time.Now()
				b, err := encjson.Marshal(tData)
				if err == nil {
					newType[tData.Direction] = true
					unknowns = append(unknowns, string(b))
				}
			}
		}
		if !skip {
			t.Items = make(map[string]tx.Values)
			t.Items["From"] = append(t.Items["From"], from)
			t.Items["To"] = append(t.Items["To"], to)
			if !commission.IsZero() {
				t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: symRplcr.Replace(symbolSlice[1]), Amount: commission})
			}
			if to.IsFiat() && from.IsFiat() {
				//ignore
				continue
			} else if to.IsFiat() {
				t.FiscalCateg = tx.CashOut
			} else if from.IsFiat() {
				t.FiscalCateg = tx.CashIn
			} else {
				t.FiscalCateg = tx.Exchanges
			}
			txs = append(txs, t)
		}
	}
	for tID, tData := range j.Deposits.Objects {
		quantity, err := decimal.NewFromString(tData.Quantity)
		if err != nil {
			log.Println(tID, "Error while parsing Quantity", tData.Quantity)
			continue
		}
		to := tx.Value{Code: tData.CurrencySymbol, Amount: quantity}
		if !to.IsFiat() {
			t := tx.TX{Timestamp: tData.CompletedAt, ID: tData.ID, Source: source, Wallet: wallet, Method: "API", Note: tData.CryptoAddress}
			t.Items = make(map[string]tx.Values)
			t.Items["To"] = append(t.Items["To"], to)
			t.FiscalCateg = tx.Deposits
			txs = append(txs, t)
		}
	}
	for tID, tData := range j.Withdrawals.Objects {
		quantity, err := decimal.NewFromString(tData.Quantity)
		if err != nil {
			log.Println(tID, "Error while parsing Quantity", tData.Quantity)
			continue
		}
		from := tx.Value{Code: tData.CurrencySymbol, Amount: quantity}
		if !from.IsFiat() {
			t := tx.TX{Timestamp: tData.CompletedAt, ID: tData.ID, Source: source, Wallet: wallet, Method: "API", Note: tData.CryptoAddress}
			t.Items = make(map[string]tx.Values)
			t.Items["From"] = append(t.Items["From"], from)
			if tData.TxCost != "" {
				fee, err := decimal.NewFromString(tData.TxCost)
				if err != nil {
					log.Println(tID, "Error while parsing Fee : ", tData.TxCost)
					continue
				}
				if !fee.IsZero() {
					t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: tData.CurrencySymbol, Amount: fee})
				}
			}
			t.FiscalCateg = tx.Withdrawals
			txs = append(txs, t)
		}
	}
	return
}
