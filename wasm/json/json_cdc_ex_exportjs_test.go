package json

import (
	"testing"
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/google/go-cmp/cmp"
	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

func TestCdCExchangeJSON_Digest(t *testing.T) {
	type args struct {
		jsonData []byte
	}
	tests := []struct {
		name       string
		cdcEx      CdCExchangeJSON
		args       args
		wantTxs    tx.TXs
		wantSource string
		wantErr    bool
	}{
		{
			name:    "Void",
			wantErr: true,
		},
		{
			name: "Broken",
			args: args{
				jsonData: []byte("{"),
			},
			wantErr: true,
		},
		{
			name: "Empty",
			args: args{
				jsonData: []byte("{}"),
			},
			wantErr: true,
		},
		{
			name: "Withdrawals",
			args: args{
				jsonData: []byte("{\"withs\":{\"financeList\":[{\"symbol\":\"BTC\",\"reason\":\"\",\"amount\":\"1.23399969\",\"fee\":0.05,\"updateAt\":\"2020-08-21 18:50:00\",\"txid\":\"130bab8c-08dc-442b-a1b8-618b07ad140a\",\"label\":\"\",\"addressTo\":\"To Crypto.com App\",\"network\":null,\"createdAt\":\"2020-08-21 18:49:20\",\"txidAddr\":\"https://blockchair.com/bitcoin/transaction/130bab8c-08dc-442b-a1b8-618b07ad140a?from=crypto.com\",\"updateAtTime\":1598035800000,\"createdAtTime\":1598035760000,\"id\":349909,\"status_text\":\"Completed\",\"status\":5}],\"count\":1,\"pageSize\":200}}"),
			},
			wantTxs: tx.TXs{
				tx.TX{
					Timestamp:   time.Date(2020, 8, 21, 18, 50, 0, 0, time.FixedZone("UTC", 0)),
					ID:          "130bab8c-08dc-442b-a1b8-618b07ad140a",
					Source:      "CdC Exchange ExportJS",
					Method:      "ExportJS",
					Wallet:      "Crypto.com Exchange",
					FiscalCateg: tx.Withdrawals,
					Items: map[string]tx.Values{
						"From": {
							tx.Value{
								Code:   "BTC",
								Amount: decimal.New(123399969, -8),
							},
						},
						"Fee": {
							tx.Value{
								Code:   "BTC",
								Amount: decimal.New(5, -2),
							},
						},
					},
					Note: "To Crypto.com App",
				},
			},
			wantSource: "CdC Exchange ExportJS",
			wantErr:    false,
		},
		{
			name: "Deposits",
			args: args{
				jsonData: []byte("{\"deps\":{\"financeList\":[{\"symbol\":\"BTC\",\"amount\":\"1.23399969\",\"updateAt\":\"2020-08-21 18:53:39\",\"txid\":\"10c8c368-8fdf-4b61-97b4-7b89628aea98\",\"note_status\":null,\"confirmDesc\":\"0/6\",\"addressTo\":\"From Crypto.com App\",\"network\":null,\"createdAt\":\"2020-08-21 18:53:39\",\"txidAddr\":\"https://blockchair.com/bitcoin/transaction/10c8c368-8fdf-4b61-97b4-7b89628aea98?from=crypto.com\",\"updateAtTime\":1598036019000,\"createdAtTime\":1598036019000,\"status_text\":\"Payment received\",\"status\":1}],\"count\":3,\"pageSize\":200}}"),
			},
			wantTxs: tx.TXs{
				tx.TX{
					Timestamp:   time.Date(2020, 8, 21, 20, 53, 39, 0, time.FixedZone("CEST", 2*60*60)),
					ID:          "10c8c368-8fdf-4b61-97b4-7b89628aea98",
					Source:      "CdC Exchange ExportJS",
					Method:      "ExportJS",
					Wallet:      "Crypto.com Exchange",
					FiscalCateg: tx.Deposits,
					Items: map[string]tx.Values{
						"To": {
							tx.Value{
								Code:   "BTC",
								Amount: decimal.New(123399969, -8),
							},
						},
					},
					Note: "From Crypto.com App",
				},
			},
			wantSource: "CdC Exchange ExportJS",
			wantErr:    false,
		},
		{
			name: "CRO Stake Interests",
			args: args{
				jsonData: []byte("{\"cros\":{\"historyList\":[{\"stakeAmount\": \"13158.88736992\",\"apr\": \"0.1\",\"coinSymbol\": \"CRO\",\"createTime\": \"2021-02-05 00:00:00\",\"extra\": null,\"destination\": \"spot\",\"interestAmount\": \"3.60517462\",\"createdAtTime\": 1612483200000,\"status_text\": \"Completed\",\"status\": 1}],\"count\":1,\"pageSize\":200}}"),
			},
			wantTxs: tx.TXs{
				tx.TX{
					Timestamp:   time.Date(2021, 2, 5, 0, 0, 0, 0, time.FixedZone("UTC", 0)),
					ID:          "0499c6775f09f991fb46d925eff4aac84856998f0c28cfef6d53c041e2f7bac2",
					Source:      "CdC Exchange ExportJS",
					Method:      "ExportJS",
					Wallet:      "Crypto.com Exchange",
					FiscalCateg: tx.Interests,
					Items: map[string]tx.Values{
						"To": {
							tx.Value{
								Code:   "CRO",
								Amount: decimal.New(360517462, -8),
							},
						},
					},
					Note: "CRO Stake Interest 13158.88736992 CRO at 10.0%",
				},
			},
			wantSource: "CdC Exchange ExportJS",
			wantErr:    false,
		},
		{
			name: "Soft Stake Interests",
			args: args{
				jsonData: []byte("{\"sstake\": {\"count\": 1,\"pageSize\": 200,\"softStakingInterestList\": [{\"principal\": \"41293.77422825\",\"reason\": null,\"amount\": \"3.39400884\",\"apr\": \"0.03\",\"coinSymbol\": \"CRO\",\"calculateDate\": 1601164800000,\"ctime\": 1601242541000,\"id\": 106657578,\"stakedCroAmount\": \"41293.77422825\",\"mtime\": 1601242541000,\"userId\": 168160,\"status\": 2}]}}"),
			},
			wantTxs: tx.TXs{
				tx.TX{
					Timestamp:   time.Date(2020, 9, 27, 2, 0, 0, 0, time.FixedZone("CEST", 2*60*60)),
					ID:          "106657578",
					Source:      "CdC Exchange ExportJS",
					Method:      "ExportJS",
					Wallet:      "Crypto.com Exchange",
					FiscalCateg: tx.Interests,
					Items: map[string]tx.Values{
						"To": {
							tx.Value{
								Code:   "CRO",
								Amount: decimal.New(339400884, -8),
							},
						},
					},
					Note: "Soft Stake Interest 41293.77422825 CRO at 3.0%",
				},
			},
			wantSource: "CdC Exchange ExportJS",
			wantErr:    false,
		},
		{
			name: "CommercialRebates",
			args: args{
				jsonData: []byte("{\"rebs\": {\"historyList\": [],\"count\": 0,\"pageSize\": 200}}"),
			},
			// wantTxs: tx.TXs{
			// tx.TX{
			// 	Timestamp:   time.Date(2020, 9, 27, 2, 0, 0, 0, time.FixedZone("CEST", 2*60*60)),
			// 	ID:          "106657578",
			// 	Source:      "CdC Exchange ExportJS",
			//  Method:      "ExportJS",
			//  Wallet:      "Crypto.com Exchange",
			// 	FiscalCateg: tx.CommercialRebates,
			// 	Items: map[string]tx.Values{
			// 		"To": {
			// 			tx.Value{
			// 				Code:   "CRO",
			// 				Amount: decimal.New(339400884, -8),
			// 			},
			// 		},
			// 	},
			// 	Note: "Soft Stake Interest 41293.77422825 CRO at 3.0%",
			// },
			// },
			// wantSource: "CdC Exchange ExportJS",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotTxs, gotSource, _, _, _, err := tt.cdcEx.Digest(tt.args.jsonData)
			if (err != nil) != tt.wantErr {
				t.Errorf("CdCExchangeJSON.Digest() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !cmp.Equal(gotTxs, tt.wantTxs) {
				t.Errorf("CdCExchangeJSON.Digest() gotTxs = %v, want %v", spew.Sdump(gotTxs), spew.Sdump(tt.wantTxs))
			}
			if gotSource != tt.wantSource {
				t.Errorf("CdCExchangeJSON.Digest() gotSource = %v, want %v", gotSource, tt.wantSource)
			}
		})
	}
}
