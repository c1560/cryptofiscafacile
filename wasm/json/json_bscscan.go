package json

import (
	encjson "encoding/json"
	"errors"
	"time"

	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

type BscscanNormalTX struct {
	BlockHash         string  `json:"blockHash"`
	BlockNumber       string  `json:"blockNumber"`
	Confirmations     int     `json:"confirmations,string"`
	ContractAddress   string  `json:"contractAddress"`
	CumulativeGasUsed int     `json:"cumulativeGasUsed,string"`
	From              string  `json:"from"`
	Gas               int     `json:"gas,string"`
	GasPrice          *bigInt `json:"gasPrice"`
	GasUsed           int64   `json:"gasUsed,string"`
	Hash              string  `json:"hash"`
	Input             string  `json:"input"`
	IsError           int     `json:"isError,string"`
	Nonce             int     `json:"nonce,string"`
	TimeStamp         int64   `json:"timeStamp,string"`
	To                string  `json:"to"`
	TransactionIndex  int     `json:"transactionIndex,string"`
	TxReceiptStatus   string  `json:"txreceipt_status"`
	Value             *bigInt `json:"value"`
}

type BscscanInternalTX struct {
	BlockNumber     string  `json:"blockNumber"`
	ContractAddress string  `json:"contractAddress"`
	ErrCode         string  `json:"errCode"`
	From            string  `json:"from"`
	Gas             int     `json:"gas,string"`
	GasUsed         int64   `json:"gasUsed,string"`
	Hash            string  `json:"hash"`
	Input           string  `json:"input"`
	IsError         int     `json:"isError,string"`
	TimeStamp       int64   `json:"timeStamp,string"`
	To              string  `json:"to"`
	TraceID         string  `json:"traceId"`
	Type            string  `json:"type"`
	Value           *bigInt `json:"value"`
}

type BscscanTokenTX struct {
	BlockHash         string  `json:"blockHash"`
	BlockNumber       string  `json:"blockNumber"`
	Confirmations     int     `json:"confirmations,string"`
	ContractAddress   string  `json:"contractAddress"`
	CumulativeGasUsed int     `json:"cumulativeGasUsed,string"`
	From              string  `json:"from"`
	Gas               int     `json:"gas,string"`
	GasPrice          *bigInt `json:"gasPrice"`
	GasUsed           int64   `json:"gasUsed,string"`
	Hash              string  `json:"hash"`
	Input             string  `json:"input"`
	Nonce             int     `json:"nonce,string"`
	TimeStamp         int64   `json:"timeStamp,string"`
	To                string  `json:"to"`
	TokenDecimal      uint8   `json:"tokenDecimal,string"`
	TokenName         string  `json:"tokenName"`
	TokenSymbol       string  `json:"tokenSymbol"`
	TransactionIndex  int     `json:"transactionIndex,string"`
	Value             *bigInt `json:"value"`
}

type BscscanNftTX struct {
	BlockHash         string  `json:"blockHash"`
	BlockNumber       string  `json:"blockNumber"`
	Confirmations     int     `json:"confirmations,string"`
	ContractAddress   string  `json:"contractAddress"`
	CumulativeGasUsed int     `json:"cumulativeGasUsed,string"`
	From              string  `json:"from"`
	Gas               int     `json:"gas,string"`
	GasPrice          *bigInt `json:"gasPrice"`
	GasUsed           int64   `json:"gasUsed,string"`
	Hash              string  `json:"hash"`
	Input             string  `json:"input"`
	Nonce             int     `json:"nonce,string"`
	TimeStamp         int64   `json:"timeStamp,string"`
	To                string  `json:"to"`
	TokenDecimal      uint8   `json:"tokenDecimal,string"`
	TokenID           string  `json:"tokenID"`
	TokenName         string  `json:"tokenName"`
	TokenSymbol       string  `json:"tokenSymbol"`
	TransactionIndex  int     `json:"transactionIndex,string"`
}

type BscscanMiningTX struct {
	// used        bool   `json:"-"`
	BlockNumber string `json:"blockNumber"`
	BlockReward int64  `json:"blockReward,string"`
	TimeStamp   int64  `json:"timeStamp,string"`
}

type BscscanJSON struct {
	Addresses   map[string]bool              `json:"bsc_addrs"`
	InternalTXs map[string]BscscanInternalTX `json:"internal_txs"`
	MiningTXs   map[string]BscscanMiningTX   `json:"mining_txs"`
	NftTXs      map[string]BscscanNftTX      `json:"nft_txs"`
	NormalTXs   map[string]BscscanNormalTX   `json:"normal_txs"`
	TokenTXs    map[string]BscscanTokenTX    `json:"token_txs"`
}

var bscscan = &BscscanJSON{}

// Digest : Digest a Bscscan AddressTransactions JSON file
func (j BscscanJSON) Digest(jsonData []byte) (txs tx.TXs, source string, wallet string, unknowns []string, addresses []string, err error) {
	err = encjson.Unmarshal(jsonData, &j)
	if len(j.Addresses) == 0 { // Mandatory fields
		err = errors.New("JSON Format does not match")
	}
	if err != nil {
		return
	}
	source = "BinanceSmartChain API"
	wallet = "BinanceSmartChain"
	newType := make(map[string]bool)
	usedNor := make(map[string]bool)
	usedInt := make(map[string]bool)
	usedTok := make(map[string]bool)
	usedNft := make(map[string]bool)
	for _, nft := range j.NftTXs {
		if !usedNft[nft.Hash] {
			toIsOwn := j.Addresses[nft.To]
			fromIsOwn := j.Addresses[nft.From]
			if toIsOwn && fromIsOwn {
				if exist := newType["bep721self"]; !exist {
					b, err := encjson.Marshal(nft)
					if err == nil {
						newType["bep721self"] = true
						unknowns = append(unknowns, "bep721self:"+string(b))
					}
				}
			} else if toIsOwn || fromIsOwn {
				t := tx.TX{Timestamp: time.Unix(nft.TimeStamp, 0), ID: nft.Hash, Source: source, Wallet: "BinanceSmartChain", Method: "API", Note: nft.BlockNumber + " " + nft.To}
				t.Items = make(map[string]tx.Values)
				t.NFTs = make(map[string]tx.NFTs)
				gasPrice := decimal.NewFromBigInt(nft.GasPrice.Int(), -18)
				gasUsed := decimal.NewFromInt(nft.GasUsed)
				if fromIsOwn {
					if !gasPrice.IsZero() && !gasUsed.IsZero() {
						t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "BNB", Amount: gasPrice.Mul(gasUsed)})
					}
					t.NFTs["From"] = append(t.NFTs["From"], tx.NFT{ID: nft.TokenID, Name: nft.TokenName, Symbol: nft.TokenSymbol})
				} else {
					t.NFTs["To"] = append(t.NFTs["To"], tx.NFT{ID: nft.TokenID, Name: nft.TokenName, Symbol: nft.TokenSymbol})
				}
				for _, ntx := range j.NormalTXs {
					if ntx.Hash == nft.Hash {
						ntxGasPrice := decimal.NewFromBigInt(ntx.GasPrice.Int(), -18)
						ntxGasUsed := decimal.NewFromInt(ntx.GasUsed)
						if !ntxGasPrice.IsZero() && !ntxGasUsed.IsZero() {
							t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "BNB", Amount: ntxGasPrice.Mul(ntxGasUsed)})
						}
						usedNft[nft.Hash] = true
						break
					}
				}
				// if is, feeHash := cat.IsTxFee(t.Hash); is {
				// 	for _, ntx := range j.NormalTXs {
				// 		for _, fee := range strings.Split(feeHash, ";") {
				// 			if ntx.Hash == fee {
				// 				ntxGasPrice := decimal.NewFromBigInt(ntx.GasPrice.Int(), -18)
				// 				ntxGasUsed := decimal.NewFromInt(ntx.GasUsed)
				// 				if !ntxGasPrice.IsZero() && !ntxGasUsed.IsZero() &&
				// 					(!ntxGasPrice.Equal(gasPrice) || !ntxGasUsed.Equal(gasUsed)) {
				// 					t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "BNB", Amount: ntxGasPrice.Mul(ntxGasUsed)})
				// 				}
				// 				usedNor[ntx.Hash] = true
				// 			}
				// 		}
				// 	}
				// }
				// TODO : handle NFT proprely, for now only Fee are handled
				t.FiscalCateg = tx.Fees
				if _, have := t.Items["Fee"]; have {
					txs = append(txs, t)
				}
				usedNft[nft.Hash] = true
			} else {
				if exist := newType["bep721"]; !exist {
					b, err := encjson.Marshal(nft)
					if err == nil {
						newType["bep721"] = true
						unknowns = append(unknowns, "bep721:"+string(b))
					}
				}
			}
		}
	}
	for _, tok := range j.TokenTXs {
		if !usedTok[tok.Hash] {
			toIsOwn := j.Addresses[tok.To]
			fromIsOwn := j.Addresses[tok.From]
			if toIsOwn && fromIsOwn {
				found := false
				for _, tr := range txs {
					if tok.Hash == tr.ID &&
						tr.FiscalCateg == tx.Transfers {
						found = true
					}
				}
				if !found {
					t := tx.TX{Timestamp: time.Unix(tok.TimeStamp, 0), ID: tok.Hash, Source: source, Wallet: "BinanceSmartChain", Method: "API", Note: tok.BlockNumber + " " + tok.To}
					t.Items = make(map[string]tx.Values)
					tokValue := decimal.NewFromBigInt(tok.Value.Int(), -int32(tok.TokenDecimal))
					t.Items["To"] = append(t.Items["To"], tx.Value{Code: tok.TokenSymbol, Amount: tokValue})
					t.Items["From"] = append(t.Items["From"], tx.Value{Code: tok.TokenSymbol, Amount: tokValue})
					tokGasPrice := decimal.NewFromBigInt(tok.GasPrice.Int(), -18)
					tokGasUsed := decimal.NewFromInt(tok.GasUsed)
					t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "BNB", Amount: tokGasPrice.Mul(tokGasUsed)})
					t.FiscalCateg = tx.Transfers
					txs = append(txs, t)
				}
				usedTok[tok.Hash] = true
			} else if toIsOwn {
				t := tx.TX{Timestamp: time.Unix(tok.TimeStamp, 0), ID: tok.Hash, Source: source, Wallet: "BinanceSmartChain", Method: "API", Note: tok.BlockNumber + " " + tok.To}
				t.Items = make(map[string]tx.Values)
				tokValue := decimal.NewFromBigInt(tok.Value.Int(), -int32(tok.TokenDecimal))
				t.Items["To"] = append(t.Items["To"], tx.Value{Code: tok.TokenSymbol, Amount: tokValue})
				usedTok[tok.Hash] = true
				// // Add declared Fee if any
				// if is, feeHash := cat.IsTxFee(tok.Hash); is {
				// 	for _, ntx2 := range j.NormalTXs {
				// 		for _, fee := range strings.Split(feeHash, ";") {
				// 			if ntx2.Hash == fee {
				// 				usedNor[ntx2.Hash] = true
				// 				t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "BNB", Amount: ntx2.GasPrice.Mul(ntx2.GasUsed)})
				// 			}
				// 		}
				// 	}
				// }
				// Add normal Fee if any
				found := false
				for _, ntx := range j.NormalTXs {
					if ntx.Hash == tok.Hash {
						found = true
						ntxGasPrice := decimal.NewFromBigInt(ntx.GasPrice.Int(), -18)
						ntxGasUsed := decimal.NewFromInt(ntx.GasUsed)
						t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "BNB", Amount: ntxGasPrice.Mul(ntxGasUsed)})
						if tok.From == "0x0000000000000000000000000000000000000000" {
							t.FiscalCateg = tx.Deposits
							txs = append(txs, t)
						} else {
							ntxValue := decimal.NewFromBigInt(ntx.Value.Int(), -18)
							if ntxValue.IsZero() {
								t.FiscalCateg = tx.Deposits
								txs = append(txs, t)
							} else {
								t.Items["From"] = append(t.Items["From"], tx.Value{Code: "BNB", Amount: ntxValue})
								// t.FiscalCateg = tx.Swaps
								t.FiscalCateg = tx.Exchanges
								txs = append(txs, t)
							}
						}
						usedNor[ntx.Hash] = true
						usedTok[tok.Hash] = true
						break
					}
				}
				if !found {
					// Look for other tokenTX with same Hash
					for _, tok2 := range j.TokenTXs {
						if !usedTok[tok2.Hash] {
							if tok2.Hash == tok.Hash {
								usedTok[tok2.Hash] = true
								to2IsOwn := j.Addresses[tok2.To]
								from2IsOwn := j.Addresses[tok2.From]
								tok2Value := decimal.NewFromBigInt(tok2.Value.Int(), -int32(tok2.TokenDecimal))
								if to2IsOwn && from2IsOwn {
									if exist := newType["bep20self1"]; !exist {
										b, err := encjson.Marshal(tok2)
										if err == nil {
											newType["bep20self1"] = true
											unknowns = append(unknowns, "bep20self1:"+string(b))
										}
									}
								} else if to2IsOwn {
									t.Items["To"] = append(t.Items["To"], tx.Value{Code: tok2.TokenSymbol, Amount: tok2Value})
								} else if from2IsOwn {
									found = true
									t.Items["From"] = append(t.Items["From"], tx.Value{Code: tok2.TokenSymbol, Amount: tok2Value})
								}
							}
						}
					}
					if found {
						// t.FiscalCateg = tx.Swaps
						t.FiscalCateg = tx.Exchanges
						txs = append(txs, t)
					} else {
						t.FiscalCateg = tx.Deposits
						txs = append(txs, t)
					}
				}
				for _, ntx := range j.NormalTXs {
					if ntx.Hash == tok.Hash {
						usedNor[ntx.Hash] = true
						break
					}
				}
			} else if fromIsOwn {
				t := tx.TX{Timestamp: time.Unix(tok.TimeStamp, 0), ID: tok.Hash, Source: source, Wallet: "BinanceSmartChain", Method: "API", Note: tok.BlockNumber + " " + tok.To}
				t.Items = make(map[string]tx.Values)
				tokValue := decimal.NewFromBigInt(tok.Value.Int(), -int32(tok.TokenDecimal))
				t.Items["From"] = append(t.Items["From"], tx.Value{Code: tok.TokenSymbol, Amount: tokValue})
				tokGasPrice := decimal.NewFromBigInt(tok.GasPrice.Int(), -18)
				tokGasUsed := decimal.NewFromInt(tok.GasUsed)
				t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "BNB", Amount: tokGasPrice.Mul(tokGasUsed)})
				usedTok[tok.Hash] = true
				// // Add declared Fee if any
				// if is, feeHash := cat.IsTxFee(tok.Hash); is {
				// 	for _, ntx2 := range j.NormalTXs {
				// 		for _, fee := range strings.Split(feeHash, ";") {
				// 			if ntx2.Hash == fee {
				// 				usedNor[ntx2.Hash] = true
				// 				ntx2GasPrice := decimal.NewFromBigInt(ntx2.GasPrice.Int(), -18)
				// 				ntx2GasUsed := decimal.NewFromInt(ntx2.GasUsed)
				// 				t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "BNB", Amount: ntx2GasPrice.Mul(ntx2GasUsed)})
				// 			}
				// 		}
				// 	}
				// }
				if tok.To == "0x0000000000000000000000000000000000000000" {
					// t.FiscalCateg = tx.Burns
					t.FiscalCateg = tx.Gifts
					txs = append(txs, t)
					usedTok[tok.Hash] = true
				} else {
					found := false
					for _, itx := range j.InternalTXs {
						if itx.TimeStamp == tok.TimeStamp &&
							itx.BlockNumber == tok.BlockNumber &&
							itx.Hash == tok.Hash {
							found = true
							itxValue := decimal.NewFromBigInt(itx.Value.Int(), -18)
							t.Items["To"] = append(t.Items["To"], tx.Value{Code: "BNB", Amount: itxValue})
							// t.FiscalCateg = tx.Swaps
							t.FiscalCateg = tx.Exchanges
							txs = append(txs, t)
							usedInt[itx.Hash+itx.ContractAddress] = true
							usedTok[tok.Hash] = true
							break
						}
					}
					if !found {
						// Look for other tokenTX with same Hash
						for _, tok2 := range j.TokenTXs {
							if !usedTok[tok2.Hash] && tok2.Hash == tok.Hash {
								usedTok[tok2.Hash] = true
								to2IsOwn := j.Addresses[tok2.To]
								from2IsOwn := j.Addresses[tok2.From]
								tok2Value := decimal.NewFromBigInt(tok2.Value.Int(), -int32(tok2.TokenDecimal))
								if to2IsOwn && from2IsOwn {
									if exist := newType["bep20self2"]; !exist {
										b, err := encjson.Marshal(tok2)
										if err == nil {
											newType["bep20self2"] = true
											unknowns = append(unknowns, "bep20self2:"+string(b))
										}
									}
								} else if to2IsOwn {
									found = true
									t.Items["To"] = append(t.Items["To"], tx.Value{Code: tok2.TokenSymbol, Amount: tok2Value})
								} else if from2IsOwn {
									t.Items["From"] = append(t.Items["From"], tx.Value{Code: tok2.TokenSymbol, Amount: tok2Value})
								}
							}
						}
						if found {
							// t.FiscalCateg = tx.Swaps
							t.FiscalCateg = tx.Exchanges
							txs = append(txs, t)
						} else {
							t.FiscalCateg = tx.Withdrawals
							txs = append(txs, t)
						}
					}
				}
				for _, ntx := range j.NormalTXs {
					if ntx.Hash == tok.Hash {
						usedNor[ntx.Hash] = true
						break
					}
				}
			} else {
				if exist := newType["bep20"]; !exist {
					b, err := encjson.Marshal(tok)
					if err == nil {
						newType["bep20"] = true
						unknowns = append(unknowns, "bep20:"+string(b))
					}
				}
			}
		}
	}
	for _, itx := range j.InternalTXs {
		if !usedInt[itx.Hash+itx.ContractAddress] {
			toIsOwn := j.Addresses[itx.To]
			fromIsOwn := j.Addresses[itx.From]
			if toIsOwn && fromIsOwn {
				if exist := newType["intself"]; !exist {
					b, err := encjson.Marshal(itx)
					if err == nil {
						newType["intself"] = true
						unknowns = append(unknowns, "intself:"+string(b))
					}
				}
			} else if toIsOwn {
				t := tx.TX{Timestamp: time.Unix(itx.TimeStamp, 0), ID: itx.Hash, Source: source, Wallet: "BinanceSmartChain", Method: "API", Note: itx.BlockNumber + " " + itx.From}
				t.Items = make(map[string]tx.Values)
				itxValue := decimal.NewFromBigInt(itx.Value.Int(), -18)
				t.Items["To"] = append(t.Items["To"], tx.Value{Code: "BNB", Amount: itxValue})
				// if is, feeHash := cat.IsTxFee(itx.Hash); is {
				// 	for _, ntx2 := range j.NormalTXs {
				// 		for _, fee := range strings.Split(feeHash, ";") {
				// 			if ntx2.Hash == fee {
				// 				usedNor[ntx2.Hash] = true
				// 				t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "BNB", Amount: ntx2.GasPrice.Mul(ntx2.GasUsed)})
				// 			}
				// 		}
				// 	}
				// }
				isExchange := false
				for _, ntx := range j.NormalTXs {
					if ntx.Hash == itx.Hash {
						ntxValue := decimal.NewFromBigInt(ntx.Value.Int(), -18)
						ntxGasPrice := decimal.NewFromBigInt(ntx.GasPrice.Int(), -18)
						ntxGasUsed := decimal.NewFromInt(ntx.GasUsed)
						t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "BNB", Amount: ntxGasPrice.Mul(ntxGasUsed)})
						if !ntxValue.IsZero() {
							if j.Addresses[ntx.From] {
								t.Items["From"] = append(t.Items["From"], tx.Value{Code: "BNB", Amount: ntxValue})
								isExchange = true
							} else {
								// Internal Deposits TX with Normal Deposits TX associated
								if exist := newType["intdep"]; !exist {
									b, err := encjson.Marshal(itx)
									if err == nil {
										newType["intdep"] = true
										unknowns = append(unknowns, "intdep:"+string(b))
									}
								}
							}
						}
						usedNor[ntx.Hash] = true
						break
					}
				}
				if isExchange {
					t.FiscalCateg = tx.Exchanges
					txs = append(txs, t)
					// } else if is, desc, val, curr := cat.IsTxExchange(itx.Hash); is {
					// 	t.Note += " crypto_exchange " + desc
					// 	t.Items["From"] = append(t.Items["From"], tx.Value{Code: curr, Amount: val})
					// 	t.FiscalCateg = tx.Exchanges
					// 	txs = append(txs, t)
				} else {
					t.FiscalCateg = tx.Deposits
					txs = append(txs, t)
				}
				usedInt[itx.Hash+itx.ContractAddress] = true
			} else if fromIsOwn {
				if exist := newType["intwit"]; !exist {
					b, err := encjson.Marshal(itx)
					if err == nil {
						newType["intwit"] = true
						unknowns = append(unknowns, "intwit:"+string(b))
					}
				}
			} else {
				if exist := newType["int"]; !exist {
					b, err := encjson.Marshal(itx)
					if err == nil {
						newType["int"] = true
						unknowns = append(unknowns, "int:"+string(b))
					}
				}
			}
		}
	}
	for _, ntx := range j.NormalTXs {
		if !usedNor[ntx.Hash] {
			toIsOwn := j.Addresses[ntx.To]
			fromIsOwn := j.Addresses[ntx.From]
			ntxValue := decimal.NewFromBigInt(ntx.Value.Int(), -18)
			ntxGasPrice := decimal.NewFromBigInt(ntx.GasPrice.Int(), -18)
			ntxGasUsed := decimal.NewFromInt(ntx.GasUsed)
			if toIsOwn && fromIsOwn {
				t := tx.TX{Timestamp: time.Unix(ntx.TimeStamp, 0), ID: ntx.Hash, Source: source, Wallet: "BinanceSmartChain", Method: "API", Note: ntx.BlockNumber + " "}
				t.Items = make(map[string]tx.Values)
				t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "BNB", Amount: ntxGasPrice.Mul(ntxGasUsed)})
				if ntx.To == ntx.From || ntx.IsError != 0 {
					t.FiscalCateg = tx.Fees
					txs = append(txs, t)
					usedNor[ntx.Hash] = true
				} else {
					t.Items["To"] = append(t.Items["To"], tx.Value{Code: "BNB", Amount: ntxValue})
					t.Items["From"] = append(t.Items["From"], tx.Value{Code: "BNB", Amount: ntxValue})
					t.FiscalCateg = tx.Transfers
					txs = append(txs, t)
					usedNor[ntx.Hash] = true
				}
			} else if toIsOwn {
				if !ntxValue.IsZero() && ntx.IsError == 0 {
					t := tx.TX{Timestamp: time.Unix(ntx.TimeStamp, 0), ID: ntx.Hash, Source: source, Wallet: "BinanceSmartChain", Method: "API", Note: ntx.BlockNumber + " " + ntx.From}
					t.Items = make(map[string]tx.Values)
					t.Items["To"] = append(t.Items["To"], tx.Value{Code: "BNB", Amount: ntxValue})
					// if is, desc, val, curr := cat.IsTxExchange(ntx.Hash); is {
					// 	t.Note += " crypto_exchange " + desc
					// 	t.Items["From"] = append(t.Items["From"], tx.Value{Code: curr, Amount: val})
					// 	t.FiscalCateg = tx.Exchanges
					// 	txs = append(txs, t)
					// } else {
					t.FiscalCateg = tx.Deposits
					txs = append(txs, t)
					// }
					usedNor[ntx.Hash] = true
				}
			} else if fromIsOwn {
				t := tx.TX{Timestamp: time.Unix(ntx.TimeStamp, 0), ID: ntx.Hash, Source: source, Wallet: "BinanceSmartChain", Method: "API", Note: ntx.BlockNumber + " " + ntx.To}
				t.Items = make(map[string]tx.Values)
				t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "BNB", Amount: ntxGasPrice.Mul(ntxGasUsed)})
				if !ntxValue.IsZero() && ntx.IsError == 0 {
					t.Items["From"] = append(t.Items["From"], tx.Value{Code: "BNB", Amount: ntxValue})
					// // Is declared Exchanges
					// if is, desc, val, curr := cat.IsTxExchange(ntx.Hash); is {
					// 	t.Note += " crypto_exchange " + desc
					// 	t.Items["To"] = append(t.Items["To"], tx.Value{Code: curr, Amount: val})
					// 	t.FiscalCateg = tx.Exchanges
					// 	txs = append(txs, t)
					// } else {
					if ntx.To == "0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c" { // Special Case WBNB
						t.Items["To"] = append(t.Items["To"], tx.Value{Code: "WBNB", Amount: ntxValue})
						t.FiscalCateg = tx.Exchanges
						// t.FiscalCateg = tx.Wraps
						txs = append(txs, t)
					} else {
						t.FiscalCateg = tx.Withdrawals
						txs = append(txs, t)
					}
					// }
					usedNor[ntx.Hash] = true
				} else {
					t.FiscalCateg = tx.Fees
					txs = append(txs, t)
					usedNor[ntx.Hash] = true
				}
			} else {
				if exist := newType["nor"]; !exist {
					b, err := encjson.Marshal(ntx)
					if err == nil {
						newType["nor"] = true
						unknowns = append(unknowns, "nor:"+string(b))
					}
				}
			}
		}
	}

	addresses = make([]string, 0)
	for address := range j.Addresses {
		addresses = append(addresses, address)
	}
	return
}
