package json

import (
	encjson "encoding/json"
	"errors"
	"strings"
	"time"

	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

type CdcExTrade struct {
	Side               string  `json:"side"`
	InstrumentName     string  `json:"instrument_name"`
	Fee                float64 `json:"fee"`
	TradeID            string  `json:"trade_id"`
	CreateTime         int64   `json:"create_time"`
	TradedPrice        float64 `json:"traded_price"`
	TradedQuantity     float64 `json:"traded_quantity"`
	FeeCurrency        string  `json:"fee_currency"`
	OrderID            string  `json:"order_id"`
	ClientOrderID      string  `json:"client_oid"` // doc say client_order_id
	LiquidityIndicator string  `json:"liquidity_indicator"`
}

type CdcExJSON struct {
	Trades      map[string]CdcExTrade `json:"trades"`
	LastTradeTS int64                 `json:"last_trade_ts"`
}

var cdcExchangeAPI = &CdcExJSON{}

// Digest : Digest a CdcEx API JSON file
func (j CdcExJSON) Digest(jsonData []byte) (txs tx.TXs, source string, wallet string, unknowns []string, addresses []string, err error) {
	newType := make(map[string]bool)
	err = encjson.Unmarshal(jsonData, &j)
	if j.LastTradeTS == 0 { // Mandatory fields
		err = errors.New("JSON Format does not match")
	}
	if err != nil {
		return
	}
	source = "Cdc Exchange API"
	wallet = "Crypto.com Exchange"
	for _, tData := range j.Trades {
		t := tx.TX{Timestamp: time.UnixMilli(tData.CreateTime).UTC(), ID: tData.OrderID, FiscalCateg: tx.Exchanges, Source: source, Wallet: "Crypto.com Exchange", Method: "API", Note: tData.Side + " " + tData.LiquidityIndicator}
		t.Items = make(map[string]tx.Values)
		curr := strings.Split(tData.InstrumentName, "_")
		price := decimal.NewFromFloat(tData.TradedPrice)
		quantity := decimal.NewFromFloat(tData.TradedQuantity)
		fee := decimal.NewFromFloat(tData.Fee)
		if !fee.IsZero() {
			t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: tData.FeeCurrency, Amount: fee})
		}
		if tData.Side == "BUY" {
			t.Items["From"] = append(t.Items["From"], tx.Value{Code: curr[1], Amount: quantity.Mul(price)})
			t.Items["To"] = append(t.Items["To"], tx.Value{Code: curr[0], Amount: quantity})
		} else if tData.Side == "SELL" {
			t.Items["From"] = append(t.Items["From"], tx.Value{Code: curr[0], Amount: quantity})
			t.Items["To"] = append(t.Items["To"], tx.Value{Code: curr[1], Amount: quantity.Mul(price)})
		} else {
			if exist := newType[tData.Side]; !exist {
				tData.CreateTime = time.Now().UnixMilli()
				tData.Fee = 0.1
				tData.TradeID = "1234"
				tData.TradedPrice = 1.0
				tData.TradedQuantity = 1.0
				tData.OrderID = "5678"
				tData.ClientOrderID = ""
				b, err := encjson.Marshal(tData)
				if err == nil {
					newType[tData.Side] = true
					unknowns = append(unknowns, string(b))
				}
			}
		}
		found := false
		for i, ex := range txs {
			if ex.ID == t.ID {
				found = true
				for k, v := range t.Items {
					txs[i].Items[k] = append(txs[i].Items[k], v...)
				}
				break
			}
		}
		if !found {
			txs = append(txs, t)
		}
	}
	return
}
