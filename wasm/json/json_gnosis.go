package json

import (
	encjson "encoding/json"
	"errors"
	"time"

	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

type GnosisNormalTX struct {
	BlockHash         string  `json:"blockHash"`
	BlockNumber       string  `json:"blockNumber"`
	Confirmations     int     `json:"confirmations,string"`
	ContractAddress   string  `json:"contractAddress"`
	CumulativeGasUsed int     `json:"cumulativeGasUsed,string"`
	From              string  `json:"from"`
	Gas               int     `json:"gas,string"`
	GasPrice          *bigInt `json:"gasPrice"`
	GasUsed           int64   `json:"gasUsed,string"`
	Hash              string  `json:"hash"`
	Input             string  `json:"input"`
	IsError           int     `json:"isError,string"`
	Nonce             int     `json:"nonce,string"`
	TimeStamp         int64   `json:"timeStamp,string"`
	To                string  `json:"to"`
	TransactionIndex  int     `json:"transactionIndex,string"`
	TxReceiptStatus   string  `json:"txreceipt_status"`
	Value             *bigInt `json:"value"`
}

type GnosisInternalTX struct {
	BlockNumber     string  `json:"blockNumber"`
	ContractAddress string  `json:"contractAddress"`
	ErrCode         string  `json:"errCode"`
	From            string  `json:"from"`
	Gas             int     `json:"gas,string"`
	GasUsed         int64   `json:"gasUsed,string"`
	Index           string  `json:"index"`
	Input           string  `json:"input"`
	IsError         int     `json:"isError,string"`
	TimeStamp       int64   `json:"timeStamp,string"`
	To              string  `json:"to"`
	TransactionHash string  `json:"transactionHash"`
	Type            string  `json:"type"`
	Value           *bigInt `json:"value"`
}

type GnosisTokenTX struct {
	BlockHash         string  `json:"blockHash"`
	BlockNumber       string  `json:"blockNumber"`
	Confirmations     int     `json:"confirmations,string"`
	ContractAddress   string  `json:"contractAddress"`
	CumulativeGasUsed int     `json:"cumulativeGasUsed,string"`
	From              string  `json:"from"`
	Gas               int     `json:"gas,string"`
	GasPrice          *bigInt `json:"gasPrice"`
	GasUsed           int64   `json:"gasUsed,string"`
	Hash              string  `json:"hash"`
	Input             string  `json:"input"`
	LogIndex          string  `json:"logIndex"`
	Nonce             int     `json:"nonce,string"`
	TimeStamp         int64   `json:"timeStamp,string"`
	To                string  `json:"to"`
	TokenDecimal      uint8   `json:"tokenDecimal,string"`
	TokenName         string  `json:"tokenName"`
	TokenSymbol       string  `json:"tokenSymbol"`
	TransactionIndex  int     `json:"transactionIndex,string"`
	Value             *bigInt `json:"value"`
}

type GnosisMiningTX struct {
	BlockNumber string `json:"blockNumber"`
	BlockReward int64  `json:"blockReward,string"`
	TimeStamp   int64  `json:"timeStamp,string"`
}

type GnosisJSON struct {
	Addresses   map[string]bool             `json:"xdai_addrs"`
	NormalTXs   map[string]GnosisNormalTX   `json:"normal_txs"`
	InternalTXs map[string]GnosisInternalTX `json:"internal_txs"`
	TokenTXs    map[string]GnosisTokenTX    `json:"token_txs"`
	MiningTXs   map[string]GnosisMiningTX   `json:"mining_txs"`
}

var gnosis = &GnosisJSON{}

func (j GnosisJSON) Digest(jsonData []byte) (txs tx.TXs, source string, wallet string, unknowns []string, addresses []string, err error) {
	err = encjson.Unmarshal(jsonData, &j)
	if len(j.Addresses) == 0 { // Mandatory fields
		err = errors.New("JSON Format does not match")
	}
	if err != nil {
		return
	}
	source = "Gnosis API"
	wallet = "Gnosis"
	newType := make(map[string]bool)
	usedNor := make(map[string]bool)
	usedInt := make(map[string]bool)
	usedTok := make(map[string]bool)

	for _, tok := range j.TokenTXs {
		if !usedTok[tok.Hash] {
			toIsOwn := j.Addresses[tok.To]
			fromIsOwn := j.Addresses[tok.From]
			if toIsOwn && fromIsOwn {
				found := false
				for _, tr := range txs {
					if tok.Hash == tr.ID && tr.FiscalCateg == tx.Transfers {
						found = true
					}
				}
				if !found {
					t := tx.TX{Timestamp: time.Unix(tok.TimeStamp, 0), ID: tok.Hash, Source: source, Wallet: "Gnosis", Method: "API", Note: tok.BlockNumber + " " + tok.To}
					t.Items = make(map[string]tx.Values)
					tokValue := decimal.NewFromBigInt(tok.Value.Int(), -int32(tok.TokenDecimal))
					t.Items["To"] = append(t.Items["To"], tx.Value{Code: tok.TokenSymbol, Amount: tokValue})
					t.Items["From"] = append(t.Items["From"], tx.Value{Code: tok.TokenSymbol, Amount: tokValue})
					tokGasPrice := decimal.NewFromBigInt(tok.GasPrice.Int(), -18)
					tokGasUsed := decimal.NewFromInt(tok.GasUsed)
					t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "XDAI", Amount: tokGasPrice.Mul(tokGasUsed)})
					t.FiscalCateg = tx.Transfers
					txs = append(txs, t)
				}
				usedTok[tok.Hash] = true
			} else if toIsOwn {
				t := tx.TX{Timestamp: time.Unix(tok.TimeStamp, 0), ID: tok.Hash, Source: source, Wallet: "Gnosis", Method: "API", Note: tok.BlockNumber + " " + tok.To}
				t.Items = make(map[string]tx.Values)
				tokValue := decimal.NewFromBigInt(tok.Value.Int(), -int32(tok.TokenDecimal))
				t.Items["To"] = append(t.Items["To"], tx.Value{Code: tok.TokenSymbol, Amount: tokValue})
				usedTok[tok.Hash] = true
				// // Add declared Fee if any
				// if is, feeHash := cat.IsTxFee(tok.Hash); is {
				// 	for _, ntx2 := range j.NormalTXs {
				// 		for _, fee := range strings.Split(feeHash, ";") {
				// 			if ntx2.Hash == fee {
				// 				usedNor[ntx2.Hash] = true
				// 				t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "XDAI", Amount: ntx2.GasPrice.Mul(ntx2.GasUsed)})
				// 			}
				// 		}
				// 	}
				// }
				// Add normal Fee if any
				found := false
				for _, ntx := range j.NormalTXs {
					if ntx.Hash == tok.Hash {
						found = true
						ntxGasPrice := decimal.NewFromBigInt(ntx.GasPrice.Int(), -18)
						ntxGasUsed := decimal.NewFromInt(ntx.GasUsed)
						t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "XDAI", Amount: ntxGasPrice.Mul(ntxGasUsed)})
						if tok.From == "0x0000000000000000000000000000000000000000" {
							t.FiscalCateg = tx.Deposits
							txs = append(txs, t)
						} else {
							ntxValue := decimal.NewFromBigInt(ntx.Value.Int(), -18)
							if ntxValue.IsZero() {
								t.FiscalCateg = tx.Deposits
								txs = append(txs, t)
							} else {
								t.Items["From"] = append(t.Items["From"], tx.Value{Code: "XDAI", Amount: ntxValue})
								// t.FiscalCateg = tx.Swaps
								t.FiscalCateg = tx.Exchanges
								txs = append(txs, t)
							}
						}
						usedNor[ntx.Hash] = true
						usedTok[tok.Hash] = true
						break
					}
				}
				if !found {
					// Look for other tokenTX with same Hash
					for _, tok2 := range j.TokenTXs {
						if !usedTok[tok2.Hash] {
							if tok2.Hash == tok.Hash {
								usedTok[tok2.Hash] = true
								to2IsOwn := j.Addresses[tok2.To]
								from2IsOwn := j.Addresses[tok2.From]
								tok2Value := decimal.NewFromBigInt(tok2.Value.Int(), -int32(tok2.TokenDecimal))
								if to2IsOwn && from2IsOwn {
									if exist := newType["erc20self1"]; !exist {
										b, err := encjson.Marshal(tok2)
										if err == nil {
											newType["erc20self1"] = true
											unknowns = append(unknowns, "erc20self1:"+string(b))
										}
									}
								} else if to2IsOwn {
									t.Items["To"] = append(t.Items["To"], tx.Value{Code: tok2.TokenSymbol, Amount: tok2Value})
								} else if from2IsOwn {
									found = true
									t.Items["From"] = append(t.Items["From"], tx.Value{Code: tok2.TokenSymbol, Amount: tok2Value})
								}
							}
						}
					}
					if found {
						// t.FiscalCateg = tx.Swaps
						t.FiscalCateg = tx.Exchanges
						txs = append(txs, t)
					} else {
						t.FiscalCateg = tx.Deposits
						txs = append(txs, t)
					}
				}
				for _, ntx := range j.NormalTXs {
					if ntx.Hash == tok.Hash {
						usedNor[ntx.Hash] = true
						break
					}
				}
			} else if fromIsOwn {
				t := tx.TX{Timestamp: time.Unix(tok.TimeStamp, 0), ID: tok.Hash, Source: source, Wallet: "Gnosis", Method: "API", Note: tok.BlockNumber + " " + tok.To}
				t.Items = make(map[string]tx.Values)
				tokValue := decimal.NewFromBigInt(tok.Value.Int(), -int32(tok.TokenDecimal))
				t.Items["From"] = append(t.Items["From"], tx.Value{Code: tok.TokenSymbol, Amount: tokValue})
				tokGasPrice := decimal.NewFromBigInt(tok.GasPrice.Int(), -18)
				tokGasUsed := decimal.NewFromInt(tok.GasUsed)
				t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "XDAI", Amount: tokGasPrice.Mul(tokGasUsed)})
				usedTok[tok.Hash] = true
				// // Add declared Fee if any
				// if is, feeHash := cat.IsTxFee(tok.Hash); is {
				// 	for _, ntx2 := range j.NormalTXs {
				// 		for _, fee := range strings.Split(feeHash, ";") {
				// 			if ntx2.Hash == fee {
				// 				usedNor[ntx2.Hash] = true
				// 				ntx2GasPrice := decimal.NewFromBigInt(ntx2.GasPrice.Int(), -18)
				// 				ntx2GasUsed := decimal.NewFromInt(ntx2.GasUsed)
				// 				t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "XDAI", Amount: ntx2GasPrice.Mul(ntx2GasUsed)})
				// 			}
				// 		}
				// 	}
				// }
				if tok.To == "0x0000000000000000000000000000000000000000" {
					// t.FiscalCateg = tx.Burns
					t.FiscalCateg = tx.Gifts
					txs = append(txs, t)
					usedTok[tok.Hash] = true
				} else {
					found := false
					for _, itx := range j.InternalTXs {
						if itx.TimeStamp == tok.TimeStamp &&
							itx.BlockNumber == tok.BlockNumber &&
							itx.TransactionHash == tok.Hash {
							found = true
							itxValue := decimal.NewFromBigInt(itx.Value.Int(), -18)
							t.Items["To"] = append(t.Items["To"], tx.Value{Code: "XDAI", Amount: itxValue})
							// t.FiscalCateg = tx.Swaps
							t.FiscalCateg = tx.Exchanges
							txs = append(txs, t)
							usedInt[itx.TransactionHash+itx.ContractAddress] = true
							usedTok[tok.Hash] = true
							break
						}
					}
					if !found {
						// Look for other tokenTX with same Hash
						for _, tok2 := range j.TokenTXs {
							if !usedTok[tok2.Hash] && tok2.Hash == tok.Hash {
								usedTok[tok2.Hash] = true
								to2IsOwn := j.Addresses[tok2.To]
								from2IsOwn := j.Addresses[tok2.From]
								tok2Value := decimal.NewFromBigInt(tok2.Value.Int(), -int32(tok2.TokenDecimal))
								if to2IsOwn && from2IsOwn {
									if exist := newType["erc20self2"]; !exist {
										b, err := encjson.Marshal(tok2)
										if err == nil {
											newType["erc20self2"] = true
											unknowns = append(unknowns, "erc20self2:"+string(b))
										}
									}
								} else if to2IsOwn {
									found = true
									t.Items["To"] = append(t.Items["To"], tx.Value{Code: tok2.TokenSymbol, Amount: tok2Value})
								} else if from2IsOwn {
									t.Items["From"] = append(t.Items["From"], tx.Value{Code: tok2.TokenSymbol, Amount: tok2Value})
								}
							}
						}
						if found {
							// t.FiscalCateg = tx.Swaps
							t.FiscalCateg = tx.Exchanges
							txs = append(txs, t)
						} else {
							t.FiscalCateg = tx.Withdrawals
							txs = append(txs, t)
						}
					}
				}
				for _, ntx := range j.NormalTXs {
					if ntx.Hash == tok.Hash {
						usedNor[ntx.Hash] = true
						break
					}
				}
			} else {
				if exist := newType["erc20"]; !exist {
					b, err := encjson.Marshal(tok)
					if err == nil {
						newType["erc20"] = true
						unknowns = append(unknowns, "erc20:"+string(b))
					}
				}
			}
		}
	}
	for _, itx := range j.InternalTXs {
		if !usedInt[itx.TransactionHash+itx.ContractAddress] {
			toIsOwn := j.Addresses[itx.To]
			fromIsOwn := j.Addresses[itx.From]
			if toIsOwn && fromIsOwn {
				if exist := newType["intself"]; !exist {
					b, err := encjson.Marshal(itx)
					if err == nil {
						newType["intself"] = true
						unknowns = append(unknowns, "intself:"+string(b))
					}
				}
			} else if toIsOwn {
				t := tx.TX{Timestamp: time.Unix(itx.TimeStamp, 0), ID: itx.TransactionHash, Source: source, Wallet: "Gnosis", Method: "API", Note: itx.BlockNumber + " " + itx.From}
				t.Items = make(map[string]tx.Values)
				itxValue := decimal.NewFromBigInt(itx.Value.Int(), -18)
				t.Items["To"] = append(t.Items["To"], tx.Value{Code: "XDAI", Amount: itxValue})
				// if is, feeHash := cat.IsTxFee(itx.Hash); is {
				// 	for _, ntx2 := range j.NormalTXs {
				// 		for _, fee := range strings.Split(feeHash, ";") {
				// 			if ntx2.Hash == fee {
				// 				usedNor[ntx2.Hash] = true
				// 				t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "XDAI", Amount: ntx2.GasPrice.Mul(ntx2.GasUsed)})
				// 			}
				// 		}
				// 	}
				// }
				isExchange := false
				for _, ntx := range j.NormalTXs {
					if ntx.Hash == itx.TransactionHash {
						ntxValue := decimal.NewFromBigInt(ntx.Value.Int(), -18)
						ntxGasPrice := decimal.NewFromBigInt(ntx.GasPrice.Int(), -18)
						ntxGasUsed := decimal.NewFromInt(ntx.GasUsed)
						t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "XDAI", Amount: ntxGasPrice.Mul(ntxGasUsed)})
						if !ntxValue.IsZero() {
							if j.Addresses[ntx.From] {
								t.Items["From"] = append(t.Items["From"], tx.Value{Code: "XDAI", Amount: ntxValue})
								isExchange = true
							} else {
								// Internal Deposits TX with Normal Deposits TX associated
								if exist := newType["intdep"]; !exist {
									b, err := encjson.Marshal(itx)
									if err == nil {
										newType["intdep"] = true
										unknowns = append(unknowns, "intdep:"+string(b))
									}
								}
							}
						}
						usedNor[ntx.Hash] = true
						break
					}
				}
				if isExchange {
					t.FiscalCateg = tx.Exchanges
					txs = append(txs, t)
					// } else if is, desc, val, curr := cat.IsTxExchange(itx.Hash); is {
					// 	t.Note += " crypto_exchange " + desc
					// 	t.Items["From"] = append(t.Items["From"], tx.Value{Code: curr, Amount: val})
					// 	t.FiscalCateg = tx.Exchanges
					// 	txs = append(txs, t)
				} else {
					t.FiscalCateg = tx.Deposits
					txs = append(txs, t)
				}
				usedInt[itx.TransactionHash+itx.ContractAddress] = true
			} else if fromIsOwn {
				if exist := newType["intwit"]; !exist {
					b, err := encjson.Marshal(itx)
					if err == nil {
						newType["intwit"] = true
						unknowns = append(unknowns, "intwit:"+string(b))
					}
				}
			} else {
				if exist := newType["int"]; !exist {
					b, err := encjson.Marshal(itx)
					if err == nil {
						newType["int"] = true
						unknowns = append(unknowns, "int:"+string(b))
					}
				}
			}
		}
	}
	for _, ntx := range j.NormalTXs {
		if !usedNor[ntx.Hash] {
			toIsOwn := j.Addresses[ntx.To]
			fromIsOwn := j.Addresses[ntx.From]
			ntxValue := decimal.NewFromBigInt(ntx.Value.Int(), -18)
			ntxGasPrice := decimal.NewFromBigInt(ntx.GasPrice.Int(), -18)
			ntxGasUsed := decimal.NewFromInt(ntx.GasUsed)
			if toIsOwn && fromIsOwn {
				t := tx.TX{Timestamp: time.Unix(ntx.TimeStamp, 0), ID: ntx.Hash, Source: source, Wallet: "Gnosis", Method: "API", Note: ntx.BlockNumber + " "}
				t.Items = make(map[string]tx.Values)
				t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "XDAI", Amount: ntxGasPrice.Mul(ntxGasUsed)})
				if ntx.To == ntx.From || ntx.IsError != 0 {
					t.FiscalCateg = tx.Fees
					txs = append(txs, t)
					usedNor[ntx.Hash] = true
				} else {
					t.Items["To"] = append(t.Items["To"], tx.Value{Code: "XDAI", Amount: ntxValue})
					t.Items["From"] = append(t.Items["From"], tx.Value{Code: "XDAI", Amount: ntxValue})
					t.FiscalCateg = tx.Transfers
					txs = append(txs, t)
					usedNor[ntx.Hash] = true
				}
			} else if toIsOwn {
				if !ntxValue.IsZero() && ntx.IsError == 0 {
					t := tx.TX{Timestamp: time.Unix(ntx.TimeStamp, 0), ID: ntx.Hash, Source: source, Wallet: "Gnosis", Method: "API", Note: ntx.BlockNumber + " " + ntx.From}
					t.Items = make(map[string]tx.Values)
					t.Items["To"] = append(t.Items["To"], tx.Value{Code: "XDAI", Amount: ntxValue})
					// if is, desc, val, curr := cat.IsTxExchange(ntx.Hash); is {
					// 	t.Note += " crypto_exchange " + desc
					// 	t.Items["From"] = append(t.Items["From"], tx.Value{Code: curr, Amount: val})
					// 	t.FiscalCateg = tx.Exchanges
					// 	txs = append(txs, t)
					// } else {
					t.FiscalCateg = tx.Deposits
					txs = append(txs, t)
					// }
					usedNor[ntx.Hash] = true
				}
			} else if fromIsOwn {
				t := tx.TX{Timestamp: time.Unix(ntx.TimeStamp, 0), ID: ntx.Hash, Source: source, Wallet: "Gnosis", Method: "API", Note: ntx.BlockNumber + " " + ntx.To}
				t.Items = make(map[string]tx.Values)
				t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: "XDAI", Amount: ntxGasPrice.Mul(ntxGasUsed)})
				if !ntxValue.IsZero() && ntx.IsError == 0 {
					t.Items["From"] = append(t.Items["From"], tx.Value{Code: "XDAI", Amount: ntxValue})
					// // Is declared Exchanges
					// if is, desc, val, curr := cat.IsTxExchange(ntx.Hash); is {
					// 	t.Note += " crypto_exchange " + desc
					// 	t.Items["To"] = append(t.Items["To"], tx.Value{Code: curr, Amount: val})
					// 	t.FiscalCateg = tx.Exchanges
					// 	txs = append(txs, t)
					// } else {
					// if ntx.To == "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2" { // Special Case WETH
					// 	t.Items["To"] = append(t.Items["To"], tx.Value{Code: "WETH", Amount: ntxValue})
					// 	t.FiscalCateg = tx.Exchanges
					// 	// t.FiscalCateg = tx.Wraps
					// 	txs = append(txs, t)
					// } else if ntx.To == "0xf786c34106762ab4eeb45a51b42a62470e9d5332" { // Special Case fETH
					// 	t.Items["To"] = append(t.Items["To"], tx.Value{Code: "fETH", Amount: ntxValue.Mul(decimal.New(99, -2))})
					// 	t.FiscalCateg = tx.Exchanges
					// 	// t.FiscalCateg = tx.Wraps
					// 	txs = append(txs, t)
					// } else {
					t.FiscalCateg = tx.Withdrawals
					txs = append(txs, t)
					// }
					// }
					usedNor[ntx.Hash] = true
				} else {
					t.FiscalCateg = tx.Fees
					txs = append(txs, t)
					usedNor[ntx.Hash] = true
				}
			} else {
				if exist := newType["nor"]; !exist {
					b, err := encjson.Marshal(ntx)
					if err == nil {
						newType["nor"] = true
						unknowns = append(unknowns, "nor:"+string(b))
					}
				}
			}
		}
	}

	addresses = make([]string, 0)
	for address := range j.Addresses {
		addresses = append(addresses, address)
	}
	return
}
