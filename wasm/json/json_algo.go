package json

import (
	encjson "encoding/json"
	"errors"
	"strconv"
	"time"

	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/token"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

type LogicSignature struct {
	Args  []string `json:"args"`
	Logic string   `json:"logic"`
}
type SignatureLogic struct {
	LogicSig LogicSignature `json:"logicsig"`
}
type SignatureShort struct {
	Sig string `json:"sig"`
}

type PayTx struct {
	Amount      int    `json:"amount"` // important
	CloseAmount int    `json:"close-amount"`
	Receiver    string `json:"receiver"`
}
type AlgoPay struct {
	CloseRewards     int         `json:"close-rewards"`
	ClosingAmount    int         `json:"closing-amount"`
	ConfirmedRound   int         `json:"confirmed-round"`
	Fee              int         `json:"fee"` // important
	FirstValid       int         `json:"first-valid"`
	GenesisHash      string      `json:"genesis-hash"`
	GenesisId        string      `json:"genesis-id"`
	Group            string      `json:"group"`
	Id               string      `json:"id"` // important
	IntraRoundOffset int         `json:"intra-round-offset"`
	LastValid        int         `json:"last-valid"`
	PayTransactions  PayTx       `json:"payment-transaction"` // important
	ReceiverRewards  int         `json:"receiver-rewards"`
	RoundTime        int         `json:"round-time"`
	Sender           string      `json:"sender"`
	SenderRewards    int         `json:"sender-rewards"`
	Signature        interface{} `json:"signature"`
	TxType           string      `json:"tx-type"` // important
}

type AssTx struct {
	Amount      int    `json:"amount"`   // important
	AssetId     int    `json:"asset-id"` // important
	CloseAmount int    `json:"close-amount"`
	Receiver    string `json:"receiver"`
}
type AlgoAxfer struct {
	AssTransTransactions AssTx       `json:"asset-transfer-transaction"` // important
	CloseRewards         int         `json:"close-rewards"`
	ClosingAmount        int         `json:"closing-amount"`
	ConfirmedRound       int         `json:"confirmed-round"`
	Fee                  int         `json:"fee"` // important
	FirstValid           int         `json:"first-valid"`
	GenesisHash          string      `json:"genesis-hash"`
	GenesisId            string      `json:"genesis-id"`
	Group                string      `json:"group"`
	Id                   string      `json:"id"` // important
	IntraRoundOffset     int         `json:"intra-round-offset"`
	LastValid            int         `json:"last-valid"`
	ReceiverRewards      int         `json:"receiver-rewards"`
	RoundTime            int         `json:"round-time"`
	Sender               string      `json:"sender"`
	SenderRewards        int         `json:"sender-rewards"`
	Signature            interface{} `json:"signature"`
	TxType               string      `json:"tx-type"` // important
}

type AlgoAppl struct {
	ApplicationTransaction interface{} `json:"application-transaction"`
	CloseRewards           int         `json:"close-rewards"`
	ClosingAmount          int         `json:"closing-amount"`
	ConfirmedRound         int         `json:"confirmed-round"`
	Fee                    int         `json:"fee"` // important
	FirstValid             int         `json:"first-valid"`
	GenesisHash            string      `json:"genesis-hash"`
	GenesisId              string      `json:"genesis-id"`
	GlobalStateDelta       interface{} `json:""global-state-delta"`
	Group                  string      `json:"group"`
	Id                     string      `json:"id"` // important
	IntraRoundOffset       int         `json:"intra-round-offset"`
	LastValid              int         `json:"last-valid"`
	LocalStateDelta        interface{} `json:""local-state-delta"`
	ReceiverRewards        int         `json:"receiver-rewards"`
	RoundTime              int         `json:"round-time"`
	Sender                 string      `json:"sender"`
	SenderRewards          int         `json:"sender-rewards"`
	Signature              interface{} `json:"signature"`
	TxType                 string      `json:"tx-type"` // important
}

type AlgoJSON struct {
	Addresses map[string]bool      `json:"algo_addrs"`
	Pay       map[string]AlgoPay   `json:"pay"`
	Axfer     map[string]AlgoAxfer `json:"axfer"`
	Appl      map[string]AlgoAppl  `json:"tx_appl"`
}

var algo = &AlgoJSON{}

// Digest : Digest a Algorand AddressTransactions JSON file
func (j AlgoJSON) Digest(jsonData []byte) (txs tx.TXs, source string, wallet string, unknowns []string, addresses []string, err error) {
	err = encjson.Unmarshal(jsonData, &j)
	if len(j.Addresses) == 0 { // Mandatory fields
		err = errors.New("JSON Format does not match")
	}
	if err != nil {
		return
	}
	source = "Algorand API"
	wallet = "Algorand"
	newType := make(map[string]bool)
	usedAppl := make(map[string]bool)
	usedPay := make(map[string]bool)
	usedAxfer := make(map[string]bool)

	for _, atx := range j.Appl {
		if !usedAppl[atx.Id+strconv.Itoa(atx.RoundTime)] {
			fromIsOwn := j.Addresses[atx.Sender]
			atxFee := decimal.NewFromFloat(float64(atx.Fee) / 1000000)
			// Find rewards (sender-rewards & receiver-rewards = minings)
			atxSenderRewards := decimal.NewFromFloat(float64(atx.SenderRewards) / 1000000)
			if fromIsOwn {
				if !atxFee.IsZero() {
					tFees := tx.TX{Timestamp: time.Unix(int64(atx.RoundTime), 0), ID: atx.Id + strconv.Itoa(atx.RoundTime), Source: source, Wallet: wallet, Method: "API", Note: "application-transaction - " + " Sender:" + atx.Sender}
					tFees.Items = make(map[string]tx.Values)
					tFees.Items["Fee"] = append(tFees.Items["Fee"], tx.Value{Code: "ALGO", Amount: atxFee})
					tFees.FiscalCateg = tx.Fees
					txs = append(txs, tFees)
				}

				if !atxSenderRewards.IsZero() {
					t := tx.TX{Timestamp: time.Unix(int64(atx.RoundTime), 0), ID: atx.Id + strconv.Itoa(atx.RoundTime), Source: source, Wallet: wallet, Method: "API", Note: "application-transaction - " + " Sender:" + atx.Sender}
					t.Items = make(map[string]tx.Values)
					t.Items["To"] = append(t.Items["To"], tx.Value{Code: "ALGO", Amount: atxSenderRewards})
					t.FiscalCateg = tx.Minings
					txs = append(txs, t)
				}
			}
			usedAppl[atx.Id+strconv.Itoa(atx.RoundTime)] = true
		}
	}
	for _, ptx := range j.Pay {
		if !usedPay[ptx.Id+strconv.Itoa(ptx.RoundTime)] {
			toIsOwn := j.Addresses[ptx.PayTransactions.Receiver]
			fromIsOwn := j.Addresses[ptx.Sender]
			ptxValue := decimal.NewFromFloat(float64(ptx.PayTransactions.Amount) / 1000000)
			ptxFee := decimal.NewFromFloat(float64(ptx.Fee) / 1000000)
			// Find rewards (sender-rewards & receiver-rewards = minings)
			ptxSenderRewards := decimal.NewFromFloat(float64(ptx.SenderRewards) / 1000000)
			ptxReceiverRewards := decimal.NewFromFloat(float64(ptx.ReceiverRewards) / 1000000)
			token := "ALGO"
			if toIsOwn && fromIsOwn {
				found := false
				for _, tr := range txs {
					if ptx.Id == tr.ID &&
						tr.FiscalCateg == tx.Transfers {
						found = true
					}
				}
				if !found {
					if !ptxValue.IsZero() {
						t := tx.TX{Timestamp: time.Unix(int64(ptx.RoundTime), 0), ID: ptx.Id + strconv.Itoa(ptx.RoundTime), Source: source, Wallet: wallet, Method: "API", Note: "payment-transaction - " + "Receiver:" + ptx.PayTransactions.Receiver + " Sender:" + ptx.Sender}
						t.Items = make(map[string]tx.Values)
						if token != "" {
							t.Items["To"] = append(t.Items["To"], tx.Value{Code: token, Amount: ptxValue})
							t.Items["From"] = append(t.Items["From"], tx.Value{Code: token, Amount: ptxValue})
						} else {
							t.Items["To"] = append(t.Items["To"], tx.Value{Code: "ALGO", Amount: ptxValue})
							t.Items["From"] = append(t.Items["From"], tx.Value{Code: "ALGO", Amount: ptxValue})
						}
						t.FiscalCateg = tx.Transfers
						txs = append(txs, t)
					}

					tFees := tx.TX{Timestamp: time.Unix(int64(ptx.RoundTime), 0), ID: ptx.Id + strconv.Itoa(ptx.RoundTime), Source: source, Wallet: wallet, Method: "API", Note: "payment-transaction - " + "Receiver:" + ptx.PayTransactions.Receiver + " Sender:" + ptx.Sender}
					tFees.Items = make(map[string]tx.Values)
					tFees.Items["Fee"] = append(tFees.Items["Fee"], tx.Value{Code: "ALGO", Amount: ptxFee})
					tFees.FiscalCateg = tx.Fees
					txs = append(txs, tFees)

					if !ptxSenderRewards.IsZero() || !ptxReceiverRewards.IsZero() {
						t := tx.TX{Timestamp: time.Unix(int64(ptx.RoundTime), 0), ID: ptx.Id + strconv.Itoa(ptx.RoundTime), Source: source, Wallet: wallet, Method: "API", Note: "payment-transaction - " + "Receiver:" + ptx.PayTransactions.Receiver + " Sender:" + ptx.Sender}
						t.Items = make(map[string]tx.Values)
						t.Items["To"] = append(t.Items["To"], tx.Value{Code: "ALGO", Amount: ptxSenderRewards})
						t.FiscalCateg = tx.Minings
						txs = append(txs, t)
					}
				}
				usedPay[ptx.Id+strconv.Itoa(ptx.RoundTime)] = true
			} else if toIsOwn {
				// if i am the receiver :
				if !ptxValue.IsZero() {
					t := tx.TX{Timestamp: time.Unix(int64(ptx.RoundTime), 0), ID: ptx.Id + strconv.Itoa(ptx.RoundTime), Source: source, Wallet: wallet, Method: "API", Note: "payment-transaction - " + "Receiver:" + ptx.PayTransactions.Receiver + " Sender:" + ptx.Sender}
					t.Items = make(map[string]tx.Values)
					t.Items["To"] = append(t.Items["To"], tx.Value{Code: "ALGO", Amount: ptxValue})
					t.FiscalCateg = tx.Deposits
					txs = append(txs, t)
				}

				if !ptxReceiverRewards.IsZero() {
					t := tx.TX{Timestamp: time.Unix(int64(ptx.RoundTime), 0), ID: ptx.Id + strconv.Itoa(ptx.RoundTime), Source: source, Wallet: wallet, Method: "API", Note: "payment-transaction - " + "Receiver:" + ptx.PayTransactions.Receiver + " Sender:" + ptx.Sender}
					t.Items = make(map[string]tx.Values)
					t.Items["To"] = append(t.Items["To"], tx.Value{Code: "ALGO", Amount: ptxReceiverRewards})
					t.FiscalCateg = tx.Minings
					txs = append(txs, t)
				}
				usedPay[ptx.Id+strconv.Itoa(ptx.RoundTime)] = true
			} else if fromIsOwn {
				// if i am the sender :
				if !ptxValue.IsZero() {
					t := tx.TX{Timestamp: time.Unix(int64(ptx.RoundTime), 0), ID: ptx.Id + strconv.Itoa(ptx.RoundTime), Source: source, Wallet: wallet, Method: "API", Note: "payment-transaction - " + "Receiver:" + ptx.PayTransactions.Receiver + " Sender:" + ptx.Sender}
					t.Items = make(map[string]tx.Values)
					t.Items["From"] = append(t.Items["From"], tx.Value{Code: "ALGO", Amount: ptxValue})
					t.FiscalCateg = tx.Withdrawals
					txs = append(txs, t)
				}

				tFees := tx.TX{Timestamp: time.Unix(int64(ptx.RoundTime), 0), ID: ptx.Id + strconv.Itoa(ptx.RoundTime), Source: source, Wallet: wallet, Method: "API", Note: "payment-transaction - " + "Receiver:" + ptx.PayTransactions.Receiver + " Sender:" + ptx.Sender}
				tFees.Items = make(map[string]tx.Values)
				tFees.Items["Fee"] = append(tFees.Items["Fee"], tx.Value{Code: "ALGO", Amount: ptxFee})
				tFees.FiscalCateg = tx.Fees
				txs = append(txs, tFees)

				if !ptxSenderRewards.IsZero() {
					t := tx.TX{Timestamp: time.Unix(int64(ptx.RoundTime), 0), ID: ptx.Id + strconv.Itoa(ptx.RoundTime), Source: source, Wallet: wallet, Method: "API", Note: "payment-transaction - " + "Receiver:" + ptx.PayTransactions.Receiver + " Sender:" + ptx.Sender}
					t.Items = make(map[string]tx.Values)
					t.Items["To"] = append(t.Items["To"], tx.Value{Code: "ALGO", Amount: ptxSenderRewards})
					t.FiscalCateg = tx.Minings
					txs = append(txs, t)
				}
				usedPay[ptx.Id+strconv.Itoa(ptx.RoundTime)] = true
			} else {
				if exist := newType["pay"]; !exist {
					b, err := encjson.Marshal(ptx)
					if err == nil {
						newType["pay"] = true
						unknowns = append(unknowns, "pay:"+string(b))
					}
				}
			}
		}
	}
	for _, axtx := range j.Axfer {
		if !usedAxfer[axtx.Id+strconv.Itoa(axtx.RoundTime)] {
			toIsOwn := j.Addresses[axtx.AssTransTransactions.Receiver]
			fromIsOwn := j.Addresses[axtx.Sender]
			axtxValue := decimal.NewFromFloat(float64(axtx.AssTransTransactions.Amount) / 1000000)
			axtxFee := decimal.NewFromFloat(float64(axtx.Fee) / 1000000)
			// Find rewards (sender-rewards & receiver-rewards = minings)
			axtxSenderRewards := decimal.NewFromFloat(float64(axtx.SenderRewards) / 1000000)
			axtxReceiverRewards := decimal.NewFromFloat(float64(axtx.ReceiverRewards) / 1000000)
			// Look for the token name base on asset-id (only for Axfer)
			assetId := axtx.AssTransTransactions.AssetId
			token := token.Get(assetId)
			if toIsOwn && fromIsOwn {
				found := false
				for _, tr := range txs {
					if axtx.Id == tr.ID &&
						tr.FiscalCateg == tx.Transfers {
						found = true
					}
				}
				if !found {
					if !axtxValue.IsZero() {
						t := tx.TX{Timestamp: time.Unix(int64(axtx.RoundTime), 0), ID: axtx.Id + strconv.Itoa(axtx.RoundTime), Source: source, Wallet: wallet, Method: "API", Note: "asset-transfer-transaction - " + "Receiver:" + axtx.AssTransTransactions.Receiver + " Sender:" + axtx.Sender}
						t.Items = make(map[string]tx.Values)
						if token != "" {
							t.Items["To"] = append(t.Items["To"], tx.Value{Code: token, Amount: axtxValue})
							t.Items["From"] = append(t.Items["From"], tx.Value{Code: token, Amount: axtxValue})
						} else {
							t.Items["To"] = append(t.Items["To"], tx.Value{Code: "ALGO", Amount: axtxValue})
							t.Items["From"] = append(t.Items["From"], tx.Value{Code: "ALGO", Amount: axtxValue})
						}
						t.FiscalCateg = tx.Transfers
						txs = append(txs, t)
					}

					tFees := tx.TX{Timestamp: time.Unix(int64(axtx.RoundTime), 0), ID: axtx.Id + strconv.Itoa(axtx.RoundTime), Source: source, Wallet: wallet, Method: "API", Note: "asset-transfer-transaction - " + "Receiver:" + axtx.AssTransTransactions.Receiver + " Sender:" + axtx.Sender}
					tFees.Items = make(map[string]tx.Values)
					tFees.Items["Fee"] = append(tFees.Items["Fee"], tx.Value{Code: "ALGO", Amount: axtxFee})
					tFees.FiscalCateg = tx.Fees
					txs = append(txs, tFees)

					if !axtxSenderRewards.IsZero() || !axtxReceiverRewards.IsZero() {
						t := tx.TX{Timestamp: time.Unix(int64(axtx.RoundTime), 0), ID: axtx.Id + strconv.Itoa(axtx.RoundTime), Source: source, Wallet: wallet, Method: "API", Note: "asset-transfer-transaction - " + "Receiver:" + axtx.AssTransTransactions.Receiver + " Sender:" + axtx.Sender}
						t.Items = make(map[string]tx.Values)
						t.Items["To"] = append(t.Items["To"], tx.Value{Code: "ALGO", Amount: decimal.Sum(axtxSenderRewards, axtxSenderRewards)})
						t.FiscalCateg = tx.Minings
						txs = append(txs, t)
					}
				}
				usedAxfer[axtx.Id+strconv.Itoa(axtx.RoundTime)] = true
			} else if toIsOwn {
				// if i am the receiver :
				if !axtxValue.IsZero() {
					t := tx.TX{Timestamp: time.Unix(int64(axtx.RoundTime), 0), ID: axtx.Id + strconv.Itoa(axtx.RoundTime), Source: source, Wallet: wallet, Method: "API", Note: "asset-transfer-transaction - " + "Receiver:" + axtx.AssTransTransactions.Receiver + " Sender:" + axtx.Sender}
					t.Items = make(map[string]tx.Values)
					if token != "" {
						t.Items["To"] = append(t.Items["To"], tx.Value{Code: token, Amount: axtxValue})
					} else {
						t.Items["To"] = append(t.Items["To"], tx.Value{Code: "ALGO", Amount: axtxValue})
					}
					t.FiscalCateg = tx.Deposits
					txs = append(txs, t)
				}
				if !axtxReceiverRewards.IsZero() {
					t := tx.TX{Timestamp: time.Unix(int64(axtx.RoundTime), 0), ID: axtx.Id + strconv.Itoa(axtx.RoundTime), Source: source, Wallet: wallet, Method: "API", Note: "asset-transfer-transaction - " + "Receiver:" + axtx.AssTransTransactions.Receiver + " Sender:" + axtx.Sender}
					t.Items = make(map[string]tx.Values)
					t.Items["To"] = append(t.Items["To"], tx.Value{Code: "ALGO", Amount: axtxReceiverRewards})
					t.FiscalCateg = tx.Minings
					txs = append(txs, t)
				}
				usedAxfer[axtx.Id+strconv.Itoa(axtx.RoundTime)] = true
			} else if fromIsOwn {
				// if i am the sender :
				if !axtxValue.IsZero() {
					t := tx.TX{Timestamp: time.Unix(int64(axtx.RoundTime), 0), ID: axtx.Id + strconv.Itoa(axtx.RoundTime), Source: source, Wallet: wallet, Method: "API", Note: "asset-transfer-transaction - " + "Receiver:" + axtx.AssTransTransactions.Receiver + " Sender:" + axtx.Sender}
					t.Items = make(map[string]tx.Values)
					if token != "" {
						t.Items["From"] = append(t.Items["From"], tx.Value{Code: token, Amount: axtxValue})
					} else {
						t.Items["From"] = append(t.Items["From"], tx.Value{Code: "ALGO", Amount: axtxValue})
					}
					t.FiscalCateg = tx.Withdrawals
					txs = append(txs, t)

					tFees := tx.TX{Timestamp: time.Unix(int64(axtx.RoundTime), 0), ID: axtx.Id + strconv.Itoa(axtx.RoundTime), Source: source, Wallet: wallet, Method: "API", Note: "asset-transfer-transaction - " + "Receiver:" + axtx.AssTransTransactions.Receiver + " Sender:" + axtx.Sender}
					tFees.Items = make(map[string]tx.Values)
					tFees.Items["Fee"] = append(tFees.Items["Fee"], tx.Value{Code: "ALGO", Amount: axtxFee})
					tFees.FiscalCateg = tx.Fees
					txs = append(txs, tFees)
				}
				if !axtxSenderRewards.IsZero() {
					t := tx.TX{Timestamp: time.Unix(int64(axtx.RoundTime), 0), ID: axtx.Id + strconv.Itoa(axtx.RoundTime), Source: source, Wallet: wallet, Method: "API", Note: "asset-transfer-transaction - " + "Receiver:" + axtx.AssTransTransactions.Receiver + " Sender:" + axtx.Sender}
					t.Items = make(map[string]tx.Values)
					t.Items["To"] = append(t.Items["To"], tx.Value{Code: "ALGO", Amount: axtxSenderRewards})
					t.FiscalCateg = tx.Minings
					txs = append(txs, t)
				}
				usedAxfer[axtx.Id+strconv.Itoa(axtx.RoundTime)] = true
			} else {
				if exist := newType["axfer"]; !exist {
					b, err := encjson.Marshal(axtx)
					if err == nil {
						newType["axfer"] = true
						unknowns = append(unknowns, "transfer:"+string(b))
					}
				}
			}
		}
	}

	addresses = make([]string, 0)
	for address := range j.Addresses {
		addresses = append(addresses, address)
	}
	return
}
