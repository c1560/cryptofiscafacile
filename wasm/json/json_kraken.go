package json

import (
	encjson "encoding/json"
	"errors"
	"fmt"
	"math"
	"reflect"
	"strings"
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/rate"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

type KrakenAsset struct {
	Class           string `json:"aclass"`
	AltName         string `json:"altname"`
	Decimals        int    `json:"decimals"`
	DisplayDecimals int    `json:"display_decimals"`
}
type KrakenAssets map[string]KrakenAsset

type KrakenAssetPair struct {
	AltName           string          `json:"altname"`
	WSName            string          `json:"wsname"`
	AClassBase        string          `json:"aclass_base"`
	Base              string          `json:"base"`
	AClassQuote       string          `json:"aclass_quote"`
	Quote             string          `json:"quote"`
	Lot               string          `json:"lot"`
	PairDecimals      int             `json:"pair_decimals"`
	LotDecimals       int             `json:"lot_decimals"`
	LotMultiplier     int             `json:"lot_multiplier"`
	LeverageBuy       []int           `json:"leverage_buy"`
	LeverageSell      []int           `json:"leverage_sell"`
	Fees              [][]interface{} `json:"fees"`
	FeesMaker         [][]interface{} `json:"fees_maker"`
	FeeVolumeCurrency string          `json:"fee_volume_currency"`
	MarginCall        int             `json:"margin_call"`
	MarginStop        int             `json:"margin_stop"`
	OrderMin          string          `json:"ordermin"`
}
type KrakenAssetPairs map[string]KrakenAssetPair

type KrakenLedger struct {
	AClass  string  `json:"aclass"`
	Amount  string  `json:"amount"`
	Asset   string  `json:"asset"`
	Balance string  `json:"balance"`
	Fee     string  `json:"fee"`
	RefID   string  `json:"refid"`
	SubType string  `json:"subtype"`
	Time    float64 `json:"time"`
	Type    string  `json:"type"`
	used    bool    `json:"-"`
}
type KrakenLedgers struct {
	Objects map[string]KrakenLedger `json:"objects"`
	Count   int                     `json:"count"`
}

type KrakenTrade struct {
	CCost     string   `json:"ccost"`
	CFee      string   `json:"cfee"`
	CMargin   string   `json:"cmargin"`
	Cost      string   `json:"cost"`
	CPrice    string   `json:"cprice"`
	CVol      string   `json:"cvol"`
	Fee       string   `json:"fee"`
	Margin    string   `json:"margin"`
	Misc      string   `json:"misc"`
	Net       string   `json:"net"`
	OrderTxID string   `json:"ordertxid"`
	OrderType string   `json:"ordertype"`
	Pair      string   `json:"pair"`
	PosTxID   string   `json:"postxid"`
	PosStatus string   `json:"posstatus"`
	Price     string   `json:"price"`
	Time      float64  `json:"time"`
	Trades    []string `json:"trades"`
	Type      string   `json:"type"`
	Vol       string   `json:"vol"`
}
type KrakenTrades struct {
	Objects map[string]KrakenTrade `json:"objects"`
	Count   int                    `json:"count"`
}

type KrakenJSON struct {
	Assets     KrakenAssets     `json:"assets"`
	AssetPairs KrakenAssetPairs `json:"assetpairs"`
	Ledger     KrakenLedgers    `json:"ledger"`
	Trades     KrakenTrades     `json:"trades"`
}

var kraken = &KrakenJSON{}

// Digest : Digest a Kraken Assets JSON file
func (j KrakenJSON) Digest(jsonData []byte) (txs tx.TXs, source string, wallet string, unknowns []string, addresses []string, err error) {
	newType := make(map[string]bool)
	err = encjson.Unmarshal(jsonData, &j)
	if len(j.Assets) == 0 { // Mandatory fields
		err = errors.New("JSON Format does not match")
	}
	if err != nil {
		return
	}
	void := KrakenAssets{}
	if reflect.DeepEqual(j.Assets, void) {
		err = errors.New("Empty")
		return
	}
	voidAssets := KrakenAsset{}
	for _, v := range j.Assets {
		vv := v
		vv.Class = ""
		if reflect.DeepEqual(vv, voidAssets) {
			err = errors.New("Empty2")
			return
		}
	}
	voidLedger := KrakenLedger{}
	for _, v := range j.Ledger.Objects {
		vv := v
		vv.AClass = ""
		vv.Fee = ""
		vv.Time = 0
		vv.Type = ""
		if reflect.DeepEqual(vv, voidLedger) {
			err = errors.New("Empty")
			return
		}
	}
	source = "Kraken API"
	wallet = "Kraken"
	for lID, lData := range j.Ledger.Objects {
		if lData.Type != "deposit" &&
			lData.Type != "staking" &&
			lData.Type != "withdrawal" &&
			lData.Type != "transfer" &&
			lData.Type != "spend" &&
			lData.Type != "receive" {
			continue
		}
		amount, err := decimal.NewFromString(lData.Amount)
		if err != nil {
			fmt.Println(lID, "Error while parsing amount", lData.Amount)
			continue
		}
		asset := j.Assets.getAltName(lData.Asset)
		fee, err := decimal.NewFromString(lData.Fee)
		if err != nil {
			fmt.Println(lID, "Error while parsing fee", lData.Fee)
			continue
		}
		sec, dec := math.Modf(lData.Time)
		t := tx.TX{Timestamp: time.Unix(int64(sec), int64(dec*(1e9))), ID: lID + "-" + lData.RefID, Source: source, Wallet: "Kraken", Method: "API", Note: lData.Type}
		t.Items = make(map[string]tx.Values)
		if !amount.IsZero() {
			if lData.Type == "withdrawal" {
				t.Items["From"] = append(t.Items["From"], tx.Value{Code: asset, Amount: amount})
			} else if amount.IsPositive() {
				t.Items["To"] = append(t.Items["To"], tx.Value{Code: asset, Amount: amount})
			} else {
				t.Items["From"] = append(t.Items["From"], tx.Value{Code: asset, Amount: amount.Neg()})
			}
		}
		if !fee.IsZero() {
			if !rate.IsFiat(asset) {
				t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: asset, Amount: fee})
			}
		}
		if lData.Type == "deposit" ||
			lData.Type == "staking" {
			if to, ok := t.Items["To"]; ok {
				if len(to) > 0 {
					if !to[0].IsFiat() {
						if lData.Type == "staking" {
							t.FiscalCateg = tx.Minings
						} else {
							t.FiscalCateg = tx.Deposits
						}
						txs = append(txs, t)
					}
					lData.used = true
					j.Ledger.Objects[lID] = lData
				}
			}
		} else if lData.Type == "withdrawal" {
			spew.Dump(lData, t)
			if from, ok := t.Items["From"]; ok {
				if len(from) > 0 {
					if !from[0].IsFiat() {
						t.FiscalCateg = tx.Withdrawals
						txs = append(txs, t)
					}
					lData.used = true
					j.Ledger.Objects[lID] = lData
				}
			}
		} else if lData.Type == "transfer" {
			if lData.SubType == "" {
				t.FiscalCateg = tx.AirDrops
				txs = append(txs, t)
			} else {
				// Ignore non void subType transfer
				// because it's a intra-account transfert
				// Just keep the Fees if any
				if fee, ok := t.Items["Fee"]; ok {
					if len(fee) > 0 {
						t.FiscalCateg = tx.Fees
						txs = append(txs, t)
					}
				}
			}
			lData.used = true
			j.Ledger.Objects[lID] = lData
		} else if lData.Type == "spend" ||
			lData.Type == "receive" {
			found := false
			for i, ex := range txs {
				if ex.ID == lData.RefID {
					found = true
					for k, v := range t.Items {
						txs[i].Items[k] = append(txs[i].Items[k], v...)
					}
					for k, v := range txs[i].Items {
						if len(v) > 0 {
							if v[0].IsFiat() {
								if k == "From" {
									txs[i].FiscalCateg = tx.CashIn
								} else if k == "To" {
									txs[i].FiscalCateg = tx.CashOut
								}
							}
						}
					}
					txs[i].Note = ex.Note + " " + t.Note
					lData.used = true
					j.Ledger.Objects[lID] = lData
					break
				}
			}
			if !found {
				t.ID = lData.RefID
				t.FiscalCateg = tx.Exchanges
				lData.used = true
				j.Ledger.Objects[lID] = lData
				txs = append(txs, t)
			}
		}
	}
	for tID, tData := range j.Trades.Objects {
		sec, dec := math.Modf(tData.Time)
		t := tx.TX{Timestamp: time.Unix(int64(sec), int64(dec*(1e9))), ID: tData.OrderTxID, FiscalCateg: tx.Exchanges, Source: source, Wallet: "Kraken", Method: "API", Note: tData.OrderType + " " + tData.Type}
		t.Items = make(map[string]tx.Values)
		vol, err := decimal.NewFromString(tData.Vol)
		if err != nil {
			fmt.Println(tID, "Error while parsing vol", tData.Vol)
			continue
		}
		cost, err := decimal.NewFromString(tData.Cost)
		if err != nil {
			fmt.Println(tID, "Error while parsing cost", tData.Cost)
			continue
		}
		fee, err := decimal.NewFromString(tData.Fee)
		if err != nil {
			fmt.Println(tID, "Error while parsing fee", tData.Fee)
			continue
		}
		pair := j.AssetPairs[tData.Pair]
		if pair.Base == "" || pair.Quote == "" {
			rightSize := 3
			if len(tData.Pair) > 7 {
				rightSize = 4
			}
			pair.Base = j.Assets.getAltName(tData.Pair[:len(tData.Pair)-rightSize])
			pair.Quote = j.Assets.getAltName(tData.Pair[len(tData.Pair)-rightSize:])
		} else {
			pair.Base = j.Assets.getAltName(pair.Base)
			pair.Quote = j.Assets.getAltName(pair.Quote)
		}
		if tData.Type == "sell" {
			leftVal := tx.Value{Code: pair.Base, Amount: vol}
			t.Items["From"] = append(t.Items["From"], leftVal)
			if rate.IsFiat(pair.Base) {
				t.FiscalCateg = tx.CashIn
			}
			rightVal := tx.Value{Code: pair.Quote, Amount: cost}
			t.Items["To"] = append(t.Items["To"], rightVal)
			if rate.IsFiat(pair.Quote) {
				t.FiscalCateg = tx.CashOut
			}
		} else if tData.Type == "buy" {
			t.Items["From"] = append(t.Items["From"], tx.Value{Code: pair.Quote, Amount: cost})
			if rate.IsFiat(pair.Quote) {
				t.FiscalCateg = tx.CashIn
			}
			t.Items["To"] = append(t.Items["To"], tx.Value{Code: pair.Base, Amount: vol})
			if rate.IsFiat(pair.Base) {
				t.FiscalCateg = tx.CashOut
			}
			if !fee.IsZero() {
				if !rate.IsFiat(pair.Quote) ||
					t.FiscalCateg == tx.CashIn {
					t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: pair.Quote, Amount: fee})
				}
			}
		}
		// Look for margin/rollover
		for lID, lData := range j.Ledger.Objects {
			if lData.Type == "trade" {
				lData.used = true
				j.Ledger.Objects[lID] = lData
				continue
			}
			if lData.Type != "margin" &&
				lData.Type != "rollover" {
				continue
			}
			if lData.RefID != tID {
				continue
			}
			amount, err := decimal.NewFromString(lData.Amount)
			if err != nil {
				fmt.Println(lID, "Error while parsing amount", lData.Amount)
				continue
			}
			asset := j.Assets.getAltName(lData.Asset)
			fee, err := decimal.NewFromString(lData.Fee)
			if err != nil {
				fmt.Println(lID, "Error while parsing fee", lData.Fee)
				continue
			}
			if !amount.IsZero() {
				if amount.IsPositive() {
					t.Items["To"] = append(t.Items["To"], tx.Value{Code: asset, Amount: amount})
				} else {
					t.Items["From"] = append(t.Items["From"], tx.Value{Code: asset, Amount: amount.Neg()})
				}
			}
			if !fee.IsZero() {
				if !rate.IsFiat(asset) ||
					t.FiscalCateg != tx.CashIn {
					t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: asset, Amount: fee})
				}
			}
			lData.used = true
			j.Ledger.Objects[lID] = lData
		}
		found := false
		for i, ex := range txs {
			if ex.ID == t.ID {
				found = true
				for k, v := range t.Items {
					txs[i].Items[k] = append(txs[i].Items[k], v...)
				}
				break
			}
		}
		if !found {
			txs = append(txs, t)
		}
	}
	for lID, lData := range j.Ledger.Objects {
		if exist := newType[lData.Type]; !exist &&
			!lData.used {
			lTX := j.Ledger.Objects[lID]
			lTX.Time = float64(time.Now().Local().Unix())
			lTX.Amount = "1.0"
			lTX.Balance = "1.0"
			lTX.RefID = "XXX"
			b, err := encjson.Marshal(lTX)
			if err == nil {
				newType[lData.Type] = true
				unknowns = append(unknowns, string(b))
			}
		}
	}
	return
}

func (assets KrakenAssets) getAltName(asset string) string {
	if n, ok := assets[asset]; ok {
		asset = n.AltName
	}
	asset = strings.TrimSuffix(asset, ".S")
	asset = strings.TrimSuffix(asset, ".M")
	asset = strings.TrimSuffix(asset, ".HOLD")
	asset = strings.NewReplacer(
		"FLOWH", "FLOW",
		"KFEE", "FEE",
		"REPV2", "REP",
		"XBT", "BTC",
		"XETC", "ETC",
		"XETH", "ETH",
		"XLTC", "LTC",
		"XMLN", "MLN",
		"XREP", "REP",
		"XXBT", "BTC",
		"XXDG", "DOGE",
		"XXLM", "XLM",
		"XXMR", "XMR",
		"XXRP", "XRP",
		"XZEC", "ZEC",
		"ZAUD", "AUD",
		"ZCAD", "CAD",
		"ZEUR", "EUR",
		"ZGBP", "GBP",
		"ZJPY", "JPY",
		"ZUSD", "USD",
	).Replace(asset)
	return asset
}
