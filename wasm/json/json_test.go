package json

import (
	"reflect"
	"testing"

	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

func TestDigest(t *testing.T) {
	type args struct {
		jsonData []byte
	}
	tests := []struct {
		name       string
		args       args
		wantTxs    tx.TXs
		wantSource string
		wantErr    bool
	}{
		{
			name: "Broken JSON",
			args: args{
				jsonData: []byte("{"),
			},
			wantErr: true,
		},
		{
			name: "Empty JSON",
			args: args{
				jsonData: []byte("{}"),
			},
			wantErr: true,
		},
		{
			name: "Unknown JSON",
			args: args{
				jsonData: []byte("{\"some\":\"value\"}"),
			},
			wantErr: true,
		},
		{
			name: "Binance Asset Dividend",
			args: args{
				jsonData: []byte("{\"rows\": [],\"total\": 1}"),
			},
			// wantSource: "Binance Asset Dividend",
			wantErr: true,
		},
		{
			name: "CdC Exchange Export JS",
			args: args{
				// jsonData: []byte("{\"withs\":{\"financeList\":[{\"symbol\":\"BTC\",\"reason\":\"\",\"amount\":\"1.23399969\",\"fee\":0.05,\"updateAt\":\"2020-08-21 18:50:00\",\"txid\":\"130bab8c-08dc-442b-a1b8-618b07ad140a\",\"label\":\"\",\"addressTo\":\"To Crypto.com App\",\"network\":null,\"createdAt\":\"2020-08-21 18:49:20\",\"txidAddr\":\"https://blockchair.com/bitcoin/transaction/130bab8c-08dc-442b-a1b8-618b07ad140a?from=crypto.com\",\"updateAtTime\":1598035800000,\"createdAtTime\":1598035760000,\"id\":349909,\"status_text\":\"Completed\",\"status\":5}],\"count\":1,\"pageSize\":200}}"),
				jsonData: []byte("{\"withs\":{\"financeList\":[],\"count\":0,\"pageSize\":200}}"),
			},
			// wantSource: "CdC Exchange Export JS",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotTxs, gotSource, _, _, _, err := Digest(tt.args.jsonData)
			if (err != nil) != tt.wantErr {
				t.Errorf("Digest() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotTxs, tt.wantTxs) {
				t.Errorf("Digest() = %v, want %v", gotTxs, tt.wantTxs)
			}
			if gotSource != tt.wantSource {
				t.Errorf("Digest() gotSource = %v, want %v", gotSource, tt.wantSource)
			}
		})
	}
}
