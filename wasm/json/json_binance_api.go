package json

import (
	encjson "encoding/json"
	"errors"
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

type BinanceDeposit struct {
	Amount       string `json:"amount"`
	Coin         string `json:"coin"`
	Network      string `json:"network"`
	Status       int    `json:"status"`
	Address      string `json:"address"`
	AddressTag   string `json:"addressTag"`
	TxID         string `json:"txId"`
	InsertTime   int64  `json:"insertTime"`
	TransferType int    `json:"transferType"`
	ConfirmTimes string `json:"confirmTimes"`
}

type BinanceTrade struct {
	Symbol          string `json:"symbol"`
	ID              int    `json:"id"`
	OrderID         int    `json:"orderId"`
	OrderListID     int    `json:"orderListId"`
	BaseAsset       string `json:"baseAsset"`
	QuoteAsset      string `json:"quoteAsset"`
	Price           string `json:"price"`
	Qty             string `json:"qty"`
	QuoteQty        string `json:"quoteQty"`
	Commission      string `json:"commission"`
	CommissionAsset string `json:"commissionAsset"`
	Time            int64  `json:"time"`
	IsBuyer         bool   `json:"isBuyer"`
	IsMaker         bool   `json:"isMaker"`
	IsBestMatch     bool   `json:"isBestMatch"`
}

type BinanceWithdrawal struct {
	Address         string `json:"address"`
	Amount          string `json:"amount"`
	ApplyTime       string `json:"applyTime"`
	Coin            string `json:"coin"`
	ConfirmNo       int    `json:"confirmNo"`
	ID              string `json:"id"`
	Info            string `json:"info"`
	Network         string `json:"network"`
	Status          int    `json:"status"`
	TransactionFee  string `json:"transactionFee"`
	TransferType    int    `json:"transferType"`
	TxID            string `json:"txId"`
	WithdrawOrderID string `json:"withdrawOrderId,omitempty"`
}

type BinanceAssetDividend struct {
	Amount  string `json:"amount"`
	Asset   string `json:"asset"`
	DivTime int64  `json:"divTime"`
	Info    string `json:"enInfo"`
	TranID  int64  `json:"tranId"`
}

type BinanceAssetDividends struct {
	Objects   map[string]BinanceAssetDividend `json:"objects,omitempty"`
	StartTime int64                           `json:"startTime,omitempty"`
}

type BinanceDeposits struct {
	Objects   map[string]BinanceDeposit `json:"objects,omitempty"`
	StartTime int64                     `json:"startTime,omitempty"`
}

type BinanceTrades struct {
	Objects   map[string]BinanceTrade `json:"objects,omitempty"`
	StartTime int64                   `json:"startTime,omitempty"`
}

type BinanceWithdrawals struct {
	Objects   map[string]BinanceWithdrawal `json:"objects,omitempty"`
	StartTime int64                        `json:"startTime,omitempty"`
}

type BinanceJSON struct {
	AssetDividends BinanceAssetDividends `json:"assetDividends"`
	Deposits       BinanceDeposits       `json:"deposits"`
	Trades         BinanceTrades         `json:"trades"`
	Withdrawals    BinanceWithdrawals    `json:"withdrawals"`
}

var binanceAPI = &BinanceJSON{}

// Digest : Digest a Binance API JSON file
func (j BinanceJSON) Digest(jsonData []byte) (txs tx.TXs, source string, wallet string, unknowns []string, addresses []string, err error) {
	// newType := make(map[string]bool)
	err = encjson.Unmarshal(jsonData, &j)
	if len(j.Deposits.Objects) == 0 &&
		len(j.Trades.Objects) == 0 &&
		len(j.Withdrawals.Objects) == 0 &&
		len(j.AssetDividends.Objects) == 0 { // Mandatory fields
		err = errors.New("JSON Format does not match")
	}
	if err != nil {
		return
	}
	source = "Binance API"
	wallet = "Binance"
	for _, div := range j.AssetDividends.Objects {
		amount, err := decimal.NewFromString(div.Amount)
		if err == nil {
			t := tx.TX{Timestamp: time.Unix(div.DivTime/1e3, 0), ID: strconv.FormatInt(div.TranID, 10), Source: source, Wallet: "Binance", Method: "API", Note: div.Info}
			t.Items = make(map[string]tx.Values)
			t.Items["To"] = append(t.Items["To"], tx.Value{Code: div.Asset, Amount: amount})
			if div.Info == "BNB Vault" ||
				div.Info == "Locked Staking" ||
				div.Info == "DeFi Staking" ||
				div.Info == "Launchpool" ||
				div.Info == "XVS distribution" || // maybe should be better to get this list by API of by https://launchpad.binance.com/en/viewall/lp
				div.Info == "FLM distribution" ||
				div.Info == "CTK distribution" ||
				div.Info == "ALPHA distribution" ||
				div.Info == "HARD distribution" ||
				div.Info == "UNFI distribution" ||
				div.Info == "JUV distribution" ||
				div.Info == "PSG distribution" ||
				div.Info == "REEF distribution" ||
				div.Info == "ASR distribution" ||
				div.Info == "ATM distribution" ||
				div.Info == "OG distribution" ||
				div.Info == "BTCST distribution" ||
				div.Info == "LIT distribution" ||
				div.Info == "DODO distribution" ||
				div.Info == "ALICE distribution" ||
				div.Info == "TLM distribution" {
				t.FiscalCateg = tx.Minings
			} else if div.Info == "Flexible Savings" {
				t.FiscalCateg = tx.Interests
			} else {
				t.FiscalCateg = tx.AirDrops
			}
			txs = append(txs, t)
		}
	}
	for tID, tData := range j.Deposits.Objects {
		t := tx.TX{Timestamp: time.UnixMilli(tData.InsertTime), ID: tData.TxID, Source: source, Wallet: "Binance", Method: "API", Note: "from " + tData.Address}
		t.Items = make(map[string]tx.Values)
		amount, err := decimal.NewFromString(tData.Amount)
		if err != nil {
			fmt.Println(tID, "Error while parsing Amount", tData.Amount)
			continue
		}
		t.Items["To"] = append(t.Items["To"], tx.Value{Code: tData.Coin, Amount: amount})
		t.FiscalCateg = tx.Deposits
		txs = append(txs, t)
	}
	for tID, tData := range j.Withdrawals.Objects {
		timestamp, err := time.Parse("2006-01-02 15:04:05", tData.ApplyTime)
		if err != nil {
			fmt.Println(tID, "Error while parsing Time", tData.ApplyTime)
			continue
		}
		amount, err := decimal.NewFromString(tData.Amount)
		if err != nil {
			fmt.Println(tID, "Error while parsing Amount", tData.Amount)
			continue
		}
		fee, err := decimal.NewFromString(tData.TransactionFee)
		if err != nil {
			fmt.Println(tID, "Error while parsing TransactionFee", tData.TransactionFee)
			continue
		}
		t := tx.TX{Timestamp: timestamp, ID: tData.ID, Source: source, Wallet: "Binance", Method: "API", Note: "to " + tData.Address}
		t.Items = make(map[string]tx.Values)
		t.Items["From"] = append(t.Items["From"], tx.Value{Code: tData.Coin, Amount: amount})
		if !fee.IsZero() {
			t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: tData.Coin, Amount: fee})
		}
		t.FiscalCateg = tx.Withdrawals
		txs = append(txs, t)
	}
	for tID, tData := range j.Trades.Objects {
		qty, err := decimal.NewFromString(tData.Qty)
		if err != nil {
			log.Println(tID, "Error while parsing Qty", tData.Qty)
			continue
		}
		quoteQty, err := decimal.NewFromString(tData.QuoteQty)
		if err != nil {
			log.Println(tID, "Error while parsing QuoteQty", tData.QuoteQty)
			continue
		}
		fee, err := decimal.NewFromString(tData.Commission)
		if err != nil {
			log.Println(tID, "Error while parsing Commission", tData.Commission)
			continue
		}
		t := tx.TX{Timestamp: time.UnixMilli(tData.Time), ID: strconv.Itoa(tData.OrderID), Source: source, Wallet: "Binance", Method: "API", Note: strconv.Itoa(tData.ID)}
		t.Items = make(map[string]tx.Values)
		if tData.IsBuyer {
			t.Items["From"] = append(t.Items["From"], tx.Value{Code: tData.QuoteAsset, Amount: quoteQty})
			t.Items["To"] = append(t.Items["To"], tx.Value{Code: tData.BaseAsset, Amount: qty})
		} else {
			t.Items["From"] = append(t.Items["From"], tx.Value{Code: tData.BaseAsset, Amount: qty})
			t.Items["To"] = append(t.Items["To"], tx.Value{Code: tData.QuoteAsset, Amount: quoteQty})
		}
		if !fee.IsZero() {
			t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: tData.CommissionAsset, Amount: fee})
		}
		t.FiscalCateg = tx.Exchanges
		found := false
		for i, ex := range txs {
			if ex.ID == t.ID {
				found = true
				for k, v := range t.Items {
					txs[i].Items[k] = append(txs[i].Items[k], v...)
				}
				break
			}
		}
		if !found {
			txs = append(txs, t)
		}
	}
	return
}
