package json

import (
	encjson "encoding/json"
	"errors"
	"log"
	"sort"
	"strconv"
	"time"

	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

type solscanJSON struct {
	// Transactions grouped by addresses
	Addresses map[string]map[string]solscanTransaction `json:"solscan_addresses"`
}

type solscanTransaction struct {
	BlockTime    int64    `json:"blockTime"`
	Slot         int      `json:"slot"`
	TxHash       string   `json:"txHash"`
	Fee          int64    `json:"fee"`
	Status       string   `json:"status"`
	Lamport      int64    `json:"lamport"`
	Signer       []string `json:"signer"`
	LogMessage   []string `json:"logMessage"`
	InputAccount []struct {
		Account     string          `json:"account"`
		Signer      bool            `json:"signer"`
		Writable    bool            `json:"writable"`
		PreBalance  decimal.Decimal `json:"preBalance"`
		PostBalance decimal.Decimal `json:"postBalance"`
	} `json:"inputAccount"`
	RecentBlockHash   string `json:"recentBlockhash"`
	InnerInstructions []struct {
		Index              int                        `json:"index"`
		ParsedInstructions []solscanParsedInstruction `json:"parsedInstructions"`
	} `json:"innerInstructions"`
	TokenBalanes       []interface{}              `json:"tokenBalanes"`
	ParsedInstructions []solscanParsedInstruction `json:"parsedInstruction"`
	Confirmations      int                        `json:"confirmations"`
	TokenTransfers     []interface{}              `json:"tokenTransfers"`
	SolTransfers       []struct {
		Source      string          `json:"source"`
		Destination string          `json:"destination"`
		Amount      decimal.Decimal `json:"amount"`
	} `json:"solTransfers"`
	SerumTransactions   []interface{} `json:"serumTransactions"`
	RaydiumTransactions []interface{} `json:"raydiumTransactions"`
	UnknownTransfers    []struct {
		ProgramId string `json:"programId"`
		Event     []struct {
			Source           string          `json:"source"`
			Destination      string          `json:"destination"`
			Amount           decimal.Decimal `json:"amount"`
			Type             string          `json:"type"`
			Decimals         int32           `json:"decimals"`
			Symbol           string          `json:"symbol"`
			TokenAddress     string          `json:"tokenAddress"`
			SourceOwner      string          `json:"sourceOwner"`
			DestinationOwner string          `json:"destinationOwner"`
		} `json:"event"`
	} `json:"unknownTransfers"`
}

type solscanParsedInstruction struct {
	ProgramId  string `json:"programId"`
	Type       string `json:"type"`
	Data       string `json:"data"`
	DataEncode string `json:"dataEncode"`
	Name       string `json:"source"`
	Params     struct {
		Account           string          `json:"account"`
		Account0          string          `json:"account0"`
		Account1          string          `json:"account1"`
		Account2          string          `json:"account2"`
		Account3          string          `json:"account3"`
		Account4          string          `json:"account4"`
		Account5          string          `json:"account5"`
		Account6          string          `json:"account6"`
		Amount            decimal.Decimal `json:"amount"`
		AssociatedAccount string          `json:"associatedAccount"`
		Authority         string          `json:"authority"`
		ClosedAccount     string          `json:"closedAccount"`
		Destination       string          `json:"destination"`
		InitAcount        string          `json:"initAcount"`
		Mint              string          `json:"mint"`
		MintAuthority     string          `json:"mintAuthority"`
		Owner             string          `json:"owner"`
		NewAccount        string          `json:"newAccount"`
		TransferAmountSol decimal.Decimal `json:"transferAmount(SOL)"`
		Source            string          `json:"source"`
	} `json:"params"`
	Extra struct {
		Amount           decimal.Decimal `json:"amount"`
		Authority        string          `json:"authority"`
		Decimals         int32           `json:"decimals"`
		Destination      string          `json:"destination"`
		DestinationOwner string          `json:"destinationOwner"`
		Source           string          `json:"source"`
		SourceOwner      string          `json:"sourceOwner"`
		Symbol           string          `json:"symbol"`
	} `json:"extra"`
}

const solscanSolDecimals = 9
const metaplexTokenMetadataAddress = "metaqbxxUerdq28cj1RbAWkYQm3ybzjb6a8bt518x1s"

var solscan = &solscanJSON{}

type solscanJSONAddress struct {
	address          string
	solBalances      map[string]decimal.Decimal
	debugSolBalances bool
	transactions     map[string]solscanTransaction
}

// Digest : Digest a BlockStream AddressTransactions JSON file
func (j solscanJSON) Digest(jsonData []byte) (txs tx.TXs, source string, wallet string, unknowns []string, addresses []string, err error) {
	err = encjson.Unmarshal(jsonData, &j)
	if len(j.Addresses) == 0 { // Mandatory fields
		err = errors.New("JSON Format does not match")
	}
	if err != nil {
		return
	}
	source = "Solscan API"
	wallet = "Solana"
	debugAddressesSolBalances := true

	addresses = make([]string, 0)
	subAddresses := make(map[string]map[string]struct{})
	for address, transactions := range j.Addresses {
		jsonAddress := solscanJSONAddress{address: address, transactions: transactions, debugSolBalances: debugAddressesSolBalances}
		if jsonAddress.debugSolBalances {
			jsonAddress.solBalances = make(map[string]decimal.Decimal)
		}
		subAddresses[address] = make(map[string]struct{})
		for _, t := range jsonAddress.toSortedSolscanTransaction() {
			tCffTxs := make([]tx.TX, 0)
			feesPayer := false
			for _, signer := range t.Signer {
				if signer == address {
					feesPayer = true
					break
				}
			}

			success := t.Status == "Success"

			if success {
				for _, inst := range t.ParsedInstructions {
					cffT := jsonAddress.createCffT(source, subAddresses[address], len(tCffTxs), t, inst)
					if cffT.ID != "" {
						tCffTxs = append(tCffTxs, cffT)
					}
				}

				for _, innerInst := range t.InnerInstructions {
					for _, inst := range innerInst.ParsedInstructions {
						cffT := jsonAddress.createCffT(source, subAddresses[address], len(tCffTxs), t, inst)
						if cffT.ID != "" {
							tCffTxs = append(tCffTxs, cffT)
						}
					}
				}
			}

			// Merge exchanges
			if len(tCffTxs) == 2 && (tCffTxs[0].FiscalCateg == tx.Deposits && tCffTxs[1].FiscalCateg == tx.Withdrawals ||
				tCffTxs[0].FiscalCateg == tx.Withdrawals && tCffTxs[1].FiscalCateg == tx.Deposits) {
				tCffTxs[0].FiscalCateg = tx.Exchanges
				tCffTxs[0].Items["From"] = append(tCffTxs[0].Items["From"], tCffTxs[1].Items["From"]...)
				tCffTxs[0].Items["To"] = append(tCffTxs[0].Items["To"], tCffTxs[1].Items["To"]...)
				tCffTxs = tCffTxs[:1]
			}

			if feesPayer {
				fees := tx.Value{Code: "SOL", Amount: decimal.New(t.Fee, -solscanSolDecimals)}
				if len(tCffTxs) == 1 {
					tCffTxs[0].Items["Fee"] = append(tCffTxs[0].Items["Fee"], fees)
				} else {
					cffT := tx.TX{Timestamp: time.Unix(t.BlockTime, 0), ID: t.TxHash + strconv.Itoa(len(tCffTxs)), Source: source, Wallet: "Solana", Method: "API", Items: make(map[string]tx.Values),
						FiscalCateg: tx.Fees,
						Note:        strconv.Itoa(t.Slot) + " " + t.TxHash}
					cffT.Items["Fee"] = append(cffT.Items["Fee"], fees)
					tCffTxs = append(tCffTxs, cffT)
				}
				jsonAddress.subBalance(address, fees.Amount)
			}

			txs = append(txs, tCffTxs...)
		}
		addresses = append(addresses, address)
		for subAddress := range subAddresses[address] {
			addresses = append(addresses, subAddress)
		}

		if jsonAddress.debugSolBalances {
			total := decimal.Zero
			for address, balance := range jsonAddress.solBalances {
				log.Println("["+address+"]", balance)
				total = total.Add(balance)
			}
			log.Println("total:", total)
		}
	}

	return
}

func (jsonAddress solscanJSONAddress) addBalance(address string, amount decimal.Decimal) {
	if jsonAddress.debugSolBalances {
		if _, exist := jsonAddress.solBalances[address]; !exist {
			jsonAddress.solBalances[address] = decimal.Zero
		}
		jsonAddress.solBalances[address] = jsonAddress.solBalances[address].Add(amount)
	}
}

func (jsonAddress solscanJSONAddress) subBalance(address string, amount decimal.Decimal) {
	if jsonAddress.debugSolBalances {
		if _, exist := jsonAddress.solBalances[address]; !exist {
			jsonAddress.solBalances[address] = decimal.Zero
		}
		jsonAddress.solBalances[address] = jsonAddress.solBalances[address].Sub(amount)
	}
}

func (jsonAddress solscanJSONAddress) toSortedSolscanTransaction() []solscanTransaction {
	slice := make([]solscanTransaction, len(jsonAddress.transactions))

	i := 0
	for _, tx := range jsonAddress.transactions {
		slice[i] = tx
		i++
	}

	sort.Slice(slice, func(i, j int) bool {
		return slice[i].BlockTime < slice[j].BlockTime
	})

	return slice
}

func (jsonAddress solscanJSONAddress) createCffT(source string, subAddresses map[string]struct{}, txSplitIndex int, t solscanTransaction, inst solscanParsedInstruction) (cffT tx.TX) {
	switch inst.Type {
	case "createAccount":
		if inst.Params.Source == jsonAddress.address && inst.Params.TransferAmountSol.IsPositive() {
			cffT = tx.TX{Timestamp: time.Unix(t.BlockTime, 0), ID: t.TxHash + strconv.Itoa(txSplitIndex), Source: source, Wallet: "Solana", Method: "API", Items: make(map[string]tx.Values),
				FiscalCateg: tx.Withdrawals,
				Note:        strconv.Itoa(t.Slot) + " " + t.TxHash}
			value := tx.Value{Code: "SOL", Amount: inst.Params.TransferAmountSol}
			cffT.Items["From"] = append(cffT.Items["From"], value)
			jsonAddress.subBalance(jsonAddress.address, value.Amount)

			if t.isNewAccountATokenUnderAuthorityOfAddress(inst.Params.NewAccount, jsonAddress.address) {
				cffT.FiscalCateg = tx.Fees
			} else if t.isInitializedToAddress(inst.Params.NewAccount, jsonAddress.address) {
				subAddresses[inst.Params.NewAccount] = struct{}{}
			}
			if _, exists := subAddresses[inst.Params.NewAccount]; exists {
				cffT.FiscalCateg = tx.Transfers
				cffT.Items["To"] = append(cffT.Items["To"], value)
				jsonAddress.addBalance(inst.Params.NewAccount, value.Amount)
			}
		}
	case "createAssociatedAccount":
		if inst.Params.Authority == jsonAddress.address {
			subAddresses[inst.Params.AssociatedAccount] = struct{}{}
		}
	case "closeAccount":
		closeAccount := inst.Params.ClosedAccount
		closeAccountAmount := decimal.Zero
		addressChangeAmount := decimal.Zero
		for _, inputAccount := range t.InputAccount {
			if inputAccount.Account == closeAccount {
				closeAccountAmount = inputAccount.PreBalance.Sub(inputAccount.PostBalance)
			}
			if inputAccount.Account == jsonAddress.address {
				addressChangeAmount = inputAccount.PostBalance.Sub(inputAccount.PreBalance)
			}
		}
		if addressChangeAmount.Add(decimal.New(t.Fee, 0)).Cmp(closeAccountAmount) == 0 {
			value := tx.Value{Code: "SOL", Amount: decimal.New(closeAccountAmount.IntPart(), -solscanSolDecimals)}
			cffT = tx.TX{Timestamp: time.Unix(t.BlockTime, 0), ID: t.TxHash + strconv.Itoa(txSplitIndex), Source: source, Wallet: "Solana", Method: "API", Items: make(map[string]tx.Values),
				FiscalCateg: tx.Deposits,
				Note:        strconv.Itoa(t.Slot) + " " + t.TxHash}
			cffT.Items["To"] = append(cffT.Items["To"], value)
			jsonAddress.addBalance(jsonAddress.address, value.Amount)

			if _, exists := subAddresses[closeAccount]; exists {
				cffT.Items["From"] = append(cffT.Items["From"], value)
				cffT.FiscalCateg = tx.Transfers
				jsonAddress.subBalance(closeAccount, value.Amount)
			}
		}
	case "sol-transfer":
		if inst.Params.Source == jsonAddress.address || inst.Params.Destination == jsonAddress.address {
			value := tx.Value{Code: "SOL", Amount: decimal.New(inst.Params.Amount.IntPart(), -solscanSolDecimals)}
			cffT = tx.TX{Timestamp: time.Unix(t.BlockTime, 0), ID: t.TxHash + strconv.Itoa(txSplitIndex), Source: source, Wallet: "Solana", Method: "API", Items: make(map[string]tx.Values),
				Note: strconv.Itoa(t.Slot) + " " + t.TxHash}
			if inst.Params.Source == jsonAddress.address {
				cffT.FiscalCateg = tx.Withdrawals
				cffT.Items["From"] = append(cffT.Items["From"], value)
				jsonAddress.subBalance(jsonAddress.address, value.Amount)
				if t.isOwnedByMetaplexTokenMetadata(inst.Params.Destination) {
					cffT.FiscalCateg = tx.Fees
				} else if _, exists := subAddresses[inst.Params.Destination]; exists {
					cffT.FiscalCateg = tx.Transfers
					cffT.Items["To"] = append(cffT.Items["To"], value)
					jsonAddress.addBalance(inst.Params.Destination, value.Amount)
				}
			} else if inst.Params.Destination == jsonAddress.address {
				cffT.FiscalCateg = tx.Deposits
				cffT.Items["To"] = append(cffT.Items["To"], value)
				jsonAddress.addBalance(jsonAddress.address, value.Amount)
				if _, exists := subAddresses[inst.Params.Source]; exists {
					cffT.FiscalCateg = tx.Transfers
					cffT.Items["From"] = append(cffT.Items["From"], value)
					jsonAddress.subBalance(inst.Params.Source, value.Amount)
				}
			}
		}
	case "spl-transfer":
		if inst.Extra.Symbol != "" && !inst.Extra.Amount.IsZero() {
			symbol := inst.Extra.Symbol
			if inst.Extra.Symbol == "GST" {
				symbol = "GST-SOL"
			}

			if _, exists := subAddresses[inst.Params.Source]; inst.Extra.SourceOwner == jsonAddress.address && !exists {
				subAddresses[inst.Extra.Source] = struct{}{}
			}
			if _, exists := subAddresses[inst.Params.Destination]; inst.Extra.DestinationOwner == jsonAddress.address && !exists {
				subAddresses[inst.Extra.Destination] = struct{}{}
			}
			if _, exists := subAddresses[inst.Params.Source]; exists {
				value := tx.Value{Code: symbol, Amount: decimal.New(inst.Extra.Amount.IntPart(), -inst.Extra.Decimals)}
				cffT = tx.TX{Timestamp: time.Unix(t.BlockTime, 0), ID: t.TxHash + strconv.Itoa(txSplitIndex), Source: source, Wallet: "Solana", Method: "API", Items: make(map[string]tx.Values),
					Note: strconv.Itoa(t.Slot) + " " + t.TxHash}
				cffT.FiscalCateg = tx.Withdrawals
				cffT.Items["From"] = append(cffT.Items["From"], value)
			} else if _, exists := subAddresses[inst.Params.Destination]; exists {
				value := tx.Value{Code: symbol, Amount: decimal.New(inst.Extra.Amount.IntPart(), -inst.Extra.Decimals)}
				cffT = tx.TX{Timestamp: time.Unix(t.BlockTime, 0), ID: t.TxHash + strconv.Itoa(txSplitIndex), Source: source, Wallet: "Solana", Method: "API", Items: make(map[string]tx.Values),
					Note: strconv.Itoa(t.Slot) + " " + t.TxHash}
				cffT.FiscalCateg = tx.Deposits
				cffT.Items["To"] = append(cffT.Items["To"], value)
			}
		}
	}
	return
}

// Detect nft token metadata account
func (t solscanTransaction) isOwnedByMetaplexTokenMetadata(address string) bool {
	for _, innerInst := range t.InnerInstructions {
		for _, inst := range innerInst.ParsedInstructions {
			if inst.Type == "assign" && inst.Params.Account == address && inst.Params.Owner == metaplexTokenMetadataAddress {
				return true
			}
		}
	}
	return false
}

func (t solscanTransaction) isInitializedToAddress(subAddress string, address string) bool {
	for _, inst := range t.ParsedInstructions {
		if inst.Type == "initializeAccount" && inst.Params.InitAcount == subAddress && inst.Params.Owner == address {
			return true
		}
	}
	return false
}

func (t solscanTransaction) isNewAccountATokenUnderAuthorityOfAddress(newAccount string, address string) bool {
	for _, inst := range t.ParsedInstructions {
		if inst.Type == "initializeMint" && inst.Params.Mint == newAccount && inst.Params.MintAuthority == address {
			return true
		}
	}
	return false
}
