package json

import (
	"encoding/base64"
	encjson "encoding/json"
	"errors"
	"strconv"
	"strings"
	"time"

	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

type HitBTCNative struct {
	TxID          string `json:"tx_id"`
	Index         int    `json:"index"`
	Currency      string `json:"currency"`
	Amount        string `json:"amount"`
	Fee           string `json:"fee"`
	Hash          string `json:"hash"`
	Address       string `json:"address"`
	Confirmations int    `json:"confirmations"`
}

type HitBTCAccountTransaction struct {
	ID        int          `json:"id"`
	CreatedAt time.Time    `json:"created_at"`
	UpdatedAt time.Time    `json:"updated_at"`
	Status    string       `json:"status"`
	Type      string       `json:"type"`
	Subtype   string       `json:"subType"`
	Native    HitBTCNative `json:"native"`
}

func (transaction HitBTCAccountTransaction) anonymize() string {
	transaction.ID = 0
	transaction.CreatedAt = time.Now()
	transaction.UpdatedAt = time.Now()
	transaction.Native.TxID = "fake"
	transaction.Native.Amount = "1"
	transaction.Native.Hash = "fake"
	transaction.Native.Address = "fake"
	b, _ := encjson.Marshal(transaction)
	return base64.StdEncoding.EncodeToString(b)
}

type HitBTCTrade struct {
	ID                 int       `json:"id"`
	ClientOrderID      string    `json:"client_order_id"`
	Symbol             string    `json:"symbol"`
	Side               string    `json:"side"`
	Status             string    `json:"status"`
	Type               string    `json:"type"`
	TimeInForce        string    `json:"time_in_force"`
	Quantity           string    `json:"quantity"`
	QuantityCumulative string    `json:"quantity_cumulative"`
	Price              string    `json:"price"`
	PostOnly           bool      `json:"post_only"`
	CreatedAt          time.Time `json:"created_at"`
	UpdatedAt          time.Time `json:"updated_at"`
	ReportType         string    `json:"report_type"`
	// Fee                string    `json:"fee"`
}

type HitBTCJSON struct {
	Transactions map[int]HitBTCAccountTransaction `json:"transactions"`
	Trades       map[int]HitBTCTrade              `json:"trades"`
}

var hitbtc = &HitBTCJSON{}

func (j HitBTCJSON) Digest(jsonData []byte) (txs tx.TXs, source string, wallet string, unknowns []string, addresses []string, err error) {
	err = encjson.Unmarshal(jsonData, &j)
	if len(j.Transactions) == 0 &&
		len(j.Trades) == 0 { // Mandatory fields
		err = errors.New("JSON Format does not match")
	}
	if err != nil {
		return
	}
	source = "HitBTC API"
	wallet = "HitBTC"
	newTX := make(map[string]string)
	for _, transaction := range j.Transactions {
		if transaction.Status != "SUCCESS" {
			continue
		}
		// https://blog.hitbtc.com/we-will-change-the-ticker-of-bchabc-to-bch-and-bchsv-will-be-displayed-as-hbv/
		currency := strings.NewReplacer("BCHA", "BCH", "BCHOLD", "BCH").Replace(transaction.Native.Currency)
		amount, err := decimal.NewFromString(transaction.Native.Amount)
		if err != nil {
			// log.Println(SOURCE, "Error Parsing Amount : ", transaction.Amount)
			continue
		}
		t := tx.TX{Timestamp: transaction.UpdatedAt, ID: transaction.Native.TxID, Source: source, Wallet: "HitBTC", Method: "API", Note: transaction.Type}
		t.Items = make(map[string]tx.Values)
		if transaction.Native.Fee != "" {
			fee, err := decimal.NewFromString(transaction.Native.Fee)
			if err != nil {
				// log.Println(SOURCE, "Error Parsing Fee : ", transaction.Fee)
				continue
			}
			if !fee.IsZero() {
				t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: currency, Amount: fee})
			}
		}
		if transaction.Type == "DEPOSIT" {
			t.Items["To"] = append(t.Items["To"], tx.Value{Code: currency, Amount: amount})
			if transaction.Subtype == "AIRDROP" {
				t.FiscalCateg = tx.AirDrops
			} else if transaction.Subtype == "AFFILIATE" {
				t.FiscalCateg = tx.Referrals
			} else if transaction.Subtype == "STAKING" {
				t.FiscalCateg = tx.Minings
			} else {
				t.FiscalCateg = tx.Deposits
			}
			txs = append(txs, t)
		} else if transaction.Type == "WITHDRAW" {
			t.Note += " " + transaction.Native.Hash + " -> " + transaction.Native.Address
			t.Items["From"] = append(t.Items["From"], tx.Value{Code: currency, Amount: amount})
			t.FiscalCateg = tx.Withdrawals
			txs = append(txs, t)
		} else if transaction.Type == "TRANSFER" {
			t.FiscalCateg = tx.DontCare
			// txs = append(txs, t)
			// } else if transaction.Type == "SWAP" { // Not yet seen, wait for user feedback
		} else {
			newTX[transaction.Type] = transaction.anonymize()
		}
	}
	for _, trade := range j.Trades {
		// https://blog.hitbtc.com/we-will-change-the-ticker-of-bchabc-to-bch-and-bchsv-will-be-displayed-as-hbv/
		symbol := strings.NewReplacer("BCHA", "BCH", "BCHOLD", "BCH").Replace(trade.Symbol)
		quantity, err := decimal.NewFromString(trade.Quantity)
		if err != nil {
			// log.Println(SOURCE, "Error Parsing Quantity : ", trade.Quantity)
			continue
		}
		price, err := decimal.NewFromString(trade.Price)
		if err != nil {
			// log.Println(SOURCE, "Error Parsing Price : ", trade.Price)
			continue
		}
		// fee, err := decimal.NewFromString(trade.Fee)
		// if err != nil {
		// 	// log.Println(SOURCE, "Error Parsing Fee : ", trade.Fee)
		// 	continue
		// }

		t := tx.TX{Timestamp: trade.UpdatedAt, ID: strconv.Itoa(trade.ID), Source: source, Wallet: "HitBTC", Method: "API", Note: trade.ClientOrderID}
		t.Items = make(map[string]tx.Values)
		curr := []string{symbol[:3], symbol[3:]}
		if trade.Side == "sell" {
			t.Items["From"] = append(t.Items["From"], tx.Value{Code: curr[0], Amount: quantity})
			t.Items["To"] = append(t.Items["To"], tx.Value{Code: curr[1], Amount: quantity.Mul(price)})
		} else { // if trade.Side == "buy"
			t.Items["From"] = append(t.Items["From"], tx.Value{Code: curr[1], Amount: quantity})
			t.Items["To"] = append(t.Items["To"], tx.Value{Code: curr[0], Amount: quantity.Mul(price)})
		}
		// if !fee.IsZero() {
		// 	t.Items["Fee"] = append(t.Items["Fee"], tx.Value{Code: curr[0], Amount: fee})
		// }
		t.FiscalCateg = tx.Exchanges
		txs = append(txs, t)
	}
	for _, v := range newTX {
		unknowns = append(unknowns, v)
	}
	return
}
