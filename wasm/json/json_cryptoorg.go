package json

import (
	encjson "encoding/json"
	"errors"
	// "log"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

type CryptoOrgTransaction struct {
	Account       string    `json:"account"`
	BlockHeight   int       `json:"blockHeight"`
	BlockHash     string    `json:"blockHash"`
	BlockTime     string    `json:"blockTime"`
	Hash          string    `json:"hash"`
	MessageTypes  []string  `json:"messageTypes"`
	Success       bool      `json:"success"`
	Code          int       `json:"code"`
	Log           string    `json:"log"`
	Fee           []Amount  `json:"fee"`
	FeePayer      string    `json:"feePayer"`
	FeeGranter    string    `json:"feeGranter"`
	GasWanted     int64     `json:"gasWanted"`
	GasUsed       int64     `json:"gasUsed"`
	Memo          string    `json:"memo"`
	TimeoutHeight int       `json:"timeoutHeight"`
	Messages      []Message `json:"messages"`
}

type Message struct {
	Type    string    `json:"type"`
	Content ContentTX `json:"content"`
}

type ContentTX struct {
	Name               string      `json:"name"`
	Height             int         `json:"height"`
	Version            int         `json:"version"`
	MsgIndex           int         `json:"msgIndex"`
	ToAddress          string      `json:"toAddress,omitempty"`
	Uuid               string      `json:"uuid,omitempty"`
	Amount             interface{} `json:"amount,omitempty"`
	TxHash             string      `json:"txHash"`
	MsgName            string      `json:"msgName"`
	FromAddress        string      `json:"fromAddress,omitempty"`
	AutoClaimedRewards Amount      `json:"autoClaimedRewards,omitempty"`
	ValidatorAddress   string      `json:"validatorAddress,omitempty"`
	DelegatorAddress   string      `json:"delegatorAddress,omitempty"`
	RecipientAddress   string      `json:"recipientAddress,omitempty"`
	Uri                string      `json:"uri,omitempty"`
	TokenId            string      `json:"tokenId,omitempty"`
	Data               string      `json:"data,omitempty"`
	DenomId            string      `json:"denomId,omitempty"`
	TokenName          string      `json:"tokenName,omitempty"`
	Sender             string      `json:"sender,omitempty"`
	Recipient          string      `json:"recipient,omitempty"`
	Params             struct {
		DestinationChannel string      `json:"destinationChannel,omitempty"`
		Token              Amount      `json:"token,omitempty"`
		PacketData         interface{} `json:"packetData,omitempty"`
		ConnectionId       string      `json:"connectionId,omitempty"`
		DestinationPort    string      `json:"destinationPort,omitempty"`
		TimeoutTimestamp   string      `json:"timeoutTimestamp,omitempty"`
		PacketSequence     string      `json:"packetSequence,omitempty"`
		ChannelOrdering    string      `json:"channelOrdering,omitempty"`
		Sender             string      `json:"sender,omitempty"`
		Receiver           string      `json:"receiver,omitempty"`
		MaybeMsgTransfer   struct {
			Sender   string `json:"sender,omitempty"`
			Receiver string `json:"receiver,omitempty"`
		}
		SourcePort    string      `json:"sourcePort,omitempty"`
		SourceChannel string      `json:"sourceChannel,omitempty"`
		TimeoutHeight interface{} `json:"channelOrdering,omitempty"`
	} `json:"params,omitempty"`
}

type Amount struct {
	Denom  string `json:"denom"`
	Amount string `json:"amount"`
}

type CryptoOrgJSON struct {
	Addresses map[string]map[string]CryptoOrgTransaction `json:"cryptoorg_addresses"`
}

var cryptoOrg = &CryptoOrgJSON{}
var cryptoOrgDenominator = decimal.New(1, 8)

// Digest : Digest a Crypto.org AddressTransactions JSON file
func (j CryptoOrgJSON) Digest(jsonData []byte) (txs tx.TXs, source string, wallet string, unknowns []string, addresses []string, err error) {
	layout := "2006-01-02T15:04:05.9Z" // Layout to retrieve the dates
	err = encjson.Unmarshal(jsonData, &j)
	if len(j.Addresses) == 0 { // Mandatory fields
		err = errors.New("JSON Format does not match")
	}
	if err != nil {
		return
	}
	source = "Crypto.org API"
	wallet = "Crypto.org"

	addresses = make([]string, 0)
	for addr, jsonTxs := range j.Addresses {
		addresses = append(addresses, addr)
		for _, jsonTx := range jsonTxs {
			if jsonTx.Success {
				txTime, _ := time.Parse(layout, jsonTx.BlockTime)
				hash := jsonTx.Hash
				createFeeTx := false
				for i, message := range jsonTx.Messages {
					content := message.Content
					id := hash + "-" + strconv.Itoa(i)
					note := message.Type
					if strings.Contains(message.Type, "MsgAcknowledgement") {
						// Nothing to do
					} else if strings.Contains(message.Type, "MsgDelegate") {
						if amount := sumAmounts(content.Amount); amount.GreaterThan(decimal.Zero) {
							t := tx.TX{Timestamp: txTime, ID: id, Source: source, FiscalCateg: tx.DontCare, Wallet: wallet, Method: apiMethod, Note: note,
								Items: map[string]tx.Values{
									"From": tx.Values{tx.Value{Code: "CRO", Amount: amount}},
									"To":   tx.Values{tx.Value{Code: "CRO", Amount: amount}},
								}}
							txs = append(txs, t)
						}
						addAutoClaimedRewardsTx(&txs, content, txTime, id, source, wallet, note)
						createFeeTx = true
					} else if strings.Contains(message.Type, "MsgMintNFT") {
						// No nft support for the moment
					} else if strings.Contains(message.Type, "MsgSend") {
						if content.ToAddress == addr {
							t := tx.TX{Timestamp: txTime, ID: id, Source: source, FiscalCateg: tx.Deposits, Wallet: wallet, Method: apiMethod, Note: note,
								Items: map[string]tx.Values{
									"To": tx.Values{tx.Value{Code: "CRO", Amount: sumAmounts(content.Amount)}},
								},
							}
							txs = append(txs, t)
						} else {
							t := tx.TX{Timestamp: txTime, ID: id, Source: source, FiscalCateg: tx.Withdrawals, Wallet: wallet, Method: apiMethod, Note: note,
								Items: map[string]tx.Values{
									"From": tx.Values{tx.Value{Code: "CRO", Amount: sumAmounts(content.Amount)}},
								},
							}
							if len(jsonTx.Messages) == 1 {
								t.Items["Fee"] = tx.Values{tx.Value{Code: "CRO", Amount: sumAmounts(jsonTx.Fee)}}
							} else {
								createFeeTx = true
							}
							txs = append(txs, t)
						}
					} else if strings.Contains(message.Type, "MsgUndelegate") {
						if amount := sumAmounts(content.Amount); amount.GreaterThan(decimal.Zero) {
							t := tx.TX{Timestamp: txTime, ID: id, Source: source, FiscalCateg: tx.DontCare, Wallet: wallet, Method: apiMethod, Note: note,
								Items: map[string]tx.Values{
									"From": tx.Values{tx.Value{Code: "CRO", Amount: amount}},
									"To":   tx.Values{tx.Value{Code: "CRO", Amount: amount}},
								}}
							txs = append(txs, t)
						}
						addAutoClaimedRewardsTx(&txs, content, txTime, id, source, wallet, note)
						createFeeTx = true
					} else if strings.Contains(message.Type, "MsgUpdateClient") {
						// Nothing to do
					} else if strings.Contains(message.Type, "MsgTransfer") {
						amountDec, _ := decimal.NewFromString(content.Params.Token.Amount)
						amountDec = amountDec.Div(cryptoOrgDenominator)
						if content.Params.Receiver == addr {
							t := tx.TX{Timestamp: txTime, ID: id, Source: source, FiscalCateg: tx.Deposits, Wallet: wallet, Method: apiMethod, Note: note,
								Items: map[string]tx.Values{
									"To": tx.Values{tx.Value{Code: "CRO", Amount: amountDec}},
								},
							}
							txs = append(txs, t)
						} else {
							t := tx.TX{Timestamp: txTime, ID: id, Source: source, FiscalCateg: tx.Withdrawals, Wallet: wallet, Method: apiMethod, Note: note,
								Items: map[string]tx.Values{
									"From": tx.Values{tx.Value{Code: "CRO", Amount: amountDec}},
								},
							}
							if len(jsonTx.Messages) == 1 {
								t.Items["Fee"] = tx.Values{tx.Value{Code: "CRO", Amount: sumAmounts(jsonTx.Fee)}}
							} else {
								createFeeTx = true
							}
							txs = append(txs, t)
						}
					} else if strings.Contains(message.Type, "MsgWithdrawDelegatorReward") {
						if amount := sumAmounts(content.Amount); amount.GreaterThan(decimal.Zero) {
							t := tx.TX{Timestamp: txTime, ID: id, Source: source, FiscalCateg: tx.Minings, Wallet: wallet, Method: apiMethod, Note: note,
								Items: map[string]tx.Values{
									"To": tx.Values{tx.Value{Code: "CRO", Amount: amount}},
								}}
							txs = append(txs, t)
						}
						createFeeTx = true
					} else if strings.Contains(message.Type, "MsgTransferNFT") {
						// Do nothing as NFT related
						// TODO to see however if this a transfer if we can handle it with EXCHANGE tx
					} else if strings.Contains(message.Type, "MsgBeginRedelegate") {
						if amount := sumAmounts(content.Amount); amount.GreaterThan(decimal.Zero) {
							t := tx.TX{Timestamp: txTime, ID: id, Source: source, FiscalCateg: tx.DontCare, Wallet: wallet, Method: apiMethod, Note: note,
								Items: map[string]tx.Values{
									"From": tx.Values{tx.Value{Code: "CRO", Amount: amount}},
									"To":   tx.Values{tx.Value{Code: "CRO", Amount: amount}},
								}}
							txs = append(txs, t)
						}
						addAutoClaimedRewardsTx(&txs, content, txTime, id, source, wallet, note)
						createFeeTx = true
					} else if strings.Contains(message.Type, "MsgGrant") {
						// Do nothing
					} else if strings.Contains(message.Type, "MsgRecvPacket") {
						amountDec, _ := decimal.NewFromString(content.Params.Token.Amount)
						amountDec = amountDec.Div(cryptoOrgDenominator)
						if content.Params.MaybeMsgTransfer.Receiver == addr {
							t := tx.TX{Timestamp: txTime, ID: id, Source: source, FiscalCateg: tx.Deposits, Wallet: wallet, Method: apiMethod, Note: note,
								Items: map[string]tx.Values{
									"To": tx.Values{tx.Value{Code: "CRO", Amount: amountDec}},
								},
							}
							txs = append(txs, t)
						} else {
							t := tx.TX{Timestamp: txTime, ID: id, Source: source, FiscalCateg: tx.Withdrawals, Wallet: wallet, Method: apiMethod, Note: note,
								Items: map[string]tx.Values{
									"From": tx.Values{tx.Value{Code: "CRO", Amount: amountDec}},
								},
							}
							if len(jsonTx.Messages) == 1 {
								t.Items["Fee"] = tx.Values{tx.Value{Code: "CRO", Amount: sumAmounts(jsonTx.Fee)}}
							} else {
								createFeeTx = true
							}
							txs = append(txs, t)
						}
					} else if strings.Contains(message.Type, "MsgIssueDenom") {
						// Do nothing
					} else if strings.Contains(message.Type, "MsgRevoke") {
						// Do nothing
					} else {
						unknowns = append(unknowns, message.Type)
					}
				}
				if createFeeTx {
					addFeeTx(&txs, jsonTx, txTime, source, wallet)
				}
			}
		}
	}
	return
}

func addAutoClaimedRewardsTx(txs *tx.TXs, content ContentTX, txTime time.Time, id string, source string, wallet string, note string) {
	autoClaimedRewardsDec, _ := decimal.NewFromString(content.AutoClaimedRewards.Amount)
	autoClaimedRewardsDec = autoClaimedRewardsDec.Div(cryptoOrgDenominator)

	if autoClaimedRewardsDec.GreaterThan(decimal.Zero) {
		tx := tx.TX{Timestamp: txTime, ID: id + "-autoclaims", Source: source, FiscalCateg: tx.Minings, Wallet: wallet, Method: apiMethod, Note: note,
			Items: map[string]tx.Values{
				"To": tx.Values{tx.Value{Code: "CRO", Amount: autoClaimedRewardsDec}},
			}}
		*txs = append(*txs, tx)
	}
}

func addFeeTx(txs *tx.TXs, jsonTx CryptoOrgTransaction, txTime time.Time, source string, wallet string) {
	fees, err := decimal.NewFromString(jsonTx.Fee[0].Amount)
	fees = fees.Div(cryptoOrgDenominator)
	if err == nil {
		tx := tx.TX{Timestamp: txTime, ID: jsonTx.Hash + "-fees", Source: source, FiscalCateg: tx.Fees, Wallet: wallet, Method: apiMethod, Note: jsonTx.Hash, Items: map[string]tx.Values{"Fee": tx.Values{tx.Value{Code: "CRO", Amount: fees}}}}
		*txs = append(*txs, tx)
	}
}

func sumAmounts(amounts interface{}) (total decimal.Decimal) {
	content := Amount{Amount: "", Denom: ""}
	switch reflect.TypeOf(amounts).String() {
	case "[]interface {}":
		amounts := amounts.([]interface{})
		amount := amounts[0].(map[string]interface{})
		content = Amount{Amount: amount["amount"].(string), Denom: amount["denom"].(string)}
	case "map[string]interface {}":
		amount := amounts.(map[string]interface{})
		content = Amount{Amount: amount["amount"].(string), Denom: amount["denom"].(string)}
	default:
		amounts := amounts.([]Amount)
		content = Amount{Amount: amounts[0].Amount, Denom: amounts[0].Denom}
	}
	val, err := decimal.NewFromString(content.Amount)
	if err != nil {
		d := decimal.New(0, 0)
		return d
	}
	return val.Div(cryptoOrgDenominator)
}
