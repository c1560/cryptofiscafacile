package json

import (
	"errors"
	// "log"
	"math/big"

	"gitlab.com/c1560/CryptoFiscaFacile/wasm/tx"
)

const (
	apiMethod = "API"
)

// Source : JSON Source
type Source interface {
	Digest([]byte) (txs tx.TXs, source string, wallet string, unknowns []string, addresses []string, err error)
}

// Digest : Digest a JSON file
func Digest(jsonData []byte) (txs tx.TXs, source string, wallet string, unknowns []string, addresses []string, err error) {
	srcs := []Source{
		algo,
		binanceAPI,
		bittrexAPI,
		blockstream,
		bscscan,
		cdcExchangeAPI,
		cdcExchangeExportJS,
		cronos,
		cryptoOrg,
		elrond,
		etherscan,
		gnosis,
		hitbtc,
		kraken,
		polygon,
		solscan,
	}
	for _, src := range srcs {
		txs, source, wallet, unknowns, addresses, err = src.Digest(jsonData)
		if err == nil {
			return
		}
	}
	err = errors.New("JSON file not identified")
	return
}

// BigInt is a wrapper over big.Int to implement only unmarshalText
// for json decoding.
type bigInt big.Int

// UnmarshalText implements the encoding.TextUnmarshaler interface.
func (b *bigInt) UnmarshalText(text []byte) (err error) {
	var bi = new(big.Int)
	err = bi.UnmarshalText(text)
	if err != nil {
		return
	}

	*b = bigInt(*bi)
	return nil
}

// MarshalText implements the encoding.TextMarshaler
func (b *bigInt) MarshalText() (text []byte, err error) {
	return []byte(b.Int().String()), nil
}

// Int returns b's *big.Int form
func (b *bigInt) Int() *big.Int {
	return (*big.Int)(b)
}
