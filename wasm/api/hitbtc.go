package api

import (
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"strconv"
	"time"

	"github.com/davecgh/go-spew/spew"
	"nhooyr.io/websocket"
	"nhooyr.io/websocket/wsjson"
)

type HitBTCJSON struct {
	Transactions map[int]interface{} `json:"transactions"`
	Trades       map[int]interface{} `json:"trades"`
}

type HitBTCAPI struct {
	debug     bool
	indent    string
	baseURL   string
	apiKey    string
	secretKey string
	wsID      int
	json      HitBTCJSON
}

var HitBTC = HitBTCAPI{
	baseURL: "wss://api.hitbtc.com/api/3/ws",
}

func (hb HitBTCAPI) GetName() string {
	return "HitBTC"
}

func (hb HitBTCAPI) GetURL() string {
	return "https://hitbtc.com/settings/api-keys"
}

func (hb HitBTCAPI) GetPlateform() bool {
	return true
}

func (hb HitBTCAPI) NeedKey() bool {
	return true
}

func (hb HitBTCAPI) GetJSONName() string {
	return "HitBTC_API.json"
}

func (hb HitBTCAPI) WithChannels() bool {
	return true
}

type HBWSRequest struct {
	Method  string                 `json:"method"`
	Channel string                 `json:"ch"`
	Params  map[string]interface{} `json:"params"`
	ID      int                    `json:"id"`
}

func (req *HBWSRequest) sign(secretKey string) {
	msg := strconv.FormatInt(req.Params["timestamp"].(int64), 10)
	if req.Params["window"].(int64) > 0 {
		msg += strconv.FormatInt(req.Params["window"].(int64), 10)
	}
	mac := hmac.New(sha256.New, []byte(secretKey))
	mac.Write([]byte(msg))
	req.Params["signature"] = hex.EncodeToString(mac.Sum(nil))
}

type HBWSNotification struct {
	Method  string      `json:"method"`
	Params  interface{} `json:"params"`
	JsonRPC string      `json:"jsonrpc"`
}

type HBWSError struct {
	Code        int    `json:"code"`
	Message     string `json:"message"`
	Description string `json:"description"`
}

type HBWSResponse struct {
	Result  interface{} `json:"result"`
	Error   HBWSError   `json:"error"`
	ID      int         `json:"id"`
	JsonRPC string      `json:"jsonrpc"`
}

func (hb HitBTCAPI) GetEmptyJson() JSONFile {
	emptyJson := HitBTCJSON{Transactions: make(map[int]interface{}), Trades: make(map[int]interface{})}
	emptyJsonFile := JSONFile{Name: "empty-hitbtc.json"}
	emptyJsonFile.Data, _ = json.MarshalIndent(emptyJson, "", hb.indent)

	return emptyJsonFile
}

func (hb HitBTCAPI) GetJSONs(conf map[string]interface{}) (jsons chan JSONFile, progressBar chan int, errorChan chan error, err error) {
	if val, ok := conf["arg1"]; ok {
		hb.apiKey = val.(string)
	} else {
		err = errors.New("missing apiKey")
		return
	}
	if val, ok := conf["arg2"]; ok {
		hb.secretKey = val.(string)
	} else {
		err = errors.New("missing secretKey")
		return
	}
	if val, ok := conf["debug"]; ok {
		hb.debug = val.(bool)
	}
	if val, ok := conf["indent"]; ok {
		hb.indent = val.(string)
	}
	if val, ok := conf["json"]; ok {
		json.Unmarshal(val.([]byte), &hb.json)
	}
	if hb.json.Transactions == nil {
		hb.json.Transactions = make(map[int]interface{})
	}
	if hb.json.Trades == nil {
		hb.json.Trades = make(map[int]interface{})
	}
	hbCtx, cancel := context.WithTimeout(context.Background(), time.Hour)
	wsWallet, _, err := websocket.Dial(hbCtx, hb.baseURL+"/wallet", nil)
	if err != nil {
		if hb.debug {
			spew.Dump("Dial:wallet", err)
		}
		cancel()
		return
	}
	hb.wsID++
	now := time.Now().UnixMilli()
	win := int64(60000)
	req := HBWSRequest{
		Method: "login",
		Params: map[string]interface{}{
			"type":      "HS256",
			"api_key":   hb.apiKey,
			"timestamp": now,
			"window":    win,
		},
		ID: hb.wsID,
	}
	req.sign(hb.secretKey)
	err = wsjson.Write(hbCtx, wsWallet, req)
	if err != nil {
		if hb.debug {
			spew.Dump("Write:wallet:login", err)
		}
		wsWallet.Close(websocket.StatusNormalClosure, "")
		cancel()
		return
	}
	var wsResp HBWSResponse
	err = wsjson.Read(hbCtx, wsWallet, &wsResp)
	if err != nil {
		if hb.debug {
			spew.Dump("Read:wallet:login", err)
		}
		wsWallet.Close(websocket.StatusNormalClosure, "")
		cancel()
		return
	}
	if wsResp.Error.Code > 0 {
		if hb.debug {
			spew.Dump("Read:trading:wallet", wsResp.Error)
		}
		err = errors.New(wsResp.Error.Message)
		wsWallet.Close(websocket.StatusNormalClosure, "")
		cancel()
		return
	}
	wsTrading, _, err := websocket.Dial(hbCtx, hb.baseURL+"/trading", nil)
	if err != nil {
		if hb.debug {
			spew.Dump("Dial:trading", err)
		}
		cancel()
		return
	}
	hb.wsID++
	now = time.Now().UnixMilli()
	req = HBWSRequest{
		Method: "login",
		Params: map[string]interface{}{
			"type":      "HS256",
			"api_key":   hb.apiKey,
			"timestamp": now,
			"window":    win,
		},
		ID: hb.wsID,
	}
	req.sign(hb.secretKey)
	err = wsjson.Write(hbCtx, wsTrading, req)
	if err != nil {
		if hb.debug {
			spew.Dump("Write:trading:login", err)
		}
		wsTrading.Close(websocket.StatusNormalClosure, "")
		cancel()
		return
	}
	err = wsjson.Read(hbCtx, wsTrading, &wsResp)
	if err != nil {
		if hb.debug {
			spew.Dump("Read:trading:login", err)
		}
		wsTrading.Close(websocket.StatusNormalClosure, "")
		cancel()
		return
	}
	if wsResp.Error.Code > 0 {
		if hb.debug {
			spew.Dump("Read:trading:login", wsResp.Error)
		}
		err = errors.New(wsResp.Error.Message)
		wsTrading.Close(websocket.StatusNormalClosure, "")
		cancel()
		return
	}
	jsons = make(chan JSONFile, 10)
	progressBar = make(chan int)
	errorChan = make(chan error)
	var j JSONFile
	j.Name = hb.GetJSONName()
	go func() {
		defer cancel()
		defer wsWallet.Close(websocket.StatusNormalClosure, "")
		defer wsTrading.Close(websocket.StatusNormalClosure, "")
		offset := 0
		limit := 1000
		progress := 1
		progressBar <- progress
		for {
			time.Sleep(100 * time.Millisecond)
			hb.wsID++
			err = wsjson.Write(hbCtx, wsWallet, HBWSRequest{Method: "get_transactions", Params: map[string]interface{}{
				"offset": offset,
				"limit":  limit,
			}, ID: hb.wsID})
			if err != nil {
				if hb.debug {
					spew.Dump("Write:get_transactions", err)
				}
				break
			}
			err = wsjson.Read(hbCtx, wsWallet, &wsResp)
			if err != nil {
				if hb.debug {
					spew.Dump("Read:get_transactions", err)
				}
				break
			}
			if wsResp.Error.Code > 0 {
				if hb.debug {
					spew.Dump("Read:get_transactions", wsResp.Error)
				}
				break
			}
			res := wsResp.Result.([]interface{})
			for _, trans := range res {
				tID := int(trans.(map[string]interface{})["id"].(float64))
				hb.json.Transactions[tID] = trans
			}
			if len(res) > 0 {
				j.Data, err = json.MarshalIndent(hb.json, "", hb.indent)
				if err == nil {
					jsons <- j
				}
			}
			if len(res) < limit {
				progress = 50
				progressBar <- progress
				break
			}
			offset += limit
			progress += 5
			if progress > 45 {
				progress = 25
			}
			progressBar <- progress
		}
		time.Sleep(100 * time.Millisecond)
		hb.wsID++
		err = wsjson.Write(hbCtx, wsTrading, HBWSRequest{Method: "spot_subscribe", ID: hb.wsID})
		if err != nil {
			if hb.debug {
				spew.Dump("Write:spot_subscribe", err)
			}
			return
		}
		err = wsjson.Read(hbCtx, wsTrading, &wsResp)
		if err != nil {
			if hb.debug {
				spew.Dump("Read:spot_subscribe", err)
			}
			return
		}
		if wsResp.Error.Code > 0 {
			if hb.debug {
				spew.Dump("Read:spot_subscribe", wsResp.Error)
			}
			return
		}
		var wsNotif HBWSNotification
		for {
			err = wsjson.Read(hbCtx, wsTrading, &wsNotif)
			if err != nil {
				if hb.debug {
					spew.Dump("Read:notif", err)
				}
				break
			}
			if hb.debug {
				spew.Dump(wsNotif)
			}
			if wsNotif.Method == "spot_order" {
				tID := wsNotif.Params.(map[string]interface{})["id"].(int)
				hb.json.Trades[tID] = wsNotif.Params
				j.Data, err = json.MarshalIndent(hb.json, "", hb.indent)
				if err == nil {
					jsons <- j
				}
			} else if wsNotif.Method == "spot_orders" {
				trades := wsNotif.Params.([]interface{})
				if len(trades) > 0 {
					for _, tra := range trades {
						tID := tra.(map[string]interface{})["id"].(int)
						hb.json.Trades[tID] = tra
					}
					j.Data, err = json.MarshalIndent(hb.json, "", hb.indent)
					if err == nil {
						jsons <- j
					}
				} else {
					progressBar <- 100
					break
				}
			}
			progress += 5
			if progress > 95 {
				progress = 75
			}
			progressBar <- progress
		}
		time.Sleep(100 * time.Millisecond)
		hb.wsID++
		err = wsjson.Write(hbCtx, wsTrading, HBWSRequest{Method: "spot_unsubscribe", ID: hb.wsID})
		if err != nil {
			if hb.debug {
				spew.Dump("Write:spot_unsubscribe", err)
			}
			return
		}
		err = wsjson.Read(hbCtx, wsTrading, &wsResp)
		if err != nil {
			if hb.debug {
				spew.Dump("Read:spot_unsubscribe", err)
			}
			return
		}
		if wsResp.Error.Code > 0 {
			if hb.debug {
				spew.Dump("Read:spot_unsubscribe", wsResp.Error)
			}
		}
	}()
	return
}

func (hb HitBTCAPI) GetJsons(map[string]interface{}) error {
	return nil
}
