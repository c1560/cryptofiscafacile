package api

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"strings"
	"time"

	resty "github.com/go-resty/resty/v2"
)

type ElrondResult struct {
	Status  string      `json:"status"`
	Message string      `json:"message"`
	Result  interface{} `json:"result"`
}

type ElrondJSON struct {
	Addresses    map[string]bool        `json:"egld_addrs"`
	Transactions map[string]interface{} `json:"transactions"`
}

type ElrondAPI struct {
	debug       bool
	indent      string
	client      *resty.Client
	address     string
	baseURL     string
	lastReqTime time.Time
	json        ElrondJSON
}

var Elrond = ElrondAPI{
	baseURL: "https://api.elrond.com/accounts/",
}

func (api ElrondAPI) GetName() string {
	return "Elrond"
}

func (api ElrondAPI) GetURL() string {
	return "https://explorer.elrond.com"
	//https://docs.elrond.com/sdk-and-tools/rest-api/rest-api/
	//https://api.elrond.com/
	//api.elrond.com (mainnet): 2 requests / IP / second
}

func (api ElrondAPI) GetPlateform() bool {
	return false
}

func (api ElrondAPI) NeedKey() bool {
	return false
}

func (api ElrondAPI) GetJSONName() string {
	return "Elrond.json"
}

func (api ElrondAPI) WithChannels() bool {
	return true
}

func (api ElrondAPI) GetEmptyJson() JSONFile {
	emptyJson := ElrondJSON{Addresses: make(map[string]bool)}
	emptyJsonFile := JSONFile{Name: "empty-elrond.json"}
	emptyJsonFile.Data, _ = json.MarshalIndent(emptyJson, "", api.indent)

	return emptyJsonFile
}

func (api ElrondAPI) GetJSONs(conf map[string]interface{}) (jsons chan JSONFile, progress chan int, errorChan chan error, err error) {
	if val, ok := conf["arg1"]; ok && val != "" {
		api.address = strings.ToLower(val.(string))
	} else {
		err = errors.New("missing EGLD Address")
		return
	}
	if val, ok := conf["debug"]; ok {
		api.debug = val.(bool)
	}
	if val, ok := conf["indent"]; ok {
		api.indent = val.(string)
	}
	if val, ok := conf["json"]; ok {
		json.Unmarshal(val.([]byte), &api.json)
	}
	if api.json.Addresses == nil {
		api.json.Addresses = make(map[string]bool)
	}
	if api.json.Transactions == nil {
		api.json.Transactions = make(map[string]interface{})
	}
	if api.client == nil {
		api.client = resty.New()
		api.client.SetTransport(http.DefaultTransport)
		api.client.SetDebug(api.debug)
		api.client.SetDebugBodyLimit(200)
	}
	api.client.SetRetryCount(3)
	api.client.SetRetryWaitTime(time.Second)
	jsons = make(chan JSONFile, 10)
	progress = make(chan int)
	errorChan = make(chan error)
	var j JSONFile
	j.Name = api.GetJSONName()
	go func() {
		progress <- 1
		txsCount, err := api.getTXsCountByAddress(api.address)
		if err == nil {
			progress <- 5
			for i := 0; i < int(txsCount/50)+1; i++ {
				txs, err := api.getTXsByAddress(api.address, i*50)
				// TODO : add a way to return the error to main app
				if err == nil {
					for _, t := range txs {
						api.json.Transactions[t.(map[string]interface{})["txHash"].(string)] = t
					}
					api.json.Addresses[api.address] = true
					j.Data, err = json.MarshalIndent(api.json, "", api.indent)
					if err == nil {
						jsons <- j
					}
					progress <- 5 + (i*94)/(int(txsCount/50)+1)
				}
			}
		}
		progress <- 100
	}()
	return
}

func (api ElrondAPI) GetJsons(map[string]interface{}) error {
	return nil
}

func (api *ElrondAPI) getTXsByAddress(addr string, from int) (txs []interface{}, err error) {
	time.Sleep(100*time.Millisecond - time.Since(api.lastReqTime))
	params := map[string]string{
		"size":           "50",
		"from":           strconv.Itoa(from),
		"withScResults":  "true",
		"withOperations": "true",
		"withLogs":       "false",
		"withScamInfo":   "false",
	}
	headers := map[string]string{
		"Accept": "application/json",
	}
	api.lastReqTime = time.Now()
	resp, err := api.client.R().
		SetQueryParams(params).
		SetHeaders(headers).
		SetResult(&txs).
		Get(api.baseURL + addr + "/transactions")
	if err != nil {
		return
	}
	if !resp.IsSuccess() {
		err = errors.New("Error Requesting TX for " + addr + " " + strconv.Itoa(resp.StatusCode()))
		return
	}
	return
}

func (api *ElrondAPI) getTXsCountByAddress(addr string) (count int, err error) {
	time.Sleep(100*time.Millisecond - time.Since(api.lastReqTime))
	headers := map[string]string{
		"Accept": "*/*",
	}
	api.lastReqTime = time.Now()
	resp, err := api.client.R().
		SetHeaders(headers).
		Get(api.baseURL + addr + "/transactions/count")
	if err != nil {
		return
	}
	if !resp.IsSuccess() {
		err = errors.New("Error Requesting TX count for " + addr + " " + strconv.Itoa(resp.StatusCode()))
		return
	}
	c, err := strconv.ParseInt(string(resp.Body()), 10, 32)
	if err == nil {
		count = int(c)
	}
	return
}
