package api

import (
	"testing"
)

func TestHBWSRequest_sign(t *testing.T) {
	type args struct {
		secretKey string
	}
	tests := []struct {
		name string
		req  *HBWSRequest
		args args
		want string
	}{
		{
			name: "Offical Doc Example",
			req: &HBWSRequest{
				Params: map[string]interface{}{
					"timestamp": int64(1626861109494),
					"window":    int64(10000),
				},
			},
			args: args{
				secretKey: "2deb570ab58fd553a4ed3ee249fd2d51",
			},
			want: "f552d9f9c74a78a8e7804430f2bfc7afb19bf21f7c5723fd602a08484ab77ad4", // calculated from Python example
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.req.sign(tt.args.secretKey)
			if tt.req.Params["signature"].(string) != tt.want {
				t.Errorf("HitBTCAPI.sign() = %v, want %v", tt.req.Params["signature"], tt.want)
			}
		})
	}
}
