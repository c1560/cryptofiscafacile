package api

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"strconv"
	"time"

	resty "github.com/go-resty/resty/v2"
)

type CryptoOrgError struct {
	Result     interface{}   `json:"result"`
	Pagination CryptoOrgPage `json:"pagination"`
}

type CryptoOrgPage struct {
	TotalRec    int `json:"total_record"`
	TotalPage   int `json:"total_page"`
	CurrentPage int `json:"current_page"`
	Limit       int `json:"limit"`
}

type CryptoOrgResult struct {
	Error      string        `json:"error,omitempty"`
	Result     interface{}   `json:"result"`
	Pagination CryptoOrgPage `json:"pagination"`
}

type CryptoOrgJSON struct {
	Addresses map[string]map[string]interface{} `json:"cryptoorg_addresses"`
}

type CryptoOrgAPI struct {
	debug       bool
	indent      string
	client      *resty.Client
	baseScheme  string
	address     string
	baseURL     string
	apiKey      string
	lastReqTime time.Time
	tier        string
	json        CryptoOrgJSON
}

var Cryptoorg = CryptoOrgAPI{
	baseScheme: "https://",
	baseURL:    "crypto.org",
}

func (cry CryptoOrgAPI) GetName() string {
	return "Crypto.org"
}

func (cry CryptoOrgAPI) GetURL() string {
	return "https://crypto.org/explorer/"
}

func (cry CryptoOrgAPI) GetPlateform() bool {
	return false
}

func (cry CryptoOrgAPI) NeedKey() bool {
	return false
}

func (cry CryptoOrgAPI) GetJSONName() string {
	return "Cryptoorg.json"
}

func (cs CryptoOrgAPI) WithChannels() bool {
	return true
}

func (cry CryptoOrgAPI) GetEmptyJson() JSONFile {
	emptyJson := CryptoOrgJSON{Addresses: make(map[string]map[string]interface{})}
	emptyJsonFile := JSONFile{Name: "empty-cryptoorg.json"}
	emptyJsonFile.Data, _ = json.MarshalIndent(emptyJson, "", cry.indent)

	return emptyJsonFile
}

func (cry CryptoOrgAPI) GetJSONs(conf map[string]interface{}) (jsons chan JSONFile, progress chan int, errorChan chan error, err error) {
	if val, ok := conf["arg1"]; ok {
		cry.address = val.(string)
	}
	if cry.address == "" {
		err = errors.New("missing CRO Address")
		return
	}
	if val, ok := conf["proxy"]; ok {
		cry.baseScheme = val.(string)
	}
	if val, ok := conf["debug"]; ok {
		cry.debug = val.(bool)
	}
	if val, ok := conf["indent"]; ok {
		cry.indent = val.(string)
	}
	if val, ok := conf["json"]; ok {
		json.Unmarshal(val.([]byte), &cry.json)
	}
	if cry.json.Addresses == nil {
		cry.json.Addresses = make(map[string]map[string]interface{})
	}
	if cry.client == nil {
		cry.client = resty.New()
		cry.client.SetTransport(http.DefaultTransport)
		cry.client.SetDebug(cry.debug)
		cry.client.SetDebugBodyLimit(200)
	}
	cry.client.SetRetryCount(3)
	cry.client.SetRetryWaitTime(time.Second)
	jsons = make(chan JSONFile, 10)
	progress = make(chan int)
	errorChan = make(chan error)
	var j JSONFile
	j.Name = cry.GetJSONName()
	go func() {
		progress <- 1
		totalPage, err := cry.getTXsCountByAddress(cry.address)
		if err == nil {
			progress <- 5
			for i := 1; i < int(totalPage)+1; i++ {
				txs, err := cry.getTXsByAddress(cry.address, i)
				// TODO : add a way to return the error to main app
				if err == nil {
					cry.json.Addresses[cry.address] = make(map[string]interface{})
					jsonTransactions := cry.json.Addresses[cry.address]
					for _, t := range txs {
						jsonTransactions[t.(map[string]interface{})["hash"].(string)] = t
					}
					j.Data, err = json.MarshalIndent(cry.json, "", cry.indent)

					if err == nil {
						jsons <- j
					}
					progress <- 5 + (i*94)/(int(totalPage)+1)
				}
			}
		}
		progress <- 100
	}()
	return
}

func (cry *CryptoOrgAPI) getTXsByAddress(addr string, page int) (txs []interface{}, err error) {
	params := map[string]string{
		"pagination": "offset",
		"page":       strconv.Itoa(page),
		"limit":      "1000",
		"order":      "height.asc",
	}
	headers := map[string]string{
		"Accept": "application/json",
	}
	var res CryptoOrgResult
	cry.lastReqTime = time.Now()
	// Go for proxying requests!
	var cryErr CryptoOrgError
	u, _ := url.Parse(cry.baseURL + "/explorer/api/v1/accounts/" + addr + "/transactions")
	base, err := url.Parse(cry.baseScheme)
	if err != nil {
		return
	}
	resp, err := cry.client.R().
		SetQueryParams(params).
		SetHeaders(headers).
		SetError(&cryErr).
		SetResult(&res).
		Get(base.ResolveReference(u).String())
	if err != nil {
		return
	}
	if !resp.IsSuccess() {
		err = errors.New("Error Requesting Account TX for " + addr + " " + strconv.Itoa(resp.StatusCode()))
		return
	}
	if res.Error == "Record not found" {
		err = errors.New("Error Requesting Account TX for " + addr)
		return
	}
	txs = res.Result.([]interface{})
	return
}

func (cs CryptoOrgAPI) GetJsons(map[string]interface{}) error {
	return nil
}

func (cry *CryptoOrgAPI) getTXsCountByAddress(addr string) (totalPage int, err error) {
	params := map[string]string{
		"limit": "1000",
	}
	headers := map[string]string{
		"Accept": "application/json",
	}
	var res CryptoOrgResult
	// Go for proxying requests!
	var cryErr CryptoOrgError
	u, _ := url.Parse(cry.baseURL + "/explorer/api/v1/accounts/" + addr + "/transactions")
	base, err := url.Parse(cry.baseScheme)
	if err != nil {
		return
	}
	resp, err := cry.client.R().
		SetQueryParams(params).
		SetHeaders(headers).
		SetError(&cryErr).
		SetResult(&res).
		Get(base.ResolveReference(u).String())
	if err != nil {
		return
	}
	if !resp.IsSuccess() {
		err = errors.New("Error Requesting TX count for " + addr + " " + strconv.Itoa(resp.StatusCode()))
		return
	}
	totalPage = res.Pagination.TotalPage
	return
}
