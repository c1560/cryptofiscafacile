package api

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"time"

	resty "github.com/go-resty/resty/v2"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/utils/slices"
)

type SolscanJSON struct {
	// Transactions grouped by addresses
	Addresses map[string]map[string]interface{} `json:"solscan_addresses"`
}

type SolscanAPI struct {
	debug       bool
	indent      string
	client      *resty.Client
	address     string
	baseURL     string
	lastReqTime time.Time
	json        SolscanJSON
}

const solscanErrorNamespace = "api.solscan."

var Solscan = SolscanAPI{
	baseURL: "https://public-api.solscan.io/",
}

func (api SolscanAPI) GetName() string {
	return "Solana"
}

func (api SolscanAPI) GetURL() string {
	return "https://solscan.io/"
}

func (api SolscanAPI) GetPlateform() bool {
	return false
}

func (api SolscanAPI) NeedKey() bool {
	return false
}

func (api SolscanAPI) GetJSONName() string {
	return "Solscan.json"
}

func (api SolscanAPI) WithChannels() bool {
	return true
}

type solscanTransaction struct {
	TxHash             string                     `json:"txHash"`
	ParsedInstructions []solscanParsedInstruction `json:"parsedInstruction"`
}

type solscanParsedInstruction struct {
	ProgramId  string `json:"programId"`
	Type       string `json:"type"`
	Data       string `json:"data"`
	DataEncode string `json:"dataEncode"`
	Name       string `json:"source"`
	Params     struct {
		AssociatedAccount string `json:"associatedAccount"`
		Authority         string `json:"authority"`
		Destination       string `json:"destination"`
		Source            string `json:"source"`
	} `json:"params"`
	Extra struct {
		Authority        string `json:"authority"`
		Destination      string `json:"destination"`
		DestinationOwner string `json:"destinationOwner"`
		Source           string `json:"source"`
		SourceOwner      string `json:"sourceOwner"`
	} `json:"extra"`
}

func (api SolscanAPI) GetEmptyJson() JSONFile {
	emptyJson := SolscanJSON{Addresses: make(map[string]map[string]interface{})}
	emptyJsonFile := JSONFile{Name: "empty-solscan.json"}
	emptyJsonFile.Data, _ = json.MarshalIndent(emptyJson, "", api.indent)

	return emptyJsonFile
}

func (api SolscanAPI) GetJSONs(conf map[string]interface{}) (jsons chan JSONFile, progress chan int, errorChan chan error, err error) {
	if val, ok := conf["arg1"]; ok && val != "" {
		api.address = val.(string)
	} else {
		err = errors.New("missing Solana Address")
		return
	}
	if val, ok := conf["debug"]; ok {
		api.debug = val.(bool)
	}
	if val, ok := conf["indent"]; ok {
		api.indent = val.(string)
	}
	if val, ok := conf["json"]; ok {
		json.Unmarshal(val.([]byte), &api.json)
	}
	if api.json.Addresses == nil {
		api.json.Addresses = make(map[string]map[string]interface{})
	}
	if api.json.Addresses[api.address] == nil {
		api.json.Addresses[api.address] = make(map[string]interface{})
	}
	if api.client == nil {
		api.client = resty.New()
		api.client.SetTransport(http.DefaultTransport)
		api.client.SetDebug(api.debug)
		api.client.SetDebugBodyLimit(200)
	}
	api.client.SetRetryCount(3)
	api.client.SetRetryWaitTime(time.Second)
	jsons = make(chan JSONFile, 4)
	progress = make(chan int)
	errorChan = make(chan error)
	go func() {
		// Check account
		apiProgress := 0
		progress <- 0
		accountInfo, err := api.getAccount()
		if err != nil {
			errorChan <- err
			return
		}
		if accountInfo.Executable || accountInfo.Type != "system_account" {
			errorChan <- errors.New("api.solscan.only.system.account.allowed")
			return
		}
		apiProgress = 5
		progress <- apiProgress

		// Fetch transactions of main address
		// Fetch transactions list
		txsItems, err := api.getAllTransactions(api.address)
		if err != nil {
			errorChan <- err
			return
		}

		apiProgress = 10
		progress <- apiProgress

		// Fetch transactions detail
		var j JSONFile
		j.Name = api.GetJSONName()
		txDetailProgress := 0
		associatedAccounts := make([]string, 0)
		for i := len(txsItems) - 1; i >= 0; i-- {
			txItem := txsItems[i]

			if _, fetched := api.json.Addresses[api.address][txItem.(map[string]interface{})["txHash"].(string)]; !fetched {
				var detailedTransaction = txItem
				if txItem.(map[string]interface{})["status"] == "Success" {
					detailedTransaction, err = api.getTransaction(txItem.(map[string]interface{})["txHash"].(string))
					if err != nil {
						errorChan <- err
						return
					}
				}

				api.json.Addresses[api.address][txItem.(map[string]interface{})["txHash"].(string)] = detailedTransaction
			}

			bytes, _ := json.MarshalIndent(api.json.Addresses[api.address][txItem.(map[string]interface{})["txHash"].(string)], "", api.indent)
			var tx solscanTransaction
			json.Unmarshal(bytes, &tx)
			associatedAccounts = append(associatedAccounts, tx.getAssociatedAccountsWithoutAuthority(api.address)...)

			if i%10 == 0 || i == len(txsItems)-1 {
				j.Data, err = json.MarshalIndent(api.json, "", api.indent)
				if err != nil {
					errorChan <- err
					return
				}
				jsons <- j
			}

			if len(txsItems) > 1 {
				txDetailProgress = 80 - (i * 80 / (len(txsItems) - 1))
				progress <- apiProgress + txDetailProgress
			}
		}

		// fetch transaction of associated accounts without authority
		for _, associatedAccount := range associatedAccounts {
			txsItems, err := api.getAllTransactions(associatedAccount)
			if err != nil {
				errorChan <- err
				return
			}

			for i := len(txsItems) - 1; i >= 0; i-- {
				txItem := txsItems[i]
				_, fetched := api.json.Addresses[api.address][txItem.(map[string]interface{})["txHash"].(string)]

				if !fetched {
					var detailedTransaction = txItem
					if txItem.(map[string]interface{})["status"] == "Success" {
						detailedTransaction, err = api.getTransaction(txItem.(map[string]interface{})["txHash"].(string))
						if err != nil {
							errorChan <- err
							return
						}
					}

					api.json.Addresses[api.address][txItem.(map[string]interface{})["txHash"].(string)] = detailedTransaction
				}

				if !fetched || i%10 == 0 || i == len(txsItems)-1 {
					j.Data, err = json.MarshalIndent(api.json, "", api.indent)
					if err != nil {
						errorChan <- err
						return
					}
					jsons <- j
				}
			}
		}

		progress <- 100
	}()
	return
}

func (tx solscanTransaction) getAssociatedAccountsWithoutAuthority(address string) (associatedAccounts []string) {
	accounts := make(map[string]struct{})
	for _, inst := range tx.ParsedInstructions {
		switch inst.Type {
		case "createAssociatedAccount":
			if inst.Params.AssociatedAccount != "" && inst.Params.Authority != address {
				accounts[inst.Params.AssociatedAccount] = struct{}{}
			}
		case "spl-transfer":
			if inst.Params.Authority != address {
				if _, exists := accounts[inst.Params.Source]; inst.Extra.SourceOwner == address && exists {
					if !slices.ContainsString(associatedAccounts, inst.Params.Source) {
						associatedAccounts = append(associatedAccounts, inst.Params.Source)
					}
				}
				if _, exists := accounts[inst.Params.Destination]; inst.Extra.DestinationOwner == address && exists {
					if !slices.ContainsString(associatedAccounts, inst.Params.Destination) {
						associatedAccounts = append(associatedAccounts, inst.Params.Destination)
					}
				}
			}
		}
	}
	return
}

type solscanAccountInfo struct {
	Executable bool   `json:"executable"`
	Type       string `json:"type"`
}

func (api SolscanAPI) getAccount() (accountInfo solscanAccountInfo, err error) {
	api.waitRateLimit()
	resource := "account/" + api.address
	headers := make(map[string]string)
	headers["Accept"] = "application/json"
	resp, err := api.client.R().
		SetHeaders(headers).
		SetResult(&accountInfo).
		Get(api.baseURL + resource)
	if err != nil {
		return
	}
	if !resp.IsSuccess() {
		body := string(resp.Body())
		if resp.StatusCode() == 400 {
			err = errors.New(solscanErrorNamespace + "invalid.address")
		} else {
			err = errors.New(body + " [" + strconv.Itoa(resp.StatusCode()) + "]")
		}
	}
	return
}

func (api SolscanAPI) getAllTransactions(address string) (txsItems []interface{}, err error) {
	txHash := ""
	for {
		var items []interface{}
		items, err = api.getTransactions(address, txHash)
		if err != nil {
			return
		}

		txsItems = append(txsItems, items...)
		if len(items) < 50 || items[49].(map[string]interface{})["txHash"] == txHash {
			break
		}
		txHash = items[49].(map[string]interface{})["txHash"].(string)
	}
	return
}

func (api SolscanAPI) getTransactions(address string, beforeHash string) (items []interface{}, err error) {
	api.waitRateLimit()
	resource := "account/transactions"
	var queryString = "account=" + address + "&limit=50"
	if beforeHash != "" {
		queryString = queryString + "&beforeHash=" + beforeHash
	}
	headers := make(map[string]string)
	headers["Accept"] = "application/json"
	resp, err := api.client.R().
		SetHeaders(headers).
		SetQueryString(queryString).
		SetResult(&items).
		Get(api.baseURL + resource)
	if err != nil {
		return
	}
	if !resp.IsSuccess() {
		body := string(resp.Body())
		err = errors.New(body + " [" + strconv.Itoa(resp.StatusCode()) + "]")
	}
	return
}

func (api SolscanAPI) getTransaction(signature string) (transaction interface{}, err error) {

	api.waitRateLimit()
	resource := "transaction/" + signature
	headers := make(map[string]string)
	headers["Accept"] = "application/json"
	resp, err := api.client.R().
		SetHeaders(headers).
		SetResult(&transaction).
		Get(api.baseURL + resource)
	if err != nil {
		return
	}
	if !resp.IsSuccess() {
		body := string(resp.Body())
		err = errors.New(body + " [" + strconv.Itoa(resp.StatusCode()) + "]")
	}
	return
}

func (api SolscanAPI) GetJsons(map[string]interface{}) error {
	return nil
}

func (api SolscanAPI) waitRateLimit() {
	// 150 call every 30 seconds (5 calls/second)
	wait := 200 * time.Millisecond
	time.Sleep(wait - time.Since(api.lastReqTime))
}
