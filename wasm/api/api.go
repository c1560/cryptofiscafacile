package api

type JSONFile struct {
	Name string
	Data []byte
}

type API interface {
	GetName() string
	GetURL() string
	GetPlateform() bool
	GetEmptyJson() JSONFile
	GetJSONs(map[string]interface{}) (chan JSONFile, chan int, chan error, error)
	GetJsons(map[string]interface{}) error
	NeedKey() bool
	GetJSONName() string
	WithChannels() bool
}

func GetAlls() []API {
	// List only the one we want to be exposed
	return []API{Algo, Blockstream, Bscscan, CdCEx, Cronos, Cryptoorg, Elrond, Etherscan, Gnosis, HitBTC, Polygon, Solscan}
}
