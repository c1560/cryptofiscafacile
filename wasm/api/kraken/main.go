package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.com/c1560/CryptoFiscaFacile/wasm/api"
)

func main() {
	pAPIKey := flag.String("key", "", "API Key")
	pSecretKey := flag.String("secret", "", "Secret Key")
	pOut := flag.String("out", "", "Output Directory")
	pDebug := flag.Bool("debug", false, "Debug")
	flag.Parse()
	conf := make(map[string]interface{})
	conf["arg1"] = *pAPIKey
	conf["arg2"] = *pSecretKey
	conf["indent"] = "  "
	conf["debug"] = *pDebug
	fileData, err := os.ReadFile(*pOut + api.Kraken.GetJSONName())
	if err == nil {
		conf["json"] = fileData
	}
	jsons, progress, err := api.Kraken.GetJSONs(conf)
	if err != nil {
		log.Fatalln(err.Error())
	}
	for {
		select {
		case p := <-progress:
			fmt.Print(p, ".")
			if p >= 100 {
				return
			}
		case j := <-jsons:
			err := os.WriteFile(*pOut+j.Name, j.Data, 0666)
			if err != nil {
				log.Println(err.Error())
			}
		}
	}
}
