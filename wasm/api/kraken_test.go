package api

import (
	"net/url"
	"testing"
)

// func TestKrakenAPI_sign(t *testing.T) {
// 	type args struct {
// 		body     url.Values
// 		resource string
// 	}
// 	tests := []struct {
// 		name string
// 		k    KrakenAPI
// 		args args
// 		want string
// 	}{
// 		{
// 			name: "Example from Kraken API Documentation",
// 			k: KrakenAPI{
// 				secretKey: "kQH5HW/8p1uGOVjbgWA7FunAmGO8lsSUXNsu3eow76sz84Q18fWxnyRzBHCd3pd5nE9qa99HAZtuZuj6F1huXg==",
// 			},
// 			args: args{
// 				body: url.Values{
// 					"pair":      []string{"XBTUSD"},
// 					"type":      []string{"buy"},
// 					"ordertype": []string{"limit"},
// 					"price":     []string{"37500"},
// 					"volume":    []string{"1.25"},
// 					"nonce":     []string{"1616492376594"},
// 				},
// 				resource: "/0/private/AddOrder",
// 			},
// 			want: "4/dpxb3iT4tp/ZCVEwSnEsLxx0bqyhLpdfOpc6fn7OR8+UClSV5n9E6aSS8MPtnRfp32bAb0nmbRn6H8ndwLUQ==",
// 		},
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			if got := tt.k.sign(tt.args.body, tt.args.resource); got != tt.want {
// 				t.Errorf("KrakenAPI.sign() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }

func TestKrakenAPI_sign(t *testing.T) {
	type args struct {
		body     url.Values
		resource string
	}
	tests := []struct {
		name    string
		k       KrakenAPI
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Example from Kraken API Documentation",
			k: KrakenAPI{
				secretKey: "kQH5HW/8p1uGOVjbgWA7FunAmGO8lsSUXNsu3eow76sz84Q18fWxnyRzBHCd3pd5nE9qa99HAZtuZuj6F1huXg==",
			},
			args: args{
				body: url.Values{
					"pair":      []string{"XBTUSD"},
					"type":      []string{"buy"},
					"ordertype": []string{"limit"},
					"price":     []string{"37500"},
					"volume":    []string{"1.25"},
					"nonce":     []string{"1616492376594"},
				},
				resource: "/0/private/AddOrder",
			},
			want:    "4/dpxb3iT4tp/ZCVEwSnEsLxx0bqyhLpdfOpc6fn7OR8+UClSV5n9E6aSS8MPtnRfp32bAb0nmbRn6H8ndwLUQ==",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.k.sign(tt.args.body, tt.args.resource)
			if (err != nil) != tt.wantErr {
				t.Errorf("KrakenAPI.sign() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("KrakenAPI.sign() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Unable to run because of cors errors
// func TestKrakenAPI_getAssets(t *testing.T) {
// 	tests := []struct {
// 		name    string
// 		k       KrakenAPI
// 		wantErr bool
// 	}{
// 		{
// 			name: "Example from Kraken API Documentation",
// 			k: KrakenAPI{
// 				client:  resty.New().SetTransport(http.DefaultTransport),
// 				baseURL: "https://api.kraken.com",
// 			},
// 			wantErr: false,
// 		},
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			_, err := tt.k.getPublic("Assets")
// 			if (err != nil) != tt.wantErr {
// 				t.Errorf("KrakenAPI.getAssets() error = %v, wantErr %v", err, tt.wantErr)
// 				return
// 			}
// 		})
// 	}
// }

// Unable to run because of cors errors
// func TestKrakenAPI_getClosedOrders(t *testing.T) {
// 	type args struct {
// 		endpoint string
// 		offset   int
// 	}
// 	tests := []struct {
// 		name    string
// 		k       KrakenAPI
// 		args    args
// 		wantErr bool
// 	}{
// 		{
// 			name: "Example from Kraken API Documentation",
// 			k: KrakenAPI{
// 				client:    resty.New().SetTransport(http.DefaultTransport),
// 				baseURL:   "https://api.kraken.com",
// 				secretKey: "kQH5HW/8p1uGOVjbgWA7FunAmGO8lsSUXNsu3eow76sz84Q18fWxnyRzBHCd3pd5nE9qa99HAZtuZuj6F1huXg==",
// 			},
// 			args: args{
// 				endpoint: "ClosedOrders",
// 				offset:   0,
// 			},
// 			wantErr: true,
// 		},
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			_, err := tt.k.getPrivate(tt.args.endpoint, tt.args.offset)
// 			if (err != nil) != tt.wantErr {
// 				t.Errorf("KrakenAPI.getPrivate() error = %v, wantErr %v", err, tt.wantErr)
// 				return
// 			}
// 		})
// 	}
// }
