package api

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/davecgh/go-spew/spew"
	resty "github.com/go-resty/resty/v2"
)

type BinanceError struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

type BinanceJSON struct {
	AssetDividends map[string]interface{} `json:"assetDividends"`
	Deposits       map[string]interface{} `json:"deposits"`
	Trades         map[string]interface{} `json:"trades"`
	Withdrawals    map[string]interface{} `json:"withdrawals"`
}

type BinanceSymbols struct {
	Symbol                     string   `json:"symbol"`
	Status                     string   `json:"status"`
	BaseAsset                  string   `json:"baseAsset"`
	BaseAssetPrecision         int      `json:"baseAssetPrecision"`
	QuotAasset                 string   `json:"quoteAsset"`
	QuotePrecision             int      `json:"quotePrecision"`
	QuoteAssetPrecision        int      `json:"quoteAssetPrecision"`
	BaseCommissionPrecision    int      `json:"baseCommissionPrecision"`
	QuoteCommissionPrecision   int      `json:"quoteCommissionPrecision"`
	OrderTypes                 []string `json:"orderTypes"`
	IcebergAllowed             bool     `json:"icebergAllowed"`
	OcoAllowed                 bool     `json:"ocoAllowed"`
	QuoteOrderQtyMarketAllowed bool     `json:"quoteOrderQtyMarketAllowed"`
	IsSpotTradingAllowed       bool     `json:"isSpotTradingAllowed"`
	IsMarginTradingAllowed     bool     `json:"isMarginTradingAllowed"`
	Filters                    []struct {
		FilterType       string `json:"filterType"`
		MinPrice         string `json:"minPrice,omitempty"`
		MaxPrice         string `json:"maxPrice,omitempty"`
		TickSize         string `json:"tickSize,omitempty"`
		MultiplierUp     string `json:"multiplierUp,omitempty"`
		MultiplierDown   string `json:"multiplierDown,omitempty"`
		AvgPriceMins     int    `json:"avgPriceMins,omitempty"`
		MinQty           string `json:"minQty,omitempty"`
		MaxQty           string `json:"maxQty,omitempty"`
		StepSize         string `json:"stepSize,omitempty"`
		MinNotional      string `json:"minNotional,omitempty"`
		ApplyToMarket    bool   `json:"applyToMarket,omitempty"`
		Limit            int    `json:"limit,omitempty"`
		MaxNumOrders     int    `json:"maxNumOrders,omitempty"`
		MaxNumAlgoOrders int    `json:"maxNumAlgoOrders,omitempty"`
	} `json:"filters"`
	Permissions []string `json:"permissions"`
}

type BinanceRatelimits struct {
	RateLimitType string `json:"rateLimitType"`
	Interval      string `json:"interval"`
	IntervalNum   int    `json:"intervalNum"`
	Limit         int    `json:"limit"`
}

type BinanceExchangeInfo struct {
	TimeZone        string              `json:"timezone"`
	ServerTime      int64               `json:"serverTime"`
	RateLimits      []BinanceRatelimits `json:"rateLimits"`
	ExchangeFilters []interface{}       `json:"exchangeFilters"`
	Symbols         []BinanceSymbols    `json:"symbols"`
}

type BinanceAPI struct {
	debug            bool
	indent           string
	client           *resty.Client
	baseScheme       string
	baseURL          string
	apiKey           string
	secretKey        string
	infos            BinanceExchangeInfo
	serverTimeOffset int64
	weightInterval   time.Duration
	weightLimit      int
	orderInterval    time.Duration
	orderLimit       int
	json             BinanceJSON
}

var Binance = BinanceAPI{
	baseScheme:     "https://",
	baseURL:        "api.binance.com",
	weightInterval: time.Minute,
	weightLimit:    1200,
}

func (api BinanceAPI) GetName() string {
	return "Binance"
}

func (api BinanceAPI) GetURL() string {
	return "https://www.binance.com/fr/support/faq/360002502072"
}

func (api BinanceAPI) GetPlateform() bool {
	return true
}

func (api BinanceAPI) NeedKey() bool {
	return true
}

func (api BinanceAPI) GetJSONName() string {
	return "Binance_API.json"
}

func (api BinanceAPI) GetJSONs(conf map[string]interface{}) (jsons chan JSONFile, progress chan int, errorChan chan error, err error) {
	if val, ok := conf["arg1"]; ok {
		api.apiKey = val.(string)
	} else {
		err = errors.New("missing apiKey")
		return
	}
	if val, ok := conf["arg2"]; ok {
		api.secretKey = val.(string)
	} else {
		err = errors.New("missing secretKey")
		return
	}
	if val, ok := conf["proxy"]; ok {
		api.baseScheme = val.(string)
	}
	if val, ok := conf["debug"]; ok {
		api.debug = val.(bool)
	}
	if val, ok := conf["indent"]; ok {
		api.indent = val.(string)
	}
	if val, ok := conf["json"]; ok {
		json.Unmarshal(val.([]byte), &api.json)
	}
	if api.json.AssetDividends == nil {
		api.json.AssetDividends = make(map[string]interface{})
	}
	if api.json.Deposits == nil {
		api.json.Deposits = make(map[string]interface{})
	}
	if api.json.Trades == nil {
		api.json.Trades = make(map[string]interface{})
	}
	if api.json.Withdrawals == nil {
		api.json.Withdrawals = make(map[string]interface{})
	}
	if api.client == nil {
		api.client = resty.New()
		api.client.SetTransport(http.DefaultTransport)
		api.client.SetDebug(api.debug)
		api.client.SetDebugBodyLimit(100)
	}
	api.client.SetRetryCount(3)
	api.client.SetRetryWaitTime(5 * time.Second)
	var apiErr BinanceError
	u, _ := url.Parse(api.baseURL + "/api/v3/exchangeInfo")
	base, err := url.Parse(api.baseScheme)
	if err != nil {
		return
	}
	resp, err := api.client.R().
		SetResult(&api.infos).
		SetError(&apiErr).
		Get(base.ResolveReference(u).String())
	if err != nil {
		return
	}
	if apiErr.Code != 0 || !resp.IsSuccess() {
		err = errors.New("Error Requesting exchangeInfo " + apiErr.Msg)
		return
	}
	api.serverTimeOffset = time.Now().UnixMilli() - api.infos.ServerTime
	for _, rate := range api.infos.RateLimits {
		if rate.RateLimitType == "REQUEST_WEIGHT" {
			api.weightLimit = rate.Limit
			interval := time.Minute
			if rate.Interval == "SECOND" {
				interval = time.Second
			} else if rate.Interval == "HOUR" {
				interval = time.Hour
			}
			api.weightInterval = time.Duration(rate.IntervalNum) * interval
		} else if rate.RateLimitType == "ORDERS" &&
			rate.Interval != "DAY" {
			api.orderLimit = rate.Limit
			interval := time.Minute
			if rate.Interval == "SECOND" {
				interval = time.Second
			} else if rate.Interval == "HOUR" {
				interval = time.Hour
			}
			api.orderInterval = time.Duration(rate.IntervalNum) * interval
		}
	}
	jsons = make(chan JSONFile, 10)
	progress = make(chan int)
	errorChan = make(chan error)
	var j JSONFile
	j.Name = api.GetJSONName()
	go func() {
		progress <- 1
		type binancePrivate struct {
			endpoint      string
			progressStart int
			progressEnd   int
			limit         int
			store         map[string]interface{}
		}
		privates := []binancePrivate{
			binancePrivate{
				endpoint:      "/sapi/v1/asset/assetDividend",
				progressStart: 0,
				progressEnd:   10,
				limit:         500,
				store:         api.json.AssetDividends,
			},
			binancePrivate{
				endpoint:      "/sapi/v1/capital/deposit/hisrec",
				progressStart: 10,
				progressEnd:   40,
				limit:         1000,
				store:         api.json.Deposits,
			},
			binancePrivate{
				endpoint:      "/sapi/v1/capital/withdraw/history",
				progressStart: 40,
				progressEnd:   70,
				limit:         1000,
				store:         api.json.Withdrawals,
			},
			// binancePrivate{
			// 	endpoint:      "/orders/closed",
			// 	progressStart: 70,
			// 	progressEnd:   100,
			// 	store:         api.json.Trades,
			// },
		}
		for _, p := range privates {
			if p.store["objects"] == nil {
				p.store["objects"] = make(map[string]interface{})
			}
			var startTime int64 = 1499990400000 // Binance opening date : July 14th 2017
			if p.store["startTime"] != nil {
				startTime = int64(p.store["startTime"].(float64))
			}
			now := time.Now().UnixMilli()
			prog := p.progressStart
			for startTime < now {
				offset := 0
				got := p.limit
				for got == p.limit {
					res, err := api.getPrivate(p.endpoint, startTime, offset)
					if err != nil {
						continue
					}
					if p.endpoint == "/sapi/v1/asset/assetDividend" {
						got = int(res.(map[string]interface{})["total"].(float64))
						for _, r := range res.(map[string]interface{})["rows"].([]interface{}) {
							p.store["objects"].(map[string]interface{})[strconv.FormatFloat(r.(map[string]interface{})["id"].(float64), 'f', 0, 64)] = r
						}
					} else {
						got = len(res.([]interface{}))
						offset += got
						for _, r := range res.([]interface{}) {
							p.store["objects"].(map[string]interface{})[r.(map[string]interface{})["txId"].(string)] = r
						}
					}
					prog += 5
					if prog > p.progressEnd-5 {
						prog = p.progressStart + 5
					}
					progress <- prog
				}
				startTime += 90 * 24 * 60 * 60 * 1000
				if startTime > now {
					startTime = now
				}
				p.store["startTime"] = startTime
				j.Data, err = json.MarshalIndent(api.json, "", api.indent)
				if err == nil {
					jsons <- j
				}
			}
			progress <- p.progressEnd
		}
		progress <- 100
	}()
	return
}

func (api BinanceAPI) getPrivate(endpoint string, startTime int64, offset int) (result interface{}, err error) {
	endTime := startTime + 90*24*60*60*1000
	now := time.Now().UnixMilli() - api.serverTimeOffset - 1000
	if now < endTime {
		endTime = now
	}
	resource := endpoint
	headers := map[string]string{
		"Accept":       "application/json",
		"X-MBX-APIKEY": api.apiKey,
	}
	params := map[string]string{
		"startTime":  strconv.FormatInt(startTime, 10),
		"endTime":    strconv.FormatInt(endTime, 10),
		"recvWindow": "60000",
		"timestamp":  strconv.FormatInt(now, 10),
	}
	if endpoint == "/sapi/v1/capital/deposit/hisrec" {
		params["status"] = "1"
	}
	if endpoint == "/sapi/v1/capital/withdraw/history" {
		params["status"] = "6"
	}
	if endpoint == "/sapi/v1/asset/assetDividend" {
		params["limit"] = "500"
	} else {
		if offset > 0 {
			params["offset"] = strconv.Itoa(offset)
		}
	}
	api.sign(params)
	time.Sleep(api.weightInterval / time.Duration(api.weightLimit))
	var apiErr BinanceError
	u, _ := url.Parse(api.baseURL + resource)
	base, err := url.Parse(api.baseScheme)
	if err != nil {
		return nil, err
	}
	resp, err := api.client.R().
		SetQueryParams(params).
		SetHeaders(headers).
		SetError(&apiErr).
		SetResult(&result).
		Get(base.ResolveReference(u).String())
	if err != nil {
		return nil, err
	}
	respHeaders := resp.Header()
	if len(respHeaders["X-Mbx-Used-Weight"]) > 0 {
		spew.Dump(respHeaders["X-Mbx-Used-Weight"])
		used, err := strconv.Atoi(respHeaders["X-Mbx-Used-Weight"][0])
		if err == nil {
			if used > api.weightLimit {
				time.Sleep(api.weightInterval)
			}
		}
	}
	if resp.StatusCode() == 429 {
		// When a 429 is received, it's your obligation as an API to back off and not spam the API.
		// A Retry-After header is sent with a 418 or 429 responses and will give the number of seconds
		// required to wait, in the case of a 429, to prevent a ban, or, in the case of a 418, until the ban is over.
		spew.Dump(respHeaders)
		var backoffTime int
		if len(respHeaders["Retry-After"]) > 0 {
			backoffTime, err = strconv.Atoi(respHeaders["Retry-After"][0])
			if err != nil {
				backoffTime = 10
			}
		}
		time.Sleep(time.Duration(backoffTime) * time.Second)
	}
	if apiErr.Code != 0 || !resp.IsSuccess() {
		return nil, errors.New("Error Requesting " + endpoint + " " + apiErr.Msg)
	}
	return
}

func (api BinanceAPI) sign(params map[string]string) {
	keys := make([]string, 0, len(params))
	for key := range params {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	paramString := []string{}
	for _, keySorted := range keys {
		paramString = append(paramString, keySorted+"="+fmt.Sprintf("%v", params[keySorted]))
	}
	sigPayload := strings.Join(paramString, "&")
	key := []byte(api.secretKey)
	mac := hmac.New(sha256.New, key)
	mac.Write([]byte(sigPayload))
	params["signature"] = hex.EncodeToString(mac.Sum(nil))
}
