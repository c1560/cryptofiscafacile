package api

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"strings"
	"time"

	resty "github.com/go-resty/resty/v2"
)

type AlgoResult struct {
	CurrentRound int         `json:"current-round"`
	NextToken    string      `json:"next-token"`
	Transactions interface{} `json:"transactions"`
}

type AlgoJSON struct {
	Addresses map[string]bool        `json:"algo_addrs"`
	Pay       map[string]interface{} `json:"pay"`     //pay
	Axfer     map[string]interface{} `json:"axfer"`   //transfer
	Appl      map[string]interface{} `json:"tx_appl"` //application
}

type AlgoAPI struct {
	debug       bool
	indent      string
	client      *resty.Client
	address     string
	baseURL     string
	lastReqTime time.Time
	json        AlgoJSON
}

var Algo = AlgoAPI{
	baseURL: "https://algoindexer.algoexplorerapi.io/v2/accounts/",
}

func (alg AlgoAPI) GetName() string {
	return "Algorand"
}

func (alg AlgoAPI) GetURL() string {
	return "https://algoexplorer.io/"
	// return "https://algoexplorer.io/api-dev/v2"
}

func (alg AlgoAPI) GetPlateform() bool {
	return false
}

func (alg AlgoAPI) NeedKey() bool {
	return false
}

func (alg AlgoAPI) GetJSONName() string {
	return "Algorand.json"
}

func (alg AlgoAPI) WithChannels() bool {
	return true
}

func (alg AlgoAPI) GetEmptyJson() JSONFile {
	emptyJson := AlgoJSON{Addresses: make(map[string]bool)}
	emptyJsonFile := JSONFile{Name: "empty-algorand.json"}
	emptyJsonFile.Data, _ = json.MarshalIndent(emptyJson, "", alg.indent)

	return emptyJsonFile
}

func (alg AlgoAPI) GetJSONs(conf map[string]interface{}) (jsons chan JSONFile, progress chan int, errorChan chan error, err error) {
	if val, ok := conf["arg1"]; ok {
		alg.address = strings.ToUpper(val.(string))
	}
	if alg.address == "" {
		err = errors.New("missing Algo Address")
		return
	}
	if val, ok := conf["debug"]; ok {
		alg.debug = val.(bool)
	}
	if val, ok := conf["indent"]; ok {
		alg.indent = val.(string)
	}
	if val, ok := conf["json"]; ok {
		json.Unmarshal(val.([]byte), &alg.json)
	}
	if alg.json.Addresses == nil {
		alg.json.Addresses = make(map[string]bool)
	}
	if alg.json.Pay == nil {
		alg.json.Pay = make(map[string]interface{})
	}
	if alg.json.Axfer == nil {
		alg.json.Axfer = make(map[string]interface{})
	}
	if alg.json.Appl == nil {
		alg.json.Appl = make(map[string]interface{})
	}
	if alg.client == nil {
		alg.client = resty.New()
		alg.client.SetTransport(http.DefaultTransport)
		alg.client.SetDebug(alg.debug)
		alg.client.SetDebugBodyLimit(200)
	}
	alg.client.SetRetryCount(3)
	alg.client.SetRetryWaitTime(time.Second)
	jsons = make(chan JSONFile, 10)
	progress = make(chan int)
	errorChan = make(chan error)
	var j JSONFile
	j.Name = alg.GetJSONName()
	go func() {
		progress <- 1
		type algoTX struct {
			endpoint    string
			progressEnd int
			store       map[string]interface{}
		}
		kinds := []algoTX{
			algoTX{
				endpoint:    "pay",
				progressEnd: 40,
				store:       alg.json.Pay,
			},
			algoTX{
				endpoint:    "axfer",
				progressEnd: 70,
				store:       alg.json.Axfer,
			},
			algoTX{
				endpoint:    "appl",
				progressEnd: 99,
				store:       alg.json.Appl,
			},
		}
		for _, k := range kinds {
			txs, err := alg.getTXsByAddress(alg.address, k.endpoint)
			// TODO : add a way to return the error to main app
			if err == nil {
				for _, t := range txs {
					key := "id"
					k.store[t.(map[string]interface{})[key].(string)] = t
				}
				alg.json.Addresses[alg.address] = true
				j.Data, err = json.MarshalIndent(alg.json, "", alg.indent)
				if err == nil {
					jsons <- j
				}
			}
			progress <- k.progressEnd
		}
		progress <- 100
	}()
	return
}

func (alg AlgoAPI) GetJsons(map[string]interface{}) error {
	return nil
}

func (alg *AlgoAPI) getTXsByAddress(addr string, txType string) (txs []interface{}, err error) {
	// No rate limit on Algorand API, but minimal timeout added
	time.Sleep(100*time.Millisecond - time.Since(alg.lastReqTime))
	params := map[string]string{
		"tx-type": txType,
	}
	headers := map[string]string{
		"Accept": "application/json",
	}
	var res AlgoResult
	alg.lastReqTime = time.Now()
	resp, err := alg.client.R().
		SetQueryParams(params).
		SetHeaders(headers).
		SetResult(&res).
		Get(alg.baseURL + addr + "/transactions")
	if err != nil {
		return
	}
	if !resp.IsSuccess() {
		err = errors.New("Error Requesting TX for " + addr + " " + strconv.Itoa(resp.StatusCode()))
		return
	}
	txs = res.Transactions.([]interface{})
	return
}
