package api

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"strings"
	"time"

	resty "github.com/go-resty/resty/v2"
)

type CronosResult struct {
	Status  string      `json:"status"`
	Message string      `json:"message"`
	Result  interface{} `json:"result"`
}

type CronosJSON struct {
	Addresses   map[string]bool        `json:"cronos_addrs"`
	NormalTXs   map[string]interface{} `json:"normal_txs"`
	InternalTXs map[string]interface{} `json:"internal_txs"`
	TokenTXs    map[string]interface{} `json:"token_txs"`
	MiningTXs   map[string]interface{} `json:"mining_txs"`
}

type CronosAPI struct {
	debug       bool
	indent      string
	client      *resty.Client
	address     string
	baseURL     string
	lastReqTime time.Time
	tier        string
	json        CronosJSON
}

var Cronos = CronosAPI{
	baseURL: "https://cronos.org/explorer/api",
}

func (cs CronosAPI) GetName() string {
	return "Cronos"
}

func (cs CronosAPI) GetURL() string {
	return "https://cronos.org/explorer/"
	// return "https://cronos.org/explorer/api-docs"
}

func (cs CronosAPI) GetPlateform() bool {
	return false
}

func (cs CronosAPI) NeedKey() bool {
	return false
}

func (cs CronosAPI) GetJSONName() string {
	return "Cronos.json"
}

func (cs CronosAPI) WithChannels() bool {
	return true
}

func (cs CronosAPI) GetEmptyJson() JSONFile {
	emptyJson := CronosJSON{Addresses: make(map[string]bool)}
	emptyJsonFile := JSONFile{Name: "empty-cronos.json"}
	emptyJsonFile.Data, _ = json.MarshalIndent(emptyJson, "", cs.indent)

	return emptyJsonFile
}

func (cs CronosAPI) GetJSONs(conf map[string]interface{}) (jsons chan JSONFile, progress chan int, errorChan chan error, err error) {
	if val, ok := conf["arg1"]; ok {
		cs.address = strings.ToLower(val.(string))
	}
	if val, ok := conf["arg2"]; ok && val != "" {
		cs.address = strings.ToLower(val.(string))
	}
	if cs.address == "" {
		err = errors.New("missing Cronos Address")
		return
	}
	if val, ok := conf["arg3"]; ok && val != "" {
		cs.tier = val.(string)
	}
	if val, ok := conf["debug"]; ok {
		cs.debug = val.(bool)
	}
	if val, ok := conf["indent"]; ok {
		cs.indent = val.(string)
	}
	if val, ok := conf["json"]; ok {
		json.Unmarshal(val.([]byte), &cs.json)
	}
	if cs.json.Addresses == nil {
		cs.json.Addresses = make(map[string]bool)
	}
	if cs.json.NormalTXs == nil {
		cs.json.NormalTXs = make(map[string]interface{})
	}
	if cs.json.InternalTXs == nil {
		cs.json.InternalTXs = make(map[string]interface{})
	}
	if cs.json.TokenTXs == nil {
		cs.json.TokenTXs = make(map[string]interface{})
	}
	if cs.json.MiningTXs == nil {
		cs.json.MiningTXs = make(map[string]interface{})
	}
	if cs.client == nil {
		cs.client = resty.New()
		cs.client.SetTransport(http.DefaultTransport)
		cs.client.SetDebug(cs.debug)
		cs.client.SetDebugBodyLimit(200)
	}
	cs.client.SetRetryCount(3)
	cs.client.SetRetryWaitTime(time.Second)
	jsons = make(chan JSONFile, 10)
	progress = make(chan int)
	errorChan = make(chan error)
	var j JSONFile
	j.Name = cs.GetJSONName()
	go func() {
		progress <- 1
		type cronosTX struct {
			endpoint    string
			progressEnd int
			store       map[string]interface{}
		}
		kinds := []cronosTX{
			cronosTX{
				endpoint:    "txlist",
				progressEnd: 20,
				store:       cs.json.NormalTXs,
			},
			cronosTX{
				endpoint:    "txlistinternal",
				progressEnd: 40,
				store:       cs.json.InternalTXs,
			},
			cronosTX{
				endpoint:    "tokentx",
				progressEnd: 60,
				store:       cs.json.TokenTXs,
			},
			cronosTX{
				endpoint:    "getminedblocks",
				progressEnd: 99,
				store:       cs.json.MiningTXs,
			},
		}
		for _, k := range kinds {
			txs, err := cs.getTXsByAddress(cs.address, k.endpoint)
			// TODO : add a way to return the error to main app
			if err == nil {
				for _, t := range txs {
					key := "hash"
					if k.endpoint == "txlistinternal" {
						key = "transactionHash"
					}
					if k.endpoint == "getminedblocks" {
						key = "blockNumber"
					}
					k.store[t.(map[string]interface{})[key].(string)] = t
				}
				cs.json.Addresses[cs.address] = true
				j.Data, err = json.MarshalIndent(cs.json, "", cs.indent)
				if err == nil {
					jsons <- j
				}
			}
			progress <- k.progressEnd
		}
		progress <- 100
	}()
	return
}

func (cs CronosAPI) GetJsons(map[string]interface{}) error {
	return nil
}

func (cs *CronosAPI) getTXsByAddress(addr, kind string) (txs []interface{}, err error) {
	cs.waitRateLimit()
	params := map[string]string{
		"module":  "account",
		"action":  kind,
		"address": addr,
	}
	if kind != "getminedblocks" {
		params["sort"] = "asc"
	}
	headers := map[string]string{
		"Accept": "application/json",
	}
	var res CronosResult
	cs.lastReqTime = time.Now()
	resp, err := cs.client.R().
		SetQueryParams(params).
		SetHeaders(headers).
		SetResult(&res).
		Get(cs.baseURL)
	if err != nil {
		return
	}
	if !resp.IsSuccess() {
		err = errors.New("Error Requesting Normal TX for " + addr + " " + strconv.Itoa(resp.StatusCode()))
		return
	}
	if res.Status == "0" {
		if res.Message == "NOTOK" {
			// Downgrade tier
			cs.tier = ""
		}
		err = errors.New("Error Requesting Normal TX for " + addr + " " + res.Message)
		return
	}
	txs = res.Result.([]interface{})
	return
}

func (cs CronosAPI) waitRateLimit() {
	// tier == "" : 1 call every 5 seconds
	wait := 5500 * time.Millisecond
	if cs.tier == "Free" {
		// 5 calls/second
		wait = 200 * time.Millisecond
	} else if cs.tier == "Standard" {
		// 10 calls/second
		wait = 100 * time.Millisecond
	} else if cs.tier == "Advanced" {
		// 20 calls/second
		wait = 50 * time.Millisecond
	} else if cs.tier == "Professional" {
		// 30 calls/second
		wait = 34 * time.Millisecond
	}
	time.Sleep(wait - time.Since(cs.lastReqTime))
}
