package api

import (
	"crypto/hmac"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base64"
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	resty "github.com/go-resty/resty/v2"
)

type KrakenErrorResult struct {
	Error  []string    `json:"error"`
	Result interface{} `json:"result"`
}

type KrakenJSON struct {
	Assets     interface{}            `json:"assets"`
	AssetPairs interface{}            `json:"assetpairs"`
	Ledger     map[string]interface{} `json:"ledger"`
	Trades     map[string]interface{} `json:"trades"`
}

type KrakenAPI struct {
	debug      bool
	indent     string
	client     *resty.Client
	baseScheme string
	baseURL    string
	apiKey     string
	secretKey  string
	json       KrakenJSON
	tier       string
	counter    int
	countDown  bool
}

var Kraken = KrakenAPI{
	baseScheme: "https://",
	baseURL:    "api.kraken.com",
	tier:       "Starter",
	counter:    0,
}

func (k KrakenAPI) GetName() string {
	return "Kraken"
}

func (k KrakenAPI) GetURL() string {
	return "https://support.kraken.com/hc/en-us/articles/360000919966-How-to-generate-an-API-key-pair-"
}

func (k KrakenAPI) GetPlateform() bool {
	return true
}

func (k KrakenAPI) NeedKey() bool {
	return true
}

func (k KrakenAPI) GetJSONName() string {
	return "Kraken.json"
}

func (k KrakenAPI) GetJSONs(conf map[string]interface{}) (jsons chan JSONFile, progress chan int, errorChan chan error, err error) {
	if val, ok := conf["arg1"]; ok {
		k.apiKey = val.(string)
	} else {
		err = errors.New("missing apiKey")
		return
	}
	if val, ok := conf["arg2"]; ok {
		k.secretKey = val.(string)
	} else {
		err = errors.New("missing secretKey")
		return
	}
	if val, ok := conf["proxy"]; ok {
		k.baseScheme = val.(string)
	}
	if val, ok := conf["debug"]; ok {
		k.debug = val.(bool)
	}
	if val, ok := conf["indent"]; ok {
		k.indent = val.(string)
	}
	if val, ok := conf["json"]; ok {
		json.Unmarshal(val.([]byte), &k.json)
	}
	if k.json.Ledger == nil {
		k.json.Ledger = make(map[string]interface{})
	}
	if k.json.Trades == nil {
		k.json.Trades = make(map[string]interface{})
	}
	if k.client == nil {
		k.client = resty.New()
		k.client.SetTransport(http.DefaultTransport)
		k.client.SetDebug(k.debug)
		k.client.SetDebugBodyLimit(100)
	}
	k.client.SetRetryCount(3)
	k.client.SetRetryWaitTime(6 * time.Second)
	jsons = make(chan JSONFile, 10)
	progress = make(chan int)
	errorChan = make(chan error)
	var j JSONFile
	j.Name = k.GetJSONName()
	go func() {
		progress <- 1
		k.json.Assets, err = k.getPublic("Assets")
		progress <- 2
		k.json.AssetPairs, err = k.getPublic("AssetPairs")
		progress <- 4
		type krakenPrivate struct {
			endpoint      string
			object        string
			progressStart int
			progressSize  int
			store         map[string]interface{}
		}
		privates := []krakenPrivate{
			krakenPrivate{
				endpoint:      "Ledgers",
				object:        "ledger",
				progressStart: 4,
				progressSize:  48,
				store:         k.json.Ledger,
			},
			krakenPrivate{
				endpoint:      "TradesHistory",
				object:        "trades",
				progressStart: 52,
				progressSize:  48,
				store:         k.json.Trades,
			},
		}
		for _, p := range privates {
			offset := 0
			totalObjects := make(map[string]interface{})
			if p.store["objects"] == nil {
				p.store["objects"] = make(map[string]interface{})
			}
			for k, v := range p.store["objects"].(map[string]interface{}) {
				offset++
				totalObjects[k] = v
			}
			totalCount := 100000
			for offset < totalCount {
				res, err := k.getPrivate(p.endpoint, offset)
				if err != nil {
					if strings.Contains(err.Error(), "EOrder:Rate limit exceeded") {
						time.Sleep(12 * time.Second) // because of preflight count as double...
						continue
					}
					// TODO : add a way to return the error to main app
					return
				}
				result := res.(map[string]interface{})
				totalCount = int(result["count"].(float64))
				p.store["count"] = totalCount
				for k, v := range result[p.object].(map[string]interface{}) {
					offset++
					totalObjects[k] = v
				}
				p.store["objects"] = totalObjects
				j.Data, err = json.MarshalIndent(k.json, "", k.indent)
				if err == nil {
					jsons <- j
				}
				if totalCount > 0 {
					progress <- int(p.progressStart + (offset * p.progressSize / totalCount))
				}
			}
		}
		progress <- 100
	}()
	return
}

func (k KrakenAPI) getPublic(endpoint string) (result interface{}, err error) {
	headers := make(map[string]string)
	headers["Accept"] = "application/json"
	var res KrakenErrorResult
	u, _ := url.Parse(k.baseURL + "/0/public/" + endpoint)
	base, err := url.Parse(k.baseScheme)
	if err != nil {
		return
	}
	resp, err := k.client.R().
		SetHeaders(headers).
		SetResult(&res).
		Get(base.ResolveReference(u).String())
	if err != nil {
		return
	}
	if len(res.Error) > 0 || !resp.IsSuccess() {
		err = errors.New("Error Requesting public/Assets" + strings.Join(res.Error, ""))
		return
	}
	result = res.Result
	return
}

func (k KrakenAPI) getPrivate(endpoint string, offset int) (result interface{}, err error) {
	resource := "/0/private/" + endpoint
	headers := make(map[string]string)
	headers["API-Key"] = k.apiKey
	headers["Content-Type"] = "application/x-www-form-urlencoded"
	// Wait for enough counter Limit credit for the request
	for (k.counter > 13 && k.tier == "Starter") ||
		k.counter > 18 {
		time.Sleep(200 * time.Millisecond)
	}
	body := url.Values{}
	body.Set("nonce", strconv.FormatInt(time.Now().UTC().UnixMilli(), 10))
	body.Set("ofs", strconv.Itoa(offset))
	if endpoint == "TradesHistory" {
		body.Set("trades", "true")
	}
	headers["API-Sign"], err = k.sign(body, resource)
	if err != nil {
		return nil, err
	}
	var res KrakenErrorResult
	u, _ := url.Parse(k.baseURL + resource)
	base, err := url.Parse(k.baseScheme)
	if err != nil {
		return
	}
	resp, err := k.client.R().
		SetHeaders(headers).
		SetFormDataFromValues(body).
		SetResult(&res).
		Post(base.ResolveReference(u).String())
	if err != nil {
		return nil, err
	}
	if len(res.Error) > 0 || !resp.IsSuccess() {
		return nil, errors.New("Error Requesting private/" + endpoint + " " + strings.Join(res.Error, ""))
	}
	if endpoint == "Ledgers" || endpoint == "TradesHistory" {
		k.counter += 2
	} else {
		k.counter++
	}
	go func() {
		if !k.countDown {
			k.countDown = true
			for k.counter > 0 {
				if k.tier == "Pro" {
					time.Sleep(time.Second)
				} else if k.tier == "Intermediate" {
					time.Sleep(2 * time.Second)
				} else { // Default = Worst case : tier "Starter"
					time.Sleep(3 * time.Second)
				}
				k.counter--
			}
			k.countDown = false
		}
	}()
	result = res.Result
	return
}

func (k KrakenAPI) sign(body url.Values, resource string) (string, error) {
	sha := sha256.New()
	sha.Write([]byte(body.Get("nonce") + body.Encode()))
	shasum := sha.Sum(nil)
	b64DecodedSecret, err := base64.StdEncoding.DecodeString(k.secretKey)
	if err != nil {
		return "", err
	}
	mac := hmac.New(sha512.New, b64DecodedSecret)
	mac.Write(append([]byte(resource), shasum...))
	return base64.StdEncoding.EncodeToString(mac.Sum(nil)), nil
}
