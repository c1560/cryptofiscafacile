module gitlab.com/c1560/CryptoFiscaFacile/wasm/api/binance

go 1.19

replace gitlab.com/c1560/CryptoFiscaFacile/wasm/api => ../../api

require gitlab.com/c1560/CryptoFiscaFacile/wasm/api v0.0.0-00010101000000-000000000000

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-resty/resty/v2 v2.7.0 // indirect
	github.com/klauspost/compress v1.10.3 // indirect
	golang.org/x/net v0.0.0-20211029224645-99673261e6eb // indirect
	nhooyr.io/websocket v1.8.7 // indirect
)
