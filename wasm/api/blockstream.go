package api

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strconv"
	"time"

	resty "github.com/go-resty/resty/v2"
)

type BlockstreamJSON struct {
	Addresses    map[string]bool        `json:"btc_addrs"`
	Transactions map[string]interface{} `json:"txs"`
}

type BlockstreamAPI struct {
	debug   bool
	indent  string
	client  *resty.Client
	address string
	baseURL string
	json    BlockstreamJSON
}

var Blockstream = BlockstreamAPI{
	baseURL: "https://blockstream.info/api",
}

func (bs BlockstreamAPI) GetName() string {
	return "Bitcoin"
}

func (bs BlockstreamAPI) GetURL() string {
	return "https://blockstream.info"
}

func (bs BlockstreamAPI) GetPlateform() bool {
	return false
}

func (bs BlockstreamAPI) NeedKey() bool {
	return false
}

func (bs BlockstreamAPI) GetJSONName() string {
	return "Blockstream.json"
}

func (bs BlockstreamAPI) WithChannels() bool {
	return false
}

func (bs BlockstreamAPI) GetJSONs(conf map[string]interface{}) (jsons chan JSONFile, progress chan int, errorChan chan error, err error) {
	log.Fatalln("Not implemented: use getJsons")
	return
}

func (bs BlockstreamAPI) GetEmptyJson() JSONFile {
	emptyJson := BlockstreamJSON{Addresses: make(map[string]bool)}
	emptyJsonFile := JSONFile{Name: "empty-blockstream.json"}
	emptyJsonFile.Data, _ = json.MarshalIndent(emptyJson, "", bs.indent)

	return emptyJsonFile
}

func (bs BlockstreamAPI) GetJsons(conf map[string]interface{}) (err error) {
	if val, ok := conf["arg1"]; ok {
		bs.address = val.(string)
	}
	if val, ok := conf["arg2"]; ok && val != "" {
		bs.address = val.(string)
	}
	if bs.address == "" {
		err = errors.New("missing BTC Address")
		return
	}
	if val, ok := conf["debug"]; ok {
		bs.debug = val.(bool)
	}
	if val, ok := conf["indent"]; ok {
		bs.indent = val.(string)
	}
	if val, ok := conf["json"]; ok {
		json.Unmarshal(val.([]byte), &bs.json)
	}
	var onJson func(JSONFile) error
	if val, ok := conf["onJson"]; ok {
		onJson = val.(func(JSONFile) error)
	}

	if bs.json.Addresses == nil {
		bs.json.Addresses = make(map[string]bool)
	}
	if bs.json.Transactions == nil {
		bs.json.Transactions = make(map[string]interface{})
	}
	if bs.client == nil {
		bs.client = resty.New()
		bs.client.SetTransport(http.DefaultTransport)
		bs.client.SetDebug(bs.debug)
		bs.client.SetDebugBodyLimit(100)
	}
	bs.client.SetRetryCount(3)
	bs.client.SetRetryWaitTime(time.Second)

	progress := conf["progress"].(*int)
	err = bs.getAddress(bs.address, progress, onJson)
	if err != nil {
		return err
	}
	*progress = 100

	return nil
}

func (bs *BlockstreamAPI) getAddress(addr string, progress *int, onJson func(JSONFile) error) (err error) {
	info, err := bs.getInformation(addr)
	if err != nil {
		return err
	}
	numberTxs := info.(map[string]interface{})["chain_stats"].(map[string]interface{})["tx_count"].(float64)

	lastSeenTxID := ""
	got := 25
	counter := 0
	for got > 24 {
		txs, err := bs.getTransactions(addr, lastSeenTxID)
		if err != nil {
			return err
		}

		got = len(txs)
		counter = counter + got
		for _, t := range txs {
			lastSeenTxID = t.(map[string]interface{})["txid"].(string)
			bs.json.Transactions[lastSeenTxID] = t
		}
		bs.json.Addresses[addr] = true
		var j JSONFile
		j.Name = bs.GetJSONName()
		j.Data, err = json.MarshalIndent(bs.json, "", bs.indent)
		if err == nil {
			err = onJson(j)
			if err != nil {
				return err
			}
			if numberTxs == 0 {
				*progress = 100
			} else {
				*progress = (counter * 100 / int(numberTxs))
			}
		}
	}
	return nil
}

func (bs BlockstreamAPI) getInformation(addr string) (info interface{}, err error) {
	resource := "/address/" + addr
	headers := make(map[string]string)
	headers["Accept"] = "application/json"
	resp, err := bs.client.R().
		SetHeaders(headers).
		SetResult(&info).
		Get(bs.baseURL + resource)
	if err != nil {
		return
	}
	if !resp.IsSuccess() {
		body := string(resp.Body())
		if body == "Invalid Bitcoin address" {
			err = errors.New("adresse invalide")
		} else {
			err = errors.New(string(resp.Body()) + " [" + strconv.Itoa(resp.StatusCode()) + "]")
		}
	}
	return
}

func (bs BlockstreamAPI) getTransactions(addr, lastSeenTxID string) (txs []interface{}, err error) {
	resource := "/address/" + addr + "/txs/chain"
	if lastSeenTxID != "" {
		resource += "/" + lastSeenTxID
	}
	headers := make(map[string]string)
	headers["Accept"] = "application/json"
	resp, err := bs.client.R().
		SetHeaders(headers).
		SetResult(&txs).
		Get(bs.baseURL + resource)
	if err != nil {
		return
	}
	if !resp.IsSuccess() {
		err = errors.New(string(resp.Body()) + " [" + strconv.Itoa(resp.StatusCode()) + "]")
	}
	return
}
