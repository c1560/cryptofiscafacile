module gitlab.com/c1560/CryptoFiscaFacile/wasm/api

go 1.19

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/go-resty/resty/v2 v2.13.1
	nhooyr.io/websocket v1.8.7
)

require (
	github.com/klauspost/compress v1.10.3 // indirect
	golang.org/x/net v0.0.0-20211029224645-99673261e6eb // indirect
)
