package api

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"strings"
	"time"

	resty "github.com/go-resty/resty/v2"
)

type GnosisResult struct {
	Status  string      `json:"status"`
	Message string      `json:"message"`
	Result  interface{} `json:"result"`
}

type GnosisJSON struct {
	Addresses   map[string]bool        `json:"xdai_addrs"`
	NormalTXs   map[string]interface{} `json:"normal_txs"`
	InternalTXs map[string]interface{} `json:"internal_txs"`
	TokenTXs    map[string]interface{} `json:"token_txs"`
	MiningTXs   map[string]interface{} `json:"mining_txs"`
}

type GnosisAPI struct {
	debug       bool
	indent      string
	client      *resty.Client
	address     string
	baseURL     string
	lastReqTime time.Time
	tier        string
	json        GnosisJSON
}

var Gnosis = GnosisAPI{
	baseURL: "https://blockscout.com/xdai/mainnet/api",
}

func (gn GnosisAPI) GetName() string {
	return "Gnosis"
}

func (gn GnosisAPI) GetURL() string {
	return "https://blockscout.com/xdai/mainnet/"
	// https://blockscout.com/xdai/mainnet/api-docs
}

func (gn GnosisAPI) GetPlateform() bool {
	return false
}

func (gn GnosisAPI) NeedKey() bool {
	return false
}

func (gn GnosisAPI) GetJSONName() string {
	return "Gnosis.json"
}

func (gn GnosisAPI) WithChannels() bool {
	return true
}

func (gn GnosisAPI) GetEmptyJson() JSONFile {
	emptyJson := GnosisJSON{Addresses: make(map[string]bool)}
	emptyJsonFile := JSONFile{Name: "empty-gnosis.json"}
	emptyJsonFile.Data, _ = json.MarshalIndent(emptyJson, "", gn.indent)

	return emptyJsonFile
}

func (gn GnosisAPI) GetJSONs(conf map[string]interface{}) (jsons chan JSONFile, progress chan int, errorChan chan error, err error) {
	if val, ok := conf["arg1"]; ok {
		gn.address = strings.ToLower(val.(string))
	}
	if val, ok := conf["arg2"]; ok && val != "" {
		gn.address = strings.ToLower(val.(string))
	}
	if gn.address == "" {
		err = errors.New("missing Gnosis Address")
		return
	}
	if val, ok := conf["arg3"]; ok && val != "" {
		gn.tier = val.(string)
	}
	if val, ok := conf["debug"]; ok {
		gn.debug = val.(bool)
	}
	if val, ok := conf["indent"]; ok {
		gn.indent = val.(string)
	}
	if val, ok := conf["json"]; ok {
		json.Unmarshal(val.([]byte), &gn.json)
	}
	if gn.json.Addresses == nil {
		gn.json.Addresses = make(map[string]bool)
	}
	if gn.json.NormalTXs == nil {
		gn.json.NormalTXs = make(map[string]interface{})
	}
	if gn.json.InternalTXs == nil {
		gn.json.InternalTXs = make(map[string]interface{})
	}
	if gn.json.TokenTXs == nil {
		gn.json.TokenTXs = make(map[string]interface{})
	}
	if gn.json.MiningTXs == nil {
		gn.json.MiningTXs = make(map[string]interface{})
	}
	if gn.client == nil {
		gn.client = resty.New()
		gn.client.SetTransport(http.DefaultTransport)
		gn.client.SetDebug(gn.debug)
		gn.client.SetDebugBodyLimit(200)
	}
	gn.client.SetRetryCount(3)
	gn.client.SetRetryWaitTime(time.Second)
	jsons = make(chan JSONFile, 10)
	progress = make(chan int)
	errorChan = make(chan error)
	var j JSONFile
	j.Name = gn.GetJSONName()
	go func() {
		progress <- 1
		type gnosisTX struct {
			endpoint    string
			progressEnd int
			store       map[string]interface{}
		}
		kinds := []gnosisTX{
			gnosisTX{
				endpoint:    "txlist",
				progressEnd: 20,
				store:       gn.json.NormalTXs,
			},
			gnosisTX{
				endpoint:    "txlistinternal",
				progressEnd: 40,
				store:       gn.json.InternalTXs,
			},
			gnosisTX{
				endpoint:    "tokentx",
				progressEnd: 60,
				store:       gn.json.TokenTXs,
			},
			gnosisTX{
				endpoint:    "getminedblocks",
				progressEnd: 99,
				store:       gn.json.MiningTXs,
			},
		}
		for _, k := range kinds {
			txs, err := gn.getTXsByAddress(gn.address, k.endpoint)
			// TODO : add a way to return the error to main app
			if err == nil {
				for _, t := range txs {
					key := "hash"
					if k.endpoint == "txlistinternal" {
						key = "transactionHash"
					}
					if k.endpoint == "getminedblocks" {
						key = "blockNumber"
					}
					k.store[t.(map[string]interface{})[key].(string)] = t
				}
				gn.json.Addresses[gn.address] = true
				j.Data, err = json.MarshalIndent(gn.json, "", gn.indent)
				if err == nil {
					jsons <- j
				}
			}
			progress <- k.progressEnd
		}
		progress <- 100
	}()
	return
}

func (gn GnosisAPI) GetJsons(map[string]interface{}) error {
	return nil
}

func (gn *GnosisAPI) getTXsByAddress(addr, kind string) (txs []interface{}, err error) {
	gn.waitRateLimit()
	params := map[string]string{
		"module":  "account",
		"action":  kind,
		"address": addr,
	}
	if kind != "getminedblocks" {
		params["sort"] = "asc"
	}
	headers := map[string]string{
		"Accept": "application/json",
	}
	var res GnosisResult
	gn.lastReqTime = time.Now()
	resp, err := gn.client.R().
		SetQueryParams(params).
		SetHeaders(headers).
		SetResult(&res).
		Get(gn.baseURL)
	if err != nil {
		return
	}
	if !resp.IsSuccess() {
		err = errors.New("Error Requesting Normal TX for " + addr + " " + strconv.Itoa(resp.StatusCode()))
		return
	}
	if res.Status == "0" {
		if res.Message == "NOTOK" {
			// Downgrade tier
			gn.tier = ""
		}
		err = errors.New("Error Requesting Normal TX for " + addr + " " + res.Message)
		return
	}
	txs = res.Result.([]interface{})
	return
}

func (gn GnosisAPI) waitRateLimit() {
	// tier == "" : 1 call every 5 seconds
	wait := 5500 * time.Millisecond
	if gn.tier == "Free" {
		// 5 calls/second
		wait = 200 * time.Millisecond
	} else if gn.tier == "Standard" {
		// 10 calls/second
		wait = 100 * time.Millisecond
	} else if gn.tier == "Advanced" {
		// 20 calls/second
		wait = 50 * time.Millisecond
	} else if gn.tier == "Professional" {
		// 30 calls/second
		wait = 34 * time.Millisecond
	}
	time.Sleep(wait - time.Since(gn.lastReqTime))
}
