package api

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"strings"
	"time"

	resty "github.com/go-resty/resty/v2"
)

type BscscanResult struct {
	Status  string      `json:"status"`
	Message string      `json:"message"`
	Result  interface{} `json:"result"`
}

type BscscanJSON struct {
	Addresses   map[string]bool        `json:"bsc_addrs"`
	NormalTXs   map[string]interface{} `json:"normal_txs"`
	InternalTXs map[string]interface{} `json:"internal_txs"`
	TokenTXs    map[string]interface{} `json:"token_txs"`
	NftTXs      map[string]interface{} `json:"nft_txs"`
	MiningTXs   map[string]interface{} `json:"mining_txs"`
}

type BscscanAPI struct {
	debug       bool
	indent      string
	client      *resty.Client
	address     string
	baseURL     string
	apiKey      string
	lastReqTime time.Time
	tier        string
	json        BscscanJSON
}

var Bscscan = BscscanAPI{
	baseURL: "https://api.bscscan.com/api",
	// tier:    "Free",
}

func (es BscscanAPI) GetName() string {
	return "BinanceSmartChain"
}

func (es BscscanAPI) GetURL() string {
	return "https://bscscan.com"
	// return "https://docs.bscscan.com/getting-started/viewing-api-usage-statistics"
}

func (es BscscanAPI) GetPlateform() bool {
	return false
}

func (es BscscanAPI) NeedKey() bool {
	return true
}

func (es BscscanAPI) GetJSONName() string {
	return "BinanceSmartChain.json"
}

func (es BscscanAPI) WithChannels() bool {
	return true
}

func (es BscscanAPI) GetEmptyJson() JSONFile {
	emptyJson := BscscanJSON{Addresses: make(map[string]bool)}
	emptyJsonFile := JSONFile{Name: "empty-binance-smart-chain.json"}
	emptyJsonFile.Data, _ = json.MarshalIndent(emptyJson, "", es.indent)

	return emptyJsonFile
}

func (es BscscanAPI) GetJSONs(conf map[string]interface{}) (jsons chan JSONFile, progress chan int, errorChan chan error, err error) {
	if val, ok := conf["arg1"]; ok && val != "" {
		es.address = strings.ToLower(val.(string))
	} else {
		err = errors.New("missing BSC Address")
		return
	}
	if val, ok := conf["arg2"]; ok {
		es.apiKey = val.(string)
		if es.apiKey != "" {
			es.tier = "Free"
		}
	}
	if val, ok := conf["arg3"]; ok && val != "" {
		es.tier = val.(string)
	}
	if val, ok := conf["debug"]; ok {
		es.debug = val.(bool)
	}
	if val, ok := conf["indent"]; ok {
		es.indent = val.(string)
	}
	if val, ok := conf["json"]; ok {
		json.Unmarshal(val.([]byte), &es.json)
	}
	if es.json.Addresses == nil {
		es.json.Addresses = make(map[string]bool)
	}
	if es.json.NormalTXs == nil {
		es.json.NormalTXs = make(map[string]interface{})
	}
	if es.json.InternalTXs == nil {
		es.json.InternalTXs = make(map[string]interface{})
	}
	if es.json.TokenTXs == nil {
		es.json.TokenTXs = make(map[string]interface{})
	}
	if es.json.NftTXs == nil {
		es.json.NftTXs = make(map[string]interface{})
	}
	if es.json.MiningTXs == nil {
		es.json.MiningTXs = make(map[string]interface{})
	}
	if es.client == nil {
		es.client = resty.New()
		es.client.SetTransport(http.DefaultTransport)
		es.client.SetDebug(es.debug)
		es.client.SetDebugBodyLimit(200)
	}
	es.client.SetRetryCount(3)
	es.client.SetRetryWaitTime(time.Second)
	jsons = make(chan JSONFile, 10)
	progress = make(chan int)
	errorChan = make(chan error)
	var j JSONFile
	j.Name = es.GetJSONName()
	go func() {
		progress <- 1
		type bscscanTX struct {
			endpoint    string
			progressEnd int
			store       map[string]interface{}
		}
		kinds := []bscscanTX{
			bscscanTX{
				endpoint:    "txlist",
				progressEnd: 20,
				store:       es.json.NormalTXs,
			},
			bscscanTX{
				endpoint:    "txlistinternal",
				progressEnd: 40,
				store:       es.json.InternalTXs,
			},
			bscscanTX{
				endpoint:    "tokentx",
				progressEnd: 60,
				store:       es.json.TokenTXs,
			},
			bscscanTX{
				endpoint:    "tokennfttx",
				progressEnd: 80,
				store:       es.json.NftTXs,
			},
			bscscanTX{
				endpoint:    "getminedblocks",
				progressEnd: 99,
				store:       es.json.MiningTXs,
			},
		}
		for _, k := range kinds {
			txs, err := es.getTXsByAddress(es.address, k.endpoint)
			// TODO : add a way to return the error to main app
			if err == nil {
				for _, t := range txs {
					key := "hash"
					if k.endpoint == "getminedblocks" {
						key = "blockNumber"
					}
					k.store[t.(map[string]interface{})[key].(string)] = t
				}
				es.json.Addresses[es.address] = true
				j.Data, err = json.MarshalIndent(es.json, "", es.indent)
				if err == nil {
					jsons <- j
				}
			}
			progress <- k.progressEnd
		}
		progress <- 100
	}()
	return
}

func (es BscscanAPI) GetJsons(map[string]interface{}) error {
	return nil
}

func (es *BscscanAPI) getTXsByAddress(addr, kind string) (txs []interface{}, err error) {
	es.waitRateLimit()
	params := map[string]string{
		"module":  "account",
		"action":  kind,
		"address": addr,
		"apikey":  es.apiKey,
	}
	if kind == "getminedblocks" {
		params["blocktype"] = "blocks"
	} else {
		params["sort"] = "asc"
	}
	headers := map[string]string{
		"Accept": "application/json",
	}
	var res BscscanResult
	es.lastReqTime = time.Now()
	resp, err := es.client.R().
		SetQueryParams(params).
		SetHeaders(headers).
		SetResult(&res).
		Get(es.baseURL)
	if err != nil {
		return
	}
	if !resp.IsSuccess() {
		err = errors.New("Error Requesting Normal TX for " + addr + " " + strconv.Itoa(resp.StatusCode()))
		return
	}
	if res.Status == "0" {
		if res.Message == "NOTOK" {
			// Downgrade tier
			es.tier = ""
		}
		err = errors.New("Error Requesting Normal TX for " + addr + " " + res.Message)
		// err = errors.New("Error Requesting Normal TX for " + addr + " " + res.Result.(string))
		return
	}
	txs = res.Result.([]interface{})
	return
}

func (es BscscanAPI) waitRateLimit() {
	// tier == "" : 1 call every 5 seconds
	wait := 5500 * time.Millisecond
	if es.tier == "Free" {
		// 5 calls/second
		wait = 200 * time.Millisecond
	} else if es.tier == "Standard" {
		// 10 calls/second
		wait = 100 * time.Millisecond
	} else if es.tier == "Advanced" {
		// 20 calls/second
		wait = 50 * time.Millisecond
	} else if es.tier == "Professional" {
		// 30 calls/second
		wait = 34 * time.Millisecond
	}
	time.Sleep(wait - time.Since(es.lastReqTime))
}
