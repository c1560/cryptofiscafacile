package api

import (
	"crypto/hmac"
	"crypto/sha512"
	"encoding/hex"
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	resty "github.com/go-resty/resty/v2"
)

type BittrexError struct {
	Code   string      `json:"code"`
	Detail string      `json:"detail"`
	Data   interface{} `json:"data"`
}

type BittrexJSON struct {
	Deposits    map[string]interface{} `json:"deposits"`
	Trades      map[string]interface{} `json:"trades"`
	Withdrawals map[string]interface{} `json:"withdrawals"`
}

type BittrexAPI struct {
	debug      bool
	indent     string
	client     *resty.Client
	baseScheme string
	baseURL    string
	apiKey     string
	secretKey  string
	json       BittrexJSON
}

var Bittrex = BittrexAPI{
	baseScheme: "https://",
	baseURL:    "api.bittrex.com/v3",
}

func (api BittrexAPI) GetName() string {
	return "Bittrex"
}

func (api BittrexAPI) GetURL() string {
	return "https://bittrex.zendesk.com/hc/en-us/articles/360031921872-How-to-create-an-API-key-"
}

func (api BittrexAPI) GetPlateform() bool {
	return true
}

func (api BittrexAPI) NeedKey() bool {
	return true
}

func (api BittrexAPI) GetJSONName() string {
	return "Bittrex_API.json"
}

func (api BittrexAPI) GetJSONs(conf map[string]interface{}) (jsons chan JSONFile, progress chan int, errorChan chan error, err error) {
	if val, ok := conf["arg1"]; ok {
		api.apiKey = val.(string)
	} else {
		err = errors.New("missing apiKey")
		return
	}
	if val, ok := conf["arg2"]; ok {
		api.secretKey = val.(string)
	} else {
		err = errors.New("missing secretKey")
		return
	}
	if val, ok := conf["proxy"]; ok {
		api.baseScheme = val.(string)
	}
	if val, ok := conf["debug"]; ok {
		api.debug = val.(bool)
	}
	if val, ok := conf["indent"]; ok {
		api.indent = val.(string)
	}
	if val, ok := conf["json"]; ok {
		json.Unmarshal(val.([]byte), &api.json)
	}
	if api.json.Deposits == nil {
		api.json.Deposits = make(map[string]interface{})
	}
	if api.json.Trades == nil {
		api.json.Trades = make(map[string]interface{})
	}
	if api.json.Withdrawals == nil {
		api.json.Withdrawals = make(map[string]interface{})
	}
	if api.client == nil {
		api.client = resty.New()
		api.client.SetTransport(http.DefaultTransport)
		api.client.SetDebug(api.debug)
		api.client.SetDebugBodyLimit(100)
	}
	api.client.SetRetryCount(3)
	api.client.SetRetryWaitTime(6 * time.Second)
	jsons = make(chan JSONFile, 10)
	progress = make(chan int)
	errorChan = make(chan error)
	var j JSONFile
	j.Name = api.GetJSONName()
	go func() {
		progress <- 1
		type bittrexPrivate struct {
			endpoint      string
			progressStart int
			progressEnd   int
			store         map[string]interface{}
		}
		privates := []bittrexPrivate{
			bittrexPrivate{
				endpoint:      "/deposits/closed",
				progressStart: 0,
				progressEnd:   30,
				store:         api.json.Deposits,
			},
			bittrexPrivate{
				endpoint:      "/withdrawals/closed",
				progressStart: 30,
				progressEnd:   60,
				store:         api.json.Withdrawals,
			},
			bittrexPrivate{
				endpoint:      "/orders/closed",
				progressStart: 60,
				progressEnd:   100,
				store:         api.json.Trades,
			},
		}
		for _, p := range privates {
			if p.store["objects"] == nil {
				p.store["objects"] = make(map[string]interface{})
			}
			startDate := ""
			if p.store["startDate"] != nil {
				startDate = p.store["startDate"].(string)
			}
			firstObject := true
			lastObjectId := ""
			got := 200
			prog := p.progressStart
			for got == 200 {
				res, err := api.getPrivate(p.endpoint, lastObjectId, startDate)
				if err != nil {
					// TODO : add a way to return the error to main app
					return
				}
				got = len(res)
				for _, r := range res {
					lastObjectId = r.(map[string]interface{})["id"].(string)
					p.store["objects"].(map[string]interface{})[lastObjectId] = r
					if firstObject {
						firstObject = false
						useField := "completedAt"
						if p.endpoint == "/orders/closed" {
							useField = "closedAt"
						}
						p.store["startDate"] = strings.Split(r.(map[string]interface{})[useField].(string), ".")[0] + "Z"
					}
				}
				j.Data, err = json.MarshalIndent(api.json, "", api.indent)
				if err == nil {
					jsons <- j
				}
				prog += 5
				if prog > p.progressEnd-5 {
					prog = p.progressStart + 5
				}
				progress <- prog
			}
			progress <- p.progressEnd
		}
		progress <- 100
	}()
	return
}

func (api BittrexAPI) getPrivate(endpoint, nextPageToken, startDate string) (result []interface{}, err error) {
	resource := endpoint
	headers := map[string]string{
		"Accept":           "application/json",
		"Api-Content-Hash": api.hash([]byte{}), // should be the Request Body, but do only do GET
		"Api-Key":          api.apiKey,
		"Api-Timestamp":    strconv.FormatInt(time.Now().UTC().UnixMilli(), 10),
	}
	paramsEncoded := "?"
	if nextPageToken != "" {
		paramsEncoded += "nextPageToken=" + nextPageToken + "&"
	}
	paramsEncoded += "pageSize=200"
	if startDate != "" {
		paramsEncoded += "&startDate=" + startDate
	}
	if endpoint == "/deposits/closed" ||
		endpoint == "/withdrawals/closed" {
		paramsEncoded += "&status=COMPLETED"
	}
	headers["Api-Signature"] = api.sign(headers, api.baseURL+resource+paramsEncoded, "GET")
	params := map[string]string{
		"pageSize": "200",
	}
	if endpoint == "/deposits/closed" ||
		endpoint == "/withdrawals/closed" {
		params["status"] = "COMPLETED"
	}
	if nextPageToken != "" {
		params["nextPageToken"] = nextPageToken
	}
	if startDate != "" {
		params["startDate"] = startDate
	}
	time.Sleep(200 * time.Millisecond)
	var apiErr BittrexError
	u, _ := url.Parse(api.baseURL + resource)
	base, err := url.Parse(api.baseScheme)
	if err != nil {
		return nil, err
	}
	resp, err := api.client.R().
		SetQueryParams(params).
		SetHeaders(headers).
		SetError(&apiErr).
		SetResult(&result).
		Get(base.ResolveReference(u).String())
	if err != nil {
		return nil, err
	}
	if resp.StatusCode() == 429 {
		// If you receive a throttling error, back off for the remainder
		// of the minute and reduce the rate of subsequent requests.
		time.Sleep(time.Minute)
	}
	if apiErr.Code != "" || !resp.IsSuccess() {
		return nil, errors.New("Error Requesting " + endpoint + " " + apiErr.Code)
	}
	return
}

func (api BittrexAPI) hash(body []byte) string {
	sha_512 := sha512.New()
	sha_512.Write(body)
	return hex.EncodeToString(sha_512.Sum(nil))
}

func (api BittrexAPI) sign(headers map[string]string, url, method string) string {
	hmac512 := hmac.New(sha512.New, []byte(api.secretKey))
	preSignature := headers["Api-Timestamp"] + url + method + headers["Api-Content-Hash"]
	hmac512.Write([]byte(preSignature))
	return hex.EncodeToString(hmac512.Sum(nil))
}
