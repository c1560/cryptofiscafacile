package api

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"strings"
	"time"

	resty "github.com/go-resty/resty/v2"
)

type PolygonResult struct {
	Status  string      `json:"status"`
	Message string      `json:"message"`
	Result  interface{} `json:"result"`
}

type PolygonJSON struct {
	Addresses   map[string]bool        `json:"matic_addrs"`
	NormalTXs   map[string]interface{} `json:"normal_txs"`
	InternalTXs map[string]interface{} `json:"internal_txs"`
	TokenTXs    map[string]interface{} `json:"token_txs"`
	NftTXs      map[string]interface{} `json:"nft_txs"`
	MiningTXs   map[string]interface{} `json:"mining_txs"`
}

type PolygonAPI struct {
	debug       bool
	indent      string
	client      *resty.Client
	address     string
	baseURL     string
	apiKey      string
	lastReqTime time.Time
	tier        string
	json        PolygonJSON
}

var Polygon = PolygonAPI{
	baseURL: "https://api.polygonscan.com/api",
	// tier:    "Free",
}

func (ps PolygonAPI) GetName() string {
	return "Polygon"
}

func (ps PolygonAPI) GetURL() string {
	return "https://polygonscan.com/"
	// return "https://docs.polygonscan.com/getting-started/endpoint-urls"
}

func (ps PolygonAPI) GetPlateform() bool {
	return false
}

func (ps PolygonAPI) NeedKey() bool {
	return true
}

func (ps PolygonAPI) GetJSONName() string {
	return "Polygon.json"
}

func (ps PolygonAPI) WithChannels() bool {
	return true
}

func (ps PolygonAPI) GetEmptyJson() JSONFile {
	emptyJson := PolygonJSON{Addresses: make(map[string]bool)}
	emptyJsonFile := JSONFile{Name: "empty-polygon.json"}
	emptyJsonFile.Data, _ = json.MarshalIndent(emptyJson, "", ps.indent)

	return emptyJsonFile
}

func (ps PolygonAPI) GetJSONs(conf map[string]interface{}) (jsons chan JSONFile, progress chan int, errorChan chan error, err error) {
	if val, ok := conf["arg1"]; ok && val != "" {
		ps.address = strings.ToLower(val.(string))
	} else {
		err = errors.New("missing MATIC Address")
		return
	}
	if val, ok := conf["arg2"]; ok {
		ps.apiKey = val.(string)
		if ps.apiKey != "" {
			ps.tier = "Free"
		}
	}
	if val, ok := conf["arg3"]; ok && val != "" {
		ps.tier = val.(string)
	}
	if val, ok := conf["debug"]; ok {
		ps.debug = val.(bool)
	}
	if val, ok := conf["indent"]; ok {
		ps.indent = val.(string)
	}
	if val, ok := conf["json"]; ok {
		json.Unmarshal(val.([]byte), &ps.json)
	}
	if ps.json.Addresses == nil {
		ps.json.Addresses = make(map[string]bool)
	}
	if ps.json.NormalTXs == nil {
		ps.json.NormalTXs = make(map[string]interface{})
	}
	if ps.json.InternalTXs == nil {
		ps.json.InternalTXs = make(map[string]interface{})
	}
	if ps.json.TokenTXs == nil {
		ps.json.TokenTXs = make(map[string]interface{})
	}
	if ps.json.NftTXs == nil {
		ps.json.NftTXs = make(map[string]interface{})
	}
	if ps.json.MiningTXs == nil {
		ps.json.MiningTXs = make(map[string]interface{})
	}
	if ps.client == nil {
		ps.client = resty.New()
		ps.client.SetTransport(http.DefaultTransport)
		ps.client.SetDebug(ps.debug)
		ps.client.SetDebugBodyLimit(200)
	}
	ps.client.SetRetryCount(3)
	ps.client.SetRetryWaitTime(time.Second)
	jsons = make(chan JSONFile, 10)
	progress = make(chan int)
	errorChan = make(chan error)
	var j JSONFile
	j.Name = ps.GetJSONName()
	go func() {
		progress <- 1
		type polygonTX struct {
			endpoint    string
			progressEnd int
			store       map[string]interface{}
		}
		kinds := []polygonTX{
			polygonTX{
				endpoint:    "txlist",
				progressEnd: 20,
				store:       ps.json.NormalTXs,
			},
			polygonTX{
				endpoint:    "txlistinternal",
				progressEnd: 40,
				store:       ps.json.InternalTXs,
			},
			polygonTX{
				endpoint:    "tokentx",
				progressEnd: 60,
				store:       ps.json.TokenTXs,
			},
			polygonTX{
				endpoint:    "tokennfttx",
				progressEnd: 80,
				store:       ps.json.NftTXs,
			},
			polygonTX{
				endpoint:    "getminedblocks",
				progressEnd: 99,
				store:       ps.json.MiningTXs,
			},
		}
		for _, k := range kinds {
			txs, err := ps.getTXsByAddress(ps.address, k.endpoint)
			// TODO : add a way to return the error to main app
			if err == nil {
				for _, t := range txs {
					key := "hash"
					if k.endpoint == "getminedblocks" {
						key = "blockNumber"
					}
					k.store[t.(map[string]interface{})[key].(string)] = t
				}
				ps.json.Addresses[ps.address] = true
				j.Data, err = json.MarshalIndent(ps.json, "", ps.indent)
				if err == nil {
					jsons <- j
				}
			}
			progress <- k.progressEnd
		}
		progress <- 100
	}()
	return
}

func (ps PolygonAPI) GetJsons(map[string]interface{}) error {
	return nil
}

func (ps *PolygonAPI) getTXsByAddress(addr, kind string) (txs []interface{}, err error) {
	ps.waitRateLimit()
	params := map[string]string{
		"module":  "account",
		"action":  kind,
		"address": addr,
		"apikey":  ps.apiKey,
	}
	if kind == "getminedblocks" {
		params["blocktype"] = "blocks"
	} else {
		params["sort"] = "asc"
	}
	headers := map[string]string{
		"Accept": "application/json",
	}
	var res PolygonResult
	ps.lastReqTime = time.Now()
	resp, err := ps.client.R().
		SetQueryParams(params).
		SetHeaders(headers).
		SetResult(&res).
		Get(ps.baseURL)
	if err != nil {
		return
	}
	if !resp.IsSuccess() {
		err = errors.New("Error Requesting Normal TX for " + addr + " " + strconv.Itoa(resp.StatusCode()))
		return
	}
	if res.Status == "0" {
		if res.Message == "NOTOK" {
			// Downgrade tier
			ps.tier = ""
		}
		err = errors.New("Error Requesting Normal TX for " + addr + " " + res.Message)
		// err = errors.New("Error Requesting Normal TX for " + addr + " " + res.Result.(string))
		return
	}
	txs = res.Result.([]interface{})
	return
}

func (ps PolygonAPI) waitRateLimit() {
	// tier == "" : 1 call every 5 seconds
	wait := 5500 * time.Millisecond
	if ps.tier == "Free" {
		// 5 calls/second
		wait = 200 * time.Millisecond
	} else if ps.tier == "Standard" {
		// 10 calls/second
		wait = 100 * time.Millisecond
	} else if ps.tier == "Advanced" {
		// 20 calls/second
		wait = 50 * time.Millisecond
	} else if ps.tier == "Professional" {
		// 30 calls/second
		wait = 34 * time.Millisecond
	}
	time.Sleep(wait - time.Since(ps.lastReqTime))
}
