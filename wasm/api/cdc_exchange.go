package api

import (
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"sort"
	"time"

	"github.com/davecgh/go-spew/spew"
	"nhooyr.io/websocket"
	"nhooyr.io/websocket/wsjson"
)

type CdCExJSON struct {
	Trades      map[string]interface{} `json:"trades"`
	LastTradeTS int64                  `json:"last_trade_ts"`
}

type CdCExAPI struct {
	debug     bool
	indent    string
	baseURL   string
	apiKey    string
	secretKey string
	wsID      int64
	json      CdCExJSON
}

var CdCEx = CdCExAPI{
	baseURL: "wss://stream.crypto.com/v2",
}

func (cdcex CdCExAPI) GetName() string {
	return "Crypto.com Exchange"
}

func (cdcex CdCExAPI) GetURL() string {
	return "https://exchange-docs.crypto.com/spot/index.html#introduction"
}

func (cdcex CdCExAPI) GetPlateform() bool {
	return true
}

func (cdcex CdCExAPI) NeedKey() bool {
	return true
}

func (cdcex CdCExAPI) GetJSONName() string {
	return "CdC_Exchange_API.json"
}

func (cdcex CdCExAPI) WithChannels() bool {
	return true
}

type CdCExWSRequest struct {
	ID     int64                  `json:"id,omitempty"`
	Method string                 `json:"method"`
	Params map[string]interface{} `json:"params,omitempty"`
	ApiKey string                 `json:"api_key,omitempty"`
	Sig    string                 `json:"sig,omitempty"`
	Nonce  int64                  `json:"nonce"`
}

func (req *CdCExWSRequest) sign(secretKey string) {
	keys := make([]string, 0, len(req.Params))
	for key := range req.Params {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	paramString := ""
	for _, keySorted := range keys {
		paramString += keySorted + fmt.Sprintf("%v", req.Params[keySorted])
	}
	sigPayload := fmt.Sprintf("%v%v%s%s%v", req.Method, req.ID, req.ApiKey, paramString, req.Nonce)
	mac := hmac.New(sha256.New, []byte(secretKey))
	mac.Write([]byte(sigPayload))
	req.Sig = hex.EncodeToString(mac.Sum(nil))
}

type CdCExWSResponse struct {
	ID       int64       `json:"id"`
	Method   string      `json:"method"`
	Result   interface{} `json:"result"`
	Code     int         `json:"code"`
	Message  string      `json:"message,omitempty"`
	Original string      `json:"original,omitempty"`
}

func (cdcex CdCExAPI) GetEmptyJson() JSONFile {
	emptyJson := CdCExJSON{Trades: make(map[string]interface{}), LastTradeTS: 1}
	emptyJsonFile := JSONFile{Name: "empty-cdc-exchange-api.json"}
	emptyJsonFile.Data, _ = json.MarshalIndent(emptyJson, "", cdcex.indent)

	return emptyJsonFile
}

func (cdcex CdCExAPI) GetJSONs(conf map[string]interface{}) (jsons chan JSONFile, progressBar chan int, errorChan chan error, err error) {
	if val, ok := conf["arg1"]; ok {
		cdcex.apiKey = val.(string)
	} else {
		err = errors.New("missing apiKey")
		return
	}
	if val, ok := conf["arg2"]; ok {
		cdcex.secretKey = val.(string)
	} else {
		err = errors.New("missing secretKey")
		return
	}
	if val, ok := conf["debug"]; ok {
		cdcex.debug = val.(bool)
	}
	if val, ok := conf["indent"]; ok {
		cdcex.indent = val.(string)
	}
	if val, ok := conf["json"]; ok {
		json.Unmarshal(val.([]byte), &cdcex.json)
	}
	if cdcex.json.Trades == nil {
		cdcex.json.Trades = make(map[string]interface{})
		// Date of CdC Exchange Launch
		cdcex.json.LastTradeTS = time.Date(2019, time.November, 14, 0, 0, 0, 0, time.UTC).UnixMilli()
	}

	return cdcex.fetchCdcExJsons()
}

func (cdcex CdCExAPI) fetchCdcExJsons() (jsons chan JSONFile, progressBar chan int, errorChan chan error, err error) {
	cdcexCtx, cancel := context.WithTimeout(context.Background(), time.Hour)
	wsUser, _, err := websocket.Dial(cdcexCtx, cdcex.baseURL+"/user", nil)
	if err != nil {
		if cdcex.debug {
			spew.Dump("Dial:user", err)
		}
		cancel()
		return
	}
	cdcex.wsID++
	// We recommend adding a 1-second sleep after establishing the websocket connection, and before requests are sent.
	// This will avoid occurrences of rate-limit (`TOO_MANY_REQUESTS`) errors,
	// as the websocket rate limits are pro-rated based on the calendar-second that the websocket connection was opened.
	time.Sleep(time.Second)
	req := CdCExWSRequest{
		ID:     cdcex.wsID,
		Method: "public/auth",
		ApiKey: cdcex.apiKey,
		Nonce:  time.Now().Add(-time.Second * 5).UnixMilli(),
	}
	req.sign(cdcex.secretKey)
	err = wsjson.Write(cdcexCtx, wsUser, req)
	if err != nil {
		if cdcex.debug {
			spew.Dump("Write:user:public/auth", err)
		}
		wsUser.Close(websocket.StatusNormalClosure, "")
		cancel()
		return
	}
	var wsResp CdCExWSResponse
	wsResp, err = cdcex.readCdcExMessage(cdcexCtx, wsUser)
	if wsResp.Code > 0 || err != nil {
		if cdcex.debug {
			spew.Dump("Read:user:public/auth", err)
		}
		if wsResp.Code > 0 {
			err = errors.New(wsResp.Message)
		}
		wsUser.Close(websocket.StatusNormalClosure, "")
		cancel()
		return
	}

	jsons = make(chan JSONFile, 10)
	progressBar = make(chan int)
	errorChan = make(chan error)
	var j JSONFile
	j.Name = cdcex.GetJSONName()
	go func() {
		defer cancel()
		defer wsUser.Close(websocket.StatusNormalClosure, "")
		beginTS := cdcex.json.LastTradeTS
		startTS := beginTS
		yesterdayTS := time.Now().Add(-24 * time.Hour).UnixMilli()
		for startTS < yesterdayTS {
			cdcex.wsID++
			pageSize := 200
			page := 0
			got := pageSize
			for got == pageSize {
				// private/get-trades and private/get-order-history is rate limited
				// at 5 requests per second on the Websocket
				time.Sleep(210 * time.Millisecond)
				err = wsjson.Write(cdcexCtx, wsUser, CdCExWSRequest{
					ID:     cdcex.wsID,
					Method: "private/get-trades",
					Params: map[string]interface{}{
						"start_ts":  startTS,
						"end_ts":    startTS + 24*time.Hour.Milliseconds() - 1,
						"page_size": pageSize,
						"page":      page,
					},
					Nonce: time.Now().UnixMilli(),
				})
				if err != nil {
					if cdcex.debug {
						spew.Dump("Write:private/get-trades", err)
					}
					break
				}
				wsResp, err = cdcex.readCdcExMessage(cdcexCtx, wsUser)
				if err != nil {
					if cdcex.debug {
						spew.Dump("Read:", err)
					}
					break
				}
				if wsResp.Code > 0 {
					if cdcex.debug {
						spew.Dump("Read:"+wsResp.Method, wsResp)
					}
					break
				}
				if wsResp.Result != nil {
					res := wsResp.Result.(map[string]interface{})
					tradeList := res["trade_list"].([]interface{})
					got = len(tradeList)
					if got == 0 {
						cdcex.json.LastTradeTS = startTS
					} else {
						for _, trade := range tradeList {
							tradeID := trade.(map[string]interface{})["trade_id"].(string)
							cdcex.json.Trades[tradeID] = trade
							tradeTS := int64(trade.(map[string]interface{})["create_time"].(float64))
							if tradeTS > cdcex.json.LastTradeTS {
								cdcex.json.LastTradeTS = tradeTS
							}
						}
					}
					j.Data, err = json.MarshalIndent(cdcex.json, "", cdcex.indent)
					if err == nil {
						jsons <- j
					}
					page++
				}
			}
			startTS += 24 * time.Hour.Milliseconds()
			progress := 100
			if wsResp.Code > 0 {
				errorChan <- errors.New(wsResp.Message)
				startTS = yesterdayTS
			} else if yesterdayTS != beginTS {
				progress = int((startTS - beginTS) * 100 / (yesterdayTS - beginTS))
				if progress > 100 {
					progress = 100
				}
			}
			progressBar <- progress
		}
	}()
	return
}

func (cdcex CdCExAPI) readCdcExMessage(cdcexCtx context.Context, wsUser *websocket.Conn) (wsResp CdCExWSResponse, err error) {
	err = wsjson.Read(cdcexCtx, wsUser, &wsResp)
	if err != nil {
		if cdcex.debug {
			spew.Dump("Read:", err)
		}
		return
	}
	if wsResp.Code > 0 {
		if cdcex.debug {
			spew.Dump("Read:"+wsResp.Method, wsResp)
		}
		return
	}
	if wsResp.Method == "public/heartbeat" {
		err = wsjson.Write(cdcexCtx, wsUser, CdCExWSRequest{
			ID:     wsResp.ID,
			Method: "public/respond-heartbeat",
		})
		if err != nil {
			if cdcex.debug {
				spew.Dump("Write:public/respond-heartbeat", err)
			}
			return
		}
		return cdcex.readCdcExMessage(cdcexCtx, wsUser)
	}
	return
}

func (cdcex CdCExAPI) GetJsons(map[string]interface{}) error {
	return nil
}
