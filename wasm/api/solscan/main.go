package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.com/c1560/CryptoFiscaFacile/wasm/api"
)

func main() {
	pAddr := flag.String("addr", "", "Solana account")
	pOut := flag.String("out", "", "Output Directory")
	pDebug := flag.Bool("debug", false, "Debug")
	flag.Parse()
	conf := make(map[string]interface{})
	conf["arg1"] = *pAddr
	conf["indent"] = "  "
	conf["debug"] = *pDebug
	fileData, err := os.ReadFile(*pOut + api.Solscan.GetJSONName())
	if err == nil {
		conf["json"] = fileData
	}
	jsons, progress, _, err := api.Solscan.GetJSONs(conf)
	if err != nil {
		log.Fatalln(err.Error())
	}
	for {
		select {
		case p := <-progress:
			fmt.Print(p, ".")
			if p >= 100 {
				return
			}
		case j := <-jsons:
			err := os.WriteFile(*pOut+j.Name, j.Data, 0666)
			if err != nil {
				log.Println(err.Error())
			}
		}
	}
}
