package rate

import (
	"errors"
	"log"
	"strings"
	"sync"
	"time"

	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain/entities"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain/enums"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/external"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/external/rates/coingecko"
)

type coinGeckoAPI struct {
	mutex        sync.Mutex
	reqTime      []time.Time
	CoinsSuccess map[string]bool
}

var cg = coinGeckoAPI{}

// GetCoinGecko : get CoinGecko rate
func GetCoinGecko(coin string) error {
	// prevent multiple requests of same coin
	cg.mutex.Lock()
	defer cg.mutex.Unlock()
	if cg.CoinsSuccess == nil {
		cg.CoinsSuccess = make(map[string]bool)
	}
	if success, exists := cg.CoinsSuccess[coin]; exists {
		if success {
			return errors.New("already got a 200")
		}
		return errors.New("already got a 404")
	}
	coinList, err := cff.GetCoinList(enums.RateProviderCoingecko)
	if err != nil {
		needRequest[coin] = true
		return err
	}
	if coin == "" {
		return errors.New("coin empty")
	}
	coinId := getCoinId(coinList, coin)
	marketChart, err := cff.GetCoingeckoClient().CoinsIDMarketChart(coinId)
	if err != nil {
		var restErr *external.RestClientError
		if errors.As(err, &restErr) {
			if restErr.StatusCode == 404 { // Not Found
				// if we get a 404, no need to ask anymore, CG does not have it
				cg.CoinsSuccess[coin] = false
			}
			if restErr.StatusCode == 429 ||
				// 429 requests are mostly blocked by cors
				strings.Contains(err.Error(), "fetch() failed: Failed to fetch") { // Too Many Requests
				// if we get a 429, wait before retry again
				log.Println("429 happened, retry in 60 seconds")
				time.Sleep(60 * time.Second)
				needRequest[coin] = true
			}
		}
		return err
	}
	cg.CoinsSuccess[coin] = true
	if marketChart.Prices != nil {
		for _, p := range *marketChart.Prices {
			Set(coin, time.Unix(int64(p[0]/1000), 0).UTC(), decimal.NewFromFloat(float64(p[1])))
		}
		if cffrToSave != nil {
			cffrToSave <- coin
		}
	}
	return nil
}

func GetCoinDetail(symbol string) (detail *coingecko.CoinsID, err error) {
	coinList, err := cff.GetCoinList(enums.RateProviderCoingecko)
	if err != nil {
		return detail, err
	}

	return cff.GetCoingeckoClient().CoinsID(getCoinId(coinList, symbol), false, false, true, false, false, false)
}

func getCoinId(coinList *entities.CoinList, symbol string) (coinId string) {
	coinId = forceCoinId(symbol)
	if coinId != "" {
		return coinId
	}

	if coinList != nil {
		for _, c := range coinList.Coins {
			if (c.ID == strings.ToLower(symbol) ||
				c.Symbol == strings.ToLower(symbol) ||
				c.Symbol == symbol) &&
				!strings.Contains(c.ID, "-peg-") {
				coinId = c.ID
				break
			}
		}
	}
	return coinId
}

/** Some coins have same ticker, replace it with coin id */
func forceCoinId(ticker string) string {
	tickers := make(map[string]string)
	tickers["BEST"] = "bitpanda-ecosystem-token"
	tickers["BETH"] = "binance-eth"
	tickers["BTC"] = "bitcoin"
	tickers["COMP"] = "compound-governance-token"
	tickers["CRO"] = "crypto-com-chain"
	tickers["DAI"] = "dai"
	tickers["DATA"] = "streamr"
	tickers["DSH"] = "dash"
	tickers["EDO"] = "pnt"
	tickers["EFI"] = "efinity"
	tickers["ETH"] = "ethereum"
	tickers["GRT"] = "the-graph"
	tickers["GXS"] = "gxchain"
	tickers["IOT"] = "miota"
	tickers["LIT"] = "litentry"
	tickers["MEX"] = "maiar-dex"
	tickers["OMG"] = "omisego"
	tickers["LUNA"] = "terra-luna"
	tickers["MEET.ONE"] = "meetone"
	tickers["SC"] = "siacoin"
	tickers["SYL"] = "xsl-labs"
	tickers["USDC"] = "usd-coin"
	tickers["USDT"] = "tether"
	tickers["XLM"] = "stellar"
	tickers["XRP"] = "ripple"

	if coinId, hasCode := tickers[ticker]; hasCode {
		return coinId
	}
	return ""
}
