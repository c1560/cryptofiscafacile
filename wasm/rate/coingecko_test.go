package rate

import (
	"context"
	"testing"

	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/app"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/domain/enums"
)

func Test_getCoinGecko(t *testing.T) {
	type args struct {
		coin string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "Simple BTC",
			args: args{
				coin: "BTC",
			},
		},
	}
	cff = app.NewCff(context.Background())
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			GetCoinGecko(tt.args.coin)
		})
	}
}

func Test_getCoinId(t *testing.T) {
	type args struct {
		coin string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "USDT",
			args: args{
				coin: "USDT",
			},
			want: "tether",
		},
		{
			name: "SOL",
			args: args{
				coin: "SOL",
			},
			want: "solana",
		},
	}
	cff = app.NewCff(context.Background())
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			coinList, err := cff.GetCoinList(enums.RateProviderCoingecko)
			if err != nil {
				t.Error(err)
				return
			}
			coinId := getCoinId(coinList, tt.args.coin)
			if coinId != tt.want {
				t.Errorf("got %q, wanted %q", coinId, tt.want)
			}
		})
	}
}
