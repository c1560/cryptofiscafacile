package main

import (
	"flag"
	"log"
	"os"

	"gitlab.com/c1560/CryptoFiscaFacile/wasm/rate"
)

func main() {
	pFilename := flag.String("file", "", "CFF Rate file")
	pCode := flag.String("code", "", "Curency Code")
	flag.Parse()
	if *pCode == "" {
		log.Fatal("Code must be given")
	}
	filename := *pCode + ".cffr"
	if *pFilename != "" {
		filename = *pFilename
	}
	data, err := os.ReadFile(filename)
	if err == nil {
		log.Println("Found cffr file, parse it...")
		rate.ParseCFFR(data)
	}
	rate.GetCoinGecko(*pCode)
	os.WriteFile(filename, rate.ExportCFFR(*pCode), 0666)
}
