module gitlab.com/c1560/CryptoFiscaFacile/wasm/rate/coingecko_leecher

go 1.19

require (
	gitlab.com/c1560/CryptoFiscaFacile/wasm/rate v0.0.0-00010101000000-000000000000
	golang.org/x/net v0.0.0-20211029224645-99673261e6eb // indirect
)

require (
	github.com/go-resty/resty/v2 v2.7.0 // indirect
	github.com/shopspring/decimal v1.3.1 // indirect
)

replace gitlab.com/c1560/CryptoFiscaFacile/wasm/rate => ../
