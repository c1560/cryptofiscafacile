package rate

import (
	"encoding/xml"
	"errors"
	"net/http"
	"sync"
	"time"

	resty "github.com/go-resty/resty/v2"
	"github.com/shopspring/decimal"
)

type europeanCentralBankAPI struct {
	mutex   sync.Mutex
	client  *resty.Client
	baseURL string
}

var ecb = europeanCentralBankAPI{
	baseURL: "https://sdw-wsrest.ecb.europa.eu/service",
}

func isFiatEuropeanCentralBank(code string) bool {
	return code == "EUR" ||
		code == "USD" ||
		code == "JPY" ||
		code == "BGN" ||
		code == "CYP" ||
		code == "CZK" ||
		code == "DKK" ||
		code == "EEK" ||
		code == "GBP" ||
		code == "HUF" ||
		code == "LTL" ||
		code == "LVL" ||
		code == "MTL" ||
		code == "PLN" ||
		code == "ROL" ||
		code == "RON" ||
		code == "SEK" ||
		code == "SIT" ||
		code == "SKK" ||
		code == "CHF" ||
		code == "ISK" ||
		code == "NOK" ||
		code == "HRK" ||
		code == "RUB" ||
		code == "TRL" ||
		code == "TRY" ||
		code == "AUD" ||
		code == "BRL" ||
		code == "CAD" ||
		code == "CNY" ||
		code == "HKD" ||
		code == "IDR" ||
		code == "ILS" ||
		code == "INR" ||
		code == "KRW" ||
		code == "MXN" ||
		code == "MYR" ||
		code == "NZD" ||
		code == "PHP" ||
		code == "SGD" ||
		code == "THB" ||
		code == "ZAR"
}

// SDMXGenericData : Response from ECB API
type SDMXGenericData struct {
	XMLName        xml.Name `xml:"GenericData"`
	Text           string   `xml:",chardata"`
	Message        string   `xml:"message,attr"`
	Common         string   `xml:"common,attr"`
	Xsi            string   `xml:"xsi,attr"`
	Generic        string   `xml:"generic,attr"`
	SchemaLocation string   `xml:"schemaLocation,attr"`
	Header         struct {
		Text     string `xml:",chardata"`
		ID       string `xml:"ID"`
		Test     string `xml:"Test"`
		Prepared string `xml:"Prepared"`
		Sender   struct {
			Text string `xml:",chardata"`
			ID   string `xml:"id,attr"`
		} `xml:"Sender"`
		Structure struct {
			Text                   string `xml:",chardata"`
			StructureID            string `xml:"structureID,attr"`
			DimensionAtObservation string `xml:"dimensionAtObservation,attr"`
			Structure              struct {
				Text string `xml:",chardata"`
				URN  string `xml:"URN"`
			} `xml:"Structure"`
		} `xml:"Structure"`
	} `xml:"Header"`
	DataSet struct {
		Text          string `xml:",chardata"`
		Action        string `xml:"action,attr"`
		ValidFromDate string `xml:"validFromDate,attr"`
		StructureRef  string `xml:"structureRef,attr"`
		Series        struct {
			Text      string `xml:",chardata"`
			SeriesKey struct {
				Text  string `xml:",chardata"`
				Value []struct {
					Text  string `xml:",chardata"`
					ID    string `xml:"id,attr"`
					Value string `xml:"value,attr"`
				} `xml:"Value"`
			} `xml:"SeriesKey"`
			Attributes struct {
				Text  string `xml:",chardata"`
				Value []struct {
					Text  string `xml:",chardata"`
					ID    string `xml:"id,attr"`
					Value string `xml:"value,attr"`
				} `xml:"Value"`
			} `xml:"Attributes"`
			Obs []struct {
				Text         string `xml:",chardata"`
				ObsDimension struct {
					Text  string `xml:",chardata"`
					Value string `xml:"value,attr"`
				} `xml:"ObsDimension"`
				ObsValue struct {
					Text  string `xml:",chardata"`
					Value string `xml:"value,attr"`
				} `xml:"ObsValue"`
				Attributes struct {
					Text  string `xml:",chardata"`
					Value []struct {
						Text  string `xml:",chardata"`
						ID    string `xml:"id,attr"`
						Value string `xml:"value,attr"`
					} `xml:"Value"`
				} `xml:"Attributes"`
			} `xml:"Obs"`
		} `xml:"Series"`
	} `xml:"DataSet"`
}

// GetEuropeanCentralBank : get European Central Bank EUR rate
func GetEuropeanCentralBank(code string) error {
	if !isFiatEuropeanCentralBank(code) {
		return errors.New("")
	}
	ecb.mutex.Lock()
	defer ecb.mutex.Unlock()
	if ecb.client == nil {
		ecb.client = resty.New()
		ecb.client.SetTransport(http.DefaultTransport)
		ecb.client.SetRetryCount(3)
		// ecb.client.SetDebug(true)
		// ecb.client.SetDebugBodyLimit(100)
	}
	resource := "/data/ECB,EXR,1.0/D." + code + ".EUR.SP00.A"
	headers := map[string]string{
		"Accept": "application/vnd.sdmx.genericdata+xml",
	}
	queryParams := map[string]string{
		"startPeriod": "2010-01-01",
	}
	var data SDMXGenericData
	_, err := ecb.client.R().
		SetHeaders(headers).
		SetQueryParams(queryParams).
		SetResult(&data).
		Get(ecb.baseURL + resource)
	if err != nil {
		needRequest[code] = true
		return err
	}
	for _, obs := range data.DataSet.Series.Obs {
		date, err := time.Parse("2006-01-02", obs.ObsDimension.Value)
		if err == nil {
			rate, err := decimal.NewFromString(obs.ObsValue.Value)
			if err == nil &&
				!rate.IsZero() {
				Set(code, date, decimal.New(1, 0).Div(rate))
			}
		}
	}
	if cffrToSave != nil {
		cffrToSave <- code
	}
	return nil
}
