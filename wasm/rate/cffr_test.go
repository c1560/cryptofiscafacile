package rate

import (
	"reflect"
	"testing"
)

func TestParseExportCFFR(t *testing.T) {
	type args struct {
		data []byte
		code string
	}
	tests := []struct {
		name         string
		args         args
		wantErr      bool
		wantExported []byte
	}{
		{
			name: "Empty",
			args: args{
				data: []byte{},
			},
			wantErr:      true,
			wantExported: []byte{'$', 'C', 'F', 'F', 'R', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		},
		{
			name: "Too Short",
			args: args{
				data: []byte{'$', 'C', 'F', 'F', 'R', 'B', 'T', 'C', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			},
			wantErr:      true,
			wantExported: []byte{'$', 'C', 'F', 'F', 'R', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		},
		{
			name: "Wrong Magic",
			args: args{
				data: []byte{'%', 'C', 'F', 'F', 'R', 'B', 'T', 'C', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				code: "BTC",
			},
			wantErr:      true,
			wantExported: []byte{'$', 'C', 'F', 'F', 'R', 'B', 'T', 'C', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		},
		{
			name: "No Rates",
			args: args{
				data: []byte{'$', 'C', 'F', 'F', 'R', 'B', 'T', 'C', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				code: "BTC",
			},
			wantErr:      false,
			wantExported: []byte{'$', 'C', 'F', 'F', 'R', 'B', 'T', 'C', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		},
		{
			name: "Set 1",
			args: args{
				data: []byte{'$', 'C', 'F', 'F', 'R', 'B', 'T', 'C', 0, 0, 0, 0, 0, 0, 0xbb, 3, 0x5f, 0, 0, 0, 0, 1, 0, 0x9a, 0x99, 0x99, 0x99, 0x99, 0x99, 0x28, 0x40},
				code: "BTC",
			},
			wantErr:      false,
			wantExported: []byte{'$', 'C', 'F', 'F', 'R', 'B', 'T', 'C', 0, 0, 0, 0, 0, 0, 0xbb, 3, 0x5f, 0, 0, 0, 0, 1, 0, 0x9a, 0x99, 0x99, 0x99, 0x99, 0x99, 0x28, 0x40},
		},
		{
			name: "Set 2 with 1 empty slot",
			args: args{
				data: []byte{'$', 'C', 'F', 'F', 'R', 'B', 'T', 'C', 0, 0, 0, 0, 0, 0, 0xbb, 3, 0x5f, 0, 0, 0, 0, 3, 0, 0x9a, 0x99, 0x99, 0x99, 0x99, 0x99, 0x28, 0x40, 0x9a, 0, 0, 0, 0, 0, 0, 0, 0, 0x99, 0x99, 0x99, 0x99, 0x99, 0x28, 0x40},
				code: "BTC",
			},
			wantErr:      false,
			wantExported: []byte{'$', 'C', 'F', 'F', 'R', 'B', 'T', 'C', 0, 0, 0, 0, 0, 0, 0xbb, 3, 0x5f, 0, 0, 0, 0, 3, 0, 0x9a, 0x99, 0x99, 0x99, 0x99, 0x99, 0x28, 0x40, 0x9a, 0, 0, 0, 0, 0, 0, 0, 0, 0x99, 0x99, 0x99, 0x99, 0x99, 0x28, 0x40},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Flush()
			if _, err := ParseCFFR(tt.args.data); (err != nil) != tt.wantErr {
				t.Errorf("Parse() error = %v, wantErr %v", err, tt.wantErr)
			}
			if exported := ExportCFFR(tt.args.code); !reflect.DeepEqual(exported, tt.wantExported) {
				t.Errorf("Export() exported = %v, wantExported %v", exported, tt.wantExported)
			}
		})
	}
}

func TestExportAllCFFR(t *testing.T) {
	type args struct {
		data []byte
	}
	tests := []struct {
		name           string
		args           args
		wantErr        bool
		wantCoinsRates []CoinRates
	}{
		{
			name:    "Empty",
			wantErr: true,
			// wantCoinsRates: []CoinRates{},
		},
		{
			name: "No Rates",
			args: args{
				data: []byte{'$', 'C', 'F', 'F', 'R', 'B', 'T', 'C', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			},
			wantErr: false,
			wantCoinsRates: []CoinRates{
				{
					Coin: "BTC",
					CFFR: []byte{'$', 'C', 'F', 'F', 'R', 'B', 'T', 'C', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Flush()
			if _, err := ParseCFFR(tt.args.data); (err != nil) != tt.wantErr {
				t.Errorf("Parse() error = %v, wantErr %v", err, tt.wantErr)
			}
			if gotCoinsRates := ExportAllCFFR(); !reflect.DeepEqual(gotCoinsRates, tt.wantCoinsRates) {
				t.Errorf("ExportAllCFFR() = %v, want %v", gotCoinsRates, tt.wantCoinsRates)
			}
		})
	}
}
