module gitlab.com/c1560/CryptoFiscaFacile/wasm/rate/ecb_leecher

go 1.19

replace gitlab.com/c1560/CryptoFiscaFacile/wasm/rate => ../

require (
	github.com/go-resty/resty/v2 v2.13.1
	github.com/shopspring/decimal v1.4.0
	gitlab.com/c1560/CryptoFiscaFacile/wasm/rate v0.0.0-00010101000000-000000000000
)

require golang.org/x/net v0.0.0-20211029224645-99673261e6eb // indirect
