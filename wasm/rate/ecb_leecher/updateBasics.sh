#!/bin/bash

for CODE in CAD CHF CNY GBP HKD JPY USD
do
  ./ecb_leecher.exe -code $CODE
  mv $CODE.cffr ../../../frontend/public/rates/
done