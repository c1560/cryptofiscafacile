package main

import (
	"encoding/xml"
	"flag"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	resty "github.com/go-resty/resty/v2"
	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/rate"
)

// SDMXGenericData : Response from ECB API
type SDMXGenericData struct {
	XMLName        xml.Name `xml:"GenericData"`
	Text           string   `xml:",chardata"`
	Message        string   `xml:"message,attr"`
	Common         string   `xml:"common,attr"`
	Xsi            string   `xml:"xsi,attr"`
	Generic        string   `xml:"generic,attr"`
	SchemaLocation string   `xml:"schemaLocation,attr"`
	Header         struct {
		Text     string `xml:",chardata"`
		ID       string `xml:"ID"`
		Test     string `xml:"Test"`
		Prepared string `xml:"Prepared"`
		Sender   struct {
			Text string `xml:",chardata"`
			ID   string `xml:"id,attr"`
		} `xml:"Sender"`
		Structure struct {
			Text                   string `xml:",chardata"`
			StructureID            string `xml:"structureID,attr"`
			DimensionAtObservation string `xml:"dimensionAtObservation,attr"`
			Structure              struct {
				Text string `xml:",chardata"`
				URN  string `xml:"URN"`
			} `xml:"Structure"`
		} `xml:"Structure"`
	} `xml:"Header"`
	DataSet struct {
		Text          string `xml:",chardata"`
		Action        string `xml:"action,attr"`
		ValidFromDate string `xml:"validFromDate,attr"`
		StructureRef  string `xml:"structureRef,attr"`
		Series        struct {
			Text      string `xml:",chardata"`
			SeriesKey struct {
				Text  string `xml:",chardata"`
				Value []struct {
					Text  string `xml:",chardata"`
					ID    string `xml:"id,attr"`
					Value string `xml:"value,attr"`
				} `xml:"Value"`
			} `xml:"SeriesKey"`
			Attributes struct {
				Text  string `xml:",chardata"`
				Value []struct {
					Text  string `xml:",chardata"`
					ID    string `xml:"id,attr"`
					Value string `xml:"value,attr"`
				} `xml:"Value"`
			} `xml:"Attributes"`
			Obs []struct {
				Text         string `xml:",chardata"`
				ObsDimension struct {
					Text  string `xml:",chardata"`
					Value string `xml:"value,attr"`
				} `xml:"ObsDimension"`
				ObsValue struct {
					Text  string `xml:",chardata"`
					Value string `xml:"value,attr"`
				} `xml:"ObsValue"`
				Attributes struct {
					Text  string `xml:",chardata"`
					Value []struct {
						Text  string `xml:",chardata"`
						ID    string `xml:"id,attr"`
						Value string `xml:"value,attr"`
					} `xml:"Value"`
				} `xml:"Attributes"`
			} `xml:"Obs"`
		} `xml:"Series"`
	} `xml:"DataSet"`
}

func main() {
	pFilename := flag.String("file", "", "CFF Rate file")
	pCode := flag.String("code", "", "Currency Code")
	flag.Parse()
	if *pCode == "" {
		log.Fatal("Code must be given")
	}
	filename := *pCode + ".cffr"
	if *pFilename != "" {
		filename = *pFilename
	}
	data, err := os.ReadFile(filename)
	if err == nil {
		log.Println("Found cffr file, parse it...")
		rate.ParseCFFR(data)
	}

	code := strings.ReplaceAll(*pCode, "DSH", "dash") // Use less but keep as demo

	client := resty.New()
	client.SetTransport(http.DefaultTransport)
	client.SetRetryCount(3)

	resource := "/data/ECB,EXR,1.0/D." + code + ".EUR.SP00.A"
	headers := map[string]string{
		"Accept": "application/vnd.sdmx.genericdata+xml",
	}
	queryParams := map[string]string{
		"startPeriod": "2010-01-01",
	}
	var genData SDMXGenericData
	resp, err := client.R().
		SetHeaders(headers).
		SetQueryParams(queryParams).
		SetResult(&genData).
		Get("https://sdw-wsrest.ecb.europa.eu/service" + resource)
	if err != nil {
		log.Println(err.Error())
		return
	}
	if resp.IsSuccess() {
		for _, obs := range genData.DataSet.Series.Obs {
			date, err := time.Parse("2006-01-02", obs.ObsDimension.Value)
			if err == nil {
				r, err := decimal.NewFromString(obs.ObsValue.Value)
				if err == nil &&
					!r.IsZero() {
					rate.Set(code, date, decimal.New(1, 0).Div(r))
				}
			}
		}
	}

	os.WriteFile(filename, rate.ExportCFFR(*pCode), 0666)
}
