package rate

import (
	"errors"
	"net/http"
	"sync"
	"time"

	resty "github.com/go-resty/resty/v2"
	"github.com/shopspring/decimal"
)

type coinLayerAPI struct {
	mutex     sync.Mutex
	client    *resty.Client
	baseURL   string
	accessKey string
}

type historicalData struct {
	Success bool `json:"success"`
	Error   struct {
		Code int    `json:"code"`
		Type string `json:"type"`
		Info string `json:"info"`
	} `json:"error"`
	Terms      string             `json:"terms"`
	Privacy    string             `json:"privacy"`
	Timestamp  int                `json:"timestamp"`
	Target     string             `json:"target"`
	Historical bool               `json:"historical"`
	Date       string             `json:"date"`
	Rates      map[string]float64 `json:"rates"`
}

var cl = coinLayerAPI{baseURL: "http://api.coinlayer.com"}

// GetCoinLayer : get CoinLayer rate
func GetCoinLayer(coin string, date time.Time) error {
	cl.mutex.Lock()
	defer cl.mutex.Unlock()
	if cl.client == nil {
		cl.client = resty.New()
		cl.client.SetTransport(http.DefaultTransport)
		cl.client.SetRetryCount(3)
		// cl.client.SetDebug(true)
	}
	if coin == "" {
		return errors.New("coin empty")
	}
	if cl.accessKey == "" {
		return errors.New("no Access Key")
	}
	result := historicalData{}
	resource := "/" + date.Format("2006-01-02")
	headers := make(map[string]string)
	// headers["Content-Type"] = "application/json"
	headers["Accept"] = "application/json"
	queryParams := map[string]string{
		"access_key": cl.accessKey,
		"target":     "EUR",
	}
	_, err := cl.client.R().
		SetHeaders(headers).
		SetQueryParams(queryParams).
		SetResult(&result).
		Get(cl.baseURL + resource)
	if err != nil {
		return err
	}
	if !result.Success {
		return errors.New(result.Error.Info)
	}
	Set(coin, date, decimal.NewFromFloat(result.Rates[coin]))
	if cffrToSave != nil {
		cffrToSave <- coin
	}
	return nil
}
