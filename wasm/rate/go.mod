module gitlab.com/c1560/CryptoFiscaFacile/wasm/rate

go 1.19

require (
	github.com/go-resty/resty/v2 v2.13.1
	github.com/shopspring/decimal v1.4.0
)

require golang.org/x/net v0.0.0-20211029224645-99673261e6eb // indirect
