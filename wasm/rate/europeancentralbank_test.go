package rate

import (
	"testing"
)

func Test_isFiatEuropeanCentralBank(t *testing.T) {
	type args struct {
		code string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "EUR",
			args: args{
				code: "EUR",
			},
			want: true,
		},
		{
			name: "BTC",
			args: args{
				code: "BTC",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isFiatEuropeanCentralBank(tt.args.code); got != tt.want {
				t.Errorf("isFiatEuropeanCentralBank() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Unable to run because of cors errors
// func TestGetEuropeanCentralBank(t *testing.T) {
// 	type args struct {
// 		code string
// 	}
// 	tests := []struct {
// 		name    string
// 		args    args
// 		wantErr bool
// 	}{
// 		{
// 			name: "Simple USD",
// 			args: args{
// 				code: "USD",
// 			},
// 			wantErr: false,
// 		},
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			if err := GetEuropeanCentralBank(tt.args.code); (err != nil) != tt.wantErr {
// 				t.Errorf("GetEuropeanCentralBank() error = %v, wantErr %v", err, tt.wantErr)
// 			}
// 		})
// 	}
// }
