package rate

import (
	"errors"
	"net/http"
	"strconv"

	resty "github.com/go-resty/resty/v2"
)

func isLocal(code string) bool {
	return code == "MCO" ||
		code == "CAD" ||
		code == "CHF" ||
		code == "CNY" ||
		code == "GBP" ||
		code == "HKD" ||
		code == "JPY" ||
		code == "USD"
}

// GetLocal : get Local rate
func GetLocal(code string) error {
	if code == "" {
		return errors.New("coin empty")
	}
	client := resty.New()
	client.SetTransport(http.DefaultTransport)
	client.SetRetryCount(3)
	// client.SetDebug(true)
	// client.SetDebugBodyLimit(1)
	resource := "/rates/" + code + ".cffr"
	resp, err := client.R().
		Get(resource)
	if err != nil {
		return err
	}
	if !resp.IsSuccess() {
		return errors.New("Error Requesting " + resource + " " + strconv.Itoa(resp.StatusCode()))
	}
	_, err = ParseCFFR(resp.Body())
	if err == nil &&
		cffrToSave != nil {
		cffrToSave <- code
	}
	return nil
}
