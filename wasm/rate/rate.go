package rate

import (
	"log"
	"time"

	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/internal/app"
)

var rates = map[string]map[time.Time]decimal.Decimal{}
var needRequest = map[string]bool{}
var FetchRates = true
var cff app.Cff

// Flush : Flush all rates from memory
func Flush() {
	rates = map[string]map[time.Time]decimal.Decimal{}
	// clean variables
	cg.CoinsSuccess = make(map[string]bool)
}

func SetCff(app app.Cff) {
	cff = app
}

// Set : Set a rate for specific currency at specific date
func Set(coin string, date time.Time, rate decimal.Decimal) {
	if coin == "" {
		return
	}
	if rates[coin] == nil {
		rates[coin] = make(map[time.Time]decimal.Decimal)
	}
	d := date.UTC().Truncate(24 * time.Hour)
	rates[coin][d] = rate
}

// Get : Get a rate for specific currency at specific date
func Get(coin string, date time.Time, remoteFetch bool) decimal.Decimal {
	if coin == "" || !FetchRates {
		return decimal.New(0, 0)
	}
	if coin == "EUR" {
		return decimal.New(1, 0)
	}
	d := date.UTC().Truncate(24 * time.Hour)
	r := rates[coin][d]
	if !r.IsZero() {
		return r
	}
	if !remoteFetch {
		log.Println("Zero Rate for " + coin + " at " + d.String())
		return r
	}
	needRequest[coin] = false
	if isLocal(coin) {
		GetLocal(coin)
	} else if isFiatEuropeanCentralBank(coin) {
		GetEuropeanCentralBank(coin)
	} else {
		err := GetCoinGecko(coin)
		if err != nil {
			GetCoinLayer(coin, date)
		}
	}
	r = rates[coin][d]
	if r.IsZero() {
		// try to average with rates of -1/+1 day around
		y := rates[coin][d.Add(-24*time.Hour)]
		t := rates[coin][d.Add(24*time.Hour)]
		if !y.IsZero() && !t.IsZero() {
			r = decimal.Avg(y, t)
			rates[coin][d] = r
		}
	}
	if r.IsZero() {
		log.Println("Zero Rate for " + coin + " at " + d.String())
	}
	return r
}

// Prepare : get rate on background
func Prepare(coin string) {
	if coin == "" || coin == "EUR" || !FetchRates {
		return
	}
	if _, exist := needRequest[coin]; !exist {
		needRequest[coin] = true
	}
	if needRequest[coin] {
		needRequest[coin] = false
		if isLocal(coin) {
			go GetLocal(coin)
		} else if isFiatEuropeanCentralBank(coin) {
			go GetEuropeanCentralBank(coin)
		} else {
			go GetCoinGecko(coin)
		}
	}
}

// IsFiat : check if code is Fiat
func IsFiat(code string) bool {
	return isFiatEuropeanCentralBank(code)
}
