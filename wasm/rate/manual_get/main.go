package main

import (
	"flag"
	"log"
	"os"
	"time"

	"gitlab.com/c1560/CryptoFiscaFacile/wasm/rate"
)

func main() {
	pFilename := flag.String("file", "", "CFF Rate file")
	pCode := flag.String("code", "", "Curency Code")
	pDate := flag.String("date", "2021-01-01T15:04:05", "Rate Date to display")
	flag.Parse()
	if *pCode == "" {
		log.Fatal("Code must be given")
	}
	filename := *pCode + ".cffr"
	if *pFilename != "" {
		filename = *pFilename
	}
	data, err := os.ReadFile(filename)
	if err == nil {
		log.Println("Found cffr file, parse it...")
		rate.ParseCFFR(data)
		d, _ := time.ParseInLocation("2006-01-02T15:04:05", *pDate, time.FixedZone("UTC", 0))
		r := rate.Get(*pCode, d)
		log.Println(d, "->", r)
	}
}
