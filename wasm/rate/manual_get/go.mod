module gitlab.com/c1560/CryptoFiscaFacile/wasm/rate/manual_get

go 1.19

replace gitlab.com/c1560/CryptoFiscaFacile/wasm/rate => ../

require gitlab.com/c1560/CryptoFiscaFacile/wasm/rate v0.0.0-00010101000000-000000000000

require (
	github.com/go-resty/resty/v2 v2.7.0 // indirect
	github.com/shopspring/decimal v1.3.1 // indirect
	golang.org/x/net v0.0.0-20211029224645-99673261e6eb // indirect
)
