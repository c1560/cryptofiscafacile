module gitlab.com/c1560/CryptoFiscaFacile/wasm/rate/coinlayer_leecher

go 1.19

replace gitlab.com/c1560/CryptoFiscaFacile/wasm/rate => ../

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/go-resty/resty/v2 v2.13.1
	github.com/nanobox-io/golang-scribble v0.0.0-20190309225732-aa3e7c118975
	github.com/shopspring/decimal v1.4.0
	gitlab.com/c1560/CryptoFiscaFacile/wasm/rate v0.0.0-00010101000000-000000000000
)

require (
	github.com/jcelliott/lumber v0.0.0-20160324203708-dd349441af25 // indirect
	golang.org/x/net v0.0.0-20211029224645-99673261e6eb // indirect
)
