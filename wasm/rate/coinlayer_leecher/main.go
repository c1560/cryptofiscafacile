package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/davecgh/go-spew/spew"
	resty "github.com/go-resty/resty/v2"
	scribble "github.com/nanobox-io/golang-scribble"
	"github.com/shopspring/decimal"
	"gitlab.com/c1560/CryptoFiscaFacile/wasm/rate"
)

type historicalData struct {
	Success bool `json:"success"`
	Error   struct {
		Code int    `json:"code"`
		Type string `json:"type"`
		Info string `json:"info"`
	} `json:"error"`
	Terms      string             `json:"terms"`
	Privacy    string             `json:"privacy"`
	Timestamp  int                `json:"timestamp"`
	Target     string             `json:"target"`
	Historical bool               `json:"historical"`
	Date       string             `json:"date"`
	Rates      map[string]float64 `json:"rates"`
}

func main() {
	pFilename := flag.String("file", "", "CFF Rate file")
	pCode := flag.String("code", "", "Currency Code")
	pKey := flag.String("key", "", "CoinLayer Access Key")
	flag.Parse()
	if *pCode == "" {
		log.Fatal("Code must be given")
	}
	if *pKey == "" {
		log.Fatal("Access Key must be given")
	}
	filename := *pCode + ".cffr"
	if *pFilename != "" {
		filename = *pFilename
	}
	data, err := os.ReadFile(filename)
	if err == nil {
		log.Println("Found cffr file, parse it...")
		rate.ParseCFFR(data)
	}
	db, err := scribble.New("./Cache", nil)
	if err != nil {
		return
	}

	coin := strings.ReplaceAll(*pCode, "DSH", "dash") // Useless but keep as demo

	client := resty.New()
	client.SetTransport(http.DefaultTransport)
	client.SetRetryCount(3)
	// client.SetDebug(true)
	// client.SetDebugBodyLimit(100)

	date := time.Now().UTC()
	for date.After(time.Date(2009, time.January, 1, 0, 0, 0, 0, time.UTC)) {
		date = date.Add(-24 * time.Hour)
		var result historicalData
		err = db.Read("CoinLayer", "EUR-"+date.Format("2006-01-02"), &result)
		if err != nil {
			resource := "/" + date.Format("2006-01-02")
			headers := map[string]string{
				"Accept": "application/json",
			}
			queryParams := map[string]string{
				"access_key": *pKey,
				"target":     "EUR",
			}
			_, err := client.R().
				SetHeaders(headers).
				SetQueryParams(queryParams).
				SetResult(&result).
				Get("http://api.coinlayer.com" + resource)
			if err != nil {
				log.Println(err.Error())
				break
			}
		}
		if result.Success {
			log.Println("EUR-" + date.Format("2006-01-02"))
			db.Write("CoinLayer", "EUR-"+date.Format("2006-01-02"), result)
			rate.Set(coin, date, decimal.NewFromFloat(result.Rates[coin]))
		} else {
			log.Println("EUR-" + date.Format("2006-01-02") + " Error " + spew.Sdump(result.Error))
			break
		}
	}

	os.WriteFile(filename, rate.ExportCFFR(*pCode), 0666)
}
