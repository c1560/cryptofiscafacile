package rate

import (
	"os"
	"testing"
	"time"

	"github.com/shopspring/decimal"
)

func TestMain(m *testing.M) {
	FetchRates = true
	exitVal := m.Run()
	os.Exit(exitVal)
}

func TestGet(t *testing.T) {
	type args struct {
		code string
		date time.Time
	}
	tests := []struct {
		name string
		args args
		want decimal.Decimal
	}{
		{
			name: "Empty",
			args: args{
				code: "BTC",
				date: time.Date(2020, time.July, 7, 0, 0, 0, 0, time.FixedZone("UTC", 0)),
			},
			want: decimal.New(82591279296875, -10),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Flush()
			if got := Get(tt.args.code, tt.args.date, true); !got.Equal(tt.want) {
				t.Errorf("Get() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSetGet(t *testing.T) {
	type args struct {
		code      string
		date      time.Time
		rate      decimal.Decimal
		wantEqual bool
	}
	tests := []struct {
		name string
		args []args
	}{
		{
			name: "Set 1",
			args: []args{
				{
					code:      "BTC",
					date:      time.Date(2020, time.July, 7, 0, 0, 0, 0, time.FixedZone("UTC", 0)),
					rate:      decimal.New(123, -1),
					wantEqual: true,
				},
			},
		},
		{
			name: "Set 2 with 1 empty slot",
			args: []args{
				{
					code:      "BTC",
					date:      time.Date(2020, time.July, 7, 0, 0, 0, 0, time.FixedZone("UTC", 0)),
					rate:      decimal.New(123, -1),
					wantEqual: true,
				},
				{
					code:      "BTC",
					date:      time.Date(2020, time.July, 9, 0, 0, 0, 0, time.FixedZone("UTC", 0)),
					rate:      decimal.New(123, 1),
					wantEqual: true,
				},
			},
		},
		{
			name: "Set Same",
			args: []args{
				{
					code:      "BTC",
					date:      time.Date(2020, time.July, 9, 0, 0, 0, 0, time.FixedZone("UTC", 0)),
					rate:      decimal.New(123, 1),
					wantEqual: true,
				},
				{
					code:      "BTC",
					date:      time.Date(2020, time.July, 9, 0, 0, 0, 0, time.FixedZone("UTC", 0)),
					rate:      decimal.New(123, 1),
					wantEqual: true,
				},
			},
		},
		{
			name: "Modify Value",
			args: []args{
				{
					code:      "BTC",
					date:      time.Date(2020, time.July, 9, 0, 0, 0, 0, time.FixedZone("UTC", 0)),
					rate:      decimal.New(123, 0),
					wantEqual: false,
				},
				{
					code:      "BTC",
					date:      time.Date(2020, time.July, 9, 0, 0, 0, 0, time.FixedZone("UTC", 0)),
					rate:      decimal.New(123, 2),
					wantEqual: true,
				},
			},
		},
		{
			name: "New Code",
			args: []args{
				{
					code:      "BTC",
					date:      time.Date(2020, time.July, 9, 0, 0, 0, 0, time.FixedZone("UTC", 0)),
					rate:      decimal.New(123, 2),
					wantEqual: true,
				},
				{
					code:      "ETH",
					date:      time.Date(2020, time.July, 8, 0, 0, 0, 0, time.FixedZone("UTC", 0)),
					rate:      decimal.New(123, 0),
					wantEqual: true,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Flush()
			for _, arg := range tt.args {
				Set(arg.code, arg.date, arg.rate)
			}
			for _, arg := range tt.args {
				if got := Get(arg.code, arg.date, true); got.Equal(arg.rate) != arg.wantEqual {
					t.Errorf("Get() = %v, want %v", got, arg.rate)
				}
			}
		})
	}
}

func TestPrepare(t *testing.T) {
	type args struct {
		code string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "Empty args",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Prepare(tt.args.code)
		})
	}
}
