package rate

import (
	"bytes"
	"encoding/binary"
	"errors"
	"strings"
	"time"

	"github.com/shopspring/decimal"
)

var cffrToSave chan string

// Header : CFFR File Header
type Header struct {
	Magic [5]byte
	Code  [8]byte
	Start int64
	Count uint16
}

// ParseCFFR : Parse a file and autodetect Binary Rate Format
func ParseCFFR(data []byte) (coin string, err error) {
	var h Header
	buf := bytes.NewReader(data)
	err = binary.Read(buf, binary.LittleEndian, &h)
	if err != nil {
		return
	}
	if h.Magic != [5]byte{'$', 'C', 'F', 'F', 'R'} {
		return "", errors.New("wrong Magic")
	}
	coin = strings.Trim(string(h.Code[:]), "\x00")
	if rates[coin] == nil {
		rates[coin] = make(map[time.Time]decimal.Decimal)
		needRequest[coin] = false
	}
	start := time.Unix(h.Start, 0).UTC().Truncate(24 * time.Hour)
	for i := 0; i < int(h.Count); i++ {
		var r float64
		err = binary.Read(buf, binary.LittleEndian, &r)
		if err == nil {
			d := start.Add(time.Duration(i) * 24 * time.Hour)
			rates[coin][d] = decimal.NewFromFloat(r)
		}
	}
	return
}

// ExportCFFR : Export all rates for specific currency
func ExportCFFR(code string) (data []byte) {
	h := Header{Magic: [5]byte{'$', 'C', 'F', 'F', 'R'}}
	toCopy := 8
	if len(code) < toCopy {
		toCopy = len(code)
	}
	for i := 0; i < toCopy; i++ {
		h.Code[i] = code[i]
	}
	if r, ok := rates[code]; ok {
		// find the oldest and newest rate's date
		oldest := time.Now()
		newest := time.Unix(0, 0)
		found := 0
		genesis := time.Date(2009, time.January, 1, 0, 0, 0, 0, time.UTC)
		for k := range r {
			if k.After(genesis) {
				found++
				if k.Before(oldest) {
					oldest = k
				}
				if newest.Before(k) {
					newest = k
				}
			}
		}
		if found > 0 {
			h.Start = oldest.Unix()
			h.Count = uint16(newest.Sub(oldest).Hours()/24) + 1
			data = make([]byte, h.Count*8)
			for k, v := range r {
				if k.After(genesis) {
					f, _ := v.Float64()
					value := new(bytes.Buffer)
					err := binary.Write(value, binary.LittleEndian, f)
					if err == nil {
						offset := int(k.Sub(oldest).Hours()/24) * 8
						if offset >= 0 &&
							offset < len(data) {
							copy(data[offset:], value.Bytes())
						}
					}
				}
			}
		}
	}
	header := new(bytes.Buffer)
	binary.Write(header, binary.LittleEndian, h)
	return append(header.Bytes(), data...)
}

// CoinRates :
type CoinRates struct {
	Coin string
	CFFR []byte
}

// ExportAllCFFR : Export all rates for all currency
func ExportAllCFFR() (coinsRates []CoinRates) {
	for k := range rates {
		coinsRates = append(coinsRates, CoinRates{Coin: k, CFFR: ExportCFFR(k)})
	}
	return
}

// RegisterSaveChannel : Register a Save Channel
func RegisterSaveChannel(cffrToS chan string) {
	cffrToSave = cffrToS
}
